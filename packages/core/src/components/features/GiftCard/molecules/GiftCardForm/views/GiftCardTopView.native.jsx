import React from 'react';
import PropTypes from 'prop-types';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import {
  GiftImage,
  StepsViewWrapper,
  StepTextView,
  GiftCardTopContainer,
} from '../styles/GiftCardForm.style.native';
import BodyCopy from '../../../../../common/atoms/BodyCopy/views/BodyCopy';

const giftCardIcon = require('mobileapp/src/assets/images/e-giftcard-balance-card.png');

const stepsText = (stepCount, stepText, stepCoText) => (
  <StepsViewWrapper>
    <BodyCopy
      fontFamily="secondary"
      fontWeight="bold"
      fontSize="fs14"
      text={stepCount}
      color="gray.900"
    />
    <StepTextView>
      <BodyCopy fontFamily="secondary" fontSize="fs14" text={stepText} color="gray.900" />
      {stepCoText && (
        <BodyCopy fontFamily="secondary" fontSize="fs12" text={stepCoText} color="gray.900" />
      )}
    </StepTextView>
  </StepsViewWrapper>
);

const GiftCardTopView = ({ label }) => {
  const step1 = `${getLabelValue(label, 'lbl_gcb_step_1', 'giftCardBalance')}`;
  const step2 = `${getLabelValue(label, 'lbl_gcb_step_2', 'giftCardBalance')}`;
  const step3 = `${getLabelValue(label, 'lbl_gcb_step_3', 'giftCardBalance')}`;
  const step4 = `${getLabelValue(label, 'lbl_gcb_step_4', 'giftCardBalance')}`;
  const step1Data = getLabelValue(label, 'lbl_gcb_step_1_data', 'giftCardBalance');
  const step2Data = getLabelValue(label, 'lbl_gcb_step_2_data', 'giftCardBalance');
  const step2DataBracket = `(${getLabelValue(
    label,
    'lbl_gcb_step_2_data_brackets',
    'giftCardBalance'
  )})`;
  const step3Data = getLabelValue(label, 'lbl_gcb_step_3_data', 'giftCardBalance');
  const step4Data = getLabelValue(label, 'lbl_gcb_step_4_data', 'giftCardBalance');
  return (
    <GiftCardTopContainer>
      <BodyCopy
        fontFamily="secondary"
        testId="pageHeading"
        fontSize="fs18"
        fontWeight="bold"
        dataLocator="Page_Heading_link_header"
        text={getLabelValue(label, 'lbl_gcb_heading', 'giftCardBalance')}
        textAlign="center"
      />
      <GiftImage testId="gift-card-image" source={giftCardIcon} />
      {stepsText(step1, step1Data)}
      {stepsText(step2, step2Data, step2DataBracket)}
      {stepsText(step3, step3Data)}
      {stepsText(step4, step4Data)}
    </GiftCardTopContainer>
  );
};

GiftCardTopView.propTypes = {
  label: PropTypes.shape({}).isRequired,
};
export default GiftCardTopView;
