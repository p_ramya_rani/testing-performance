// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import GiftCardForm from '../GiftCardForm.view.native';

describe('GiftCardForm component', () => {
  const props = {
    labels: {},
    handleSubmit: jest.fn(),
    className: 'testclass',
    giftcardBalanceError: '',
    change: jest.fn(),
  };

  it('Should renders correctly', () => {
    const component = shallow(<GiftCardForm {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('Should renders correctly with error', () => {
    props.giftcardBalanceError = 'test error';
    const component = shallow(<GiftCardForm {...props} />);
    expect(component).toMatchSnapshot();
  });
});
