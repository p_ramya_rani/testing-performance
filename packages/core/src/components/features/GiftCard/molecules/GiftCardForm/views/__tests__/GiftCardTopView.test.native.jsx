import React from 'react';
import { shallow } from 'enzyme';
import GiftCardTopView from '../GiftCardTopView.native';

describe('GiftCardForm component', () => {
  const props = {
    labels: {},
  };
  it('Should renders correctly', () => {
    const component = shallow(<GiftCardTopView {...props} />);
    expect(component).toMatchSnapshot();
  });
});
