import React, { Fragment } from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import { reduxForm, Field } from 'redux-form';
import get from 'lodash/get';
import { FormStyle, GiftCardFormContainer } from '../styles/GiftCardForm.style.native';
import withStyles from '../../../../../common/hoc/withStyles';
import TextBox from '../../../../../common/atoms/TextBox';
import BodyCopy from '../../../../../common/atoms/BodyCopy';
import createValidateMethod from '../../../../../../utils/formValidation/createValidateMethod';
import getStandardConfig from '../../../../../../utils/formValidation/validatorStandardConfig';
import GiftCardTopView from './GiftCardTopView.native';
import RecaptchaModal from '../../../../../common/molecules/recaptcha/recaptchaModal';
import CustomButton from '../../../../../common/atoms/Button/views/Button';
import { getLabelValue } from '../../../../../../utils/utils';
import GiftCardBalance from '../../GiftCardBalance/views/GiftCardBalance.view';

const styles = {
  loginButtonStyle: {
    marginTop: 60,
  },
};

class GiftCard extends React.Component {
  constructor() {
    super();
    this.state = {
      setRecaptchaModalMountedState: false,
    };
  }

  componentWillUnmount() {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
  }

  onMessage = (event) => {
    const { change, handleSubmit } = this.props;
    if (event && event.nativeEvent.data) {
      let value = get(event, 'nativeEvent.data', '');
      if (['cancel', 'error', 'expired'].includes(value)) {
        value = '';
      }
      this.setRecaptchaModalMountState();
      this.timeout = setTimeout(() => {
        change('recaptchaToken', value);
        handleSubmit();
      }, 1000);
    }
  };

  onClose = () => {
    this.setRecaptchaModalMountState();
  };

  setRecaptchaModalMountState = () => {
    const { setRecaptchaModalMountedState } = this.state;
    this.setState({
      setRecaptchaModalMountedState: !setRecaptchaModalMountedState,
    });
  };

  handleAddGiftCardClick = (e) => {
    const { handleSubmit, isRecapchaEnabled } = this.props;
    e.preventDefault();
    if (isRecapchaEnabled) {
      this.setRecaptchaModalMountState();
    } else {
      handleSubmit();
    }
  };

  handleChange = () => {
    const { giftCardBalance, resetGiftCardBalance } = this.props;
    if (giftCardBalance && Object.keys(giftCardBalance).length > 0) {
      resetGiftCardBalance();
    }
  };

  render() {
    const { setRecaptchaModalMountedState } = this.state;
    const { className, giftcardBalanceError, labels, giftCardBalance } = this.props;
    return (
      <Fragment>
        <GiftCardFormContainer testId={`${className} container`}>
          <GiftCardTopView label={labels} />
          {giftcardBalanceError && (
            <BodyCopy
              testId="pageHeading"
              fontSize="fs12"
              color="error"
              fontFamily="secondary"
              fontWeight="bold"
              dataLocator="Page_Heading_link_header"
              text={giftcardBalanceError}
            />
          )}
          <Field
            id="giftCardNumber"
            label={`${getLabelValue(labels, 'lbl_gcb_gift_card_number', 'giftCardBalance')} `}
            name="giftCardNumber"
            keyboardType="number-pad"
            component={TextBox}
            dataLocator="gift-card-cardnaumberfield"
            className="input-field elem-mb-SM"
            onChange={(e) => this.handleChange(e)}
          />
          <Field
            label={`${getLabelValue(labels, 'lbl_gcb_gift_card_pin', 'giftCardBalance')} `}
            name="giftCardPin"
            component={TextBox}
            dataLocator="gift-card-pinnumberfield"
            keyboardType="number-pad"
            autoComplete="off"
            className="input-field elem-mb-SM"
            onChange={(e) => this.handleChange(e)}
          />
          <View>
            {setRecaptchaModalMountedState && (
              <RecaptchaModal
                onMessage={this.onMessage}
                setRecaptchaModalMountedState={setRecaptchaModalMountedState}
                toggleRecaptchaModal={this.setRecaptchaModalMountState}
                onClose={this.onClose}
              />
            )}

            <Field
              label=""
              component={TextBox}
              title=""
              type="hidden"
              name="recaptchaToken"
              id="recaptchaToken"
              data-locator="gift-card-recaptchcb"
            />
          </View>

          {giftCardBalance && Object.keys(giftCardBalance).length > 0 && (
            <GiftCardBalance giftCardBalance={giftCardBalance} />
          )}
          <CustomButton
            fill="BLUE"
            text={getLabelValue(labels, 'lbl_gcb_check_balance', 'giftCardBalance')}
            customStyle={styles.loginButtonStyle}
            onPress={this.handleAddGiftCardClick}
            className="check-balance-button-test-class"
          />
        </GiftCardFormContainer>
      </Fragment>
    );
  }
}

GiftCard.propTypes = {
  className: PropTypes.string,
  giftcardBalanceError: PropTypes.string,
  giftCardBalance: PropTypes.string,
  resetGiftCardBalance: PropTypes.string,
  handleSubmit: PropTypes.func.isRequired,
  change: PropTypes.func,
  labels: PropTypes.shape({}).isRequired,
  isRecapchaEnabled: PropTypes.bool,
};

GiftCard.defaultProps = {
  change: () => {},
  className: '',
  giftcardBalanceError: '',
  giftCardBalance: '',
  resetGiftCardBalance: '',
  isRecapchaEnabled: false,
};

const validateMethod = createValidateMethod(
  getStandardConfig([
    { giftCardNumber: 'giftCardNumber' },
    { giftCardPin: 'cardPin' },
    'recaptchaToken',
  ])
);
export default reduxForm({
  form: 'GiftCard',
  ...validateMethod,
})(withStyles(GiftCard, FormStyle));
