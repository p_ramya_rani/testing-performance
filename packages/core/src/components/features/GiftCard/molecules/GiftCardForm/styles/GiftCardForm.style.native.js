// 9fbef606107a605d69c0edbcd8029e5d
import styled, { css } from 'styled-components/native';

const getPageStyle = (props) => {
  const { theme } = props;
  return `
  margin: ${theme.spacing.APP_LAYOUT_SPACING.SM} ${theme.spacing.APP_LAYOUT_SPACING.XS} auto ${
    theme.spacing.APP_LAYOUT_SPACING.XS
  };
  justify-content: ${'center'};

  `;
};
const FormStyle = css`
  ${getPageStyle}
`;

/**
 * @param {Object} props : props for getTextBaseStyle
 * @return {Object} : Return object
 * @desc This method get font base style
 */

const getTextBaseStyle = (props) => {
  const { theme } = props;
  const { typography, colorPalette } = theme;
  return `
  font-size: ${typography.fontSizes.fs12};
  color: ${colorPalette.text.secondary};
  font-family: ${typography.fonts.secondary};
  `;
};

/**
 * @param {Object} props : props for getTextBaseStyle
 * @return {Object} : Return object
 * @desc This method get font base style
 */
const getDescriptionStyle = (props) => {
  const { theme } = props;
  const { typography, colorPalette } = theme;
  return `
  ${getTextBaseStyle};
  font-size: ${typography.fontSizes.fs12};
  color: ${colorPalette.text.primary};
  margin-top: ${'27px'};
  text-align: ${'center'};
  `;
};

const HideShowField = (props) =>
  `
  background: ${props.theme.colorPalette.white};
  height:${props.theme.spacing.ELEM_SPACING.MED};
  position: absolute;
  right: 0;
  top: 20px;
  border-bottom-width: 1px;
  border-bottom-color: black;
  `;

const DescriptionStyle = styled.Text`
  ${getDescriptionStyle}
`;

const HideShowFieldWrapper = styled.View`
  ${HideShowField}
`;

const GuestButtonWrapper = styled.View`
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
`;

const IconContainer = styled.View`
  position: absolute;
  right: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  width: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
`;

const StyledErrorWrapper = styled.View`
  display: flex;
  flex-direction: row;
  width: 90%;
  margin-bottom: 20px;
`;
const StepsViewWrapper = styled.View`
  margin-bottom: 20px;
  width: 90%;
  display: flex;
  flex-direction: row;
`;

const BalanceContainer = styled.View`
  margin-top: ${(props) => props.theme.spacing.APP_LAYOUT_SPACING.XXS};
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
`;
const GiftCardFormContainer = styled.View`
  margin-bottom: 150px;
`;
const InputTextViewWrapper = styled.View``;
const StyledErrorIcon = styled.View`
  margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
`;

const GiftImage = styled.Image`
  width: 254.7px;
  height: 160px;
  align-self: center;
  margin: 24px 59.3px 24px 61px;
`;

const GiftCardTopContainer = styled.View`
  margin-top: 10px;
  margin-bottom: 20px;
`;

const RecaptchaContainer = styled.View`
  width: 100%;
  height: 100px;
  margin-bottom: 40px;
`;
const GiftPriceView = styled.View`
  margin-top: 25px;
  margin-bottom: 25px;
`;

export const MyView = styled.View`
  margin-top: 25px;
  margin-bottom: 25px;
`;

const StepTextView = styled.View`
  margin-left: 25px;
  margin-right: 10px;
`;

export {
  GiftCardFormContainer,
  BalanceContainer,
  GiftCardTopContainer,
  StepTextView,
  GiftPriceView,
  RecaptchaContainer,
  GiftImage,
  InputTextViewWrapper,
  StepsViewWrapper,
  StyledErrorIcon,
  StyledErrorWrapper,
  FormStyle,
  DescriptionStyle,
  HideShowFieldWrapper,
  GuestButtonWrapper,
  IconContainer,
};
