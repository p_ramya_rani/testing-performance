// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  .page-heading {
    text-align: center;
    @media ${(props) => props.theme.mediaQuery.mediumMax} {
      margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    }
  }
  &.container {
    margin: 0 auto;
    width: 75%;
    font-family: 'Nunito';
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: 92%;
    }
    .steps {
      font-family: 'Nunito';
      span {
        font-family: 'Nunito';
      }
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        display: flex;
      }
    }
  }

  .sub-heading {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      flex: 0 0 auto;
    }
  }
  .sub-data {
    display: inline-block;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      display: none;
    }
  }
  .sub-data-mobile {
    display: inline-block;
    flex: 1;
    @media ${(props) => props.theme.mediaQuery.medium} {
      display: none;
    }
  }
  .text-box.text-box {
    width: 449px;
    height: 46px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: 100%;
      height: auto;
      margin-right: 0;
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XXXL};
      width: 100%;
      margin-right: 0;
    }
  }
  .gift-card-image {
    width: 255px;
    height: 160px;
    margin-right: auto;
    margin-left: auto;
    @media ${(props) => props.theme.mediaQuery.large} {
      flex-direction: row;
      order: 1;
    }
    @media ${(props) => props.theme.mediaQuery.mediumMax} {
      order: 0;
      margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    }
  }
  .top-section {
    font-family: 'Nunito';
    width: 100%;
    display: flex;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      flex-direction: column;
    }
    @media ${(props) => props.theme.mediaQuery.mediumMax} {
      flex-direction: column;
    }
  }
  .captcha {
    @media ${(props) => props.theme.mediaQuery.mediumMax} {
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    }
  }
  .input-row {
    margin-right: 0px;
    margin-left: 0px;
  }

  .check-balance-button {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-top: ${(props) => props.theme.spacing.LAYOUT_SPACING.XL};
      width: 100%;
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      margin-top: ${(props) => props.theme.spacing.LAYOUT_SPACING.XL};
    }
  }
`;

export default styles;
