// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { getIconPath, getViewportInfo, isClient } from '@tcp/core/src/utils';
import PropTypes from 'prop-types';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import { Heading } from '@tcp/core/styles/themes/TCP/typotheme';
import { reduxForm, Field, change as reduxChange, untouch } from 'redux-form';
import Notification from '@tcp/core/src/components/common/molecules/Notification';
import styles from '../styles/GiftCardForm.styles';
import withStyles from '../../../../../common/hoc/withStyles';
import { Image, Row, Col, Button, TextBox, BodyCopy } from '../../../../../common/atoms';
import Recaptcha from '../../../../../common/molecules/recaptcha/recaptcha';
import createValidateMethod from '../../../../../../utils/formValidation/createValidateMethod';
import getStandardConfig from '../../../../../../utils/formValidation/validatorStandardConfig';

class GiftCard extends React.PureComponent {
  focusFirstField = () => {
    const { fullPage } = this.props;
    if (isClient()) {
      const { isMobile = false } = getViewportInfo() || {};
      return fullPage && !isMobile;
    }
    return false;
  };

  render() {
    const { className, handleSubmit, change, giftcardBalanceError, labels } = this.props;
    return (
      <BodyCopy component="div" className={`${className} container`}>
        <Heading HeadingSmall="four" fontWeight="bold" className="elem-mb-XXXL page-heading">
          {getLabelValue(labels, 'lbl_gcb_heading', 'giftCardBalance')}
        </Heading>
        <BodyCopy component="div" className="top-section">
          <Image alt="gift card" src={getIconPath('GIFT-CARD-MODAL')} className="gift-card-image" />
          <Col colSize={{ small: '50%', medium: '50%', large: '50%' }} colCount="2">
            <BodyCopy component="p" fontFamily="Nunito" className="instructions">
              <BodyCopy
                component="div"
                fontSize={['fs14', 'fs14', 'fs16']}
                className="elem-mb-LRG_1 steps"
              >
                <BodyCopy component="span" fontWeight="bold" className="elem-mr-MED sub-heading">
                  {getLabelValue(labels, 'lbl_gcb_step_1', 'giftCardBalance')}
                </BodyCopy>
                {getLabelValue(labels, 'lbl_gcb_step_1_data', 'giftCardBalance')}
              </BodyCopy>

              <BodyCopy
                component="div"
                fontSize={['fs14', 'fs14', 'fs16']}
                className="elem-mb-LRG_1 steps"
              >
                <BodyCopy component="span" fontWeight="bold" className="elem-mr-MED sub-heading">
                  {getLabelValue(labels, 'lbl_gcb_step_2', 'giftCardBalance')}
                </BodyCopy>

                <BodyCopy component="span">
                  {getLabelValue(labels, 'lbl_gcb_step_2_data', 'giftCardBalance')}
                  <BodyCopy
                    component="span"
                    fontSize={['fs12', 'fs10', 'fs12']}
                    className="sub-data"
                  >
                    &nbsp;
                    {`(${getLabelValue(
                      labels,
                      'lbl_gcb_step_2_data_brackets',
                      'giftCardBalance'
                    )})`}
                  </BodyCopy>
                  <BodyCopy
                    component="span"
                    fontSize={['fs12', 'fs10', 'fs12']}
                    className="sub-data-mobile"
                  >
                    {`(${getLabelValue(
                      labels,
                      'lbl_gcb_step_2_data_brackets',
                      'giftCardBalance'
                    )})`}
                  </BodyCopy>
                </BodyCopy>
              </BodyCopy>

              <BodyCopy
                component="div"
                fontSize={['fs14', 'fs14', 'fs16']}
                className="elem-mb-LRG_1 steps"
              >
                <BodyCopy component="span" fontWeight="bold" className="elem-mr-MED sub-heading">
                  {getLabelValue(labels, 'lbl_gcb_step_3', 'giftCardBalance')}
                </BodyCopy>
                {getLabelValue(labels, 'lbl_gcb_step_3_data', 'giftCardBalance')}
              </BodyCopy>

              <BodyCopy
                component="div"
                fontSize={['fs14', 'fs14', 'fs16']}
                className="elem-mb-LRG_1 steps"
              >
                <BodyCopy component="span" fontWeight="bold" className="elem-mr-MED sub-heading">
                  {getLabelValue(labels, 'lbl_gcb_step_4', 'giftCardBalance')}
                </BodyCopy>
                {getLabelValue(labels, 'lbl_gcb_step_4_data', 'giftCardBalance')}
              </BodyCopy>
            </BodyCopy>
          </Col>
        </BodyCopy>
        {giftcardBalanceError && (
          <Notification
            status="error"
            colSize={{ large: 12, medium: 8, small: 6 }}
            message={giftcardBalanceError}
          />
        )}
        <form onSubmit={handleSubmit}>
          <Row className="input-row">
            <Field
              id="giftCardNumber"
              placeholder={getLabelValue(labels, 'lbl_gcb_gift_card_number', 'giftCardBalance')}
              name="giftCardNumber"
              component={TextBox}
              className="elem-mr-LRG_2 text-box"
              height="40px"
              tabIndex="0"
              autoFocus={this.focusFirstField()}
            />
            <Field
              id="giftCardPin"
              placeholder={getLabelValue(labels, 'lbl_gcb_gift_card_pin', 'giftCardBalance')}
              name="giftCardPin"
              component={TextBox}
              className="elem-mb-SM text-box"
            />
          </Row>
          <Row fullBleed>
            <BodyCopy component="div" className="elem-mt-XXL captcha">
              <Recaptcha
                verifyCallback={(token) => change('recaptchaToken', token)}
                expiredCallback={() => change('recaptchaToken', '')}
              />
              <Field
                id="recaptchaToken"
                component={TextBox}
                type="hidden"
                name="recaptchaToken"
                data-locator="giftcard-recaptcha"
              />
            </BodyCopy>
          </Row>

          <Button
            fill="BLUE"
            type="submit"
            buttonVariation="variable-width"
            className="elem-mt-XL check-balance-button"
          >
            {getLabelValue(labels, 'lbl_gcb_check_balance', 'giftCardBalance')}
          </Button>
        </form>
      </BodyCopy>
    );
  }
}

GiftCard.propTypes = {
  className: PropTypes.string,
  giftcardBalanceError: PropTypes.string,
  handleSubmit: PropTypes.func.isRequired,
  change: PropTypes.func,
  labels: PropTypes.shape({}).isRequired,
  fullPage: PropTypes.bool,
};

GiftCard.defaultProps = {
  change: () => {},
  className: '',
  giftcardBalanceError: '',
  fullPage: false,
};

const validateMethod = createValidateMethod(
  getStandardConfig([
    { giftCardNumber: 'giftCardNumber' },
    { giftCardPin: 'cardPin' },
    'recaptchaToken',
  ])
);

const afterSubmit = (result, dispatch) => {
  dispatch(reduxChange('GiftCard', 'recaptchaToken', ''));
  dispatch(untouch('GiftCard', 'recaptchaToken'));
};

export default reduxForm({
  form: 'GiftCard',
  onSubmitSuccess: afterSubmit,
  ...validateMethod,
})(withStyles(GiftCard, styles));

export { GiftCard as GiftCardFormVanilla };
