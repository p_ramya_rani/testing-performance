import React from 'react';
import { shallow } from 'enzyme';
import GiftCardTopView from '../GiftCardTopView.native';

describe('GiftCardForm component', () => {
  const props = {
    labels: {
      giftCardBalance: {
        lbl_gcb_check_balance: 'Check Balance',
        lbl_gcb_gift_card_number: 'Gift Card / Merchandise Credit Number',
        lbl_gcb_gift_card_pin: 'Gift Card Pin',
        lbl_gcb_heading: 'How to check your gift card balance',
        lbl_gcb_step_1: 'Step 1',
        lbl_gcb_step_1_data: 'Enter gift card number.',
        lbl_gcb_step_2: 'Step 2',
        lbl_gcb_step_2_data: 'Enter PIN number',
        lbl_gcb_step_2_data_brackets: 'Refer to the example for PIN location',
        lbl_gcb_step_3: 'Step 3',
        lbl_gcb_step_3_data: 'Click Check Balance Button',
        lbl_gcb_step_4: 'Step 4',
        lbl_gcb_step_4_data: 'To check balances on additional gift cards, repeat steps 1-3',
      },
    },
  };
  it('Should renders correctly', () => {
    const component = shallow(<GiftCardTopView label={props.labels} />);
    expect(component).toMatchSnapshot();
  });
});
