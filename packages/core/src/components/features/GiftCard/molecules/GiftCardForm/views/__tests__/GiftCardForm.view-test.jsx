// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { GiftCardFormVanilla } from '../GiftCardForm.view';

describe('GiftCardForm component', () => {
  const props = {
    labels: {},
    handleSubmit: jest.fn(),
    className: 'testclass',
    giftcardBalanceError: '',
  };

  it('Should renders correctly', () => {
    const component = shallow(<GiftCardFormVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('Should renders correctly with error', () => {
    props.giftcardBalanceError = 'test error';
    const component = shallow(<GiftCardFormVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('Should fire onSubmit callback when form is submitted', () => {
    const wrapper = shallow(<GiftCardFormVanilla {...props} />);
    const form = wrapper.find('form');
    form.find('#giftCardNumber').simulate('change', { target: { value: '6006491259499902590' } });
    form.find('#giftCardPin').simulate('change', { target: { value: '9734' } });
    form
      .find('#recaptchaToken')
      .simulate('change', { target: { value: '03AGdBq27Ta0YK8IyuuXFkOqC' } });
    form.simulate('submit');
    expect(props.handleSubmit).toHaveBeenCalled();
  });

  it('Should display input field error on the submit', () => {
    const wrapper = shallow(<GiftCardFormVanilla {...props} />);
    const form = wrapper.find('form');
    form.find('#giftCardNumber').simulate('change', { target: { value: '' } });
    form.find('#giftCardPin').simulate('change', { target: { value: '' } });
    form.find('#recaptchaToken').simulate('change', { target: { value: '' } });
    form.simulate('submit');
    setTimeout(() => {
      expect(wrapper.find('[role="alert"]')).toHaveLength(3);
    }, 500);
  });
});
