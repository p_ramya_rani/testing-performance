// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  position: relative;
  margin: 0 auto;
  width: 75%;
  @media ${(props) => props.theme.mediaQuery.smallOnly} {
    width: 92%;
  }
  .gift-card-balance {
    position: relative;
    text-align: center;
    z-index: -1;
    bottom: 169px;
    @media ${(props) => props.theme.mediaQuery.large} {
      right: 34px;
    }

    @media ${(props) => props.theme.mediaQuery.mediumMax} {
      bottom: 107px;
      text-align: left;
      font-size: ${(props) => props.theme.typography.fontSizes.fs32};
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      bottom: 110px;
      text-align: left;
      font-size: ${(props) => props.theme.typography.fontSizes.fs20};
    }
  }
`;

export default styles;
