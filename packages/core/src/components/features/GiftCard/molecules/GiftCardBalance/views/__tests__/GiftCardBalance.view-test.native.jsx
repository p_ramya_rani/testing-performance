// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import GiftCardBalance from '../GiftCardBalance.view.native';

describe('GiftCardBalance component', () => {
  const props = {
    labels: {},
    className: 'test-class',
    giftCardBalance: {
      giftCardAuthorizedAmt: '0',
      giftCardNbr: '***************2624',
      giftCardPin: '****',
    },
  };
  it('Should renders correctly', () => {
    const component = shallow(<GiftCardBalance {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('Should renders GiftCardBalance correctly', () => {
    const component = shallow(<GiftCardBalance {...props} />);
    expect(component.text().includes('$0')).toBe(false);
  });
});
