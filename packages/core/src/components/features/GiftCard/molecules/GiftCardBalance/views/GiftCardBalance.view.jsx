// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { BodyCopy } from '@tcp/core/src/components/common/atoms';
import withStyles from '../../../../../common/hoc/withStyles';
import styles from '../styles/GiftCardBalance.styles';

const GiftCardBalance = ({ giftCardBalance: { giftCardAuthorizedAmt }, className }) => (
  <div className={`${className}`}>
    <BodyCopy
      component="p"
      fontWeight="bold"
      fontFamily="secondary"
      fontSize="fs38"
      className="gift-card-balance"
    >
      {`$${giftCardAuthorizedAmt}`}
    </BodyCopy>
  </div>
);

GiftCardBalance.propTypes = {
  giftCardBalance: PropTypes.shape({
    giftCardAuthorizedAmt: PropTypes.oneOfType([
      PropTypes.number.isRequired,
      PropTypes.string.isRequired,
    ]).isRequired,
    giftCardNbr: PropTypes.string.isRequired,
    giftCardPin: PropTypes.string.isRequired,
  }).isRequired,
  className: PropTypes.string,
};

GiftCardBalance.defaultProps = {
  className: '',
};

export default withStyles(GiftCardBalance, styles);

export { GiftCardBalance as GiftCardBalanceVanilla };
