import React from 'react';
import PropTypes from 'prop-types';
import { BodyCopy } from '@tcp/core/src/components/common/atoms';
import { BalanceContainer } from '../../GiftCardForm/styles/GiftCardForm.style.native';

const GiftCardBalance = ({ giftCardBalance: { giftCardAuthorizedAmt } }) => (
  <BalanceContainer testId="gift-card-balance">
    <BodyCopy
      testId="pageHeading"
      fontSize="fs20"
      fontFamily="secondary"
      fontWeight="extrabold"
      dataLocator="Page_Heading_link_header"
      text={`$${giftCardAuthorizedAmt} `}
    />
  </BalanceContainer>
);

GiftCardBalance.propTypes = {
  giftCardBalance: PropTypes.shape({
    giftCardAuthorizedAmt: PropTypes.oneOfType([
      PropTypes.number.isRequired,
      PropTypes.string.isRequired,
    ]).isRequired,
    giftCardNbr: PropTypes.string.isRequired,
    giftCardPin: PropTypes.string.isRequired,
  }).isRequired,
};

export default GiftCardBalance;
