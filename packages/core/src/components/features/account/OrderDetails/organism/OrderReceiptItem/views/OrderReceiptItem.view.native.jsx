// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { BodyCopy, Image } from '@tcp/core/src/components/common/atoms';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import { BodyCopyWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';
import {
  ImageStyle,
  ImageBrandStyle,
  OrderItemContainer,
  OrderItemImage,
  OrderItemContent,
  OrderContentWrapper,
} from '../styles/OrderReceiptItem.style.native';

const gymboreeImage = require('../../../../../../../../../mobileapp/src/assets/images/gymboree-logo.png');
const tcpImage = require('../../../../../../../../../mobileapp/src/assets/images/tcp-logo.png');

const getImagePath = (imagePath) =>
  imagePath.indexOf('https:') > -1 ? imagePath : `https:${imagePath}`;

const getCommonBodyCopy = (textCopy) => (
  <BodyCopy fontSize="fs13" mobileFontFamily="secondary" text={textCopy} />
);

const OrderReceiptItem = ({ ...otherProps }) => {
  const {
    item: {
      productInfo: { name, imagePath, variantNo, upc },
      itemInfo: { itemBrand, quantity, quantityCanceled },
    },
    isCanceledList,
    ordersLabels,
    returnDate,
  } = otherProps;
  return (
    <>
      <OrderItemContainer>
        <OrderItemImage>
          {imagePath && (
            <ImageStyle>
              <Image url={getImagePath(imagePath)} height={100} width={100} isFastImage />
            </ImageStyle>
          )}
          {itemBrand === 'TCP' && <ImageBrandStyle source={tcpImage} />}
          {itemBrand === 'GYM' && <ImageBrandStyle source={gymboreeImage} />}
        </OrderItemImage>
        <OrderItemContent spacingStyles="padding-right-SM padding-left-SM">
          <BodyCopy
            fontSize="fs14"
            fontWeight="bold"
            textAlign="left"
            text={name}
            fontFamily="secondary"
          />
          {variantNo ? (
            <BodyCopyWithSpacing
              spacingStyles="margin-top-XS"
              mobileFontFamily="secondary"
              fontSize="fs13"
              text={`${getLabelValue(ordersLabels, 'lbl_orderDetails_itemnumber')} ${variantNo}`}
            />
          ) : (
            <BodyCopyWithSpacing
              spacingStyles="margin-top-XS"
              mobileFontFamily="secondary"
              fontSize="fs13"
              text={`${getLabelValue(ordersLabels, 'lbl_orderDetails_upc')} ${upc}`}
            />
          )}
          <OrderContentWrapper>
            {getCommonBodyCopy(getLabelValue(ordersLabels, 'lbl_orderDetails_qty'))}
            {getCommonBodyCopy(isCanceledList ? quantityCanceled : quantity)}
          </OrderContentWrapper>
          {returnDate ? (
            <OrderContentWrapper>
              {getCommonBodyCopy(getLabelValue(ordersLabels, 'lbl_orderDetails_return_by'))}
              {getCommonBodyCopy(returnDate)}
            </OrderContentWrapper>
          ) : null}
        </OrderItemContent>
      </OrderItemContainer>
    </>
  );
};

OrderReceiptItem.propTypes = {
  orderGroup: PropTypes.shape({}).isRequired,
};

OrderReceiptItem.defaultProps = {};

export default OrderReceiptItem;
