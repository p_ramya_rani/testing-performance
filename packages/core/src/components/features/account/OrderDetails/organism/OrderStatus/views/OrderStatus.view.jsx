// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { BodyCopy, Row, Col, Anchor } from '@tcp/core/src/components/common/atoms';
import {
  getLabelValue,
  getOrderGroupLabelAndMessage,
  hasTrackingNum,
  getTrimmedTrackingNum,
} from '@tcp/core/src/utils/utils';
import ClickTracker from '@tcp/web/src/components/common/atoms/ClickTracker';
import constants from '../../../OrderDetails.constants';

/**
 * This function component use for return the Order Status
 * can be passed in the component.
 * @param otherProps - otherProps object used pass params to other component
 */

const renderOrderTracking = (showNumAsLink, trackingUrl, trackingNumber, ordersLabels) => {
  return (
    <>
      <BodyCopy className={showNumAsLink ? 'button-track-rounded' : 'button-track'} component="div">
        <ClickTracker
          clickData={{
            customEvents: ['event118'],
            eventName: 'trackingnumberclick',
            pageName: 'myplace:orders:order details',
            pageShortName: 'myplace:orders:order details',
          }}
        >
          {!showNumAsLink && (
            <Anchor
              to={trackingUrl}
              anchorVariation="button"
              buttonVariation="fixed-width"
              fill="BLUE"
              centered
              dataLocator={trackingNumber}
              target="_blank"
            >
              {getLabelValue(ordersLabels, 'lbl_orders_trackit')}
            </Anchor>
          )}
          {showNumAsLink && trackingNumber && (
            <Anchor
              to={trackingUrl}
              anchorVariation="button"
              className="track-order-cta"
              right
              target="_blank"
            >
              {getLabelValue(ordersLabels, 'lbl_orders_trackit')}
            </Anchor>
          )}
        </ClickTracker>
      </BodyCopy>
      {showNumAsLink && trackingNumber && (
        <BodyCopy className="orderDetail-trackingId">
          <BodyCopy
            component="span"
            className="orderDetail-trackingId-lbl"
            fontWeight="bold"
            fontFamily="secondary"
          >
            {getLabelValue(ordersLabels, 'lbl_orders_trackingId')}
          </BodyCopy>
          <Anchor
            underline
            anchorVariation="primary"
            to={trackingUrl}
            dataLocator={trackingNumber}
            target="_blank"
          >
            <BodyCopy fontFamily="secondary" fontWeight="semibold">
              {trackingNumber}
            </BodyCopy>
          </Anchor>
        </BodyCopy>
      )}
    </>
  );
};

/**
 * This function component use for return the Order Status
 * can be passed in the component.
 * @param otherProps - otherProps object used pass params to other component
 */

const OrderStatus = (props) => {
  const {
    isBopisOrder,
    trackingNumber,
    trackingUrl,
    ordersLabels,
    showNumAsLink,
    status = '',
  } = props;
  const { label, message } = getOrderGroupLabelAndMessage(props);
  const trimTrackingNumber = getTrimmedTrackingNum(trackingNumber);

  return (
    <Row fullBleed>
      <Col colSize={{ large: showNumAsLink ? 1 : 6, medium: 8, small: 6 }}>
        <BodyCopy className="order-status-header" component="div">
          <BodyCopy component="span" fontSize="fs16" fontFamily="secondary">
            {label}
          </BodyCopy>

          <BodyCopy fontWeight="extrabold" component="span" fontSize="fs16" fontFamily="secondary">
            {message}
          </BodyCopy>

          {!isBopisOrder && hasTrackingNum(trimTrackingNumber) && (
            <>
              <div />
              {!showNumAsLink && (
                <BodyCopy className="orderDetail-trackingNumber">
                  <BodyCopy component="span" fontSize="fs16" fontFamily="secondary">
                    {getLabelValue(ordersLabels, 'lbl_orders_trackingNumber')}
                  </BodyCopy>
                  <BodyCopy
                    component="span"
                    fontWeight="extrabold"
                    fontSize="fs16"
                    fontFamily="secondary"
                  >
                    {trimTrackingNumber}
                  </BodyCopy>
                </BodyCopy>
              )}
            </>
          )}
        </BodyCopy>
      </Col>

      {!isBopisOrder &&
        trackingUrl &&
        trackingUrl !== constants.STATUS_CONSTANTS.NA &&
        status.toLowerCase() !== constants.STATUS_CONSTANTS.ORDER_BEING_PROCESSED && (
          <Col
            className="button-container"
            colSize={{ large: showNumAsLink ? 11 : 6, medium: 0, small: 6 }}
          >
            <Row fullBleed>
              <Col colSize={{ large: 3, medium: 8, small: 6 }} />
              <Col colSize={{ large: 9, medium: 8, small: 6 }}>
                {renderOrderTracking(showNumAsLink, trackingUrl, trimTrackingNumber, ordersLabels)}
              </Col>
            </Row>
          </Col>
        )}
    </Row>
  );
};
OrderStatus.propTypes = {
  trackingNumber: PropTypes.string,
  trackingUrl: PropTypes.string,
  isBopisOrder: PropTypes.bool.isRequired,
  ordersLabels: PropTypes.shape({
    lbl_orderDetails_shipping: PropTypes.string,
  }),
  showNumAsLink: PropTypes.bool,
};

OrderStatus.defaultProps = {
  trackingNumber: '',
  trackingUrl: '',
  ordersLabels: {
    lbl_orderDetails_shipping: '',
  },
  showNumAsLink: false,
};

export default OrderStatus;
