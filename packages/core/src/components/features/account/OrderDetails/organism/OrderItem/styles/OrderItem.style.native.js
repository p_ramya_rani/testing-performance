// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';
import { ViewWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';

const OrderItemContainer = styled.View`
  display: flex;
  flex-direction: row;
  padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
`;

const OrderItemImage = styled.View`
  display: flex;
  align-items: center;
  width: 30%;
  opacity: ${(props) => (props.isReturnOrRefund ? 0.5 : 1)};
`;

const OrderItemContent = styled(ViewWithSpacing)`
  width: 70%;
`;

const OrderContentWrapper = styled.View`
  display: flex;
  flex-direction: row;
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
`;

const ImageStyle = styled.View`
  padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
`;

const ImageBrandStyle = styled.Image`
  width: 65px;
  height: 22px;
  /* stylelint-disable-next-line */
  resize-mode: contain;
`;

const ReturnedItems = styled.View`
  opacity: ${(props) => (props.isReturnOrRefund ? 0.5 : 1)};
`;

const Infobar = styled.View`
  background: ${(props) => props.theme.colorPalette.gray[300]};
  padding: 9px;
  max-width: 100%;
  width: 100%;
  margin-left: 0;
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
  font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy2}px;
  flex-direction: row;
  flex-shrink: 1;
  margin-bottom: 0;
`;

export {
  ImageStyle,
  ImageBrandStyle,
  OrderItemContainer,
  OrderItemImage,
  OrderContentWrapper,
  OrderItemContent,
  ReturnedItems,
  Infobar,
};
