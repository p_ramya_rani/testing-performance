// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';
import { ViewWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';

const OrderItemContainer = styled.View`
  display: flex;
  flex-direction: row;
`;

const OrderItemImage = styled.View`
  display: flex;
  align-items: center;
  width: 30%;
`;

const OrderItemContent = styled(ViewWithSpacing)`
  width: 70%;
`;

const OrderContentWrapper = styled.View`
  display: flex;
  flex-direction: row;
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.XXS};
`;

const ImageStyle = styled.View`
  padding-bottom: ${props => props.theme.spacing.ELEM_SPACING.SM};
`;

const ImageBrandStyle = styled.Image`
  width: 65px;
  height: 22px;
  /* stylelint-disable-next-line */
  resize-mode: contain;
`;

export {
  ImageStyle,
  ImageBrandStyle,
  OrderItemContainer,
  OrderItemImage,
  OrderContentWrapper,
  OrderItemContent,
};
