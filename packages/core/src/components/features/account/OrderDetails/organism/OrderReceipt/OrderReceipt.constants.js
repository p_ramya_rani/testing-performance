// 9fbef606107a605d69c0edbcd8029e5d
export default {
  BRAND_TCP: 'TCP',
  BRAND_GYMBOREE: 'GYM',
  STATUS_CONSTANTS: {
    ORDER_SHIPPED: 'order shipped',
    ORDER_PARTIALLY_SHIPPED: 'partially shipped',
    ITEMS_READY_FOR_PICKUP: 'ready for pickup',
    ITEMS_PICKED_UP: 'picked up',
    ORDER_PICKED_UP: 'order picked up',
    EXPIRED: 'order expired',
    CANCELLED: 'order cancelled',
    SUCCESSFULLY_PICKED_UP: 'successfully picked up',
    ORDER_RECEIVED: 'order received',
    ORDER_IN_PROCESS: 'order in process',
    ORDER_CANCELED: 'canceled',
    ORDER_CANCELLED: 'cancelled',
    ITEMS_RECEIVED: 'received',
    ORDER_RECEIVED_LABEL: 'lbl_orders_statusOrderReceived',
    ORDER_RETURNED: 'order returned',
    RETURN_COMPLETED: 'return completed',
    RETURN_INITIATED: 'return initiated',
  },
};
