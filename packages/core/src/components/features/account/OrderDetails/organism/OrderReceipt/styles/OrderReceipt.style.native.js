// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';

const OrderReceiptWrapper = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  text-align: center;
`;

const OrderNoReceiptContainer = styled.View`
  margin-top: 25%;
  width: 87%;
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
`;

const BackButton = styled.View`
  margin-top: ${props => props.theme.spacing.LAYOUT_SPACING.SM};
  width: 100%;
`;

const PrintReceiptContainer = styled.View`
  margin-top: ${props => props.theme.spacing.LAYOUT_SPACING.XS};
  width: 100%;
`;

const ReceiptsTextContainer = styled.View`
  width: 80%;
`;

const ReceiptsContainer = styled.View`
  padding: 20px 10px;
`;

const ShipmentViewContainer = styled.View`
  border: 1px solid ${props => props.theme.colorPalette.gray[300]};
  margin: ${props => props.theme.spacing.ELEM_SPACING.SM} 0px;
`;

const ShipmentHeaderContainer = styled.View`
  background: ${props => props.theme.colorPalette.gray[300]};
  height: 45px;
  flex-direction: row;
  padding: 0 ${props => props.theme.spacing.ELEM_SPACING.SM};
`;

const ShipmentTextContainer = styled.View`
  align-items: flex-start;
  justify-content: center;
  flex: 0.4;
`;

const GiftContainer = styled.View`
  flex: 0.6;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-end;
  text-align: center;
`;

const ShipmentItemWrapper = styled.View`
  margin: 10px;
`;

const HiddenBarCodeReceipt = styled.View`
  position: absolute;
  top: 999%;
`;

const BarcodeContainer = styled.View`
  margin: ${props => props.theme.spacing.ELEM_SPACING.MED};
`;

const OrderReturnContainer = styled.View`
  border: 1px solid ${props => props.theme.colorPalette.gray[600]};
  padding: ${props => props.theme.spacing.ELEM_SPACING.SM};
`;

export {
  OrderReceiptWrapper,
  OrderNoReceiptContainer,
  BackButton,
  ReceiptsTextContainer,
  ReceiptsContainer,
  ShipmentViewContainer,
  ShipmentHeaderContainer,
  ShipmentItemWrapper,
  HiddenBarCodeReceipt,
  PrintReceiptContainer,
  GiftContainer,
  ShipmentTextContainer,
  BarcodeContainer,
  OrderReturnContainer,
};
