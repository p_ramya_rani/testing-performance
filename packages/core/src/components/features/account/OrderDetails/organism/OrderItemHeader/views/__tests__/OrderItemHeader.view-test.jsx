// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import constants from '../../../../OrderDetails.constants';
import OrderItemHeader from '../OrderItemHeader.view';

const ORDER_STATUS = {
  SHIPPED_ON: 'Shipped on',
  DELIVERED_ON: 'Delivered On',
};

describe('Order Basic Details component ', () => {
  it('should render correctly for ECOM order', () => {
    const props = {
      OrderDetailsData: {
        orderStatus: constants.STATUS_CONSTANTS.ORDER_RECEIVED,
        shippedDate: '2019-10-09',
        trackingNumber: '123455',
        trackingUrl: '/test',
        orderNumber: '654321',
        purchasedItems: [
          {
            items: [
              {
                itemInfo: {
                  itemBrand: 'TCP',
                  linePrice: 24.94,
                  listPrice: 24.94,
                  offerPrice: 24.94,
                  quantity: 1,
                  quantityCanceled: 0,
                  quantityOOS: 0,
                  quantityReturned: 0,
                  quantityShipped: 0,
                },
              },
            ],
          },
        ],
        summary: {
          currencySymbol: '$',
        },
        isBopisOrder: false,
        ordersLabels: {},
        orderType: 'ECOM',
      },
    };
    const component = shallow(<OrderItemHeader {...props} />);
    expect(component).toMatchSnapshot();
  });

  it.skip('should render order with ship Date', () => {
    const props = {
      status: constants.STATUS_CONSTANTS.SHIPPED,
      ordersLabels: {
        lbl_orderDetails_shipment: 'Shipped',
        lbl_orderDetails_shipment_on: ORDER_STATUS.SHIPPED_ON,
      },
      index: 1,
      totalCount: 1,
      returnedOrder: false,
      orderGroup: {
        shipDate: '9/27/21',
        quantity: 1,
        shipVia: 'UFSI',
        status: 'processing',
        trackingNbr: '420770079200190185641401868623',
        trackingUrl: '',
      },
      extObj: { isBopisOrder: false, isNonEcom: false },
      estimatedDeliveryDate: '',
      actualDeliveryDate: '',
    };
    const component = shallow(<OrderItemHeader {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should render delivery order with ActualDelivery Date', () => {
    const props = {
      status: constants.STATUS_CONSTANTS.DELIVERED,
      ordersLabels: {
        lbl_edd_delivered_on: ORDER_STATUS.DELIVERED_ON,
        lbl_orderDetails_shipment_on: ORDER_STATUS.SHIPPED_ON,
        lbl_orderDetails_shipment: 'Shipped',
      },
      index: 1,
      totalCount: 1,
      returnedOrder: false,
      orderGroup: {
        shipDate: '',
        quantity: 1,
        shipVia: 'UFSI',
        status: ORDER_STATUS.SHIPPED_ON,
        trackingNbr: '420770079200190185641401868623',
        trackingUrl:
          'https://childrensplace.narvar.com/childrensplace/tracking/usps?tracking_numbers=420770079200190185641401868623&service=FC&ozip=07470&dzip=77007&locale=en_US&order_number=343984240&order_date=2021-08-31T04:09:00.000-04:00&ship_date=2021-08-31T08:28:11.000-04:00',
      },
      extObj: { isBopisOrder: false, isNonEcom: false },
      estimatedDeliveryDate: '',
      actualDeliveryDate: '2021-09-16 18:10:10',
    };
    const component = shallow(<OrderItemHeader {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('should render shipper order with Arriving at EDD', () => {
    const props = {
      status: constants.STATUS_CONSTANTS.SHIPPED,
      ordersLabels: {
        lbl_edd_delivered_on: ORDER_STATUS.DELIVERED_ON,
        lbl_edd_arrivedby: 'Arriving by',
        lbl_orderDetails_shipment_on: ORDER_STATUS.SHIPPED_ON,
        lbl_orderDetails_shipment: 'Shipped',
      },
      index: 1,
      totalCount: 1,
      returnedOrder: false,
      orderGroup: {
        shipDate: '',
        quantity: 1,
        shipVia: 'UFSI',
        status: 'Out for Delivery',
        trackingNbr: '420770079200190185641401868623',
        trackingUrl:
          'https://childrensplace.narvar.com/childrensplace/tracking/usps?tracking_numbers=420770079200190185641401868623&service=FC&ozip=07470&dzip=77007&locale=en_US&order_number=343984240&order_date=2021-08-31T04:09:00.000-04:00&ship_date=2021-10-31T08:28:11.000-04:00',
      },
      extObj: { isBopisOrder: false, isNonEcom: false },
      estimatedDeliveryDate: '2021-09-24',
      actualDeliveryDate: '',
    };
    const component = shallow(<OrderItemHeader {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('should render shipper order with Arriving when you get carrier date', () => {
    const props = {
      status: constants.STATUS_CONSTANTS.SHIPPED,
      ordersLabels: {
        lbl_edd_delivered_on: ORDER_STATUS.DELIVERED_ON,
        lbl_edd_arrivedby: 'Arriving by',
        lbl_orderDetails_shipment_on: ORDER_STATUS.SHIPPED_ON,
        lbl_orderDetails_shipment: 'Shipped',
      },
      index: 1,
      totalCount: 1,
      returnedOrder: false,
      orderGroup: {
        shipDate: '',
        quantity: 1,
        shipVia: 'UFSI',
        status: 'Out for Delivery',
        trackingNbr: '420770079200190185641401868623',
        trackingUrl:
          'https://childrensplace.narvar.com/childrensplace/tracking/usps?tracking_numbers=420770079200190185641401868623&service=FC&ozip=07470&dzip=77007&locale=en_US&order_number=343984240&order_date=2021-08-31T04:09:00.000-04:00&ship_date=2021-08-31T08:28:11.000-04:00',
      },
      extObj: { isBopisOrder: false, isNonEcom: false },
      estimatedDeliveryDate: '',
      carrierDeliveryDate: '2021-09-24',
      actualDeliveryDate: '',
    };
    const component = shallow(<OrderItemHeader {...props} />);
    expect(component).toMatchSnapshot();
  });
});
