// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  .shipment-container {
    border: 1px solid ${(props) => props.theme.colors.TEXTBOX.BACKGROUND};
    margin-bottom: 20px;
  }
  .no-border-margin {
    border-bottom: none;
    margin-bottom: 0px;
  }

  .shipment-header {
    background: ${(props) => props.theme.colors.TEXTBOX.BACKGROUND};
    color: #101010;
    display: flex;
    padding: 13px 28px;
    justify-content: space-between;
  }
  .shipment-index {
    font-size: 16px;
    font-family: ${(props) => props.theme.fonts.secondary};
    font-weight: 700;
  }
  .print-text {
    color: inherit;
  }
  .print-receipt-button {
    .print-text {
      color: inherit;
    }
  }
  .print-gift-cta {
    display: inline-block;
    vertical-align: middle;
  }
  .print-gift-reciept {
    font-size: 16px;
    font-family: ${(props) => props.theme.fonts.secondary};
    font-weight: 700;
    text-decoration: underline;
    cursor: pointer;
    .print-gift-cta {
      display: inline-block;
      vertical-align: middle;
      .print-text {
        color: inherit;
      }
    }
  }
  .shipment-body {
    padding: 8px;
  }
  .item-info-container {
    display: flex;
    border-bottom: 1px solid #d7d7d7;
  }

  .no-border-bottom {
    border-bottom: none;
  }
  .item-thumb-container {
    margin: 0 16px;
  }
  .item-thumb {
    width: 107px;
    height: 133px;
  }
  .item-details-container {
    color: #101010;
    font-size: 14px;
  }
  .item-details-container .item-name {
    margin-bottom: 13px;
    text-align: left;
    margin-top: 8px;
  }
  .brand-logo {
    text-align: center;
    margin: 6px 0;
  }
  .shipment-barcode-container {
    text-align: center;
    padding: 20px;
  }
  .shipment-barcode-container p {
    margin-bottom: 6px;
  }
  .print-gift-icon {
    width: 17px;
    margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    vertical-align: middle;
  }
  .no-receipts-container {
    margin: 0 auto;
    text-align: center;
    p {
      padding: 0 48px;
    }
  }

  .order-return-message {
    border: 1px solid ${(props) => props.theme.colorPalette.gray[600]};
    padding: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  }

  .no-receipts-heading {
    margin-top: ${(props) => props.theme.spacing.LAYOUT_SPACING.LRG};
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  }
  .back-to-order-cta {
    max-width: 410px;
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
  }
  @media ${(props) => props.theme.mediaQuery.mediumOnly} {
    .no-receipts-container p {
      padding: 0 32%;
    }
  }
`;

export default styles;
