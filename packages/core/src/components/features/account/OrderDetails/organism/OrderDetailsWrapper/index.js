export {
  renderOrderDetailWithCRMView,
  renderReturnedAndRefundedOrders,
  renderCancelledAndOutOfStockOrders,
  renderBopisAndBossOrder,
} from './views/OrderDetailsWrapper.view';
