const getItemQuantity = (
  isCanceledList,
  quantityCanceled,
  isNonEcom,
  quantity,
  quantityReturned = 0
) => {
  if (isCanceledList) {
    return quantityCanceled;
  }
  if (isNonEcom && quantity > quantityReturned) {
    return quantity - quantityReturned;
  }
  return quantity;
};

export default getItemQuantity;
