// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { BodyCopy, Image } from '@tcp/core/src/components/common/atoms';
import { getLabelValue, mobileOrderDateFormatted } from '@tcp/core/src/utils/utils';
import { getTranslatedDate } from '@tcp/core/src/components/features/account/Orders/molecules/OrderListWithDetails/views/OrderListWithDetails.utils';
import { getRCStatus } from '@tcp/core/src/components/features/account/OrderDetails/organism/OrderDetailsWrapper/views/OrderDetailsWrapper.utils';
import {
  getLogoIconPath,
  getStatusTextClass,
} from '../../OrderDetailsWrapper/views/OrderDetailsWrapper.utils';
import OrderStatus from '../../OrderStatus';
import constants from '../../../OrderDetails.constants';

const { STATUS_CONSTANTS = {} } = constants;
const { PROCESSING, ORDER_IN_PROCESS, ORDER_BEING_PROCESSED } = STATUS_CONSTANTS;

const displayPickedUpDate = (pickedUpDate, ordersLabels) => {
  return pickedUpDate && pickedUpDate.toLowerCase() !== 'n/a' ? (
    <BodyCopy component="p" className="shipment-count">
      {`${getLabelValue(ordersLabels, 'lbl_orders_on')} ${getTranslatedDate(pickedUpDate, true)}`}
    </BodyCopy>
  ) : null;
};

const getEstimateDate = (estimatedDate) =>
  estimatedDate ? mobileOrderDateFormatted(estimatedDate) : '';

const getStatusText = (status, labels) => {
  switch (status) {
    case PROCESSING:
    case ORDER_IN_PROCESS:
    case ORDER_BEING_PROCESSED:
      return getLabelValue(labels, 'lbl_order_being_processed', 'orders');
    default:
      return status;
  }
};

const OrderItemHeader = ({
  status,
  ordersLabels,
  index,
  totalCount,
  returnedOrder,
  orderGroup,
  extObj,
  estimatedDeliveryDate,
  actualDeliveryDate,
  carrierDeliveryDate,
  returnInitiatedOrder,
}) => {
  const statusText = status.toLowerCase();
  const actualDeliveryDateValue = getEstimateDate(actualDeliveryDate);
  const carrierDeliveryDateValue = getEstimateDate(carrierDeliveryDate);
  const estimatedDeliveryDateValue = getEstimateDate(estimatedDeliveryDate);
  const shipDateDateValue = getEstimateDate(orderGroup?.shipDate);

  const { label, displayDate } =
    getRCStatus(
      status,
      estimatedDeliveryDateValue,
      actualDeliveryDateValue,
      shipDateDateValue,
      carrierDeliveryDateValue,
      ordersLabels
    ) || {};
  return (
    <div className="order-status-container">
      <div className="order-status-logo">
        <Image src={getLogoIconPath(statusText)} />
      </div>
      <div className="order-status-text">
        <BodyCopy
          component="p"
          fontWeight="bold"
          fontFamily="secondary"
          className={`${getStatusTextClass(statusText)} ${
            returnedOrder || returnInitiatedOrder ? 'no-text-transform elem-mb-XS' : ''
          }`}
          fontSize="fs18"
        >
          {returnedOrder || returnInitiatedOrder ? status : getStatusText(statusText, ordersLabels)}
        </BodyCopy>

        {index && index > 0 && totalCount && !extObj.isNonEcom ? (
          <BodyCopy component="p" className="shipment-count">
            {`${getLabelValue(
              ordersLabels,
              'lbl_orderDetails_shipment'
            )} ${index} of ${totalCount}`}
            {label && displayDate && (
              <>
                <span className="elem-mr-XXS elem-ml-XXS">|</span>
                <span className="elem-mr-XS delivered">{`${label} ${displayDate}`}</span>
              </>
            )}
          </BodyCopy>
        ) : null}

        {returnedOrder ? (
          <BodyCopy
            component="p"
            fontSize={['fs14', 'fs12', 'fs14']}
            fontFamily="secondary"
            fontWeight="semibold"
          >
            {`${getLabelValue(ordersLabels, 'lbl_orders_returnedNotification')}`}
          </BodyCopy>
        ) : null}
        {returnInitiatedOrder ? (
          <BodyCopy
            component="p"
            fontSize={['fs14', 'fs12', 'fs14']}
            fontFamily="secondary"
            fontWeight="semibold"
          >
            {`${getLabelValue(ordersLabels, 'lbl_orders_initiatedNotification')}`}
          </BodyCopy>
        ) : null}
        {displayPickedUpDate(extObj?.pickedUpDate, ordersLabels)}
      </div>
      {orderGroup && (
        <div className="order-status-cta hide-on-mobile">
          <OrderStatus
            status={orderGroup.status}
            trackingNumber={orderGroup.trackingNbr}
            trackingUrl={orderGroup.trackingUrl}
            shippedDate={orderGroup.shipDate}
            ordersLabels={ordersLabels}
            isBopisOrder={extObj && extObj.isBopisOrder}
            showNumAsLink
          />
        </div>
      )}
    </div>
  );
};

OrderItemHeader.propTypes = {
  status: PropTypes.string,
  ordersLabels: PropTypes.shape({}),
  index: PropTypes.number,
  totalCount: PropTypes.number,
  returnedOrder: PropTypes.bool,
  orderGroup: PropTypes.shape([]),
  extObj: PropTypes.shape({}),
  estimatedDeliveryDate: PropTypes.string,
  actualDeliveryDate: PropTypes.string,
  carrierDeliveryDate: PropTypes.string,
  returnInitiatedOrder: PropTypes.bool,
};

OrderItemHeader.defaultProps = {
  status: '',
  ordersLabels: {},
  index: 0,
  totalCount: 0,
  returnedOrder: false,
  returnInitiatedOrder: false,
  orderGroup: [],
  extObj: {},
  estimatedDeliveryDate: '',
  actualDeliveryDate: '',
  carrierDeliveryDate: '',
};

export default OrderItemHeader;
