// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { BodyCopy, Anchor, Image } from '@tcp/core/src/components/common/atoms';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import {
  ViewWithSpacing,
  BodyCopyWithSpacing,
} from '@tcp/core/src/components/common/atoms/styledWrapper';
import { disableTcpAppProduct, getBrandIcon } from '@tcp/core/src/utils';
import {
  ImageStyle,
  ImageBrandStyle,
  OrderItemContainer,
  OrderItemImage,
  OrderItemContent,
  OrderContentWrapper,
  ReturnedItems,
  Infobar,
} from '../styles/OrderItem.style.native';
import getItemQuantity from './OrderItem.view.utils';

const showReviewLink = ({ propShowReview, itemBrand }) =>
  propShowReview && !disableTcpAppProduct(itemBrand);

const getImagePath = (imagePath) =>
  imagePath.indexOf('https:') > -1 ? imagePath : `https:${imagePath}`;

const getItemNumber = (ordersLabels, variantNo, upc) => {
  return variantNo ? (
    <BodyCopyWithSpacing
      spacingStyles="margin-top-XXS"
      fontFamily="secondary"
      color="gray.800"
      fontSize="fs14"
      text={`${getLabelValue(ordersLabels, 'lbl_orderDetails_itemnumber')} ${variantNo}`}
    />
  ) : (
    <BodyCopyWithSpacing
      spacingStyles="margin-top-XXS"
      fontFamily="secondary"
      color="gray.800"
      fontSize="fs14"
      text={`${getLabelValue(ordersLabels, 'lbl_orderDetails_upc')} ${upc}`}
    />
  );
};

const renderAppeasement = (ordersLabels, appeasement, isAppeasementEnabled, currencySymbol) => {
  return appeasement && isAppeasementEnabled ? (
    <Infobar>
      <BodyCopy
        fontFamily="secondary"
        fontSize="fs12"
        text={`${getLabelValue(ordersLabels, 'lbl_orders_adjustment')}, - ${currencySymbol}${Number(
          appeasement
        ).toFixed(2)}`}
        color="black"
        fontWeight="bold"
      />
    </Infobar>
  ) : null;
};

const OrderItems = ({ className, ...otherProps }) => {
  /**
   * @function return  Used to render the JSX of the component
   * @param    {[Void]} function does not accept anything.
   * @return   {[Object]} JSX of the component
   */

  const {
    item: {
      productInfo: { name, imagePath, color, fit, size, variantNo, upc, pdpUrl },
      itemInfo: {
        linePrice,
        itemBrand,
        quantity,
        quantityCanceled,
        quantityReturned,
        listPrice,
        offerPrice,
        appeasement,
      },
    },
    currencySymbol,
    isCanceledList,
    isShowWriteReview,
    ordersLabels,
    navigation,
    isReturnOrRefund,
    isAppeasementEnabled,
    isNonEcom,
  } = otherProps;
  const itemQuantity = getItemQuantity(
    isCanceledList,
    quantityCanceled,
    isNonEcom,
    quantity,
    quantityReturned
  );

  return (
    <>
      <OrderItemContainer>
        <OrderItemImage isReturnOrRefund={isReturnOrRefund}>
          {imagePath && (
            <ImageStyle>
              <Image url={getImagePath(imagePath)} height={100} width={100} isFastImage />
            </ImageStyle>
          )}
          {itemBrand ? <ImageBrandStyle source={getBrandIcon({ itemBrand })} /> : null}
        </OrderItemImage>
        <OrderItemContent spacingStyles="padding-right-SM padding-left-SM">
          <ReturnedItems isReturnOrRefund={isReturnOrRefund}>
            <BodyCopy
              fontSize="fs14"
              fontWeight="semibold"
              textAlign="left"
              text={name}
              fontFamily="secondary"
            />
            {getItemNumber(ordersLabels, variantNo, upc)}
            {!!size && (
              <BodyCopyWithSpacing
                spacingStyles="margin-top-XXS"
                margin-top="margin-top-XXS"
                fontFamily="secondary"
                color="gray.800"
                fontSize="fs14"
                text={`${getLabelValue(ordersLabels, 'lbl_orderDetails_color')} ${color.name}`}
              />
            )}
            <OrderContentWrapper>
              {!!fit && (
                <BodyCopy
                  fontFamily="secondary"
                  color="gray.800"
                  fontSize="fs14"
                  text={`${getLabelValue(ordersLabels, 'lbl_orderDetails_fit')} ${fit}`}
                />
              )}
              {!!size && (
                <BodyCopy
                  fontFamily="secondary"
                  color="gray.800"
                  fontSize="fs14"
                  text={`${getLabelValue(ordersLabels, 'lbl_orderDetails_size')} ${size}`}
                />
              )}
            </OrderContentWrapper>
          </ReturnedItems>
          <ViewWithSpacing spacingStyles="margin-top-MED">
            <ReturnedItems isReturnOrRefund={isReturnOrRefund}>
              <OrderContentWrapper>
                <BodyCopy
                  fontSize="fs14"
                  fontFamily="secondary"
                  fontWeight="semibold"
                  text={getLabelValue(ordersLabels, 'lbl_orderDetails_price')}
                />

                <BodyCopy
                  fontSize="fs14"
                  fontFamily="secondary"
                  text={`${currencySymbol}${listPrice.toFixed(2)}`}
                />
              </OrderContentWrapper>

              <OrderContentWrapper>
                <BodyCopy
                  fontSize="fs14"
                  fontFamily="secondary"
                  fontWeight="semibold"
                  text={getLabelValue(ordersLabels, 'lbl_orderDetails_youPaid')}
                />

                <BodyCopy
                  fontSize="fs14"
                  fontFamily="secondary"
                  text={`${currencySymbol}${offerPrice.toFixed(2)}`}
                />
              </OrderContentWrapper>
              <OrderContentWrapper>
                <BodyCopy
                  fontSize="fs14"
                  fontFamily="secondary"
                  fontWeight="semibold"
                  text={getLabelValue(ordersLabels, 'lbl_orderDetails_quantity')}
                />

                <BodyCopy fontSize="fs14" fontFamily="secondary" text={itemQuantity} />
              </OrderContentWrapper>
              <OrderContentWrapper>
                <BodyCopy
                  fontSize="fs14"
                  fontFamily="secondary"
                  fontWeight="semibold"
                  text={getLabelValue(ordersLabels, 'lbl_orderDetails_subTotal')}
                />

                <BodyCopy
                  fontSize="fs14"
                  fontFamily="secondary"
                  text={`${currencySymbol}${(linePrice || 0).toFixed(2)}`}
                />
              </OrderContentWrapper>
            </ReturnedItems>
            {showReviewLink({
              propShowReview: isShowWriteReview,
              itemBrand,
            }) && (
              <OrderContentWrapper>
                <Anchor
                  url={pdpUrl}
                  text={getLabelValue(ordersLabels, 'lbl_orderDetails_writeReview')}
                  underline
                  noLink
                  href="#"
                  anchorVariation="primary"
                  fontSize="fs14"
                  fontWeight="semibold"
                  onPress={() =>
                    navigation.navigate('ProductDetail', {
                      title: name,
                      pdpUrl,
                      reset: true,
                    })
                  }
                />
              </OrderContentWrapper>
            )}
          </ViewWithSpacing>
        </OrderItemContent>
      </OrderItemContainer>
      {renderAppeasement(ordersLabels, appeasement, isAppeasementEnabled, currencySymbol)}
    </>
  );
};
OrderItems.propTypes = {
  className: PropTypes.string,
  currencySymbol: PropTypes.string.isRequired,
  orderGroup: PropTypes.shape({}).isRequired,
};

OrderItems.defaultProps = {
  className: '',
};

export default OrderItems;
