import getItemQuantity from '../OrderItem.view.utils';

describe('OrderItem utils function', () => {
  it('should return quantity as 1', () => {
    expect(getItemQuantity(true, 1, false, 2, 0)).toEqual(1);
  });
  it('should return picked up item as 2', () => {
    expect(getItemQuantity(false, 0, true, 2, 0)).toEqual(2);
  });
  it('should return picked up item as 1', () => {
    expect(getItemQuantity(false, 1, true, 2, 1)).toEqual(1);
  });
  it('should return chipped items as 5', () => {
    expect(getItemQuantity(false, 1, false, 5, 1)).toEqual(5);
  });
});
