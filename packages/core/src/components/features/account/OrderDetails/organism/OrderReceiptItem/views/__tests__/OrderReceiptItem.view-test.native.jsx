// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import OrderReceiptItem from '../OrderReceiptItem.view.native';

describe('Order Items component', () => {
  it('should renders correctly', () => {
    const props = {
      item: {
        productInfo: {
          name: 'test product',
          imagePath: 'test',
          upc: 'test',
          pdpUrl: 'test',
        },
        itemInfo: {
          linePrice: 2.15,
          itemBrand: 'TCP',
          quantity: 1,
          quantityCanceled: false,
          listPrice: 2.25,
          offerPrice: 2.25,
        },
      },
      ordersLabels: {},
    };
    const component = shallow(<OrderReceiptItem {...props} />);
    expect(component).toMatchSnapshot();
  });
});
