// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import { BodyCopy, Image } from '@tcp/core/src/components/common/atoms';
import { getLabelValue, mobileOrderDateFormatted } from '@tcp/core/src/utils/utils';
import { getRCStatus } from '@tcp/core/src/components/features/account/OrderDetails/organism/OrderDetailsWrapper/views/OrderDetailsWrapper.utils';
import {
  getStatusTextClass,
  getShipmentIcon,
  getTranslateFormattedDate,
} from '../../OrderDetailsWrapper/views/OrderDetailsWrapper.utils';
import {
  OrderLogoAndStatus,
  OrderStatusImage,
  ShipmentText,
  OrderHeadingStatus,
  ReturnLabelText,
} from '../styles/OrderItemHeader.style.native';
import constants from '../../../OrderDetails.constants';

const pickedUpIcon = require('../../../../../../../../../mobileapp/src/assets/images/order-delivered-colored-filled.png');
const shippingCancelled = require('../../../../../../../../../mobileapp/src/assets/images/cancelled.png');
const inTransit = require('../../../../../../../../../mobileapp/src/assets/images/in-transit.png');
const outForDelivery = require('../../../../../../../../../mobileapp/src/assets/images/out-for-delivery.png');
const processingPreparing = require('../../../../../../../../../mobileapp/src/assets/images/processing-preparing.png');
const readyForPickup = require('../../../../../../../../../mobileapp/src/assets/images/ready-for-pickup.png');
const shipped = require('../../../../../../../../../mobileapp/src/assets/images/shipped.png');
const returns = require('../../../../../../../../../mobileapp/src/assets/images/order-returned-colored-filled.png');

const iconsObj = {
  pickedUpIcon,
  shippingCancelled,
  inTransit,
  outForDelivery,
  partiallyReturned: shippingCancelled,
  processingPreparing,
  readyForPickup,
  shipped,
  returns,
};

const renderPickedUpDate = (trackingInfo, ordersLabels) => {
  const { pickedUpDate } = trackingInfo || {};
  return pickedUpDate && pickedUpDate.toLowerCase() !== 'n/a' ? (
    <>
      <BodyCopy
        fontFamily="secondary"
        fontSize="fs14"
        color="gray.800"
        text={`${getLabelValue(ordersLabels, 'lbl_orders_on')} ${getTranslateFormattedDate(
          pickedUpDate.replace(/-/g, '/')
        )}`}
      />
    </>
  ) : null;
};

const getEstimateDate = (date) => (date ? mobileOrderDateFormatted(date) : '');

const renderEstimatedDate = (
  status,
  ordersLabels,
  estimateDeliveryDate,
  actualDeliveryDate,
  carrierDeliveryDate,
  trackingInfo
) => {
  const actualDeliveryDateValue = getEstimateDate(actualDeliveryDate);
  const carrierDeliveryDateValue = getEstimateDate(carrierDeliveryDate);
  const estimatedDeliveryDateValue = getEstimateDate(estimateDeliveryDate);
  const shipDateDateValue = getEstimateDate(trackingInfo?.shipDate);
  const { label, displayDate } =
    getRCStatus(
      status,
      estimatedDeliveryDateValue,
      actualDeliveryDateValue,
      shipDateDateValue,
      carrierDeliveryDateValue,
      ordersLabels
    ) || {};
  return label && displayDate ? (
    <>
      <BodyCopy fontFamily="secondary" fontSize="fs14" color="gray.800" text=" | " />
      <BodyCopy
        fontFamily="secondary"
        fontSize="fs14"
        color="green.500"
        text={`${label} ${displayDate}`}
      />
    </>
  ) : null;
};

const { STATUS_CONSTANTS = {} } = constants;
const { PROCESSING, ORDER_IN_PROCESS, ORDER_BEING_PROCESSED } = STATUS_CONSTANTS;
const getStatusText = (status, labels) => {
  switch (status) {
    case PROCESSING:
    case ORDER_IN_PROCESS:
    case ORDER_BEING_PROCESSED:
      return getLabelValue(labels, 'lbl_order_being_processed', 'orders');
    default:
      return status;
  }
};

const OrderItemHeader = ({
  statusText,
  ordersLabels,
  index,
  totalCount,
  returnedOrder,
  isNonEcom,
  trackingInfo,
  estimatedDeliveryDate,
  actualDeliveryDate,
  carrierDeliveryDate,
  returnInitiatedOrder,
}) => {
  return (
    <OrderLogoAndStatus>
      <OrderStatusImage>
        <Image
          height="24px"
          width="24px"
          source={getShipmentIcon(statusText && statusText.toLowerCase().trim(), iconsObj)}
        />
      </OrderStatusImage>
      <View>
        <ShipmentText
          isReturn={returnedOrder || returnInitiatedOrder}
          fontFamily="secondary"
          fontSize="fs16"
          fontWeight="bold"
          statusTextClass={`${getStatusTextClass(statusText)} ${
            returnedOrder || returnInitiatedOrder ? 'returnInitiated' : ''
          }`}
          text={
            returnedOrder || returnInitiatedOrder
              ? statusText
              : getStatusText(statusText, ordersLabels)
          }
        />
        {index && index > 0 && totalCount && !isNonEcom ? (
          <OrderHeadingStatus>
            <BodyCopy
              fontFamily="secondary"
              fontSize="fs14"
              color="gray.800"
              text={`${getLabelValue(
                ordersLabels,
                'lbl_orderDetails_shipment',
                'orders'
              )} ${index} of ${totalCount}`}
            />
            {renderEstimatedDate(
              statusText,
              ordersLabels,
              estimatedDeliveryDate,
              actualDeliveryDate,
              carrierDeliveryDate,
              trackingInfo
            )}
          </OrderHeadingStatus>
        ) : null}
        {returnedOrder ? (
          <ReturnLabelText
            fontFamily="secondary"
            fontSize="fs14"
            text={`${getLabelValue(ordersLabels, 'lbl_orders_returnedNotification')}`}
          />
        ) : null}
        {returnInitiatedOrder ? (
          <ReturnLabelText
            fontFamily="secondary"
            fontSize="fs14"
            text={getLabelValue(ordersLabels, 'lbl_orders_initiatedNotification')}
          />
        ) : null}
        {renderPickedUpDate(trackingInfo, ordersLabels)}
      </View>
    </OrderLogoAndStatus>
  );
};

OrderItemHeader.propTypes = {
  statusText: PropTypes.string,
  ordersLabels: PropTypes.shape({}),
  index: PropTypes.number,
  totalCount: PropTypes.number,
  returnedOrder: PropTypes.bool,
  isNonEcom: PropTypes.bool,
  trackingInfo: PropTypes.shape({}),
  estimatedDeliveryDate: PropTypes.string,
  actualDeliveryDate: PropTypes.string,
  carrierDeliveryDate: PropTypes.string,
  returnInitiatedOrder: PropTypes.bool,
};

OrderItemHeader.defaultProps = {
  statusText: '',
  ordersLabels: {},
  index: 0,
  totalCount: 0,
  returnedOrder: false,
  isNonEcom: false,
  trackingInfo: {},
  estimatedDeliveryDate: '',
  actualDeliveryDate: '',
  carrierDeliveryDate: '',
  returnInitiatedOrder: false,
};

export default OrderItemHeader;
