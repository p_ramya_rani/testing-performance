import React from 'react';
import { View } from 'react-native';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import { parseDate, COMPLETE_MONTH } from '@tcp/core/src/utils/parseDate';
import { BodyCopy } from '@tcp/core/src/components/common/atoms';
import { ViewWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';
import Barcode from '@tcp/core/src/components/common/molecules/Barcode';
import ResendOrderConfirmation from '@tcp/core/src/components/common/organisms/ResendOrderConfirmation/container/ResendOrderConfirmation.container';
import LineComp from '@tcp/core/src/components/common/atoms/Line';
import {
  ReturnedInfo,
  ReturnedStatus,
  RefundAmount,
} from '../styles/OrderDetailsWrapper.style.native';
import OrderBasicDetails from '../../OrderBasicDetails';
import OrderShippingDetails from '../../OrderShippingDetails';
import OrderBillingDetails from '../../OrderBillingDetails';
import OrderSummaryDetails from '../../OrderSummaryDetails';
import OrderItemsList from '../../OrderItemsList';
import OrderStatus from '../../OrderStatus';
import OrderItemHeader from '../../OrderItemHeader';
import OrderGroupHeader from '../../OrderGroupHeader';
import OrderGroupNotification from '../../OrderGroupNotification';
import constants from '../../../OrderDetails.constants';
import {
  isPartiallyCancelled,
  getShipments,
  getIsNonEcom,
  getIsShowOrderReview,
  getNotificationHeader,
  getNotificationMessage,
  getStatusText,
} from './OrderDetailsWrapper.utils';

const renderOrderStatusHeader = (
  statusText,
  ordersLabels,
  index,
  totalCount,
  returnedOrder,
  isNonEcom,
  ...others
) => {
  const [trackingInfo, returnInitiatedOrder] = others;
  return (
    <OrderItemHeader
      statusText={statusText}
      ordersLabels={ordersLabels}
      index={index}
      totalCount={totalCount}
      returnedOrder={returnedOrder}
      isNonEcom={isNonEcom}
      trackingInfo={trackingInfo}
      returnInitiatedOrder={returnInitiatedOrder}
    />
  );
};

/**
 * This function component use for rendering Bopis and Boss orders
 * can be passed in the component.
 * @param orderDetailsData - orderDetailsData object used for showing Order Details
 */

export const renderBopisAndBossOrder = (
  orderDetailsData,
  ordersLabels,
  navigation,
  showStatusHeader,
  isAppeasementEnabled
) => {
  const {
    pickUpExpirationDate,
    summary,
    orderStatus = '',
    pickedUpDate = '',
    purchasedItems,
    isBossOrder,
    isBopisOrder,
  } = orderDetailsData || {};
  const { currencySymbol } = summary || {};

  return (
    (isBossOrder || isBopisOrder) &&
    purchasedItems &&
    purchasedItems.length > 0 && (
      <>
        {showStatusHeader ? (
          renderOrderStatusHeader(
            orderStatus?.toLowerCase().replace('order', ''),
            ordersLabels,
            1,
            1,
            false,
            true,
            { pickedUpDate }
          )
        ) : (
          <>
            {isBopisOrder ? (
              <OrderStatus
                status={orderStatus}
                pickUpExpirationDate={pickUpExpirationDate}
                pickedUpDate={pickedUpDate}
                isBopisOrder={isBopisOrder}
                ordersLabels={ordersLabels}
              />
            ) : (
              <View />
            )}
            <OrderGroupHeader
              label={getLabelValue(ordersLabels, 'lbl_orders_purchasedItems')}
              message={summary.purchasedItems}
            />
          </>
        )}

        <OrderItemsList
          navigation={navigation}
          ordersLabels={ordersLabels}
          items={purchasedItems[0].items}
          currencySymbol={currencySymbol}
          isShowWriteReview={
            isBossOrder ? true : orderStatus === constants.STATUS_CONSTANTS.ITEMS_PICKED_UP
          }
          isAppeasementEnabled={isAppeasementEnabled}
          isNonEcom
        />
      </>
    )
  );
};

/**
 * This function component use for rendering Cancelled and Out of Stock orders
 * can be passed in the component.
 * @param orderDetailsData - orderDetailsData object used for showing Order Details
 */

export const renderCancelledAndOutOfStockOrders = (
  items,
  ordersLabels,
  currencySymbol,
  headerLabel,
  notificationMessage,
  { isCanceledList = false, showStatusHeader = false, isPartiallyCancelledOrder = false } = {}
) => {
  const statusText = getStatusText(isPartiallyCancelledOrder, ordersLabels);
  return (
    items &&
    items.length > 0 && (
      <>
        {showStatusHeader ? (
          renderOrderStatusHeader(statusText, ordersLabels)
        ) : (
          <>
            <OrderGroupHeader label={headerLabel} message={items.length} />
            <OrderGroupNotification message={notificationMessage} />
          </>
        )}
        <OrderItemsList
          ordersLabels={ordersLabels}
          items={items}
          currencySymbol={currencySymbol}
          isShowWriteReview={false}
          isCanceledList={isCanceledList || false}
        />
      </>
    )
  );
};

export const renderReturnedAndRefundedOrders = (
  items,
  ordersLabels,
  currencySymbol,
  headerLabel,
  notificationMessage,
  showStatusHeader = false,
  navigation
) => {
  const statusText = getLabelValue(ordersLabels, 'lbl_orders_returns_completed');
  return (
    items &&
    items.length > 0 && (
      <>
        {showStatusHeader ? (
          renderOrderStatusHeader(statusText, ordersLabels, null, null, true)
        ) : (
          <>
            <OrderGroupHeader label={headerLabel} message={items.length} />
            <OrderGroupNotification message={notificationMessage} />
          </>
        )}
        {items.map((orderGroup) => {
          const { refundDate, refundAmount } = orderGroup;
          const translatedDate = refundDate && parseDate(refundDate);
          return (
            <>
              <OrderItemsList
                ordersLabels={ordersLabels}
                items={orderGroup.items}
                currencySymbol={currencySymbol}
                isShowWriteReview
                isReturnOrRefund
                navigation={navigation}
              />
              <View>
                <ReturnedInfo>
                  <ReturnedStatus>
                    <View>
                      {refundAmount ? (
                        <BodyCopy
                          fontFamily="secondary"
                          fontSize="fs12"
                          text={getLabelValue(ordersLabels, 'lbl_orders_refund_processed').slice(
                            0,
                            -1
                          )}
                          color="green.500"
                          fontWeight="bold"
                          textDecoration="underline"
                        />
                      ) : (
                        <BodyCopy
                          fontFamily="secondary"
                          fontSize="fs12"
                          text={getLabelValue(ordersLabels, 'lbl_orders_item_returned')}
                          color="green.500"
                          fontWeight="extrabold"
                          textDecoration="underline"
                        />
                      )}
                    </View>
                  </ReturnedStatus>
                  {refundAmount ? (
                    <RefundAmount>
                      <BodyCopy
                        fontFamily="secondary"
                        fontSize="fs12"
                        text={`, ${currencySymbol}${refundAmount}`}
                        color="black"
                        fontWeight="bold"
                      />
                    </RefundAmount>
                  ) : (
                    <View />
                  )}
                  {refundAmount ? (
                    <BodyCopy
                      fontFamily="secondary"
                      fontSize="fs12"
                      text={`${getLabelValue(ordersLabels, 'lbl_orders_on')} ${
                        COMPLETE_MONTH[translatedDate.getMonth()]
                      } ${translatedDate.getDate()}, ${translatedDate.getFullYear()}.`}
                      color="black"
                    />
                  ) : (
                    <BodyCopy
                      fontFamily="secondary"
                      fontSize="fs12"
                      text={`${getLabelValue(ordersLabels, 'lbl_orders_refund_in_progress')}.`}
                      color="black"
                      fontWeight="bold"
                    />
                  )}
                </ReturnedInfo>
              </View>
            </>
          );
        })}
      </>
    )
  );
};

export const renderReturnInitiatedOrders = (
  items,
  ordersLabels,
  currencySymbol,
  headerLabel,
  notificationMessage,
  showStatusHeader = false,
  navigation
) => {
  const statusText = getLabelValue(ordersLabels, 'lbl_orders_returns_initiated');
  return (
    items &&
    items.length > 0 && (
      <>
        {showStatusHeader ? (
          renderOrderStatusHeader(statusText, ordersLabels, null, null, null, null, null, true)
        ) : (
          <>
            <OrderGroupHeader label={headerLabel} message={items.length} />
            <OrderGroupNotification message={notificationMessage} />
          </>
        )}
        {items.map((orderGroup) => {
          const { returnInitiatedDate } = orderGroup;
          const translatedDate = returnInitiatedDate && parseDate(returnInitiatedDate);
          return (
            <>
              <OrderItemsList
                ordersLabels={ordersLabels}
                items={orderGroup.items}
                currencySymbol={currencySymbol}
                isShowWriteReview
                isReturnOrRefund
                navigation={navigation}
              />
              <View>
                <ReturnedInfo>
                  <ReturnedStatus>
                    {returnInitiatedDate ? (
                      <View>
                        {returnInitiatedDate ? (
                          <BodyCopy
                            fontFamily="secondary"
                            fontSize="fs12"
                            text={getLabelValue(ordersLabels, 'lbl_orders_return_initiated')}
                            color="green.500"
                            fontWeight="bold"
                            textDecoration="underline"
                          />
                        ) : (
                          <BodyCopy
                            fontFamily="secondary"
                            fontSize="fs12"
                            text={getLabelValue(ordersLabels, 'lbl_orders_item_returned')}
                            color="black"
                            fontWeight="extrabold"
                          />
                        )}
                      </View>
                    ) : (
                      <View />
                    )}
                  </ReturnedStatus>
                  <BodyCopy
                    fontFamily="secondary"
                    fontSize="fs12"
                    text={`${getLabelValue(ordersLabels, 'lbl_orders_on')} ${
                      COMPLETE_MONTH[translatedDate.getMonth()]
                    } ${translatedDate.getDate()}, ${translatedDate.getFullYear()}.`}
                    color="black"
                  />
                </ReturnedInfo>
              </View>
            </>
          );
        })}
      </>
    )
  );
};

export const renderOrderDetailWithCRMView = (
  orderDetailsData,
  ordersLabels,
  navigation,
  isAppeasementEnabled
) => {
  const {
    summary,
    orderStatus,
    purchasedItems,
    isBopisOrder,
    canceledItems,
    orderNumber,
    outOfStockItems,
    isBossOrder,
    emailAddress,
    returnedItems,
    estimatedDeliveryDate,
    returnedInitiatedItems,
  } = orderDetailsData || {};
  const { currencySymbol } = summary || {};

  const notificationHeader = getNotificationHeader(isBossOrder, ordersLabels);
  const notificationMessage = getNotificationMessage(isBossOrder, ordersLabels);

  const isPartiallyCancelledOrder = isPartiallyCancelled(
    purchasedItems,
    canceledItems,
    returnedItems
  );
  const isNonEcom = getIsNonEcom(isBossOrder, isBopisOrder);
  const shipments = getShipments(purchasedItems, canceledItems);
  const totalCount = shipments.length || 1;

  return (
    <ViewWithSpacing spacingStyles="margin-left-MED margin-right-MED">
      <View>
        <Barcode value={orderNumber} barcodeId={orderNumber} width="2" displayValue />
      </View>
      <ResendOrderConfirmation orderId={orderNumber} trackingEmail={emailAddress} />
      <OrderBasicDetails orderDetailsData={orderDetailsData} ordersLabels={ordersLabels} />
      <OrderShippingDetails orderDetailsData={orderDetailsData} ordersLabels={ordersLabels} />
      <OrderBillingDetails orderDetailsData={orderDetailsData} ordersLabels={ordersLabels} />

      <OrderSummaryDetails
        orderDetailsData={orderDetailsData}
        ordersLabels={ordersLabels}
        isAppeasementEnabled={isAppeasementEnabled}
      />
      {!isNonEcom &&
        shipments &&
        shipments.length > 0 &&
        shipments.map((obj, index) => {
          const { shipment } = obj;
          return (
            <>
              {shipment &&
                shipment.length > 0 &&
                shipment.map((orderGroup) => {
                  let status =
                    (orderGroup && getLabelValue(ordersLabels, orderGroup.shipmentStatus)) || '';
                  const trackingInfo =
                    orderGroup && orderGroup.items[0] && orderGroup.items[0].trackingInfo[0];
                  status = status.toLowerCase().trim();
                  const isShowOrderReview =
                    orderGroup && getIsShowOrderReview(orderGroup.shipmentStatus, orderStatus);
                  return (
                    <>
                      <OrderItemHeader
                        statusText={status}
                        ordersLabels={ordersLabels}
                        index={index + 1}
                        totalCount={totalCount}
                        returnedOrder={false}
                        isNonEcom={isNonEcom}
                        trackingInfo={trackingInfo}
                        estimatedDeliveryDate={estimatedDeliveryDate}
                        actualDeliveryDate={trackingInfo?.actualDeliveryDate}
                        carrierDeliveryDate={trackingInfo.carrierDeliveryDate}
                      />
                      {trackingInfo && trackingInfo.trackingNbr ? (
                        <>
                          <OrderStatus
                            status={trackingInfo.status}
                            trackingNumber={trackingInfo.trackingNbr}
                            trackingUrl={trackingInfo.trackingUrl}
                            shippedDate={trackingInfo.shipDate}
                            ordersLabels={ordersLabels}
                            isBopisOrder={isBopisOrder}
                            showNumAsLink
                          />
                        </>
                      ) : null}
                      <OrderItemsList
                        key={index.toString()}
                        ordersLabels={ordersLabels}
                        items={orderGroup.items}
                        currencySymbol={currencySymbol}
                        navigation={navigation}
                        hideLine
                        isShowWriteReview={isShowOrderReview}
                        isAppeasementEnabled={isAppeasementEnabled}
                      />
                      <LineComp marginTop={32} borderWidth={0.5} borderColor="gray.1600" />
                    </>
                  );
                })}
            </>
          );
        })}
      {renderBopisAndBossOrder(
        orderDetailsData,
        ordersLabels,
        navigation,
        true,
        isAppeasementEnabled
      )}
      {renderCancelledAndOutOfStockOrders(
        outOfStockItems,
        ordersLabels,
        currencySymbol,
        getLabelValue(ordersLabels, 'lbl_orders_outOfStock'),
        getLabelValue(ordersLabels, 'lbl_orders_outOfStockNotification'),
        { isCanceledList: false, showStatusHeader: true, isPartiallyCancelledOrder }
      )}

      {renderCancelledAndOutOfStockOrders(
        canceledItems,
        ordersLabels,
        currencySymbol,
        notificationHeader,
        notificationMessage,
        { isCanceledList: true, showStatusHeader: true, isPartiallyCancelledOrder }
      )}
      {renderReturnedAndRefundedOrders(
        returnedItems,
        ordersLabels,
        currencySymbol,
        getLabelValue(ordersLabels, 'lbl_orders_returns_completed'),
        getLabelValue(ordersLabels, 'lbl_orders_returnedNotification'),
        true,
        navigation
      )}
      {renderReturnInitiatedOrders(
        returnedInitiatedItems,
        ordersLabels,
        currencySymbol,
        getLabelValue(ordersLabels, 'lbl_orders_returns'),
        getLabelValue(ordersLabels, 'lbl_orders_initiatedNotification'),
        true,
        navigation
      )}
    </ViewWithSpacing>
  );
};
