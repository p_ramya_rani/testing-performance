// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { Image, BodyCopy, Row, Col, Button, Anchor } from '@tcp/core/src/components/common/atoms';
import Barcode from '@tcp/core/src/components/common/molecules/Barcode';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { getIconPath } from '@tcp/core/src/utils';
import {
  getLabelValue,
  convertToISOString,
  getFormatedOrderDate,
  isValidOrderForReceipts,
} from '@tcp/core/src/utils/utils';
import PrintComponent from '@tcp/core/src/components/common/atoms/PrintComponent';
import GiftReceipt from '@tcp/core/src/components/features/CnC/common/organism/GiftReceipt';
import { OrderReturnReceiptContainer } from '../../../../Orders/molecules/OrderReturnReceipt/container/OrderReturnReceipt.container';
import styles from '../styles/OrderReceipt.style';
import constants from '../OrderReceipt.constants';

const ShipmentItemThumb = ({ itemBrand, name, imagePath, pdpUrl }) => {
  return (
    <BodyCopy component="div" className="item-thumb-container">
      <Anchor url={pdpUrl} fontSizeVariation="large" anchorVariation="primary">
        <BodyCopy component="div" className="item-thumb">
          <Image src={imagePath} alt={name} />
        </BodyCopy>
      </Anchor>
      <BodyCopy component="div" className="brand-logo">
        {itemBrand === constants.BRAND_TCP && (
          <Image
            alt={itemBrand}
            className="brand-image"
            src={getIconPath(`header__brand-tab--tcp`)}
            data-locator="order_item_brand_logo"
          />
        )}
        {itemBrand === constants.BRAND_GYMBOREE && (
          <Image
            alt={itemBrand}
            className="brand-image"
            src={getIconPath('header__brand-tab-gymboree')}
            data-locator="order_item_brand_logo"
          />
        )}
      </BodyCopy>
    </BodyCopy>
  );
};

ShipmentItemThumb.propTypes = {
  itemBrand: PropTypes.string,
  name: PropTypes.string,
  imagePath: PropTypes.string,
};

ShipmentItemThumb.defaultProps = {
  itemBrand: '',
  name: '',
  imagePath: '',
};

export const ShipmentItem = ({ item, orderDateParsed, ordersLabels, noBorderBottom }) => {
  const {
    itemInfo: { itemBrand, quantity },
    productInfo: { imagePath, name, variantNo, pdpUrl },
  } = item;
  return (
    <BodyCopy
      component="div"
      className={`item-info-container ${noBorderBottom ? 'no-border-bottom' : ''}`}
    >
      <ShipmentItemThumb itemBrand={itemBrand} name={name} imagePath={imagePath} pdpUrl={pdpUrl} />
      <BodyCopy component="div" className="item-details-container">
        <Anchor url={pdpUrl} fontSizeVariation="large" anchorVariation="primary">
          <BodyCopy
            component="p"
            color="gray.900"
            fontFamily="secondary"
            fontSize="fs14"
            fontWeight="extrabold"
            textAlign="center"
            className="item-name"
          >
            {name}
          </BodyCopy>
        </Anchor>
        <BodyCopy component="p" color="gray.900" fontFamily="secondary" fontSize="fs14">
          {`${getLabelValue(ordersLabels, 'lbl_orderDetails_itemnumber')} ${variantNo}`}
        </BodyCopy>
        <BodyCopy component="p" color="gray.900" fontFamily="secondary" fontSize="fs14">
          {`${getLabelValue(ordersLabels, 'lbl_orderDetails_qty')} ${quantity}`}
        </BodyCopy>
        {orderDateParsed && (
          <BodyCopy
            component="p"
            color="gray.900"
            fontFamily="secondary"
            fontSize="fs14"
            className="return-date"
          >
            {`${getLabelValue(ordersLabels, 'lbl_orderDetails_return_by')} ${getFormatedOrderDate(
              orderDateParsed
            )}`}
          </BodyCopy>
        )}
      </BodyCopy>
    </BodyCopy>
  );
};

ShipmentItem.propTypes = {
  item: PropTypes.shape({}),
  orderDateParsed: PropTypes.string,
  ordersLabels: PropTypes.shape({}),
  noBorderBottom: PropTypes.bool,
};

ShipmentItem.defaultProps = {
  item: {},
  orderDateParsed: '',
  ordersLabels: {},
  noBorderBottom: false,
};
const { RETURN_COMPLETED, RETURN_INITIATED } = constants.STATUS_CONSTANTS;

const ShipmentContainer = ({
  index,
  totalCount,
  purchasedItem,
  orderNumber,
  orderDateParsed,
  ordersLabels,
  orderStatus,
  orderDate,
  isBopisOrder,
  isBossOrder,
  orderReturnDays,
  returnStatus,
}) => {
  const { items } = purchasedItem;
  const isPickedUp =
    orderStatus === constants.STATUS_CONSTANTS.ORDER_PICKED_UP || isBopisOrder || isBossOrder;

  return (
    <BodyCopy
      component="div"
      className={`shipment-container ${
        returnStatus && index !== totalCount ? 'no-border-margin' : ''
      }`}
    >
      {!(returnStatus && index !== 1) && (
        <BodyCopy component="div" className="shipment-header">
          <BodyCopy component="span" className="shipment-index" fontFamily="secondary">
            {returnStatus === RETURN_COMPLETED &&
              getLabelValue(ordersLabels, 'lbl_orders_returns_completed')}
            {returnStatus === RETURN_INITIATED &&
              getLabelValue(ordersLabels, 'lbl_orders_returns_initiated')}
            {!returnStatus &&
              (isPickedUp
                ? `${getLabelValue(ordersLabels, 'lbl_orders_statusItemsPickedUp')}`
                : `${getLabelValue(
                    ordersLabels,
                    'lbl_orderDetails_shipment'
                  )} ${index} of ${totalCount}`)}
          </BodyCopy>
          <BodyCopy component="span" className="print-gift-reciept" fontFamily="secondary">
            <Image
              alt="image-placeholder"
              className="print-gift-icon"
              src={getIconPath(`gift_guide_sm`)}
            />
            {isValidOrderForReceipts({ orderNumber, orderStatus }) && (
              <PrintComponent
                ComponentToPrint={GiftReceipt}
                compProps={{
                  orderNumbers: [orderNumber, orderNumber, orderNumber],
                  orderDate,
                }}
                className="print-gift-cta"
                printTriggerText={getLabelValue(
                  ordersLabels,
                  'lbl_orderDetails_print_gift_receipt'
                )}
                displayContentBeforePrint={false}
              />
            )}
          </BodyCopy>
        </BodyCopy>
      )}
      <BodyCopy component="div" className="shipment-body">
        {items.map((item, i) => {
          return (
            // eslint-disable-next-line react/no-array-index-key
            <React.Fragment key={i}>
              <Row fullBleed>
                <Col colSize={{ large: 12, medium: 12, small: 12 }}>
                  <ShipmentItem
                    item={item}
                    orderDateParsed={orderReturnDays ? orderDateParsed : ''}
                    ordersLabels={ordersLabels}
                    isBopisOrder={isBopisOrder}
                    isBossOrder={isBossOrder}
                    noBorderBottom={returnStatus && index !== totalCount}
                  />
                </Col>
              </Row>
            </React.Fragment>
          );
        })}
        {!(returnStatus && index !== totalCount) && (
          <Row fullBleed>
            <Col colSize={{ large: 12, medium: 12, small: 12 }}>
              <BodyCopy component="div" aria-hidden className="shipment-barcode-container">
                <BodyCopy
                  fontFamily="secondary"
                  fontSize="fs14"
                  component="p"
                  fontWeight="bold"
                  textAlign="center"
                >
                  {`${getLabelValue(ordersLabels, 'lbl_orderDetails_show_barcode')}`}
                </BodyCopy>
                <Barcode
                  value={orderNumber}
                  barcodeId={orderNumber}
                  height={37}
                  renderer="svg"
                  width="3.7"
                />
              </BodyCopy>
            </Col>
          </Row>
        )}
      </BodyCopy>
    </BodyCopy>
  );
};

ShipmentContainer.propTypes = {
  index: PropTypes.number,
  totalCount: PropTypes.number,
  purchasedItem: PropTypes.shape({}),
  orderNumber: PropTypes.string,
  orderDateParsed: PropTypes.instanceOf(Date),
  ordersLabels: PropTypes.shape({}),
  orderStatus: PropTypes.string,
  orderDate: PropTypes.string,
  isBopisOrder: PropTypes.bool,
  orderReturnDays: PropTypes.number,
  isBossOrder: PropTypes.bool,
  returnStatus: PropTypes.string,
};

ShipmentContainer.defaultProps = {
  index: 1,
  totalCount: 1,
  purchasedItem: {},
  orderNumber: '0',
  orderDateParsed: new Date(),
  ordersLabels: {},
  orderStatus: '',
  orderDate: '',
  orderReturnDays: 0,
  isBopisOrder: false,
  isBossOrder: false,
  returnStatus: '',
};

const checkForEligibleOrder = (orderStatus, trackingNumber, isBossOrder, isBopisOrder) => {
  return (
    (orderStatus === constants.STATUS_CONSTANTS.ORDER_SHIPPED ||
      orderStatus === constants.STATUS_CONSTANTS.ORDER_PARTIALLY_SHIPPED ||
      orderStatus === constants.STATUS_CONSTANTS.ORDER_PICKED_UP ||
      isBopisOrder) &&
    (orderStatus === constants.STATUS_CONSTANTS.ORDER_PICKED_UP ||
      trackingNumber ||
      isBossOrder ||
      isBopisOrder)
  );
};

const OrderReciept = ({
  className,
  orderDetailsData,
  orderReturnDays,
  ordersLabels,
  userAccountId,
  isLoggedIn,
  toggleReceiptsView,
}) => {
  const {
    purchasedItems = [],
    purchasedItemsAll = [],
    orderNumber,
    orderDate,
    isBopisOrder,
    isBossOrder,
    orderStatus: status,
    returnedInitiatedItems,
    returnedItems,
  } = orderDetailsData;
  const orderPlacedDate = new Date(convertToISOString(orderDate));
  const orderDateParsed = new Date(orderPlacedDate);
  orderDateParsed.setDate(orderDateParsed.getDate() + orderReturnDays);

  const allItems =
    purchasedItemsAll && purchasedItemsAll.length ? purchasedItemsAll : purchasedItems;

  const purchasedItemsEligible = allItems.filter((purchasedItem) => {
    const { trackingNumber, orderStatus } = purchasedItem;
    return checkForEligibleOrder(orderStatus, trackingNumber, isBossOrder, isBopisOrder);
  });

  if (
    (purchasedItemsEligible.length <= 0 ||
      status.toLowerCase() === constants.STATUS_CONSTANTS.ITEMS_READY_FOR_PICKUP ||
      status.toLowerCase() === constants.STATUS_CONSTANTS.ORDER_RECEIVED_LABEL.toLowerCase()) &&
    returnedInitiatedItems?.length <= 0 &&
    returnedItems?.length <= 0
  ) {
    return (
      <BodyCopy component="div" className={className}>
        <Row fullBleed>
          <Col colSize={{ large: 9, medium: 12, small: 12 }} className="no-receipts-container">
            <BodyCopy
              fontFamily="secondary"
              fontSize="fs26"
              component="h3"
              fontWeight="bold"
              textAlign="center"
              className="no-receipts-heading"
            >
              {`${getLabelValue(ordersLabels, 'lbl_orderDetails_no_receipts_yet')}`}
            </BodyCopy>
            <BodyCopy
              fontFamily="secondary"
              fontSize="fs16"
              component="p"
              fontWeight="normal"
              textAlign="center"
            >
              {`${getLabelValue(ordersLabels, 'lbl_orderDetails_provided_once_order_ships')}`}
            </BodyCopy>
            <Button
              buttonVariation="fixed-width"
              fill="WHITE"
              type="submit"
              fontSize="fs14"
              className="back-to-order-cta"
              fontWeight="semibold"
              onClick={() => toggleReceiptsView(false)}
            >
              {`${getLabelValue(ordersLabels, 'lbl_orderDetails_back_to_order_details')}`}
            </Button>
          </Col>
        </Row>
      </BodyCopy>
    );
  }

  const renderTriggerButton = () => (
    <Button
      buttonVariation="fixed-width"
      fill="WHITE"
      type="submit"
      fontSize="fs14"
      className="elem-mt-MED print-receipt-button"
      fontWeight="semibold"
    >
      {getLabelValue(ordersLabels, 'lbl_orderDetails_print_receipts')}
    </Button>
  );

  return (
    <BodyCopy component="div" className={className}>
      <Row fullBleed>
        <Col colSize={{ large: 9, medium: 12, small: 12 }}>
          <BodyCopy fontFamily="secondary" fontSize="fs16" component="h3" fontWeight="semibold">
            {`${getLabelValue(ordersLabels, 'lbl_orderDetails_shipped_multiple_packages')}`}
          </BodyCopy>
        </Col>
        <Col colSize={{ large: 3, medium: 12, small: 12 }}>
          <PrintComponent
            ComponentToPrint={OrderReturnReceiptContainer}
            compProps={{
              orderDetailsData,
              ordersLabels,
              orderReturnDays,
              userAccountId,
              isLoggedIn,
            }}
            printTriggerText={getLabelValue(ordersLabels, 'lbl_orderDetails_print_receipts')}
            displayContentBeforePrint={false}
            className={className}
            printTextClassName="elem-mt-MED print-receipt-button"
            renderTriggerButton={renderTriggerButton}
          />
        </Col>
      </Row>
      <Row fullBleed>
        <Col colSize={{ large: 12, medium: 12, small: 12 }}>
          <BodyCopy fontFamily="secondary" fontSize="fs16" component="h3" fontWeight="semibold">
            {`${getLabelValue(ordersLabels, 'lbl_orderDetails_receipts_shown_below')}`}
          </BodyCopy>
        </Col>
      </Row>

      {!orderReturnDays && (
        <Row fullBleed>
          <Col colSize={{ large: 12, medium: 12, small: 12 }}>
            <div className="order-return-message">
              <BodyCopy fontFamily="secondary" fontSize="fs16" component="h3" fontWeight="semibold">
                {`${getLabelValue(ordersLabels, 'lbl_order_receipt_message_heading')}`}
              </BodyCopy>
              <BodyCopy fontFamily="secondary" fontSize="fs15">
                {`${getLabelValue(ordersLabels, 'lbl_order_receipt_message_information')}`}
              </BodyCopy>
            </div>
          </Col>
        </Row>
      )}

      {purchasedItemsEligible &&
        purchasedItemsEligible.length > 0 &&
        purchasedItemsEligible.map((purchasedItem, i) => {
          const { orderStatus } = purchasedItem;
          return (
            // eslint-disable-next-line react/no-array-index-key
            <Row key={i} fullBleed>
              <Col colSize={{ large: 12, medium: 12, small: 12 }}>
                <ShipmentContainer
                  index={i + 1}
                  totalCount={purchasedItemsEligible.length}
                  purchasedItem={purchasedItem}
                  orderNumber={orderNumber}
                  orderDateParsed={orderDateParsed}
                  ordersLabels={ordersLabels}
                  orderStatus={orderStatus}
                  orderDate={orderDate}
                  isBopisOrder={isBopisOrder}
                  isBossOrder={isBossOrder}
                  orderReturnDays={orderReturnDays}
                />
              </Col>
            </Row>
          );
        })}
      {returnedInitiatedItems &&
        returnedInitiatedItems.length > 0 &&
        returnedInitiatedItems.map((purchasedItem, i) => {
          const { orderStatus } = purchasedItem;
          return (
            // eslint-disable-next-line react/no-array-index-key
            <Row key={i} fullBleed>
              <Col colSize={{ large: 12, medium: 12, small: 12 }}>
                <ShipmentContainer
                  index={i + 1}
                  purchasedItem={purchasedItem}
                  orderNumber={orderNumber}
                  orderDateParsed={orderDateParsed}
                  ordersLabels={ordersLabels}
                  orderStatus={orderStatus}
                  orderDate={orderDate}
                  isBopisOrder={isBopisOrder}
                  isBossOrder={isBossOrder}
                  orderReturnDays={orderReturnDays}
                  returnStatus={RETURN_INITIATED}
                  totalCount={returnedInitiatedItems.length}
                />
              </Col>
            </Row>
          );
        })}
      {returnedItems &&
        returnedItems.length > 0 &&
        returnedItems.map((purchasedItem, i) => {
          const { orderStatus } = purchasedItem;
          return (
            // eslint-disable-next-line react/no-array-index-key
            <Row key={i} fullBleed>
              <Col colSize={{ large: 12, medium: 12, small: 12 }}>
                <ShipmentContainer
                  index={i + 1}
                  purchasedItem={purchasedItem}
                  orderNumber={orderNumber}
                  orderDateParsed={orderDateParsed}
                  ordersLabels={ordersLabels}
                  orderStatus={orderStatus}
                  orderDate={orderDate}
                  isBopisOrder={isBopisOrder}
                  isBossOrder={isBossOrder}
                  orderReturnDays={orderReturnDays}
                  returnStatus={RETURN_COMPLETED}
                  totalCount={returnedItems.length}
                />
              </Col>
            </Row>
          );
        })}
    </BodyCopy>
  );
};
OrderReciept.propTypes = {
  className: PropTypes.string,
  orderDetailsData: PropTypes.shape({}),
  orderReturnDays: PropTypes.number,
  ordersLabels: PropTypes.shape({}),
  userAccountId: PropTypes.string,
  isLoggedIn: PropTypes.bool,
  toggleReceiptsView: PropTypes.func,
};

OrderReciept.defaultProps = {
  className: '',
  orderDetailsData: {},
  orderReturnDays: 0,
  ordersLabels: {},
  userAccountId: '',
  isLoggedIn: false,
  toggleReceiptsView: () => {},
};

export default withStyles(OrderReciept, styles);
export { OrderReciept as OrderRecieptVanilla };
