// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';

const ImageWrapper = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.XXXS};
`;

const ImageStyle = styled.Image`
  width: 47px;
  height: 30px;
  /* stylelint-disable-next-line */
  resize-mode: contain;
`;

const LogoContainer = styled.View`
  border: 1px solid ${(props) => props.theme.colorPalette.gray[500]};
  border-radius: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
  ${(props) =>
    props.afterPayPayment
      ? `background-color: ${props.theme.colors.AFTERPAY.MINT};padding: 0 4px;`
      : ''}
`;

export { ImageWrapper, ImageStyle, LogoContainer };
