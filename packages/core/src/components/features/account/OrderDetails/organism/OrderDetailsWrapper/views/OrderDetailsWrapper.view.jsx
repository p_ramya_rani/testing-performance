/* eslint-disable max-lines */
/* eslint-disable max-params */
import React from 'react';
import { Row, Col } from '@tcp/core/src/components/common/atoms';
import config from '@tcp/core/src/config/orderConfig';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import { parseDate, COMPLETE_MONTH } from '@tcp/core/src/utils/parseDate';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import HelpWithOrder from '@tcp/core/src/components/common/organisms/HelpWithOrder/container/HelpWithOrder.container';
import Barcode from '@tcp/core/src/components/common/molecules/Barcode';
import { getViewportInfo } from '@tcp/core/src/utils';
import {
  isPartiallyCancelled,
  getShipments,
  getIsNonEcom,
  getIsShowOrderReview,
  getNotificationHeader,
  getNotificationMessage,
  getStatusText,
} from './OrderDetailsWrapper.utils';
import OrderItem from '../../OrderItem';
import OrderBasicDetails from '../../OrderBasicDetails';
import OrderShippingDetails from '../../OrderShippingDetails';
import OrderBillingDetails from '../../OrderBillingDetails';
import OrderSummaryDetails from '../../OrderSummaryDetails';
import OrderGroupHeader from '../../OrderGroupHeader';
import OrderItemsList from '../../OrderItemsList';
import OrderStatus from '../../OrderStatus';
import OrderItemHeader from '../../OrderItemHeader';
import OrderGroupNotification from '../../OrderGroupNotification';
import constants from '../../../OrderDetails.constants';

const renderOrderStatusHeader = (
  status,
  ordersLabels,
  index,
  totalCount,
  returnedOrder,
  orderGroup,
  extObj,
  returnInitiatedOrder
) => {
  return (
    <OrderItemHeader
      status={status}
      ordersLabels={ordersLabels}
      index={index}
      totalCount={totalCount}
      returnedOrder={returnedOrder}
      orderGroup={orderGroup}
      extObj={extObj}
      returnInitiatedOrder={returnInitiatedOrder}
    />
  );
};

const renderBopisAndBossStatusHeader = (
  ordersLabels,
  summary,
  isBopisOrder,
  orderStatus,
  pickedUpDate,
  showStatusWithLogo
) => {
  const status = orderStatus && orderStatus.toLowerCase();

  if (showStatusWithLogo) {
    return (
      <Col colSize={{ large: 12, medium: 12, small: 12 }}>
        {renderOrderStatusHeader(
          status.replace('order', ''),
          ordersLabels,
          1,
          1,
          false,
          {},
          {
            isBopisOrder,
            isNonEcom: true,
            pickedUpDate,
          }
        )}
      </Col>
    );
  }

  return (
    <>
      {isBopisOrder && (
        <OrderStatus
          status={orderStatus}
          pickedUpDate={pickedUpDate}
          isBopisOrder={isBopisOrder}
          ordersLabels={ordersLabels}
        />
      )}
      <OrderGroupHeader
        label={getLabelValue(ordersLabels, 'lbl_orders_purchasedItems')}
        message={summary.purchasedItems}
      />
    </>
  );
};

/**
 * This function component use for rendering Bopis and Boss orders
 * can be passed in the component.
 * @param orderDetailsData - orderDetailsData object used for showing Order Details
 */
export const renderBopisAndBossOrder = (
  orderDetailsData,
  ordersLabels,
  showStatusWithLogo,
  isAppeasementEnabled
) => {
  const { summary, orderStatus, pickedUpDate, purchasedItems, isBossOrder, isBopisOrder } =
    orderDetailsData || {};

  const { currencySymbol } = summary || {};
  return (
    (isBossOrder || isBopisOrder) &&
    purchasedItems &&
    purchasedItems.length > 0 && (
      <Row fullBleed className="group-row">
        {renderBopisAndBossStatusHeader(
          ordersLabels,
          summary,
          isBopisOrder,
          orderStatus,
          pickedUpDate,
          showStatusWithLogo
        )}
        <Col colSize={{ large: 12, medium: 8, small: 6 }}>
          <OrderItemsList
            ordersLabels={ordersLabels}
            items={purchasedItems[0].items}
            currencySymbol={currencySymbol}
            isShowWriteReview={
              isBossOrder ? true : orderStatus === constants.STATUS_CONSTANTS.ITEMS_PICKED_UP
            }
            isAppeasementEnabled={isAppeasementEnabled}
            isNonEcom
          />
        </Col>
      </Row>
    )
  );
};

/**
 * This function component use for rendering Cancelled and Out of Stock orders
 * can be passed in the component.
 * @param orderDetailsData - orderDetailsData object used for showing Order Details
 */

export const renderCancelledAndOutOfStockOrders = (
  items,
  ordersLabels,
  currencySymbol,
  headerLabel,
  notificationMessage,
  { isCanceledList = false, showStatusHeader = false, isPartiallyCancelledOrder = false } = {}
) => {
  const statusText = getStatusText(isPartiallyCancelledOrder, ordersLabels);
  return (
    items &&
    items.length > 0 && (
      <Row fullBleed className="group-row">
        {!showStatusHeader && (
          <>
            <OrderGroupHeader label={headerLabel} message={items.length} />
            <OrderGroupNotification message={notificationMessage} />
          </>
        )}
        {showStatusHeader && renderOrderStatusHeader(statusText, ordersLabels)}
        <Col colSize={{ large: 12, medium: 8, small: 6 }}>
          <OrderItemsList
            ordersLabels={ordersLabels}
            items={items}
            currencySymbol={currencySymbol}
            isShowWriteReview={false}
            isCanceledList={isCanceledList || false}
          />
        </Col>
      </Row>
    )
  );
};

const renderOrderGroup = (showStatusHeader, headerLabel, items, notificationMessage) =>
  !showStatusHeader && (
    <>
      <OrderGroupHeader label={headerLabel} message={items.length} />
      <OrderGroupNotification message={notificationMessage} />
    </>
  );

export const renderReturnedAndRefundedOrders = (
  items,
  ordersLabels,
  currencySymbol,
  headerLabel,
  notificationMessage,
  showStatusHeader = false
) => {
  const statusText = getLabelValue(ordersLabels, 'lbl_orders_returns_completed');
  return (
    items &&
    items.length > 0 && (
      <Row fullBleed className="group-row">
        {renderOrderGroup(showStatusHeader, headerLabel, items, notificationMessage)}
        {showStatusHeader && renderOrderStatusHeader(statusText, ordersLabels, null, null, true)}
        <Col colSize={{ large: 12, medium: 8, small: 6 }}>
          <Row fullBleed>
            {items.map((orderGroup, i) => {
              const { refundDate, refundAmount, items: orderItems = [] } = orderGroup;
              const translatedDate = refundDate && parseDate(refundDate);
              return (
                <Col
                  colSize={{ large: 6, medium: 6, small: 6 }}
                  ignoreGutter={(i + 1) % 2 === 0 ? { large: true, medium: true } : {}}
                  className="elem-pt-XL"
                >
                  <div className="returned-items">
                    {orderItems &&
                      orderItems.length > 0 &&
                      orderItems.map((item, index) => (
                        <OrderItem
                          key={index.toString()}
                          {...{ item }}
                          ordersLabels={ordersLabels}
                          currencySymbol={currencySymbol}
                          isShowWriteReview
                          isReturnOrRefund
                        />
                      ))}
                  </div>
                  <div className="returned-info elem-mt-LRG_1">
                    <span className={`returned-status ${refundAmount ? 'refunded' : ''}`}>
                      {refundAmount
                        ? getLabelValue(ordersLabels, 'lbl_orders_refund_processed')
                        : getLabelValue(ordersLabels, 'lbl_orders_item_returned')}
                    </span>
                    {refundAmount ? (
                      <span className="refund-amount">{` ${currencySymbol}${refundAmount}`}</span>
                    ) : null}
                    <span className="refund-status">
                      {refundAmount
                        ? `${getLabelValue(ordersLabels, 'lbl_orders_on')} ${
                            COMPLETE_MONTH[translatedDate.getMonth()]
                          } ${translatedDate.getDate()}, ${translatedDate.getFullYear()}.`
                        : `${getLabelValue(ordersLabels, 'lbl_orders_refund_in_progress')}.`}
                    </span>
                  </div>
                </Col>
              );
            })}
          </Row>
        </Col>
      </Row>
    )
  );
};

export const renderReturnInitiatedOrders = (
  items,
  ordersLabels,
  currencySymbol,
  headerLabel,
  notificationMessage,
  showStatusHeader = false
) => {
  const statusText = getLabelValue(ordersLabels, 'lbl_orders_returns_initiated');
  return (
    items &&
    items.length > 0 && (
      <Row fullBleed className="group-row">
        {renderOrderGroup(showStatusHeader, headerLabel, items, notificationMessage)}
        {showStatusHeader &&
          renderOrderStatusHeader(statusText, ordersLabels, null, null, null, null, null, true)}
        <Col colSize={{ large: 12, medium: 8, small: 6 }}>
          <Row fullBleed>
            {items.map((orderGroup, i) => {
              const { items: orderItems = [], returnInitiatedDate } = orderGroup;
              const translatedDate = returnInitiatedDate && parseDate(returnInitiatedDate);
              return (
                <Col
                  colSize={{ large: 6, medium: 6, small: 6 }}
                  ignoreGutter={(i + 1) % 2 === 0 ? { large: true, medium: true } : {}}
                  className="elem-pt-XL"
                >
                  <div className="returned-items">
                    {orderItems &&
                      orderItems.length > 0 &&
                      orderItems.map((item, index) => (
                        <OrderItem
                          {...{ item }}
                          key={index.toString()}
                          ordersLabels={ordersLabels}
                          currencySymbol={currencySymbol}
                          isShowWriteReview
                          isReturnOrRefund
                        />
                      ))}
                  </div>
                  <div className="returned-info elem-mt-LRG_1">
                    <span className="returned-status refunded">
                      {getLabelValue(ordersLabels, 'lbl_orders_return_initiated')}
                    </span>
                    <span className="refund-status">
                      {returnInitiatedDate &&
                        ` ${getLabelValue(ordersLabels, 'lbl_orders_on')} ${
                          COMPLETE_MONTH[translatedDate.getMonth()]
                        } ${translatedDate.getDate()}, ${translatedDate.getFullYear()}.`}
                    </span>
                  </div>
                </Col>
              );
            })}
          </Row>
        </Col>
      </Row>
    )
  );
};

const displayEcomOrder = (orderType, shipments) => {
  return orderType === config.ORDER_ITEM_TYPE.ECOM && shipments && shipments.length > 0;
};

/**
 * This function component use for rendering Cancelled and Out of Stock orders
 * can be passed in the component.
 * @param orderDetailsData - orderDetailsData object used for showing Order Details
 * @param ordersLabels - ordersLabels object used for labels
 */
export const renderOrderDetailWithCRMView = (
  orderDetailsData,
  ordersLabels,
  toggleReceiptsView,
  isAppeasementEnabled
) => {
  const {
    summary,
    orderStatus,
    purchasedItems,
    outOfStockItems,
    isBossOrder,
    isBopisOrder,
    orderNumber,
    canceledItems,
    emailAddress,
    encryptedEmailAddress,
    returnedItems,
    returnedInitiatedItems,
    orderType,
    estimatedDeliveryDate,
  } = orderDetailsData || {};

  const { currencySymbol } = summary || {};
  const notificationHeader = getNotificationHeader(isBossOrder, ordersLabels);
  const notificationMessage = getNotificationMessage(isBossOrder, ordersLabels);
  const { isDesktop } = getViewportInfo();

  const shipments = getShipments(purchasedItems, canceledItems);
  const totalCount = shipments.length || 1;
  const isPartiallyCancelledOrder = isPartiallyCancelled(
    purchasedItems,
    canceledItems,
    returnedItems
  );
  const isNonEcom = getIsNonEcom(isBossOrder, isBopisOrder);
  return (
    orderDetailsData && (
      <>
        <Row fullBleed className="elem-mt-XL">
          <Col colSize={{ large: 6, medium: 4, small: 6 }}>
            <Row fullBleed>
              <Col colSize={{ large: 6, medium: 8, small: 6 }}>
                {!isDesktop && (
                  <BodyCopy component="div" className="orderdetails-barcode" textAlign="center">
                    <Barcode
                      className="barcode"
                      value={orderNumber}
                      barcodeId={orderNumber}
                      height={75}
                      displayValue
                    />
                  </BodyCopy>
                )}
              </Col>
            </Row>
            <Row fullBleed>
              <Col colSize={{ large: 6, medium: 8, small: 6 }}>
                <OrderBasicDetails
                  orderDetailsData={orderDetailsData}
                  ordersLabels={ordersLabels}
                />
              </Col>
              <Col colSize={{ large: 6, medium: 8, small: 6 }} className="margin-tablet">
                <OrderShippingDetails
                  orderDetailsData={orderDetailsData}
                  ordersLabels={ordersLabels}
                />
              </Col>
            </Row>
          </Col>
          <Col colSize={{ large: 6, medium: 4, small: 6 }}>
            <Row fullBleed>
              <Col colSize={{ large: 5, medium: 8, small: 6 }} className="margin-mobile">
                <OrderBillingDetails
                  orderDetailsData={orderDetailsData}
                  ordersLabels={ordersLabels}
                />
              </Col>
              <Col colSize={{ large: 7, medium: 8, small: 6 }} className="margin-tablet">
                <OrderSummaryDetails
                  orderDetailsData={orderDetailsData}
                  ordersLabels={ordersLabels}
                  isAppeasementEnabled={isAppeasementEnabled}
                />
                <HelpWithOrder
                  orderId={orderNumber}
                  trackingEmail={emailAddress}
                  encryptedEmailAddress={encryptedEmailAddress}
                  toggleReceiptsView={toggleReceiptsView}
                  orderDetailsData={orderDetailsData}
                />
              </Col>
            </Row>
          </Col>
        </Row>
        <div className="elem-mt-XL">
          {displayEcomOrder(orderType, shipments) &&
            shipments.map((obj, index) => {
              const { shipment } = obj;
              return (
                <>
                  {shipment &&
                    shipment.length > 0 &&
                    shipment.map((orderGroup) => {
                      const status = getLabelValue(ordersLabels, orderGroup.shipmentStatus) || '';
                      const trackingInfo =
                        orderGroup && orderGroup.items[0] && orderGroup.items[0].trackingInfo[0];
                      const isShowOrderReview =
                        orderGroup && getIsShowOrderReview(orderGroup.shipmentStatus, orderStatus);
                      return (
                        <Row fullBleed className="group-row">
                          <Col colSize={{ large: 12, medium: 12, small: 12 }}>
                            <OrderItemHeader
                              status={status}
                              ordersLabels={ordersLabels}
                              index={index + 1}
                              totalCount={totalCount}
                              returnedOrder={false}
                              orderGroup={trackingInfo}
                              extObj={{
                                isBopisOrder,
                                isNonEcom,
                              }}
                              estimatedDeliveryDate={estimatedDeliveryDate}
                              actualDeliveryDate={trackingInfo.actualDeliveryDate}
                              carrierDeliveryDate={trackingInfo.carrierDeliveryDate}
                            />
                          </Col>
                          <Col colSize={{ large: 12, medium: 8, small: 6 }}>
                            {trackingInfo && (
                              <div className="order-status-cta hide-on-desktop hide-on-tablet">
                                <OrderStatus
                                  status={trackingInfo.status}
                                  trackingNumber={trackingInfo.trackingNbr}
                                  trackingUrl={trackingInfo.trackingUrl}
                                  shippedDate={trackingInfo.shipDate}
                                  ordersLabels={ordersLabels}
                                  isBopisOrder={isBopisOrder}
                                  showNumAsLink
                                />
                              </div>
                            )}
                            <OrderItemsList
                              key={index.toString()}
                              ordersLabels={ordersLabels}
                              items={orderGroup.items}
                              currencySymbol={currencySymbol}
                              isEcom
                              isShowWriteReview={isShowOrderReview}
                              isAppeasementEnabled={isAppeasementEnabled}
                            />
                          </Col>
                        </Row>
                      );
                    })}
                </>
              );
            })}
          {renderBopisAndBossOrder(orderDetailsData, ordersLabels, true, isAppeasementEnabled)}
          {renderCancelledAndOutOfStockOrders(
            outOfStockItems,
            ordersLabels,
            currencySymbol,
            getLabelValue(ordersLabels, 'lbl_orders_outOfStock'),
            getLabelValue(ordersLabels, 'lbl_orders_outOfStockNotification'),
            { isCanceledList: false, showStatusHeader: true, isPartiallyCancelledOrder }
          )}
          {renderCancelledAndOutOfStockOrders(
            canceledItems,
            ordersLabels,
            currencySymbol,
            notificationHeader,
            notificationMessage,
            { isCanceledList: true, showStatusHeader: true, isPartiallyCancelledOrder }
          )}
          {renderReturnedAndRefundedOrders(
            returnedItems,
            ordersLabels,
            currencySymbol,
            getLabelValue(ordersLabels, 'lbl_orders_returns_completed'),
            getLabelValue(ordersLabels, 'lbl_orders_returnedNotification'),
            true
          )}
          {renderReturnInitiatedOrders(
            returnedInitiatedItems,
            ordersLabels,
            currencySymbol,
            getLabelValue(ordersLabels, 'lbl_orders_returns_initiated'),
            getLabelValue(ordersLabels, 'lbl_orders_initiatedNotification'),
            true
          )}
        </div>
      </>
    )
  );
};
