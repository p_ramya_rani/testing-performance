import { getIconPath, getLabelValue, convertToISOString } from '@tcp/core/src/utils/utils';
import {
  filterMapping,
  getItemsWithTargetStatus,
  getTranslatedDate,
} from '@tcp/core/src/components/features/account/Orders/molecules/OrderListWithDetails/views/OrderListWithDetails.utils';
import constants from '@tcp/core/src/components/features/account/Orders/Orders.constants';

const { STATUS_CONSTANTS = {} } = constants;
const {
  CANCELLED,
  ORDER_CANCELED,
  DELIVERED,
  PARTIALLY_CANCELED,
  PARTIALLY_CANCELLED,
  SHIPPED,
  ITEMS_PICKED_UP,
  ITEM_READY_FOR_PICK_UP,
  PREPARING_YOUR_ORDER,
  ORDER_PREPARING,
  ORDER_PARTIALLY_SHIPPED,
  ORDER_SHIPPED,
  ORDER_IN_PROCESS,
  PROCESSING,
  IN_TRANSIT,
  OUT_FOR_DELIVERY,
  RETURNS,
  RETURNS_COMPLETED,
  RETURNS_INITIATED,
  NA,
  IN_PROCESS,
  ITEMS_RECEIVED,
  ITEMS_READY_FOR_PICKUP,
  SUCCESSFULLY_PICKED_UP,
  ORDER_CANCELLED,
  ORDER_PICKED_UP,
  RETURN_INITIATED,
  RETURN_COMPLETED,
  ORDER_BEING_PROCESSED,
  ORDER_PROCESSING,
  PARTIALLY_SHIPPED,
} = STATUS_CONSTANTS;

export const getLogoIconPath = (statusText = '') => {
  const cancelled = 'status-shipping-cancelled';
  const processing = 'processing-preparing';
  const delivered = 'order-delivered-filled';
  const returned = 'order-returned-filled';
  const iconsMap = {
    [DELIVERED]: delivered,
    [ITEMS_PICKED_UP]: delivered,
    [SUCCESSFULLY_PICKED_UP]: delivered,
    [CANCELLED]: cancelled,
    [ORDER_CANCELED]: cancelled,
    [ORDER_CANCELLED]: cancelled,
    [PARTIALLY_CANCELED]: cancelled,
    [PARTIALLY_CANCELLED]: cancelled,
    [RETURNS]: returned,
    [RETURNS_COMPLETED]: returned,
    [RETURNS_INITIATED]: returned,
    [IN_TRANSIT]: 'in-transit',
    [OUT_FOR_DELIVERY]: 'out-for-delivery',
    [SHIPPED]: 'shipped',
    [ITEM_READY_FOR_PICK_UP]: 'ready-for-pickup',
    [ITEMS_READY_FOR_PICKUP]: 'ready-for-pickup',
    [PROCESSING]: processing,
    [ORDER_BEING_PROCESSED]: processing,
    [ORDER_PROCESSING]: processing,
    [PREPARING_YOUR_ORDER]: processing,
    [ORDER_PREPARING]: processing,
    [IN_PROCESS]: processing,
    [NA]: cancelled,
    [ITEMS_RECEIVED]: processing,
    [RETURN_INITIATED]: 'return-initiated',
    [RETURN_COMPLETED]: 'return-initiated',
  };
  const statusTextToMatch = statusText.toLowerCase().trim();
  const iconPath = iconsMap[statusTextToMatch] || '';
  return getIconPath(iconPath);
};

export const getShipmentIcon = (
  statusText,
  {
    pickedUpIcon,
    shippingCancelled,
    inTransit,
    outForDelivery,
    partiallyReturned,
    processingPreparing,
    readyForPickup,
    shipped,
    returns,
  }
) => {
  const iconsMap = {
    [DELIVERED]: pickedUpIcon,
    [ITEMS_PICKED_UP]: pickedUpIcon,
    [CANCELLED]: shippingCancelled,
    [ORDER_CANCELED]: shippingCancelled,
    [CANCELLED]: shippingCancelled,
    [ORDER_CANCELED]: shippingCancelled,
    [ORDER_CANCELLED]: shippingCancelled,
    [PARTIALLY_CANCELED]: partiallyReturned,
    [PARTIALLY_CANCELLED]: partiallyReturned,
    [RETURNS]: returns,
    [IN_TRANSIT]: inTransit,
    [OUT_FOR_DELIVERY]: outForDelivery,
    [SHIPPED]: shipped,
    [ITEM_READY_FOR_PICK_UP]: readyForPickup,
    [ITEMS_READY_FOR_PICKUP]: readyForPickup,
    [PROCESSING]: processingPreparing,
    [PREPARING_YOUR_ORDER]: processingPreparing,
    [ORDER_PREPARING]: processingPreparing,
    [IN_PROCESS]: processingPreparing,
    [ITEMS_RECEIVED]: processingPreparing,
    [RETURNS_INITIATED]: returns,
    [RETURNS_COMPLETED]: returns,
    [RETURN_COMPLETED]: returns,
    [RETURN_INITIATED]: returns,
  };
  return iconsMap[statusText] || '';
};

export const getStatusTextClass = (statusText = '') => {
  let statusTextClass = 'default';
  const statusTextToMatch = statusText.toLowerCase().trim();
  if (
    statusTextToMatch === DELIVERED ||
    statusTextToMatch === ITEMS_PICKED_UP ||
    statusTextToMatch === ORDER_PICKED_UP
  ) {
    statusTextClass = 'success-status';
  }

  if (
    statusTextToMatch === CANCELLED ||
    statusTextToMatch === ORDER_CANCELED ||
    statusTextToMatch === ORDER_CANCELLED ||
    statusTextToMatch === NA
  ) {
    statusTextClass = 'cancelled-status';
  }

  if (statusTextToMatch === RETURN_COMPLETED || statusTextToMatch === RETURN_INITIATED) {
    statusTextClass = 'return-status';
  }
  return statusTextClass;
};

export const getStatusLogoClass = (statusText = '') => {
  let statusLogoClass = '';
  const statusTextToMatch = statusText.toLowerCase().trim();
  if (statusTextToMatch === RETURN_COMPLETED || statusTextToMatch === RETURN_INITIATED) {
    statusLogoClass = 'return-status-logo';
  }
  return statusLogoClass;
};

export const isPartiallyCancelled = (
  purchaseditems = [],
  canceledItems = [],
  returnedItems = []
) => {
  return (
    canceledItems &&
    canceledItems.length &&
    ((purchaseditems && purchaseditems[0] && purchaseditems[0].items.length) ||
      (returnedItems && returnedItems.length))
  );
};

export const getCancelledItemsLength = (canceledItems) => {
  return (
    canceledItems && canceledItems[0] && canceledItems[0].items && canceledItems[0].items.length
  );
};

/**
 * @description - This method creates seperate shipments based on trackingNumber
 */
const processMultiLineShipments = (shipments = [], shippedShipment = [], targetStatus = '') => {
  const { statusToMatch = [] } = filterMapping[targetStatus] || {};
  const uniqueShipments = {};
  const trackingNbrs = [];
  shippedShipment.forEach((shipment) => {
    const { items = [] } = shipment || {};
    items.forEach((item) => {
      const { trackingInfo = [] } = item || {};
      trackingInfo.forEach((info) => {
        const { trackingNbr = null, status = '', quantity = 0 } = info || {};
        const statusText = status && status.toLowerCase().trim();
        if (quantity && statusToMatch.includes(statusText)) {
          if (!uniqueShipments[trackingNbr]) {
            uniqueShipments[trackingNbr] = [];
          }
          uniqueShipments[trackingNbr].push(item);
          trackingNbrs.push(trackingNbr);
        }
      });
    });
  });
  const uniqueTrackingNbrs = [...new Set(trackingNbrs)];
  uniqueTrackingNbrs.forEach((trackingNbr) => {
    const items = uniqueShipments[trackingNbr] || [];
    const itemsArray = [];
    items.forEach((item) => {
      const { trackingInfo: itemTrackingInfo = [] } = item || {};
      const trackingInfoFiltered = itemTrackingInfo.filter((info) => {
        const { trackingNbr: targetTrackingNbr, status = '' } = info || {};
        const statusText = status.toLowerCase().trim();
        return targetTrackingNbr === trackingNbr && statusToMatch.includes(statusText);
      });

      const { quantity = 0, appeasement } = trackingInfoFiltered.length && trackingInfoFiltered[0];
      const obj = {
        ...item,
        trackingInfo: trackingInfoFiltered,
        itemInfo: {
          ...item.itemInfo,
          quantity,
          linePrice: item.itemInfo.offerPrice * parseInt(quantity, 10),
          appeasement,
        },
      };
      itemsArray.push(obj);
    });
    const { trackingInfo = [] } = itemsArray[0] || {};
    const { trackingUrl = '' } = trackingInfo[0] || {};
    shipments.push({
      shipment: [
        {
          items: itemsArray,
          shipmentStatus: targetStatus,
          trackingUrl,
        },
      ],
      isCancelledItems: false,
    });
  });
  return shipments;
};

export const buildShipments = (totalCount, purchasedItems, processingShipment, ...others) => {
  let shipments = [];
  const [
    shippedShipment,
    pickedUpShipment,
    readyForPickUpShipment,
    {
      canceledItems,
      preparingItemShipment,
      deliveredItemShipment,
      inTransitItemShipment,
      outForDeliveryItemShipment,
    },
  ] = others;
  const checkProcessing = (obj) => obj.packageStatus === PROCESSING;
  const ProcessingStatus = processingShipment.some(checkProcessing)
    ? PROCESSING
    : ORDER_BEING_PROCESSED;
  shipments = processMultiLineShipments(shipments, processingShipment, ProcessingStatus);
  if (shippedShipment.length) {
    shipments = processMultiLineShipments(shipments, shippedShipment, SHIPPED);
  }
  shipments = processMultiLineShipments(shipments, pickedUpShipment, ITEMS_PICKED_UP);
  shipments = processMultiLineShipments(shipments, readyForPickUpShipment, ITEM_READY_FOR_PICK_UP);
  shipments = processMultiLineShipments(shipments, preparingItemShipment, PREPARING_YOUR_ORDER);
  if (deliveredItemShipment.length) {
    shipments = processMultiLineShipments(shipments, deliveredItemShipment, DELIVERED);
  }
  if (inTransitItemShipment.length) {
    shipments = processMultiLineShipments(shipments, inTransitItemShipment, IN_TRANSIT);
  }
  if (outForDeliveryItemShipment.length) {
    shipments = processMultiLineShipments(shipments, outForDeliveryItemShipment, OUT_FOR_DELIVERY);
  }
  if (getCancelledItemsLength(canceledItems)) {
    shipments.push({
      shipment: canceledItems,
      isCancelledItems: true,
    });
  }

  shipments = processMultiLineShipments(shipments, purchasedItems, NA);

  return shipments;
};

export const getShipments = (purchasedItems, canceledItems) => {
  let processingShipment = [];
  let orderBeingProcessedShipment = [];
  let shippedShipment = [];
  let pickedUpShipment = [];
  let readyForPickUpShipment = [];
  let preparingItemShipment = [];
  let deliveredItemShipment = [];
  let inTransitItemShipment = [];
  let outForDeliveryItemShipment = [];

  if (purchasedItems) {
    processingShipment = getItemsWithTargetStatus(purchasedItems, PROCESSING);
    orderBeingProcessedShipment = getItemsWithTargetStatus(purchasedItems, ORDER_BEING_PROCESSED);
    shippedShipment = getItemsWithTargetStatus(purchasedItems, SHIPPED);
    pickedUpShipment = getItemsWithTargetStatus(purchasedItems, ITEMS_PICKED_UP);
    readyForPickUpShipment = getItemsWithTargetStatus(purchasedItems, ITEM_READY_FOR_PICK_UP);
    preparingItemShipment = getItemsWithTargetStatus(purchasedItems, PREPARING_YOUR_ORDER);
    deliveredItemShipment = getItemsWithTargetStatus(purchasedItems, DELIVERED);
    inTransitItemShipment = getItemsWithTargetStatus(purchasedItems, IN_TRANSIT);
    outForDeliveryItemShipment = getItemsWithTargetStatus(purchasedItems, OUT_FOR_DELIVERY);
  }
  const totalCount =
    processingShipment.length +
    orderBeingProcessedShipment.length +
    shippedShipment.length +
    pickedUpShipment.length +
    readyForPickUpShipment.length +
    preparingItemShipment.length +
    deliveredItemShipment.length +
    inTransitItemShipment.length +
    outForDeliveryItemShipment.length;

  return buildShipments(
    totalCount,
    purchasedItems,
    [...processingShipment, ...orderBeingProcessedShipment],
    shippedShipment,
    pickedUpShipment,
    readyForPickUpShipment,
    {
      canceledItems,
      preparingItemShipment,
      deliveredItemShipment,
      inTransitItemShipment,
      outForDeliveryItemShipment,
    }
  );
};

export const getTranslateFormattedDate = (shipDate) =>
  shipDate ? getTranslatedDate(shipDate, true) : '';

export const getIsNonEcom = (isBossOrder, isBopisOrder) => isBossOrder || isBopisOrder;

export const getIsShowOrderReview = (shipmentStatus, orderStatus) => {
  return (
    shipmentStatus === ORDER_SHIPPED ||
    orderStatus === ORDER_PARTIALLY_SHIPPED ||
    shipmentStatus === SHIPPED ||
    shipmentStatus === DELIVERED ||
    shipmentStatus === IN_TRANSIT ||
    shipmentStatus === OUT_FOR_DELIVERY
  );
};

export const getNotificationHeader = (isBossOrder, ordersLabels) =>
  isBossOrder
    ? getLabelValue(ordersLabels, 'lbl_orders_noLongerAvailable')
    : getLabelValue(ordersLabels, 'lbl_orders_canceledItems');

export const getNotificationMessage = (isBossOrder, ordersLabels) =>
  isBossOrder
    ? getLabelValue(ordersLabels, 'lbl_orders_isBossOrderCancelNotification')
    : getLabelValue(ordersLabels, 'lbl_orders_CancelNotification');

export const getStatusText = (isPartiallyCancelledOrder, ordersLabels) =>
  isPartiallyCancelledOrder
    ? getLabelValue(ordersLabels, 'lbl_orders_statusOrderPartiallyCancelled')
    : getLabelValue(ordersLabels, 'lbl_orders_statusOrderCancelled');

// Return Date and label based on order status
export const getRCStatus = (
  status,
  estimatedDeliveryDate,
  actualDeliveryDate,
  shippedDate,
  carrierDeliveryDate,
  ordersLabels
) => {
  const shipmentStatus = status && status.toLowerCase();
  switch (shipmentStatus) {
    case DELIVERED:
      return actualDeliveryDate
        ? {
            label: getLabelValue(ordersLabels, 'lbl_edd_delivered_on'),
            displayDate: actualDeliveryDate,
          }
        : {
            label: getLabelValue(ordersLabels, 'lbl_orderDetails_shipment_on'),
            displayDate: shippedDate,
          };
    case IN_TRANSIT:
    case OUT_FOR_DELIVERY:
    case SHIPPED:
    case ORDER_SHIPPED:
    case PARTIALLY_SHIPPED:
      return carrierDeliveryDate
        ? {
            label: getLabelValue(ordersLabels, 'lbl_edd_arrivedby'),
            displayDate: carrierDeliveryDate,
          }
        : {
            label: getLabelValue(ordersLabels, 'lbl_orderDetails_shipment_on'),
            displayDate: shippedDate,
          };
    case PROCESSING:
    case ORDER_IN_PROCESS:
      return estimatedDeliveryDate
        ? {
            label: getLabelValue(ordersLabels, 'lbl_edd_arrivedby'),
            displayDate: estimatedDeliveryDate,
          }
        : {};
    default:
      return {};
  }
};

export const getParseDate = (date) => (date ? new Date(convertToISOString(date)) : '');
