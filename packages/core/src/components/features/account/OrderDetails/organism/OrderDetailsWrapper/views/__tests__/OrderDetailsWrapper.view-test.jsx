import React from 'react';
import { shallow } from 'enzyme';
import { getLogoIconPath } from '../OrderDetailsWrapper.utils';
import constants from '../../../../OrderDetails.constants';
// import { renderOrderDetailWithCRMView } from '../OrderDetailsWrapper.view';

jest.mock('@tcp/core/src/utils', () => ({
  getViewportInfo: jest.fn().mockImplementation(() => {
    return { isDesktop: true };
  }),
  getAPIConfig: () => ({
    brandId: 'tcp',
  }),
  isCanada: jest.fn(),
  isClient: jest.fn(),
  isMobileApp: jest.fn(),
  getSiteId: jest.fn(),
  getIconPath: (icon) => `/static/${icon}`,
  getStaticFilePath: jest.fn,
}));

describe('Order Group Header component', () => {
  it('should renders correctly', () => {
    const props = {
      OrderDetailsData: {
        orderStatus: constants.STATUS_CONSTANTS.ORDER_RECEIVED,
        shippedDate: '2019-10-09',
        trackingNumber: '123455',
        trackingUrl: '/test',
        orderNumber: '654321',
        purchasedItems: [
          {
            items: [
              {
                itemInfo: {
                  itemBrand: 'TCP',
                  linePrice: 24.94,
                  listPrice: 24.94,
                  offerPrice: 24.94,
                  quantity: 1,
                  quantityCanceled: 0,
                  quantityOOS: 0,
                  quantityReturned: 0,
                  quantityShipped: 0,
                },
              },
            ],
          },
        ],
        summary: {
          currencySymbol: '$',
        },
        isBopisOrder: false,
        ordersLabels: {},
        orderType: 'ECOM',
      },
    };
    const component = shallow(<renderOrderDetailWithCRMView {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders Return Initiated Item correctly', () => {
    const props = {
      OrderDetailsData: {
        orderStatus: constants.STATUS_CONSTANTS.ORDER_RECEIVED,
        shippedDate: '2019-10-10',
        trackingNumber: '123455',
        trackingUrl: '/test',
        orderNumber: '654321',
        purchasedItems: [
          {
            items: [
              {
                itemInfo: {
                  itemBrand: 'TCP',
                  linePrice: 24.94,
                  listPrice: 24.94,
                  offerPrice: 24.94,
                  quantity: 1,
                  quantityCanceled: 0,
                  quantityOOS: 0,
                  quantityReturned: 0,
                  quantityShipped: 0,
                },
              },
            ],
          },
        ],
        returnedInitiatedItems: [
          {
            orderStatus: 'return initiated',
            returnInitiatedDate: '9/22/21 12:31:00',
            status: 'return initiated',
            isCSHReturn: 'TRUE',
            items: [
              {
                itemInfo: {
                  itemBrand: 'TCP',
                  linePrice: 24.94,
                  listPrice: 24.94,
                  offerPrice: 24.94,
                  quantity: 1,
                  quantityCanceled: 0,
                  quantityOOS: 0,
                  quantityReturned: 0,
                  quantityShipped: 0,
                },
              },
            ],
          },
        ],

        summary: {
          currencySymbol: '$',
        },
        isBopisOrder: false,
        ordersLabels: {},
        orderType: 'ECOM',
      },
    };
    const component = shallow(<renderOrderDetailWithCRMView {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders correctly for shipped orders', () => {
    const props = {
      OrderDetailsData: {
        orderStatus: constants.STATUS_CONSTANTS.ORDER_SHIPPED,
        shippedDate: '2019-10-09',
        trackingNumber: '123455',
        trackingUrl: '/test',
        orderNumber: '654321',
        purchasedItems: [
          {
            items: [
              {
                itemInfo: {
                  itemBrand: 'TCP',
                  linePrice: 24.94,
                  listPrice: 24.94,
                  offerPrice: 24.94,
                  quantity: 1,
                  quantityCanceled: 0,
                  quantityOOS: 0,
                  quantityReturned: 0,
                  quantityShipped: 0,
                },
                trackingInfo: {
                  status: constants.STATUS_CONSTANTS.ORDER_SHIPPED,
                  trackingNbr: '123',
                  trackingUrl: '/test',
                },
              },
            ],
          },
        ],
        summary: {
          currencySymbol: '$',
        },
        isBopisOrder: false,
        ordersLabels: {},
        orderType: 'ECOM',
      },
    };
    const component = shallow(<renderOrderDetailWithCRMView {...props} />);
    expect(component).toMatchSnapshot();
  });
});

describe('OrderDetailsWrapper utils', () => {
  it('should return delivered icon', () => {
    expect(getLogoIconPath('delivered')).toBe(
      'undefined/ecom/assets/static/icon/order-delivered-filled.svg'
    );
  });
  it('should return canceled icon', () => {
    expect(getLogoIconPath('canceled')).toBe(
      'undefined/ecom/assets/static/icon/shipping-cancelled.svg'
    );
  });
  it('should return canceled icon', () => {
    expect(getLogoIconPath('partially canceled')).toBe(
      'undefined/ecom/assets/static/icon/shipping-cancelled.svg'
    );
  });
});
