// 9fbef606107a605d69c0edbcd8029e5d
const cardIconMapping = {
  DISC: 'disc-small',
  'MASTER CARD': 'mc-small',
  AMEX: 'amex-small',
  VISA: 'visa-small',
  'GIFT CARD': 'gift-card-small',
  'MY PLACE REWARDS CREDIT CARD': 'place-card-small',
  'PLACE CARD': 'place-card-small',
  VENMO: 'venmo-blue-acceptance-mark',
  PAYPAL: 'paypal-icon',
  'APPLE PAY': 'apple-pay-icon',
  APPLEPAY: 'apple-pay-icon',
  AFTERPAY: 'after-pay-icon',
};

export default cardIconMapping;
