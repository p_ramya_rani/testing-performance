// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

const AnchorWrapper = styled.View`
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.MED};
  flex-direction: row;
  align-items: flex-start;
`;

export default AnchorWrapper;
