// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { PickUpSummaryVanilla } from '../PickUpSummary.view';
import Anchor from '../../../../../../../common/atoms/Anchor';

describe('Order Summary Details component', () => {
  const props = {
    ordersLabels: {},
    pickUpStore: {
      basicInfo: {
        address: {
          addressLine1: 'space 170',
          city: 'New Jersey',
          state: 'NJ',
          zipCode: '33232',
        },
        phone: '23423423423',
      },
      pickUpPrimary: {
        firstName: 'Username1',
        lastName: 'Username2',
      },
      pickUpAlternative: {},
    },
  };

  it('should renders correctly', () => {
    const component = shallow(<PickUpSummaryVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should call directions click', () => {
    const getDirections = jest.fn();
    window.open = jest.fn();
    const component = shallow(<PickUpSummaryVanilla {...props} />);
    expect(component.find(Anchor)).toHaveLength(1);
    component.find(Anchor).at(0).simulate('click', { preventDefault: jest.fn() });
    getDirections.mockImplementation();
    expect(window.open).toBeCalled();
  });
});
