// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { BodyCopy, Button, Anchor } from '@tcp/core/src/components/common/atoms';
import { UrlHandler } from '@tcp/core/src/utils/utils.app';

import {
  getLabelValue,
  getOrderGroupLabelAndMessage,
  hasTrackingNum,
  getTrimmedTrackingNum,
} from '@tcp/core/src/utils/utils';
import names from '../../../../../../../constants/eventsName.constants';
import constants from '../../../OrderDetails.constants';
import {
  ContentWrapper,
  StyledAnchor,
  ContentCol,
  ContentRow,
} from '../../../styles/OrderDetails.style.native';
import ClickTracker from '../../../../../../../../../mobileapp/src/components/common/atoms/ClickTracker';

const AnalyticsData = {
  pageName: 'myplace:orders:order details',
  pageType: 'myplace',
  pageSection: 'myplace',
  pageSubSection: 'myplace',
  customEvents: 'event118',
};

const TrackingNumber = (props) => {
  const { isBopisOrder, trackingNumber, ordersLabels } = props;
  const orderTrackingNumber = getTrimmedTrackingNum(trackingNumber);

  if (!isBopisOrder && hasTrackingNum(orderTrackingNumber)) {
    return (
      <ContentWrapper>
        <BodyCopy
          fontSize="fs14"
          fontFamily="secondary"
          text={getLabelValue(ordersLabels, 'lbl_orders_trackingNumber')}
        />
        <BodyCopy
          fontSize="fs14"
          fontWeight="semibold"
          fontFamily="secondary"
          text={orderTrackingNumber}
        />
      </ContentWrapper>
    );
  }
  return <></>;
};

const TrackingLinkView = (props) => {
  const { isBopisOrder, trackingUrl, ordersLabels } = props;
  if (!isBopisOrder && trackingUrl && trackingUrl !== constants.STATUS_CONSTANTS.NA) {
    return (
      <ContentWrapper>
        <ClickTracker
          as={Button}
          clickData={AnalyticsData}
          name={names.screenNames.order_tracking}
          module="account"
          width="100%"
          buttonVariation="fixed-width"
          fill="BLUE"
          color="white"
          onPress={() => UrlHandler(trackingUrl)}
          data-locator="orders-shop-now-btn"
          text={getLabelValue(ordersLabels, 'lbl_orders_trackit')}
        />
      </ContentWrapper>
    );
  }
  return <></>;
};

const TrackingLinkCtaView = (props) => {
  const { isBopisOrder, trackingUrl, ordersLabels, trackingNumber } = props;
  const orderTrackingNumber = getTrimmedTrackingNum(trackingNumber);
  if (!isBopisOrder && trackingUrl && trackingUrl !== constants.STATUS_CONSTANTS.NA) {
    return (
      <ContentRow>
        <StyledAnchor
          onPress={() => UrlHandler(trackingUrl)}
          anchorVariation="white"
          fontWeight="extrabold"
          fontFamily="secondary"
          fontSizeVariation="medium"
          text={`${getLabelValue(ordersLabels, 'lbl_orders_trackit')}`}
        />
        <ContentCol>
          <Anchor
            onPress={() => UrlHandler(trackingUrl)}
            fontFamily="secondary"
            fontSizeVariation="medium"
            text={orderTrackingNumber}
            underline
          />
        </ContentCol>
      </ContentRow>
    );
  }
  return <></>;
};

/**
 * This function component use for return the Order Status
 * can be passed in the component.
 * @param otherProps - otherProps object used pass params to other component
 */
const OrderStatus = (props) => {
  const { label, message } = getOrderGroupLabelAndMessage(props);
  const { showNumAsLink } = props;
  return (
    <>
      {label || message ? (
        <ContentWrapper>
          <BodyCopy fontSize="fs14" fontFamily="secondary" text={label} />
          <BodyCopy fontSize="fs14" fontFamily="secondary" fontWeight="semibold" text={message} />
        </ContentWrapper>
      ) : null}
      {!showNumAsLink && TrackingNumber(props)}
      {!showNumAsLink && TrackingLinkView(props)}
      {showNumAsLink && TrackingLinkCtaView(props)}
    </>
  );
};

const commonPropTypes = {
  trackingNumber: PropTypes.string,
  trackingUrl: PropTypes.string,
  isBopisOrder: PropTypes.bool.isRequired,
  ordersLabels: PropTypes.shape({
    lbl_orderDetails_shipping: PropTypes.string,
  }),
};

const commonDefaultProps = {
  trackingNumber: '',
  trackingUrl: '',
  ordersLabels: {
    lbl_orderDetails_shipping: '',
  },
};

TrackingLinkView.propTypes = {
  ...commonPropTypes,
};

TrackingLinkView.defaultProps = {
  ...commonDefaultProps,
};

TrackingNumber.propTypes = {
  ...commonPropTypes,
};

TrackingNumber.defaultProps = {
  ...commonDefaultProps,
};

TrackingLinkCtaView.propTypes = {
  ...commonPropTypes,
};

TrackingLinkCtaView.defaultProps = {
  ...commonDefaultProps,
};

OrderStatus.propTypes = {
  ordersLabels: PropTypes.shape({
    lbl_orderDetails_shipping: PropTypes.string,
  }),
  showNumAsLink: PropTypes.bool,
};

OrderStatus.defaultProps = {
  ordersLabels: {
    lbl_orderDetails_shipping: '',
  },
  showNumAsLink: false,
};

export default OrderStatus;
