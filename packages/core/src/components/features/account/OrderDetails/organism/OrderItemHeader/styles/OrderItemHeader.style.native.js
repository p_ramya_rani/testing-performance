import styled from 'styled-components/native';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';

const OrderStatusImage = styled.View`
  margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
`;

const getShipmentColor = (props) => {
  const { statusTextClass } = props;
  if (statusTextClass === 'success-status') {
    return props.theme.colors.CUSTOM.ORDER_SUCCESS;
  }
  if (statusTextClass === 'cancelled-status') {
    return props.theme.colorPalette.red[500];
  }
  if (statusTextClass === 'default returnInitiated') {
    return props.theme.colors.TEXT.DARK;
  }
  return props.theme.colorPalette.gray[800];
};

const ShipmentText = styled(BodyCopy)`
  text-transform: ${(props) => (props.isReturn ? 'none' : 'capitalize')};
  margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  color: ${(props) => getShipmentColor(props)};

  ${(props) =>
    props.isOrderSuccess
      ? props.theme.colors.CUSTOM.ORDER_SUCCESS
      : props.theme.colorPalette.red[500]};
`;

const ReturnLabelText = styled(BodyCopy)`
  margin-top: 14px;
  color: ${(props) => props.theme.colors.TEXT.DARK};
  margin-right: 30px;
`;

const OrderLogoAndStatus = styled.View`
  flex-direction: row;
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
`;

const OrderHeadingStatus = styled.View`
  flex-direction: row;
`;

export { OrderLogoAndStatus, OrderStatusImage, ShipmentText, OrderHeadingStatus, ReturnLabelText };
