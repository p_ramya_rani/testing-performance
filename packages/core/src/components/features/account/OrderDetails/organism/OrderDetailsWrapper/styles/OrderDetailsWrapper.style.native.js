import styled from 'styled-components/native';

const ReturnedInfo = styled.View`
  background: ${(props) => props.theme.colorPalette.gray[300]};
  padding: 9px;
  max-width: 364px;
  width: 100%;
  margin-left: 0;
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
  font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy2}px;
  flex-direction: row;
  flex-shrink: 1;
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
`;

const ReturnedStatus = styled.View`
  font-weight: ${(props) => props.theme.fonts.fontWeight.bold};
  text-decoration: underline;
`;

const RefundAmount = styled.View`
  font-weight: ${(props) => props.theme.fonts.fontWeight.bold};
`;

export { ReturnedInfo, ReturnedStatus, RefundAmount };
