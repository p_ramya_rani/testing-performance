// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import { ScrollView, View } from 'react-native';
import PropTypes from 'prop-types';
import {
  Image,
  BodyCopy,
  Button,
  Anchor,
  ScreenViewShot,
} from '@tcp/core/src/components/common/atoms';
import LineComp from '@tcp/core/src/components/common/atoms/Line';
import Barcode from '@tcp/core/src/components/common/molecules/Barcode';
import RNPrint from 'react-native-print';
import {
  getLabelValue,
  convertToISOString,
  getMobileFormatedOrderDate,
} from '@tcp/core/src/utils/utils';
import OrderReceiptItem from '@tcp/core/src/components/features/account/OrderDetails/organism/OrderReceiptItem/views/OrderReceiptItem.view.native';
import getGiftReceiptMarkupForPrint from '@tcp/core/src/components/features/CnC/common/organism/GiftReceipt/views/GiftReceiptHTML.native';
import OrderReturnReceipt from '@tcp/core/src/components/features/account/Orders/molecules/OrderReturnReceipt/views/OrderReturnReceipt.view.native';
import constants from '../OrderReceipt.constants';
import {
  OrderReceiptWrapper,
  OrderNoReceiptContainer,
  BackButton,
  ReceiptsTextContainer,
  ReceiptsContainer,
  ShipmentViewContainer,
  ShipmentHeaderContainer,
  ShipmentItemWrapper,
  HiddenBarCodeReceipt,
  PrintReceiptContainer,
  GiftContainer,
  ShipmentTextContainer,
  OrderReturnContainer,
  BarcodeContainer,
} from '../styles/OrderReceipt.style.native';

const giftIcon = require('../../../../../../../../../mobileapp/src/assets/images/gift_guide.png');

class OrderReceipt extends PureComponent {
  constructor(props) {
    super(props);
    this.screenViewShotRef = null;
    this.barcode = null;
    this.isPrintReceiptOpen = false;
    this.isPrintGiftReceiptOpen = false;
  }

  checkForEligibleOrder = (orderStatus, trackingNumber, isBossOrder, isBopisOrder) => {
    return (
      (orderStatus === constants.STATUS_CONSTANTS.ORDER_SHIPPED ||
        orderStatus === constants.STATUS_CONSTANTS.ORDER_PARTIALLY_SHIPPED ||
        orderStatus === constants.STATUS_CONSTANTS.ORDER_PICKED_UP ||
        isBopisOrder) &&
      (orderStatus === constants.STATUS_CONSTANTS.ORDER_PICKED_UP ||
        trackingNumber ||
        isBossOrder ||
        isBopisOrder)
    );
  };

  /**
   * @method renderNoReceiptView
   * @description render when no receipts are there from the order
   */
  renderNoReceiptView = () => {
    const { ordersLabels, navigation } = this.props;
    return (
      <OrderNoReceiptContainer>
        <BodyCopy
          mobileFontFamily="secondary"
          fontSize="fs18"
          fontWeight="semibold"
          textAlign="center"
          margin="20px 0"
          text={getLabelValue(ordersLabels, 'lbl_orderDetails_no_receipts_yet')}
        />
        <ReceiptsTextContainer>
          <BodyCopy
            mobileFontFamily="secondary"
            fontSize="fs16"
            textAlign="center"
            text={getLabelValue(ordersLabels, 'lbl_orderDetails_provided_once_order_ships')}
          />
        </ReceiptsTextContainer>
        <BackButton>
          <Button
            color="black"
            fill="WHITE"
            type="submit"
            fontSize="fs14"
            text={getLabelValue(ordersLabels, 'lbl_orderDetails_back_to_order_details')}
            onPress={() => navigation.goBack()}
          />
        </BackButton>
      </OrderNoReceiptContainer>
    );
  };

  /**
   * @method isPurchasedItemsShipmentEligible
   * @description filter items which are eligible for shipment from the order details list
   */
  isPurchasedItemsShipmentEligible = () => {
    const { orderDetailsData } = this.props;
    const {
      purchasedItems = [],
      purchasedItemsAll = [],
      isBopisOrder,
      isBossOrder,
    } = orderDetailsData;

    const allItems =
      purchasedItemsAll && purchasedItemsAll.length ? purchasedItemsAll : purchasedItems;

    return allItems && allItems.length
      ? allItems.filter((purchasedItem) => {
          const { trackingNumber, orderStatus } = purchasedItem;
          return this.checkForEligibleOrder(orderStatus, trackingNumber, isBossOrder, isBopisOrder);
        })
      : [];
  };

  setScreenViewShotRef = (element) => {
    this.screenViewShotRef = element;
  };

  getOrderReceipt = ({
    orderNumber,
    returnDays,
    ordersLabels,
    orderDate,
    orderDetailsData,
    userAccountId,
    barcode,
  }) => {
    return OrderReturnReceipt({
      barcode,
      orderNumber,
      returnDays,
      ordersLabels,
      orderDate,
      orderDetailsData,
      userAccountId,
    });
  };

  getBarcode = async () => {
    if (!this.barcode) {
      this.barcode = this.screenViewShotRef ? await this.screenViewShotRef.capture() : null;
    }
    return this.barcode;
  };

  /**
   * @method printOrderReturnReceipt
   * @description print order return receipt in the print dialog
   * @param {object} printOrderParams
   */
  printOrderReturnReceipt = async ({
    orderNumber,
    returnDays,
    ordersLabels,
    orderDate,
    orderDetailsData,
    userAccountId,
  }) => {
    const barcode = await this.getBarcode();
    const orderReceiptHtml = this.getOrderReceipt({
      barcode,
      orderNumber,
      returnDays,
      ordersLabels,
      orderDate,
      orderDetailsData,
      userAccountId,
    });
    await RNPrint.print({
      html: orderReceiptHtml,
    });
    this.isPrintReceiptOpen = false;
  };

  /**
   * @description print gift receipt html in the print dialog
   * @param {object} printOrderParams
   */
  printHTML = async ({ orderNumber, returnDays, giftReceiptLabels, orderDate }) => {
    const barcode = await this.getBarcode();
    await RNPrint.print({
      html: getGiftReceiptMarkupForPrint({
        barcode,
        orderNumber,
        returnDays,
        giftReceiptLabels,
        orderDate,
      }),
    });
    this.isPrintGiftReceiptOpen = false;
  };

  renderShipmentContainer = ({
    index,
    totalCount,
    purchasedItem,
    orderNumber,
    orderDateParsed,
    ordersLabels,
    orderStatus,
    orderDate,
    isBopisOrder,
    isBossOrder,
  }) => {
    const { orderReturnDays, giftReceiptLabels } = this.props;
    const { items } = purchasedItem;
    const isPickedUp =
      orderStatus === constants.STATUS_CONSTANTS.ORDER_PICKED_UP || isBopisOrder || isBossOrder;
    const shipmentText = isPickedUp
      ? `${getLabelValue(ordersLabels, 'lbl_orders_statusItemsPickedUp')}`
      : `${getLabelValue(ordersLabels, 'lbl_orderDetails_shipment')} ${index} of ${totalCount}`;
    const linkMargin = { marginLeft: 5 };
    return (
      <ShipmentViewContainer>
        <ShipmentHeaderContainer>
          <ShipmentTextContainer>
            <BodyCopy
              fontFamily="secondary"
              fontSize="fs15"
              fontWeight="bold"
              text={shipmentText}
            />
          </ShipmentTextContainer>
          <GiftContainer>
            <Image alt="image-placeholder" source={giftIcon} width="15" height="15" />
            <Anchor
              fontFamily="secondary"
              customStyle={linkMargin}
              underline
              to="/#"
              anchorVariation="primary"
              dataLocator="print-gift-receipt"
              onPress={() => {
                if (!this.isPrintGiftReceiptOpen) {
                  this.isPrintGiftReceiptOpen = true;
                  this.printHTML({
                    orderNumber,
                    returnDays: orderReturnDays,
                    giftReceiptLabels,
                    orderDate,
                  });
                }
              }}
              text={getLabelValue(ordersLabels, 'lbl_orderDetails_print_gift_receipt')}
            />
          </GiftContainer>
        </ShipmentHeaderContainer>
        {items &&
          items.length > 0 &&
          items.map((item) => {
            return (
              <ShipmentItemWrapper>
                <OrderReceiptItem
                  key={index.toString()}
                  item={item}
                  ordersLabels={ordersLabels}
                  returnDate={orderReturnDays ? getMobileFormatedOrderDate(orderDateParsed) : null}
                />
                <LineComp marginTop={32} borderWidth={0.5} borderColor="gray.800" />
              </ShipmentItemWrapper>
            );
          })}
        <BarcodeContainer>
          <BodyCopy
            fontFamily="secondary"
            fontSize="fs14"
            fontWeight="bold"
            textAlign="center"
            text={`${getLabelValue(ordersLabels, 'lbl_orderDetails_show_barcode')}`}
          />
          <Barcode
            value={orderNumber}
            barcodeId={orderNumber}
            height={35}
            width={2.5}
            fontWeight="regular"
            displayValue
          />
        </BarcodeContainer>
      </ShipmentViewContainer>
    );
  };

  renderReceiptView = (purchasedItemsEligible) => {
    const { orderDetailsData, orderReturnDays, ordersLabels, userAccountId } = this.props;
    const { orderNumber, orderDate, isBopisOrder, isBossOrder } = orderDetailsData;
    const orderPlacedDate = new Date(convertToISOString(orderDate));
    const orderDateParsed = new Date(orderPlacedDate);
    orderDateParsed.setDate(orderDateParsed.getDate() + orderReturnDays);
    return (
      <ReceiptsContainer>
        <BodyCopy
          mobileFontFamily="secondary"
          fontSize="fs16"
          text={getLabelValue(ordersLabels, 'lbl_orderDetails_shipped_multiple_packages')}
        />
        <PrintReceiptContainer>
          <Button
            color="black"
            fill="WHITE"
            type="submit"
            fontSize="fs14"
            text={getLabelValue(ordersLabels, 'lbl_orderDetails_print_receipts')}
            onPress={() => {
              if (!this.isPrintReceiptOpen) {
                this.isPrintReceiptOpen = true;
                this.printOrderReturnReceipt({
                  orderNumber,
                  returnDays: orderReturnDays,
                  ordersLabels,
                  orderDate,
                  orderDetailsData,
                  userAccountId,
                });
              }
            }}
          />
        </PrintReceiptContainer>
        <BodyCopy
          mobileFontFamily="secondary"
          fontSize="fs14"
          fontWeight="semibold"
          textAlign="center"
          margin="30px 0 5px 0"
          text={getLabelValue(ordersLabels, 'lbl_orderDetails_receipts_shown_below')}
        />
        {!orderReturnDays && (
          <OrderReturnContainer>
            <BodyCopy
              mobileFontFamily="secondary"
              fontSize="fs15"
              fontWeight="semibold"
              text={getLabelValue(ordersLabels, 'lbl_order_receipt_message_heading')}
            />
            <BodyCopy
              mobileFontFamily="secondary"
              fontSize="fs14"
              margin="14px 0 8px 0"
              text={getLabelValue(ordersLabels, 'lbl_order_receipt_message_information')}
            />
          </OrderReturnContainer>
        )}
        {purchasedItemsEligible &&
          purchasedItemsEligible.length > 0 &&
          purchasedItemsEligible.map((purchasedItem, i) => {
            const { orderStatus } = purchasedItem;
            return (
              <View>
                {this.renderShipmentContainer({
                  index: i + 1,
                  totalCount: purchasedItemsEligible.length,
                  purchasedItem,
                  orderNumber,
                  orderDateParsed,
                  ordersLabels,
                  orderStatus,
                  orderDate,
                  isBopisOrder,
                  isBossOrder,
                })}
              </View>
            );
          })}
      </ReceiptsContainer>
    );
  };

  /**
   * @method isShipmentReceiptAvailable
   * @description - condition to check if we need to show shipment or not
   * @param {string} status
   */
  isShipmentReceiptAvailable = (status) => {
    return status
      ? status.toLowerCase() === constants.STATUS_CONSTANTS.ITEMS_READY_FOR_PICKUP.toLowerCase() ||
          status.toLowerCase() === constants.STATUS_CONSTANTS.ORDER_RECEIVED_LABEL.toLowerCase()
      : false;
  };

  render() {
    const { orderDetailsData, orderReturnDays } = this.props;
    const { orderDate, orderNumber, orderStatus: status } = orderDetailsData;
    const orderPlacedDate = new Date(convertToISOString(orderDate));
    const orderDateParsed = new Date(orderPlacedDate);
    orderDateParsed.setDate(orderDateParsed.getDate() + orderReturnDays);
    const purchasedItemsEligible = this.isPurchasedItemsShipmentEligible();
    return (
      <ScrollView keyboardShouldPersistTaps="handled">
        <OrderReceiptWrapper>
          {purchasedItemsEligible &&
            purchasedItemsEligible.length > 0 &&
            !this.isShipmentReceiptAvailable(status) &&
            this.renderReceiptView(purchasedItemsEligible)}
          {((purchasedItemsEligible && purchasedItemsEligible.length === 0) ||
            this.isShipmentReceiptAvailable(status)) &&
            this.renderNoReceiptView()}

          <HiddenBarCodeReceipt>
            <ScreenViewShot
              setScreenViewShotRef={(e) => this.setScreenViewShotRef(e)}
              options={{ format: 'png', quality: 0.9, result: 'base64' }}
            >
              <Barcode value={orderNumber} barcodeId={orderNumber} width="2" displayValue={false} />
            </ScreenViewShot>
          </HiddenBarCodeReceipt>
        </OrderReceiptWrapper>
      </ScrollView>
    );
  }
}

OrderReceipt.propTypes = {
  orderDetailsData: PropTypes.shape({}),
  orderReturnDays: PropTypes.number,
  ordersLabels: PropTypes.shape({}),
  giftReceiptLabels: PropTypes.shape({}),
  userAccountId: PropTypes.string,
  navigation: PropTypes.shape({}),
};

OrderReceipt.defaultProps = {
  orderDetailsData: {},
  orderReturnDays: 0,
  ordersLabels: {},
  giftReceiptLabels: {},
  userAccountId: '',
  navigation: { goBack: () => {} },
};

export default OrderReceipt;
