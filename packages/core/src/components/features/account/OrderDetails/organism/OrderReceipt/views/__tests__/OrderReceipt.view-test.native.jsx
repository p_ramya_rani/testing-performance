// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable sonarjs/no-duplicate-string */
import React from 'react';
import { shallow } from 'enzyme';
import OrderReceipt from '../OrderReceipt.view.native';

const orderDetailsData = {
  orderNumber: '6005056419',
  emailAddress: '',
  orderDate: '2020-06-25',
  pickUpExpirationDate: '2020-06-29',
  pickedUpDate: '2020-06-25',
  shippedDate: '2020-06-25',
  orderStatus: 'order picked up',
  encryptedEmailAddress: 'N8gQ0YhPwuRLvj36P9glyExWITIHq2qmWGYNKkOXM54%3D',
  status: 'lbl_orders_statusItemsPickedUp',
  trackingNumber: null,
  trackingUrl: null,
  isBopisOrder: true,
  isBossOrder: false,
  orderType: 'BOPIS',
  bossMaxDate: null,
  bossMinDate: null,
  summary: {
    currencySymbol: '$',
    totalItems: 1,
    subTotal: 7.98,
    purchasedItems: 1,
    shippedItems: 1,
    canceledItems: 0,
    returnedItems: 0,
    returnedTotal: 0,
    couponsTotal: 0,
    shippingTotal: 0,
    totalTax: 0,
    grandTotal: 7.98,
  },
  appliedGiftCards: [],
  canceledItems: [],
  purchasedItems: [
    {
      trackingNumber: '927489634847050027688407',
      trackingUrl:
        'https://childrensplace.narvar.com/childrensplace/tracking/ups?order_number=6005056419&order_date=2020-06-25T20:01:08-04:00&dzip=&locale=en_US&tracking_numbers=927489634847050027688407&ship_date=2020-07-05T00:00:00-04:00&service=UG',
      shippedDate: '7/5/20 00:00',
      status: 'Order Shipped',
      orderStatus: 'order shipped',
      items: [
        {
          itemInfo: {
            listPrice: 7.98,
            offerPrice: 7.98,
            linePrice: 7.98,
            quantity: 1,
            quantityCanceled: 0,
            quantityShipped: 1,
            quantityReturned: 0,
            quantityOOS: 0,
            itemBrand: 'GYM',
          },
          productInfo: {
            fit: null,
            pdpUrl:
              'http://test-uat1.gymboree.com/us/p/Boys-Woven-Pull-On-Cargo-Shorts---Opening-Day-and-Whale-Hello-There-3009571-131',
            name: 'KHK CARGO SHORT',
            imagePath:
              'https://test1.theplace.com/image/upload/ecom/assets/products/gym/3009571/3009571_131.jpg',
            upc: '00193511635967',
            size: '12-18 M',
            color: {
              name: 'ALMOND',
              imagePath: '3009571/3009571_131_swatch.jpg',
            },
          },
          trackingInfo: [
            {
              carrier: 'UPS',
              quantity: 1,
              shipDate: '7/5/20 00:00',
              shipVia: 'UGNR',
              status: 'N/A',
              trackingNbr: '927489634847050027688407',
              trackingUrl:
                'https://childrensplace.narvar.com/childrensplace/tracking/ups?order_number=6005056419&order_date=2020-06-25T20:01:08-04:00&dzip=&locale=en_US&tracking_numbers=927489634847050027688407&ship_date=2020-07-05T00:00:00-04:00&service=UG',
            },
          ],
        },
      ],
    },
    {
      items: [
        {
          productInfo: {
            fit: null,
            pdpUrl:
              'http://test-uat1.gymboree.com/us/p/Boys-Woven-Pull-On-Cargo-Shorts---Opening-Day-and-Whale-Hello-There-3009571-131',
            name: 'KHK CARGO SHORT',
            imagePath:
              'https://test1.theplace.com/image/upload/ecom/assets/products/gym/3009571/3009571_131.jpg',
            upc: '00193511635967',
            size: '12-18 M',
            color: {
              name: 'ALMOND',
              imagePath: '3009571/3009571_131_swatch.jpg',
            },
          },
          itemInfo: {
            listPrice: 7.98,
            offerPrice: 7.98,
            linePrice: 7.98,
            quantity: 1,
            quantityCanceled: 0,
            quantityShipped: 1,
            quantityReturned: 0,
            quantityOOS: 0,
            itemBrand: 'GYM',
          },
          trackingInfo: [
            {
              carrier: 'UPS',
              quantity: 1,
              shipDate: '7/5/20 00:00',
              shipVia: 'UGNR',
              status: 'N/A',
              trackingNbr: '927489634847050027688407',
              trackingUrl:
                'https://childrensplace.narvar.com/childrensplace/tracking/ups?order_number=6005056419&order_date=2020-06-25T20:01:08-04:00&dzip=&locale=en_US&tracking_numbers=927489634847050027688407&ship_date=2020-07-05T00:00:00-04:00&service=UG',
            },
          ],
          isShippedItem: true,
        },
      ],
    },
  ],
  outOfStockItems: [],
  checkout: {
    shippingAddress: null,
    pickUpStore: {
      basicInfo: {
        id: null,
        storeName: 'BERGEN TOWN CENTER',
        address: {
          addressLine1: '2701 BERGEN TOWN CENTER ',
          city: 'PARAMUS',
          state: 'NJ',
          zipCode: '07652',
        },
        phone: '2012269710',
      },
      distance: null,
      pickUpPrimary: {
        firstName: 'Sandy',
        lastName: 'Benyamen',
        emailAddress: 'TEST.SAGAR145@YOPMAIL.COM',
      },
      pickUpAlternative: null,
      hours: {
        regularHours: [],
      },
    },
    billing: {
      card: {
        endingNumbers: '************1111',
        cardType: 'Visa',
        chargedAmount: '$7.98',
        id: null,
      },
      sameAsShipping: false,
      billingAddress: {
        firstName: 'sagar',
        lastName: 'test',
        addressLine1: '2160 S 1st Ave',
        addressLine2: '',
        city: 'Maywood',
        state: 'IL',
        zipCode: '60153',
        country: 'United States',
      },
    },
  },
};
const initialProps = {
  ...{ orderDetailsData },
  orderReturnDays: 0,
};

describe('Order Reciept Container component', () => {
  let component;
  const props = initialProps;
  beforeEach(() => {
    component = shallow(<OrderReceipt {...props} />);
  });

  it('should be defined', () => {
    expect(component).toBeDefined();
  });

  it.skip('should renders correctly', () => {
    expect(component).toMatchSnapshot();
  });

  it.skip('should renders correctly with order return days', () => {
    component = shallow(<OrderReceipt {...props} orderReturnDays={45} />);
    expect(component).toMatchSnapshot();
  });

  it('should not render no receipts page', () => {
    orderDetailsData.orderStatus = 'order shipped';
    orderDetailsData.isBopisOrder = false;
    orderDetailsData.isBossOrder = false;
    orderDetailsData.purchasedItemsAll = [...orderDetailsData.purchasedItems];
    component.setProps({ orderDetailsData });
    expect(component.find('.no-receipts-container')).toHaveLength(0);
    expect(component).toMatchSnapshot();
  });
});
