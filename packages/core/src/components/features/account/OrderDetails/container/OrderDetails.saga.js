// 9fbef606107a605d69c0edbcd8029e5d
import { call, takeLatest, put, select } from 'redux-saga/effects';
import { navigateXHRAction } from '@tcp/core/src/components/features/account/NavigateXHR/container/NavigateXHR.action';
import navigateXHRSelectors from '@tcp/core/src/components/features/account/NavigateXHR/container/NavigateXHR.selectors';
import logger from '@tcp/core/src/utils/loggerInstance';
import { getGroupedOrderItems } from '@tcp/core/src/utils/utils';
import { getShipmentStatus } from '@tcp/core/src/services/abstractors/customerHelp/customerHelp';

import { setShipmentStatus } from '@tcp/core/src/components/features/account/CustomerHelp/container/CustomerHelp.actions';
import ORDERDETAILS_CONSTANTS from '../OrderDetails.constants';
import { setOrderDetails, showLoader, hideLoader } from './OrderDetails.actions';
import { setOrderDetailInfo, setError } from '../../TrackOrder/container/TrackOrder.actions';
import { setRecentOrderDetails } from '../../Orders/container/Orders.actions';

import { getOrderInfoByOrderId } from '../../../../../services/abstractors/account/ordersList';
import { getUserEmail, getIsGuest } from '../../User/container/User.selectors';

export function* getOrderDetailsListSaga({ payload }) {
  const { fromCSH } = payload;
  const userEmail = yield select(getUserEmail);
  let isGuest = yield select(getIsGuest);
  const cookieToken = yield select(navigateXHRSelectors.getCookieToken);

  let updatedPayload = { ...payload, ...{ emailAddress: userEmail, isGuest, cookieToken } };
  if (payload.emailAddress) {
    isGuest = true;
    updatedPayload = { ...payload, isGuest, cookieToken };
  }

  try {
    yield put(showLoader());
    const OrderDetailsList = yield call(getOrderInfoByOrderId, updatedPayload);
    let orderDetailsData = OrderDetailsList?.orderDetailsReturn || {};
    let orderItems = [];
    if (fromCSH) {
      const { groupedShoppingBagItems, ...orderData } = orderDetailsData || {};
      const { orderNumber } = orderData || {};
      const shipmentData = yield call(getShipmentStatus, { orderId: orderNumber });
      yield put(setShipmentStatus(shipmentData));
      orderItems = getGroupedOrderItems(groupedShoppingBagItems, shipmentData);
      orderDetailsData = { ...orderData, orderItems };
    }
    yield put(setOrderDetails(orderDetailsData || {}));
    if (payload.isRecentOrder) {
      yield put(setRecentOrderDetails(OrderDetailsList.orderDetailsReturn));
    }
    if (payload.emailAddress) {
      yield put(setOrderDetailInfo(OrderDetailsList.trackOrderInfo));
      yield put(navigateXHRAction());
    }
  } catch (err) {
    logger.error({
      error: err,
      extraData: {
        component: 'OrderDetails Saga - getOrderDetailsListSaga',
        payloadRecieved: payload,
      },
    });
    yield put(setError(err));
  } finally {
    yield put(hideLoader());
  }
}

export function* OrderDetailsListSaga() {
  yield takeLatest(ORDERDETAILS_CONSTANTS.GET_ORDERDETAILS, getOrderDetailsListSaga);
}

export default OrderDetailsListSaga;
