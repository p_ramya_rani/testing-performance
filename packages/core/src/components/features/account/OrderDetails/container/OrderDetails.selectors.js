// 9fbef606107a605d69c0edbcd8029e5d
import { createSelector } from 'reselect';
import { parseBoolean } from '@tcp/core/src/utils';
import { ORDERDETAILS_REDUCER_KEY } from '@tcp/core/src/constants/reducer.constants';
import { getRecentOrderDetailsState } from '../../Orders/container/Orders.selectors';

const getState = (state) => state[ORDERDETAILS_REDUCER_KEY];

export const getOrdersLabels = (state) => {
  return state.Labels && state.Labels.account && state.Labels.account.orders;
};

export const getOrderDetailsDataState = createSelector(
  getState,
  (state) => (state && state.orderDetailsData) || false
);

export const getOrderReceiptReturnDays = (state) =>
  state &&
  state.session &&
  state.session.siteDetails &&
  parseInt(state.session.siteDetails.ORDER_GIFT_RETURN_BY_DAYS, 10);

export const getOrderDetails = (orderState) => {
  const items = [];
  if (orderState) {
    if (orderState.purchasedItems && orderState.purchasedItems.length > 0) {
      orderState.purchasedItems.forEach((item) => {
        items.push(...item.items);
      });
    }
    if (orderState.canceledItems && orderState.canceledItems.length > 0) {
      items.push(...orderState.canceledItems);
    }
    if (orderState.outOfStockItems && orderState.outOfStockItems.length > 0) {
      items.push(...orderState.outOfStockItems);
    }
  }
  return items;
};

export const getAllItems = createSelector(getOrderDetailsDataState, getOrderDetails);

export const getMostRecentOrders = createSelector(getRecentOrderDetailsState, getOrderDetails);

export const getOrderDetailsDataFetchingState = createSelector(
  getState,
  (state) => state && state.isFetching
);

export const getOrderLookUpAPISwitch = (state) => {
  return (
    state.session &&
    state.session.siteDetails &&
    parseBoolean(state.session.siteDetails.IS_ORDERLOOKUP_MICROSERVICE_ACTIVE)
  );
};

export const getIsAppeasementEnabled = (state) => {
  return (
    state.session &&
    state.session.siteDetails &&
    parseBoolean(state.session.siteDetails.IS_APPEASEMENT_FEATURE_ENABLED)
  );
};
