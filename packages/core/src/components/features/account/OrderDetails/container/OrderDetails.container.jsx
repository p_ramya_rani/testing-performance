// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
// import BagPageSelector from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.selectors';
import {
  getGiftReceiptLabels,
  getGiftReceiptReturnDays,
} from '@tcp/core/src/components/features/CnC/common/organism/GiftReceipt/container/GiftReceipt.selectors';
import { isClient } from '@tcp/core/src/utils';
import OrderDetailsView from '../views';
import { getOrderDetails } from './OrderDetails.actions';
import {
  getOrderDetailsDataState,
  getOrderReceiptReturnDays,
  getOrdersLabels,
  getOrderDetailsDataFetchingState,
  getOrderLookUpAPISwitch,
  getIsAppeasementEnabled,
} from './OrderDetails.selectors';
import { getUserLoggedInState, mprUserId } from '../../User/container/User.selectors';
import ORDERDETAILS_CONSTANTS from '../OrderDetails.constants';

export const getOrderDetailsParams = props => {
  let orderId = '';
  let emailAddress = '';
  let fromEpsilon = '';

  if (isClient()) {
    const {
      PATH: { TRACK_ORDER, ORDER_DETAILS },
    } = ORDERDETAILS_CONSTANTS;
    const url = window.location.pathname;
    const trackOrderString = url.split(url.match(TRACK_ORDER) ? TRACK_ORDER : ORDER_DETAILS);
    const trackOrderStringArray = Array.isArray(trackOrderString) && trackOrderString[1];
    const orderInformation = (trackOrderStringArray && trackOrderStringArray.split('/')) || [];
    [orderId = '', emailAddress = '', fromEpsilon = ''] = orderInformation;
  } else {
    const {
      orderId: propsOrderID = '',
      emailAddress: propsEmailAddress = '',
      fromEpsilon: propsFromEpsilon = '',
    } = props;
    orderId = propsOrderID;
    emailAddress = propsEmailAddress;
    fromEpsilon = propsFromEpsilon;
  }

  return {
    orderId,
    emailAddress,
    fromEpsilon,
  };
};

/**
 * This Class component use for return the Order Details data
 * can be passed in the component.
 * @param state - initial state of selectedActivity set to be null
 */
export class OrderDetailsContainer extends PureComponent {
  componentDidMount() {
    this.setOrderUpdate();
  }

  componentDidUpdate(prevProps) {
    const { getOrderDetailsAction, orderDetailsData, orderLookUpAPISwitch } = this.props;

    const { orderId, emailAddress, fromEpsilon } = getOrderDetailsParams(this.props);

    const payload = {
      orderId,
      emailAddress,
      fromEpsilon,
      orderLookUpAPISwitch,
    };

    if (
      prevProps.orderId !== orderId &&
      (orderDetailsData && orderDetailsData.orderNumber !== orderId)
    ) {
      getOrderDetailsAction(payload);
    }
  }

  setOrderUpdate = () => {
    const { getOrderDetailsAction, orderDetailsData, orderLookUpAPISwitch } = this.props;

    const { orderId, emailAddress, fromEpsilon } = getOrderDetailsParams(this.props);

    const payload = {
      orderId,
      emailAddress,
      fromEpsilon,
      orderLookUpAPISwitch,
    };
    if (
      orderId &&
      (!orderDetailsData || (orderDetailsData && orderDetailsData.orderNumber !== orderId))
    ) {
      getOrderDetailsAction(payload);
    }
  };

  /**
   * @function render  Used to render the JSX of the component
   * @param    {[Void]} function does not accept anything.
   * @return   {[Object]} JSX of the component
   */

  render() {
    const {
      orderDetailsData,
      ordersLabels,
      navigation,
      isFetching,
      isLoggedIn,
      userAccountId,
      giftReceiptLabels,
      returnDays,
    } = this.props;
    const { orderId } = getOrderDetailsParams(this.props);
    return (
      <OrderDetailsView
        {...this.props}
        orderDetailsData={orderDetailsData}
        ordersLabels={ordersLabels}
        orderId={orderId}
        navigation={navigation}
        isFetching={isFetching}
        isLoggedIn={isLoggedIn}
        returnDays={returnDays}
        giftReceiptLabels={giftReceiptLabels}
        userAccountId={userAccountId}
      />
    );
  }
}

export const mapDispatchToProps = dispatch => {
  return {
    getOrderDetailsAction: payload => {
      dispatch(getOrderDetails(payload));
    },
  };
};

export const mapStateToProps = (state, ownProps) => {
  return {
    orderId: ownProps.router.query.orderId,
    emailAddress: ownProps.router.query.emailAddress,
    fromEpsilon: ownProps.router.query.fromEpsilon,
    orderDetailsData: getOrderDetailsDataState(state),
    ordersLabels: getOrdersLabels(state),
    orderReturnDays: getOrderReceiptReturnDays(state),
    isFetching: getOrderDetailsDataFetchingState(state),
    isLoggedIn: getUserLoggedInState(state),
    userAccountId: mprUserId(state),
    returnDays: getGiftReceiptReturnDays(state),
    giftReceiptLabels: getGiftReceiptLabels(state),
    orderLookUpAPISwitch: getOrderLookUpAPISwitch(state),
    isAppeasementEnabled: getIsAppeasementEnabled(state),
  };
};

OrderDetailsContainer.propTypes = {
  emailAddress: PropTypes.string,
  orderId: PropTypes.string,
  orderDetailsData: PropTypes.shape({}),
  ordersLabels: PropTypes.shape({}),
  getOrderDetailsAction: PropTypes.func.isRequired,
  navigation: PropTypes.shape({}),
  isFetching: PropTypes.bool,
  userAccountId: PropTypes.string,
  isLoggedIn: PropTypes.bool,
  orderLookUpAPISwitch: PropTypes.bool,
  returnDays: PropTypes.number,
  giftReceiptLabels: PropTypes.shape({}),
  fromEpsilon: PropTypes.string,
};

OrderDetailsContainer.defaultProps = {
  emailAddress: '',
  orderId: '',
  ordersLabels: {},
  orderDetailsData: {},
  navigation: {},
  isFetching: false,
  isLoggedIn: false,
  orderLookUpAPISwitch: false,
  userAccountId: '',
  returnDays: 45,
  giftReceiptLabels: {},
  fromEpsilon: '',
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OrderDetailsContainer);
