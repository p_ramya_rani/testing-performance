// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import {
  OrderDetailsContainer,
  mapDispatchToProps,
  getOrderDetailsParams,
} from '../OrderDetails.container';
import OrderDetailsData from '../../views/OrderDetails.view';

const URLs = {
  trackOrder: '/us/track-order/600526999/9DqYVhAKLcEAdvaHMbyvO1qt8I3t%2BJbJvme7zVC7uPQ%3D',
  orderDetails: '/us/account/orders/order-details/6005269105',
};
describe('Order Details container', () => {
  beforeAll(() => {
    delete window.location;
    window.location = {
      pathname: '',
    };
  });

  const props = {
    ordersLabels: {},
  };
  it('should render Order Details component', () => {
    const component = shallow(
      <OrderDetailsContainer getOrderDetailsAction={() => {}} {...props} />
    );
    expect(component.is(OrderDetailsData)).toBeTruthy();
  });

  it('should return an action getOrderDetailsAction which will call dispatch function on execution', () => {
    const dispatch = jest.fn();
    const dispatchProps = mapDispatchToProps(dispatch);
    dispatchProps.getOrderDetailsAction();
    expect(dispatch.mock.calls).toHaveLength(1);
  });

  it('should parse order-detail URL', () => {
    window.location.pathname = URLs.orderDetails;
    const response = {
      orderId: '6005269105',
      emailAddress: '',
      fromEpsilon: '',
    };
    expect(getOrderDetailsParams(null)).toEqual(response);
  });

  it('should parse track-order URL', () => {
    window.location.pathname = URLs.trackOrder;

    const response = {
      orderId: '600526999',
      emailAddress: '9DqYVhAKLcEAdvaHMbyvO1qt8I3t%2BJbJvme7zVC7uPQ%3D',
      fromEpsilon: '',
    };
    expect(getOrderDetailsParams(null)).toEqual(response);
  });

  it('should parse detail from props', () => {
    delete global.window; /** For mobile app and server test */
    const prop = {
      orderId: '123456',
      emailAddress: 'xyz@yopmail.com',
      fromEpsilon: 'true',
    };
    expect(getOrderDetailsParams(prop)).toEqual(prop);
  });
});
