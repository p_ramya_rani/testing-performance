// 9fbef606107a605d69c0edbcd8029e5d 
import LOGOUT_CONSTANTS from '@tcp/core/src/components/features/account/Logout/LogOut.constants';
import ORDERDETAILS_CONSTANTS from '../OrderDetails.constants';

const initialState = {
  orderDetailsData: null,
  isFetching: false,
  isOrderResendModalOpen: false,
};

const OrderDetailsDataReducer = (state = initialState, action) => {
  switch (action.type) {
    case ORDERDETAILS_CONSTANTS.GET_ORDERDETAILS:
      return {
        isFetching: true,
        orderDetailsData: null,
      };
    case ORDERDETAILS_CONSTANTS.SHOW_LOADER:
      Object.assign(state, {
        isFetching: true,
      });
      return { ...state };
    case ORDERDETAILS_CONSTANTS.SET_ORDERDETAILS:
      return { ...state, ...{ orderDetailsData: action.payload } };
    case ORDERDETAILS_CONSTANTS.HIDE_LOADER:
      return { ...state, isFetching: false };
    case LOGOUT_CONSTANTS.LOGOUT_APP:
      return { ...initialState };
    case ORDERDETAILS_CONSTANTS.RESEND_ORDER_CONFIRMATION_MODAL:
      return { ...state, isOrderResendModalOpen: action.payload };
    default:
      return state;
  }
};

export default OrderDetailsDataReducer;
