// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';
import { Anchor } from '@tcp/core/src/components/common/atoms';

const OrderDetailsMainView = styled.View`
  border-bottom-color: ${(props) => props.theme.colorPalette.gray[500]};
  border-bottom-width: 1px;
`;

const OrdersNumberWrapper = styled.View`
  width: 33%;
`;
const ContentWrapper = styled.View`
  display: flex;
  flex-direction: row;
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
`;

const ContentCol = styled.View`
  display: flex;
  flex-direction: column;
  margin-left: 10px;
`;

const ContentRow = styled.View`
  display: flex;
  flex-direction: row;
  margin-top: 5px;
  align-items: center;
  margin-left: 33px;
`;

const OrderGroupWrapper = styled.View`
  border-bottom: solid 1px ${(props) => props.theme.colorPalette.gray[500]};
`;

const StyledAnchor = styled(Anchor)`
  background-color: ${(props) => props.theme.colors.BLACK};
  padding: 9px 20px;
  align-items: center;
  border-radius: 6px;
  overflow: hidden;
  text-transform: uppercase;
  font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy2}px;
`;

export {
  OrderDetailsMainView,
  OrdersNumberWrapper,
  ContentWrapper,
  OrderGroupWrapper,
  StyledAnchor,
  ContentCol,
  ContentRow,
};
