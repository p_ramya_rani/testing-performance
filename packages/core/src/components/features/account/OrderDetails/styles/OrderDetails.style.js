// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

export default css`
  .elem-margin-right {
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-right: 0;
    }
  }
  .order-status-header {
    @media ${(props) => props.theme.mediaQuery.large} {
      padding-top: ${(props) => props.theme.spacing.LAYOUT_SPACING.XXS};
    }
  }
  .group-row {
    border-bottom: solid 1px ${(props) => props.theme.colorPalette.gray[500]};
    padding-bottom: ${(props) => props.theme.spacing.LAYOUT_SPACING.SM};
    margin-bottom: ${(props) => props.theme.spacing.LAYOUT_SPACING.XS};
    @media ${(props) => props.theme.mediaQuery.medium} {
      padding-bottom: ${(props) => props.theme.spacing.LAYOUT_SPACING.MED};
      margin-bottom: ${(props) => props.theme.spacing.LAYOUT_SPACING.XXS};
      padding-top: ${(props) => props.theme.spacing.LAYOUT_SPACING.XXS};
    }
  }
  .button-container {
    text-align: left;
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      margin-top: ${(props) => props.theme.spacing.LAYOUT_SPACING.XS};
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-top: 0px;
      text-align: right;
    }
  }
  .button-track {
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      width: 40%;
    }
  }
  .button-track-rounded {
    margin-top: -5px;
    text-align: right;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-top: 5px;
      float: left;
      width: 107px;
      margin-left: 36px;
      text-align: left;
    }
  }
  .track-order-cta {
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    padding: 10px 30px;
    border-radius: 6px;
    background-color: ${(props) => props.theme.colors.PRIMARY.DARK};
    text-transform: uppercase;
    color: ${(props) => props.theme.colors.WHITE};
    cursor: pointer;
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy2}px;
    font-weight: ${(props) => props.theme.fonts.fontWeight.extrabold};
    min-height: 0;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      font-size: 10px;
      padding: 8px 26px;
    }
  }
  .orderDetail-trackingNumber {
    display: block;
    margin-top: ${(props) => props.theme.spacing.APP_LAYOUT_SPACING.XXS};
    @media ${(props) => props.theme.mediaQuery.medium} {
      display: inline;
      margin-top: 0;
    }
  }

  .orderDetail-trackingId {
    display: block;
    font-size: 14px;
    margin-bottom: ${(props) => props.theme.spacing.APP_LAYOUT_SPACING.XXS};
    text-align: right;
    margin-top: ${(props) => props.theme.spacing.APP_LAYOUT_SPACING.XXS};

    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      text-align: left;
      font-size: 12px;
      margin-left: 10px;
    }
    a {
      display: inline-block;
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        margin-left: 10px;
      }
      @media ${(props) => props.theme.mediaQuery.medium} {
        margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
      }
    }
  }

  .orderDetail-trackingId-lbl {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      display: none;
    }
  }

  .order-return-details {
    display: block;
  }

  .order-Item {
    padding-top: ${(props) => props.theme.spacing.LAYOUT_SPACING.SM};
  }
  .margin-tablet {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-top: 0;
    }
  }
  .margin-mobile {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    @media ${(props) => props.theme.mediaQuery.medium} {
      margin-top: 0;
    }
  }

  .back-from-receipt {
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    color: rgb(46, 106, 145);
    font-size: 16px;
    margin-bottom: 24px;
    border: none;
    background: none;
    cursor: pointer;
    outline: none;
    min-height: auto;
    letter-spacing: normal;
    text-transform: none;
  }
  .back-from-receipt .left-arrow {
    border: solid ${(props) => props.theme.colors.ANCHOR.SECONDARY};
    border-width: 0 2px 2px 0;
    display: inline-block;
    padding: 5px;
    transform: rotate(135deg);
  }
  .returned-info {
    background: ${(props) => props.theme.colorPalette.gray[300]};
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    padding: 9px;
    max-width: 364px;
    width: 100%;
    margin-left: 146px;
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy2}px;
    box-sizing: border-box;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG_1};
      margin-left: 0;
    }
    .returned-status {
      font-weight: ${(props) => props.theme.fonts.fontWeight.bold};
      text-decoration: underline;
    }
    .refund-amount {
      font-weight: ${(props) => props.theme.fonts.fontWeight.bold};
    }
    .refunded {
      color: ${(props) => props.theme.colorPalette.green[500]};
    }
  }
  .order-status-container {
    display: flex;
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    position: relative;
    width: 100%;
  }
  .delivered {
    color: ${(props) => props.theme.colorPalette.green[500]};
  }
  .order-status-logo {
    height: 38px;
    width: 38px;
    align-self: flex-start;
    display: flex;
    margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      height: 24px;
      width: 24px;
    }
    img {
      width: 100%;
    }
  }
  .order-status-text {
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy5}px;
    flex: 1;
    display: flex;
    flex-wrap: wrap;
    flex-direction: column;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy4}px;
    }
    p {
      display: flex;
      align-items: center;
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        display: inline-block;
        margin-right: 8px;
        flex: auto;
      }
    }
  }

  .order-status-cta {
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy4}px;
    flex: 1;
    display: flex;
    flex-wrap: wrap;
    flex-direction: column;
    text-align: right;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy4}px;
      text-align: center;
    }
  }

  .shipment-count {
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    font-weight: ${(props) => props.theme.fonts.fontWeight.normal};
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy3}px;
    color: ${(props) => props.theme.colors.TEXT.DARKERGRAY};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy2}px;
    }
  }
  .cancelled-status {
    color: ${(props) => props.theme.colors.NOTIFICATION.ERROR};
    text-transform: capitalize;
    font-weight: ${(props) => props.theme.fonts.fontWeight.bold};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy4}px;
    }
  }
  .success-status {
    color: ${(props) => props.theme.colorPalette.green[500]};
    text-transform: capitalize;
    font-weight: ${(props) => props.theme.fonts.fontWeight.bold};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy4}px;
    }
  }
  .default {
    text-transform: capitalize;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy4}px;
    }
  }
  .no-text-transform {
    text-transform: none;
  }
  .appeasment-info {
    background: ${(props) => props.theme.colorPalette.gray[300]};
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    padding: 9px;
    max-width: 364px;
    margin-left: 25%;
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy2}px;
    box-sizing: border-box;
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      margin-left: 38%;
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-left: 0;
      max-width: 100%;
    }
  }
`;
