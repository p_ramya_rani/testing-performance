// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable sonarjs/no-duplicate-string */
import React from 'react';
import { shallow } from 'enzyme';
import { getViewportInfo } from '@tcp/core/src/utils';
import { OrderDetailsViewVanilla } from '../OrderDetails.view';
import {
  renderCancelledAndOutOfStockOrders,
  renderOrderDetailWithCRMView,
} from '../../organism/OrderDetailsWrapper';
import constants from '../../OrderDetails.constants';

jest.mock('@tcp/core/src/utils', () => ({
  getViewportInfo: jest.fn().mockImplementation(() => {
    return { isDesktop: true };
  }),
  getAPIConfig: () => ({
    brandId: 'tcp',
  }),
  isCanada: jest.fn(),
  isClient: jest.fn(),
  getStaticFilePath: jest.fn,
  getIconPath: jest.fn(),
  isMobileApp: jest.fn(),
  getSiteId: jest.fn(),
}));

jest.mock(
  '@tcp/core/src/components/features/account/OrderDetails/organism/OrderDetailsWrapper',
  () => ({
    ...jest.requireActual(
      '@tcp/core/src/components/features/account/OrderDetails/organism/OrderDetailsWrapper'
    ),
    renderOrderDetailWithCRMView: jest.fn(),
  })
);

renderOrderDetailWithCRMView.mockImplementation(() => {});

describe('Order Summary Details component', () => {
  it('should renders correctly', () => {
    const props = {
      OrderDetailsData: {
        orderStatus: constants.STATUS_CONSTANTS.ORDER_RECEIVED,
        shippedDate: '2019-10-09',
        trackingNumber: '123455',
        trackingUrl: '/test',
        orderNumber: '654321',
        purchasedItems: [
          {
            items: [
              {
                itemInfo: {
                  itemBrand: 'TCP',
                  linePrice: 24.94,
                  listPrice: 24.94,
                  offerPrice: 24.94,
                  quantity: 1,
                  quantityCanceled: 0,
                  quantityOOS: 0,
                  quantityReturned: 0,
                  quantityShipped: 0,
                },
              },
            ],
          },
        ],
        summary: {
          currencySymbol: '$',
        },
        isBopisOrder: false,
        ordersLabels: {},
        orderType: 'ECOM',
      },
    };

    const component = shallow(<OrderDetailsViewVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders BOSSOrder and Bopis orders', () => {
    const props = {
      OrderDetailsData: {
        orderStatus: constants.STATUS_CONSTANTS.ORDER_RECEIVED,
        pickedUpDate: '2019-10-09',
        purchasedItems: [
          {
            items: [
              {
                itemInfo: {
                  itemBrand: 'TCP',
                  linePrice: 24.94,
                  listPrice: 24.94,
                  offerPrice: 24.94,
                  quantity: 1,
                  quantityCanceled: 0,
                  quantityOOS: 0,
                  quantityReturned: 0,
                  quantityShipped: 0,
                },
              },
            ],
          },
        ],
        summary: {
          currencySymbol: '$',
        },
        isBopisOrder: true,
        ordersLabels: {},
      },
    };

    const component = shallow(<OrderDetailsViewVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders Out of Stock items', () => {
    const props = {
      OrderDetailsData: {
        outOfStockItems: [
          {
            itemInfo: {
              itemBrand: 'TCP',
              linePrice: 24.94,
              listPrice: 24.94,
              offerPrice: 24.94,
              quantity: 1,
              quantityCanceled: 0,
              quantityOOS: 0,
              quantityReturned: 0,
              quantityShipped: 0,
            },
          },
        ],
        summary: {
          currencySymbol: '$',
        },
        isBopisOrder: false,
        ordersLabels: {},
      },
    };

    const component = shallow(<OrderDetailsViewVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders CanceledItems items', () => {
    const props = {
      OrderDetailsData: {
        canceledItems: [
          {
            itemInfo: {
              itemBrand: 'TCP',
              linePrice: 24.94,
              listPrice: 24.94,
              offerPrice: 24.94,
              quantity: 1,
              quantityCanceled: 0,
              quantityOOS: 0,
              quantityReturned: 0,
              quantityShipped: 0,
            },
          },
        ],
        summary: {
          currencySymbol: '$',
        },
        isBopisOrder: false,
        ordersLabels: {},
        isLoggedIn: true,
      },
    };

    const component = shallow(<OrderDetailsViewVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders order with barcode correctly', () => {
    const props = {
      OrderDetailsData: {
        orderStatus: constants.STATUS_CONSTANTS.ORDER_RECEIVED,
        shippedDate: '2020-05-19',
        trackingNumber: '123455',
        trackingUrl: '/test',
        purchasedItems: [
          {
            items: [
              {
                itemInfo: {
                  itemBrand: 'TCP',
                  linePrice: 24.94,
                  listPrice: 24.94,
                  offerPrice: 24.94,
                  quantity: 1,
                  quantityCanceled: 0,
                  quantityOOS: 0,
                  quantityReturned: 0,
                  quantityShipped: 0,
                },
              },
            ],
          },
        ],
        summary: {
          currencySymbol: '$',
        },
        isBopisOrder: false,
        ordersLabels: {},
        orderType: 'ECOM',
      },
    };

    getViewportInfo.mockImplementation(() => {
      return { isDesktop: false };
    });
    const component = shallow(<OrderDetailsViewVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should render orders receipt correctly', () => {
    const props = {
      receiptsView: true,
      OrderDetailsData: {
        orderStatus: constants.STATUS_CONSTANTS.ORDER_RECEIVED,
        shippedDate: '2019-10-09',
        trackingNumber: '123455',
        trackingUrl: '/test',
        orderNumber: '654321',
        purchasedItems: [
          {
            items: [
              {
                itemInfo: {
                  itemBrand: 'TCP',
                  linePrice: 24.94,
                  listPrice: 24.94,
                  offerPrice: 24.94,
                  quantity: 1,
                  quantityCanceled: 0,
                  quantityOOS: 0,
                  quantityReturned: 0,
                  quantityShipped: 0,
                },
              },
            ],
          },
        ],
        summary: {
          currencySymbol: '$',
        },
        isBopisOrder: false,
        ordersLabels: {},
        orderType: 'ECOM',
      },
    };

    const component = shallow(<OrderDetailsViewVanilla {...props} />);
    expect(component.find('.order-receipt-container')).toHaveLength(1);
    expect(component).toMatchSnapshot();
  });
  it('should render orders details correctly without order data', () => {
    const props = {
      receiptsView: true,
      OrderDetailsData: null,
    };

    const component = shallow(<OrderDetailsViewVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders Returned items', () => {
    const props = {
      OrderDetailsData: {
        orderStatus: constants.STATUS_CONSTANTS.ORDER_RECEIVED,
        shippedDate: '2019-10-09',
        trackingNumber: '123455',
        trackingUrl: '/test',
        orderNumber: '654321',
        returnedItems: [
          {
            items: [
              {
                itemInfo: {
                  itemBrand: 'TCP',
                  linePrice: 24.94,
                  listPrice: 24.94,
                  offerPrice: 24.94,
                  quantity: 1,
                  quantityCanceled: 0,
                  quantityOOS: 0,
                  quantityReturned: 0,
                  quantityShipped: 0,
                },
              },
            ],
          },
        ],
        summary: {
          currencySymbol: '$',
        },
        isBopisOrder: false,
        ordersLabels: {},
        orderType: 'ECOM',
      },
    };

    const component = shallow(<OrderDetailsViewVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('renderCancelledAndOutOfStockOrders should be defined for outofstockorders', () => {
    const cancelledItemsComponent = renderCancelledAndOutOfStockOrders([], {}, '', '', '', false);
    expect(cancelledItemsComponent).toBeDefined();
    expect(cancelledItemsComponent).toMatchSnapshot();
  });

  it('renderCancelledAndOutOfStockOrders should be defined for canceled orders', () => {
    const cancelledItemsComponent = renderCancelledAndOutOfStockOrders([], {}, '', '', '', true);
    expect(cancelledItemsComponent).toBeDefined();
    expect(cancelledItemsComponent).toMatchSnapshot();
  });

  it('should not render renderOrderDetailWithCRMView', () => {
    const props = {
      orderDetailsData: {
        orderStatus: constants.STATUS_CONSTANTS.ORDER_RECEIVED,
        shippedDate: '2019-10-09',
        trackingNumber: '123455',
        trackingUrl: '/test',
        orderNumber: '654321',
        purchasedItems: [
          {
            items: [
              {
                itemInfo: {
                  itemBrand: 'TCP',
                  linePrice: 24.94,
                  listPrice: 24.94,
                  offerPrice: 24.94,
                  quantity: 1,
                  quantityCanceled: 0,
                  quantityOOS: 0,
                  quantityReturned: 0,
                  quantityShipped: 0,
                },
                shipmentAndStatusInfo: [
                  {
                    carrier: 'UPS',
                    quantity: 1,
                    shipDate: null,
                    shipVia: null,
                    status: 'shipped',
                    trackingUrl:
                      'https://childrensplace.narvar.com/childrensplace/tracking/ups?order_number=343731003&order_date=2021-04-07T23:37:37-04:00&dzip=43054&locale=en_US&type=p',
                    trackingNbr: null,
                    refundAmount: 0,
                    refundDate: null,
                    appeasement: 0,
                  },
                ],
              },
            ],
          },
        ],
        summary: {
          currencySymbol: '$',
        },
        isBopisOrder: false,
        ordersLabels: {},
        orderType: 'BOPIS',
      },
      isFetching: false,
      orderLookUpAPISwitch: false,
    };

    const component = shallow(<OrderDetailsViewVanilla {...props} />);
    expect(renderOrderDetailWithCRMView).toHaveBeenCalledTimes(0);
    expect(component).toMatchSnapshot();
  });

  it('should render renderOrderDetailWithCRMView', () => {
    const props = {
      orderDetailsData: {
        orderStatus: constants.STATUS_CONSTANTS.ORDER_RECEIVED,
        shippedDate: '2019-10-09',
        trackingNumber: '123455',
        trackingUrl: '/test',
        orderNumber: '654321',
        purchasedItems: [
          {
            items: [
              {
                itemInfo: {
                  itemBrand: 'TCP',
                  linePrice: 24.94,
                  listPrice: 24.94,
                  offerPrice: 24.94,
                  quantity: 1,
                  quantityCanceled: 0,
                  quantityOOS: 0,
                  quantityReturned: 0,
                  quantityShipped: 0,
                },
                shipmentAndStatusInfo: [
                  {
                    carrier: 'UPS',
                    quantity: 1,
                    shipDate: null,
                    shipVia: null,
                    status: 'shipped',
                    trackingUrl:
                      'https://childrensplace.narvar.com/childrensplace/tracking/ups?order_number=343731003&order_date=2021-04-07T23:37:37-04:00&dzip=43054&locale=en_US&type=p',
                    trackingNbr: null,
                    refundAmount: 0,
                    refundDate: null,
                    appeasement: 0,
                  },
                ],
              },
            ],
          },
        ],
        summary: {
          currencySymbol: '$',
        },
        isBopisOrder: false,
        ordersLabels: {},
        orderType: 'ECOM',
      },
      isFetching: false,
      orderLookUpAPISwitch: true,
    };

    const component = shallow(<OrderDetailsViewVanilla {...props} />);
    expect(renderOrderDetailWithCRMView).toHaveBeenCalled();
    expect(component).toMatchSnapshot();
  });
});
