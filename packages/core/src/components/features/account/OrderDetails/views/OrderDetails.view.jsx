// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Button } from '@tcp/core/src/components/common/atoms';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import Anchor from '@tcp/core/src/components/common/atoms/Anchor';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import config from '@tcp/core/src/config/orderConfig';
import {
  getLabelValue,
  isValidOrderForReceipts,
  convertToISOString,
} from '@tcp/core/src/utils/utils';
import Barcode from '@tcp/core/src/components/common/molecules/Barcode';
import { getViewportInfo } from '@tcp/core/src/utils';
import PrintComponent from '@tcp/core/src/components/common/atoms/PrintComponent';
import GiftReceipt from '@tcp/core/src/components/features/CnC/common/organism/GiftReceipt';
import HelpWithOrder from '@tcp/core/src/components/common/organisms/HelpWithOrder/container/HelpWithOrder.container';
import styles from '../styles/OrderDetails.style';
import OrderBasicDetails from '../organism/OrderBasicDetails';
import OrderShippingDetails from '../organism/OrderShippingDetails';
import OrderBillingDetails from '../organism/OrderBillingDetails';
import OrderSummaryDetails from '../organism/OrderSummaryDetails';
import OrderItemsList from '../organism/OrderItemsList';
import OrderStatus from '../organism/OrderStatus';
import OrderReceipt from '../organism/OrderReceipt';
import constants from '../OrderDetails.constants';
import internalEndpoints from '../../common/internalEndpoints';
import FormPageHeadingComponent from '../../common/molecule/FormPageHeading';
import OrderDetailsSkeleton from '../skeleton/OrderDetailsSkeleton.view';
import {
  renderOrderDetailWithCRMView,
  renderReturnedAndRefundedOrders,
  renderCancelledAndOutOfStockOrders,
  renderBopisAndBossOrder,
} from '../organism/OrderDetailsWrapper';

/**
 * This function component use for rendering Cancelled and Out of Stock orders
 * can be passed in the component.
 * @param orderDetailsData - orderDetailsData object used for showing Order Details
 * @param ordersLabels - ordersLabels object used for labels
 */
const renderOrderDetailView = (
  orderDetailsData,
  ordersLabels,
  toggleReceiptsView,
  isAppeasementEnabled
) => {
  const {
    summary,
    orderStatus,
    purchasedItems,
    outOfStockItems,
    isBossOrder,
    orderNumber,
    canceledItems,
    isBopisOrder,
    emailAddress,
    encryptedEmailAddress,
    returnedItems,
    orderDate,
  } = orderDetailsData || {};

  const { currencySymbol } = summary || {};
  const notificationHeader = isBossOrder
    ? getLabelValue(ordersLabels, 'lbl_orders_noLongerAvailable')
    : getLabelValue(ordersLabels, 'lbl_orders_canceledItems');
  const notificationMessage = isBossOrder
    ? getLabelValue(ordersLabels, 'lbl_orders_isBossOrderCancelNotification')
    : getLabelValue(ordersLabels, 'lbl_orders_CancelNotification');
  const { isDesktop } = getViewportInfo();
  return (
    orderDetailsData && (
      <>
        <Row fullBleed className="elem-mt-XL">
          <Col colSize={{ large: 6, medium: 4, small: 6 }}>
            <Row fullBleed>
              <Col colSize={{ large: 6, medium: 8, small: 6 }}>
                {!isDesktop && (
                  <BodyCopy component="div" className="orderdetails-barcode" textAlign="center">
                    <Barcode
                      className="barcode"
                      value={orderNumber}
                      barcodeId={orderNumber}
                      height={75}
                      displayValue
                    />
                  </BodyCopy>
                )}
              </Col>
            </Row>
            <Row fullBleed>
              <Col colSize={{ large: 6, medium: 8, small: 6 }}>
                <OrderBasicDetails
                  orderDetailsData={orderDetailsData}
                  ordersLabels={ordersLabels}
                />
              </Col>
              <Col colSize={{ large: 6, medium: 8, small: 6 }} className="margin-tablet">
                <OrderShippingDetails
                  orderDetailsData={orderDetailsData}
                  ordersLabels={ordersLabels}
                />
              </Col>
            </Row>
          </Col>
          <Col colSize={{ large: 6, medium: 4, small: 6 }}>
            <Row fullBleed>
              <Col colSize={{ large: 5, medium: 8, small: 6 }} className="margin-mobile">
                <OrderBillingDetails
                  orderDetailsData={orderDetailsData}
                  ordersLabels={ordersLabels}
                />
              </Col>
              <Col colSize={{ large: 7, medium: 8, small: 6 }} className="margin-tablet">
                <OrderSummaryDetails
                  orderDetailsData={orderDetailsData}
                  ordersLabels={ordersLabels}
                  isAppeasementEnabled={isAppeasementEnabled}
                />
                <HelpWithOrder
                  orderId={orderNumber}
                  trackingEmail={emailAddress}
                  encryptedEmailAddress={encryptedEmailAddress}
                  toggleReceiptsView={toggleReceiptsView}
                  orderDate={convertToISOString(orderDate)}
                  orderDetailsData={orderDetailsData}
                />
              </Col>
            </Row>
          </Col>
        </Row>
        <div className="elem-mt-XL">
          {orderDetailsData.orderType === config.ORDER_ITEM_TYPE.ECOM &&
            purchasedItems &&
            purchasedItems.length > 0 &&
            purchasedItems.map((orderGroup, index) => (
              <Row fullBleed className="group-row">
                <OrderStatus
                  status={orderGroup.orderStatus}
                  trackingNumber={orderGroup.trackingNumber}
                  trackingUrl={orderGroup.trackingUrl}
                  shippedDate={orderGroup.shippedDate}
                  isBopisOrder={isBopisOrder}
                  ordersLabels={ordersLabels}
                />

                <Col colSize={{ large: 12, medium: 8, small: 6 }}>
                  <OrderItemsList
                    key={index.toString()}
                    ordersLabels={ordersLabels}
                    items={orderGroup.items}
                    currencySymbol={currencySymbol}
                    isEcom
                    isShowWriteReview={
                      orderGroup.orderStatus === constants.STATUS_CONSTANTS.ORDER_SHIPPED ||
                      orderStatus === constants.STATUS_CONSTANTS.ORDER_PARTIALLY_SHIPPED
                    }
                    isAppeasementEnabled={isAppeasementEnabled}
                  />
                </Col>
              </Row>
            ))}
          {renderBopisAndBossOrder(orderDetailsData, ordersLabels)}
          {renderCancelledAndOutOfStockOrders(
            outOfStockItems,
            ordersLabels,
            currencySymbol,
            getLabelValue(ordersLabels, 'lbl_orders_outOfStock'),
            getLabelValue(ordersLabels, 'lbl_orders_outOfStockNotification')
          )}
          {renderCancelledAndOutOfStockOrders(
            canceledItems,
            ordersLabels,
            currencySymbol,
            notificationHeader,
            notificationMessage,
            { isCanceledList: true }
          )}
          {renderReturnedAndRefundedOrders(
            returnedItems,
            ordersLabels,
            currencySymbol,
            getLabelValue(ordersLabels, 'lbl_orders_returned'),
            getLabelValue(ordersLabels, 'lbl_orders_returnedNotification')
          )}
        </div>
      </>
    )
  );
};

/**
 * This function component use for return the OrderDetailsView
 * can be passed in the component.
 * @param orderDetailsData - orderDetailsData object used for showing Order Details
 */
class OrderDetailsView extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      receiptsView: props.receiptsView || false,
    };
  }

  toggleReceiptsView = (receiptsView) => {
    this.setState({ receiptsView });
  };

  // eslint-disable-next-line complexity
  render() {
    const {
      orderDetailsData,
      className,
      ordersLabels,
      isFetching,
      isLoggedIn,
      orderReturnDays,
      userAccountId,
      orderLookUpAPISwitch: displayUiWithStatusLogo,
      isAppeasementEnabled,
    } = this.props;
    const { receiptsView } = this.state;
    const { orderNumber = '', orderDate, orderStatus } = orderDetailsData || {};
    return (
      <div className={className}>
        {!receiptsView && isLoggedIn && (
          <BodyCopy component="div" className="elem-mb-LRG">
            <Anchor
              to={internalEndpoints.myOrderPage.link}
              asPath={internalEndpoints.myOrderPage.path}
              fontSizeVariation="xlarge"
              anchorVariation="secondary"
              data-locator="backLink"
            >
              <span className="left-arrow" />
              {getLabelValue(ordersLabels, 'lbl_orderDetails_back')}
            </Anchor>
          </BodyCopy>
        )}

        {receiptsView && (
          <Button className="back-from-receipt" onClick={() => this.toggleReceiptsView(false)}>
            <span className="left-arrow" />
            {getLabelValue(ordersLabels, 'lbl_orderDetails_back')}
          </Button>
        )}

        {!receiptsView && (
          <FormPageHeadingComponent
            heading={getLabelValue(ordersLabels, 'lbl_orderDetails_heading')}
            className="myAccountRightView"
          />
        )}

        {receiptsView && (
          <FormPageHeadingComponent heading="RECEIPTS" className="myAccountRightView" />
        )}

        {isValidOrderForReceipts({ orderNumber, orderStatus }) && (
          <PrintComponent
            ComponentToPrint={GiftReceipt}
            compProps={{
              orderNumbers: [orderNumber, orderNumber, orderNumber],
              orderDate,
            }}
            printTriggerText={getLabelValue(ordersLabels, 'lbl_orders_gift_receipt_print_text')}
            displayContentBeforePrint={false}
          />
        )}

        {!receiptsView && isFetching && <OrderDetailsSkeleton ordersLabels={ordersLabels} />}
        {!receiptsView &&
          !isFetching &&
          !displayUiWithStatusLogo &&
          renderOrderDetailView(
            orderDetailsData,
            ordersLabels,
            this.toggleReceiptsView,
            isAppeasementEnabled
          )}
        {!receiptsView &&
          !isFetching &&
          displayUiWithStatusLogo &&
          renderOrderDetailWithCRMView(
            orderDetailsData,
            ordersLabels,
            this.toggleReceiptsView,
            isAppeasementEnabled
          )}
        {receiptsView && orderDetailsData && (
          <OrderReceipt
            className="order-receipt-container"
            orderDetailsData={orderDetailsData}
            orderReturnDays={orderReturnDays}
            ordersLabels={ordersLabels}
            userAccountId={userAccountId}
            isLoggedIn={isLoggedIn}
            toggleReceiptsView={this.toggleReceiptsView}
          />
        )}
      </div>
    );
  }
}

OrderDetailsView.propTypes = {
  className: PropTypes.string,
  orderDetailsData: PropTypes.shape({}),
  ordersLabels: PropTypes.shape({}),
  orderReturnDays: PropTypes.number,
  userAccountId: PropTypes.string,
  isLoggedIn: PropTypes.bool,
  isFetching: PropTypes.bool,
  receiptsView: PropTypes.bool,
  orderLookUpAPISwitch: PropTypes.bool,
  isAppeasementEnabled: PropTypes.bool,
};

OrderDetailsView.defaultProps = {
  className: '',
  ordersLabels: {},
  orderDetailsData: {},
  orderReturnDays: 0,
  userAccountId: '',
  isLoggedIn: false,
  isFetching: false,
  receiptsView: false,
  orderLookUpAPISwitch: false,
  isAppeasementEnabled: false,
};

export default withStyles(OrderDetailsView, styles);
export { OrderDetailsView as OrderDetailsViewVanilla };
