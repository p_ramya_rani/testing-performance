// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { ScrollView, View } from 'react-native';
import PropTypes from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { getLabelValue, convertToISOString } from '@tcp/core/src/utils/utils';
import { ViewWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';
import config from '@tcp/core/src/config/orderConfig';
import Barcode from '@tcp/core/src/components/common/molecules/Barcode';
import ResendOrderConfirmation from '@tcp/core/src/components/common/organisms/ResendOrderConfirmation/container/ResendOrderConfirmation.container';
import HelpWithOrder from '@tcp/core/src/components/common/organisms/HelpWithOrder/container/HelpWithOrder.container';
import OrderBasicDetails from '../organism/OrderBasicDetails';
import OrderShippingDetails from '../organism/OrderShippingDetails';
import OrderBillingDetails from '../organism/OrderBillingDetails';
import OrderSummaryDetails from '../organism/OrderSummaryDetails';
import OrderItemsList from '../organism/OrderItemsList';
import OrderStatus from '../organism/OrderStatus';
import constants from '../OrderDetails.constants';
import OrderDetailsSkeleton from '../skeleton/OrderDetailsSkeleton.view';
import {
  renderOrderDetailWithCRMView,
  renderReturnedAndRefundedOrders,
  renderCancelledAndOutOfStockOrders,
  renderBopisAndBossOrder,
} from '../organism/OrderDetailsWrapper';

/**
 * This function component use for rendering Cancelled and Out of Stock orders
 * can be passed in the component.
 * @param orderDetailsData - orderDetailsData object used for showing Order Details
 * @param purchasedItems - Get purchased items array
 * @param isBopisOrder - check if it is BOPIS
 * @param ordersLabels - for all order labels
 * @param currencySymbol - for getting order currency symbol
 * @param orderStatus - for getting order status
 */

const RenderOrderDetailView = (props) => {
  const { orderDetailsData, ordersLabels, navigation, orderLookUpAPISwitch, isAppeasementEnabled } =
    props;

  if (orderLookUpAPISwitch) {
    return renderOrderDetailWithCRMView(
      orderDetailsData,
      ordersLabels,
      navigation,
      isAppeasementEnabled
    );
  }

  const {
    summary,
    orderStatus,
    purchasedItems,
    isBopisOrder,
    canceledItems,
    orderNumber,
    outOfStockItems,
    isBossOrder,
    emailAddress,
    returnedItems,
  } = orderDetailsData || {};
  const { currencySymbol } = summary || {};

  const notificationHeader = isBossOrder
    ? getLabelValue(ordersLabels, 'lbl_orders_noLongerAvailable')
    : getLabelValue(ordersLabels, 'lbl_orders_canceledItems');
  const notificationMessage = isBossOrder
    ? getLabelValue(ordersLabels, 'lbl_orders_isBossOrderCancelNotification')
    : getLabelValue(ordersLabels, 'lbl_orders_CancelNotification');

  return (
    <ViewWithSpacing spacingStyles="margin-left-MED margin-right-MED">
      <View>
        <Barcode value={orderNumber} barcodeId={orderNumber} width="2" displayValue />
      </View>
      <ResendOrderConfirmation orderId={orderNumber} trackingEmail={emailAddress} />
      <OrderBasicDetails orderDetailsData={orderDetailsData} ordersLabels={ordersLabels} />
      <OrderShippingDetails orderDetailsData={orderDetailsData} ordersLabels={ordersLabels} />
      <OrderBillingDetails orderDetailsData={orderDetailsData} ordersLabels={ordersLabels} />
      <OrderSummaryDetails
        orderDetailsData={orderDetailsData}
        ordersLabels={ordersLabels}
        isAppeasementEnabled={isAppeasementEnabled}
      />
      {orderDetailsData.orderType === config.ORDER_ITEM_TYPE.ECOM &&
        purchasedItems &&
        purchasedItems.length > 0 &&
        purchasedItems.map((orderGroup, index) => (
          <>
            <OrderStatus
              status={orderGroup.orderStatus}
              trackingNumber={orderGroup.trackingNumber}
              trackingUrl={orderGroup.trackingUrl}
              shippedDate={orderGroup.shippedDate}
              isBopisOrder={isBopisOrder}
              ordersLabels={ordersLabels}
            />

            <OrderItemsList
              key={index.toString()}
              ordersLabels={ordersLabels}
              items={orderGroup.items}
              currencySymbol={currencySymbol}
              navigation={navigation}
              isShowWriteReview={
                orderGroup.orderStatus === constants.STATUS_CONSTANTS.ORDER_SHIPPED ||
                orderStatus === constants.STATUS_CONSTANTS.ORDER_PARTIALLY_SHIPPED
              }
            />
          </>
        ))}
      {renderBopisAndBossOrder(orderDetailsData, ordersLabels, navigation)}
      {renderCancelledAndOutOfStockOrders(
        outOfStockItems,
        ordersLabels,
        currencySymbol,
        getLabelValue(ordersLabels, 'lbl_orders_outOfStock'),
        getLabelValue(ordersLabels, 'lbl_orders_outOfStockNotification')
      )}

      {renderCancelledAndOutOfStockOrders(
        canceledItems,
        ordersLabels,
        currencySymbol,
        notificationHeader,
        notificationMessage,
        true
      )}
      {renderReturnedAndRefundedOrders(
        returnedItems,
        ordersLabels,
        currencySymbol,
        getLabelValue(ordersLabels, 'lbl_orders_returned'),
        getLabelValue(ordersLabels, 'lbl_orders_returnedNotification')
      )}
    </ViewWithSpacing>
  );
};

RenderOrderDetailView.propTypes = {
  orderDetailsData: PropTypes.shape({}),
  ordersLabels: PropTypes.shape({}),
  navigation: PropTypes.shape({}),
  giftReceiptLabels: PropTypes.shape({}),
  orderLookUpAPISwitch: PropTypes.bool,
  isAppeasementEnabled: PropTypes.bool,
};

RenderOrderDetailView.defaultProps = {
  ordersLabels: {},
  orderDetailsData: {},
  navigation: {},
  giftReceiptLabels: {},
  orderLookUpAPISwitch: false,
  isAppeasementEnabled: false,
};

export const OrderDetailsView = ({
  orderDetailsData,
  ordersLabels,
  navigation,
  isFetching,
  userAccountId,
  orderReturnDays,
  giftReceiptLabels,
  orderLookUpAPISwitch,
  isAppeasementEnabled,
}) => {
  const { purchasedItems, orderNumber, emailAddress, orderDate } = orderDetailsData || {};
  const router = {
    orderId: orderNumber,
    orderDetailsData,
    orderReturnDays,
    ordersLabels,
    userAccountId,
    giftReceiptLabels,
  };
  const toggleReceiptsView = () => {
    navigation.navigate('OrderReceiptPage', {
      title: `${getLabelValue(ordersLabels, 'lbl_orders_receipt')}`,
      router,
    });
  };

  return (
    <>
      <ScrollView keyboardShouldPersistTaps="handled">
        {isFetching && <OrderDetailsSkeleton ordersLabels={ordersLabels} />}
        {!isFetching &&
          orderDetailsData &&
          RenderOrderDetailView({
            orderDetailsData,
            ordersLabels,
            navigation,
            orderLookUpAPISwitch,
            isAppeasementEnabled,
          })}
      </ScrollView>
      {purchasedItems && purchasedItems.length ? (
        <HelpWithOrder
          orderId={orderNumber}
          trackingEmail={emailAddress}
          toggleReceiptsView={toggleReceiptsView}
          navigation={navigation}
          orderDate={convertToISOString(orderDate)}
          orderDetailsData={orderDetailsData}
        />
      ) : null}
    </>
  );
};

OrderDetailsView.propTypes = {
  orderDetailsData: PropTypes.shape({}),
  ordersLabels: PropTypes.shape({}),
  navigation: PropTypes.shape({}),
  isFetching: PropTypes.bool,
  userAccountId: PropTypes.string,
  orderReturnDays: PropTypes.number,
  giftReceiptLabels: PropTypes.shape({}),
  orderLookUpAPISwitch: PropTypes.bool,
  isAppeasementEnabled: PropTypes.bool,
};

OrderDetailsView.defaultProps = {
  ordersLabels: {},
  orderDetailsData: {},
  navigation: {},
  isFetching: false,
  userAccountId: '',
  orderReturnDays: 0,
  giftReceiptLabels: {},
  orderLookUpAPISwitch: false,
  isAppeasementEnabled: false,
};

export default withStyles(OrderDetailsView);
