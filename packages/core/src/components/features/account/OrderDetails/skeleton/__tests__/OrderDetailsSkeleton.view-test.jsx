// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { OrderDetailsSkeletonVanilla } from '../OrderDetailsSkeleton.view';

describe('OrderDetailsSkeleton component', () => {
  it('should renders correctly', () => {
    const props = {
      className: 'sample-class',
    };
    const component = shallow(<OrderDetailsSkeletonVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
