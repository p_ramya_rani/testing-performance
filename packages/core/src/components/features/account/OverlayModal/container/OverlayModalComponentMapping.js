// 9fbef606107a605d69c0edbcd8029e5d 
import CreateAccount from '../../CreateAccount';
import LoginPage from '../../LoginPage';
import AccountDrawer from '../../AccountDrawer';

const OverlayModalComponentMapping = {
  login: LoginPage,
  createAccount: CreateAccount,
  accountDrawer: AccountDrawer,
};

export default OverlayModalComponentMapping;
