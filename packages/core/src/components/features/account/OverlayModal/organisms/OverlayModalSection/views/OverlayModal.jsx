// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import * as scopeTab from 'react-modal/lib/helpers/scopeTab';
import { Modal } from '@tcp/core/src/components/common/molecules';
import { KEY_CODES } from '@tcp/core/src/constants/keyboard.constants';
import {
  getViewportInfo,
  isMobileWeb,
  isCanada,
  getLabelValue,
  enableBodyScroll,
  disableBodyScroll,
} from '@tcp/core/src/utils';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import styles from '../styles/OverlayModal.style';

const propTypes = {
  component: PropTypes.string,
  closeOverlay: PropTypes.func,
  className: PropTypes.string,
  ModalContent: PropTypes.node.isRequired,
  color: PropTypes.shape({}),
  componentProps: PropTypes.shape({}).isRequired,
  showCondensedHeader: PropTypes.bool.isRequired,
  labels: PropTypes.shape({
    lbl_login_loginCTA: PropTypes.string,
    lbl_login_createAccountCTA: PropTypes.string,
  }),
  isLoggedIn: PropTypes.bool,
  setNeedHelpModal: PropTypes.func.isRequired,
};

const defaultProps = {
  component: null,
  closeOverlay: () => {},
  className: '',
  color: '',
  labels: PropTypes.shape({
    lbl_login_loginCTA: '',
    lbl_login_createAccountCTA: '',
  }),
  isLoggedIn: false,
};

class OverlayModal extends React.Component {
  constructor(props) {
    super(props);
    const overlayElementWrapper = document.getElementById('overlayWrapper');
    const overlayElement = document.getElementById('overlayComponent');
    const bodyContainer = document.querySelector('.non-checkout-pages');
    this.overlayElementWrapper = overlayElementWrapper;
    this.overlayElement = overlayElement;
    this.bodyContainer = bodyContainer;
    const [body] = document.getElementsByTagName('body');
    this.body = body;
    this.isMobile = getViewportInfo().isMobile && isMobileWeb();
    this.handleWindowClick = this.handleWindowClick.bind(this);
    this.handleWindowMouseDown = this.handleWindowMouseDown.bind(this);
    this.keydownInOverlay = this.keydownInOverlay.bind(this);
    this.selectOverlay = false;
  }

  componentDidMount() {
    this.overlayScrollTarget = document.getElementById('dialogContent');
    this.overlayElementWrapper.style.position = 'relative';
    this.overlayElementWrapper.style.pointerEvents = 'none';
    this.overlayElement.classList.add('overlay');
    /* istanbul ignore else */
    if (this.body) {
      this.body.addEventListener('click', this.handleWindowClick, { capture: true });
      this.body.addEventListener('mousedown', this.handleWindowMouseDown, { capture: true });
      this.body.addEventListener('keydown', this.handleDocumentKeydown, { capture: true });
    }
    this.getCustomStyles({ styleModal: true });
    if (this.modalRef) {
      // IE11 doesn't support preventScroll and causing autoScrolling, so adding a fallback, setActive is supported in IE11 only
      if (this.modalRef.setActive) {
        this.modalRef.setActive();
      } else {
        this.modalRef.focus({ preventScroll: true });
      }
    }
  }

  componentDidUpdate(prevProps) {
    const {
      component: nextTargetComponent,
      showCondensedHeader: nextCondensedState,
      isLoggedIn,
    } = this.props;
    const {
      component: prevTargetComponent,
      showCondensedHeader: prevCondensedState,
      isLoggedIn: prevLoggedIn,
    } = prevProps;
    const modal = document.getElementById('dialogContent');
    const loginStateChanged = !prevLoggedIn && isLoggedIn;
    const condensedStateChanged = nextCondensedState !== prevCondensedState;

    if (nextTargetComponent !== prevTargetComponent) {
      modal.scrollTo(0, 0);
      return this.getCustomStyles({ styleModal: false });
    }
    if (condensedStateChanged || loginStateChanged) {
      this.getCustomStyles({ styleModal: true });
    }

    this.isMobile = getViewportInfo().isMobile && isMobileWeb();

    if (!this.isMobile) {
      modal.addEventListener('keydown', this.keydownInOverlay);
    }

    if (this.isMobile && nextTargetComponent === 'accountDrawer') {
      document
        .querySelectorAll('#overlayWrapper, .header-promo__container, footer')
        .forEach((element) => element.setAttribute('aria-hidden', 'true'));
    }

    return null;
  }

  componentWillUnmount() {
    const { setNeedHelpModal } = this.props;
    setNeedHelpModal(false);
    this.overlayElementWrapper.style.position = 'static';
    this.overlayElementWrapper.style.pointerEvents = 'auto';
    /* istanbul ignore else */
    if (this.overlayElement) this.overlayElement.classList.remove('overlay');
    /* istanbul ignore else */
    if (this.body) {
      this.body.removeEventListener('click', this.handleWindowClick);
      this.body.removeEventListener('mousedown', this.handleWindowMouseDown);
      this.body.removeEventListener('keydown', this.handleWindowMouseDown);

      enableBodyScroll(this.overlayScrollTarget);
    }
    const modal = document.getElementById('dialogContent');
    modal.removeEventListener('keydown', this.keydownInOverlay);
    this.resetBodyScrollStyles();
    document
      .querySelectorAll('#overlayWrapper, .header-promo__container, footer')
      .forEach((element) => element.removeAttribute('aria-hidden'));
  }

  getHeading = () => {
    const { labels, component, componentProps } = this.props;
    if (component === 'login' && componentProps.currentForm !== 'forgotPassword') {
      return getLabelValue(labels, 'lbl_login_loginCTA');
    }
    if (component === 'createAccount') {
      return getLabelValue(labels, 'lbl_login_createAccountCTA');
    }
    return '';
  };

  /**
   * This function will calculate the right offset of the content
   * as the max-width of our content is 1440px.
   */
  getMaxRightOffset = () => {
    const overlayModalWrapper = document.getElementById('overlayModalWrapper');
    if (overlayModalWrapper) {
      const modalRect = overlayModalWrapper.getBoundingClientRect();
      if (window.innerWidth > modalRect.width) {
        return window.innerWidth - (modalRect.left + modalRect.width);
      }
    }

    return 0;
  };

  /**
   * Set Left position of modal triangle
   * @param {*} comp
   */

  // eslint-disable-next-line complexity
  styleModalTriangle = (comp, maxRightOffset) => {
    const { showCondensedHeader, component } = this.props;
    const isAccountDrawer = component === 'accountDrawer';
    if (this.isMobile && !isAccountDrawer) return;
    const compRectBoundingX = comp.getBoundingClientRect().left;
    const compWidth = comp.getBoundingClientRect().width / 2;
    const modal = document.getElementById('dialogContent');
    const modalRectBoundingX = modal && modal.getBoundingClientRect().left;
    const modalTriangle = document.getElementById('modalTriangle');
    const modalTrianglePos = modalTriangle && window && modalTriangle.getBoundingClientRect().top;

    if (showCondensedHeader && this.body) {
      modal.style.maxHeight = `${window.innerHeight - 70}px`;
    } else {
      modal.style.maxHeight = `${window.innerHeight - (modalTrianglePos + 20)}px`;
    }
    this.overlayScrollTarget = document.getElementById('dialogContent');
    disableBodyScroll(this.overlayScrollTarget);
    /* istanbul ignore else */
    if ((!showCondensedHeader || this.isMobile) && modal && modalTriangle) {
      const marginPx = showCondensedHeader ? 15 : 10;
      modalTriangle.style.left = `${
        compRectBoundingX + compWidth - (modalRectBoundingX + marginPx)
      }px`;
    } else {
      modalTriangle.style.left = 'auto';
      if (maxRightOffset > 0) {
        modalTriangle.style.right = `${maxRightOffset + 52}px`; // currently for condensed header, right position is 74, need to minus 11px for triangle width.
      }
    }
  };

  modalTrianglePositioning = ({ comp, maxRightOffset }) => {
    let compElement = comp;
    if (!this.isMobile && document.getElementById('account-info-user-points')) {
      compElement = document.getElementById('account-info-user-points');
    }
    this.styleModalTriangle(compElement, maxRightOffset);
  };

  // eslint-disable-next-line complexity
  getCustomStyles = ({ styleModal }) => {
    const { component } = this.props;
    if (this.isMobile && component !== 'accountDrawer') return;
    const comp = document.getElementById(component);
    /* istanbul ignore else */
    if (comp && window) {
      const compRectBoundingY = comp.getBoundingClientRect().top + window.pageYOffset;
      const compHeight = comp.getBoundingClientRect().height;
      const modalWrapper = document.getElementById('modalWrapper');
      const maxRightOffset = this.getMaxRightOffset();
      /* istanbul ignore else */
      if (styleModal && compRectBoundingY) {
        modalWrapper.style.top = `${compRectBoundingY + compHeight + 24}px`;
        if (maxRightOffset > 0) {
          modalWrapper.style.right = `${maxRightOffset}px`;
        }
      }
      this.modalTrianglePositioning({ comp, maxRightOffset });
    }
  };

  closeModal = () => {
    const { closeOverlay } = this.props;
    closeOverlay();
    if (this.body) {
      enableBodyScroll(this.overlayScrollTarget);
    }
    this.resetBodyScrollStyles();
  };

  /**
   * Set the wrapper ref
   */
  setModalRef = (node) => {
    this.modalRef = node;
  };

  /**
   * Reset Body scroll styles
   */
  resetBodyScrollStyles = () => {
    this.bodyContainer.style.height = '';
    this.bodyContainer.style.overflow = 'visible';
  };

  isTargetOutsideOverlay = (e) => {
    return (
      this.modalRef &&
      !this.modalRef.contains(e.target) &&
      !e.target.closest('.TCPModal__InnerContent')
    );
  };

  handleDocumentKeydown = (e) => {
    if (e.keyCode === KEY_CODES.KEY_TAB || (e.keyCode === KEY_CODES.KEY_TAB && e.shiftKey)) {
      if (this.modalRef && !this.modalRef.contains(document.activeElement)) {
        this.closeModal();
      }
    } else if (e.keyCode === KEY_CODES.KEY_ESCAPE) {
      this.closeModal();
    }
  };

  /**
   * Bind Tab key Down
   */
  keydownInOverlay(event) {
    if (event.keyCode === KEY_CODES.KEY_TAB) {
      scopeTab(this.modalRef, event);
    }
  }

  handleWindowClick(e) {
    const { component } = this.props;
    const nextComponent = e.target;
    let componentAttributeValue = null;
    if (nextComponent) {
      componentAttributeValue =
        nextComponent.getAttribute('data-overlayTarget') ||
        (nextComponent.closest('[data-overlayTarget]') &&
          nextComponent.closest('[data-overlayTarget]').getAttribute('data-overlayTarget'));
    }

    /* istanbul ignore else */
    if (this.isTargetOutsideOverlay(e)) {
      // TODO: find a better way to handle - prevent close overlay when click on popup modal
      if (
        this.selectOverlay &&
        componentAttributeValue !== 'createAccount' &&
        componentAttributeValue !== 'login'
      ) {
        e.stopImmediatePropagation();
        this.selectOverlay = false;
        return this.selectOverlay;
      }
      this.closeModal();

      if (component === componentAttributeValue) {
        e.stopImmediatePropagation();
      }
    }
    return null;
  }

  /**
   * this function used against Defect RWD-17344
   * on MouseDown set selectOverlay to true so that in case of dragging outside of overlay, we can prevent overlay close
   */

  handleWindowMouseDown(e) {
    if (!this.isTargetOutsideOverlay(e)) {
      this.selectOverlay = true;
    }
  }

  render() {
    const { className, ModalContent, color, componentProps, component, showCondensedHeader } =
      this.props;

    const modalHeading = {
      className: 'Modal_Heading_Overlay',
    };

    const headingForMobile = this.getHeading();
    const headingProps = headingForMobile
      ? {
          heading: headingForMobile,
          headingStyle: modalHeading,
        }
      : {};

    return this.isMobile && component !== 'accountDrawer' ? (
      <div>
        <Modal
          contentRef={this.setModalRef}
          isOpen
          className={className}
          overlayClassName="TCPModal__Overlay"
          onRequestClose={this.closeModal}
          noPadding
          id="modalWrapper"
          widthConfig={{ small: '100%' }}
          heightConfig={{ minHeight: '500px' }}
          {...headingProps}
        >
          <div
            id="dialogContent"
            className={`dialog__content ${showCondensedHeader && 'condensed-overlay'}`}
          >
            <ModalContent className="modal__content" {...componentProps} />
          </div>
        </Modal>
      </div>
    ) : (
      <div className="content-wrapper" id="overlayModalWrapper">
        <div
          className={className}
          id="modalWrapper"
          color={color}
          ref={this.setModalRef}
          tabIndex="-1"
          aria-label={headingForMobile}
        >
          <div
            id="dialogContent"
            className={`dialog__content ${showCondensedHeader && 'condensed-overlay'}`}
          >
            <button
              className={`hide-on-tablet hide-on-desktop ${
                component === 'accountDrawer' ? 'modal__closeIcon_account' : 'modal__closeIcon'
              }`}
              onClick={this.closeModal}
            />
            <div
              className={`${
                isCanada() ? 'triangle-ca-no-theme ' : 'triangle-theme'
              } modal__triangle ${showCondensedHeader && 'condensed-modal-triangle'}`}
              id="modalTriangle"
            />
            <div className={`${isCanada() ? 'ca-no-theme' : 'mpr-plcc-theme'} modal__bar`} />
            <ModalContent className="modal__content" {...componentProps} />
          </div>
        </div>
      </div>
    );
  }
}

OverlayModal.propTypes = propTypes;
OverlayModal.defaultProps = defaultProps;

export default withStyles(OverlayModal, styles);

export { OverlayModal as OverlayModalVanilla };
