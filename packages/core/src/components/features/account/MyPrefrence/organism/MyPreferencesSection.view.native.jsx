// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { View, ScrollView } from 'react-native';
import PropTypes from 'prop-types';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import PageHeadingWithLinks from '@tcp/core/src/components/features/account/common/molecule/PageHeadingWithLinks';
import withStyles from '../../../../common/hoc/withStyles.native';

import MyFavoriteStore from '../../MyProfile/organism/MyFavoriteStore';
import SocialContainer from '../../../../common/organisms/SocialAccount/container/Social.container';
import MyPreferenceSubscriptionContainer from '../../MyPreferenceSubscription/container/MyPreferenceSubscription.container';

class MyPrefrenceSection extends React.PureComponent {
  componentDidMount() {
    const { componentProps } = this.props;
    setTimeout(() => {
      if (componentProps === 'favoriteStore' && this.favoriteStore) {
        this.favoriteStore.measure((fx, fy) => {
          this.scrollRef.scrollTo({ y: fy - 100, animated: true });
        });
      } else if (componentProps === 'socialContainer' && this.socialContainer) {
        this.socialContainer.measure((fx, fy) => {
          this.scrollRef.scrollTo({ y: fy, animated: true });
        });
      } else if (componentProps === 'myPreferenceSubscription' && this.myPreferenceSubscription) {
        this.myPreferenceSubscription.measure((fx, fy) => {
          this.scrollRef.scrollTo({ y: fy, animated: true });
        });
      }
    }, 0);
  }

  render() {
    const { labels, handleComponentChange, componentProps } = this.props;
    return (
      <View>
        <ScrollView
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps="handled"
          ref={ref => {
            this.scrollRef = ref;
          }}
          scrollToOverflowEnabled
        >
          <PageHeadingWithLinks
            heading={getLabelValue(labels, 'lbl_prefrence_heading')}
            programDetailsCta={getLabelValue(labels, 'lbl_prefrence_program_details')}
            termsConditionCta={getLabelValue(labels, 'lbl_prefrence_term_codition')}
          >
            <View
              ref={view => {
                this.favoriteStore = view;
              }}
            >
              <MyFavoriteStore isMyPreferences handleComponentChange={handleComponentChange} />
            </View>

            <View
              ref={view => {
                this.socialContainer = view;
              }}
            >
              <SocialContainer
                labels={labels}
                handleComponentChange={handleComponentChange}
                componentProps={componentProps}
              />
            </View>
            <View
              ref={view => {
                this.myPreferenceSubscription = view;
              }}
            >
              <MyPreferenceSubscriptionContainer labels={labels} />
            </View>
          </PageHeadingWithLinks>
        </ScrollView>
      </View>
    );
  }
}

MyPrefrenceSection.propTypes = {
  labels: PropTypes.shape({}),
  handleComponentChange: PropTypes.func,
  componentProps: PropTypes.shape({}),
};

MyPrefrenceSection.defaultProps = {
  labels: {},
  handleComponentChange: () => {},
  componentProps: {},
};

export default withStyles(MyPrefrenceSection);
