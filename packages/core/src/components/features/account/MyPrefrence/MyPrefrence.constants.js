// 9fbef606107a605d69c0edbcd8029e5d 
import { MY_PROFILE_ACTION_PATTERN } from '../../../../constants/reducer.constants';

export default {
  UPDATE_PROFILE_SUCCESS: `${MY_PROFILE_ACTION_PATTERN}UPDATE_PROFILE_SUCCESS`,
};
