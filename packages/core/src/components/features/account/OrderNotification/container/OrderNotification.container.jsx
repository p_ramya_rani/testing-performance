// 9fbef606107a605d69c0edbcd8029e5d 
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { validateDiffInDaysNotification } from '@tcp/core/src/utils/utils';
import {
  getLastBopis,
  getLastSTHOrder,
  getLastBoss,
  getLimitToDisplayBossOrder,
  getLimitToDisplayLastOrderNotification,
  getTransactionNotificationsInMyAccountEnabled,
  getLabels,
} from './OrderNotification.selectors';
import { getLabels as getOrderLabels } from '../../Account/container/Account.selectors';
import {
  getOrdersListState,
  getShowOrderListWithItemDetails,
} from '../../Orders/container/Orders.selectors';
import { getSiteId, isCanada } from '../../../../../utils';
import { getOrdersList } from '../../Orders/container/Orders.actions';
import constants from '../../Orders/Orders.constants';
import OrderNotificationComponent from '../views';

/**
 * This component will render OrderNotification container component
 */
export class OrderNotification extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { STHEnabled: false, BOSSEnabled: false, BOPISEnabled: false };
  }

  componentDidMount() {
    const { fetchOrders, showOrderListWithItemDetails, ordersListItems } = this.props;
    const { pathname = '' } = (window && window.location) || {};
    let listPayload = getSiteId();
    if (showOrderListWithItemDetails) {
      listPayload = {
        reqParams: {
          pageNumber: 0,
          pageSize: constants.ORDER_SIZE,
        },
      };
    }
    if (
      !(
        ordersListItems &&
        ordersListItems.orders &&
        showOrderListWithItemDetails &&
        (pathname.includes('/account/orders') || pathname.includes('/customer-help'))
      )
    ) {
      fetchOrders(listPayload);
    }
  }

  componentDidUpdate() {
    const {
      orderSTH,
      orderBOPIS,
      orderBOSS,
      limitOfDaysToDisplayNotification,
      limitOfDaysToDisplayBossNotification,
    } = this.props;

    if (
      orderBOPIS &&
      validateDiffInDaysNotification(orderBOPIS.orderDate, limitOfDaysToDisplayNotification)
    ) {
      this.updateState('BOPISEnabled');
    }

    if (
      !isCanada() &&
      orderBOSS &&
      validateDiffInDaysNotification(orderBOSS.orderDate, limitOfDaysToDisplayBossNotification)
    ) {
      this.updateState('BOSSEnabled');
    }

    if (
      orderSTH &&
      validateDiffInDaysNotification(orderSTH.orderDate, limitOfDaysToDisplayNotification)
    ) {
      this.updateState('STHEnabled');
    }
  }

  updateState = key => {
    this.setState({
      [key]: true,
    });
  };

  render() {
    const {
      labels,
      orderLabels,
      isTransactionNotificationsInMyAccountEnabled,
      orderSTH,
      orderBOPIS,
      orderBOSS,
      closedOverlay,
    } = this.props;

    const { STHEnabled, BOSSEnabled, BOPISEnabled } = this.state;
    return (
      <>
        {isTransactionNotificationsInMyAccountEnabled && (
          <>
            {BOPISEnabled && (
              <OrderNotificationComponent
                order={orderBOPIS}
                labels={labels}
                orderLabels={orderLabels}
                separator={BOSSEnabled || STHEnabled}
                closedOverlay={closedOverlay}
              />
            )}

            {BOSSEnabled && (
              <OrderNotificationComponent
                order={orderBOSS}
                labels={labels}
                orderLabels={orderLabels}
                separator={STHEnabled}
                closedOverlay={closedOverlay}
              />
            )}
            {STHEnabled && (
              <OrderNotificationComponent
                order={orderSTH}
                orderLabels={orderLabels}
                labels={labels}
                separator={false}
                closedOverlay={closedOverlay}
              />
            )}
          </>
        )}
      </>
    );
  }
}

export const mapStateToProps = (state, ownProps) => ({
  labels: getLabels(state),
  orderLabels: getOrderLabels(state),
  ordersListItems: getOrdersListState(state),
  orderSTH: getLastSTHOrder(state),
  orderBOSS: getLastBoss(state),
  orderBOPIS: getLastBopis(state),
  limitOfDaysToDisplayNotification: getLimitToDisplayLastOrderNotification(state),
  limitOfDaysToDisplayBossNotification: getLimitToDisplayBossOrder(state),
  isTransactionNotificationsInMyAccountEnabled: getTransactionNotificationsInMyAccountEnabled(
    state
  ),
  showOrderListWithItemDetails: getShowOrderListWithItemDetails(state, ownProps),
});

export const mapDispatchToProps = dispatch => ({
  fetchOrders: payload => {
    dispatch(getOrdersList(payload));
  },
});

OrderNotification.propTypes = {
  fetchOrders: PropTypes.func,
  labels: PropTypes.shape({}).isRequired,
  orderLabels: PropTypes.shape({}).isRequired,
  orderSTH: PropTypes.shape({}),
  orderBOSS: PropTypes.shape({}),
  orderBOPIS: PropTypes.shape({}),
  limitOfDaysToDisplayNotification: PropTypes.number.isRequired,
  limitOfDaysToDisplayBossNotification: PropTypes.number.isRequired,
  isTransactionNotificationsInMyAccountEnabled: PropTypes.bool.isRequired,
  closedOverlay: PropTypes.func.isRequired,
  showOrderListWithItemDetails: PropTypes.bool,
  ordersListItems: PropTypes.shape([]),
};

OrderNotification.defaultProps = {
  fetchOrders: () => {},
  orderSTH: {},
  orderBOSS: {},
  orderBOPIS: {},
  showOrderListWithItemDetails: false,
  ordersListItems: [],
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OrderNotification);
