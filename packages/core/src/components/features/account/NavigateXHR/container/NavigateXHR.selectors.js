// 9fbef606107a605d69c0edbcd8029e5d 
export const getCookieToken = state => {
  return state.NavigateXHR && state.NavigateXHR.getIn(['cookie_token', 'encodedcookie']);
};

export default { getCookieToken };
