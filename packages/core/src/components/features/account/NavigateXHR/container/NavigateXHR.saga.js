// 9fbef606107a605d69c0edbcd8029e5d 
import { call, takeLatest, put, select } from 'redux-saga/effects';
import { readCookie } from '@tcp/core/src/utils/cookie.util';
import { getIsSessionSharingEnabled } from '@tcp/core/src/reduxStore/selectors/session.selectors';
import { NavigateXHR } from '../../../../../services/abstractors/account';
import NAVIGATE_XHR_CONSTANTS from './NavigateXHR.constants';
import { setNavigateCookieToken } from './NavigateXHR.action';
import { setLocalStorage } from '../../../../../utils/localStorageManagement';
import { isMobileApp, getAPIConfig } from '../../../../../utils';

const getNavigateResValue = response => {
  let obj;
  if (response.success) {
    obj = response.success;
  }
  if (response.data && response.data === 'success') {
    obj = response.data;
  }
  return obj;
};

const getEncodedCookie = response => {
  let value;
  if (response.encodedcookie) {
    value = response.encodedcookie;
  }
  if (response.data && response.data.encodedCookie) {
    value = response.data.encodedCookie;
  }
  return value;
};

export function* navigateCrossDomainSaga({ payload }) {
  try {
    const ismobile = isMobileApp();
    const { headers = {} } = payload || {};
    const isSessionSharedEnabled = yield select(getIsSessionSharingEnabled);

    if (isSessionSharedEnabled) {
      const cookies = ismobile ? yield call(readCookie) : '';
      const response = yield call(NavigateXHR, cookies, headers);
      const navigationXHRVal = getNavigateResValue(response);
      const encodedCookieVal = getEncodedCookie(response);
      if (navigationXHRVal) {
        return navigationXHRVal;
      }

      if (encodedCookieVal) {
        let encodedcookie = encodedCookieVal;
        const { brandId } = getAPIConfig();
        if (headers && headers.actionTaken === 'logout') {
          encodedcookie = headers.actionTaken;
        }
        setLocalStorage({ key: `${brandId && brandId.toLowerCase()}_ct`, value: encodedcookie });
        yield put(
          setNavigateCookieToken({
            encodedcookie,
          })
        );
      }
      return response;
    }
    return null;
  } catch (err) {
    return '';
  }
}

export function* NavigateXHRSaga() {
  yield takeLatest(NAVIGATE_XHR_CONSTANTS.NAVIGATE_XHR_STATE, navigateCrossDomainSaga);
}

export default NavigateXHRSaga;
