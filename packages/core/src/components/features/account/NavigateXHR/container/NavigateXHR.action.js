// 9fbef606107a605d69c0edbcd8029e5d 
import NAVIGATE_XHR_CONSTANTS from './NavigateXHR.constants';

export const navigateXHRAction = payload => {
  return {
    type: NAVIGATE_XHR_CONSTANTS.NAVIGATE_XHR_STATE,
    payload,
  };
};

export const setNavigateCookieToken = payload => {
  return {
    type: NAVIGATE_XHR_CONSTANTS.SET_NAVIGATE_TOKEN,
    payload,
  };
};

export default { navigateXHRAction };
