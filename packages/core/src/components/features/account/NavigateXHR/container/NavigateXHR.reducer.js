// 9fbef606107a605d69c0edbcd8029e5d 
import { fromJS } from 'immutable';
import NAVIGATE_XHR_CONSTANTS from './NavigateXHR.constants';

const initialState = fromJS({
  cookie_token: null,
});

const NavigateReducer = (state = initialState, action) => {
  switch (action.type) {
    case NAVIGATE_XHR_CONSTANTS.SET_NAVIGATE_TOKEN:
      return state.set('cookie_token', action.payload);
    default:
      if (state instanceof Object) {
        return fromJS(state);
      }
      return state;
  }
};

export default NavigateReducer;
