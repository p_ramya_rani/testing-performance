// 9fbef606107a605d69c0edbcd8029e5d 
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getLabels } from '../../Account/container/Account.selectors';
import ChangePasswordPageContainer from '../views/ChangePasswordPage';
import ResetPasswordRouteContainer from '../../ResetPasswordRoute';
import { getUrlParameter } from '../../../../../utils';

export class ChangePasswordRouteContainer extends PureComponent {
  static propTypes = {
    labels: PropTypes.shape({}).isRequired,
  };

  state = {
    showResetPassword: false,
    loading: true,
  };

  componentDidMount() {
    const page = getUrlParameter('page');
    if (page && page === 'reset') {
      this.setState({ showResetPassword: true, loading: false });
    } else {
      this.setState({ loading: false });
    }
  }

  render() {
    const { labels } = this.props;
    const { loading, showResetPassword } = this.state;
    if (loading) {
      return null;
    }
    if (showResetPassword) {
      return <ResetPasswordRouteContainer />;
    }

    return <ChangePasswordPageContainer labels={labels} />;
  }
}

export const mapStateToProps = state => ({
  labels: getLabels(state),
});

export default connect(mapStateToProps)(ChangePasswordRouteContainer);
