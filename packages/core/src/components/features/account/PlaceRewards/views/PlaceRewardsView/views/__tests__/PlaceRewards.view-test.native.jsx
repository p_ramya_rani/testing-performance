// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import PlaceRewardsView from '../PlaceRewards.view';

describe('MyRewards', () => {
  it('should render correctly', () => {
    const labels = {
      common: {},
      myPlaceRewards: {},
    };
    const tree = shallow(<PlaceRewardsView labels={labels} />);
    expect(tree).toMatchSnapshot();
  });
});
