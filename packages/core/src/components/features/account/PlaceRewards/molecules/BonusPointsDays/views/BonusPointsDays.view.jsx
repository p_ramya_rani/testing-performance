// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import BonusPointsDays from '../../../../../../common/organisms/BonusPointsDays';

const BonusPointsDaysSection = props => {
  return <BonusPointsDays {...props} />;
};

export default BonusPointsDaysSection;
