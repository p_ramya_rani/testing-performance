// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  .create-acc-cta {
    margin: ${props => props.theme.spacing.ELEM_SPACING.SM} 0;
  }
`;

export default styles;
