// 9fbef606107a605d69c0edbcd8029e5d
import styled, { css } from 'styled-components/native';

const getPageStyle = (props) => {
  const { theme } = props;
  return `
  margin: ${theme.spacing.APP_LAYOUT_SPACING.XXS} ${theme.spacing.LAYOUT_SPACING.XXS} auto ${
    theme.spacing.LAYOUT_SPACING.XXS
  };
  justify-content: ${'center'};
  `;
};

const FormStyle = css`
  ${(props) => (!props.showOnPlccModal ? getPageStyle : '')}
`;

/**
 * @param {Object} props : props for getTextBaseStyle
 * @return {Object} : Return object
 * @desc This method get font base style
 */

const getTextBaseStyle = (props) => {
  const { theme } = props;
  const { typography, colorPalette } = theme;
  return `
  font-size: ${typography.fontSizes.fs12};
  color: ${colorPalette.text.secondary};
  font-family: ${typography.fonts.secondary};
  `;
};

/**
 * @param {Object} props : props for getTextBaseStyle
 * @return {Object} : Return object
 * @desc This method get font base style
 */
const getDescriptionStyle = (props) => {
  const { theme } = props;
  const { typography, colorPalette } = theme;
  return `
  ${getTextBaseStyle};
  font-size: ${typography.fontSizes.fs14};
  color: ${colorPalette.text.primary};
  margin-top: ${'27px'};
  text-align: ${'center'};
  `;
};

const ShowHideWrapperStyle = () => `position:relative;`;

const HideShowField = (props) => {
  const { theme } = props;
  const { colorPalette } = theme;
  return `
  background: ${props.theme.colorPalette.white};
  height:${props.theme.spacing.ELEM_SPACING.MED};
  position: absolute;
  right: 17px;
  top: 30px;
  border-bottom-width: 1px;
  border-bottom-color:${colorPalette.gray[800]}
  ;
  `;
};

const DescriptionStyle = styled.Text`
  ${getDescriptionStyle}
`;

const ShowHideWrapper = styled.View`
  ${ShowHideWrapperStyle}
`;

const HideShowFieldWrapper = styled.View`
  ${HideShowField}
`;

const GuestButtonWrapper = styled.View`
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
`;

const IconContainer = styled.View`
  position: absolute;
  right: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  width: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
`;

const BodyTextWrapper = styled.View`
  right: -12px;
  top: 10px;
  ${'' /* 10px and -12px doesn't exist in ELEM_SPACING in spacing.js */}
`;

const ButtonWrapper = styled.View`
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.REG};
`;

const SwitchStyle = {
  touchStatusWrapperStyle: {
    flexDirection: 'row',
    flex: 1,
    marginBottom: 26,
    marginTop: 16,
  },

  switchInActiveTextStyle: {
    fontSize: 14,
    fontFamily: 'Nunito-Bold',
    color: '#050303',
    paddingLeft: 1,
  },

  switchActiveTextStyle: {
    paddingRight: 2,
  },

  switchInnerCircleStyle: {
    marginLeft: 5,
    marginRight: 2,
  },

  switchOuterCircleStyle: {
    width: 73,
  },
};

const ResetPassword = styled.View`
  display: flex;
  flex-direction: row;
`;

export {
  FormStyle,
  DescriptionStyle,
  ShowHideWrapper,
  HideShowFieldWrapper,
  GuestButtonWrapper,
  IconContainer,
  BodyTextWrapper,
  ButtonWrapper,
  SwitchStyle,
  ResetPassword,
};
