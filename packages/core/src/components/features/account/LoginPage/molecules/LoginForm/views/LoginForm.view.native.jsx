// 9fbef606107a605d69c0edbcd8029e5d
import React, { Fragment } from 'react';
import { View } from 'react-native';
import { reduxForm, Field, change as changeReduxFrom } from 'redux-form';
import { PropTypes } from 'prop-types';
import RecaptchaModal from '@tcp/core/src/components/common/molecules/recaptcha/recaptchaModal.native';

import get from 'lodash/get';
import noop from 'lodash/noop';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import ButtonRedesign from '@tcp/core/src/components/common/atoms/ButtonRedesign';
import CustomSwitch from '@tcp/core/src/components/common/atoms/Switch/Switch.native';
import withStyles from '../../../../../../common/hoc/withStyles.native';

import {
  FormStyle,
  ShowHideWrapper,
  HideShowFieldWrapper,
  GuestButtonWrapper,
  BodyTextWrapper,
  ButtonWrapper,
  SwitchStyle,
  ResetPassword,
} from '../styles/LoginForm.style.native';
import TextBox from '../../../../../../common/atoms/TextBox';
import CustomButton from '../../../../../../common/atoms/Button';
import Anchor from '../../../../../../common/atoms/Anchor';
import LineComp from '../../../../../../common/atoms/Line';
import InputCheckbox from '../../../../../../common/atoms/InputCheckbox';
import createValidateMethod from '../../../../../../../utils/formValidation/createValidateMethod';
import getStandardConfig from '../../../../../../../utils/formValidation/validatorStandardConfig';
import names from '../../../../../../../constants/eventsName.constants';
import ClickTracker from '../../../../../../../../../mobileapp/src/components/common/atoms/ClickTracker';
import { BodyCopy } from '../../../../../../common/atoms';

const styles = {
  loginButtonStyle: {
    marginTop: 30,
  },

  createAccountStyle: {
    marginTop: 30,
  },

  forgotPasswordStyle: {
    marginTop: 10,
  },

  emptyViewStyle: {
    marginBottom: 27,
  },
};

const {
  touchStatusWrapperStyle,
  switchInActiveTextStyle,
  switchActiveTextStyle,
  switchInnerCircleStyle,
  switchOuterCircleStyle,
} = SwitchStyle;

/**
 * @param {string} props : props for CustomIcon
 * @return {JSX} IconClass : Return jsx icon component
 * @desc This method based on the props generate icon component.
 */
class LoginForm extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      type: 'password',
      setRecaptchaModalMountedState: false,
      switchValue: false,
      passwordFieldFocus: false,
    };
  }

  componentDidMount() {
    const { fromResetView, toastMessage, labels } = this.props;
    if (fromResetView) {
      toastMessage(getLabelValue(labels, 'lbl_reset_pass_succcessMsg', 'login'));
    }
  }

  componentDidUpdate(prevProps) {
    const { change, setEmailid } = this.props;
    if (!prevProps.setEmailid && setEmailid) {
      change('emailAddress', setEmailid);
    }
  }

  setRecaptchaModalMountState = () => {
    const { setRecaptchaModalMountedState } = this.state;
    this.setState({
      setRecaptchaModalMountedState: !setRecaptchaModalMountedState,
    });
  };

  onMessage = (event) => {
    const { handleSubmit, onSubmit, change, plccEmail } = this.props;
    if (event && event.nativeEvent.data) {
      const value = get(event, 'nativeEvent.data', '');
      change('recaptchaToken', value);
      handleSubmit((data) => {
        const { emailAddress, password, rememberMe, savePlcc, useTouchID, useFaceID } = data;
        const emailAddressData = plccEmail || emailAddress;
        const LoginData = {
          emailAddress: emailAddressData,
          password,
          rememberMe,
          savePlcc,
          useTouchID,
          useFaceID,

          recaptchaToken: value,
        };
        onSubmit(LoginData);
      })();

      this.setRecaptchaModalMountState();
    }
  };

  onClose = () => {
    this.setRecaptchaModalMountState();
  };

  handleLoginClick = (e) => {
    const { handleSubmit, invalid, showRecaptcha } = this.props;
    e.preventDefault();
    if (!invalid && showRecaptcha) {
      this.setRecaptchaModalMountState();
    } else {
      handleSubmit();
    }
  };

  showForgotPassword = () => {
    const { showForgotPasswordForm, resetForm } = this.props;
    showForgotPasswordForm();
    resetForm();
  };

  handleContinueAsGuest = () => {
    const { handleContinueAsGuest } = this.props;
    setTimeout(() => {
      handleContinueAsGuest();
    }, 50);
  };

  changeType = (e) => {
    e.preventDefault();
    const { type } = this.state;
    this.setState({
      type: type === 'password' ? 'text' : 'password',
    });
  };

  getSwitchValue = (value) => {
    this.setState({ switchValue: value });
    const { dispatch } = this.props;
    if (dispatch) {
      dispatch(changeReduxFrom('myLoginForm', 'toggleValue', value));
    }
  };

  getBiometricStatus = (status, labels) => {
    if (status) {
      return getLabelValue(labels, 'lbl_createAccount_useTouchId', 'registration');
    }
    return getLabelValue(labels, 'lbl_createAccount_useFaceId', 'registration');
  };

  showShowPlccCheckbox = (userplccCardNumber, userplccCardId, showOnPlccModal) =>
    !!(userplccCardNumber && userplccCardId) && !showOnPlccModal;

  // eslint-disable-next-line sonarjs/cognitive-complexity
  render() {
    const {
      labels,
      variation,
      getTouchStatus,
      showRecaptcha,
      userplccCardNumber,
      userplccCardId,
      pristine,
      isLoading,
      initialValues,
      loyaltyPageName,
      showOnPlccModal,
      isRtpsFlow,
    } = this.props;

    const { type, setRecaptchaModalMountedState, passwordFieldFocus, switchValue } = this.state;
    const getPlccLbl = getLabelValue(
      labels,
      'lbl_createAccount_plcc_checkbox_Text',
      'registration'
    ).replace('#number', `${userplccCardNumber}`);

    const shouldLoginButtonDisabled = pristine || isLoading;

    return (
      <Fragment>
        <View {...this.props}>
          {!showOnPlccModal && (
            <Field
              label={getLabelValue(labels, 'lbl_login_email', 'login')}
              name="emailAddress"
              id="emailAddress"
              type="text"
              keyboardType="email-address"
              autoCapitalize="none"
              component={TextBox}
              showAnimatedBorder
              dataLocator="emailAddress"
            />
          )}

          <ShowHideWrapper>
            <Field
              label={getLabelValue(labels, 'lbl_login_password', 'login')}
              name="password"
              id="password"
              type={type}
              component={TextBox}
              dataLocator="password"
              showAnimatedBorder
              secureTextEntry={type === 'password'}
              onPasswordFocus={() => {
                if (!passwordFieldFocus) {
                  this.setState({ passwordFieldFocus: true });
                }
              }}
              rightText={
                type === 'password'
                  ? getLabelValue(labels, 'lbl_createAccount_show', 'registration')
                  : getLabelValue(labels, 'lbl_createAccount_hide', 'registration')
              }
            />
            {showOnPlccModal && (
              <ResetPassword>
                <Anchor
                  id="forgotPasswordForm"
                  class="clickhere"
                  fontSizeVariation="large"
                  anchorVariation="primary"
                  colorName="gray.900"
                  text={getLabelValue(labels, 'lbl_login_Description_clickhere', 'login')}
                  underline
                  onPress={() => {
                    this.showForgotPassword(this.props);
                  }}
                />
              </ResetPassword>
            )}
            {passwordFieldFocus ? (
              <HideShowFieldWrapper>
                <Anchor
                  fontSizeVariation="medium"
                  fontFamily="secondary"
                  anchorVariation="lightGray"
                  onPress={this.changeType}
                  noLink
                  to="/#"
                  dataLocator=""
                  text={
                    type === 'password'
                      ? getLabelValue(labels, 'lbl_createAccount_show', 'registration')
                      : getLabelValue(labels, 'lbl_createAccount_hide', 'registration')
                  }
                />
              </HideShowFieldWrapper>
            ) : null}
          </ShowHideWrapper>
          {this.showShowPlccCheckbox(userplccCardNumber, userplccCardId, showOnPlccModal) && (
            <Field
              inputVariation="inputVariation-1"
              name="plcc_checkbox"
              component={InputCheckbox}
              dataLocator="plcc_checkbox"
              disabled={false}
              rightText={getPlccLbl}
              marginTop={13}
              isChecked={initialValues.plcc_checkbox}
            />
          )}

          {getTouchStatus && !showOnPlccModal ? (
            <View style={touchStatusWrapperStyle}>
              <CustomSwitch
                value={switchValue}
                onValueChange={(val) => this.getSwitchValue(val)}
                disabled={false}
                activeText="YES"
                inActiveText="NO"
                circleSize={25}
                barHeight={34}
                circleBorderWidth={0}
                backgroundActive="#254f6e"
                backgroundInactive="#d8d8d8"
                circleActiveColor="#ffffff"
                circleInActiveColor="#ffffff"
                inactiveTextStyle={switchInActiveTextStyle}
                activeTextStyle={switchActiveTextStyle}
                changeValueImmediately // if rendering inside circle, change state immediately or wait for animation to complete
                innerCircleStyle={switchInnerCircleStyle} // style for inner animated circle for what you (may) be rendering inside the circle
                outerCircleStyle={switchOuterCircleStyle} // style for outer animated circle
                renderActiveText
                renderInActiveText
                switchLeftPx={2} // denominator for logic when sliding to TRUE position. Higher number = more space from RIGHT of the circle to END of the slider
                switchRightPx={2} // denominator for logic when sliding to FALSE position. Higher number = more space from LEFT of the circle to BEGINNING of the slider
                switchWidthMultiplier={3.1} // multiplied by the `circleSize` prop to calculate total width of the Switch
                switchBorderRadius={17} // Sets the border Radius of the switch slider. If unset, it remains the circleSize.
              />
              <BodyTextWrapper>
                {(getTouchStatus === 'TouchID' || getTouchStatus === true) && (
                  <BodyCopy
                    fontSize="fs14"
                    fontWeight="regular"
                    fontFamily="secondary"
                    color="gray.900"
                    marginLeft="12px"
                    width="343"
                    alignSelf="stretch"
                    text={getLabelValue(labels, 'lbl_createAccount_useTouchId', 'registration')}
                  />
                )}
                {getTouchStatus === 'FaceID' && (
                  <BodyCopy
                    fontSize="fs14"
                    fontWeight="regular"
                    fontFamily="secondary"
                    color="gray.900"
                    marginLeft="12px"
                    width="343"
                    alignSelf="stretch"
                    text={getLabelValue(labels, 'lbl_createAccount_useFaceId', 'registration')}
                  />
                )}
              </BodyTextWrapper>
            </View>
          ) : (
            <View style={styles.emptyViewStyle} />
          )}
          <ButtonWrapper>
            <ButtonRedesign
              text={
                isRtpsFlow
                  ? getLabelValue(labels, 'lbl_changePassword_loginToCheckoutCta_PLCC', 'password')
                  : getLabelValue(labels, 'lbl_login_loginCTA', 'login')
              }
              fill="BLUE"
              className="login-button-test-class"
              customStyle={styles.loginButtonStyle}
              borderRadius="16px"
              fontFamily="secondary"
              fontWeight="bold"
              fontSize="fs16"
              paddings="8px"
              showShadow
              disableButton={shouldLoginButtonDisabled}
              onPress={this.handleLoginClick}
              showLoadingSpinner={isLoading}
            />
          </ButtonWrapper>

          {variation === 'checkout' && (
            <GuestButtonWrapper>
              <ClickTracker
                as={CustomButton}
                name={names.screenNames.loyalty_continue_as_guest}
                clickData={{
                  pageName: variation,
                  customEvents: ['event137'],
                  loyaltyLocation: loyaltyPageName,
                }}
                text={getLabelValue(labels, 'lbl_login_modal_checkout_as_guest', 'login')}
                onPress={this.handleContinueAsGuest}
              />
            </GuestButtonWrapper>
          )}

          {!showOnPlccModal && (
            <Anchor
              style={styles.underline}
              class="underlink"
              underline
              fontSizeVariation="large"
              anchorVariation="primary"
              text={getLabelValue(labels, 'lbl_login_forgetPasswordCTA', 'login')}
              customStyle={styles.forgotPasswordStyle}
              onPress={this.showForgotPassword}
            />
          )}
          {!showOnPlccModal && <LineComp marginTop={26} borderColor="gray.2700" />}

          <React.Fragment>
            {setRecaptchaModalMountedState && showRecaptcha && (
              <RecaptchaModal
                onMessage={this.onMessage}
                setRecaptchaModalMountedState={setRecaptchaModalMountedState}
                toggleRecaptchaModal={this.setRecaptchaModalMountState}
                onClose={this.onClose}
              />
            )}
          </React.Fragment>
        </View>
      </Fragment>
    );
  }
}
LoginForm.propTypes = {
  labels: PropTypes.shape({
    login: {
      lbl_login_email: PropTypes.string,
      lbl_login_password: PropTypes.string,
      lbl_login_rememberMe: PropTypes.string,
      lbl_login_saveMyPlace: PropTypes.string,
      login: PropTypes.string,
      lbl_login_createAccountCTA: PropTypes.string,
      lbl_login_forgetPasswordCTA: PropTypes.string,
      lbl_login_createAccountHelp: PropTypes.string,
    },
  }),
  handleSubmit: PropTypes.func,
  onSubmit: PropTypes.func,
  showForgotPasswordForm: PropTypes.func.isRequired,
  resetForm: PropTypes.func.isRequired,
  userplccCardNumber: PropTypes.string,
  userplccCardId: PropTypes.string,

  change: PropTypes.bool.isRequired,
  setEmailid: PropTypes.func.isRequired,
  invalid: PropTypes.bool.isRequired,
  showRecaptcha: PropTypes.bool.isRequired,
  handleContinueAsGuest: PropTypes.func.isRequired,
  variation: PropTypes.string.isRequired,
  getTouchStatus: PropTypes.func.isRequired,
  fromResetView: PropTypes.bool.isRequired,
  toastMessage: PropTypes.func.isRequired,
  pristine: PropTypes.bool,
  isLoading: PropTypes.bool.isRequired,
  initialValues: PropTypes.shape({}),
  loyaltyPageName: PropTypes.string,
  showOnPlccModal: PropTypes.bool,
  plccEmail: PropTypes.string,
  isRtpsFlow: PropTypes.bool,
};

LoginForm.defaultProps = {
  labels: {
    login: {
      lbl_login_email: 'Email Address',
      lbl_login_password: 'Password',
      lbl_login_rememberMe: `Remember me.\nNot recommended on shared devices.`,
      lbl_login_saveMyPlace: `Save My Place Rewards Credit Card ending in 1234\nto my account for future purchases.`,
      lbl_login_loginCTA: 'LOG IN',
      lbl_login_createAccountCTA: 'CREATE ACCOUNT',
      lbl_login_forgetPasswordCTA: 'Forgot password?',
      lbl_login_createAccountHelp: "Don't have an account? Create one now to start earning points!",
    },
  },
  handleSubmit: noop,
  onSubmit: noop,
  userplccCardNumber: '',
  userplccCardId: '',
  pristine: false,
  initialValues: {},
  loyaltyPageName: 'account',
  showOnPlccModal: false,
  plccEmail: '',
  isRtpsFlow: false,
};

const validateMethod = createValidateMethod(
  getStandardConfig([
    { emailAddress: 'emailAddressNoAsync' },
    { password: 'legacyPassword' },
    'recaptchaToken',
  ])
);

export default reduxForm({
  form: 'myLoginForm',
  enableReinitialize: true,
  ...validateMethod,
})(withStyles(LoginForm, FormStyle));
export { LoginForm as LoginFormVanilla };
