// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { LoginFormVanilla } from '../LoginForm.view.native';

describe('LoginForm component', () => {
  const props = {
    labels: {
      login: {},
      handleSubmit: () => {},
      onSubmit: () => {},
      loginErrorMessage: '',
      showForgotPasswordForm: () => {},
      resetForm: () => {},
      registration: {},
    },
    handleSubmit: () => {},
    onSubmit: () => {},
    loginErrorMessage: '',
  };
  it('should renders correctly', () => {
    const component = shallow(<LoginFormVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders correctly with login button disabled', () => {
    props.isLoading = true;
    const component = shallow(<LoginFormVanilla {...props} />);
    expect(component.find('.login-button-test-class').prop('disableButton')).toEqual(true);
  });

  it('should renders correctly with login button enabled', () => {
    props.isLoading = false;
    const component = shallow(<LoginFormVanilla {...props} />);
    expect(component.find('.login-button-test-class').prop('disableButton')).toEqual(false);
  });
});
