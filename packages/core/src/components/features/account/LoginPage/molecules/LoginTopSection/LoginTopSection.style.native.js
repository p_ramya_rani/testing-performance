// 9fbef606107a605d69c0edbcd8029e5d
import styled, { css } from 'styled-components/native';

const getPageStyle = () => {
  return `
  margin: auto;
  justify-content: ${'center'};
  `;
};
const SectionStyle = css`
  ${getPageStyle}
`;

/**
 * @param {Object} props : props for getTextBaseStyle
 * @return {Object} : Return object
 * @desc This method get font base style
 */

const getTextBaseStyle = (props) => {
  const { theme } = props;
  const { typography, colorPalette } = theme;
  return `
  font-size: ${typography.fontSizes.fs12};
  color: ${colorPalette.text.secondary};
  font-family: ${typography.fonts.secondary};
  `;
};
const getHeadingStyle = (props) => {
  const { theme } = props;
  const { typography } = theme;
  return `
  ${getTextBaseStyle};
  font-weight: ${typography.fontWeights.bold};
  align-self: ${'center'};
  padding-bottom: ${props.theme.spacing.ELEM_SPACING.SM};
  font-family: ${typography.fonts.secondary};
  font-size: ${typography.fontSizes.fs14};
  `;
};

const getSubHeadingStyle = (props) => {
  const { theme } = props;
  const { typography } = theme;
  return `
  ${getTextBaseStyle};
  font-size: ${typography.fontSizes.fs14};
  margin-bottom: 10px;
  align-self: ${'center'};
  `;
};

const ImageWrapperStyle = () => {
  return `
  margin-top: 16px;
  align-self: ${'center'};
  height:40px;
  width:30px;
  `;
};
const getDescriptionStyle = (props) => {
  const { theme } = props;
  const { typography, colorPalette } = theme;
  return `
  ${getTextBaseStyle};
  font-size: ${typography.fontSizes.fs12};
  color: ${colorPalette.text.primary};
  text-align: ${'center'};
  `;
};

const FavtHeadingStyle = () => {
  return `
    text-align:center;
    margin-bottom:14px;

  `;
};

const FavtSubHeadingStyle = () => {
  // width 57% specific for pixel perfect style of font size
  return `
    width:57%;
    text-align:center;
    margin :0 auto;
  `;
};

const ResetPassword = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: ${'center'};
`;

const FavtSubHeading = styled.Text`
  ${FavtSubHeadingStyle}
`;

const HeadingStyle = styled.Text`
  ${getHeadingStyle}
`;

const FavtHeading = styled.Text`
  ${FavtHeadingStyle}
`;

const SubHeadingStyle = styled.Text`
  ${getSubHeadingStyle}
`;
const DescriptionStyle = styled.Text`
  ${getDescriptionStyle}
`;

const ImageWrapper = styled.Text`
  ${ImageWrapperStyle}
`;

const WelcomeBackWrapper = styled.View`
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
`;

export {
  SectionStyle,
  HeadingStyle,
  SubHeadingStyle,
  DescriptionStyle,
  ImageWrapper,
  FavtHeading,
  FavtSubHeading,
  ResetPassword,
  WelcomeBackWrapper,
};
