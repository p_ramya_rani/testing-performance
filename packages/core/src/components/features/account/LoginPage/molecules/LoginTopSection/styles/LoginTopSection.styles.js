// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  ${(props) => (props.fullPage ? 'padding:100px 28px 0;' : '')}
  .bordered {
    border-bottom: 1px solid
      ${(props) => (props.fullPage ? props.theme.colors.WHITE : props.theme.colors.PRIMARY.BLUE)};
    ${(props) => (props.fullPage ? 'padding-bottom:0;margin: 0 auto;' : '')}
  }
  .logos {
    display: flex;
    justify-content: center;
    img {
      height: 57px;
      @media (max-width: 767px) {
        height: 45px;
      }
    }
  }
  .logo-seperator {
    width: 100px;
    height: 1px;
    transform: translate(0, 28px) rotate(90deg);
    background-color: ${(props) => props.theme.colors.PRIMARY.GRAY};
    display: inline-block;
    margin: 0 -15px;
  }
  .logo {
    width: 192px;
  }
  .my-rewards-img-wrapper {
    width: 192px;
  }
  .signuptext {
    padding-bottom: ${(props) =>
      props.isRememberedUser ? 0 : props.theme.spacing.ELEM_SPACING.XS};
    ${(props) =>
      props.fullPage ? `font-size:${props.theme.fonts.fontSize.body.bodytext.copy3}px` : ''}
  }
  .forgot-password-link {
    ${(props) =>
      props.fullPage ? `font-size:${props.theme.fonts.fontSize.body.bodytext.copy3}px` : ''}
  }
  .description-heading-first {
    ${(props) =>
      props.fullPage ? `font-size:${props.theme.fonts.fontSize.body.bodytext.copy3}px` : ''}
  }
  .description-heading-second {
    ${(props) =>
      props.fullPage
        ? `font-size:${props.theme.fonts.fontSize.body.bodytext.copy3}px;width: 270px;margin: 0 auto;`
        : ''}
  }
  .checkout-border {
    width: 25%;
    margin: 0 auto;
    border-bottom: 2px solid ${(props) => props.theme.colorPalette.blue[1000]};
  }
  .checkout_modal_heading_2 {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      display: block;
    }
  }

  .checkout_modal_heading_margin {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    }
  }
`;

export default styles;
