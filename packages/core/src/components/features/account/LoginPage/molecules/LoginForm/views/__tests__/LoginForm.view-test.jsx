// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { LoginFormFormVanilla } from '../LoginForm.view';

describe('LoginForm component', () => {
  it('should renders correctly', () => {
    const props = {
      labels: {
        login: {},
      },
      handleSubmit: () => {},
      className: 'test-class',
    };
    const component = shallow(<LoginFormFormVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders correctly with error', () => {
    const props = {
      labels: {
        login: {},
        registration: {},
      },
      handleSubmit: () => {},
      className: 'test-class',
      loginErrorMessage: 'test errror',
    };
    const component = shallow(<LoginFormFormVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders correctly with login button disabled', () => {
    const props = {
      labels: {
        login: {},
        registration: {},
      },
      isLoading: true,
    };
    const component = shallow(<LoginFormFormVanilla {...props} />);
    expect(component.find('.login-button-test-class').prop('disabled')).toEqual(true);
  });

  it('should renders correctly with login button enabled', () => {
    const props = {
      labels: {
        login: {},
        registration: {},
      },
      isLoading: false,
    };
    const component = shallow(<LoginFormFormVanilla {...props} />);
    expect(component.find('.login-button-test-class').prop('disabled')).toEqual(false);
  });
});
