// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { PropTypes } from 'prop-types';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import { getBrand, routerPush } from '@tcp/core/src/utils';
import Notification from '../../../../../../common/molecules/Notification';
import withStyles from '../../../../../../common/hoc/withStyles';
import ImageComp from '../../../../../../common/atoms/Image';
import { BodyCopy } from '../../../../../../common/atoms';
import BrandLogo from '../../../../../../../../../web/src/components/common/atoms/BrandLogo';
import { getIconPath, getImageFilePath } from '../../../../../../../utils';
import styles from '../styles/LoginTopSection.styles';
import Anchor from '../../../../../../common/atoms/Anchor';
import config from '../../../../../../../../../web/src/components/features/content/Header/config';

/**
 * @param {string} props : props for CustomIcon
 * @return {JSX} IconClass : Return jsx icon component
 * @desc This method based on the props generate icon component.
 */
const showForgotPassword = (
  showForgotPasswordForm,
  isResetPassCodeValidationEnabled,
  closeModal,
  closeOverlay
) => {
  if (isResetPassCodeValidationEnabled) {
    closeModal();
    closeOverlay();
    routerPush(`/change-password?page=reset`, `/change-password?page=reset`);
  } else {
    showForgotPasswordForm();
  }
};

const showRedirectFromResetMessage = (labels, redirectFromReset) => {
  return (
    <>
      {redirectFromReset && (
        <div className="elem-pl-LRG elem-pr-LRG elem-pt-LRG">
          <Notification
            status="success"
            colSize={{ large: 12, medium: 8, small: 6 }}
            message={getLabelValue(labels, 'lbl_reset_pass_succcessMsg', 'login')}
          />
        </div>
      )}
    </>
  );
};
const LoginTopSection = ({
  labels,
  className,
  isCanada,
  variation,
  resetLoginState,
  isRememberedUser,
  showForgotPasswordForm,
  closeModal,
  closeOverlay,
  isResetPassCodeValidationEnabled,
  redirectFromReset,
  fullPage,
}) => {
  const renderFavtandCheckout = () => {
    return (
      <React.Fragment>
        {variation === 'checkout' && (
          <>
            <BodyCopy
              fontSize={['fs28', 'fs28', 'fs36']}
              fontWeight="black"
              fontFamily="primary"
              textAlign="center"
              className="checkout_modal_heading_margin"
            >
              {getLabelValue(labels, 'lbl_login_checkout_modal_heading', 'login')}
            </BodyCopy>

            <BodyCopy
              className="checkout_modal_heading_2"
              component="span"
              fontSize="fs18"
              fontFamily="secondary"
              textAlign="center"
            >
              {getLabelValue(labels, 'lbl_login_checkout_modal_heading_1', 'login')}
            </BodyCopy>
            <BodyCopy
              component="span"
              fontSize="fs18"
              fontWeight="bold"
              fontFamily="secondary"
              textAlign="center"
            >
              {getLabelValue(labels, 'lbl_login_checkout_modal_heading_2', 'login')}
            </BodyCopy>
            <BodyCopy component="div" className="elem-pt-XS elem-pb-SM checkout-border" />
          </>
        )}
        {variation === 'favorites' && (
          <>
            <BodyCopy component="div" textAlign="center">
              <ImageComp
                width={25}
                height={23}
                src={getIconPath('favourites')}
                className="elem-mb-LRG"
                data-locator="login-HEART"
              />
            </BodyCopy>
            <BodyCopy
              fontSize="fs16"
              fontWeight="black"
              fontFamily="secondary"
              textAlign="center"
              className="favt_modal_heading"
              color="gray.700"
            >
              {getLabelValue(labels, 'lbl_login_favorites_modal_heading', 'login')}
            </BodyCopy>

            <BodyCopy component="span" fontSize="fs12" fontFamily="secondary" textAlign="center">
              {getLabelValue(labels, 'lbl_login_favorites_modal_heading_1', 'login')}
            </BodyCopy>
          </>
        )}
      </React.Fragment>
    );
  };
  const brand = getBrand();

  const getLogoElement = () => {
    return (
      <>
        {!isCanada && !(variation === 'favorites' || variation === 'checkout') && (
          <BodyCopy
            component="div"
            textAlign="center"
            className={`bordered elem-ml-XXL  elem-mr-XXL ${fullPage ? 'logos' : ''}`}
          >
            {fullPage && (
              <>
                {config[brand] && (
                  <>
                    <BrandLogo
                      alt={config[brand].alt}
                      className="header-brand__home-logo--brand"
                      dataLocator={config[brand].dataLocator}
                      imgSrc={`${getImageFilePath()}/${config[brand].imgSrc}`}
                    />
                  </>
                )}
                <span className="logo-seperator" />
                <BodyCopy component="div" className="my-rewards-img-wrapper">
                  <ImageComp
                    src={getIconPath('my-place-rewards')}
                    className="logo elem-mb-LRG"
                    data-locator="login-mprbanner"
                    alt={getLabelValue(labels, 'my_place_rewards', 'accessibility')}
                  />
                </BodyCopy>
              </>
            )}
            {!fullPage && (
              <ImageComp
                src={getIconPath('my-place-rewards')}
                className="logo elem-mb-LRG"
                data-locator="login-mprbanner"
                alt={getLabelValue(labels, 'my_place_rewards', 'accessibility')}
              />
            )}
          </BodyCopy>
        )}
      </>
    );
  };
  const headingLabel = fullPage ? 'lbl_login_loginCTA' : 'lbl_login_heading';
  return (
    <BodyCopy component="div" textAlign="center" className={className}>
      {getLogoElement()}
      {showRedirectFromResetMessage(labels, redirectFromReset)}
      {!(variation === 'favorites' || variation === 'checkout') && (
        <>
          <BodyCopy
            component="div"
            className="bordered elem-pt-MED elem-ml-XXL  elem-mr-XXL  elem-pb-LRG"
          >
            <BodyCopy fontSize="fs14" fontWeight="black" fontFamily="secondary" textAlign="center">
              {getLabelValue(labels, headingLabel, 'login')}
            </BodyCopy>

            {!isCanada && (
              <BodyCopy
                fontSize="fs13"
                fontFamily="secondary"
                textAlign="center"
                className="signuptext"
              >
                {getLabelValue(labels, 'lbl_login_subHeading', 'login')}
              </BodyCopy>
            )}
            {isCanada && (
              <BodyCopy fontSize="fs12" fontFamily="secondary" textAlign="center">
                {getLabelValue(labels, 'lbl_login_heading_2', 'login')}
              </BodyCopy>
            )}
            {!isRememberedUser && (
              <>
                <BodyCopy
                  fontFamily="secondary"
                  fontSize="fs12"
                  textAlign="center"
                  className="description-heading-first"
                >
                  {getLabelValue(labels, 'lbl_login_Description_heading_1', 'login')}
                </BodyCopy>

                <BodyCopy
                  fontFamily="secondary"
                  fontSize="fs12"
                  textAlign="center"
                  className="description-heading-second"
                >
                  {getLabelValue(labels, 'lbl_login_Description_heading_2', 'login')}
                </BodyCopy>
                <BodyCopy component="div" textAlign="center">
                  <Anchor
                    id="forgotPasswordForm"
                    fontSizeVariation="medium"
                    anchorVariation="primary"
                    underline
                    textAlign="center"
                    noLink
                    aria-label={`${getLabelValue(
                      labels,
                      'lbl_login_Description_clickhere',
                      'login'
                    )} ${getLabelValue(labels, 'lbl_login_Description_heading_3', 'login')}`}
                    onClick={(e) => {
                      e.preventDefault();
                      resetLoginState();
                      showForgotPassword(
                        showForgotPasswordForm,
                        isResetPassCodeValidationEnabled,
                        closeModal,
                        closeOverlay
                      );
                    }}
                    className="forgot-password-link"
                  >
                    {getLabelValue(labels, 'lbl_login_Description_clickhere', 'login')}
                  </Anchor>
                  <BodyCopy
                    fontFamily="secondary"
                    component="span"
                    textAlign="center"
                    fontSize="fs12"
                  >
                    {getLabelValue(labels, 'lbl_login_Description_heading_3', 'login')}
                  </BodyCopy>
                </BodyCopy>
              </>
            )}
          </BodyCopy>
        </>
      )}
      {renderFavtandCheckout()}
    </BodyCopy>
  );
};

LoginTopSection.propTypes = {
  labels: PropTypes.shape({
    lbl_login_heading: PropTypes.string,
    lbl_login_subHeading: PropTypes.string,
    lbl_login_subDescription: PropTypes.string,
  }),
  className: PropTypes.string.isRequired,
  isCanada: PropTypes.bool,
  variation: PropTypes.bool.isRequired,
  resetLoginState: PropTypes.bool.isRequired,
  isRememberedUser: PropTypes.bool.isRequired,
  showForgotPasswordForm: PropTypes.func,
  closeModal: PropTypes.func,
  closeOverlay: PropTypes.func,
  isResetPassCodeValidationEnabled: PropTypes.bool,
  redirectFromReset: PropTypes.bool,
  fullPage: PropTypes.bool,
};

LoginTopSection.defaultProps = {
  labels: {},
  isCanada: false,
  showForgotPasswordForm: () => {},
  closeModal: () => {},
  closeOverlay: () => {},
  isResetPassCodeValidationEnabled: false,
  redirectFromReset: false,
  fullPage: false,
};

export default withStyles(LoginTopSection, styles);
export { LoginTopSection as LoginTopSectionVanilla };
