// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import LoginView from '../LoginPage.view.native';

describe('LoginView Page', () => {
  it('should render correctly', () => {
    const props = {
      loginErrorMessage: '',
      navigation: {},
      showCheckoutModal: jest.fn(),
      showLogin: jest.fn(),
      userplccCardNumber: '',
      userplccCardId: '',
      toastMessage: jest.fn(),
      resetChangePasswordState: jest.fn(),
      onSubmit: jest.fn(),
      labels: {},
      initialValues: {},
      showRecaptcha: '',
      resetLoginState: '',
      SubmitForgot: '',
      showNotification: '',
      successFullResetEmail: '',
      resetForm: '',
      resetForgotPasswordErrorResponse: '',
      onCreateAccountClick: '',
      variation: false,
      handleContinueAsGuest: jest.fn(),
      loginError: false,
      updateHeader: jest.fn(),
    };
    const tree = shallow(<LoginView {...props} />);
    expect(tree).toMatchSnapshot();
  });
});
