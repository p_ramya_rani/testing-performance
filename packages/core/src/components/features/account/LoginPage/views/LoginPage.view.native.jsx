// 9fbef606107a605d69c0edbcd8029e5d
/**
 * These are temporary changes for a dummy login page
 */

import React from 'react';
import {
  trackForterNavigation,
  ForterNavigationType,
} from '@tcp/core/src/utils/forter.util.native';

import PropTypes from 'prop-types';
import LoginSection from '../organism/LoginSection';
import ScrollViewStyle from '../styles/LoginPage.style.native';
import {
  setUserLoginDetails,
  getUserLoginDetails,
  resetTouchPassword,
  touchIDCheck,
  isSupportedTouch,
} from '../container/loginUtils/keychain.utils.native';
import validatorMethods from '../../../../../utils/formValidation/validatorMethods';

class LoginView extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      setEmailid: '',
      getTouchStatus: false,
    };

    isSupportedTouch().then((biometryType) => {
      this.setState({ getTouchStatus: biometryType });
    });
  }

  componentDidMount() {
    const { onSubmit } = this.props;
    getUserLoginDetails().then((credentials) => {
      const emailValidator = validatorMethods.validEmail;
      const { username, password } = credentials;
      if (emailValidator(username)) {
        const userDetails = {
          emailAddress: username,
          password,
        };

        this.setState({ setEmailid: username });
        if (username && password) {
          isSupportedTouch().then((techAvailable) => {
            if (techAvailable) {
              touchIDCheck().then((touchIdResp) => {
                if (touchIdResp) {
                  trackForterNavigation('TouchLoginPage', ForterNavigationType.ACCOUNT, {}, true);
                  onSubmit(userDetails);
                }
              });
            }
          });
        }
      }
    });
  }

  onSubmitHandler = (formdata) => {
    const { onSubmit, plccEmail, plccCardId } = this.props;
    resetTouchPassword();
    const modfiedformdata = formdata;
    if (plccEmail) {
      modfiedformdata.emailAddress = plccEmail;
      modfiedformdata.fromPLCCModal = true;
      modfiedformdata.plccCardId = plccCardId;
    }
    onSubmit(modfiedformdata);
    if (formdata && formdata.toggleValue) {
      isSupportedTouch().then((biometryType) => {
        if (biometryType) {
          setUserLoginDetails(formdata.emailAddress, formdata.password);
          touchIDCheck();
        } else {
          setUserLoginDetails(formdata.emailAddress, '');
        }
      });
    } else {
      setUserLoginDetails(formdata.emailAddress, '');
    }
  };

  render() {
    const {
      labels,
      loginErrorMessage,
      initialValues,
      showRecaptcha,
      resetLoginState,
      SubmitForgot,
      showNotification,
      successFullResetEmail,
      resetForm,
      resetForgotPasswordErrorResponse,
      onCreateAccountClick,
      navigation,
      variation,
      handleContinueAsGuest,
      loginError,
      showErrorModal,
      showCheckoutModal,
      showLogin,
      userplccCardNumber,
      userplccCardId,
      updateHeader,
      isResetPassCodeValidationEnabled,
      toastMessage,
      fromResetView,
      resetChangePasswordState,
      onRequestClose,
      espotShowResetPassword,
      isLoading,
      loyaltyPageName,
      showOnPlccModal,
      plccEmail,
      isRtpsFlow,
    } = this.props;
    const { setEmailid, getTouchStatus } = this.state;
    return (
      <ScrollViewStyle keyboardShouldPersistTaps="handled">
        <LoginSection
          isLoading={isLoading}
          getTouchStatus={getTouchStatus}
          setEmailid={setEmailid}
          onSubmit={this.onSubmitHandler}
          labels={labels}
          loginError={loginError}
          showErrorModal={showErrorModal}
          loginErrorMessage={loginErrorMessage}
          initialValues={initialValues}
          showRecaptcha={showRecaptcha}
          resetLoginState={resetLoginState}
          SubmitForgot={SubmitForgot}
          showNotification={showNotification}
          successFullResetEmail={successFullResetEmail}
          resetForm={resetForm}
          resetForgotPasswordErrorResponse={resetForgotPasswordErrorResponse}
          onCreateAccountClick={onCreateAccountClick}
          navigation={navigation}
          variation={variation}
          handleContinueAsGuest={handleContinueAsGuest}
          showCheckoutModal={showCheckoutModal}
          showLogin={showLogin}
          userplccCardNumber={userplccCardNumber}
          userplccCardId={userplccCardId}
          updateHeader={updateHeader}
          toastMessage={toastMessage}
          resetChangePasswordState={resetChangePasswordState}
          fromResetView={fromResetView}
          onRequestClose={onRequestClose}
          isResetPassCodeValidationEnabled={isResetPassCodeValidationEnabled}
          espotShowResetPassword={espotShowResetPassword}
          loyaltyPageName={loyaltyPageName}
          showOnPlccModal={showOnPlccModal}
          plccEmail={plccEmail}
          isRtpsFlow={isRtpsFlow}
        />
      </ScrollViewStyle>
    );
  }
}
LoginView.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  labels: PropTypes.shape({}).isRequired,
  loginErrorMessage: PropTypes.string,
  initialValues: PropTypes.shape({}).isRequired,
  showRecaptcha: PropTypes.string.isRequired,
  resetLoginState: PropTypes.string.isRequired,
  SubmitForgot: PropTypes.string.isRequired,
  showNotification: PropTypes.string.isRequired,
  successFullResetEmail: PropTypes.string.isRequired,
  resetForm: PropTypes.string.isRequired,
  resetForgotPasswordErrorResponse: PropTypes.string.isRequired,
  onCreateAccountClick: PropTypes.string.isRequired,
  navigation: PropTypes.shape({}),
  variation: PropTypes.bool.isRequired,
  handleContinueAsGuest: PropTypes.func.isRequired,
  loginError: PropTypes.bool.isRequired,
  showErrorModal: PropTypes.bool,
  fromResetView: PropTypes.bool,
  showCheckoutModal: PropTypes.func,
  showLogin: PropTypes.func,
  userplccCardNumber: PropTypes.string,
  userplccCardId: PropTypes.string,
  isResetPassCodeValidationEnabled: PropTypes.string,
  updateHeader: PropTypes.func.isRequired,
  toastMessage: PropTypes.func,
  resetChangePasswordState: PropTypes.func,
  onRequestClose: PropTypes.func,
  espotShowResetPassword: PropTypes.bool,
  isLoading: PropTypes.bool.isRequired,
  loyaltyPageName: PropTypes.string,
  showOnPlccModal: PropTypes.bool,
  plccEmail: PropTypes.string,
};

LoginView.defaultProps = {
  loginErrorMessage: '',
  showErrorModal: false,
  isResetPassCodeValidationEnabled: false,
  fromResetView: false,
  navigation: {},
  showCheckoutModal: () => {},
  showLogin: () => {},
  userplccCardNumber: '',
  userplccCardId: '',
  toastMessage: () => {},
  resetChangePasswordState: () => {},
  onRequestClose: () => {},
  espotShowResetPassword: false,
  loyaltyPageName: 'account',
  showOnPlccModal: false,
  plccEmail: '',
};

export default LoginView;
