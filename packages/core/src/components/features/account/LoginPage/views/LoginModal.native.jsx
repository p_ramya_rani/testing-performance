// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import { SafeAreaView } from 'react-native';
import PropTypes from 'prop-types';
import ModalNative from '@tcp/core/src/components/common/molecules/Modal';
import LoginPageContainer from '@tcp/core/src/components/features/account/LoginPage';
import CreateAccount from '../../CreateAccount';
import { getLabelValue } from '../../../../../utils';

class OpenLoginModal extends PureComponent {
  constructor(props) {
    super(props);
    const modalLabel =
      props.component === 'login'
        ? getLabelValue(props.labels, 'lbl_login_loginCTA', 'login', 'global')
        : getLabelValue(props.labels, 'lbl_login_createAccountCTA', 'login', 'global');
    this.state = {
      currentForm: 'login',
      component: props.component || 'login',
      horizontalBar: true,
      modalHeaderLbl: modalLabel,
      showHeader: true,
    };
  }

  onClose = () => {
    const { setLoginModalMountState } = this.props;
    setLoginModalMountState({ state: false });
  };

  openForgotPasswordModal = (params) => {
    const { setLoginModalMountState } = this.props;
    this.setState(
      {
        component: params.component,
        currentForm: params.componentProps ? params.componentProps.currentForm : '',
      },
      () => {
        setLoginModalMountState({ state: true });
      }
    );
  };

  showCreateAccountModal = () => {
    const { labels } = this.props;
    this.setState({
      component: 'createAccount',
      modalHeaderLbl: getLabelValue(labels, 'lbl_login_createAccountCTA', 'login', 'global'),
    });
  };

  showLoginModal = () => {
    const { labels } = this.props;
    this.setState({
      component: 'login',
      modalHeaderLbl: getLabelValue(labels, 'lbl_login_loginCTA', 'login', 'global'),
    });
  };

  updateHeader = (source = '') => {
    if (source === 'forgotpassword') {
      this.setState({
        showHeader: false,
      });
    } else if (source === 'login') {
      this.setState({
        showHeader: true,
        modalHeaderLbl: 'Login',
        horizontalBar: true,
      });
    } else {
      this.setState({
        modalHeaderLbl: ' ',
        horizontalBar: false,
      });
    }
  };

  render() {
    const {
      openState,
      variation,
      handleContinueAsGuest,
      handleAfterLogin,
      espotShowResetPassword,
    } = this.props;

    const { currentForm, component, modalHeaderLbl, horizontalBar, showHeader } = this.state;

    return (
      <SafeAreaView>
        <ModalNative
          heading={modalHeaderLbl}
          isOpen={openState}
          onRequestClose={this.onClose}
          horizontalBar={horizontalBar}
          showHeader={showHeader}
          roundedModal={false}
          showTextForClose={false}
        >
          {component === 'login' ? (
            <LoginPageContainer
              variation={variation}
              currentForm={currentForm}
              closeModal={this.onClose}
              setLoginModalMountState={this.openForgotPasswordModal}
              handleContinueAsGuest={handleContinueAsGuest}
              handleAfterLogin={handleAfterLogin}
              updateHeader={this.updateHeader}
              showCheckoutModal={this.showCreateAccountModal}
              espotShowResetPassword={espotShowResetPassword}
              onRequestClose={this.onClose}
            />
          ) : (
            <CreateAccount
              currentForm={currentForm}
              setLoginModalMountState={this.openForgotPasswordModal}
              showLogin={this.showLoginModal}
              onRequestClose={this.onClose}
            />
          )}
        </ModalNative>
      </SafeAreaView>
    );
  }
}

OpenLoginModal.propTypes = {
  setLoginModalMountState: PropTypes.bool.isRequired,
  handleContinueAsGuest: PropTypes.func,
  handleAfterLogin: PropTypes.func,
  component: PropTypes.node.isRequired,
  labels: PropTypes.shape({}).isRequired,
  openState: PropTypes.bool.isRequired,
  variation: PropTypes.string.isRequired,
  espotShowResetPassword: PropTypes.bool,
};

OpenLoginModal.defaultProps = {
  handleContinueAsGuest: () => {},
  handleAfterLogin: () => {},
  espotShowResetPassword: false,
};

export default OpenLoginModal;
