// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import OpenLoginModal from '../LoginModal.native';

describe('LoginView Page', () => {
  const props = {
    openState: false,
    variation: '',
    handleContinueAsGuest: jest.fn(),
    handleAfterLogin: jest.fn(),
    onDeleteCard: jest.fn(),
    data: {
      description: '',
    },
    setLoginModalMountState: jest.fn(),
  };
  it('should render correctly', () => {
    const tree = shallow(<OpenLoginModal {...props} />);
    expect(tree).toMatchSnapshot();
  });

  it('test openForgotPasswordModal', () => {
    const tree = shallow(<OpenLoginModal {...props} />);
    tree.instance().openForgotPasswordModal({
      params: {
        component: '',
      },
    });
    expect(props.setLoginModalMountState).toHaveBeenCalledTimes(1);
  });

  it('test onClose', () => {
    const tree = shallow(<OpenLoginModal {...props} />);
    tree.instance().onClose();
    expect(props.setLoginModalMountState).toHaveBeenCalledTimes(2);
  });
});
