// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { LoginSectionVanilla } from '../LoginSection.view.native';

describe('LoginSection component', () => {
  const props = {
    onSubmit: () => {},
    labels: {
      lbl_login_createAccountCTA: 'Create Account',
      lbl_login_createAccountHelp: 'Help',
      lbl_login_createAccountHelp_1: 'Don\u0027t have an account? Create one now to',
      lbl_login_createAccountHelp_2: 'start earning points!',
    },
    initialValues: {},
    resetPassword: true,
  };
  it('should renders correctly', () => {
    const component = shallow(<LoginSectionVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('should renders correctly with OTP Reset Pwd View', () => {
    const component = shallow(<LoginSectionVanilla {...props} isResetPassCodeValidationEnabled />);
    expect(component).toMatchSnapshot();
  });
  it('should renders correctly with OTP Reset Pwd View and Login Error', () => {
    const component = shallow(
      <LoginSectionVanilla
        {...props}
        isResetPassCodeValidationEnabled
        loginError="Login error"
        showErrorModal
      />
    );
    expect(component).toMatchSnapshot();
  });
});
