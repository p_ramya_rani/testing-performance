// 9fbef606107a605d69c0edbcd8029e5d
import React, { Fragment, PureComponent } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import createThemeColorPalette from '@tcp/core/styles/themes/createThemeColorPalette';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import CustomButton from '../../../../../../common/atoms/ButtonRedesign';
import BodyCopy from '../../../../../../common/atoms/BodyCopy';
import LoginForm from '../../../molecules/LoginForm';
import LoginTopSection from '../../../molecules/LoginTopSection';
import ForgotPasswordContainer from '../../../../ForgotPassword';
import ResetPassword from '../../../../ResetPassword';
import LoginErrorModal from '../../LoginErrorModal';
import {
  FormStyle,
  FormStyleView,
  LoginFormInfoStyle,
} from '../../../molecules/LoginForm/LoginForm.style.native';
import names from '../../../../../../../constants/eventsName.constants';
import ClickTracker from '../../../../../../../../../mobileapp/src/components/common/atoms/ClickTracker';

const colorPallete = createThemeColorPalette();
class LoginSection extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      resetPassword: false,
      newPassword: false,
    };
    this.queryParams = {};
  }

  componentDidMount() {
    const { espotShowResetPassword } = this.props;
    if (espotShowResetPassword) this.showForgotPassword();
  }

  componentDidUpdate() {
    const { newPassword } = this.state;
    if (!newPassword) this.navigateToResetPassword();
  }

  navigateToResetPassword = () => {
    const { navigation, isResetPassCodeValidationEnabled } = this.props;
    if (navigation && navigation.state) {
      const {
        state: { params },
      } = navigation;
      if (params) {
        const { component, email = '', page, fromDeepLink, logonPasswordOld, em } = params;
        if (component && component === 'change-password') {
          if (fromDeepLink && !email && isResetPassCodeValidationEnabled) {
            this.showForgotPassword();
          } else {
            this.showNewPassword();
          }
          this.queryParams = {
            email,
            page,
            logonPasswordOld,
            em,
          };
          navigation.setParams({
            component: null,
            logonPasswordOld: null,
            em: null,
            email: null,
            page: null,
            navigatedFromDeepLink: null,
          }); // reset the params
        }
      }
    }
  };

  toggleCheckoutModal = () => {
    const { showCheckoutModal } = this.props;
    showCheckoutModal();
  };

  showForgotPassword = () => {
    const { resetPassword } = this.state;
    const { updateHeader } = this.props;

    updateHeader('forgotpassword');
    this.setState({
      resetPassword: !resetPassword,
    });
  };

  showNewPassword = () => {
    const { newPassword, resetPassword } = this.state;
    this.setState({
      newPassword: !newPassword,
    });
    if (resetPassword) this.showForgotPassword(); // if user is on forgot password then dismiss it
  };

  showCreateAccountCTA = (isResetPassCodeValidationEnabled, resetPassword, newPassword) =>
    !(isResetPassCodeValidationEnabled && (resetPassword || newPassword));

  render() {
    const {
      onSubmit,
      labels,
      loginErrorMessage,
      initialValues,
      showRecaptcha,
      loginInfo,
      getUserInfo,
      SubmitForgot,
      showNotification,
      resetLoginState,
      successFullResetEmail,
      resetForm,
      resetForgotPasswordErrorResponse,
      navigation,
      variation,
      handleContinueAsGuest,
      loginError,
      showErrorModal,
      showLogin,
      setEmailid,
      getTouchStatus,
      userplccCardNumber,
      userplccCardId,
      updateHeader,
      isResetPassCodeValidationEnabled,
      toastMessage,
      fromResetView,
      resetChangePasswordState,
      onRequestClose,
      isLoading,
      loyaltyPageName,
      showOnPlccModal,
      plccEmail,
      isRtpsFlow,
    } = this.props;

    const { email = '' } = this.queryParams;
    const { resetPassword, newPassword } = this.state;
    return (
      <View>
        {!resetPassword && !newPassword && (
          <Fragment>
            {!showOnPlccModal && (
              <LoginTopSection
                showForgotPasswordForm={this.showForgotPassword}
                variation={variation}
                labels={labels}
                updateHeader={updateHeader}
              />
            )}
            <LoginForm
              getTouchStatus={getTouchStatus}
              setEmailid={setEmailid}
              onSubmit={onSubmit}
              labels={labels}
              loginError={loginError}
              loginErrorMessage={loginErrorMessage}
              initialValues={initialValues}
              showRecaptcha={showRecaptcha}
              showForgotPasswordForm={this.showForgotPassword}
              resetForm={resetForm}
              variation={variation}
              navigation={navigation}
              handleContinueAsGuest={handleContinueAsGuest}
              isResetPassCodeValidationEnabled={isResetPassCodeValidationEnabled}
              userplccCardNumber={userplccCardNumber}
              fromResetView={fromResetView}
              userplccCardId={userplccCardId}
              toastMessage={toastMessage}
              isLoading={isLoading}
              loyaltyPageName={loyaltyPageName}
              showOnPlccModal={showOnPlccModal}
              plccEmail={plccEmail}
              isRtpsFlow={isRtpsFlow}
            />
          </Fragment>
        )}

        {loginError && showErrorModal && (
          <LoginErrorModal showModal={loginError && showErrorModal} labels={labels} />
        )}

        {resetPassword && (
          <ForgotPasswordContainer
            onRequestClose={onRequestClose}
            SubmitForgot={SubmitForgot}
            loginInfo={loginInfo}
            getUserInfo={getUserInfo}
            showNotification={showNotification}
            showForgotPasswordForm={this.showForgotPassword}
            resetForgotPasswordErrorResponse={resetForgotPasswordErrorResponse}
            labels={labels}
            resetPassword={resetPassword}
            resetLoginState={resetLoginState}
            successFullResetEmail={successFullResetEmail}
            showNewPassword={this.showNewPassword}
            showLogin={showLogin}
            isResetPassCodeValidationEnabled={isResetPassCodeValidationEnabled}
            updateHeader={updateHeader}
            toastMessage={toastMessage}
            showOnPlccModal={showOnPlccModal}
          />
        )}

        {newPassword && (
          <ResetPassword
            onRequestClose={onRequestClose}
            labels={labels.password}
            queryParams={this.queryParams}
            showLogin={showLogin}
            showNewPassword={this.showNewPassword}
            logonId={email}
            isResetPassCodeValidationEnabled={isResetPassCodeValidationEnabled}
            updateHeader={updateHeader}
            resetChangePasswordState={resetChangePasswordState}
          />
        )}
        {this.showCreateAccountCTA(isResetPassCodeValidationEnabled, resetPassword, newPassword) &&
          !showOnPlccModal && (
            <FormStyleView>
              <LoginFormInfoStyle>
                <BodyCopy
                  fontFamily="secondary"
                  fontWeight="semibold"
                  textAlign="center"
                  fontSize="fs14"
                  color="gray.900"
                  text={getLabelValue(labels, 'lbl_login_createAccountHelp_1_v1', 'login')}
                />
                <BodyCopy
                  fontFamily="secondary"
                  fontWeight="semibold"
                  fontSize="fs14"
                  textAlign="center"
                  color="gray.900"
                  text={getLabelValue(labels, 'lbl_login_createAccountHelp_2_v1', 'login')}
                />
              </LoginFormInfoStyle>

              <ClickTracker
                as={CustomButton}
                name={names.screenNames.loyalty_join_click}
                clickData={{
                  customEvents: ['event125'],
                  loyaltyLocation: loyaltyPageName,
                }}
                module="global"
                color={colorPallete.primary.navyBlue}
                fill="WHITE"
                type="submit"
                data-locator=""
                fontSize="fs16"
                borderRadius="16px"
                text={getLabelValue(labels, 'lbl_login_createAccountCTA', 'login')}
                onPress={this.toggleCheckoutModal}
                fontFamily="secondary"
                fontWeight="bold"
                showBorder
                paddings="8px"
              />
            </FormStyleView>
          )}
      </View>
    );
  }
}

LoginSection.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  labels: PropTypes.shape({}),
  loginErrorMessage: PropTypes.string,
  initialValues: PropTypes.shape({}).isRequired,
  userplccCardNumber: PropTypes.string,
  userplccCardId: PropTypes.string,
  navigation: PropTypes.shape({}).isRequired,
  showCheckoutModal: PropTypes.func.isRequired,
  updateHeader: PropTypes.func.isRequired,
  showRecaptcha: PropTypes.bool.isRequired,
  loginInfo: PropTypes.shape({}).isRequired,
  getUserInfo: PropTypes.func.isRequired,
  SubmitForgot: PropTypes.shape({}).isRequired,
  showNotification: PropTypes.bool.isRequired,
  resetLoginState: PropTypes.func.isRequired,
  successFullResetEmail: PropTypes.bool.isRequired,
  resetForm: PropTypes.func.isRequired,
  resetForgotPasswordErrorResponse: PropTypes.func.isRequired,
  variation: PropTypes.string.isRequired,
  handleContinueAsGuest: PropTypes.bool.isRequired,
  loginError: PropTypes.string.isRequired,
  showErrorModal: PropTypes.bool.isRequired,
  showLogin: PropTypes.bool.isRequired,
  setEmailid: PropTypes.string.isRequired,
  getTouchStatus: PropTypes.func.isRequired,
  toastMessage: PropTypes.string.isRequired,
  resetChangePasswordState: PropTypes.func.isRequired,
  onRequestClose: PropTypes.func.isRequired,
  isResetPassCodeValidationEnabled: PropTypes.bool,
  fromResetView: PropTypes.bool,
  espotShowResetPassword: PropTypes.bool,
  isLoading: PropTypes.bool.isRequired,
  loyaltyPageName: PropTypes.string,
  showOnPlccModal: PropTypes.bool,
  plccEmail: PropTypes.string,
};

LoginSection.defaultProps = {
  loginErrorMessage: '',
  labels: {
    login: {
      lbl_login_createAccountCTA: '',
      lbl_login_createAccountHelp: '',
      lbl_login_createAccountHelp_1: 'Don\u0027t have an account? Create one now to',
      lbl_login_createAccountHelp_2: 'start earning points!',
    },
  },
  userplccCardNumber: '',
  userplccCardId: '',
  isResetPassCodeValidationEnabled: false,
  fromResetView: false,
  espotShowResetPassword: false,
  loyaltyPageName: 'account',
  showOnPlccModal: false,
  plccEmail: '',
};

export default withStyles(LoginSection, FormStyle);
export { LoginSection as LoginSectionVanilla };
