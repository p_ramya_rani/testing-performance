// 9fbef606107a605d69c0edbcd8029e5d 
import React, { useState } from 'react';
import { TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native';
import PropTypes from 'prop-types';
import Modal from '@tcp/core/src/components/common/molecules/Modal';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import Button from '@tcp/core/src/components/common/atoms/Button';
import { UrlHandler } from '@tcp/core/src/utils/utils.app';
import { getLabelValue, getAPIConfig } from '@tcp/core/src/utils';
import {
  StyledViewWrapper,
  ButtonWrapper,
  StyledHeader,
  ImageWrapper,
  StyledTouchableOpacity,
  StyledText,
  StyledCrossImage,
} from './styles/LoginErrorModal.style.native';

const styles = {
  TransparentModalContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
};

const closeIcon = require('../../../../../../../../../mobileapp/src/assets/images/close.png');

const LoginErrorModal = ({ showModal, labels }) => {
  const [show, setShow] = useState(showModal);
  const closeModal = () => {
    setShow(false);
  };
  const okClicked = () => {
    const { webAppDomain, siteId } = getAPIConfig();
    UrlHandler(`${webAppDomain}/${siteId}/home/login`);
    closeModal();
  };
  const cancel = getLabelValue(labels, 'lbl_login_errormodal_cancel', 'login');
  const ok = getLabelValue(labels, 'lbl_login_errormodal_ok', 'login');
  const title = getLabelValue(labels, 'lbl_login_errormodal_title', 'login');
  const message = getLabelValue(labels, 'lbl_login_errormodal_message', 'login');

  return (
    <Modal isOpen={show} customTransparent>
      <TouchableOpacity
        accessibilityLabel={cancel}
        accessibilityRole="none"
        onPress={closeModal}
        style={styles.TransparentModalContainer}
      >
        <StyledViewWrapper>
          <TouchableWithoutFeedback accessibilityRole="none">
            <View>
              <ImageWrapper>
                <StyledTouchableOpacity onPress={closeModal}>
                  <StyledCrossImage source={closeIcon} />
                </StyledTouchableOpacity>
              </ImageWrapper>
              <StyledHeader>
                <BodyCopy
                  fontSize="fs16"
                  fontFamily="secondary"
                  fontWeight="extrabold"
                  text={title}
                  textAlign="center"
                />
              </StyledHeader>
              <StyledText>
                <BodyCopy
                  fontSize="fs16"
                  fontFamily="secondary"
                  fontWeight="regular"
                  text={message}
                  textAlign="center"
                />
              </StyledText>
              <ButtonWrapper>
                <Button fill="BLUE" type="submit" color="white" onPress={okClicked} text={ok} />
              </ButtonWrapper>
              <ButtonWrapper>
                <Button
                  fill="WHITE"
                  type="submit"
                  color="gray"
                  fontWeight="extrabold"
                  onPress={closeModal}
                  text={cancel}
                />
              </ButtonWrapper>
            </View>
          </TouchableWithoutFeedback>
        </StyledViewWrapper>
      </TouchableOpacity>
    </Modal>
  );
};

LoginErrorModal.propTypes = {
  showModal: PropTypes.bool.isRequired,
  labels: PropTypes.shape({}).isRequired,
};

export default LoginErrorModal;
