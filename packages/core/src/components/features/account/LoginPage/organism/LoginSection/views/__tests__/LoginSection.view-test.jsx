// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { routerPush } from '@tcp/core/src/utils';
import { LoginSectionVanilla } from '../LoginSection.view';
import ForgotPasswordContainer from '../../../../../ForgotPassword/container/ForgotPassword.container';
import ResetPassword from '../../../../../ResetPassword';

jest.mock('@tcp/core/src/utils', () => ({
  isMobileApp: jest.fn(),
  routerPush: jest.fn(),
  getAPIConfig: jest.fn(),
  isClient: jest.fn(),
  getIconPath: jest.fn(),
  getStaticFilePath: jest.fn(),
  isCanada: jest.fn(),
  scrollPage: jest.fn(),
  getUrlParameter: jest.fn(),
  getImageFilePath: jest.fn(),
}));

describe('LoginSection component', () => {
  const mockedRouterPush = jest.fn();
  it('should renders correctly', () => {
    const props = {
      onSubmit: () => {},
      labels: {
        login: {},
      },
      logoutlabels: {
        CREATE_ACC_SIGN_OUT: 'Sign Out',
      },
      isRememberedUser: false,
      initialValues: {},
    };
    const component = shallow(<LoginSectionVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders correctly if remmemberme user', () => {
    const props = {
      onSubmit: () => {},
      labels: {
        login: {},
      },
      logoutlabels: {
        CREATE_ACC_SIGN_OUT: 'Sign Out',
      },
      isRememberedUser: true,
      initialValues: {},
    };
    const component = shallow(<LoginSectionVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should render ForgotPasswordContainer if currentForm prop is forgotPassword', () => {
    const props = {
      onSubmit: () => {},
      labels: {
        login: {},
      },
      initialValues: {},
      currentForm: 'forgotPassword',
      logoutlabels: {
        CREATE_ACC_SIGN_OUT: 'Sign Out',
      },
      isRememberedUser: false,
    };
    const component = shallow(<LoginSectionVanilla {...props} />);
    expect(component.find(ForgotPasswordContainer)).toHaveLength(1);
  });

  it('should render ResetPassword if currentForm props is resetPassword', () => {
    const props = {
      onSubmit: () => {},
      labels: {
        login: {},
      },
      initialValues: {},
      currentForm: 'resetPassword',
      logoutlabels: {
        CREATE_ACC_SIGN_OUT: 'Sign Out',
      },
      isRememberedUser: false,
    };
    const component = shallow(<LoginSectionVanilla {...props} />);
    expect(component.find(ResetPassword)).toHaveLength(1);
  });

  it('should not call openLoginOverlay from  showCreateAccountForm method', () => {
    const props = {
      onSubmit: () => {},
      labels: {
        login: {},
      },
      logoutlabels: {
        CREATE_ACC_SIGN_OUT: 'Sign Out',
      },
      isRememberedUser: false,
      initialValues: {},
      openOverlay: jest.fn(),
      onClose: jest.fn(),
      closeOverlay: jest.fn(),
      closeModal: jest.fn(),
    };
    const tree = shallow(<LoginSectionVanilla {...props} fullPageAuthEnabled />);
    routerPush.mockImplementation(mockedRouterPush);
    const componentInstance = tree.instance();
    componentInstance.showCreateAccountForm();
    expect(props.openOverlay).not.toBeCalled();
  });

  it('should call openLoginOverlay from  showCreateAccountForm method', () => {
    const props = {
      onSubmit: () => {},
      labels: {
        login: {},
      },
      logoutlabels: {
        CREATE_ACC_SIGN_OUT: 'Sign Out',
      },
      isRememberedUser: false,
      initialValues: {},
      openOverlay: jest.fn(),
      onClose: jest.fn(),
      closeOverlay: jest.fn(),
      closeModal: jest.fn(),
    };
    const tree = shallow(<LoginSectionVanilla {...props} />);
    routerPush.mockImplementation(mockedRouterPush);
    const componentInstance = tree.instance();
    componentInstance.showCreateAccountForm();
    expect(props.openOverlay).toBeCalled();
  });
});
