// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import LoginErrorModal from '../LoginErrorModal.view.native';

describe('LoginErrorModal component default state', () => {
  const props = {
    labels: {},
    showModal: false,
  };
  it('should renders correctly', () => {
    const component = shallow(<LoginErrorModal {...props} />);
    expect(component).toMatchSnapshot();
  });
});

describe('LoginErrorModal component opened state', () => {
  const props = {
    labels: {},
    showModal: true,
  };
  it('should renders correctly', () => {
    const component = shallow(<LoginErrorModal {...props} />);
    expect(component).toMatchSnapshot();
  });
});
