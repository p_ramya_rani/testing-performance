// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  ${props => (props.fullPage ? 'max-width:450px;margin:0 auto;' : '')}
  .login-button-test-class {
    ${props => (props.fullPage ? 'width: 210px;display: block;margin: 10px auto;' : '')}
  }
  .elem-pl-LRG {
    padding-left: 15px;
  }
  .elem-pr-LRG {
    padding-right: 15px;
  }
  .checkoutForm {
    padding-left: 0px;
    padding-right: 0px;
    margin: 0 auto;
    @media ${props => props.theme.mediaQuery.largeOnly} {
      padding-left: 74px;
      padding-right: 74px;
    }
  }
  .border {
    ${props => (props.fullPage ? '' : `border-top: 1px solid ${props.theme.colors.BORDER.BLUE};`)}
    width: 91%;
    margin: 0 auto;
    p {
      max-width: 227px;
      margin: 0 auto;
      ${props =>
        props.fullPage
          ? `max-width:260px;font-size:14px;font-weight: ${
              props.theme.typography.fontWeights.semibold
            };`
          : ''}
    }
  }
  .create-acc-cta {
    margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.SM};
    ${props => (props.fullPage ? 'width: 210px;display: block;margin: 10px auto;' : '')}
  }
  .border.elem-pt-MED {
    padding-top: 27px;
  }
  .border.elem-pt-LRG {
    padding-bottom: 27px;
  }
  .password-required-msg {
    .reset-password-heading,
    .reset-password-list {
      font-size: ${props => props.theme.fonts.fontSize.body.bodytext.copy2}px;
      font-weight: ${props => props.theme.fonts.fontWeight.normal};
    }
    .reset-password-note {
      font-size: ${props => props.theme.fonts.fontSize.body.bodytext.copy1}px;
      text-align: center;
    }

    li:before {
      content: '-';
      text-indent: -5px;
    }
  }
  .remember-me-text {
    ${props => (props.fullPage ? 'font-size:12px;' : '')}
  }
  .remember-me-sub-text {
    ${props => (props.fullPage ? 'font-size:12px;' : '')}
  }
  .remember-me-checkbox {
    ${props => (props.fullPage ? 'margin:40px 0;' : '')}
  }
  ${props =>
    props.fullPage
      ? '.rightAlignedContent {width: auto;top: 20px;.reset-tooltip {display: none;}a {font-size: 16px;}}'
      : ''}
`;

export default styles;
