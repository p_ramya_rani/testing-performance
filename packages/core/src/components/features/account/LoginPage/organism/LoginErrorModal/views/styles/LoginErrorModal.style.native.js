// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

export const StyledViewWrapper = styled.View`
  padding: 15px;
  align-content: center;
  padding-bottom: 0;
  background-color: white;
  width: 305px;
`;

export const StyledHeader = styled.View`
  margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.MED};
  justify-content: center;
  align-content: center;
`;

export const StyledText = styled.View`
  margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.LRG};
`;

export const ButtonWrapper = styled.View`
  margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.LRG};
  min-width: 225px;
  align-self: center;
`;

export const ImageWrapper = styled.View`
  width: 20%;
  align-self: flex-end;
`;

export const StyledTouchableOpacity = styled.TouchableOpacity`
  align-items: flex-end;
  padding: 0px 0px ${props => props.theme.spacing.ELEM_SPACING.SM}
    ${props => props.theme.spacing.ELEM_SPACING.SM};
`;

export const StyledCrossImage = styled.Image`
  width: ${props => props.theme.spacing.ELEM_SPACING.MED};
`;
