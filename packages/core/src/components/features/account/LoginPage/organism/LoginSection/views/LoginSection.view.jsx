// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { getCreateAccountPayload, setLoyaltyLocation } from '@tcp/core/src/constants/analytics';
import ClickTracker from '@tcp/web/src/components/common/atoms/ClickTracker';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import { Anchor } from '@tcp/core/src/components/common/atoms';
import { routerPush } from '@tcp/core/src/utils';
import LogOutPageContainer from '../../../../Logout/container/LogOut.container';
import LoginForm from '../../../molecules/LoginForm';
import { BodyCopy } from '../../../../../../common/atoms';
import LoginTopSection from '../../../molecules/LoginTopSection';
import ForgotPasswordContainer from '../../../../ForgotPassword/container/ForgotPassword.container';
import ResetPassword from '../../../../ResetPassword';
import Row from '../../../../../../common/atoms/Row';
import Col from '../../../../../../common/atoms/Col';
import Button from '../../../../../../common/atoms/Button';
import styles from './styles/LoginSection.styles';
import constants from '../../../LoginPage.constants';
import { isCanada, scrollPage, getUrlParameter } from '../../../../../../../utils';

class LoginSection extends React.PureComponent {
  constructor(props) {
    super(props);
    this.isCanada = isCanada();
    this.parentRef = React.createRef();
    this.state = {
      redirectFromReset: false,
    };
  }

  componentDidMount() {
    const redirectFromReset = getUrlParameter('redirectFromReset');
    if (redirectFromReset && redirectFromReset.length > 0) {
      this.setState({ redirectFromReset: true });
      routerPush('/home?target=login', '/home/login', { shallow: true });
    }
    if (this.parentRef && this.parentRef.current) {
      setTimeout(() => {
        this.parentRef.current.scrollIntoView();
      }, 0);
    }
  }

  componentDidUpdate(prevProps) {
    const { currentForm } = this.props;

    if (currentForm !== prevProps.currentForm) {
      scrollPage();
    }
  }

  showForgotPasswordForm = () => {
    const { openModal } = this.props;
    openModal({
      component: 'login',
      componentProps: {
        currentForm: constants.PAGE_TYPE.FORGOT_PASSWORD,
      },
    });
  };

  showLoginForm = () => {
    const { openModal } = this.props;
    openModal({
      component: 'login',
      componentProps: {
        currentForm: constants.PAGE_TYPE.LOGIN,
      },
    });
  };

  showCreateAccountForm = () => {
    const {
      onClose,
      openOverlay,
      closeModal,
      fullPage,
      closeOverlay,
      fullPageAuthEnabled,
      variation,
    } = this.props;
    const { pathname = 'referrer' } = (window && window.location) || {};
    if (variation !== 'favorites' && (fullPage || fullPageAuthEnabled)) {
      closeOverlay();
      routerPush(`/home?target=register&successtarget=${pathname}`, '/home/register');
    } else {
      openOverlay({
        component: 'createAccount',
        variation: 'primary',
      });
    }
    onClose();
    closeModal();
  };

  getSignOutSection = ({ labels, isRememberedUser, logoutlabels, userName }) => {
    return isRememberedUser ? (
      <BodyCopy component="div" className="elem-pb-LRG">
        <BodyCopy fontFamily="primary" fontSize="fs14">
          {`${getLabelValue(labels, 'lbl_login_not', 'login')} ${userName} ? `}
          <Anchor
            underline
            fontSizeVariation="medium"
            anchorVariation="primary"
            onClick={this.showLoginForm}
          >
            <LogOutPageContainer labels={logoutlabels} underline />
          </Anchor>
        </BodyCopy>
      </BodyCopy>
    ) : null;
  };

  getCreateAccountHelp = ({ labels, fullPage }) => {
    return !fullPage ? (
      <BodyCopy fontFamily="secondary" fontSize="fs12" textAlign="center">
        {getLabelValue(labels, 'lbl_login_createAccountHelp', 'login')}
      </BodyCopy>
    ) : (
      <>
        <BodyCopy fontFamily="secondary" fontSize="fs12" textAlign="center">
          {getLabelValue(labels, 'lbl_login_createAccountHelp_1_v1', 'login')}
        </BodyCopy>
        <BodyCopy fontFamily="secondary" fontSize="fs12" textAlign="center">
          {getLabelValue(labels, 'lbl_login_createAccountHelp_2_v1', 'login')}
        </BodyCopy>
      </>
    );
  };

  getLoyaltyLocation = (variation) => {
    return variation === 'checkout' ? 'checkout-popup' : 'Login';
  };

  render() {
    const {
      onSubmit,
      labels,
      formErrorMessage,
      loginErrorMessage,
      initialValues,
      showRecaptcha,
      resetForm,
      className,
      queryParams,
      currentForm,
      variation,
      handleContinueAsGuest,
      tooltipContent,
      resetLoginState,
      userplccCardNumber,
      userplccCardId,
      isLoading,
      isRememberedUser,
      closeOverlay,
      isResetPassCodeValidationEnabled,
      closeModal,
      fullPage,
    } = this.props;

    const { redirectFromReset } = this.state;

    return (
      <div className={className} ref={this.parentRef}>
        {(!currentForm || currentForm === constants.PAGE_TYPE.LOGIN) && (
          <LoginTopSection
            variation={variation}
            labels={labels}
            className="elem-mb-SM elem-mt-MED"
            isCanada={this.isCanada}
            resetLoginState={resetLoginState}
            showForgotPasswordForm={this.showForgotPasswordForm}
            isRememberedUser={isRememberedUser}
            closeOverlay={closeOverlay}
            closeModal={closeModal}
            isResetPassCodeValidationEnabled={isResetPassCodeValidationEnabled}
            redirectFromReset={redirectFromReset}
            fullPage={fullPage}
          />
        )}
        <Row>
          <Col
            colSize={{
              small: 6,
              medium: 8,
              large: `${variation === 'checkout' ? 7 : 12}`,
            }}
            className={`elem-pt-SM elem-pl-LRG elem-pr-LRG ${
              variation === 'checkout' ? 'checkoutForm' : 'loginForm'
            }`}
          >
            {(!currentForm || currentForm === constants.PAGE_TYPE.LOGIN) && (
              <React.Fragment>
                <LoginForm
                  onSubmit={onSubmit}
                  labels={labels}
                  formErrorMessage={formErrorMessage}
                  loginErrorMessage={loginErrorMessage}
                  initialValues={initialValues}
                  showRecaptcha={showRecaptcha}
                  isRememberedUser={isRememberedUser}
                  showForgotPasswordForm={this.showForgotPasswordForm}
                  resetForm={resetForm}
                  className="elem-mb-LRG"
                  onCreateAccountClick={this.showCreateAccountForm}
                  variation={variation}
                  handleContinueAsGuest={handleContinueAsGuest}
                  tooltipContent={tooltipContent}
                  resetLoginState={resetLoginState}
                  userplccCardNumber={userplccCardNumber}
                  userplccCardId={userplccCardId}
                  isLoading={isLoading}
                  closeOverlay={closeOverlay}
                  closeModal={closeModal}
                  isResetPassCodeValidationEnabled={isResetPassCodeValidationEnabled}
                  fullPage={fullPage}
                />
              </React.Fragment>
            )}
            {currentForm === constants.PAGE_TYPE.FORGOT_PASSWORD && (
              <ForgotPasswordContainer
                showForgotPasswordForm={this.showLoginForm}
                labels={labels}
              />
            )}
            {currentForm === constants.PAGE_TYPE.RESET_PASSWORD && (
              <ResetPassword
                backToLoginAction={this.showLoginForm}
                labels={labels.password}
                queryParams={queryParams}
              />
            )}
            {!isRememberedUser && (
              <>
                <BodyCopy component="div" className="border elem-pt-MED elem-pb-LRG">
                  {this.getCreateAccountHelp(this.props)}
                </BodyCopy>
                <ClickTracker
                  onClick={() => {
                    setLoyaltyLocation(this.getLoyaltyLocation(variation));
                  }}
                  clickData={getCreateAccountPayload(this.getLoyaltyLocation(variation))}
                >
                  <Button
                    className="create-acc-cta"
                    fill="WHITE"
                    type="submit"
                    buttonVariation="fixed-width"
                    data-locator=""
                    onClick={this.showCreateAccountForm}
                  >
                    {getLabelValue(labels, 'lbl_login_createAccountCTA', 'login')}
                  </Button>
                </ClickTracker>
              </>
            )}

            {this.getSignOutSection(this.props)}
          </Col>
        </Row>
      </div>
    );
  }
}

LoginSection.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  labels: PropTypes.shape({}).isRequired,
  loginErrorMessage: PropTypes.string,
  initialValues: PropTypes.shape({}).isRequired,
  showRecaptcha: PropTypes.bool,
  openModal: PropTypes.func,
  queryParams: PropTypes.shape({}).isRequired,
  currentForm: PropTypes.string,
  handleContinueAsGuest: PropTypes.func.isRequired,
  formErrorMessage: PropTypes.shape({}).isRequired,
  userplccCardNumber: PropTypes.string.isRequired,
  userplccCardId: PropTypes.string.isRequired,
  logoutlabels: PropTypes.shape({}),
  onClose: PropTypes.func.isRequired,
  openOverlay: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
  resetForm: PropTypes.bool.isRequired,
  className: PropTypes.string.isRequired,
  variation: PropTypes.string.isRequired,
  tooltipContent: PropTypes.string.isRequired,
  resetLoginState: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  isRememberedUser: PropTypes.bool.isRequired,
  closeOverlay: PropTypes.func,
  isResetPassCodeValidationEnabled: PropTypes.bool,
  fullPage: PropTypes.bool,
  fullPageAuthEnabled: PropTypes.bool,
};

LoginSection.defaultProps = {
  loginErrorMessage: '',
  showRecaptcha: false,
  openModal: () => {},
  currentForm: constants.PAGE_TYPE.LOGIN,
  logoutlabels: {
    CREATE_ACC_SIGN_OUT: 'Sign Out',
  },
  closeOverlay: () => {},
  isResetPassCodeValidationEnabled: false,
  fullPage: false,
  fullPageAuthEnabled: false,
};

export default withStyles(LoginSection, styles);
export { LoginSection as LoginSectionVanilla };
