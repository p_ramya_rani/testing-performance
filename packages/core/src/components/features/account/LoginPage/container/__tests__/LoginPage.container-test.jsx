// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { isCanada } from '@tcp/core/src/utils';
import { LoginPageContainer } from '../LoginPage.container';

jest.mock('@tcp/core/src/utils', () => ({
  isMobileWeb: jest.fn(),
  getViewportInfo: jest.fn(),
  getAPIConfig: () => ({
    brandId: 'tcp',
  }),
  isCanada: jest.fn(),
  isMobileApp: jest.fn(),
  isClient: jest.fn().mockImplementation(() => true),
  getIconPath: jest.fn(),
  getStaticFilePath: jest.fn(),
  getBrand: jest.fn(),
  readCookie: jest.fn().mockImplementation(() => true),
  WebViewRedirectURL: jest.fn(),
  getImageFilePath: jest.fn(),
}));

describe('LoginPageContainer', () => {
  it('should render correctly for US', () => {
    isCanada.mockImplementation(() => false);
    const component = shallow(<LoginPageContainer />);
    expect(component).toMatchSnapshot();
  });

  it('should render correctly for Canada', () => {
    isCanada.mockImplementation(() => true);
    const component = shallow(<LoginPageContainer />);
    expect(component).toMatchSnapshot();
  });

  it('should render correctly fullpageview for US', () => {
    isCanada.mockImplementation(() => false);
    const component = shallow(<LoginPageContainer fullpage />);
    expect(component).toMatchSnapshot();
  });

  it('should render correctly fullpageview for Canada', () => {
    isCanada.mockImplementation(() => true);
    const component = shallow(<LoginPageContainer fullpage />);
    expect(component).toMatchSnapshot();
  });

  it('should call handleLoggedInState after login', () => {
    isCanada.mockImplementation(() => false);
    const component = shallow(<LoginPageContainer fullpage />);
    const componentInstance = component.instance();
    const handleLoggedInStateSpy = jest.spyOn(componentInstance, 'handleLoggedInState');
    component.setProps({ isUserLoggedIn: true });
    expect(handleLoggedInStateSpy).toHaveBeenCalled();
  });
});
