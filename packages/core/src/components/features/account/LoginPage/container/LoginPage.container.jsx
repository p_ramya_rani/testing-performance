// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { closeMiniBag } from '@tcp/core/src/components/common/organisms/Header/container/Header.actions';
import { closeAddedToBag } from '@tcp/core/src/components/features/CnC/AddedToBag/container/AddedToBag.actions';

import { isCanada, routerPush, WebViewRedirectURL } from '@tcp/core/src/utils';
import { setLocalStorage } from '@tcp/core/src/utils/localStorageManagement';
import { md5 } from '@tcp/core/src/components/features/browse/ProductListing/molecules/ProductReviews/encoding';
import { getEnableFullPageAuth } from '@tcp/web/src/components/features/content/HomePage/HomePage.selectors';
import { readCookie } from '@tcp/core/src/utils/cookie.util';
import logger from '@tcp/core/src/utils/loggerInstance';
import {
  resetPassword,
  resetLoginForgotPasswordState,
} from '../../ForgotPassword/container/ForgotPassword.actions';
import {
  getShowNotificationState,
  getResetEmailResponse,
  toggleSuccessfulEmailSection,
} from '../../ForgotPassword/container/ForgotPassword.selectors';
import { getResetPassCodeValidationFlow } from '../../ResetPassword/container/ResetPassword.selectors';
import { login, resetLoginInfo } from './LoginPage.actions';
import {
  closeOverlayModal,
  openOverlayModal,
} from '../../OverlayModal/container/OverlayModal.actions';
import { getFormValidationErrorMessages } from '../../Account/container/Account.selectors';
import {
  getLoginError,
  shouldShowRecaptcha,
  getLoginErrorMessage,
  getShowErrorModal,
  getLabels,
  getLoadingState,
} from './LoginPage.selectors';
import {
  getUserLoggedInState,
  getplccCardId,
  getplccCardNumber,
  isRememberedUser,
  getUserEmail,
  getUserName,
} from '../../User/container/User.selectors';
import { toastMessageInfo } from '../../../../common/atoms/Toast/container/Toast.actions';

import LoginView from '../views';
import { getPageNameFromState } from '../../../../../reduxStore/selectors/session.selectors';
import {
  CUSTOMER_HELP_DIRECT_LINK_TEMPLATE,
  CUSTOMER_HELP_DIRECT_ROUTES,
} from '../../CustomerHelp/CustomerHelp.constants';
import { routerReplace } from '../../../../../utils';

export class LoginPageContainer extends React.PureComponent {
  componentDidUpdate(prevProps) {
    const {
      isUserLoggedIn,
      closeOverlay,
      closeModal,
      variation,
      toastMessage,
      loginErrorMessage,
      loginError,
      showErrorModal,
      userEmail,
    } = this.props;
    if (!prevProps.loginError && loginError && !showErrorModal) {
      toastMessage(loginErrorMessage);
    }
    if (!prevProps.isUserLoggedIn && isUserLoggedIn) {
      this.handleLoggedInState();
      if (variation === 'checkout') {
        closeModal();
      }
      if (userEmail) {
        const hash = md5(userEmail.toLowerCase()).toUpperCase();
        setLocalStorage({ key: 'email_hash', value: hash });
        /* eslint no-underscore-dangle: 0 */
        if (window && window._dataLayer) window._dataLayer.email_hash = hash;
      }
      closeOverlay();
    }
  }

  componentWillUnmount() {
    const { resetLoginState, loginError, resetAccountOverViewState } = this.props;
    if (loginError) {
      resetLoginState();
    }

    if (resetAccountOverViewState) {
      resetAccountOverViewState();
    }
  }

  openModal = (params) => {
    const { openOverlay, setLoginModalMountState } = this.props;
    if (setLoginModalMountState) {
      setLoginModalMountState(params);
    } else {
      openOverlay(params);
    }
  };

  closeBagModal = (e) => {
    if (e) e.preventDefault();
    const { closeMiniBagDispatch, closeAddedToBagModal } = this.props;
    closeMiniBagDispatch();
    closeAddedToBagModal();
  };

  handleLoggedInState = () => {
    const { variation, fullPage, router } = this.props;
    try {
      const webView = JSON.parse(readCookie('fromMobileApp')) || false;
      if (webView) {
        WebViewRedirectURL();
      } else if (variation !== 'checkout' && fullPage) {
        const { successtarget = '/home' } = (router && router.query) || {};
        if (
          successtarget === '/home' ||
          successtarget.indexOf('confirmation') !== -1 ||
          successtarget.indexOf('register') !== -1
        ) {
          routerPush('/', '/home');
        } else if (
          successtarget.indexOf(CUSTOMER_HELP_DIRECT_LINK_TEMPLATE.MERGE_MYPLACE_ACCOUNTS) !== -1
        ) {
          routerReplace(
            CUSTOMER_HELP_DIRECT_ROUTES.MERGE_MYPLACE_ACCOUNTS.to,
            CUSTOMER_HELP_DIRECT_ROUTES.MERGE_MYPLACE_ACCOUNTS.asPath
          );
        } else if (window.history && window.history.back) {
          window.history.back();
        }
      }
    } catch (err) {
      logger.error({ error: err });
    }
  };

  render() {
    const {
      onSubmit,
      loginError,
      showErrorModal,
      loginErrorMessage,
      showRecaptcha,
      resetForm,
      getUserInfoAction,
      labels,
      resetLoginState,
      SubmitForgot,
      showNotification,
      successFullResetEmail,
      currentForm,
      queryParams,
      setLoginModalMountState,
      onRequestClose,
      variation,
      handleContinueAsGuest,
      formErrorMessage,
      showCheckoutModal,
      showLogin,
      userplccCardNumber,
      userplccCardId,
      updateHeader,
      navigation,
      toastMessage,
      resetChangePasswordState,
      isLoading,
      rememberedUserFlag,
      userEmail,
      userName,
      openOverlay,
      closeModal,
      closeOverlay,
      isResetPassCodeValidationEnabled,
      fullPage,
      plccEmail,
      ...otherProps
    } = this.props;
    const errorMessage = loginError ? loginErrorMessage : '';
    const initialValues = {
      rememberMe: true,
      savePlcc: true,
      emailAddress: rememberedUserFlag ? userEmail : '',
      plcc_checkbox: !isCanada(),
    };
    if (plccEmail) {
      initialValues.emailAddress = plccEmail;
    }
    const isFullPageView = fullPage && variation !== 'checkout';
    const fullPageAuthEnabled = fullPage;
    return (
      <LoginView
        onSubmit={onSubmit}
        labels={labels}
        loginErrorMessage={errorMessage}
        initialValues={initialValues}
        showRecaptcha={showRecaptcha}
        resetForm={resetForm}
        getUserInfo={getUserInfoAction}
        openModal={this.openModal}
        resetLoginState={resetLoginState}
        SubmitForgot={SubmitForgot}
        showNotification={showNotification}
        successFullResetEmail={successFullResetEmail}
        currentForm={currentForm}
        queryParams={queryParams}
        setLoginModalMountState={setLoginModalMountState}
        onRequestClose={onRequestClose}
        variation={variation}
        handleContinueAsGuest={handleContinueAsGuest}
        loginError={loginError}
        showErrorModal={showErrorModal}
        formErrorMessage={formErrorMessage}
        showCheckoutModal={showCheckoutModal}
        showLogin={showLogin}
        userplccCardNumber={userplccCardNumber}
        userplccCardId={userplccCardId}
        updateHeader={updateHeader}
        navigation={navigation}
        toastMessage={toastMessage}
        isRememberedUser={rememberedUserFlag}
        resetChangePasswordState={resetChangePasswordState}
        isLoading={isLoading}
        userName={userName}
        openOverlay={openOverlay}
        onClose={this.closeBagModal}
        closeModal={closeModal}
        closeOverlay={closeOverlay}
        isResetPassCodeValidationEnabled={isResetPassCodeValidationEnabled}
        fullPage={isFullPageView}
        fullPageAuthEnabled={fullPageAuthEnabled}
        plccEmail={plccEmail}
        {...otherProps}
      />
    );
  }
}

LoginPageContainer.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  resetLoginState: PropTypes.func,
  isUserLoggedIn: PropTypes.bool,
  closeOverlay: PropTypes.func,
  loginError: PropTypes.bool,
  loginErrorMessage: PropTypes.string,
  showRecaptcha: PropTypes.bool,
  resetForm: PropTypes.bool.isRequired,
  getUserInfoAction: PropTypes.bool.isRequired,
  openOverlay: PropTypes.func,
  navigation: PropTypes.shape({}),
  labels: PropTypes.shape({}).isRequired,
  SubmitForgot: PropTypes.bool.isRequired,
  showNotification: PropTypes.bool.isRequired,
  successFullResetEmail: PropTypes.bool.isRequired,
  currentForm: PropTypes.string,
  queryParams: PropTypes.shape({}),
  onRequestClose: PropTypes.shape({}).isRequired,
  setLoginModalMountState: PropTypes.bool.isRequired,
  closeModal: PropTypes.bool.isRequired,
  variation: PropTypes.bool.isRequired,
  handleContinueAsGuest: PropTypes.func,
  toastMessage: PropTypes.string.isRequired,
  formErrorMessage: PropTypes.shape({}).isRequired,
  showCheckoutModal: PropTypes.func.isRequired,
  showLogin: PropTypes.func.isRequired,
  resetAccountOverViewState: PropTypes.func,
  userplccCardNumber: PropTypes.string.isRequired,
  userplccCardId: PropTypes.string.isRequired,
  updateHeader: PropTypes.func.isRequired,
  resetChangePasswordState: PropTypes.func,
  isLoading: PropTypes.bool.isRequired,
  rememberedUserFlag: PropTypes.bool,
  userEmail: PropTypes.string,
  userName: PropTypes.string,
  closeMiniBagDispatch: PropTypes.func,
  closeAddedToBagModal: PropTypes.func,
  showErrorModal: PropTypes.bool,
  isResetPassCodeValidationEnabled: PropTypes.bool,
  fullPage: PropTypes.bool,
  router: PropTypes.shape({}),
  plccEmail: PropTypes.string,
};

LoginPageContainer.defaultProps = {
  showRecaptcha: false,
  loginError: false,
  loginErrorMessage: '',
  resetLoginState: () => {},
  closeOverlay: () => {},
  openOverlay: () => {},
  handleContinueAsGuest: () => {},
  isUserLoggedIn: false,
  navigation: {},
  currentForm: '',
  queryParams: {},
  resetAccountOverViewState: () => {},
  resetChangePasswordState: () => {},
  rememberedUserFlag: false,
  userEmail: '',
  userName: '',
  closeMiniBagDispatch: () => {},
  closeAddedToBagModal: () => {},
  showErrorModal: false,
  isResetPassCodeValidationEnabled: false,
  fullPage: false,
  router: {},
  plccEmail: '',
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    onSubmit: (payload) => {
      const { variation = '', fullPage = false } = props;
      const modfiedPayload = { ...payload, mergeCart: variation !== 'checkout', fullPage };
      dispatch(login(modfiedPayload, props.handleAfterLogin));
    },
    resetForm: (payload) => {
      dispatch(resetLoginForgotPasswordState(payload));
    },
    resetLoginState: () => {
      dispatch(resetLoginInfo());
    },
    SubmitForgot: (payload) => {
      dispatch(resetPassword(payload));
    },
    closeOverlay: () => {
      dispatch(closeOverlayModal());
    },
    openOverlay: (payload) => {
      dispatch(openOverlayModal(payload));
    },
    toastMessage: (palyoad) => {
      dispatch(toastMessageInfo(palyoad));
    },
    closeMiniBagDispatch: () => {
      dispatch(closeMiniBag());
    },
    closeAddedToBagModal: () => {
      dispatch(closeAddedToBag());
    },
  };
};

const mapStateToProps = (state) => {
  return {
    isLoading: getLoadingState(state),
    showNotification: getShowNotificationState(state),
    resetForgotPasswordErrorResponse: getResetEmailResponse(state),
    successFullResetEmail: toggleSuccessfulEmailSection(state),
    isUserLoggedIn: getUserLoggedInState(state),
    loginError: getLoginError(state),
    loginErrorMessage: getLoginErrorMessage(state),
    showErrorModal: getShowErrorModal(state),
    showRecaptcha: shouldShowRecaptcha(state),
    labels: getLabels(state),
    formErrorMessage: getFormValidationErrorMessages(state),
    userplccCardNumber: getplccCardNumber(state),
    userplccCardId: getplccCardId(state),
    rememberedUserFlag: isRememberedUser(state),
    userEmail: getUserEmail(state),
    userName: getUserName(state),
    isResetPassCodeValidationEnabled: getResetPassCodeValidationFlow(state),
    fullPage: getEnableFullPageAuth(state),
    loyaltyPageName: getPageNameFromState(state),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPageContainer);
