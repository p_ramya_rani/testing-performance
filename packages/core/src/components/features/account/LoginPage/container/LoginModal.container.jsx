// 9fbef606107a605d69c0edbcd8029e5d 
import { connect } from 'react-redux';
import React from 'react';
import { getUserLoggedInState } from '@tcp/core/src/components/features/account/User/container/User.selectors';
import LoginModal from '../views/LoginModal.native';

const LoginModalContainer = props => <LoginModal {...props} />;

const mapStateToProps = state => {
  const labels = state && state.Labels;
  const isUserLoggedIn = getUserLoggedInState(state) || false;
  return {
    labels,
    isUserLoggedIn,
  };
};

export default connect(mapStateToProps)(LoginModalContainer);
