// 9fbef606107a605d69c0edbcd8029e5d
import { put, call, takeLatest } from 'redux-saga/effects';
import { LoginPageSaga, loginSaga } from '../LoginPage.saga';
import { setLoginInfo, setLoginLoadingState } from '../LoginPage.actions';
import { getUserInfoSaga } from '../../../User/container/User.saga';
import constants from '../../LoginPage.constants';

describe('LoginPage saga', () => {
  describe('loginSaga', () => {
    let loginGen;
    beforeEach(() => {
      const payload = {};
      loginGen = loginSaga({ payload });
      loginGen.next();
      loginGen.next();
      loginGen.next();
      loginGen.next();
      loginGen.next();
    });

    it('should dispatch getUserInfo action for success resposnse', () => {
      loginGen.next({ success: true });
      expect(loginGen.next().value).toEqual(
        call(getUserInfoSaga, { payload: { loadFavStore: true, ignoreCache: true } })
      );
    });

    it('should dispatch setLoginInfo action for API error', () => {
      loginGen.next();
      loginGen.next();
      loginGen.next();
      const putDescriptor = loginGen.next({
        isLoading: false,
      }).value;
      expect(putDescriptor).toEqual(
        put(
          setLoginLoadingState({
            isLoading: false,
          })
        )
      );
    });

    it('should dispatch setLoginInfo for error response', () => {
      const error = new Error();
      loginGen.throw(error);
      loginGen.next();
      const putDescriptor = loginGen.next(error).value;
      expect(putDescriptor).toEqual(
        put(
          setLoginInfo({
            success: false,
          })
        )
      );
    });
  });

  describe('LoginPageSaga', () => {
    it('should return correct takeLatest effect', () => {
      const generator = LoginPageSaga();
      const takeLatestDescriptor = generator.next().value;
      const expected = takeLatest(constants.LOGIN, loginSaga);
      expect(takeLatestDescriptor).toEqual(expected);
    });
  });
});
