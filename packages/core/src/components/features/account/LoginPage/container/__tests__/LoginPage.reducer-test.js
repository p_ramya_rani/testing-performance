// 9fbef606107a605d69c0edbcd8029e5d 
import { fromJS } from 'immutable';
import LoginPageReducer from '../LoginPage.reducer';
import { setLoginInfo } from '../LoginPage.actions';

describe('LoginPage reducer', () => {
  const initialState = fromJS({
    error: null,
    loginModalMountedState: false,
    checkoutModalMountedState: false,
    loginErrorMessage: null,
    componentType: 'login',
    isLoading: false,
    isAccountCardVisible: true,
  });
  it('should return default state', () => {
    expect(LoginPageReducer(undefined, {})).toEqual(initialState);
  });

  it('should handle setLoginInfo action correctly', () => {
    const payload = {
      success: true,
    };
    const expectedState = fromJS({
      error: fromJS(payload),
      loginModalMountedState: false,
      checkoutModalMountedState: false,
      loginErrorMessage: null,
      componentType: 'login',
      isLoading: false,
      isAccountCardVisible: true,
    });

    expect(LoginPageReducer(initialState, setLoginInfo(payload))).toEqual(expectedState);
  });
});
