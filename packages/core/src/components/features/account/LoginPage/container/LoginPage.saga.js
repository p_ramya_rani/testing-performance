/* eslint-disable max-statements */
// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable sonarjs/cognitive-complexity */
import { call, takeLatest, putResolve, put, select, delay } from 'redux-saga/effects';
import logger from '@tcp/core/src/utils/loggerInstance';
import {
  setLoginModalMountedState,
  loginToSFCC,
} from '@tcp/core/src/components/features/account/LoginPage/container/LoginPage.actions';
import setLoaderState from '@tcp/core/src/components/common/molecules/Loader/container/Loader.actions';
import { trackError } from '@tcp/core/src/utils/errorReporter.util';
import {
  getIsBagCarouselEnabled,
  getPageNameFromState,
  getIsSNJEnabled,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import { getCouponList } from '@tcp/core/src/components/features/CnC/common/organism/CouponAndPromos/container/Coupon.actions';
import { resetBagCarouselModuleData } from '@tcp/core/src/components/common/molecules/BagCarouselModule/container/BagCarouselModule.actions';
import { setClickAnalyticsData, trackClick, trackFormError } from '@tcp/core/src/analytics/actions';
import { updateMiniBagFlag } from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.actions';
import { setValueInAsyncStorage } from '@tcp/core/src/utils';
import { md5 } from '@tcp/core/src/components/features/browse/ProductListing/molecules/ProductReviews/encoding';
import { setLocalStorage } from '@tcp/core/src/utils/localStorageManagement';
import { readCookie } from '@tcp/core/src/utils/cookie.util';
import { setIsPlccDone } from '@tcp/core/src/components/features/browse/ApplyCardPage/container/ApplyCard.actions';
import LOGINPAGE_CONSTANTS from '../LoginPage.constants';
import names from '../../../../../constants/eventsName.constants';
import {
  setLoginInfo,
  setCheckoutModalMountedState,
  setLoginLoadingState,
} from './LoginPage.actions';
import { navigateXHRAction } from '../../NavigateXHR/container/NavigateXHR.action';
import { getUserInfoSaga } from '../../User/container/User.saga';
import fetchData from '../../../../../service/API';
import { login } from '../../../../../services/abstractors/account';
import endpoints from '../../../../../service/endpoint';
import { isMobileApp, routerPush, scrollPage, WebViewRedirectURL } from '../../../../../utils';
import { checkoutModalOpenState, getAccountCardVisibility } from './LoginPage.selectors';
import { openOverlayModal } from '../../OverlayModal/container/OverlayModal.actions';
import {
  getplccCardId,
  getUserId,
  getUserEmail,
  getUserName,
} from '../../User/container/User.selectors';

const errorLabel = 'Error in API';

const notIsLocalHost = (siteOrigin) => {
  return siteOrigin.indexOf('local') === -1;
};

function setEmailDataInStorage(userEmail) {
  const emailHash = userEmail ? md5(userEmail.toLowerCase()).toUpperCase() : null;
  if (emailHash && !isMobileApp()) {
    setLocalStorage({ key: 'email_hash', value: emailHash });
    /* eslint no-underscore-dangle: 0 */
    if (window && window._dataLayer) window._dataLayer.email_hash = emailHash;
  } else if (emailHash && isMobileApp()) {
    setValueInAsyncStorage('email_hash', emailHash);
  }
}

function* fireAnalytics() {
  let pageName = '';
  if (isMobileApp()) {
    pageName = yield select(getPageNameFromState);
  } else {
    pageName = window && window.loyaltyLocation;
  }
  yield put(
    setClickAnalyticsData({
      eventName: 'login',
      customEvents: ['event14', 'event128'],
      pageNavigationText: `${pageName || ''}-log in`,
      clickEvent: true,
      loyaltyLocation: pageName,
    })
  );
  yield put(trackClick({ name: names.screenNames.login_success, module: 'account' }));
}

function redirectWebView(webView, isMergeAccount) {
  return webView || isMergeAccount;
}

function isOpenOverlayModal(
  afterLoginHandler,
  fullPage,
  isAccountCardVisible,
  isApplyPlccCardFlow
) {
  return (!afterLoginHandler || fullPage) && isAccountCardVisible && !isApplyPlccCardFlow;
}

// eslint-disable-next-line complexity
// eslint-disable-next-line max-statements
export function* loginSaga({ payload, afterLoginHandler }) {
  yield put(setLoginLoadingState({ isLoading: true }));
  try {
    const { isApplyPlccCardFlow } = payload;
    const plccCardId = yield select(getplccCardId);
    const userId = yield select(getUserId);
    const isAccountCardVisible = yield select(getAccountCardVisibility);
    const updatedPayload =
      plccCardId && payload.plcc_checkbox ? { ...payload, plccCardId } : payload;
    updatedPayload.userId = userId;
    const { fullPage, fromPLCCModal } = payload;
    const response = yield call(login, updatedPayload);
    if (response?.success) {
      yield put(
        getCouponList({
          isSourceLogin: true,
          ignoreCache: true,
        })
      );
      yield call(getUserInfoSaga, { payload: { loadFavStore: true, ignoreCache: true } });
      if (afterLoginHandler) {
        yield call(afterLoginHandler);
      }
      yield put(setLoginModalMountedState({ state: false, isAccountCardVisible }));
      yield put(setLoginLoadingState({ isLoading: false }));
      yield put(navigateXHRAction());
      if (
        isOpenOverlayModal(afterLoginHandler, fullPage, isAccountCardVisible, isApplyPlccCardFlow)
      ) {
        yield delay(100);
        yield put(
          openOverlayModal({
            component: 'accountDrawer',
            variation: 'primary',
          })
        );
      }
      const userEmail = yield select(getUserEmail);
      const name = yield select(getUserName);
      if (isMobileApp()) {
        setEmailDataInStorage(userEmail);
        const isBagCarouselEnabled = yield select(getIsBagCarouselEnabled);
        if (isBagCarouselEnabled) {
          yield put(resetBagCarouselModuleData());
        }
        if (name) {
          setValueInAsyncStorage('username', name);
        }
        if (fromPLCCModal) {
          yield put(setIsPlccDone(true));
        }
      }
      if (!isMobileApp()) {
        setEmailDataInStorage(userEmail);
        yield put(updateMiniBagFlag(true));
        if (window.location.href.includes('customer-help')) {
          const isMergeAccount = window.location.href.includes('account-and-coupons');
          const webView = JSON.parse(readCookie('fromMobileApp')) || false;
          if (redirectWebView(webView, isMergeAccount)) {
            WebViewRedirectURL();
          } else {
            routerPush('/', '/home');
          }
          scrollPage();
        }
      }
      yield call(fireAnalytics);
    }
    yield put(setLoginLoadingState({ isLoading: false }));
    const isSNJEnable = yield select(getIsSNJEnabled);
    if (isSNJEnable) {
      yield put(loginToSFCC({ ...updatedPayload, useSFCCAPI: true }));
    }
    return yield putResolve(setLoginInfo(response));
  } catch (err) {
    const { emailAddress } = payload;
    yield put(trackFormError({ formName: 'LoginForm', formData: { emailAddress } }));
    trackError({
      error: err,
      extraData: {
        component: 'LoginPage - Saga',
        payloadRecieved: { emailAddress },
      },
    });
    yield put(setLoginLoadingState({ isLoading: false }));
    const { errorCode, errorMessage, errorResponse, networkStatusCode, status } = err;

    if (networkStatusCode === 200) {
      yield put(
        setLoginInfo({
          success: false,
          ...errorResponse,
          errorCode,
          errorMessage,
          networkStatusCode,
        })
      );

      const isCheckoutModalOpen = yield select(checkoutModalOpenState);
      if (isCheckoutModalOpen) {
        yield put(setCheckoutModalMountedState({ state: true }));
      }
    } else {
      yield put(
        setLoginInfo({
          success: false,
          status,
        })
      );
    }
    return null;
  } finally {
    yield put(setLoginLoadingState({ isLoading: false }));
    yield put(setLoaderState(false));
  }
}

function* getUserInfoPOCSaga() {
  try {
    const { relURI, method } = endpoints.registeredUserInfoPOC;
    const siteOrigin = window && window.location && window.location.origin;
    const baseURI = notIsLocalHost(siteOrigin)
      ? siteOrigin
      : endpoints.registeredUserInfoPOC.baseURI || endpoints.global.baseURI;

    const res = yield call(
      fetchData,
      baseURI,
      relURI,
      {
        langId: -1,
        catalogId: 10551,
        storeId: 10151,
      },
      method
    );
    yield put(setLoginInfo(res.body));
  } catch (err) {
    logger.info(errorLabel);
    logger.info(err);
  }
}

function* getOrderDetailSaga() {
  try {
    const { relURI, method } = endpoints.getOrderDetails;
    const siteOrigin = window && window.location && window.location.origin;
    const baseURI = notIsLocalHost(siteOrigin)
      ? siteOrigin
      : endpoints.getOrderDetails.baseURI || endpoints.global.baseURI;

    const res = yield call(
      fetchData,
      baseURI,
      relURI,
      {
        langId: -1,
        catalogId: 10551,
        storeId: 10151,
        pageName: 'fullOrderInfo',
        poc: 'withCredentials',
      },
      method
    );
    yield put(setLoginInfo(res.body));
  } catch (err) {
    logger.error({
      error: err,
      extraData: {
        component: 'LoginPage - Saga - getOrderDetailSaga',
      },
    });
  }
}

export function* loginSFCCSaga({ payload }) {
  try {
    yield call(login, payload);
  } catch (err) {
    logger.error({
      error: `Exception in sfccLogon API Call: ${JSON.stringify(err)}`,
      payload,
    });
  }
}

export function* LoginPageSaga() {
  yield takeLatest(LOGINPAGE_CONSTANTS.LOGIN, loginSaga);
  yield takeLatest('GET_ORDER_DETAIL', getOrderDetailSaga);
  yield takeLatest('GET_USER_DETAIL_POC', getUserInfoPOCSaga);
  yield takeLatest(LOGINPAGE_CONSTANTS.LOGIN_SFCC, loginSFCCSaga);
}

export default LoginPageSaga;
