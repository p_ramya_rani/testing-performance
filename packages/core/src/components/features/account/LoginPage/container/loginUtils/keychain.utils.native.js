// 9fbef606107a605d69c0edbcd8029e5d 
// eslint-disable-next-line import/no-unresolved
import * as Keychain from 'react-native-keychain';
// eslint-disable-next-line import/no-unresolved
import TouchID from 'react-native-touch-id';

export const setUserLoginDetails = (emailAddress, password) => {
  return Keychain.setGenericPassword(emailAddress, password);
};

export const getUserLoginDetails = () => {
  return Keychain.getGenericPassword();
};

export const resetTouchPassword = () => {
  return Keychain.resetGenericPassword();
};

export const touchIDCheck = () => {
  return TouchID.authenticate('Authentication Required')
    .then(() => {
      return true;
    })
    .catch(() => {
      return false;
    });
};

// Will store the server/username/password combination in the secure storage.
export const setKeyChainCredentials = (server, username, password) => {
  return Keychain.setInternetCredentials(server, username, password);
};

// Will retrieve the server/username/password combination from the secure storage.
// Resolves to { username, password } if an entry exists or false if it doesn't.
// It will reject only if an unexpected error is encountered like lacking entitlements or permission.
export const getKeyChainCredentials = server => {
  return Keychain.getInternetCredentials(server);
};

export const isSupportedTouch = () => {
  return TouchID.isSupported()
    .then(biometryType => {
      if (biometryType === 'FaceID' || biometryType === 'TouchID') {
        return biometryType;
      }
      return true;
    })
    .catch(() => {
      return false;
    });
};

export const KeyChainConstants = {
  MOBILE_APP: 'mobileapp',
};
