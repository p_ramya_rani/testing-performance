// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { PurchaseGiftsCard } from '../PurchaseGiftsCard.view.native';

describe('AnimatedGiftsCard View component should render animated gift card', () => {
  it('AnimatedGiftsCard should renders correctly', () => {
    const props = {
      labels: {
        common: { lbl_purchaseGiftsCard_animatedGiftCards: 'Animatd gift card' },
      },
      navigation: {
        getParam: jest.fn(),
      },
    };
    const component = shallow(<PurchaseGiftsCard {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('PhysicalGiftsCard should renders correctly', () => {
    const props = {
      labels: {
        common: { lbl_purchaseGiftsCard_physicalGiftCard: 'Physical gift card' },
      },
      navigation: {
        getParam: jest.fn(),
      },
    };
    const component = shallow(<PurchaseGiftsCard {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('eGiftsCard should renders correctly', () => {
    const props = {
      labels: {
        common: { lbl_purchaseGiftsCard_eGiftCard: 'eGift gift card' },
      },
      navigation: {
        getParam: jest.fn(),
      },
    };
    const component = shallow(<PurchaseGiftsCard {...props} />);
    expect(component).toMatchSnapshot();
  });
});
