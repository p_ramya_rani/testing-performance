// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { getLabelValue } from '@tcp/core/src/utils';
import { UrlHandler } from '@tcp/core/src/utils/utils.app';
import { GiftCardWrapper } from '../styles/PurchaseGiftsCard.view.style.native';
import GiftCardTile from './GiftCardTile.view.native';
import AccessibilityRoles from '../../../../../constants/AccessibilityRoles.constant';
import SpringAnimation from '../../../../../../../mobileapp/src/components/common/molecules/Animations/SpringAnimation';

const onEGiftCardClick = (labels, navigation, isGiftCardBalanceEnabled) => {
  if (isGiftCardBalanceEnabled) return navigation.navigate('EGiftCardWebPage');
  return UrlHandler(getLabelValue(labels, 'lbl_purchaseGiftsCard_eGiftCardLinkUrl', 'common'));
};

const onPhysicalGiftCardClick = (labels, navigation) => {
  return navigation.navigate('ProductDetail', {
    title: getLabelValue(labels, 'lbl_purchaseGiftsCard_physicalGiftCardPageTitle', 'common'),
    pdpUrl: 'Gift Card',
  });
};

const onAnimatedGiftCardClick = (labels) => {
  return UrlHandler(
    getLabelValue(labels, 'lbl_purchaseGiftsCard_animatedGiftCardsLinkUrl', 'common')
  );
};

const getAnimatedGiftCardValue = (labels) => {
  const animatedGiftCards = getLabelValue(
    labels,
    'lbl_purchaseGiftsCard_animatedGiftCards',
    'common'
  );
  return animatedGiftCards && animatedGiftCards !== 'lbl_purchaseGiftsCard_animatedGiftCards'
    ? animatedGiftCards
    : null;
};

export const PurchaseGiftsCard = ({ labels, navigation, isGiftCardBalanceEnabled }) => {
  const { getParam } = navigation;
  const accentColor = getParam('accentColor');
  const eGiftCard = getLabelValue(labels, 'lbl_purchaseGiftsCard_eGiftCard', 'common');
  const physicalGiftCard = getLabelValue(
    labels,
    'lbl_purchaseGiftsCard_physicalGiftCard',
    'common'
  );
  const eGiftCardValue =
    eGiftCard && eGiftCard !== 'lbl_purchaseGiftsCard_eGiftCard' ? eGiftCard : null;
  const physicalGiftCardValue =
    physicalGiftCard && physicalGiftCard !== 'lbl_purchaseGiftsCard_physicalGiftCard'
      ? physicalGiftCard
      : null;

  const animatedGiftCardsValue = isGiftCardBalanceEnabled
    ? false
    : getAnimatedGiftCardValue(labels);

  return (
    <GiftCardWrapper>
      <SpringAnimation>
        {animatedGiftCardsValue && (
          <GiftCardTile
            accessibilityRole={AccessibilityRoles.Link}
            onTileClick={onAnimatedGiftCardClick}
            accentColor={accentColor}
            title={animatedGiftCardsValue}
            navigation={navigation}
            isGiftCardBalanceEnabled={isGiftCardBalanceEnabled}
            labels={labels}
          />
        )}
        {eGiftCardValue && (
          <GiftCardTile
            accessibilityRole={AccessibilityRoles.Link}
            onTileClick={onEGiftCardClick}
            accentColor={accentColor}
            title={eGiftCardValue}
            navigation={navigation}
            isGiftCardBalanceEnabled={isGiftCardBalanceEnabled}
            labels={labels}
          />
        )}
        {physicalGiftCardValue && (
          <GiftCardTile
            accessibilityRole={AccessibilityRoles.Link}
            onTileClick={onPhysicalGiftCardClick}
            accentColor={accentColor}
            title={physicalGiftCardValue}
            navigation={navigation}
            isGiftCardBalanceEnabled={isGiftCardBalanceEnabled}
            labels={labels}
          />
        )}
      </SpringAnimation>
    </GiftCardWrapper>
  );
};

PurchaseGiftsCard.propTypes = {
  labels: PropTypes.shape({}),
  navigation: PropTypes.func.isRequired,
  isGiftCardBalanceEnabled: PropTypes.bool,
};

PurchaseGiftsCard.defaultProps = {
  labels: {},
  isGiftCardBalanceEnabled: false,
};

export default PurchaseGiftsCard;
