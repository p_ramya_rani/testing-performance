// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import GiftCardTile from '../GiftCardTile.view.native';

describe('Gift Card tile component should render correctly', () => {
  it('should renders correctly', () => {
    const props = {
      labels: {},
      navigation: {
        getParam: jest.fn(),
      },
      accessibilityRole: {},
      title: 'Gift Card',
      accentColor: '#fff',
      onTileClick: jest.fn(),
    };
    const component = shallow(<GiftCardTile {...props} />);
    expect(component).toMatchSnapshot();
  });
});
