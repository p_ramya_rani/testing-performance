// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { isTCP } from '@tcp/core/src/utils';
import ImageComp from '../../../../common/atoms/Image';
import BodyCopy from '../../../../common/atoms/BodyCopy';
import {
  TouchableContainer,
  RightArrowImageContainer,
  MenuItem,
} from '../styles/PurchaseGiftsCard.view.style.native';

const rightIcon = require('../../../../../../../mobileapp/src/assets/images/icons-carets-medium-right.png');

const isTcpApp = isTCP();

const GiftCardTile = ({
  accessibilityRole,
  onTileClick,
  accentColor,
  title,
  labels,
  isGiftCardBalanceEnabled,
  navigation,
}) => {
  return (
    <TouchableContainer
      accessible
      accessibilityRole={accessibilityRole}
      accessibilityLabel={title}
      onPress={() => onTileClick(labels, navigation, isGiftCardBalanceEnabled)}
      accentColor={accentColor}
    >
      <MenuItem>
        <BodyCopy
          fontFamily={isTcpApp ? 'secondary' : 'primary'}
          fontSize="fs16"
          fontWeight={isTcpApp ? 'bold' : 'medium'}
          text={title}
          color="text.primary"
          numberOfLines={2}
          textAlign="left"
        />
        <RightArrowImageContainer>
          <ImageComp source={rightIcon} width={10} height={18} />
        </RightArrowImageContainer>
      </MenuItem>
    </TouchableContainer>
  );
};

GiftCardTile.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  navigation: PropTypes.shape({}).isRequired,
  accessibilityRole: PropTypes.shape({}).isRequired,
  onTileClick: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  isGiftCardBalanceEnabled: PropTypes.bool.isRequired,
  accentColor: PropTypes.string.isRequired,
};

export default GiftCardTile;
