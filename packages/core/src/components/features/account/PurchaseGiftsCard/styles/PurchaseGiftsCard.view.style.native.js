// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

const GiftCardWrapper = styled.View`
  flex: 1;
  background: ${props => props.theme.colorPalette.gray[300]};
  padding: ${props => props.theme.spacing.LAYOUT_SPACING.XS}
    ${props => props.theme.spacing.LAYOUT_SPACING.XXS};
`;

const TouchableContainer = styled.TouchableOpacity`
  height: 48px;
  width: 99%;
  border-radius: 15px;
  margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.SM};
  background-color: ${props => (props.accentColor ? props.accentColor : props.theme.colors.WHITE)};
  box-shadow: 2px 2px 2px ${props => props.theme.colors.BOX_SHADOW_LIGHT};
`;

const MenuItem = styled.View`
  flex: 1;
  flex-direction: row;
  align-items: center;
  background: ${props => props.theme.colors.WHITE};
  border-top-right-radius: 15px;
  border-bottom-right-radius: 15px;
  padding: ${props => props.theme.spacing.ELEM_SPACING.SM};
  margin-left: ${props => props.theme.spacing.ELEM_SPACING.SM};
`;

const RightArrowImageContainer = styled.View`
  margin: ${props => props.theme.spacing.LAYOUT_SPACING.SM}
    ${props => props.theme.spacing.ELEM_SPACING.XXS};
  flex: 1;
  align-items: flex-end;
`;

export { TouchableContainer, RightArrowImageContainer, MenuItem, GiftCardWrapper };
