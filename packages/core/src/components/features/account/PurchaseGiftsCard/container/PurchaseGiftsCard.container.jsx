// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import PurchaseGiftsCard from '../views';
import { getLabels } from '../../Account/container/Account.selectors';
import { getIsGiftCardBalance } from '../../../../../reduxStore/selectors/session.selectors';

export class PurchaseGiftsCardContainer extends React.PureComponent {
  render() {
    const { navigation, labels, isGiftCardBalanceEnabled } = this.props;
    return (
      <PurchaseGiftsCard
        labels={labels}
        navigation={navigation}
        isGiftCardBalanceEnabled={isGiftCardBalanceEnabled}
      />
    );
  }
}

PurchaseGiftsCardContainer.propTypes = {
  labels: PropTypes.shape({}),
  isGiftCardBalanceEnabled: PropTypes.bool,
  navigation: PropTypes.func.isRequired,
};

PurchaseGiftsCardContainer.defaultProps = {
  labels: {},
  isGiftCardBalanceEnabled: false,
};

const mapStateToProps = (state) => ({
  labels: getLabels(state),
  isGiftCardBalanceEnabled: getIsGiftCardBalance(state),
});

export default connect(mapStateToProps, null)(PurchaseGiftsCardContainer);
