// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { PointsClaimView } from '../PointsClaim.view';

describe('PointsClaim Native View component', () => {
  it('should renders correctly', () => {
    const props = {
      labels: {},
      successMessage: 'SUCCESS',
      errorMessage: 'ERROR',
      onSubmit: () => {},
    };
    const component = shallow(<PointsClaimView {...props} />);
    expect(component).toMatchSnapshot();
  });
});
