// 9fbef606107a605d69c0edbcd8029e5d 
export const Constants = {
  Stage: {
    New: 'NEW',
    Saved: 'SAVED',
  },
  QUESTION1: 'question1',
  QUESTION2: 'question2',
};

export default Constants;
