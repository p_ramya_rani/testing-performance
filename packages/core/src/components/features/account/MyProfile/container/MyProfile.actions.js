// 9fbef606107a605d69c0edbcd8029e5d 
import constants from '../MyProfile.constants';

export const updateProfileSuccess = payload => ({
  type: constants.UPDATE_PROFILE_SUCCESS,
  payload,
});

export default updateProfileSuccess;
