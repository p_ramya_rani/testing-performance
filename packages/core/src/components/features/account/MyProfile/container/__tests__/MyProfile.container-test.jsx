// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { MyProfileContainer } from '../MyProfile.container';
import MyProfileComponent from '../../views/MyProfile.view';

describe('MyProfile container', () => {
  it('should render MyProfile component', () => {
    const component = shallow(<MyProfileContainer labels={{ accountOverview: {} }} />);
    expect(component.is(MyProfileComponent)).toBeTruthy();
  });
});
