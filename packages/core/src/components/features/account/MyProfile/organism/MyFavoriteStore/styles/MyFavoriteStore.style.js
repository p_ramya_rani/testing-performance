// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  .favStoreData {
    padding: 0;
  }
  .capFirstLetter {
    text-transform: capitalize;
  }
`;

export default styles;
