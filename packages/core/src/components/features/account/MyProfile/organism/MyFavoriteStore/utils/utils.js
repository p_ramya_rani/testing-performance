// 9fbef606107a605d69c0edbcd8029e5d 
import { getLabelValue } from '../../../../../../../utils';

const ctaTitleDefaultStore = (labels, favStoreName, isMyReferences) => {
  if (favStoreName) {
    if (isMyReferences) {
      return getLabelValue(labels, 'lbl_common_edit');
    }
    return getLabelValue(labels, 'lbl_common_updateFavoriteStore');
  }
  return getLabelValue(labels, 'lbl_common_addAStore');
};

export default ctaTitleDefaultStore;
