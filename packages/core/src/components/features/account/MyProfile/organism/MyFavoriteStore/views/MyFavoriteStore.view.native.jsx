// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import ACCOUNT_CONSTANTS from '@tcp/core/src/components/features/account/Account/Account.constants';
import { BodyCopyWithSpacing } from '../../../../../../common/atoms/styledWrapper';
import { getLabelValue } from '../../../../../../../utils';
import MyProfileTile from '../../../../../../common/molecules/MyProfileTile';
import ctaTitleDefaultStore from '../utils';
import {
  Row,
  BodyCopyWithTextTransform,
} from '../../../../../../common/atoms/styledWrapper/styledWrapper.native';

const ANALYTICS_CONST = ACCOUNT_CONSTANTS.ACCOUNT_ANALYTICS;
const getStoreEventData = (favStore, isMyRef) => {
  const eventData = {
    pageName: 'myplace',
    pageType: 'myplace',
    pageSection: 'myplace',
    pageSubSection: 'myplace',
    pageNavigationText: ANALYTICS_CONST.navigationText.profile_addFavStore,
  };
  if (favStore) {
    if (isMyRef) {
      eventData.pageNavigationText = ANALYTICS_CONST.navigationText.preferences_edit;
    } else {
      eventData.pageNavigationText = ANALYTICS_CONST.navigationText.profile_addFavStore;
    }
    return eventData;
  }
  return eventData;
};

const MyFavoriteStore = ({
  labels,
  favStoreName,
  favStoreAddress,
  favStoreState,
  favStoreCity,
  favStoreZipcode,
  favStorePhone,
  className,
  isMyPreferences,
  handleComponentChange,
}) => {
  const clickEventdata = getStoreEventData(favStoreName, isMyPreferences);
  return (
    <MyProfileTile
      className={className}
      title={getLabelValue(labels, 'lbl_common_myFavoriteStore')}
      ctaTitle={ctaTitleDefaultStore(labels, favStoreName, isMyPreferences)}
      dataLocator="myFavStoreLbl"
      ctaLink="StoreLanding"
      isPageNavigation
      handleComponentChange={handleComponentChange}
      clickEventdata={clickEventdata}
    >
      {isMyPreferences && (
        <BodyCopyWithSpacing
          spacingStyles="margin-bottom-XXL"
          fontSize="fs16"
          text={getLabelValue(labels, 'lbl_common_accessBuyOnline')}
        />
      )}
      {!favStoreName && (
        <BodyCopyWithSpacing
          spacingStyles="margin-bottom-LRG"
          fontSize="fs16"
          text={getLabelValue(labels, 'lbl_common_favStoreNotAdded')}
        />
      )}
      {!!favStoreName && (
        <>
          <BodyCopyWithTextTransform
            fontSize="fs16"
            fontFamily="secondary"
            text={favStoreName}
            fontWeight={isMyPreferences ? 'semibold' : 'regular'}
          />

          <BodyCopyWithTextTransform
            fontSize="fs16"
            fontFamily="secondary"
            text={favStoreAddress}
          />
          <Row>
            <BodyCopyWithTextTransform
              fontSize="fs16"
              fontFamily="secondary"
              text={`${favStoreCity}, `}
            />
            <BodyCopyWithTextTransform
              textTransform="uppercase"
              fontSize="fs16"
              fontFamily="secondary"
              text={`${favStoreState} ${favStoreZipcode}`}
            />
          </Row>
          <BodyCopyWithTextTransform fontSize="fs16" fontFamily="secondary" text={favStorePhone} />
        </>
      )}
    </MyProfileTile>
  );
};

MyFavoriteStore.propTypes = {
  favStoreName: PropTypes.string,
  favStoreAddress: PropTypes.string,
  favStoreState: PropTypes.string,
  favStoreCity: PropTypes.string,
  favStoreZipcode: PropTypes.string,
  favStorePhone: PropTypes.string,
  labels: PropTypes.shape({}).isRequired,
  className: PropTypes.string,
  isMyPreferences: PropTypes.bool,
  handleComponentChange: PropTypes.func.isRequired,
};

MyFavoriteStore.defaultProps = {
  favStoreName: '',
  favStoreState: '',
  favStoreCity: '',
  favStoreZipcode: '',
  favStorePhone: '',
  favStoreAddress: '',
  className: '',
  isMyPreferences: false,
};

export default MyFavoriteStore;
