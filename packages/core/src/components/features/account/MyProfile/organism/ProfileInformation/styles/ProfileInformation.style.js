// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

export default css`
  .profileInfoCol {
    display: flex;
    flex-direction: column;
  }

  .content {
    padding-bottom: 28px;
  }
  @media ${props => props.theme.mediaQuery.mediumOnly} {
    .programAndTermsLinks {
      justify-content: center;
    }
    .programDetails {
      width: auto;
    }
  }

  .profileInformationCol {
    display: flex;
    flex-direction: column;
    height: 100%;
  }
  .profileInfoSeparator {
    border-bottom: 1px solid ${props => props.theme.colors.FOOTER.DIVIDER};
  }

  .profileInformationAddress {
    font-size: ${props => props.theme.typography.fontSizes.fs16};
  }

  .hideOnSmallerScreens {
    @media ${props => props.theme.mediaQuery.mediumMax} {
      display: none;
    }
  }
`;
