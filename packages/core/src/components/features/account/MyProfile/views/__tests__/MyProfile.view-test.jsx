// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import MyProfile from '../MyProfile.view';

describe('MyProfile component', () => {
  it('should render correctly', () => {
    const component = shallow(<MyProfile />);
    expect(component).toMatchSnapshot();
  });
});
