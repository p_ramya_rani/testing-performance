// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { EmptyAddressList } from '../EmptyAddressList.view';

describe('EmptyAddressList component', () => {
  it('should renders correctly', () => {
    const props = {
      labels: { addressBook: {}, common: {} },
    };
    const component = shallow(<EmptyAddressList {...props} />);
    expect(component).toMatchSnapshot();
  });
});
