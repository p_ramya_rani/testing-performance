// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import AddressTileWrapper from '../styles/AddressListComponentSkeleton.style.native';
import AddressSkelton from '../../../../common/molecules/GenericSkeleton/GenericSkeleton.view.native';

const AddressListComponentSkeleton = () => {
  return (
    <>
      <AddressTileWrapper>
        <AddressSkelton />
      </AddressTileWrapper>
      <AddressTileWrapper>
        <AddressSkelton />
      </AddressTileWrapper>
    </>
  );
};

export default AddressListComponentSkeleton;
