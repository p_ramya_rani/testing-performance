// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { AddressListComponentSkeletonVanilla } from '../AddressListComponentSkeleton.view';

describe('AddressListComponentSkeleton component', () => {
  it('should renders correctly', () => {
    const props = {
      className: 'sample-class',
    };
    const component = shallow(<AddressListComponentSkeletonVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
