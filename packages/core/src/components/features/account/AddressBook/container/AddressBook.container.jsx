// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { List } from 'immutable';
import { getVerificationResult } from '@tcp/core/src/components/common/organisms/AddressVerification/container/AddressVerification.selectors';
import { resetVerifyAddress } from '@tcp/core/src/components/common/organisms/AddressVerification/container/AddressVerification.actions';
import {
  getAddressList,
  deleteAddress,
  setDeleteModalMountedState,
  setAddressBookNotification,
  clearErrorState,
} from './AddressBook.actions';
import AddressView from '../views/AddressView';
import {
  getAddressListState,
  getAddressListFetchingState,
  showUpdatedNotificationState,
  deleteModalOpenState,
  showUpdatedNotificationOnModalState,
  getAddEditAddressLabels,
} from './AddressBook.selectors';
import { setDefaultShippingAddressRequest } from './DefaultShippingAddress.actions';

export class AddressBookContainer extends React.Component {
  static propTypes = {
    getAddressListAction: PropTypes.func.isRequired,
    clearAddressBookNotification: PropTypes.func.isRequired,
    addressList: PropTypes.shape([]).isRequired,
    isFetching: PropTypes.bool.isRequired,
    onDefaultShippingAddressClick: PropTypes.func.isRequired,
    showUpdatedNotification: PropTypes.bool.isRequired,
    onDeleteAddress: PropTypes.func.isRequired,
    deleteModalMountedState: PropTypes.func.isRequired,
    setDeleteModalMountState: PropTypes.func.isRequired,
    showUpdatedNotificationOnModal: PropTypes.bool.isRequired,
    labels: PropTypes.shape({}).isRequired,
    addressLabels: PropTypes.shape({}).isRequired,
    verificationResult: PropTypes.shape({}).isRequired,
    clearNotificationError: PropTypes.string.isRequired,
    resetVerifyAddressAction: PropTypes.func.isRequired,
  };

  componentDidMount() {
    const { getAddressListAction } = this.props;
    getAddressListAction();
  }

  componentWillUnmount() {
    const { clearAddressBookNotification } = this.props;
    clearAddressBookNotification();
  }

  render() {
    const {
      addressList,
      isFetching,
      onDefaultShippingAddressClick,
      showUpdatedNotification,
      onDeleteAddress,
      deleteModalMountedState,
      setDeleteModalMountState,
      showUpdatedNotificationOnModal,
      labels,
      addressLabels,
      verificationResult,
      clearNotificationError,
      resetVerifyAddressAction,
    } = this.props;
    if (List.isList(addressList)) {
      return (
        <AddressView
          addresses={addressList}
          isFetching={isFetching}
          labels={labels}
          onDefaultShippingAddressClick={onDefaultShippingAddressClick}
          showUpdatedNotification={showUpdatedNotification}
          onDeleteAddress={onDeleteAddress}
          deleteModalMountedState={deleteModalMountedState}
          clearNotificationError={clearNotificationError}
          setDeleteModalMountState={setDeleteModalMountState}
          showUpdatedNotificationOnModal={showUpdatedNotificationOnModal}
          addressLabels={addressLabels}
          verificationResult={verificationResult}
          resetVerifyAddress={resetVerifyAddressAction}
        />
      );
    }
    return null;
  }
}

export const mapDispatchToProps = dispatch => {
  return {
    getAddressListAction: payload => {
      dispatch(getAddressList(payload));
    },
    onDefaultShippingAddressClick: payload => {
      dispatch(setDefaultShippingAddressRequest(payload));
    },
    onDeleteAddress: payload => {
      dispatch(deleteAddress(payload));
    },
    setDeleteModalMountState: payload => {
      dispatch(setDeleteModalMountedState(payload));
    },
    clearAddressBookNotification: () => {
      dispatch(
        setAddressBookNotification({
          status: '',
        })
      );
    },
    clearNotificationError: () => {
      dispatch(clearErrorState());
    },
    resetVerifyAddressAction: () => {
      dispatch(resetVerifyAddress());
    },
  };
};

const mapStateToProps = state => {
  return {
    addressList: getAddressListState(state),
    isFetching: getAddressListFetchingState(state),
    showUpdatedNotification: showUpdatedNotificationState(state),
    showUpdatedNotificationOnModal: showUpdatedNotificationOnModalState(state),
    deleteModalMountedState: deleteModalOpenState(state),
    addressLabels: getAddEditAddressLabels(state),
    verificationResult: getVerificationResult(state),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddressBookContainer);
