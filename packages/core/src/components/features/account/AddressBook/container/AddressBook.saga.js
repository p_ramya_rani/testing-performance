// 9fbef606107a605d69c0edbcd8029e5d 
import { List } from 'immutable';
import { call, takeLatest, put, select } from 'redux-saga/effects';
import {
  resetEddData,
  clearEddTTL,
} from '@tcp/core/src/components/features/CnC/common/molecules/EddComponent/container/EddComponent.actions';
import { getAddressListState } from '@tcp/core/src/components/features/account/AddressBook/container/AddressBook.selectors';
import ADDRESS_BOOK_CONSTANTS from '../AddressBook.constants';
import { validateReduxCache } from '../../../../../utils/cache.util';
import { setAddressList, showLoader, hideLoader } from './AddressBook.actions';
import { getAddressListData } from '../../../../../services/abstractors/account';

export function* getAddressList() {
  try {
    yield put(showLoader());
    const contact = yield call(getAddressListData);
    const addressList = yield select(getAddressListState);
    if (!List.isList(addressList)) {
      yield put(clearEddTTL());
      yield put(resetEddData());
    }
    yield put(setAddressList(contact));
  } catch (err) {
    yield null;
  } finally {
    yield put(hideLoader());
  }
}

export function* AddressBookSaga() {
  const cachedAddressList = validateReduxCache(getAddressList);
  yield takeLatest(ADDRESS_BOOK_CONSTANTS.GET_ADDRESS_LIST, cachedAddressList);
}

export default AddressBookSaga;
