// 9fbef606107a605d69c0edbcd8029e5d 
import { call, put, takeLatest } from 'redux-saga/effects';
import { setLoaderState } from '@tcp/core/src/components/common/molecules/Loader/container/Loader.actions';
import {
  resetEddData,
  clearEddTTL,
} from '@tcp/core/src/components/features/CnC/common/molecules/EddComponent/container/EddComponent.actions';
import ADDRESS_BOOK_CONSTANTS from '../AddressBook.constants';
import {
  setDefaultShippingAddressSuccess,
  setDefaultShippingAddressFailure,
} from './DefaultShippingAddress.actions';
import { defaultShippingAddressApi } from '../../../../../services/abstractors/account';

export function* updateDefaultShippingAddress({ payload }) {
  yield put(setLoaderState(true));
  yield put(clearEddTTL());
  try {
    const res = yield call(defaultShippingAddressApi, payload);
    yield put(setLoaderState(false));
    yield put(setDefaultShippingAddressSuccess(res.body));
    yield put(resetEddData());
  } catch (err) {
    yield put(setLoaderState(false));
    yield put(setDefaultShippingAddressFailure(err));
  }
}

export function* SetDefaultShippingAddressSaga() {
  yield takeLatest(
    ADDRESS_BOOK_CONSTANTS.SET_DEFAULT_SHIPPING_ADDRESS_REQUEST,
    updateDefaultShippingAddress
  );
}
