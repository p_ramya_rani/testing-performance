// 9fbef606107a605d69c0edbcd8029e5d 
import { call, takeLatest, put } from 'redux-saga/effects';
import { setLoaderState } from '@tcp/core/src/components/common/molecules/Loader/container/Loader.actions';
import { clearCardListTTL } from '../../Payment/container/Payment.actions';
import ADDRESS_BOOK_CONSTANTS from '../AddressBook.constants';
import {
  updateAddressListOnDelete,
  updateAddressListOnDeleteErr,
  setDeleteModalMountedState,
} from './AddressBook.actions';
import { deleteAddressApi } from '../../../../../services/abstractors/account';
import { getUserInfo } from '../../User/container/User.actions';

export function* deleteAddress({ payload }) {
  yield put(setLoaderState(true));
  try {
    const res = yield call(deleteAddressApi, payload);
    yield put(setLoaderState(false));
    if (res.statusCode === 200) {
      yield put(updateAddressListOnDelete(res.body || ''));
      yield put(clearCardListTTL());
      yield put(setDeleteModalMountedState({ state: false }));
      yield put(
        getUserInfo({
          ignoreCache: true,
        })
      );
    } else {
      yield put(updateAddressListOnDeleteErr(res.error));
    }
  } catch (err) {
    yield put(setLoaderState(false));
    yield put(updateAddressListOnDeleteErr(err));
  }
}

export function* DeleteAddressSaga() {
  yield takeLatest(ADDRESS_BOOK_CONSTANTS.DELETE_ADDRESS, deleteAddress);
}

export default DeleteAddressSaga;
