// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

export default css`
  .col-border-color {
    border: 1px solid ${props => props.theme.colorPalette.gray[700]};
  }
`;
