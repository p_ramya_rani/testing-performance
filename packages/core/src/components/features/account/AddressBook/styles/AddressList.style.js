// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  .addressList__col {
    margin-bottom: ${props => props.theme.spacing.LAYOUT_SPACING.XS};
  }
`;

export default styles;
