// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  margin-bottom: ${props => props.theme.spacing.LAYOUT_SPACING.SM};
  .emptyAddressList__row {
    margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.MED};
  }
`;

export default styles;
