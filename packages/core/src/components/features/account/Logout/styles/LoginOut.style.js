// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  &.signOutLink {
    border-radius: 0;
  }
  ${props =>
    props.accountDrawer
      ? ` text-align: left;
          padding: ${props.theme.spacing.ELEM_SPACING.MED}  0px;
        `
      : `padding-bottom: ${props.theme.spacing.ELEM_SPACING.SM}`}
`;

export default styles;
