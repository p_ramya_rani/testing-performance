// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';

const logoutButtonStyle = (props) => {
  return `
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-between;
  font-size: ${props.theme.typography.fontSizes.fs12};
  padding-top: ${props.theme.spacing.ELEM_SPACING.SM};
  `;
};

const LogoutWrapper = styled.View`
  ${logoutButtonStyle};
`;

const logggedInStyle = (props) => {
  return `
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  font-size: ${props.theme.typography.fontSizes.fs12};
  width: 100%;
  margin:${props.fromOnBoarding ? '13px' : props.theme.spacing.ELEM_SPACING.LRG} 0 ${
    props.theme.spacing.ELEM_SPACING.XXL
  } 0;
  `;
};

const logggedInText = (props) => {
  return `
  font-size: ${props.theme.typography.fontSizes.fs12};
  width:100%;
  `;
};

const LoggedinWrapper = styled.View`
  ${logggedInStyle}
  ${(props) => props.loggedInWrapperStyle || ''}
`;

const LoggedinTextWrapper = styled.View`
  ${logggedInText}
`;

export { LogoutWrapper, LoggedinWrapper, LoggedinTextWrapper };
