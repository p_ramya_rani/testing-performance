// 9fbef606107a605d69c0edbcd8029e5d
import { call, takeLatest, put, take, select } from 'redux-saga/effects';
import { setLoaderState } from '@tcp/core/src/components/common/molecules/Loader/container/Loader.actions';
import BAG_PAGE_ACTIONS from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.actions';
import { resetBagCarouselModuleData } from '@tcp/core/src/components/common/molecules/BagCarouselModule/container/BagCarouselModule.actions';
import { setClickAnalyticsData, trackPageView } from '@tcp/core/src/analytics/actions';
import { trackForterAction, ForterActionType } from '@tcp/core/src/utils/forter.util';

import {
  getIsBagCarouselEnabled,
  getIsSNJEnabled,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import { clearLoginCookiesFromAsyncStore, removeValueFromAsyncStorage } from '@tcp/core/src/utils';
import { resetFavoritesReducer } from '@tcp/core/src/components/features/browse/Favorites/container/Favorites.actions';
import { resetCustomerHelpReducer } from '@tcp/core/src/components/features/account/CustomerHelp/container/CustomerHelp.actions';
import LOGOUT_CONSTANTS from '../LogOut.constants';
import { resetUserInfo, getUserInfo } from '../../User/container/User.actions';
import CONSTANTS from '../../User/User.constants';
import { closeOverlayModal } from '../../OverlayModal/container/OverlayModal.actions';
import { routerPush, isMobileApp, scrollPage } from '../../../../../utils';
import { navigateXHRAction } from '../../NavigateXHR/container/NavigateXHR.action';
import { LogoutApplication } from '../../../../../services/abstractors/account';
import {
  resetWalletAppState,
  resetCouponReducer,
} from '../../../CnC/common/organism/CouponAndPromos/container/Coupon.actions';
import { setCheckoutModalMountedState } from '../../LoginPage/container/LoginPage.actions';
import { resetAirmilesReducer } from '../../../CnC/common/organism/AirmilesBanner/container/AirmilesBanner.actions';
import CHECKOUT_ACTIONS from '../../../CnC/Checkout/container/Checkout.action';

// eslint-disable-next-line complexity, sonarjs/cognitive-complexity
export function* logoutSaga() {
  yield put(setLoaderState(true));
  try {
    const isSNJEnabled = yield select(getIsSNJEnabled);
    const res = yield call(LogoutApplication, isSNJEnabled);
    yield put(setLoaderState(false));
    if (res.statusCode === 200) {
      if (!isMobileApp()) {
        window.localStorage.removeItem('email_hash');
        window.localStorage.removeItem('defaultStore');
      }
      trackForterAction(ForterActionType.TAP, 'Logout');
      if (isMobileApp()) {
        removeValueFromAsyncStorage('email_hash');
        removeValueFromAsyncStorage('username');
        yield put(resetWalletAppState());
      }
      yield put(resetUserInfo());
      yield put(
        navigateXHRAction({
          headers: {
            actionTaken: 'logout',
          },
        })
      );
      yield put(
        setClickAnalyticsData({
          customEvents: ['event80'],
        })
      );
      yield put(CHECKOUT_ACTIONS.resetCheckoutReducer());
      yield put(resetAirmilesReducer());
      yield put(resetCouponReducer());
      yield put(BAG_PAGE_ACTIONS.resetCartReducer());
      yield put(resetFavoritesReducer());
      yield put(resetCustomerHelpReducer());
      yield put(getUserInfo({ ignoreCache: true, loadFavStore: true }));
      if (!isMobileApp()) {
        yield put(closeOverlayModal());
        yield put(setCheckoutModalMountedState({ state: false }));
        if (
          window.location.href.includes('account') ||
          window.location.href.includes('checkout/confirmation') ||
          window.location.href.includes('customer-help')
        ) {
          routerPush('/', '/home');
          scrollPage();
        } else {
          scrollPage();
        }
      }
      if (isMobileApp()) {
        const isBagCarouselEnabled = yield select(getIsBagCarouselEnabled);
        if (isBagCarouselEnabled) {
          yield put(resetBagCarouselModuleData());
        }
        yield call(clearLoginCookiesFromAsyncStore);
      }

      // Trgigger analytics event after reset user data
      yield take(CONSTANTS.RESET_USER_INFO);
      yield put(trackPageView());
    }
  } catch (err) {
    yield put(setLoaderState(false));
    if (!isMobileApp()) {
      routerPush('/', '/home');
    }
  }
}

export function* LogOutPageSaga() {
  yield takeLatest(LOGOUT_CONSTANTS.LOGOUT_APP, logoutSaga);
}

export default LogOutPageSaga;
