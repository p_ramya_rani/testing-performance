// 9fbef606107a605d69c0edbcd8029e5d 
import LOGOUT_CONSTANTS from '../LogOut.constants';

export const logout = payload => {
  return {
    type: LOGOUT_CONSTANTS.LOGOUT_APP,
    payload,
  };
};

export default LOGOUT_CONSTANTS;
