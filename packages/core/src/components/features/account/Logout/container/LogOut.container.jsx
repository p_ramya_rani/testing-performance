// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getUserLoggedInState } from '@tcp/core/src/components/features/account/User/container/User.selectors';
import { logout } from './LogOut.actions';
import LogOutView from '../views/LogOut.view';

class LogOutPageContainer extends React.PureComponent {
  componentDidUpdate() {
    const { isUserLoggedIn } = this.props;
    if (!isUserLoggedIn) {
      if (window && window.localStorage) delete window.localStorage.email_hash;
      /* eslint no-underscore-dangle: 0 */
      if (window && window._dataLayer) delete window._dataLayer.email_hash;
    }
  }

  render() {
    const { triggerLogout, underline, labels, accountDrawer } = this.props;
    return (
      <LogOutView
        underline={underline}
        triggerLogout={triggerLogout}
        labels={labels}
        accountDrawer={accountDrawer}
      />
    );
  }
}

LogOutPageContainer.propTypes = {
  triggerLogout: PropTypes.func.isRequired,
  labels: PropTypes.shape({}).isRequired,
  underline: PropTypes.bool,
  accountDrawer: PropTypes.bool,
  isUserLoggedIn: PropTypes.bool,
};

LogOutPageContainer.defaultProps = {
  underline: false,
  accountDrawer: false,
  isUserLoggedIn: false,
};

export const mapDispatchToProps = dispatch => {
  return {
    triggerLogout: payload => {
      dispatch(logout(payload));
    },
  };
};

const mapStateToProps = state => {
  return {
    isUserLoggedIn: getUserLoggedInState(state),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LogOutPageContainer);
export { LogOutPageContainer as LogOutPageContainerVanilla };
