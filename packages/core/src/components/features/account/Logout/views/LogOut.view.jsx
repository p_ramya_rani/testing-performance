// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import Button from '../../../../common/atoms/Button';
import styles from '../styles/LoginOut.style';

class LogOutView extends React.Component {
  constructor(props) {
    super(props);
    this.logoutApp = this.logoutApp.bind(this);
    const [body] = document.getElementsByTagName('body');
    this.body = body;
  }

  logoutApp(e) {
    e.preventDefault();
    const { triggerLogout } = this.props;
    triggerLogout();
  }

  render() {
    const { className, underline, labels, accountDrawer } = this.props;

    const otherProps = {};
    if (accountDrawer) {
      otherProps.fullWidth = true;
    }

    return (
      <React.Fragment>
        <Button
          onClick={this.logoutApp}
          nohover
          type="button"
          link
          underline={underline}
          className={`${className} signOutLink`}
          fontSizeVariation="large"
          anchorVariation="primary"
          {...otherProps}
        >
          {getLabelValue(labels, 'CREATE_ACC_SIGN_OUT')}
        </Button>
      </React.Fragment>
    );
  }
}

LogOutView.propTypes = {
  className: PropTypes.string.isRequired,
  triggerLogout: PropTypes.string.isRequired,
  labels: PropTypes.shape({}).isRequired,
  underline: PropTypes.bool,
  accountDrawer: PropTypes.bool,
};

LogOutView.defaultProps = {
  underline: false,
  accountDrawer: false,
};

export default withStyles(LogOutView, styles);
export { LogOutView as LogOutViewVanilla };
