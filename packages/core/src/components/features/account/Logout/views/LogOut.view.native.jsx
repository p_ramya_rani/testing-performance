// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import {
  setKeyChainCredentials,
  KeyChainConstants,
} from '@tcp/core/src/components/features/account/LoginPage/container/loginUtils/keychain.utils.native';
import { setValueInAsyncStorage } from '@tcp/core/src/utils/utils.app';
import BodyCopy from '../../../../common/atoms/BodyCopy';
import AccessibilityRoles from '../../../../../constants/AccessibilityRoles.constant';

class LogOutView extends React.Component {
  constructor(props) {
    super(props);
    this.logoutApp = this.logoutApp.bind(this);
  }

  logoutApp(e) {
    e.preventDefault();
    const { triggerLogout } = this.props;
    triggerLogout();

    // clear the username and password
    setKeyChainCredentials(KeyChainConstants.MOBILE_APP, '', '');
    setValueInAsyncStorage('mergeCartMessageShown', ''); // Clear message on logout
  }

  render() {
    const { className, labels } = this.props;
    return (
      <React.Fragment className={className}>
        <BodyCopy
          onPress={this.logoutApp}
          fontFamily="secondary"
          fontSize="fs13"
          fontWeight="regular"
          accessible
          accessibilityRole={AccessibilityRoles.Button}
          accessibilityLabel={getLabelValue(labels, 'lbl_overview_logout')}
          text={getLabelValue(labels, 'lbl_overview_logout')}
          color="gray.900"
        />
      </React.Fragment>
    );
  }
}

LogOutView.propTypes = {
  className: PropTypes.string.isRequired,
  triggerLogout: PropTypes.string.isRequired,
  labels: PropTypes.shape({}).isRequired,
};

export default LogOutView;
export { LogOutView as LogOutViewVanilla };
