// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { LogOutViewVanilla } from '../LogOut.view.native';

describe('LogOutView Page', () => {
  it('should render correctly', () => {
    const props = {
      className: '',
      labels: {
        CREATE_ACC_SIGN_OUT: 'hello',
      },
    };
    const tree = shallow(<LogOutViewVanilla {...props} />);
    expect(tree).toMatchSnapshot();
  });

  it('logout click', () => {
    const logoutApp = jest.fn().mockImplementation(() => true);
    const props = {
      className: '',
      labels: {},
    };
    shallow(<LogOutViewVanilla {...props} />);
    expect(logoutApp).not.toBeUndefined();
  });
});
