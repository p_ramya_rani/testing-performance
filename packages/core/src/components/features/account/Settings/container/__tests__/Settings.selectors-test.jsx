// 9fbef606107a605d69c0edbcd8029e5d 
import getAccountOverviewLabels from '../Settings.selectors';

describe('#Settings Selectors', () => {
  it('#getLabels should return Labels', () => {
    const state = {
      Labels: {
        accountOverview: {},
      },
    };
    expect(getAccountOverviewLabels(state)).toMatchObject({});
  });
});
