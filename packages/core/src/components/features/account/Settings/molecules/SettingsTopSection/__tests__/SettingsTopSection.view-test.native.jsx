// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import SettingsTopSection from '../views/SettingsTopSection.view.native';

describe('SettingsTopSection component', () => {
  it('should renders correctly', () => {
    const props = {
      labels: {},
      onRequestClose: jest.fn(),
    };
    const component = shallow(<SettingsTopSection {...props} />);
    expect(component).toMatchSnapshot();
  });
});
