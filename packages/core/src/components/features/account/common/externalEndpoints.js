// 9fbef606107a605d69c0edbcd8029e5d 
import { getAPIConfig, isMobileApp } from '@tcp/core/src/utils';

const externalEndpoints = () => {
  const apiConfigObj = getAPIConfig() || {};
  const { siteId = 'us', webAppDomain } = apiConfigObj;
  const domain = (isMobileApp && webAppDomain) || '';

  return {
    termsAndConditionsPage: `${domain}/${siteId}/help-center/myplacerewards#termsandconditions`,
    myPlaceRewardsPage: `${domain}/${siteId}/content/myplace-rewards-page`,
    privacyPolicyPage: `${domain}/${siteId}/help-center/policies#privacypolicy`,
    faqPage: `${domain}/${siteId}/help-center/faq`,
    policiesPage: `${domain}/${siteId}/help-center/policies`,
    mprTermsPage: `${domain}/${siteId}/content/mpr-terms`,
    internationalOrdersPage: 'https://www.borderfree.com/order-status/',
    appDownloadPage: `${domain}/${siteId}/content/mobile`,
    managePlaceCardPage: 'https://d.comenity.net/childrensplace/?ecid=manageacct',
    walletPage: 'https://mp.vibescm.com/c/76cmsb?data[loyalty_id]=',
    customerService: `${domain}/${siteId}/help-center/contact-us`,
  };
};

export default externalEndpoints;
