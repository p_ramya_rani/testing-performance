// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import ContentLink from '@tcp/core/src/components/features/account/common/molecule/ContentLink';
import { StyledAnchorWrapper, AnchorLeftMargin } from '../styles/RewardsFooterLinks.style.native';

export const RewardsFooterLinks = ({ programDetailsCta, termsConditionCta }) => {
  return (
    <>
      {programDetailsCta && termsConditionCta && (
        <StyledAnchorWrapper>
          <ContentLink
            fontSizeVariation="medium"
            underline
            urlKey="lbl_rewardprogram_link"
            anchorVariation="primary"
            dataLocator="my-rewards-program-details"
            text={programDetailsCta}
          />
          <AnchorLeftMargin>
            <ContentLink
              fontSizeVariation="medium"
              underline
              urlKey="lbl_termcondition_link"
              anchorVariation="primary"
              dataLocator="my-rewards-tnc"
              text={termsConditionCta}
            />
          </AnchorLeftMargin>
        </StyledAnchorWrapper>
      )}
    </>
  );
};

RewardsFooterLinks.propTypes = {
  programDetailsCta: PropTypes.string.isRequired,
  termsConditionCta: PropTypes.string.isRequired,
  labels: PropTypes.shape({}),
};

RewardsFooterLinks.defaultProps = {
  labels: {},
};

export default RewardsFooterLinks;
