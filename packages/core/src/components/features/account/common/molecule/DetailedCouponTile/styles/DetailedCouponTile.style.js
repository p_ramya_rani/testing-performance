// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  border: 1px solid
    ${props =>
      props.coupon.error ? props.theme.colorPalette.red[500] : props.theme.colorPalette.gray[500]};
  position: relative;
  width: 100%;
  .notification {
    background-color: ${props => props.theme.colorPalette.gray[800]};
    color: ${props => props.theme.colorPalette.white};
    position: absolute;
    top: 0;
    width: 100%;
  }
  .warning-icon {
    height: 15px;
    width: 13px;
  }
  .content {
    display: grid;
    min-height: 302px;
  }
  .IE_content {
    display: grid;
    min-height: 127px;
    padding-bottom: 175px;
  }

  .coupon {
    margin: 0 auto 8px;
  }
  .overlap {
    z-index: 1;
    position: relative;
  }

  .overlay {
    display: flex;
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    align-items: center;
    color: ${props => props.theme.colorPalette.white};
    background-color: ${props => props.theme.colors.REWARDS_OVERLAY};
    z-index: 1;
    justify-content: center;
  }
  .overlay__content {
    flex: none;
    width: 100%;
    text-align: center;
  }
  .overlap-text {
    color: ${props => props.theme.colorPalette.white};
  }

  .couponDetailsFont {
    padding-left: ${props => props.theme.spacing.LAYOUT_SPACING.XXS};
    padding-right: ${props => props.theme.spacing.LAYOUT_SPACING.XXS};
    @media ${props => props.theme.mediaQuery.large} {
      padding-left: 0;
      padding-right: 0;
    }
  }

  .top-content {
    align-self: flex-start;
  }

  .bottom-content {
    align-self: flex-end;
  }

  .IE_bottom-content {
    width: calc(100% - 24px);
    position: absolute;
    bottom: 0px;
  }

  &.IE_my-rewards {
    -ms-grid-row: ${props => Math.floor(props.index / 5) + 1};
    -ms-grid-column: ${props => (props.index % 5) + 1};
    margin-right: 24px;
    width: calc(100% - 24px);
  }

  .coupon-desc {
    display: flex;
    align-items: flex-end;
    justify-content: space-between;
  }

  .barcode-content {
    width: 100%;

    svg {
      width: 100%;
    }
  }
`;

export default styles;
