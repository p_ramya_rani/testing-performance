// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import Heading from '@tcp/core/src/components/common/atoms/Heading';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import ContentLink from '@tcp/core/src/components/features/account/common/molecule/ContentLink';
import styles from '../styles/PageHeadingWithLinks.styles';

export const PageHeadingWithLinks = ({
  className,
  programDetailsCta,
  termsConditionCta,
  children,
  labels,
  heading,
  ...otherProps
}) => {
  return (
    <>
      <BodyCopy className="elem-mb-XXL" component="div">
        <Heading component="h2" className={className} variant="h6" {...otherProps}>
          {heading}
        </Heading>
      </BodyCopy>
      {children}

      {programDetailsCta && termsConditionCta && (
        <BodyCopy textAlign="center">
          <ContentLink
            fontSizeVariation="large"
            underline
            urlKey="lbl_rewardprogram_link"
            anchorVariation="primary"
            dataLocator="program_details_cta"
            target="_blank"
          >
            {programDetailsCta}
          </ContentLink>
          <ContentLink
            fontSizeVariation="large"
            underline
            urlKey="lbl_termcondition_link"
            anchorVariation="primary"
            dataLocator="terms_condition_cta"
            className="elem-ml-XL"
          >
            {termsConditionCta}
          </ContentLink>
        </BodyCopy>
      )}
    </>
  );
};

PageHeadingWithLinks.propTypes = {
  className: PropTypes.string.isRequired,
  heading: PropTypes.string.isRequired,
  programDetailsCta: PropTypes.string.isRequired,
  termsConditionCta: PropTypes.string.isRequired,
  labels: PropTypes.shape({}),
  children: PropTypes.string.isRequired,
};

PageHeadingWithLinks.defaultProps = {
  labels: {},
};

export default withStyles(PageHeadingWithLinks, styles);
