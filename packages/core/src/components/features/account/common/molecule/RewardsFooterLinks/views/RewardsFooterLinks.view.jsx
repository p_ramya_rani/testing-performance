// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import ContentLink from '@tcp/core/src/components/features/account/common/molecule/ContentLink';
import styles from '../styles/RewardsFooterLinks.styles';

export const RewardsFooterLinks = ({ className, programDetailsCta, termsConditionCta }) => {
  return (
    <>
      <div className={className}>
        {programDetailsCta && termsConditionCta && (
          <BodyCopy className="footer-links-wrapper" component="div">
            <ContentLink
              fontSizeVariation="large"
              underline
              urlKey="lbl_rewardprogram_link"
              anchorVariation="primary"
              dataLocator="program_details_cta"
              target="_blank"
            >
              {programDetailsCta}
            </ContentLink>

            <ContentLink
              fontSizeVariation="large"
              underline
              urlKey="lbl_termcondition_link"
              anchorVariation="primary"
              dataLocator="terms_condition_cta"
              className="elem-ml-XXL"
            >
              {termsConditionCta}
            </ContentLink>
          </BodyCopy>
        )}
      </div>
    </>
  );
};

RewardsFooterLinks.propTypes = {
  className: PropTypes.string.isRequired,
  programDetailsCta: PropTypes.string.isRequired,
  termsConditionCta: PropTypes.string.isRequired,
  labels: PropTypes.shape({}),
};

RewardsFooterLinks.defaultProps = {
  labels: {},
};

export default withStyles(RewardsFooterLinks, styles);
