// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getLabelValue } from '@tcp/core/src/utils';
import { getCommonLabels } from '@tcp/core/src/components/features/account/Account/container/Account.selectors';
import Anchor from '@tcp/core/src/components/common/atoms/Anchor/views';

const mapStateToProps = state => ({
  commonLabels: getCommonLabels(state),
});

const ContentLink = ({ urlKey, commonLabels, ...otherProps }) => {
  return <Anchor url={getLabelValue(commonLabels, urlKey)} {...otherProps} />;
};

ContentLink.propTypes = {
  urlKey: PropTypes.string.isRequired,
  commonLabels: PropTypes.shape({}).isRequired,
};

export default connect(mapStateToProps)(ContentLink);
