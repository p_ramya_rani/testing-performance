/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Modalize } from 'react-native-modalize';
import { Portal } from 'react-native-portalize';
import isEmpty from 'lodash/isEmpty';
import createThemeColorPalette from '@tcp/core/styles/themes/createThemeColorPalette';
import ModalNative from '@tcp/core/src/components/common/molecules/Modal';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import CustomButton from '@tcp/core/src/components/common/atoms/Button';
import CustomRedesignButton from '@tcp/core/src/components/common/atoms/ButtonRedesign';
import { getLabelValue } from '@tcp/core/src/utils';
import {
  ModalViewWrapper,
  CancelText,
} from '@tcp/core/src/components/features/account/LoginPage/molecules/LoginForm/LoginForm.style.native';
import {
  LoggedinWrapper,
  LoggedinTextWrapper,
} from '@tcp/core/src/components/features/account/Logout/styles/LoginOut.style.native';
import CreateAccount from '@tcp/core/src/components/features/account/CreateAccount';
import LoginPageContainer from '@tcp/core/src/components/features/account/LoginPage';
import { isTCP } from '@tcp/core/src/utils/utils';
import { isAndroid } from '@tcp/core/src/utils/utils.app';
import ClickTracker from '../../../../../../../../../mobileapp/src/components/common/atoms/ClickTracker';
import names from '../../../../../../../constants/eventsName.constants';
import { isMobileApp } from '../../../../../../../utils';

class GuestLoginOverview extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      getComponentId: {
        login: '',
        createAccount: '',
        favorites: '',
      },
      horizontalBar: true,
      modalHeaderLbl: ' ',
      changePassword: false,
      showHeader: true,
      fromResetView: false,
    };
    this.modalizeRef = React.createRef();
    this.isAndroidDevice = isAndroid();
  }

  static getDerivedStateFromProps(props, state) {
    const deepLink = props.navigation && props.navigation.getParam('navigatedFromDeepLink');
    if (
      (props.isUserLoggedIn && state.showModal) ||
      (state.showModal && deepLink === 'change-password')
    ) {
      return { showModal: false };
    }
    return null;
  }

  componentDidMount() {
    const { changePassword } = this.state;
    if (!changePassword) this.navigateToChangePassword();
    const { navigation, isUserLoggedIn } = this.props;
    const customNavigateToPage = navigation && navigation.getParam('customNavigateToPage');
    if (customNavigateToPage && !isUserLoggedIn) {
      this.setState({
        showModal: true,
        getComponentId: {
          login: true,
          createAccount: false,
          favorites: false,
        },
        fromResetView: false,
      });
    }
  }

  componentDidUpdate() {
    const { changePassword } = this.state;
    if (!changePassword) this.navigateToChangePassword();
  }

  navigateToChangePassword = () => {
    const { labels, navigation } = this.props;
    const { showModal } = this.state;
    if (!isEmpty(labels) && navigation) {
      const {
        state: { params },
      } = navigation;
      if (params) {
        const { component } = params;

        if (component && component === 'change-password') {
          // eslint-disable-next-line react/no-did-update-set-state
          this.setState({ changePassword: true });
          if (showModal) {
            // if login modal is already opened
            this.setState({ showModal: false });
          }
          // using set timeout as labels doesn't load sometime just after opening app
          this.toggleModal({
            getComponentId: {
              login: true,
              createAccount: false,
              favorites: false,
            },
          });
        }
      }
    }
  };

  renderComponent = ({ navigation, getComponentId, isUserLoggedIn, fromResetView }) => {
    let componentContainer = null;
    if (getComponentId.login || getComponentId.favorites) {
      componentContainer = (
        <LoginPageContainer
          onRequestClose={this.toggleModal}
          navigation={navigation}
          isUserLoggedIn={isUserLoggedIn}
          variation={getComponentId.favorites && 'favorites'}
          showLogin={this.showloginModal}
          showCheckoutModal={this.showCheckoutModal}
          updateHeader={this.updateHeader}
          resetChangePasswordState={this.resetChangePasswordState}
          fromResetView={fromResetView}
        />
      );
    }
    if (getComponentId.createAccount) {
      componentContainer = (
        <CreateAccount
          showCheckoutModal={this.showCheckoutModal}
          showLogin={this.showloginModal}
          navigation={navigation}
          onRequestClose={this.toggleModal}
          updateHeader={this.updateHeader}
        />
      );
    }
    return <React.Fragment>{componentContainer}</React.Fragment>;
  };

  showloginModal = (fromResetView = false) => {
    this.setState({
      getComponentId: {
        login: true,
      },
      fromResetView,
    });
  };

  showCheckoutModal = () => {
    this.setState({
      getComponentId: {
        createAccount: true,
      },
    });
  };

  toggleModal = ({ getComponentId }) => {
    this.setState((state) => ({
      showModal: !state.showModal,
      getComponentId: getComponentId
        ? {
            login: getComponentId.login,
            createAccount: getComponentId.createAccount,
            favorites: getComponentId.favorites,
          }
        : '',
      fromResetView: false,
    }));
  };

  getModalHeader = (getComponentId, labels) => {
    let header = ' ';
    if (getComponentId.login || getComponentId.favorites) {
      header = getLabelValue(labels, 'lbl_overview_login_text');
      this.setState({
        horizontalBar: true,
        showHeader: true,
      });
    }
    if (getComponentId.createAccount) {
      header = getLabelValue(labels, 'lbl_overview_createAccount');
      this.setState({
        horizontalBar: true,
        showHeader: true,
      });
    }
    this.setState({ modalHeaderLbl: header });
  };

  updateHeader = () => {
    this.setState({
      modalHeaderLbl: ' ',
      horizontalBar: false,
      showHeader: false,
    });
  };

  resetChangePasswordState = () => {
    this.setState({
      changePassword: false,
    });
  };

  loyaltyClickAnalytics = (type) => {
    setTimeout(() => {
      const {
        joinAnalyticsData,
        loginAnalyticsData,
        trackClickAction,
        setClickAnalyticsDataAction,
      } = this.props;
      setClickAnalyticsDataAction({});
      let events = ['event125'];
      let analyticsData = joinAnalyticsData;
      let name = names.screenNames.loyalty_join_click;
      if (type === 'login') {
        events = ['event126'];
        analyticsData = loginAnalyticsData;
        name = names.screenNames.loyalty_login_click;
      }
      analyticsData.customEvents = events;
      analyticsData.loyaltyLocation = analyticsData.pageName;
      setClickAnalyticsDataAction(analyticsData);
      trackClickAction({ name });
    }, 100);
  };

  onOpen = () => this.modalizeRef.current?.open();

  closeAnimatedModal = () => this.modalizeRef.current?.close();

  renderModal = () => {
    const { animationCallback, isNewRedesignOnboarding, navigation, labels, isUserLoggedIn } =
      this.props;
    const { showModal, getComponentId, modalHeaderLbl, horizontalBar, showHeader, fromResetView } =
      this.state;
    const overrideCustomStyle = {
      borderWidth: 0.5,
    };

    if ((this.isAndroidDevice || !isNewRedesignOnboarding) && showModal) {
      return (
        <ModalNative
          isOpen={showModal}
          onRequestClose={this.toggleModal}
          heading={modalHeaderLbl}
          headingFontFamily="secondary"
          fontSize="fs16"
          isLoginOrSignup
          horizontalBar={horizontalBar}
          showHeader={showHeader}
          showTextForClose
          textForClose={getLabelValue(labels, 'lbl_header_text')}
          overrideCustomStyle={overrideCustomStyle}
          fullWidth
        >
          <ModalViewWrapper>
            {this.renderComponent({
              navigation,
              getComponentId,
              isUserLoggedIn,
              fromResetView,
            })}
          </ModalViewWrapper>
        </ModalNative>
      );
    }
    if (!this.isAndroidDevice && isNewRedesignOnboarding) {
      return (
        <Portal>
          <Modalize
            ref={this.modalizeRef}
            handlePosition="inside"
            onClose={() => animationCallback()}
          >
            <CancelText
              onPress={this.closeAnimatedModal}
              hitSlop={{ top: 20, right: 20, bottom: 20, left: 20 }}
            >
              <BodyCopy
                fontFamily="secondary"
                fontSize="fs14"
                textAlign="center"
                text="cancel" // ToDo: replace  from CMS in next PR
              />
            </CancelText>
            <ModalViewWrapper>
              {this.renderComponent({
                navigation,
                getComponentId,
                isUserLoggedIn,
                fromResetView,
              })}
            </ModalViewWrapper>
          </Modalize>
        </Portal>
      );
    }
    return null;
  };

  invokeAnimationCallback = () => {
    const { animationCallback } = this.props;
    if (!this.isAndroidDevice) animationCallback();
  };

  getRedesignStyles = (isNewRedesignOnboarding) => {
    return isNewRedesignOnboarding
      ? {
          fontSize: 'fs16',
          fontWeight: 'regular',
          fontFamily: 'secondary',
          paddings: '10px',
          textPadding: '0px 5px',
          margin: '10px 0px 0px',
          borderRadius: '16px',
          showShadow: true,
          buttonVariation: 'variable-width',
          height: '46px',
          wrapContent: false,
          width: '70%',
        }
      : {};
  };

  render() {
    const {
      isUserLoggedIn,
      labels,
      hideLogoutText,
      loggedInWrapperStyle,
      fromOnBoarding,
      joinAnalyticsData,
      loginAnalyticsData,
      isNewRedesignOnboarding,
    } = this.props;
    const { getComponentId } = this.state;
    this.getModalHeader(getComponentId, labels);
    const colorPallete = createThemeColorPalette();
    const CTA = fromOnBoarding ? ClickTracker : CustomButton;
    const margin = fromOnBoarding && isTCP() ? '16px 0 0 0' : '0 0 0 0';

    const redesignStyles = this.getRedesignStyles(isNewRedesignOnboarding);

    return (
      <>
        {!isUserLoggedIn && (
          <React.Fragment>
            {hideLogoutText ? null : (
              <LoggedinTextWrapper>
                <BodyCopy
                  fontFamily="secondary"
                  fontSize="fs14"
                  textAlign="center"
                  text={getLabelValue(labels, 'lbl_overview_logout_heading_Text_1')}
                />
                <BodyCopy
                  fontFamily="secondary"
                  fontSize="fs14"
                  textAlign="center"
                  text={getLabelValue(labels, 'lbl_overview_logout_heading_Text_2')}
                />
              </LoggedinTextWrapper>
            )}
            <LoggedinWrapper
              fromOnBoarding={fromOnBoarding}
              loggedInWrapperStyle={loggedInWrapperStyle}
            >
              <CTA
                as={isNewRedesignOnboarding ? CustomRedesignButton : CustomButton}
                clickData={joinAnalyticsData}
                name={isMobileApp() ? names.screenNames.join : 'ONBoarding_Join_Clicks_E154'}
                module="global"
                className="classBtn"
                color={colorPallete.text.secondary}
                fill={fromOnBoarding ? 'BLUE' : 'WHITE'}
                id="createAccount"
                type="submit"
                width="47%"
                data-locator=""
                {...redesignStyles}
                text={
                  fromOnBoarding && isTCP()
                    ? getLabelValue(labels, 'lbl_overview_createAccount')
                    : getLabelValue(labels, 'lbl_overview_join_text')
                }
                onPress={(e) => {
                  this.loyaltyClickAnalytics('join');
                  this.invokeAnimationCallback();
                  this.onOpen();
                  this.toggleModal({
                    e,
                    getComponentId: {
                      login: false,
                      createAccount: true,
                      favorites: false,
                    },
                  });
                }}
              />

              <CTA
                as={isNewRedesignOnboarding ? CustomRedesignButton : CustomButton}
                clickData={loginAnalyticsData}
                name={isMobileApp() ? names.screenNames.login : 'ONBoarding_Login_Clicks_E155'}
                module="global"
                fill={fromOnBoarding ? 'WHITE' : 'BLUE'}
                id="login"
                type="submit"
                data-locator=""
                width="47%"
                {...redesignStyles}
                text={getLabelValue(labels, 'lbl_overview_login_text')}
                margin={margin}
                onPress={(e) => {
                  this.loyaltyClickAnalytics('login');
                  this.invokeAnimationCallback();
                  this.onOpen();
                  this.toggleModal({
                    e,
                    getComponentId: {
                      login: true,
                      createAccount: false,
                      favorites: false,
                    },
                  });
                }}
              />
              {this.renderModal()}
            </LoggedinWrapper>
          </React.Fragment>
        )}
      </>
    );
  }
}

GuestLoginOverview.propTypes = {
  labels: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
  isUserLoggedIn: PropTypes.bool.isRequired,
  navigation: PropTypes.shape({}).isRequired,
  hideLogoutText: PropTypes.bool,
  loggedInWrapperStyle: PropTypes.shape({}),
  fromOnBoarding: PropTypes.bool,
  joinAnalyticsData: PropTypes.shape({}),
  loginAnalyticsData: PropTypes.shape({}),
  setClickAnalyticsDataAction: PropTypes.func,
  trackClickAction: PropTypes.func,
  isNewRedesignOnboarding: PropTypes.bool,
  animationCallback: PropTypes.func,
};

GuestLoginOverview.defaultProps = {
  labels: {},
  hideLogoutText: false,
  loggedInWrapperStyle: {},
  fromOnBoarding: false,
  joinAnalyticsData: {},
  loginAnalyticsData: {},
  setClickAnalyticsDataAction: () => {},
  trackClickAction: () => {},
  isNewRedesignOnboarding: false,
  animationCallback: () => {},
};

export default GuestLoginOverview;
