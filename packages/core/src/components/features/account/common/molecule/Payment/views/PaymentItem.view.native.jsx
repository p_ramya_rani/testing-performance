// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { Field, reduxForm } from 'redux-form';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import ImageComp from '@tcp/core/src/components/common/atoms/Image';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import Anchor from '@tcp/core/src/components/common/atoms/Anchor';
import CustomButton from '@tcp/core/src/components/common/atoms/Button';
import TextBox from '@tcp/core/src/components/common/atoms/TextBox';
import createValidateMethod from '@tcp/core/src/utils/formValidation/createValidateMethod';
import getStandardConfig from '@tcp/core/src/utils/formValidation/validatorStandardConfig';
import RecaptchaModal from '@tcp/core/src/components/common/molecules/recaptcha/recaptchaModal.native';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import {
  PaymentContainer,
  PaymentType,
  PaymentInfoContainer,
  PaymentInfo,
  RecaptchaWrapper,
  PaymentWrapper,
  CheckBalanceContainer,
} from '../styles/PaymentItem.style.native';

class PaymentItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      setRecaptchaModalMountedState: false,
    };
  }

  setRecaptchaModalMountState = () => {
    const { setRecaptchaModalMountedState } = this.state;
    this.setState({
      setRecaptchaModalMountedState: !setRecaptchaModalMountedState,
    });
  };

  handleGetGiftCardBalanceClick = (formData, card, onGetBalanceCard) => {
    onGetBalanceCard({ formData, card });
  };

  getGiftCardBalance = (key, checkbalanceValueInfo) => {
    return checkbalanceValueInfo && key && checkbalanceValueInfo.get(key.accountNo);
  };

  checkBalanceHandler = e => {
    const { isRecapchaEnabled, onGetBalanceCard, card } = this.props;
    if (isRecapchaEnabled) {
      this.setRecaptchaModalMountState(e);
    } else {
      onGetBalanceCard({
        formData: {
          recaptchaToken: '',
        },
        card,
      });
    }
  };

  checkIfBalance = balance => balance !== null && balance !== undefined;

  renderCardDetails = (variation, paymentInfo) => {
    return (
      <>
        {variation === 'edit' && (
          <PaymentInfoContainer>
            <ImageComp source={paymentInfo.icon} width={50} height={30} />
            <PaymentInfo>
              <BodyCopy
                fontFamily="secondary"
                fontSize="fs12"
                fontWeight="extrabold"
                text={paymentInfo.text}
              />
              {!!paymentInfo.subText && (
                <BodyCopy
                  fontFamily="secondary"
                  fontSize="fs10"
                  fontWeight="regular"
                  text={paymentInfo.subText}
                  color="gray.700"
                />
              )}
            </PaymentInfo>
          </PaymentInfoContainer>
        )}
      </>
    );
  };

  recaptchaRender({ setRecaptchaModalMountedState, onMessage }) {
    return (
      <React.Fragment>
        {setRecaptchaModalMountedState && (
          <RecaptchaModal
            onMessage={onMessage}
            setRecaptchaModalMountedState={setRecaptchaModalMountedState}
            toggleRecaptchaModal={this.setRecaptchaModalMountState}
            onClose={this.onClose}
          />
        )}
      </React.Fragment>
    );
  }

  render() {
    const {
      paymentInfo,
      handleComponentChange,
      isGiftCard,
      onGetBalanceCard,
      card,
      labels,
      checkbalanceValueInfo,
    } = this.props;
    const { setRecaptchaModalMountedState } = this.state;
    const variation = paymentInfo && paymentInfo.variation && paymentInfo.variation.toLowerCase();
    const balance = this.getGiftCardBalance(card, checkbalanceValueInfo);
    const onMessage = event => {
      if (event && event.nativeEvent.data) {
        const value = get(event, 'nativeEvent.data', '');
        if (value) {
          const formData = { recaptchaToken: value };
          this.setRecaptchaModalMountState();
          onGetBalanceCard({ formData, card });
        }
      }
    };

    return (
      <PaymentContainer>
        <PaymentWrapper>
          <PaymentType>
            <BodyCopy
              fontFamily="secondary"
              fontSize="fs14"
              fontWeight="extrabold"
              text={paymentInfo.title}
              color="gray.900"
            />
            {variation === 'add' && (
              <BodyCopy fontFamily="secondary" fontSize="fs13" text={paymentInfo.text} />
            )}
            {this.renderCardDetails(variation, paymentInfo)}
          </PaymentType>
          <Anchor
            anchorVariation="primary"
            text={paymentInfo.variation}
            onPress={() =>
              handleComponentChange(
                'paymentGiftCardsPageMobile',
                isGiftCard ? 'gift-card' : 'credit-card'
              )
            }
            underline
            fontSizeVariation="large"
            noLink
            dataLocator="payment-overview-editlink"
            color="gray.900"
            lineHeight="10"
          />
        </PaymentWrapper>

        {isGiftCard && variation === 'edit' && !this.checkIfBalance(balance) && (
          <RecaptchaWrapper>
            {this.recaptchaRender({
              labels,
              onMessage,
              setRecaptchaModalMountedState,
            })}
            <Field
              label=""
              title=""
              component={TextBox}
              type="hidden"
              name="recaptchaToken"
              id="recaptchaToken"
              data-locator="gift-card-recaptchcb"
              className="visibility-recaptcha"
            />

            {isGiftCard && !this.checkIfBalance(balance) && (
              <CheckBalanceContainer>
                <CustomButton
                  fill="BLUE"
                  text={getLabelValue(labels, 'lbl_overview_check_balance')}
                  width="190px"
                  onPress={this.checkBalanceHandler}
                />
              </CheckBalanceContainer>
            )}
          </RecaptchaWrapper>
        )}
      </PaymentContainer>
    );
  }
}

PaymentItem.propTypes = {
  paymentInfo: PropTypes.shape({}).isRequired,
  isRecapchaEnabled: PropTypes.bool,

  onGetBalanceCard: PropTypes.func.isRequired,
  card: PropTypes.shape({}).isRequired,
  handleComponentChange: PropTypes.func.isRequired,
  isGiftCard: PropTypes.bool.isRequired,
  labels: PropTypes.shape({}).isRequired,
  checkbalanceValueInfo: PropTypes.func.isRequired,
};

PaymentItem.defaultProps = {
  isRecapchaEnabled: false,
};

const validateMethod = createValidateMethod(getStandardConfig(['recaptchaToken']));

export default reduxForm({
  form: 'PaymentItemForm', // a unique identifier for this form
  ...validateMethod,
  enableReinitialize: true,
})(PaymentItem);

export { PaymentItem as PaymentItemVanilla };
