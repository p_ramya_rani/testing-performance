// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { StyledHeading } from '@tcp/core/src/components/common/atoms/styledWrapper';
import LineComp from '@tcp/core/src/components/common/atoms/Line';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import ContentLink from '@tcp/core/src/components/features/account/common/molecule/ContentLink';
import { StyledAnchorWrapper, AnchorLeftMargin } from '../styles/PageHeadingWithLinks.style.native';

export const PageHeadingWithLinks = ({
  programDetailsCta,
  termsConditionCta,
  children,
  heading,
  noTopPadding,
  noCTA,
}) => {
  return (
    <View>
      <StyledHeading noTopPadding={noTopPadding}>
        <BodyCopy
          fontSize="fs16"
          fontWeight="extrabold"
          dataLocator="Page_Heading_link_header"
          text={heading}
        />
      </StyledHeading>
      <LineComp marginBottom={28} borderWidth={1} borderColor="black" />
      {children}
      {!noCTA && (
        <StyledAnchorWrapper>
          <ContentLink
            fontSizeVariation="medium"
            underline
            urlKey="lbl_rewardprogram_link"
            anchorVariation="primary"
            dataLocator="my-rewards-program-details"
            text={programDetailsCta}
          />
          <AnchorLeftMargin>
            <ContentLink
              fontSizeVariation="medium"
              underline
              urlKey="lbl_termcondition_link"
              anchorVariation="primary"
              dataLocator="my-rewards-tnc"
              text={termsConditionCta}
            />
          </AnchorLeftMargin>
        </StyledAnchorWrapper>
      )}
    </View>
  );
};

PageHeadingWithLinks.propTypes = {
  heading: PropTypes.string.isRequired,
  programDetailsCta: PropTypes.string.isRequired,
  termsConditionCta: PropTypes.string.isRequired,
  labels: PropTypes.shape({}),
  children: PropTypes.string.isRequired,
  noTopPadding: PropTypes.bool,
  noCTA: PropTypes.bool,
};

PageHeadingWithLinks.defaultProps = {
  labels: {},
  noTopPadding: false,
  noCTA: false,
};

export default PageHeadingWithLinks;
