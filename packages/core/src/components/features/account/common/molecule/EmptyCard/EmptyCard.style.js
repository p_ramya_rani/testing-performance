// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  .emptyCard__body {
    margin-right: 0;
  }
  .emptyCard__description--desktop {
    margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.MED};
    display: none;
    @media ${props => props.theme.mediaQuery.medium} {
      margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.XL};
      display: block;
      line-height: 1.3;
    }
  }
  .emptyCard__description--mobile {
    margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.MED};
    line-height: 1.3;
    @media ${props => props.theme.mediaQuery.medium} {
      display: none;
    }
  }
  .emptyCard__heading {
    margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.XS};
    line-height: 1.08;
    font-weight: ${props => props.theme.typography.fontWeights.bold};
    @media ${props => props.theme.mediaQuery.smallMax} {
      margin-top: 6px;
    }
  }
  .emptyCard__imgWrapper {
    text-align: center;
  }
  .emptyCard__img {
    width: 64px;
  }
`;

export default styles;
