// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import CouponHelpModal from '@tcp/core/src/components/features/CnC/common/organism/CouponAndPromos/views/CouponHelpModal.view';
import BAG_PAGE_ACTIONS from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.actions';
import BagPageSelector from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.selectors';
import {
  getCouponList,
  applyCoupon,
  removeCoupon,
  setError,
  toggleNeedHelpModalState,
} from '../../../../../CnC/common/organism/CouponAndPromos/container/Coupon.actions';

import {
  getAllCoupons,
  getAllRewardsCoupons,
  getCouponsLabels,
  getCouponInitialFetchingState,
  getNeedHelpModalState,
  getNeedHelpContent,
} from '../../../../../CnC/common/organism/CouponAndPromos/container/Coupon.selectors';
import {
  getCommonLabels,
  getAccessibilityLabels,
} from '../../../../Account/container/Account.selectors';
import MyRewards from '../views';
import CouponDetailModal from '../../../../../CnC/common/organism/CouponAndPromos/views/CouponDetailModal.view';
import { toastMessageInfo } from '../../../../../../common/atoms/Toast/container/Toast.actions';
import { DEFAULT_TOAST_ERROR_MESSAGE_TTL } from '../../../../../../../config/site.config';

export class MyRewardsContainer extends PureComponent {
  static propTypes = {
    fetchCoupons: PropTypes.func.isRequired,
    view: PropTypes.string,
    coupons: PropTypes.shape([]).isRequired,
    rewardCoupons: PropTypes.shape([]).isRequired,
    couponsLabels: PropTypes.shape({}).isRequired,
    onApplyCouponToBagFromList: PropTypes.func,
    handleErrorCoupon: PropTypes.func,
    toastMessage: PropTypes.func,
    isCouponInitialFetchingState: PropTypes.bool,
    isNeedHelpModalOpen: PropTypes.bool,
    toggleNeedHelpModal: PropTypes.func.isRequired,
    needHelpRichText: PropTypes.string,
    fetchNeedHelpContent: PropTypes.func.isRequired,
    needHelpContentId: PropTypes.string,
    couponSelfHelp: PropTypes.bool,
  };

  static defaultProps = {
    view: 'reward',
    onApplyCouponToBagFromList: () => {},
    handleErrorCoupon: () => {},
    toastMessage: () => {},
    isCouponInitialFetchingState: false,
    isNeedHelpModalOpen: false,
    needHelpRichText: '',
    needHelpContentId: '',
    couponSelfHelp: false,
  };

  constructor(props) {
    super(props);
    this.state = {
      selectedCoupon: null,
    };
  }

  componentDidMount() {
    const { fetchCoupons, needHelpContentId, fetchNeedHelpContent, couponSelfHelp } = this.props;
    fetchCoupons(couponSelfHelp);
    fetchNeedHelpContent([needHelpContentId]);
  }

  /**
   * This function use for view coupon details for popup modal
   * can be passed in the component.
   * @param coupon - this is coupon data used for show coupon details
   */
  onViewCouponDetails = (coupon) => {
    this.setState({
      selectedCoupon: coupon,
    });
  };

  /**
   * This function use for close coupon details for popup modal
   * can be passed in the component.
   * @param coupon - this is coupon data used for show coupon details
   */
  onCloseCouponDetails = () => {
    this.setState({
      selectedCoupon: null,
    });
  };

  render() {
    const {
      coupons,
      rewardCoupons,
      couponsLabels,
      view,
      handleErrorCoupon,
      onApplyCouponToBagFromList,
      toastMessage,
      isCouponInitialFetchingState,
      isNeedHelpModalOpen,
      toggleNeedHelpModal,
      needHelpRichText,
      ...otherProps
    } = this.props;
    const { selectedCoupon } = this.state;
    const updateLabels = { ...couponsLabels, NEED_HELP_RICH_TEXT: needHelpRichText };

    return (
      <>
        <MyRewards
          coupons={view === 'reward' ? rewardCoupons : coupons}
          view={view}
          onViewCouponDetails={this.onViewCouponDetails}
          onApplyCouponToBagFromList={onApplyCouponToBagFromList}
          handleErrorCoupon={handleErrorCoupon}
          toastMessage={toastMessage}
          selectedCoupon={selectedCoupon}
          couponsLabels={couponsLabels}
          onRequestClose={this.onCloseCouponDetails}
          isFetching={isCouponInitialFetchingState}
          {...otherProps}
        />
        {selectedCoupon && (
          <CouponDetailModal
            labels={couponsLabels}
            openState={selectedCoupon}
            coupon={selectedCoupon}
            handleErrorCoupon={handleErrorCoupon}
            onRequestClose={this.onCloseCouponDetails}
            onApplyCouponToBagFromList={onApplyCouponToBagFromList}
          />
        )}
        <CouponHelpModal
          labels={updateLabels}
          openState={isNeedHelpModalOpen}
          coupon={selectedCoupon}
          onRequestClose={() => {
            toggleNeedHelpModal();
          }}
          heading="Help Modal"
        />
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  coupons: getAllCoupons(state),
  commonLabels: {
    ...getCommonLabels(state),
    ...getAccessibilityLabels(state),
  },
  rewardCoupons: getAllRewardsCoupons(state),
  couponsLabels: getCouponsLabels(state),
  isCouponInitialFetchingState: getCouponInitialFetchingState(state),
  isNeedHelpModalOpen: getNeedHelpModalState(state),
  needHelpRichText: getNeedHelpContent(state),
  needHelpContentId: BagPageSelector.getNeedHelpContent(state),
});

const analyticsData = {
  eventName: 'walletlinksclickevent',
  pageNavigationText: 'my account-my wallet-apply to bag',
  pageType: 'myplace',
};

export const mapDispatchToProps = (dispatch) => ({
  fetchCoupons: (couponSelfHelp) => {
    dispatch(getCouponList({ fromCSH: couponSelfHelp }));
  },
  onApplyCouponToBagFromList: (coupon) => {
    return new Promise((resolve, reject) => {
      dispatch(
        applyCoupon({
          formData: { couponCode: coupon.id, analyticsData },
          formPromise: { resolve, reject },
          coupon,
        })
      );
    });
  },
  onRemove: (coupon) => {
    return new Promise((resolve, reject) => {
      dispatch(
        removeCoupon({
          analyticsCustomData: {
            eventName: 'walletlinksclickevent',
            pageNavigationText: 'my account-my wallet-remove from bag',
          },
          coupon,
          formPromise: { resolve, reject },
        })
      );
    });
  },
  handleErrorCoupon: (coupon) => {
    const errorCoupenTimeout = setTimeout(() => {
      dispatch(setError({ msg: null, couponCode: coupon.id }));
      clearTimeout(errorCoupenTimeout);
    }, DEFAULT_TOAST_ERROR_MESSAGE_TTL);
  },
  toastMessage: (coupon) => {
    dispatch(toastMessageInfo(coupon.error));
    const toastMsgTimeout = setTimeout(() => {
      dispatch(setError({ msg: null, couponCode: coupon.id }));
      clearTimeout(toastMsgTimeout);
    }, DEFAULT_TOAST_ERROR_MESSAGE_TTL);
  },
  toggleNeedHelpModal: () => {
    dispatch(toggleNeedHelpModalState());
  },
  fetchNeedHelpContent: (contentIds) => {
    dispatch(BAG_PAGE_ACTIONS.fetchModuleX(contentIds));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(MyRewardsContainer);
