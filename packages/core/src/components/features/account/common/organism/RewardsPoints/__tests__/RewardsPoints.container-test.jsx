// 9fbef606107a605d69c0edbcd8029e5d 
import { shallow } from 'enzyme';
import React from 'react';
import { RewardsPointsContainer } from '../container/RewardsPoints.container';

describe('Rewards Points Container View', () => {
  it('should render Rewards Points Container Correctly', () => {
    const tree = shallow(<RewardsPointsContainer />);
    expect(tree).toMatchSnapshot();
  });
});
