// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

const OrdersTypeContainer = styled.View`
  flex: 1;
  flex-direction: row;
  margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.XXS};
`;

export default OrdersTypeContainer;
