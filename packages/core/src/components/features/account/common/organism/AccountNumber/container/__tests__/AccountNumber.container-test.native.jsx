// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { AccountNumberContainer } from '../AccountNumber.container.native';

describe('AccountNumberContainer container', () => {
  const props = {
    labels: {},
    commonLabels: {},
    myPlaceNumber: '123',
  };

  it('should render AccountNumberContainer component', () => {
    const component = shallow(<AccountNumberContainer {...props} />);
    expect(component).toMatchSnapshot();
  });
});
