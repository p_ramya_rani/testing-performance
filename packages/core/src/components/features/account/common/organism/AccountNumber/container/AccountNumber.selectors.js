// 9fbef606107a605d69c0edbcd8029e5d 
const getAccountOverviewLabels = labels => {
  return (labels && labels.accountOverview) || {};
};

export default getAccountOverviewLabels;
