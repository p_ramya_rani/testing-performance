// 9fbef606107a605d69c0edbcd8029e5d 
export const getBrierleySwitch = state => {
  return state.session &&
    state.session.siteDetails &&
    state.session.siteDetails.isBrierleyEnabled !== undefined
    ? state.session.siteDetails.isBrierleyEnabled
    : true;
};

export default getBrierleySwitch;
