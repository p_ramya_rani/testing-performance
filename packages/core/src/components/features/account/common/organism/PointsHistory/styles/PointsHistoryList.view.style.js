// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  .list-fontsizes {
    font-size: ${props => props.theme.typography.fontSizes.fs12};
    @media ${props => props.theme.mediaQuery.medium} {
      font-size: ${props => props.theme.typography.fontSizes.fs16};
    }
    @media ${props => props.theme.mediaQuery.large} {
      font-size: ${props => props.theme.typography.fontSizes.fs16};
    }
    word-wrap: break-word;
  }
`;

export default styles;
