// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { trackClick, setClickAnalyticsData } from '@tcp/core/src/analytics/actions';
import MyPlaceRewardsCreditCardComponent from '../views';
import {
  getMyPlaceRewardCreditCard,
  getCardListFetchingState,
} from '../../../../Payment/container/Payment.selectors';
import { toggleApplyNowModal } from '../../../../../../common/molecules/ApplyNowPLCCModal/container/ApplyNowModal.actions';
import MyPlaceRewardsCreditCardTileSkeleton from '../skeleton/MyPlaceRewardsCreditCardTileSkeleton.view';
import { getPageNameFromState } from '../../../../../../../reduxStore/selectors/session.selectors';

export class MyPlaceRewardsCreditCardTile extends React.PureComponent {
  static propTypes = {
    labels: PropTypes.shape({
      lbl_overview_manageYourCard: PropTypes.string,
    }),
    cardList: PropTypes.shape({}),
    toggleModal: PropTypes.func,
    handleComponentChange: PropTypes.func,
    isFetching: PropTypes.bool,
    setClickAnalyticsDataAction: PropTypes.func,
    trackClickAction: PropTypes.func,
    loyaltyPageName: PropTypes.string,
  };

  static defaultProps = {
    labels: {
      lbl_overview_manageYourCard: '',
    },
    cardList: {},
    toggleModal: () => {},
    handleComponentChange: () => {},
    isFetching: false,
    setClickAnalyticsDataAction: () => {},
    trackClickAction: () => {},
    loyaltyPageName: 'account',
  };

  openModal = e => {
    e.preventDefault();
    const { toggleModal } = this.props;
    toggleModal({ isModalOpen: true });
  };

  render() {
    const {
      cardList,
      labels,
      handleComponentChange,
      toggleModal,
      isFetching,
      trackClickAction,
      setClickAnalyticsDataAction,
      loyaltyPageName,
    } = this.props;
    const cardListValue = cardList && cardList.get(0);

    if (isFetching) {
      return <MyPlaceRewardsCreditCardTileSkeleton />;
    }
    return (
      <MyPlaceRewardsCreditCardComponent
        myPlaceRewardCard={cardListValue}
        labels={labels}
        openModal={this.openModal}
        handleComponentChange={handleComponentChange}
        toggleModal={toggleModal}
        trackClickAction={trackClickAction}
        setClickAnalyticsDataAction={setClickAnalyticsDataAction}
        loyaltyPageName={loyaltyPageName}
      />
    );
  }
}

export const mapDispatchToProps = dispatch => {
  return {
    toggleModal: payload => {
      dispatch(toggleApplyNowModal(payload));
    },
    trackClickAction: data => {
      dispatch(trackClick(data));
    },
    setClickAnalyticsDataAction: data => {
      dispatch(setClickAnalyticsData(data));
    },
  };
};

const mapStateToProps = state => {
  return {
    cardList: getMyPlaceRewardCreditCard(state),
    isFetching: getCardListFetchingState(state),
    loyaltyPageName: getPageNameFromState(state),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MyPlaceRewardsCreditCardTile);
