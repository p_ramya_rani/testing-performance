// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { BodyCopy } from '@tcp/core/src/components/common/atoms';
import { getLabelValue } from '@tcp/core/src/utils';
import ACCOUNT_CONSTANTS from '@tcp/core/src/components/features/account/Account/Account.constants';
import CustomButton from '../../../../../../common/atoms/Button';
import EmptyOrdersTile from '../../../molecule/EmptyOrdersTile';
import { OrdersTileItem } from '../molecules/OrderTileItem/views/OrdersTileItem.view';
import {
  UnderlineStyle,
  OrdersTileContainer,
  ButtonWrapperStyle,
} from '../styles/OrdersTile.style.native';
import ClickTracker from '../../../../../../../../../mobileapp/src/components/common/atoms/ClickTracker';
/*
OrdersTile component is used in AccountOverview screen on mobile app
*/
export const OrdersTile = ({ labels, ordersList, navigation, handleComponentChange }) => {
  const selectedOrders = ordersList && ordersList.orders && ordersList.orders.slice(0, 2);
  let ordersItemList;
  const AnalyticsData = {
    pageName: 'myplace',
    pageType: 'myplace',
    pageSection: 'myplace',
    pageSubSection: 'myplace',
    pageNavigationText: ACCOUNT_CONSTANTS.ACCOUNT_ANALYTICS.navigationText.overview_allOrders,
  };

  if (selectedOrders && selectedOrders.length) {
    ordersItemList = selectedOrders.map(orderItem => (
      <OrdersTileItem orderItem={orderItem} labels={labels} navigation={navigation} />
    ));
  } else {
    ordersItemList = <EmptyOrdersTile labels={labels} navigation={navigation} />;
  }
  return (
    <OrdersTileContainer>
      <BodyCopy
        fontFamily="secondary"
        fontSize="fs16"
        text={getLabelValue(labels, 'lbl_ordersTile_heading', 'orders')}
        color="black"
        fontWeight="extrabold"
      />
      <UnderlineStyle />
      {ordersItemList}
      <ButtonWrapperStyle>
        <ClickTracker
          as={CustomButton}
          clickData={AnalyticsData}
          name="account_cta"
          module="account"
          text={getLabelValue(labels, 'lbl_ordersTile_viewAllOrders', 'orders')}
          fill="BLUE"
          onPress={() => {
            handleComponentChange('myOrdersPageMobile');
          }}
        />
      </ButtonWrapperStyle>
    </OrdersTileContainer>
  );
};

OrdersTile.propTypes = {
  labels: PropTypes.shape({}),
  ordersList: PropTypes.shape({}).isRequired,
  navigation: PropTypes.shape({}).isRequired,
  handleComponentChange: PropTypes.isRequired,
};

OrdersTile.defaultProps = {
  labels: {
    lbl_ordersTile_heading: '',
    lbl_ordersTile_viewAllOrders: '',
  },
};

export default OrdersTile;
