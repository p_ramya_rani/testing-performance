// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { checkBalance } from '@tcp/core/src/components/features/account/Payment/container/Payment.actions';
import { getIsRecapchaEnabled } from '@tcp/core/src/reduxStore/selectors/session.selectors';
import {
  getCardListState,
  checkbalanceValue,
  getCardListFetchingState,
} from '@tcp/core/src/components/features/account/Payment/container/Payment.selectors';
import PaymentTileComponent from '../views';
import PaymentOverviewTileSkeleton from '../skeleton/PaymentOverviewTileSkeleton.view.native';

export class PaymentTile extends React.PureComponent {
  render() {
    const { isFetching } = this.props;
    if (isFetching) {
      return <PaymentOverviewTileSkeleton />;
    }
    return <PaymentTileComponent {...this.props} />;
  }
}

export const mapDispatchToProps = dispatch => {
  return {
    onGetBalanceCard: payload => {
      dispatch(checkBalance(payload));
    },
  };
};

const mapStateToProps = state => {
  return {
    cardList: getCardListState(state),
    checkbalanceValueInfo: checkbalanceValue(state),
    isFetching: getCardListFetchingState(state),
    isRecapchaEnabled: getIsRecapchaEnabled(state),
  };
};

PaymentTile.propTypes = {
  isFetching: PropTypes.bool.isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PaymentTile);
