/* eslint-disable max-lines */
/* eslint-disable complexity */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm, reset } from 'redux-form';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import ACCOUNT_CONSTANTS from '@tcp/core/src/components/features/account/Account/Account.constants';
import {
  cardIconMapping,
  loading,
} from '@tcp/core/src/components/features/account/common/molecule/CardTile/views/CardTile.utils';
import styles from './PaymentOverviewTile.style';
import { getIconPath } from '../../../../../../../utils';
import { Row, Col, BodyCopy, Image, Button, TextBox } from '../../../../../../common/atoms';
import Anchor from '../../../../../../common/atoms/Anchor';
import AccountOverviewTile from '../../../../../../common/molecules/AccountOverviewTile';
import Recaptcha from '../../../../../../common/molecules/recaptcha/recaptcha';
import createValidateMethod from '../../../../../../../utils/formValidation/createValidateMethod';
import getStandardConfig from '../../../../../../../utils/formValidation/validatorStandardConfig';
import internalEndpoints from '../../../internalEndpoints';

class PaymentOverviewTile extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isTokenDirty: false,
    };
    this.handleCheckBalanceClick = this.handleCheckBalanceClick.bind(this);
  }

  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch(reset('giftCardCheckBalanceForm'));
  }

  /**
   * This will return true if card balance is requested.
   * @param {string} key - redux state key
   * @returns {boolean} true/false based on card requested state
   */
  getIfGiftCardBalanceRequested = key => {
    const { checkbalanceValueInfo } = this.props;
    return checkbalanceValueInfo && checkbalanceValueInfo.has(key);
  };

  /**
   * This will return Gift card balance
   * @param {string} key - redux state key
   * @returns {number} gift card balance
   */
  getGiftCardBalance = key => {
    const { checkbalanceValueInfo } = this.props;
    const balance = checkbalanceValueInfo && checkbalanceValueInfo.get(key);
    return typeof balance === 'number' ? balance.toString() : balance;
  };

  /**
   * This will handle recaptcha verification
   * @param {string} token - recaptcha token
   */
  handleRecaptchaVerify = token => {
    const { change } = this.props;
    change('recaptchaToken', token);
    this.setState({ isTokenDirty: false });
  };

  /**
   * This will handle recaptcha expiration and clear token from state
   */
  handleRecaptchaExpired = () => {
    const { change } = this.props;
    change('recaptchaToken', '');
  };

  /**
   * Set Recaptcha container reference
   */
  attachReCaptchaRef = ref => {
    this.recaptcha = ref;
  };

  /**
   * This will render balance section
   * @param {string} labels - Account overview labels
   * @param {string} balance - Gift card balance
   * @param {string} isGiftCardBalanceRequested - Gift card requested
   */
  renderBalance = ({ labels, balance, isGiftCardBalanceRequested }) => {
    return (
      <React.Fragment>
        {balance && (
          <BodyCopy
            tag="span"
            fontSize="fs20"
            fontFamily="secondary"
            fontWeight="extrabold"
            className=""
            lineHeights="lh115"
          >
            {`$${balance}`}
          </BodyCopy>
        )}
        {!isGiftCardBalanceRequested && (
          <Button
            onClick={this.handleCheckBalanceClick}
            type="submit"
            data-locator="gift-card-checkbalance-btn"
            fill="BLACK"
            buttonVariation="variable-width"
          >
            {getLabelValue(labels, 'lbl_overview_check_balance')}
          </Button>
        )}
      </React.Fragment>
    );
  };

  /**
   * This will render Venmo Card List
   */

  renderVenmoCardList = () => {
    const { venmoCardList, labels } = this.props;
    return (
      <>
        {venmoCardList && venmoCardList.ccBrand && (
          <div className="heading venmo-tile">
            <BodyCopy component="div" className="elem-mt-LRG">
              <Row fullBleed>
                <Col
                  colSize={{
                    small: 6,
                    large: 12,
                    medium: 8,
                  }}
                  className="section-heading"
                >
                  <BodyCopy
                    component="div"
                    fontSize="fs14"
                    fontWeight="extrabold"
                    fontFamily="secondary"
                  >
                    {getLabelValue(labels, 'lbl_overview_venmo')}
                  </BodyCopy>
                  <Anchor
                    fontSizeVariation="large"
                    underline
                    anchorVariation="primary"
                    to={internalEndpoints.paymentPage.link}
                    asPath={internalEndpoints.paymentPage.path}
                  >
                    {getLabelValue(labels, 'lbl_overview_addressBookEdit')}
                  </Anchor>
                </Col>
              </Row>
              <Row fullBleed className="elem-mb-XXL">
                <Col
                  colSize={{
                    small: 5,
                    large: 10,
                    medium: 7,
                  }}
                >
                  <BodyCopy component="div" className="cardDetailsWrapper">
                    <Image
                      className="venmoCardList_img"
                      src={getIconPath(cardIconMapping[venmoCardList.ccBrand])}
                      alt={venmoCardList.ccBrand || venmoCardList.ccType}
                      width={50}
                    />
                    <BodyCopy
                      fontSize="fs12"
                      fontFamily="secondary"
                      className="elem-mt-XS elem-ml-SM"
                      fontWeight="extrabold"
                    >
                      {venmoCardList.properties.venmoUserId}
                    </BodyCopy>
                  </BodyCopy>
                </Col>
              </Row>
            </BodyCopy>
          </div>
        )}
      </>
    );
  };

  /**
   * This will render credit card list
   */
  renderCreditCardList = () => {
    const { creditCardDefault, labels } = this.props;
    return (
      <div className="heading">
        <div className="section-heading">
          <BodyCopy fontSize="fs14" fontWeight="extrabold" fontFamily="secondary">
            {getLabelValue(labels, 'lbl_overview_default_creditCard')}
          </BodyCopy>
          {creditCardDefault && creditCardDefault.ccType ? (
            <Anchor
              fontSizeVariation="large"
              underline
              anchorVariation="primary"
              to={internalEndpoints.paymentPage.link}
              asPath={internalEndpoints.paymentPage.path}
              aria-label="Edit default credit card"
            >
              {getLabelValue(labels, 'lbl_overview_addressBookEdit')}
            </Anchor>
          ) : (
            <Anchor
              fontSizeVariation="large"
              underline
              anchorVariation="primary"
              to={internalEndpoints.paymentPage.link}
              asPath={internalEndpoints.paymentPage.path}
            >
              {getLabelValue(labels, 'lbl_overview_addressBookAdd')}
            </Anchor>
          )}
        </div>
        <Row fullBleed className="elem-mb-MED">
          <Col
            colSize={{
              small: 6,
              large: 12,
              medium: 8,
            }}
          >
            {creditCardDefault && creditCardDefault.ccType ? (
              <BodyCopy component="div" className="cardDetailsWrapper">
                <Image
                  className="elem-mr-XS"
                  src={getIconPath(cardIconMapping[creditCardDefault.ccBrand])}
                  alt={creditCardDefault.ccBrand || creditCardDefault.ccType}
                  width={50}
                />
                <BodyCopy component="div" className="cardDescriptionWrapper">
                  <BodyCopy fontSize="fs12" fontFamily="secondary" fontWeight="extrabold">
                    <BodyCopy component="span" className="cardEndingSpace">
                      {getLabelValue(labels, 'lbl_overview_card_ending')}
                    </BodyCopy>
                    <BodyCopy component="span">{creditCardDefault.accountNo.slice(-4)}</BodyCopy>
                  </BodyCopy>
                  {creditCardDefault.ccType !== 'PLACE CARD' && (
                    <BodyCopy fontSize="fs10" fontFamily="secondary">
                      <BodyCopy component="span">
                        {getLabelValue(labels, 'lbl_overview_expires')}
                      </BodyCopy>
                      <BodyCopy component="span"> </BodyCopy>
                      <BodyCopy component="span">
                        {`0${creditCardDefault.expMonth.trim()}`.slice(-2)}
                      </BodyCopy>
                      <BodyCopy component="span">/</BodyCopy>
                      <BodyCopy component="span">{creditCardDefault.expYear.slice(-2)}</BodyCopy>
                    </BodyCopy>
                  )}
                </BodyCopy>
              </BodyCopy>
            ) : (
              <BodyCopy fontSize="fs14" fontFamily="secondary">
                {getLabelValue(labels, 'lbl_overview_add_creditCard')}
              </BodyCopy>
            )}
          </Col>
        </Row>
      </div>
    );
  };

  /**
   * This will render gift card list
   */
  renderGiftCardList = () => {
    const { giftCardList, commonLabels, handleSubmit, labels, isRecapchaEnabled } = this.props;

    const isGiftCardBalanceRequested =
      giftCardList &&
      giftCardList.ccType &&
      this.getIfGiftCardBalanceRequested(giftCardList.accountNo);

    const balance =
      giftCardList && giftCardList.ccType && this.getGiftCardBalance(giftCardList.accountNo);

    return (
      <BodyCopy component="div" className="elem-mt-LRG">
        <div className="section-heading">
          <BodyCopy component="div" fontSize="fs14" fontWeight="extrabold" fontFamily="secondary">
            {getLabelValue(labels, 'lbl_overview_giftCard')}
          </BodyCopy>
          {giftCardList && giftCardList.ccType ? (
            <Anchor
              fontSizeVariation="large"
              underline
              anchorVariation="primary"
              to={internalEndpoints.paymentPage.link}
              asPath={internalEndpoints.paymentPage.path}
              aria-label="Edit default gift card"
            >
              {getLabelValue(labels, 'lbl_overview_addressBookEdit')}
            </Anchor>
          ) : (
            <Anchor
              fontSizeVariation="large"
              underline
              anchorVariation="primary"
              to={internalEndpoints.paymentPage.link}
              asPath={internalEndpoints.paymentPage.path}
            >
              {getLabelValue(labels, 'lbl_overview_addressBookAdd')}
            </Anchor>
          )}
        </div>
        <Row fullBleed className="elem-mb-XXL">
          <Col
            colSize={{
              small: 5,
              large: 10,
              medium: 7,
            }}
          >
            {giftCardList && giftCardList.ccType ? (
              <>
                <BodyCopy component="div" className="cardDetailsWrapper">
                  <Image
                    className="elem-mr-XS"
                    src={getIconPath(cardIconMapping[giftCardList.ccBrand])}
                    alt={giftCardList.ccBrand || giftCardList.ccType}
                    width={50}
                  />
                  <BodyCopy fontSize="fs12" fontFamily="secondary" fontWeight="extrabold">
                    <BodyCopy component="span" className="cardEndingSpace">
                      {getLabelValue(labels, 'lbl_overview_card_ending')}
                    </BodyCopy>
                    <BodyCopy component="span">{giftCardList.accountNo.slice(-4)}</BodyCopy>
                  </BodyCopy>
                </BodyCopy>
                <div className="giftcardTile__wrapper">
                  <form
                    name="giftcardBalance_1"
                    onSubmit={handleSubmit}
                    autoComplete="off"
                    noValidate
                  >
                    <div className="giftcardTile__row">
                      {isRecapchaEnabled && !isGiftCardBalanceRequested && (
                        <>
                          <Recaptcha
                            ref={this.attachReCaptchaRef}
                            onloadCallback={this.handleRecaptchaOnload}
                            verifyCallback={this.handleRecaptchaVerify}
                            expiredCallback={this.handleRecaptchaExpired}
                          />
                          <Field
                            component={TextBox}
                            title=""
                            type="hidden"
                            placeholder="recaptcha value"
                            name="recaptchaToken"
                            id="recaptchaToken"
                            data-locator="gift-card-recaptchcb"
                          />
                        </>
                      )}
                      {loading(isGiftCardBalanceRequested, commonLabels, balance)}
                      {this.remainBalance(isGiftCardBalanceRequested, balance)}
                    </div>
                  </form>
                </div>
              </>
            ) : (
              <BodyCopy fontSize="fs14" fontFamily="secondary">
                {getLabelValue(labels, 'lbl_overview_add_giftCard')}
              </BodyCopy>
            )}
          </Col>
        </Row>
      </BodyCopy>
    );
  };

  /**
   * This will render remaining balance section
   * @param {string} isGiftCardBalanceRequested - Gift card requested
   * @param {string} balance - Gift card balance
   */
  remainBalance = (isGiftCardBalanceRequested, balance) => {
    const { labels } = this.props;
    return (
      <React.Fragment>
        {balance && (
          <BodyCopy
            tag="span"
            fontSize="fs14"
            fontFamily="secondary"
            fontWeight="semibold"
            className=""
            lineHeights="lh115"
          >
            {balance && getLabelValue(labels, 'lbl_overview_remaining_balance')}
          </BodyCopy>
        )}
        {this.renderBalance({ balance, isGiftCardBalanceRequested, labels })}
      </React.Fragment>
    );
  };

  /**
   * Handle Button click for check balance
   * @param {object} e - Button event
   */
  handleCheckBalanceClick = e => {
    const { isTokenDirty } = this.state;
    const { change, handleSubmit, giftCardList } = this.props;
    e.preventDefault();
    if (isTokenDirty) {
      change('recaptchaToken', '');
      this.setState({
        isTokenDirty: false,
      });
      return;
    }

    /**
     * Submit Gift Card Form
     * @param {object} formData - Gift Card form object
     */
    handleSubmit(formData => {
      const { onGetBalanceCard } = this.props;
      onGetBalanceCard({ formData, card: giftCardList });
    })();
  };

  render() {
    const { className, labels } = this.props;
    const clickEventdata = {
      customEvents: [],
      eventName: ACCOUNT_CONSTANTS.ACCOUNT_ANALYTICS.navigationName.overview_viewPayment,
      pageNavigationText: ACCOUNT_CONSTANTS.ACCOUNT_ANALYTICS.navigationText.overview_viewPayment,
    };

    return (
      <AccountOverviewTile
        title={getLabelValue(labels, 'lbl_overview_paymentHeading')}
        ctaTitle={getLabelValue(labels, 'lbl_overview_paymentCTA')}
        ctaLink={internalEndpoints.paymentPage.link}
        ctaPath={internalEndpoints.paymentPage.path}
        clickEventdata={clickEventdata}
      >
        <div className={className}>
          {this.renderCreditCardList()}
          {this.renderVenmoCardList()}
          {this.renderGiftCardList()}
        </div>
      </AccountOverviewTile>
    );
  }
}

const validateMethod = createValidateMethod(getStandardConfig(['recaptchaToken']));

PaymentOverviewTile.propTypes = {
  className: PropTypes.string,
  labels: PropTypes.shape({
    lbl_overview_paymentHeading: PropTypes.string,
    lbl_overview_paymentCTA: PropTypes.string,
  }),
  commonLabels: PropTypes.shape({}),
  creditCardDefault: PropTypes.shape({}),
  giftCardList: PropTypes.shape({}),
  venmoCardList: PropTypes.shape({}),
  dispatch: PropTypes.func.isRequired,
  checkbalanceValueInfo: PropTypes.string.isRequired,
  onGetBalanceCard: PropTypes.func.isRequired,
  handleSubmit: PropTypes.string.isRequired,
  change: PropTypes.string.isRequired,
  isRecapchaEnabled: PropTypes.bool.isRequired,
};

PaymentOverviewTile.defaultProps = {
  className: '',
  labels: {
    lbl_overview_paymentHeading: '',
    lbl_overview_paymentCTA: '',
  },
  commonLabels: {},
  creditCardDefault: {},
  giftCardList: {},
  venmoCardList: {},
};

export default reduxForm({
  form: 'giftCardCheckBalanceForm',
  destroyOnUnmount: false,
  enableReinitialize: true,
  ...validateMethod,
})(withStyles(PaymentOverviewTile, styles));
export { PaymentOverviewTile as PaymentOverviewTileVanilla };
