// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

const RewardsOverviewContainer = styled.View`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: center;
  flex: 1;
  margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.XXXL};
`;

const MyRewardsWrapper = styled.View`
  flex: 1;
  align-items: center;
`;

const TextWrapper = styled.View`
  height: 65%;
  flex-direction: column-reverse;
`;
const LastTextWrapper = styled.View`
  height: 65%;
  flex-direction: column-reverse;
  width: 85px;
`;
const VerticalLine = styled.View`
  border-left-width: 1px;
  border-color: ${props => props.theme.colorPalette.gray[700]};
  height: 45px;
  align-self: flex-start;
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.SM};
`;

export { RewardsOverviewContainer, MyRewardsWrapper, TextWrapper, LastTextWrapper, VerticalLine };
