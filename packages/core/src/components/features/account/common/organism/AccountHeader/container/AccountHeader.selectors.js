// 9fbef606107a605d69c0edbcd8029e5d 
import { createSelector } from 'reselect';
import { ACCOUNTHEADER_REDUCER_KEY } from '../../../../../../../constants/reducer.constants';

export const getAccountHeaderState = state => {
  return state[ACCOUNTHEADER_REDUCER_KEY];
};

export const getRewardsPointsBannerContent = createSelector(
  getAccountHeaderState,
  state => state && state.get('rewardsPointsBannerContent')
);
