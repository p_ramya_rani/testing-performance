// 9fbef606107a605d69c0edbcd8029e5d 
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { BodyCopy } from '@tcp/core/src/components/common/atoms';
import { View } from 'react-native';
import Carousel from '@tcp/core/src/components/common/molecules/Carousel';
import { getScreenWidth } from '@tcp/core/src/utils';
import { ViewWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import ContentLink from '@tcp/core/src/components/features/account/common/molecule/ContentLink';
import {
  CouponHeading,
  StyledAnchorWrapper,
  AnchorLeftMargin,
  CarouselContainer,
} from '../styles/MyRewards.style.native';
import DetailedCouponTile from '../../../molecule/DetailedCouponTile';
import EmptyRewards from '../../../molecule/EmptyRewards';
import CouponDetailModal from '../../../../../CnC/common/organism/CouponAndPromos/views/CouponDetailModal.view.native';
import { COUPON_STATUS } from '../../../../../../../services/abstractors/CnC/CartItemTile';
import MyRewardsSkeleton from '../skeleton/MyRewardsSkeleton.view.native';

/**
 * Module height and width.
 * Height is fixed for mobile
 * Width can vary as per device width.
 */
const MODULE_HEIGHT = 220;
const MODULE_WIDTH = getScreenWidth();

/**
 * This Component return the app reawrd tile
 */
class MyRewards extends PureComponent {
  /**
   * @desc Returns app reawrd detail tile
   * Content render on the basis of copoun items.
   */
  renderView = ({ item }) => {
    const {
      commonLabels,
      coupons,
      onViewCouponDetails,
      onApplyCouponToBagFromList,
      toastMessage,
      onRemove,
      isApplyingOrRemovingCoupon,
      couponsLabels,
    } = this.props;
    const isApplyingCoupon = !!coupons.find(
      coupon => coupon.status === COUPON_STATUS.APPLYING || coupon.status === COUPON_STATUS.REMOVING
    );
    return (
      <DetailedCouponTile
        key={item.id}
        labels={commonLabels}
        couponsLabels={couponsLabels}
        coupon={item}
        onViewCouponDetails={onViewCouponDetails}
        onApplyCouponToBagFromList={onApplyCouponToBagFromList}
        toastMessage={toastMessage}
        onRemove={onRemove}
        isDisabled={isApplyingOrRemovingCoupon || isApplyingCoupon}
        className="elem-mb-LRG"
      />
    );
  };

  renderCarousel = ({ coupons, navigation, labels }) => {
    return coupons.size > 0 ? (
      <CarouselContainer>
        <Carousel
          data={coupons.toArray()}
          renderItem={this.renderView}
          height={MODULE_HEIGHT}
          width={MODULE_WIDTH}
          variation="show-arrow"
          showDots
          darkArrow
          autoplay={false}
          options={{
            autoplay: false,
          }}
        />
      </CarouselContainer>
    ) : (
      <EmptyRewards navigation={navigation} labels={labels} />
    );
  };

  render() {
    const {
      labels,
      showLink,
      navigation,
      coupons,
      couponsLabels,
      selectedCoupon,
      isFetching,
      ...otherProps
    } = this.props;
    const heading = `${getLabelValue(labels, 'lbl_my_rewards_heading', 'placeRewards')} (${
      coupons.size
    })`;
    const isSelected = selectedCoupon !== null;
    return (
      <View>
        {selectedCoupon && (
          <CouponDetailModal
            labels={couponsLabels}
            openState={isSelected}
            coupon={selectedCoupon}
            {...otherProps}
          />
        )}
        <ViewWithSpacing spacingStyles="margin-bottom-LRG margin-top-LRG">
          <CouponHeading>
            <BodyCopy
              fontFamily="secondary"
              fontSize="fs16"
              fontWeight="bold"
              className="my-rewards-heading"
              data-locator="my-rewards-heading"
              text={heading}
            />
          </CouponHeading>
        </ViewWithSpacing>
        {isFetching && <MyRewardsSkeleton />}
        {!isFetching && this.renderCarousel({ coupons, navigation, labels })}

        {showLink && (
          <StyledAnchorWrapper>
            <ContentLink
              fontSizeVariation="medium"
              underline
              urlKey="lbl_rewardprogram_link"
              anchorVariation="primary"
              dataLocator="my-rewards-program-details"
              text={getLabelValue(labels, 'lbl_my_rewards_program_details', 'placeRewards')}
            />
            <AnchorLeftMargin>
              <ContentLink
                fontSizeVariation="medium"
                underline
                urlKey="lbl_termcondition_link"
                anchorVariation="primary"
                dataLocator="my-rewards-tnc"
                text={getLabelValue(labels, 'lbl_common_tnc', 'placeRewards')}
              />
            </AnchorLeftMargin>
          </StyledAnchorWrapper>
        )}
      </View>
    );
  }
}

MyRewards.propTypes = {
  labels: PropTypes.shape({ placeRewards: {} }),
  commonLabels: PropTypes.shape({}),
  coupons: PropTypes.shape([]),
  onViewCouponDetails: PropTypes.func,
  onApplyCouponToBagFromList: PropTypes.func,
  onRemove: PropTypes.func,
  toastMessage: PropTypes.func,
  isApplyingOrRemovingCoupon: PropTypes.bool,
  showLink: PropTypes.bool,
  selectedCoupon: PropTypes.shape({}),
  couponsLabels: PropTypes.shape({}),
  navigation: PropTypes.shape({}),
  isFetching: PropTypes.bool,
};

MyRewards.defaultProps = {
  labels: {
    placeRewards: {
      lbl_my_rewards_program_details: '',
      lbl_my_rewards_heading: '',
      lbl_common_tnc: '',
    },
  },
  commonLabels: {},
  coupons: [],
  onViewCouponDetails: () => {},
  onApplyCouponToBagFromList: () => {},
  onRemove: () => {},
  toastMessage: () => {},
  isApplyingOrRemovingCoupon: false,
  showLink: false,
  selectedCoupon: {},
  couponsLabels: {},
  navigation: {},
  isFetching: false,
};

export default MyRewards;
