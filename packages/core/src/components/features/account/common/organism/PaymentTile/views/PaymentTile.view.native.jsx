// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import { getIconCard } from '@tcp/core/src/utils/index.native';
import { cardIconMapping } from '@tcp/core/src/components/features/account/common/molecule/CardTile/views/CardTile.utils';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import CardImage from '@tcp/core/src/components/common/molecules/CardImage';
import ACCOUNT_CONSTANTS from '@tcp/core/src/components/features/account/Account/Account.constants';
import { BodyCopyWithSpacing } from '../../../../../../common/atoms/styledWrapper';
import PaymentItem from '../../../molecule/Payment';
import CustomButton from '../../../../../../common/atoms/Button';
import {
  UnderlineStyle,
  PaymentTileContainer,
  ButtonWrapperStyle,
} from '../styles/PaymentTile.style.native';
import ClickTracker from '../../../../../../../../../mobileapp/src/components/common/atoms/ClickTracker';

const AnalyticsData = {
  pageName: 'myplace',
  pageType: 'myplace',
  pageSection: 'myplace',
  pageSubSection: 'myplace',
  pageNavigationText: ACCOUNT_CONSTANTS.ACCOUNT_ANALYTICS.navigationText.overview_viewPayment,
};

export class PaymentTile extends React.PureComponent {
  static propTypes = {
    cardList: PropTypes.arrayOf({}),
    labels: PropTypes.shape({}),
    handleComponentChange: PropTypes.func.isRequired,
    isRecapchaEnabled: PropTypes.bool.isRequired,
    checkbalanceValueInfo: PropTypes.func.isRequired,
    onGetBalanceCard: PropTypes.func.isRequired,
  };

  static defaultProps = {
    cardList: [],
    labels: {},
  };

  getCreditCardView = (card, isAddVariation) => {
    const { labels, handleComponentChange, checkbalanceValueInfo } = this.props;
    const cardTileProps = {
      title: getLabelValue(labels, 'lbl_overview_default_creditCard'),
      text: !isAddVariation
        ? `${getLabelValue(labels, 'lbl_overview_card_ending')} ${card.accountNo.slice(-4)}`
        : getLabelValue(labels, 'lbl_overview_add_creditCard'),
      subText:
        !isAddVariation && card.ccType !== 'PLACE CARD'
          ? `${getLabelValue(labels, 'lbl_overview_expires')} ${card.expMonth.trim()}/${
              card.expYear
            }`
          : '',
      variation: !isAddVariation
        ? getLabelValue(labels, 'lbl_overview_addressBookEdit')
        : getLabelValue(labels, 'lbl_overview_addressBookAdd'),
      icon: !isAddVariation ? getIconCard(cardIconMapping[card.ccBrand]) : '',
    };
    return (
      <PaymentItem
        paymentInfo={cardTileProps}
        handleComponentChange={handleComponentChange}
        isGiftCard={false}
        labels={labels}
        checkbalanceValueInfo={checkbalanceValueInfo}
      />
    );
  };

  getGiftCardView = (card, isAddVariation) => {
    const {
      labels,
      handleComponentChange,
      onGetBalanceCard,
      checkbalanceValueInfo,
      isRecapchaEnabled,
    } = this.props;
    const balance = (card && this.getGiftCardBalance(card.accountNo, checkbalanceValueInfo)) || '';

    const cardTileProps = {
      title: getLabelValue(labels, 'lbl_overview_giftCard'),
      text: !isAddVariation
        ? `${getLabelValue(labels, 'lbl_overview_card_ending')} ${card.accountNo.slice(-4)}`
        : getLabelValue(labels, 'lbl_overview_add_giftCard'),
      subText:
        !isAddVariation && balance
          ? `${getLabelValue(labels, 'lbl_overview_remaining_balance')}: $${balance}`
          : '',
      variation: !isAddVariation
        ? getLabelValue(labels, 'lbl_overview_addressBookEdit')
        : getLabelValue(labels, 'lbl_overview_addressBookAdd'),
      icon: !isAddVariation ? getIconCard(cardIconMapping.GC) : '',
    };
    return (
      <PaymentItem
        paymentInfo={cardTileProps}
        handleComponentChange={handleComponentChange}
        isGiftCard
        card={card}
        onGetBalanceCard={onGetBalanceCard}
        checkbalanceValueInfo={checkbalanceValueInfo}
        labels={labels}
        isRecapchaEnabled={isRecapchaEnabled}
      />
    );
  };

  getVenmoCardView = card => {
    const { labels } = this.props;
    const venmoPayment = {
      userName: card.properties.venmoUserId,
      ccBrand: card.ccBrand,
      ccType: card.ccType,
      defaultInd: true,
    };
    return (
      <>
        <BodyCopyWithSpacing
          fontFamily="secondary"
          fontSize="fs16"
          text={getLabelValue(labels, 'lbl_overview_venmo')}
          color="black"
          fontWeight="bold"
          spacingStyles="margin-bottom-SM"
        />
        <CardImage card={venmoPayment} cardNumber={card.properties.venmoUserId} fontSize="fs12" />
        <UnderlineStyle />
      </>
    );
  };
  /**
   * Get the gift card balance
   * @param {*} key
   * @param {*} checkbalanceValueInfo
   */

  getGiftCardBalance = (key, checkbalanceValueInfo) => {
    return checkbalanceValueInfo && checkbalanceValueInfo.get(key);
  };

  getGiftCard = cardList => {
    const cards =
      cardList && cardList.size > 0 && cardList.filter(card => card.ccType === 'GiftCard');
    return cards && cards.size && cards.get(cards.size - 1);
  };

  getCreditCardList = cardList =>
    cardList &&
    cardList.size > 0 &&
    cardList.filter(
      card => card.ccType !== 'GiftCard' && card.ccType !== 'VENMO' && card.defaultInd
    );

  getVenmoCard = cardList => {
    const cards = cardList && cardList.size > 0 && cardList.filter(card => card.ccType === 'VENMO');
    return cards && cards.size && cards.get(cards.size - 1);
  };

  render() {
    const { cardList, labels, handleComponentChange } = this.props;
    const creditCardList = this.getCreditCardList(cardList);
    const giftCard = this.getGiftCard(cardList);
    const venmoCard = this.getVenmoCard(cardList);
    return (
      <PaymentTileContainer>
        <BodyCopy
          fontFamily="secondary"
          fontSize="fs16"
          text={getLabelValue(labels, 'lbl_overview_paymentHeading')}
          color="black"
          fontWeight="extrabold"
        />
        <UnderlineStyle />
        {creditCardList && creditCardList.size > 0
          ? creditCardList.map(card => this.getCreditCardView(card, false))
          : this.getCreditCardView(null, true)}
        <UnderlineStyle />
        {!!venmoCard && this.getVenmoCardView(venmoCard)}
        {giftCard ? this.getGiftCardView(giftCard, false) : this.getGiftCardView(null, true)}
        <ButtonWrapperStyle>
          <ClickTracker
            as={CustomButton}
            clickData={AnalyticsData}
            name="account_cta"
            module="account"
            text={getLabelValue(labels, 'lbl_overview_view_payments')}
            fill="BLUE"
            onPress={() => handleComponentChange('paymentGiftCardsPageMobile')}
          />
        </ButtonWrapperStyle>
      </PaymentTileContainer>
    );
  }
}

export default PaymentTile;
