// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { PointHistoryViewSkeletonVanilla } from '../PointHistoryViewSkeleton.view';

describe('PointHistoryViewSkeleton component', () => {
  it('should renders correctly', () => {
    const props = {
      className: 'sample-class',
    };
    const component = shallow(<PointHistoryViewSkeletonVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
