// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { PaymentOverviewTileVanilla } from '../PaymentOverviewTile.view';

describe('PaymentOverviewTile component', () => {
  it('should render correctly', () => {
    const component = shallow(<PaymentOverviewTileVanilla />);
    expect(component).toMatchSnapshot();
  });
});
