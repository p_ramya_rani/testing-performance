// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  .cardDetailsWrapper {
    display: flex;
    padding-top: 12px;
    img {
      border: 1px solid ${props => props.theme.colorPalette.gray[500]};
      border-radius: ${props => props.theme.spacing.ELEM_SPACING.XS};
      width: 50px;
      height: 31px;
    }
  }
  .cardDescriptionWrapper {
    display: flex;
    flex-direction: column;
    justify-content: center;
  }
  .section-heading {
    display: flex;
    justify-content: space-between;
  }
  .venmo-tile {
    @media ${props => props.theme.mediaQuery.medium} {
      display: none;
    }
  }
  .cardExpiryWrapper {
    color: ${props => props.theme.colorPalette.gray[700]};
  }
  .recaptcha {
    -webkit-transform: scale(0.6);
    -ms-transform: scale(0.6);
    -moz-transform: scale(0.6);
    transform: scale(0.6);
    position: relative;
    left: -60px;
  }
  & .giftcardTile__wrapper {
    display: flex;
    align-items: flex-end;
  }
  & .input-fields-wrapper {
    height: auto;
  }
  & .TextBox__error {
    padding-bottom: ${props => props.theme.spacing.ELEM_SPACING.XS};
  }
  & .TextBox__label {
    display: none;
    height: 100%;
    min-height: ${props => (props.card && props.card.ccType === 'VENMO' ? '173px' : '')};
    padding: ${props => props.theme.spacing.ELEM_SPACING.MED}
      ${props => props.theme.spacing.ELEM_SPACING.MED};
    @media ${props => props.theme.mediaQuery.medium} {
      padding: ${props => props.theme.spacing.ELEM_SPACING.LRG}
        ${props => props.theme.spacing.ELEM_SPACING.MED};
    }
  }
  .cardEndingSpace {
    margin-right: ${props => props.theme.spacing.ELEM_SPACING.XXS};
  }
`;

export default styles;
