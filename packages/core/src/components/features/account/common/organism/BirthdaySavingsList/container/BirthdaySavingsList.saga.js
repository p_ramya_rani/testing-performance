// 9fbef606107a605d69c0edbcd8029e5d
import { call, takeLatest, put } from 'redux-saga/effects';
import { setLoaderState } from '@tcp/core/src/components/common/molecules/Loader/container/Loader.actions';
import { setClickAnalyticsData, trackClick, trackFormError } from '@tcp/core/src/analytics/actions';
import CONSTANTS from '../BirthdaySavingsList.constants';
import { setUserChildren, getUserInfo } from '../../../../User/container/User.actions';
import {
  getChildren,
  deleteChild,
  addChildBirthday,
} from '../../../../../../../services/abstractors/account';
import {
  getChildrenAction,
  updateBirthdaySavingSuccess,
  updateBirthdaySavingError,
} from './BirthdaySavingsList.actions';
import names from '../../../../../../../constants/eventsName.constants';
import { isMobileApp } from '../../../../../../../utils';

/**
 * @function getChildrenSaga
 * @description This function will call getChildren Abstractor to get children birthday saving list
 */
export function* getChildrenSaga({ payload }) {
  if (payload && payload.skipLoader) {
    yield put(setLoaderState(false));
  } else {
    yield put(setLoaderState(true));
  }
  try {
    const response = yield call(getChildren);
    yield put(setLoaderState(false));
    yield put(
      setUserChildren({
        children: response,
      })
    );
    // yield put(
    //   setClickAnalyticsData({
    //     eventName: isMobileApp() ? names.screenNames.birthday_success : 'birthdayClubSign_ups_e16',
    //     customEvents: ['event16'],
    //     pageNavigationText: 'my account-earn extra points-view points history',
    //     clickEvent: true,
    //   })
    // );
    yield put(
      trackClick({
        name: isMobileApp() ? names.screenNames.birthday_signup : 'birthdayClubSign_ups_e16',
        module: 'account',
      })
    );
  } catch (err) {
    yield put(setLoaderState(false));
  }
}

/**
 * @function removeChildSaga
 * @description This function will call getChildren Abstractor to get children birthday saving list
 */
export function* removeChildSaga({ payload }) {
  yield put(setLoaderState(true));
  try {
    const response = yield call(deleteChild, payload);
    yield put(setLoaderState(false));
    yield put(getChildrenAction());
    yield put(updateBirthdaySavingSuccess(response));
  } catch (err) {
    yield put(setLoaderState(false));
    yield put(updateBirthdaySavingError(err));
  }
}

/**
 * @function addChildSaga
 * @description This function will call addChildBirthday Abstractor to add children birthday saving list
 */
export function* addChildrenSaga({ payload }) {
  yield put(setLoaderState(true));
  try {
    const response = yield call(addChildBirthday, payload);
    yield put(setLoaderState(false));
    yield put(
      getUserInfo({
        ignoreCache: true,
      })
    );
    yield put(getChildrenAction());
    yield put(updateBirthdaySavingSuccess(response));
    yield put(
      setClickAnalyticsData({
        eventName: isMobileApp() ? names.screenNames.birthday_success : 'birthdayClubSign_ups_e16',
        customEvents: ['event16'],
        pageNavigationText: 'my account-earn extra points-view points history',
        clickEvent: true,
      })
    );
  } catch (err) {
    const { childName, acceptAddChildAgreement, lastName, firstName, gender } = payload;
    const requiredList3Val = { childName, acceptAddChildAgreement, lastName, firstName, gender };
    yield put(
      trackFormError({ formName: CONSTANTS.ADD_CHILD_BIRTHDAY_FORM, formData: requiredList3Val })
    );

    yield put(setLoaderState(false));
    yield put(updateBirthdaySavingError(err));
  }
}

/**
 * @function BirthdaySavingsListSaga
 * @description watcher function for getChildrenSaga.
 */
export function* BirthdaySavingsListSaga() {
  yield takeLatest(CONSTANTS.GET_CHILDREN, getChildrenSaga);
  yield takeLatest(CONSTANTS.REMOVE_CHILD, removeChildSaga);
  yield takeLatest(CONSTANTS.ADD_CHILD, addChildrenSaga);
}

export default BirthdaySavingsListSaga;
