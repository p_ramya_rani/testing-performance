// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import BagPageSelector from '../../../../../CnC/BagPage/container/BagPage.selectors';
import BAG_PAGE_ACTIONS from '../../../../../CnC/BagPage/container/BagPage.actions';
import { getCouponList } from '../../../../../CnC/common/organism/CouponAndPromos/container/Coupon.actions';
import CouponAndPromos from '../../../../../CnC/common/organism/CouponAndPromos';
import { getCouponFetchingState } from '../../../../../CnC/common/organism/CouponAndPromos/container/Coupon.selectors';

export class MyOffersCouponsContainer extends PureComponent {
  componentDidMount() {
    const {
      needHelpContent,
      fetchNeedHelpContent,
      fetchCoupons,
      isCouponFetching,
      couponSelfHelp,
    } = this.props;
    fetchNeedHelpContent([needHelpContent]);
    if (!isCouponFetching) {
      fetchCoupons(couponSelfHelp);
    }
  }

  render() {
    const { closedOverlay, couponSelfHelp } = this.props;
    return (
      <CouponAndPromos
        showAccordian={false}
        isCarouselView
        closedOverlay={closedOverlay}
        couponSelfHelp={couponSelfHelp}
      />
    );
  }
}

export const mapDispatchToProps = (dispatch) => ({
  fetchNeedHelpContent: (contentIds) => {
    dispatch(BAG_PAGE_ACTIONS.fetchModuleX(contentIds));
  },
  fetchCoupons: (couponSelfHelp) => {
    dispatch(getCouponList({ fromCSH: couponSelfHelp }));
  },
});

export const mapStateToProps = (state) => ({
  needHelpContent: BagPageSelector.getNeedHelpContent(state),
  isCouponFetching: getCouponFetchingState(state),
});

MyOffersCouponsContainer.propTypes = {
  fetchCoupons: PropTypes.func.isRequired,
  closedOverlay: PropTypes.func,
  fetchNeedHelpContent: PropTypes.func.isRequired,
  needHelpContent: PropTypes.string,
  isCouponFetching: PropTypes.bool,
  couponSelfHelp: PropTypes.bool,
};

MyOffersCouponsContainer.defaultProps = {
  closedOverlay: () => {},
  needHelpContent: '',
  isCouponFetching: false,
  couponSelfHelp: false,
};

export default connect(mapStateToProps, mapDispatchToProps)(MyOffersCouponsContainer);
