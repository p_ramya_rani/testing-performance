// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { BodyCopy, Anchor } from '@tcp/core/src/components/common/atoms';
import { BodyCopyWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';
import { getLabelValue } from '@tcp/core/src/utils';
import ImageComp from '@tcp/core/src/components/common/atoms/Image';
import {
  ContactPreferencesTileItemContainer,
  RightContainer,
  MiddleContainer,
  ImageWrapper,
} from '../styles/ContactPreferencesTileItem.style.native';

const PushEnabledIcon = require('../../../../../../../../../../../mobileapp/src/assets/images/push-enabled.png');
const PushDisabledIcon = require('../../../../../../../../../../../mobileapp/src/assets/images/push-disabled.png');
const SmsDisabledIcon = require('../../../../../../../../../../../mobileapp/src/assets/images/sms-disabled.png');
const SmsEnabledIcon = require('../../../../../../../../../../../mobileapp/src/assets/images/sms-enabled.png');

const ContactPreferencesTileItem = ({ labels, customerPreferences, handleComponentChange }) => {
  const { placeRewardsPush, placeRewardsSms } = customerPreferences;
  const addEditText =
    placeRewardsPush || placeRewardsSms ? 'lbl_preference_tileEdit' : 'lbl_preference_tileAdd';
  return (
    <>
      <ContactPreferencesTileItemContainer>
        <BodyCopy
          fontFamily="secondary"
          fontSize="fs14"
          text={getLabelValue(labels, 'lbl_preference_tileContactPreference', 'preferences')}
          color="black"
        />
        <MiddleContainer>
          <ImageWrapper>
            {placeRewardsSms ? (
              <ImageComp
                source={SmsEnabledIcon}
                width={30}
                height={30}
                alt={getLabelValue(labels, 'lbl_sms_enabled_icon')}
              />
            ) : (
              <ImageComp
                source={SmsDisabledIcon}
                width={30}
                height={30}
                alt={getLabelValue(labels, 'lbl_sms_disabled_icon')}
              />
            )}
            <BodyCopyWithSpacing
              fontFamily="secondary"
              fontSize="fs10"
              fontWeight="semibold"
              text={getLabelValue(labels, 'lbl_preference_tileTextText', 'preferences')}
              accessibilityState={{ selected: placeRewardsSms }}
              accessibilityLabel={getLabelValue(
                labels,
                'lbl_preference_tileTextText',
                'preferences'
              ).toLowerCase()}
              color="black"
              spacingStyles="margin-top-XS"
            />
          </ImageWrapper>
          <ImageWrapper spacingStyles="margin-left-LRG">
            {placeRewardsPush ? (
              <ImageComp
                source={PushEnabledIcon}
                width={24}
                height={30}
                alt={getLabelValue(labels, 'lbl_push_enabled_icon')}
              />
            ) : (
              <ImageComp
                source={PushDisabledIcon}
                width={24}
                height={30}
                alt={getLabelValue(labels, 'lbl_push_disabled_icon')}
              />
            )}
            <BodyCopyWithSpacing
              fontFamily="secondary"
              fontSize="fs10"
              fontWeight="semibold"
              accessibilityState={{ selected: placeRewardsPush }}
              accessibilityLabel={getLabelValue(
                labels,
                'lbl_preference_tileAppText',
                'preferences'
              ).toLowerCase()}
              text={getLabelValue(labels, 'lbl_preference_tileAppText', 'preferences')}
              color="black"
              spacingStyles="margin-top-XS"
            />
          </ImageWrapper>
        </MiddleContainer>
        <RightContainer>
          <Anchor
            anchorVariation="primary"
            text={getLabelValue(labels, addEditText, 'preferences')}
            onPress={() =>
              handleComponentChange('myPreferencePageMobile', 'myPreferenceSubscription')
            }
            underline
            fontSizeVariation="large"
            noLink
            dataLocator=""
            color="gray.900"
          />
        </RightContainer>
      </ContactPreferencesTileItemContainer>
    </>
  );
};

ContactPreferencesTileItem.propTypes = {
  labels: PropTypes.shape({}),
  customerPreferences: PropTypes.shape({}),
  handleComponentChange: PropTypes.func.isRequired,
};

ContactPreferencesTileItem.defaultProps = {
  labels: {
    lbl_preference_tileContactPreference: '',
    lbl_preference_tileAdd: '',
    lbl_preference_tileEdit: '',
    lbl_preference_tileTextText: '',
    lbl_preference_tileAppText: '',
  },
  customerPreferences: {},
};

export default ContactPreferencesTileItem;
