// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getIsRecapchaEnabled } from '@tcp/core/src/reduxStore/selectors/session.selectors';
import PaymentOverviewTileComponent from '../views';
import {
  getCreditCardDefault,
  getGiftCards,
  getVenmoCards,
  getCardListFetchingState,
  checkbalanceValue,
} from '../../../../Payment/container/Payment.selectors';
import { checkBalance } from '../../../../Payment/container/Payment.actions';
import PaymentOverviewTileSkelton from '../skelton/PaymentOverviewTileSkelton.view';

export class PaymentOverviewTile extends React.PureComponent {
  static propTypes = {
    checkbalanceValueInfo: PropTypes.string.isRequired,
    onGetBalanceCard: PropTypes.func.isRequired,
    labels: PropTypes.shape({
      lbl_overview_paymentHeading: PropTypes.string,
      lbl_overview_paymentCTA: PropTypes.string,
    }),
    commonLabels: PropTypes.shape({}),
    creditCardDefault: PropTypes.shape({}),
    giftCardList: PropTypes.shape({}),
    venmoCardList: PropTypes.shape({}),
    isFetching: PropTypes.bool,
    isRecapchaEnabled: PropTypes.bool,
  };

  static defaultProps = {
    labels: {
      lbl_overview_paymentHeading: '',
      lbl_overview_paymentCTA: '',
    },
    commonLabels: {},
    creditCardDefault: {},
    giftCardList: {},
    venmoCardList: {},
    isFetching: false,
    isRecapchaEnabled: false,
  };

  render() {
    const {
      creditCardDefault,
      giftCardList,
      venmoCardList,
      labels,
      commonLabels,
      isFetching,
      checkbalanceValueInfo,
      onGetBalanceCard,
      isRecapchaEnabled,
    } = this.props;
    const creditCardDefaultValue = creditCardDefault && creditCardDefault.get(0);
    const giftCardListValue = giftCardList && giftCardList.get(-1);
    const venmoCardListValue = venmoCardList && venmoCardList.get(0);

    if (isFetching) {
      return <PaymentOverviewTileSkelton />;
    }
    return (
      <PaymentOverviewTileComponent
        creditCardDefault={creditCardDefaultValue}
        giftCardList={giftCardListValue}
        venmoCardList={venmoCardListValue}
        labels={labels}
        commonLabels={commonLabels}
        checkbalanceValueInfo={checkbalanceValueInfo}
        onGetBalanceCard={onGetBalanceCard}
        isRecapchaEnabled={isRecapchaEnabled}
      />
    );
  }
}

export const mapDispatchToProps = dispatch => {
  return {
    onGetBalanceCard: payload => {
      dispatch(checkBalance(payload));
    },
  };
};

const mapStateToProps = state => {
  return {
    creditCardDefault: getCreditCardDefault(state),
    giftCardList: getGiftCards(state),
    venmoCardList: getVenmoCards(state),
    isFetching: getCardListFetchingState(state),
    checkbalanceValueInfo: checkbalanceValue(state),
    isRecapchaEnabled: getIsRecapchaEnabled(state),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PaymentOverviewTile);
