// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { MyRewardsSkeletonVanilla } from '../MyRewardsSkeleton.view';

describe('MyRewardsSkeleton component', () => {
  it('should renders correctly', () => {
    const props = {
      className: 'sample-class',
    };
    const component = shallow(<MyRewardsSkeletonVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
