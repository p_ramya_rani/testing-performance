// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  height: 100%;
  .cardDetailsWrapper {
    display: flex;
    align-items: center;

    img {
      border: 1px solid ${props => props.theme.colorPalette.gray[500]};
      border-radius: ${props => props.theme.spacing.ELEM_SPACING.XS};
      width: 50px;
      height: 31px;
    }
  }
  .container-top {
    display: flex;
    flex-direction: column;
    height: 100%;
    justify-content: space-between;
  }
  .list-style {
    text-decoration: none;
    li {
      list-style-type: disc;
      line-height: ${props => props.theme.fonts.lineHeight.normal};
      padding-left: 0.5em;
      span {
        margin: -7px;
      }
    }
  }
  .cardEndingSpace {
    margin-right: ${props => props.theme.spacing.ELEM_SPACING.XXS};
  }
`;

export default styles;
