// 9fbef606107a605d69c0edbcd8029e5d 
import { EARNEXTRAPOINTS_ACTION_PATTERN } from '../../../../../../constants/reducer.constants';

const EARNEXTRAPOINTS_CONSTANTS = {
  GET_EARNEXTRAPOINTS_LIST: `${EARNEXTRAPOINTS_ACTION_PATTERN}GET_EARNEXTRAPOINTS_LIST`,
  GET_EARNEDPOINTS_NOTIFICATION: `${EARNEXTRAPOINTS_ACTION_PATTERN}GET_EARNEDPOINTS_NOTIFICATION`,
  SET_EARNEDPOINTS_NOTIFICATION: `${EARNEXTRAPOINTS_ACTION_PATTERN}SET_EARNEDPOINTS_NOTIFICATION`,
  SET_EARNEXTRAPOINTS_LIST: `${EARNEXTRAPOINTS_ACTION_PATTERN}SET_EARNEXTRAPOINTS_LIST`,
  SHOW_LOADER: `${EARNEXTRAPOINTS_ACTION_PATTERN}SHOW_LOADER`,
  HIDE_LOADER: `${EARNEXTRAPOINTS_ACTION_PATTERN}HIDE_LOADER`,
  GET_EARNEXTRAPOINTS_LIST_TTL: 0,
  MAX_TILE_COUNT: 8,
};

export default EARNEXTRAPOINTS_CONSTANTS;
