// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { PaymentTile } from '../PaymentTile.view.native';

describe('PaymentTile component', () => {
  it('should render correctly', () => {
    const component = shallow(<PaymentTile handleComponentChange={jest.fn()} />);
    expect(component).toMatchSnapshot();
  });
});
