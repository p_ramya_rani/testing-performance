// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import BodyCopy from '../../../../../../../common/atoms/BodyCopy';
import {
  RewardsOverviewContainer,
  MyRewardsWrapper,
  TextWrapper,
  LastTextWrapper,
  VerticalLine,
} from '../../styles/RewardsPointsTable.style';

/**
 * @function RewardsPointsTable The RewardsPointsTable component will provide rewards table listing on the account overview page
 */

export class RewardsPointsTable extends React.PureComponent {
  getPointsColor = isPlcc => (isPlcc ? 'blue.B100' : 'orange.800');

  render() {
    const { labels, pointsToNextReward, currentPoints, totalRewards, plccUser } = this.props;
    return (
      <RewardsOverviewContainer>
        <MyRewardsWrapper>
          <BodyCopy
            fontFamily="secondary"
            fontSize="fs20"
            fontWeight="black"
            color={this.getPointsColor(plccUser)}
            text={`${
              getLabelValue(labels, 'lbl_rewardPoints_currency')
                ? getLabelValue(labels, 'lbl_rewardPoints_currency')
                : ''
            }${Math.round(totalRewards || 0)}`}
          />
          <TextWrapper>
            <BodyCopy
              fontFamily="secondary"
              fontSize="fs13"
              fontWeight="regular"
              textAlign="center"
              color="black"
              text={getLabelValue(labels, 'lbl_rewardPoints_heading')}
            />
          </TextWrapper>
        </MyRewardsWrapper>
        <VerticalLine />
        <MyRewardsWrapper>
          <BodyCopy
            fontFamily="secondary"
            fontSize="fs20"
            fontWeight="black"
            color={this.getPointsColor(plccUser)}
            text={Math.round(currentPoints || 0)}
          />
          <TextWrapper>
            <BodyCopy
              fontFamily="secondary"
              fontSize="fs13"
              fontWeight="regular"
              textAlign="center"
              color="black"
              text={getLabelValue(labels, 'lbl_rewardPoints_currentPoints')}
            />
          </TextWrapper>
        </MyRewardsWrapper>
        <VerticalLine />
        <MyRewardsWrapper>
          <BodyCopy
            fontFamily="secondary"
            fontSize="fs20"
            fontWeight="black"
            color={this.getPointsColor(plccUser)}
            text={Math.round(pointsToNextReward || 100)}
          />
          <LastTextWrapper>
            <BodyCopy
              fontFamily="secondary"
              fontSize="fs13"
              fontWeight="regular"
              textAlign="center"
              color="black"
              text={getLabelValue(labels, 'lbl_rewardPoints_nextReward')}
            />
          </LastTextWrapper>
        </MyRewardsWrapper>
      </RewardsOverviewContainer>
    );
  }
}

RewardsPointsTable.propTypes = {
  plccUser: PropTypes.bool.isRequired,
  pointsToNextReward: PropTypes.number,
  currentPoints: PropTypes.number,
  totalRewards: PropTypes.number,
  labels: PropTypes.shape({}),
};

RewardsPointsTable.defaultProps = {
  pointsToNextReward: 100,
  currentPoints: 0,
  totalRewards: 0,
  labels: {
    lbl_rewardPoints_currency: '',
    lbl_rewardPoints_heading: '',
    lbl_rewardPoints_currentPoints: '',
    lbl_rewardPoints_nextReward_points: '',
  },
};

export default RewardsPointsTable;
