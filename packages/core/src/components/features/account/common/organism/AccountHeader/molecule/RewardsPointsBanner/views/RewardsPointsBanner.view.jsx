// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';

import ModuleX from '@tcp/core/src/components/common/molecules/ModuleX';

export const RewardsPointsBanner = ({ content, className }) => {
  return <ModuleX richTextList={[{ text: content }]} className={className} />;
};

RewardsPointsBanner.propTypes = {
  content: PropTypes.node,
  className: PropTypes.string,
};

RewardsPointsBanner.defaultProps = {
  content: '',
  className: '',
};

export default RewardsPointsBanner;
