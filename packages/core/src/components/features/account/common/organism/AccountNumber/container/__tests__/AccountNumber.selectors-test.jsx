// 9fbef606107a605d69c0edbcd8029e5d 
import getAccountOverviewLabels from '../AccountNumber.selectors';

describe('#AccountNumber Selectors', () => {
  it('#getLabels should return Labels', () => {
    const state = {
      Labels: {
        accountOverview: {},
      },
    };
    expect(getAccountOverviewLabels(state)).toMatchObject({});
  });
});
