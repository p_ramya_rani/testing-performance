// 9fbef606107a605d69c0edbcd8029e5d 
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import OrdersTileComponent from '../views';
import {
  getOrdersListState,
  getOrderListFetchingState,
  getShowOrderListWithItemDetails,
} from '../../../../Orders/container/Orders.selectors';
import { getOrdersList } from '../../../../Orders/container/Orders.actions';
import constants from '../../../../Orders/Orders.constants';
import { getSiteId } from '../../../../../../../utils';
import { getLabels } from '../../../../Account/container/Account.selectors';
import OrdersTileSkelton from '../skelton/OrdersTileSkelton.view';

class OrdersTile extends PureComponent {
  componentDidMount() {
    const { fetchOrders, showOrderListWithItemDetails } = this.props;
    let listPayload = getSiteId();
    if (showOrderListWithItemDetails) {
      listPayload = {
        reqParams: {
          pageNumber: 0,
          pageSize: constants.ORDER_SIZE,
        },
      };
    }
    fetchOrders(listPayload);
  }

  render() {
    const { labels, ordersListItems, navigation, handleComponentChange, isFetching } = this.props;

    if (isFetching) {
      return <OrdersTileSkelton />;
    }
    return (
      <OrdersTileComponent
        labels={labels}
        ordersList={ordersListItems}
        navigation={navigation}
        handleComponentChange={handleComponentChange}
      />
    );
  }
}

OrdersTile.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  fetchOrders: PropTypes.func,
  ordersListItems: PropTypes.shape({}).isRequired,
  navigation: PropTypes.shape({}).isRequired,
  handleComponentChange: PropTypes.func.isRequired,
  isFetching: PropTypes.bool,
  showOrderListWithItemDetails: PropTypes.bool,
};

OrdersTile.defaultProps = {
  fetchOrders: () => {},
  isFetching: false,
  showOrderListWithItemDetails: false,
};
export const mapStateToProps = state => ({
  labels: getLabels(state),
  ordersListItems: getOrdersListState(state),
  isFetching: getOrderListFetchingState(state),
  showOrderListWithItemDetails: getShowOrderListWithItemDetails(state),
});

export const mapDispatchToProps = dispatch => ({
  fetchOrders: payload => {
    dispatch(getOrdersList(payload));
  },
});

export { OrdersTile as OrdersTileVanilla };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OrdersTile);
