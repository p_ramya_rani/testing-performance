// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { RecentOrdersSkeletonVanilla } from '../RecentOrdersSkeleton.view';

describe('RecentOrdersSkeleton component', () => {
  it('should renders correctly', () => {
    const props = {
      className: 'sample-class',
    };
    const component = shallow(<RecentOrdersSkeletonVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
