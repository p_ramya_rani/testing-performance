// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import OrderStatusContainer from '../views/OrderStatusContainer.view.native';

const props = {
  labels: {},
  isUpcomingOrdersView: true,
  orderShipmentStatus: 'lbl_orders_statusNa',
  isEcomOrder: true,
  orderTrackingUrl:
    'https://childrensplace.narvar.com/childrensplace/tracking/ups?order_number=7000037172&order_date=2019-10-04T07:22:00-04:00&dzip=10036&locale=en_US&type=p',
  totalCount: 12,
  index: 2,
  isPartiallyCancelled: false,
  orderSummary: {
    shippingAddress: '123 main st, secaucus, NJ - 10307',
  },
  storeAddress: {
    addressLine1: 'Secaucus outlet',
  },
};

describe('OrdersList View component', () => {
  it('should renders correctly', () => {
    const component = shallow(<OrderStatusContainer {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders upcoming order correctly', () => {
    const newProps = {
      ...props,
      isUpcomingOrdersView: false,
    };
    const component = shallow(<OrderStatusContainer {...newProps} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders ECOM order correctly', () => {
    const newProps = {
      ...props,
      isEcomOrder: false,
    };
    const component = shallow(<OrderStatusContainer {...newProps} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders partially cancelled order correctly', () => {
    const newProps = {
      ...props,
      isPartiallyCancelled: true,
    };
    const component = shallow(<OrderStatusContainer {...newProps} />);
    expect(component).toMatchSnapshot();
  });

  it('should show estimated date', () => {
    const newProps = {
      ...props,
      isPartiallyCancelled: true,
      expectedDeliveryDate: 'Fri, Sep 3',
    };
    const component = shallow(<OrderStatusContainer {...newProps} />);
    expect(component).toMatchSnapshot();
  });
});
