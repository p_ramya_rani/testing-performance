// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Text } from 'react-native';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import Anchor from '@tcp/core/src/components/common/atoms/Anchor';
import { Image, BodyCopy } from '../../../../../../common/atoms';
import {
  getMessageBoldText,
  getMessageNormalText,
  getOrderTypeText,
} from './OrdersListItemHeader.utils';
import HelpWithOrderModal from '../../../../../../common/organisms/HelpWithOrder/HelpWithOrderModal/HelpWithOrderModal.view.native';
import {
  HeaderWrapper,
  HeaderTopSection,
  OrderTypeSection,
  NotificationSection,
  BottomSection,
  OrderTotal,
  ShippingWrapper,
  PriceWrapper,
  NotificationText,
  TextWrapper,
  OrderTypeText,
  StyledTrackAnchor,
  OrderMainView,
} from '../styles/OrdersListItemHeader.style.native';

const ecomIcon = require('../../../../../../../../../mobileapp/src/assets/images/shipping.png');
const pickupIcon = require('../../../../../../../../../mobileapp/src/assets/images/store.png');
const warningImg = require('../../../../../../../../../mobileapp/src/assets/images/circle-warning-fill.png');

const fontWeight = { fontWeight: '500' };

const OrdersListItemHeader = ({
  orderDate,
  orderNumber,
  isEcomOrder,
  showNotification,
  labels,
  navigation,
  expiredItemShipment,
  helpWithOrderLabels,
  cshReturnItemOlderDays,
  isCSHAppEnabled,
  setOrderResendModalOpen,
  orderDateWithoutFormatting,
  refundEligibleShipmentStatus,
  ...otherProps
}) => {
  const router = {
    query: {
      orderId: orderNumber,
    },
  };
  const messageBoldText = getMessageBoldText(labels, isEcomOrder);
  const messageNormalText = getMessageNormalText(labels, isEcomOrder);
  const orderTypeText = getOrderTypeText(labels, isEcomOrder, expiredItemShipment);
  const [isHelpWithOrderOpen, setHelpWithOrderOpen] = useState(false);

  const OrderDetailsClick = () =>
    navigation.navigate('OrderDetailPage', {
      title: `${getLabelValue(labels, 'lbl_orderDetail_heading', 'orders')} #${orderNumber}`,
      router,
    });

  const OrderHelpClick = () => setHelpWithOrderOpen(true);

  return (
    <HeaderWrapper>
      <HeaderTopSection>
        <ShippingWrapper>
          <Image width="27px" height="27px" source={isEcomOrder ? ecomIcon : pickupIcon} />
          <OrderTypeSection>
            <OrderTypeText
              fontWeight="extrabold"
              fontFamily="secondary"
              fontSize="fs16"
              color="black"
              text={orderTypeText}
            />
            <BodyCopy
              fontSize="fs16"
              fontWeight="bold"
              color="gray.800"
              fontFamily="secondary"
              text={`#${orderNumber}`}
            />
          </OrderTypeSection>
        </ShippingWrapper>
        <PriceWrapper>
          <BottomSection>
            <BodyCopy
              fontSize="fs12"
              fontWeight="bold"
              data-locator="order-date-value"
              text={orderDate}
              color="gray.900"
              fontFamily="secondary"
            />
          </BottomSection>
          <OrderMainView>
            <OrderTotal>
              <Anchor
                fontSizeVariation="medium"
                fontWeightVariation="active"
                noLink
                underline
                anchorVariation="custom"
                colorName="black"
                text={`${getLabelValue(labels, 'lbl_orders_view_details', 'orders')}`}
                onPress={OrderDetailsClick}
                fontFamily="secondary"
              />
            </OrderTotal>
            <StyledTrackAnchor
              anchorVariation="white"
              fontWeight="extrabold"
              fontFamily="secondary"
              fontSizeVariation="medium"
              onPress={OrderHelpClick}
              text="Order Help"
            />
          </OrderMainView>
          {isHelpWithOrderOpen && (
            <HelpWithOrderModal
              isCSHAppEnabled={isCSHAppEnabled}
              setHelpWithOrderOpen={setHelpWithOrderOpen}
              setOrderResendModalOpen={setOrderResendModalOpen}
              isHelpWithOrderOpen={isHelpWithOrderOpen}
              helpWithOrderLabels={helpWithOrderLabels}
              orderDate={orderDate}
              navigation={navigation}
              cshReturnItemOlderDays={cshReturnItemOlderDays}
              orderDateWithoutFormatting={orderDateWithoutFormatting}
              orderId={orderNumber}
              refundEligibleShipmentStatus={refundEligibleShipmentStatus}
              {...otherProps}
            />
          )}
        </PriceWrapper>
      </HeaderTopSection>
      {showNotification ? (
        <NotificationSection>
          <Image width="24px" height="21px" source={warningImg} />
          <NotificationText>
            <TextWrapper>
              {messageBoldText}
              <Text style={fontWeight}>{messageNormalText}</Text>
            </TextWrapper>
          </NotificationText>
        </NotificationSection>
      ) : null}
    </HeaderWrapper>
  );
};

OrdersListItemHeader.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  orderDate: PropTypes.string.isRequired,
  orderNumber: PropTypes.string.isRequired,
  isEcomOrder: PropTypes.bool.isRequired,
  showNotification: PropTypes.bool.isRequired,
  orderSummary: PropTypes.shape({}).isRequired,
  navigation: PropTypes.func.isRequired,
  expiredItemShipment: PropTypes.bool,
};

OrdersListItemHeader.defaultProps = {
  expiredItemShipment: false,
};

export default OrdersListItemHeader;
export { OrdersListItemHeader as OrdersListItemHeaderVanilla };
