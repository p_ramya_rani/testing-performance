// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { OrdersListItemvanilla } from '../views/OrdersListItem.view';

describe('OrdersListItem component', () => {
  it('should renders correctly', () => {
    const props = {
      labels: {},
      orderItem: {
        orderTotal: 10.5,
      },
    };
    const component = shallow(<OrdersListItemvanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
