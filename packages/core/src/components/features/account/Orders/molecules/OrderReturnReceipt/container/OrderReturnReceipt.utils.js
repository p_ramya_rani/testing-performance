// 9fbef606107a605d69c0edbcd8029e5d
import constants from '@tcp/core/src/components/features/account/OrderDetails/OrderDetails.constants';
import config from '@tcp/core/src/config/orderConfig';

const {
  STATUS_CONSTANTS: {
    ORDER_SHIPPED,
    ORDER_PARTIALLY_SHIPPED,
    ORDER_PICKED_UP,
    SUCCESSFULLY_PICKED_UP,
    ORDER_CANCELLED,
    ORDER_RECEIVED,
  },
} = constants;
/**
 * @description method to decide if items needs to be on shipment block
 * @param {object} orderGroup
 */
const showShipmentBlock = (orderDetailsData, orderGroup) => {
  const { orderStatus: orderTotalStatus } = orderDetailsData;
  if (orderTotalStatus === ORDER_PICKED_UP) {
    return true;
  }
  const { orderStatus, trackingNumber } = orderGroup;

  // For Boss order picked up.
  if (orderTotalStatus === SUCCESSFULLY_PICKED_UP && orderStatus) {
    return true;
  }

  if (orderStatus || trackingNumber) {
    return (
      (orderStatus === ORDER_SHIPPED ||
        orderStatus === ORDER_PARTIALLY_SHIPPED ||
        orderStatus === ORDER_PICKED_UP) &&
      (orderStatus === ORDER_PICKED_UP || trackingNumber)
    );
  }
  return false;
};

const isValidReceiptItem = (orderDetailsData, orderGroup) => {
  const { orderType } = orderDetailsData;
  const { orderStatus, trackingNumber } = orderGroup;
  const {
    ORDER_ITEM_TYPE: { ECOM },
  } = config;
  if (
    (orderType === ECOM &&
      (orderStatus === ORDER_SHIPPED || orderStatus === ORDER_CANCELLED) &&
      !trackingNumber) ||
    orderStatus === ORDER_RECEIVED
  ) {
    return false;
  }
  return true;
};

const isOrderPickedUp = (orderStatus) =>
  orderStatus === ORDER_PICKED_UP || orderStatus === SUCCESSFULLY_PICKED_UP;

export { showShipmentBlock, isOrderPickedUp, isValidReceiptItem };
