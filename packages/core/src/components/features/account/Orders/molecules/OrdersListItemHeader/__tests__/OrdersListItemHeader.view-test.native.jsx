// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import OrdersListItemHeader from '../views/OrdersListItemHeader.view.native';

const props = {
  labels: {},
  orderDate: 'Jan 14, 2021',
  orderNumber: '#34567890',
  messageBoldText: 'Sample bold text',
  messageNormalText: 'Sample normal text',
  orderTotal: 210,
  currencySymbol: '$',
  isEcomOrder: true,
  showNotification: true,
  openStoreDirections: () => {},
  orderSummary: {
    purchasedItems: [],
    canceledItems: [{ items: [] }],
    shippingAddress: {},
  },
  storeAddress: '123 main St, Secaucus, Nj, 07094',
  navigation: () => {},
};

describe('OrdersListItem component', () => {
  it('should renders correctly for Ecom', () => {
    const component = shallow(<OrdersListItemHeader {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('should renders correctly for non Ecom', () => {
    const newProps = {
      ...props,
      isEcomOrder: false,
    };
    const component = shallow(<OrdersListItemHeader {...newProps} />);
    expect(component).toMatchSnapshot();
  });
  it('should renders correctly for non Ecom', () => {
    const newProps = {
      ...props,
      isEcomOrder: false,
      showNotification: false,
    };
    const component = shallow(<OrdersListItemHeader {...newProps} />);
    expect(component).toMatchSnapshot();
  });
});
