// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
  .order-help-cta {
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    padding: ${(props) => props.theme.spacing.ELEM_SPACING.XS_6}
      ${(props) => props.theme.spacing.ELEM_SPACING.XS_6};
    border-radius: 4px;
    background-color: ${(props) => props.theme.colors.PRIMARY.DARK};
    text-transform: uppercase;
    color: ${(props) => props.theme.colors.WHITE};
    cursor: pointer;
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy2}px;
    font-weight: ${(props) => props.theme.fonts.fontWeight.bold};
    text-align: center;
    min-height: 0;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      padding: ${(props) => props.theme.spacing.ELEM_SPACING.XS}
        ${(props) => props.theme.spacing.ELEM_SPACING.MED_1};
      margin-top: 0;
      margin-left: auto;
    }
  }
  .bottom-right-section {
    display: flex;
  }
  .top-section {
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    background: ${(props) =>
      props.theme.isGymboree
        ? props.theme.colorPalette.orange[100]
        : props.theme.colors.PRIMARY.COLOR2};
    display: flex;
    padding: ${(props) => props.theme.spacing.ELEM_SPACING.SM}
      ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  }
  .order-type-logo {
    width: 27px;
  }
  .order-type-information {
    flex: 1;
    margin: 0 ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy6}px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy4}px;
    }
  }
  .order-type-text {
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    font-weight: ${(props) => props.theme.typography.fontWeights.extrabold};
    text-transform: uppercase;
  }
  .order-number {
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy4}px;
    font-weight: ${(props) => props.theme.typography.fontWeights.bold};
    color: ${(props) => props.theme.colors.TEXT.DARKERGRAY};
  }
  .right-section {
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    display: flex;
    flex-direction: column;
  }
  .order-date {
    text-align: right;
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    font-weight: ${(props) => props.theme.typography.fontWeights.extrabold};
    flex: 1;
  }
  .view-details {
    font-weight: ${(props) => props.theme.typography.fontWeights.extrabold};
    text-decoration: underline;
    text-align: right;
    margin-top: 9px;
    margin-right: 5px;
  }
  .order-total {
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy8}px;
    font-weight: ${(props) => props.theme.typography.fontWeights.bold};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy5}px;
    }
  }
  .notification-section {
    display: flex;
  }
  .notification {
    flex: 1;
    margin-bottom: 0;
  }
`;

export default styles;
