// 9fbef606107a605d69c0edbcd8029e5d
import {
  getLabelValue,
  getFormatedOrderDate,
  getFormatedOrderTime,
  convertToISOString,
} from '@tcp/core/src/utils/utils';
import { isAndroid } from '@tcp/core/src/utils/index.native';
import { getPaymentDetails } from '@tcp/core/src/utils';
import {
  showShipmentBlock,
  isOrderPickedUp,
  isValidReceiptItem,
} from '../container/OrderReturnReceipt.utils';
import css from '../styles/OrderReturnReceipt.style.native';

const getAllItems = (purchasedItems, purchasedItemsAll) => {
  return purchasedItemsAll && purchasedItemsAll.length ? purchasedItemsAll : purchasedItems;
};

export default function OrderReturnReceipt({
  barcode,
  orderNumber,
  returnDays,
  ordersLabels,
  orderDate,
  orderDetailsData,
  userAccountId,
}) {
  const returnReceiptHeading = getLabelValue(ordersLabels, 'lbl_orders_return_receipt');
  const upcText = getLabelValue(ordersLabels, 'lbl_orderDetails_upc');
  const variantText = getLabelValue(ordersLabels, 'lbl_orderDetails_itemnumber');

  const { purchasedItems, purchasedItemsAll } = orderDetailsData;
  const allItems = getAllItems(purchasedItems, purchasedItemsAll);
  const orderPlacedDate = new Date(convertToISOString(orderDate));
  const orderDatePlaced = new Date(orderPlacedDate);
  orderDatePlaced.setDate(orderDatePlaced.getDate() + returnDays);

  const renderReturnByDate = () => {
    return `
      <div class="order-return-header"">
          <p class="message-header-text">${getLabelValue(
            ordersLabels,
            'lbl_orderDetails_return_by'
          )}:</p>
        <p class="product-detail">${getFormatedOrderDate(orderDatePlaced)}
        </p>
      </div>
      `;
  };

  const renderReturnMessage = () => {
    return `
    <div class="order-return-message-header">
    <div>
          <p class="message-header-text">
          ${getLabelValue(ordersLabels, 'lbl_order_receipt_message_heading')}
          </p>
        </div>
    </div>

  <div class="order-return-message-info">
  <div>
        <p class="product-detail">${getLabelValue(
          ordersLabels,
          'lbl_order_receipt_message_information'
        )}</p>
    </div>
  </div>
    `;
  };

  const renderReturnMember = () => {
    return userAccountId
      ? `
    <div class="order-return-header"">
      <p class="message-header-text">${getLabelValue(ordersLabels, 'lbl_order_return_member')}:</p>
      <p class="product-detail">${userAccountId}</p>
  </div>
  `
      : '';
  };

  const renderHeaderDetails = () => {
    const orderDateParsed = orderPlacedDate;
    const paymentDetails = getPaymentDetails(orderDetailsData);
    const paymentText = paymentDetails ? paymentDetails.join(', ') : '';
    const orderTime = !isAndroid()
      ? `<span> ${getLabelValue(
          ordersLabels,
          'lbl_orderDetails_at'
        )} </span><span>${getFormatedOrderTime(orderDateParsed)}</span>`
      : '';

    return `
  <div class="order-return-header-container">

    <div class="order-return-header">
          <p class="message-header-text">${getLabelValue(
            ordersLabels,
            'lbl_orderDetails_orderNumber'
          )}:</p>
        <p class="product-detail">${orderNumber}</p>
    </div>

    <div class="order-return-header">
        <p class="message-header-text">${getLabelValue(ordersLabels, 'lbl_orders_orderDate')}:</p>
        <p class="product-detail">${getFormatedOrderDate(orderDateParsed)}${orderTime}</p>
    </div>

  <div class="order-return-header"">
        <p class="message-header-text">${getLabelValue(
          ordersLabels,
          'lbl_order_return_payment'
        )}:</p>
        <p class="product-detail">${paymentText}</p>
  </div>

  ${renderReturnMember()}

    ${returnDays && returnDays > 0 ? renderReturnByDate() : renderReturnMessage()}

  </div>
  `;
  };

  const getProductInfo = (item, isShipment) => {
    const {
      productInfo: { name, variantNo, upc },
      itemInfo: { offerPrice, quantity, quantityShipped },
    } = item;
    const qty = isShipment ? quantityShipped : quantity;
    const itemNumber = variantNo || upc;
    const itemNumberText = variantNo ? variantText : upcText;
    return `
          <div class="product-info-container">
            <div class="product-info">
                <span class="product-name">${name}</span>
              </div>
            <div class="product-info">
              <div class="product-info-item">
                <span class="product-detail">${itemNumberText} ${itemNumber}</span>
              </div>
              <div class="price-info">
                <span class="product-detail">${qty && qty > 1 ? qty : ''}${
      qty && qty > 1 ? ' X ' : ''
    }</span>
                <span class="product-detail">$${offerPrice}</span>
              </div>
            </div>
          </div>`;
  };

  const renderShipmentOrderInfo = (orderGroup, index) => {
    const { items } = orderGroup;
    const { orderStatus } = orderDetailsData;
    const shipmentText = `${getLabelValue(ordersLabels, 'lbl_orderDetails_shipment')} ${index + 1}`;
    return `
    <div class="order-shipment-wrapper">
      <div class="shipment-index-container">
        <p class="product-detail">${
          isOrderPickedUp(orderStatus)
            ? getLabelValue(ordersLabels, 'lbl_orders_statusItemsPickedUp')
            : shipmentText
        }
        </p>
      </div>
      <div class="barcode-container">
       <img style="height:47px" src="data:image/png;base64, ${barcode}" alt="TCP | GYMBOREE" />
      </div>
      ${items && items.map((item) => getProductInfo(item, true)).join('')}
  </div>`;
  };

  const renderOrderDetails = (orderGroup, index) => {
    const { items, orderStatus } = orderGroup;
    if (showShipmentBlock(orderDetailsData, orderGroup)) {
      return renderShipmentOrderInfo(orderGroup, index);
    }
    const { isBossOrder, isBopisOrder } = orderDetailsData;
    if (isBossOrder || isBopisOrder) {
      return `
        <div className="order-process-wrapper">
          ${
            items && items.length && orderStatus
              ? items.map((item) => this.getProductInfo(item, false)).join('')
              : ''
          }
        </div>`;
    }
    if (isValidReceiptItem(orderDetailsData, orderGroup)) {
      return `<div class="order-list-wrapper">
        ${items && items.map((item) => getProductInfo(item, false)).join('')}
        </div>`;
    }
    return `<span></span>`;
  };

  const render = () => `
    <div class="order-return-container">
      <div class="return-brand">
        <img class="brand-icon" src="https://assets.theplace.com/image/upload/w_185,h_84,f_auto,q_auto,dpr_2.0/v1593674792/TCP-GYM_Logos_PMS-Horizontal_BLK.jpg" alt="Brand Logo" />
      </div>

      <p class="order-return-title">${returnReceiptHeading}</p>
        ${renderHeaderDetails()}
        ${
          allItems &&
          allItems.length > 0 &&
          allItems
            .map((items, index) => {
              return renderOrderDetails(items, index);
            })
            .join('')
        }
        <div class="order-return-address-container">
        <div class="order-return-address-detail">
        <div class="order-return-address-info-header">
          <p class="order-return-address-detail">${getLabelValue(
            ordersLabels,
            'lbl_return_address_receipt_us_heading'
          )}
          </p>
        </div>
        <div class="order-return-address-info">
        <p class="order-return-address-detail">${getLabelValue(
          ordersLabels,
          'lbl_return_address_receipt_us'
        )}
        </p>
      </div>
      </div>
      <div class="order-return-address-detail">
      <div class="order-return-address-info-header">
      <p class="order-return-address-detail">${getLabelValue(
        ordersLabels,
        'lbl_return_address_receipt_ca_heading'
      )}
      </p>
    </div>
    <div class="order-return-address-info">
    <p class="order-return-address-detail">${getLabelValue(
      ordersLabels,
      'lbl_return_address_receipt_ca'
    )}
    </p>
  </div>
       </div>
    </div>
    </div>
    
    `;

  const htmlToPrint = `
      ${render()}
      <style type="text/css">${css}</style>
  `;

  return `<div>
      ${htmlToPrint}
    </div>`;
}
