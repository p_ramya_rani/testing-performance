// 9fbef606107a605d69c0edbcd8029e5d 
import { getLabelValue } from '@tcp/core/src/utils/utils';

const messageLabel = isEcomOrder =>
  isEcomOrder
    ? 'lbl_orders_notification_cancelled_items'
    : 'lbl_orders_notification_ordernotpicked_1';

export const getMessageBoldText = (labels, isEcomOrder) =>
  getLabelValue(labels, messageLabel(isEcomOrder), 'orders');

export const getMessageNormalText = (labels, isEcomOrder) => {
  return isEcomOrder
    ? ''
    : getLabelValue(labels, 'lbl_orders_notification_ordernotpicked_2', 'orders');
};

export const getOrderTypeText = (labels, isEcomOrder, expiredItemShipment) => {
  let orderTypeText = isEcomOrder
    ? getLabelValue(labels, 'lbl_orderDetails_shipping', 'orders')
    : getLabelValue(labels, 'lbl_orders_heading_store_pickup', 'orders');
  if (!isEcomOrder && expiredItemShipment) {
    orderTypeText = getLabelValue(labels, 'lbl_orders_heading_store_pickup_expired', 'orders');
  }
  return orderTypeText;
};
