// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';

import {
  TrackingCTA,
  GetTooltipContent,
  VanillaOrderStatusContainer,
} from '../views/OrderStatusContainer.view';

const props = {
  labels: {},
  className: '',
  ordersListItems: [],
  isEcomOrder: true,
  storeAddress: {},
  orderSummary: {
    shippingAddress: {
      addressLine1: '',
      city: '',
      state: '',
      zipCode: '',
    },
  },
};

describe('orderStatus container', () => {
  it('should render order-status-logo', () => {
    const component = shallow(<VanillaOrderStatusContainer {...props} />);
    expect(component.find('.order-status-logo')).toHaveLength(1);
  });

  it('should render order shipment counts', () => {
    const component = shallow(<VanillaOrderStatusContainer {...props} />);
    expect(component.find('.shipment-count')).toHaveLength(1);
  });

  it('should not render order shipment edd', () => {
    const component = shallow(<VanillaOrderStatusContainer {...props} isItemShipped />);
    expect(component.find('.shipment-edd')).toHaveLength(0);
  });
});

describe('TrackingCTA', () => {
  it('should render track order cta ', () => {
    const component = shallow(<TrackingCTA {...props} isEcomOrder />);
    expect(component.find('.track-order-cta')).toHaveLength(1);
  });

  it('should not render tracking number hyperlink without orderTrackingNumber', () => {
    const component = shallow(<TrackingCTA {...props} isEcomOrder />);
    expect(component.find('.order-trackingNumber')).toHaveLength(0);
  });

  it('should render tracking number hyperlink', () => {
    const component = shallow(<TrackingCTA {...props} isEcomOrder orderTrackingNumber="48927" />);
    expect(component.find('.order-trackingNumber')).toHaveLength(1);
  });

  it('should direction cta', () => {
    const component = shallow(<TrackingCTA {...props} isEcomOrder={false} isMobile />);
    expect(component.find('.directions-cta')).toHaveLength(1);
  });
});

describe('GetTooltipContent component', () => {
  it('should render correctly', () => {
    const props1 = {
      labels: {},
      storeAddressLine: '',
      city: '',
      zipCode: '',
      storeName: '',
      state: '',
      phone: '',
    };
    const component = shallow(<GetTooltipContent {...props1} />);
    expect(component).toMatchSnapshot();
  });
});
