// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

export default css`
  .order-return-wrapper {
    margin: 0 auto;
    text-align: center;
    margin-top: ${(props) => props.theme.spacing.LAYOUT_SPACING.XS};
    position: relative;
  }

  .order-return-container {
    border: 1px solid ${(props) => props.theme.colorPalette.gray[600]};
    margin: 0 auto;
    text-align: center;
    width: 442px;
  }

  .return-brand {
    margin: 0 auto;
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    height: 84px;
    width: 185px;
  }

  .brand-icon {
    margin: 10px;
    height: 84px;
    width: 185px;
  }

  .order-return-title {
    margin: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    margin-top: 0;
    text-align: center;
  }

  .product-name {
    text-align: left;
  }

  .product-detail {
    display: inline;
    text-align: left;
    margin-right: 10px;
  }

  .orderdetails-barcode {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  }

  .product-info-container {
    display: block;
    width: 100%;
    margin: 11px 0;
    break-before: auto;
  }

  .product-info {
    text-align: left;
    margin: 1px 0;
  }

  .order-shipment-wrapper {
    margin: 20px;
    border: 1px solid ${(props) => props.theme.colorPalette.gray[600]};
    text-align: center;
    page-break-after: auto;
  }

  .order-shipment-container {
    width: 88%;
    margin: 0 auto;
  }

  .order-return-header-container {
    text-align: left;
  }
  .order-return-address-container {
    text-align: left;
    margin-bottom: 60px;
  }
  .price-info {
    text-align: right;
  }

  .order-return-header {
    width: 85%;
    margin: 0 auto;
  }

  .order-process-wrapper {
    width: 90%;
    margin: 0 auto;
    margin-bottom: 40px;
  }

  .shipment-index-container {
    text-align: left;
    margin-top: 10px;
    margin-left: 8px;
  }

  .order-return-message-header {
    width: 85%;
    margin: 0 auto;
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  }

  .order-return-message-info {
    width: 85%;
    margin: 0 auto;
    margin-top: 20px;
  }

  .add-bottom-margin {
    margin-bottom: 70px;
  }

  .add-top-margin {
    page-break-before: auto;
  }

  .add-break-before {
    page-break-before: always;
  }

  .add-bottom-padding {
    padding-bottom: 60px;
  }

  .paging-line {
    width: 80%;
  }
  .order-return-address-header {
    width: 91%;
    margin: 0 auto;
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  }
  .order-return-address-info {
    width: 91%;
    margin: 0 auto;
  }

  @media print {
    .page-footer-index {
      position: fixed;
      bottom: 5px;
      right: 10px;
      padding: 6px;
    }

    @page {
      size: auto;
      margin: 12mm 0;
    }
  }

  .underline {
    text-decoration: underline;
  }
`;
