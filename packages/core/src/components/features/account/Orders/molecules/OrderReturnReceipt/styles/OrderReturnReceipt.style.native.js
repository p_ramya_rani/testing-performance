// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

export default css`
  @page {
    margin: 10px;
  }

  .order-return-wrapper {
    margin: 0 auto;
    text-align: center;
    margin-top: 12px;
    position: relative;
  }

  .order-return-container {
    border: 1px solid black;
    text-align: center;
    margin: 0 auto;
    width: 435px;
    font-family: 'Nunito';
    font-style: normal;
    font-weight: 400;
  }

  .return-brand {
    margin-top: 8px;
    text-align: center;
  }

  .brand-icon {
    margin: 10px;
    height: 84px;
    width: 185px;
  }

  .order-return-title {
    font-size: 16px;
    font-weight: 800;
    line-height: normal;
    letter-spacing: normal;
    color: rgb(26, 26, 26);
    text-align: center;
    margin: 0px 12px 12px;
  }

  .product-name {
    font-weight: 900;
    font-size: 16px;
    display: inline;
    text-align: left;
  }

  .product-detail {
    display: inline;
    text-align: left;
    margin-right: 2px;
    font-weight: 400;
  }

  .product-info-item {
    display: inline-block;
    margin-left: 0%;
    margin-right: 5%;
    width: 60%;
  }

  .orderdetails-barcode {
    margin-top: 12px;
  }

  .product-info-container {
    width: 100%;
    margin: 8px 10px;
    display: block;
    break-before: auto;
  }

  .product-info {
    text-align: left;
    margin-top: 0;
  }

  .order-shipment-wrapper {
    margin: 10px 15px;
    border: 1px solid gray;
    text-align: center;
    page-break-before: auto;
  }
  .order-return-address-container {
    margin: 10px 15px;
    text-align: center;
  }
  .order-return-address-info {
    text-align: left;
    margin-top: 5px;
    margin-left: 8px;
    font-weight: 400;
  }
  .order-return-address-info-header {
    margin-top: 3px;
    font-size: 16px;
    font-weight: 800;
    text-align: left;
    margin-left: 8px;
  }
  .order-return-address-detail {
    display: inline;
    text-align: left;
    margin-right: 2px;
    margin-top: 10px;
  }

  .order-shipment-container {
    width: 88%;
    margin: 0 auto;
  }

  .order-return-header-container {
    text-align: left;
  }

  .price-info {
    text-align: right;
    display: inline-block;
    margin-left: 0%;
    width: 30%;
    margin-right: 0px;
  }

  .order-return-header {
    width: 90%;
    margin: 0 auto;
  }

  .order-list-wrapper {
    margin: 0 20px;
    margin-bottom: 30px;
  }

  .shipment-index-container {
    text-align: left;
    margin-top: 10px;
    margin-left: 8px;
  }

  .order-return-message-header {
    width: 85%;
    margin: 0 auto;
    margin-top: 12px;
  }

  .message-header-text {
    font-weight: 900;
    font-size: 14px;
    display: inline;
    text-align: left;
    margin-right: 5px;
  }

  .order-return-message-info {
    width: 85%;
    margin: 0 auto;
    margin-top: 20px;
  }

  .page-footer-index {
    position: absolute;
    bottom: 10px;
    padding: 12px;
    left: 28%;
    width: 320px;
    border-top: 1.5px solid gray;
  }

  .add-bottom-margin {
    margin-bottom: 70px;
  }

  .add-top-margin {
    margin-top: 100px;
  }

  .paging-line {
    width: 80%;
  }

  .order-info-left {
    text-align: left;
    display: inline-block;
    margin-right: 2%;
    width: 35%;
  }

  .barcode-container {
    width: 200px;
    height: 47px;
    margin: 0 auto;
  }

  .order-info-right {
    text-align: left;
    display: inline-block;
    width: 55%;
    margin-right: 0px;
  }
`;
