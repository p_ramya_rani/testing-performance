// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { parseStoreHours, mapHandler } from '@tcp/core/src/utils';
import { parseDate } from '@tcp/core/src/utils/parseDate';
import toTimeString from '@tcp/core/src/utils/formatTime';
import { getLabelValue, capitalizeWords } from '@tcp/core/src/utils/utils';
import { BodyCopy } from '../../../../../../common/atoms';
import {
  StorePickupWrapper,
  StoreSection,
  StoreButtonText,
  StoreWrapper,
  StyledAnchor,
  PickupText,
  BodySection,
  StoreTimeWrapper,
} from '../styles/OrderListStorePickup.style.native';

const getStoreTime = time => {
  const timeString = toTimeString(parseDate(time)) || '';
  return timeString.toUpperCase();
};

export const openStoreDirections = (e, orderSummary) => {
  e.preventDefault();
  const { shippingAddress } = orderSummary;
  const store = {
    basicInfo: { address: shippingAddress },
  };
  mapHandler(store);
};

export const displayStoreDetails = (storeAddress, labels) => {
  const { addressLine1 = '', addressLine2 = '', city, zipCode, state, storeHours } = storeAddress;
  const addressArr = addressLine1.split('|');
  const addressLineTwo = addressArr.length > 1 && addressArr[1];
  const storeAddressLine = `${addressLineTwo || ''} ${addressLine2}`;
  const addressLine = capitalizeWords(storeAddressLine);
  const cityName = capitalizeWords(city);
  let today;

  if (storeHours) {
    const hours = parseStoreHours(storeHours);
    const now = new Date();
    today = hours.find(day => {
      const openingHour = parseDate(day.openIntervals[0].fromHour);
      return now.getDay() === openingHour.getDay();
    });
  }
  return (
    <BodySection>
      <BodyCopy
        fontFamily="secondary"
        color="text.secondary"
        fontSize="fs12"
        text={`${addressLine.trim()},`}
      />
      <BodyCopy
        fontFamily="secondary"
        color="text.secondary"
        fontSize="fs12"
        text={`${cityName}, ${state} ${zipCode}`}
      />
      {today && (
        <StoreTimeWrapper>
          <BodyCopy
            fontFamily="secondary"
            color="text.primary"
            fontSize="fs12"
            text={`${getLabelValue(labels, 'lbl_orders_openToday', 'orders')}: `}
          />
          <BodyCopy
            fontFamily="secondary"
            color="text.primary"
            fontSize="fs12"
            fontWeight="bold"
            text={`${getStoreTime(today.openIntervals[0].fromHour)} - ${getStoreTime(
              today.openIntervals[0].toHour
            )}`}
          />
        </StoreTimeWrapper>
      )}
    </BodySection>
  );
};

const OrderListStorePickup = props => {
  const { orderSummary, labels, isCancelledItems, storeAddress, showStore } = props;

  const { addressLine1 = '' } = storeAddress;
  const storeName = addressLine1.split('|')[0];

  return (
    <StorePickupWrapper>
      <StoreSection>
        <StoreWrapper>
          {!showStore && (
            <PickupText>
              <StoreButtonText
                onPress={e => openStoreDirections(e, orderSummary)}
                fontSize="fs14"
                fontFamily="secondary"
                text={storeName}
              />
            </PickupText>
          )}
          {showStore && displayStoreDetails(storeAddress, labels)}
        </StoreWrapper>
        {!isCancelledItems && showStore && (
          <StyledAnchor
            anchorVariation="white"
            noLink
            to="/#"
            onPress={e => openStoreDirections(e, orderSummary)}
            text={getLabelValue(labels, 'lbl_orderDetails_direction', 'orders')}
          />
        )}
      </StoreSection>
    </StorePickupWrapper>
  );
};

OrderListStorePickup.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  orderSummary: PropTypes.shape({}).isRequired,
  storeAddress: PropTypes.shape({}).isRequired,
  isCancelledItems: PropTypes.bool,
  showStore: PropTypes.bool,
};

OrderListStorePickup.defaultProps = {
  isCancelledItems: false,
  showStore: false,
};

export default OrderListStorePickup;
