// 9fbef606107a605d69c0edbcd8029e5d 
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { getIconPath } from '@tcp/core/src/utils/utils';
import styles from '../Notification.style';
import { BodyCopy } from '../../../../../../common/atoms';

/**
 *
 * @param {String} className -  classname for the parent container
 * @param {String} status - to show error or success message
 * @param {String} message - error message
 * @param {node} children - notification child nodes.
 */
const Notification = ({
  className,
  status,
  alt,
  messageBoldText,
  messageNormalText,
  children,
  scrollIntoView,
  isErrorTriangleIcon,
}) => {
  const successIcon = getIconPath('circle-check-fill');
  const errorIcon = isErrorTriangleIcon
    ? getIconPath('circle-warning-fill')
    : getIconPath('circle-error-fill');
  const infoIcon = getIconPath('circle-info-fill');
  const warningIcon = getIconPath('circle-warning-fill');

  let imageAlt = '';
  let imageSrc = '';
  if (status === 'success') {
    imageSrc = successIcon;
    imageAlt = alt || 'success icon';
  }
  if (status === 'error') {
    imageSrc = errorIcon;
    imageAlt = alt || 'error icon';
  }
  if (status === 'info') {
    imageSrc = infoIcon;
    imageAlt = alt || 'info icon';
  }
  if (status === 'warning') {
    imageSrc = warningIcon;
    imageAlt = alt || 'warning icon';
  }

  // For window scroll up to top once component gets loaded.
  useEffect(() => {
    if (scrollIntoView) {
      window.scrollTo({
        top: 0,
        behavior: 'smooth',
      });
    }
  }, []);

  return (
    <div
      className={`${className} elem-pt-SM elem-pr-LRG elem-pb-SM elem-pl-LRG elem-mb-LRG`}
      role="alert"
    >
      <img alt={imageAlt} src={imageSrc} className="notification__image elem-mr-MED" />
      <BodyCopy fontFamily="secondary">
        <b>{messageBoldText}</b>
        {messageNormalText}
        {children}
      </BodyCopy>
    </div>
  );
};

Notification.propTypes = {
  className: PropTypes.string,
  status: PropTypes.string.isRequired,
  messageBoldText: PropTypes.string.isRequired,
  messageNormalText: PropTypes.string.isRequired,
  alt: PropTypes.string,
  children: PropTypes.node,
  scrollIntoView: PropTypes.bool,
  isErrorTriangleIcon: PropTypes.bool,
};

Notification.defaultProps = {
  className: '',
  children: null,
  alt: '',
  scrollIntoView: false,
  isErrorTriangleIcon: false,
};

export default withStyles(Notification, styles);
export { Notification as NotificationVanilla };
