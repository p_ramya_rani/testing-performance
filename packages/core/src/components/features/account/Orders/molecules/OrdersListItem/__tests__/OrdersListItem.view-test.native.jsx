// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { OrdersListItemVanilla } from '../views/OrdersListItem.view.native';

describe('OrdersListItem component', () => {
  it('should renders correctly', () => {
    const props = {
      labels: {},
      orderItem: {
        orderTotal: 10.5,
      },
    };
    const component = shallow(<OrdersListItemVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders correctly', () => {
    const props = {
      labels: {},
      orderItem: {
        currencySymbol: '$',
        isBOSSOrder: false,
        isCanadaOrder: false,
        isEcomOrder: true,
        orderDate: 'Oct 4, 2019',
        orderNumber: '7000037172',
        orderStatus: 'lbl_orders_statusNa',
        orderTotal: 10.5,
        orderTracking: 'N/A',
      },
    };
    const component = shallow(<OrdersListItemVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
