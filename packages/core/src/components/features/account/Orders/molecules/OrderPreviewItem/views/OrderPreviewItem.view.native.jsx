// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { BodyCopy, Image } from '@tcp/core/src/components/common/atoms';

import { getLabelValue } from '@tcp/core/src/utils/utils';

import {
  ImageStyle,
  ImageBrandStyle,
  OrderItemContainer,
  OrderItemImage,
  OrderItemContent,
  ItemContentWrapper,
} from '../styles/OrderPreviewItem.style.native';

const gymboreeImage = require('../../../../../../../../../mobileapp/src/assets/images/gymboree-logo.png');
const tcpImage = require('../../../../../../../../../mobileapp/src/assets/images/tcp-logo.png');

/**
 * This function component use for return the OrderItems
 * can be passed in the component.
 * @param otherProps - otherProps object used pass params to other component
 */

/**
 * This function component use for return the OrderItems
 * can be passed in the component.
 * @param otherProps - otherProps object used pass params to other component
 */

const getImagePath = imagePath =>
  imagePath.indexOf('https:') > -1 ? imagePath : `https:${imagePath}`;

const OrderPreviewItem = ({ className, ...otherProps }) => {
  /**
   * @function return  Used to render the JSX of the component
   * @param    {[Void]} function does not accept anything.
   * @return   {[Object]} JSX of the component
   */

  const {
    item: {
      productInfo: { name, imagePath, variantNo },
      itemInfo: { itemBrand, quantity, quantityCanceled },
    },
    labels,
    isCanceledList,
  } = otherProps;

  const { item } = otherProps;

  return (
    <>
      <OrderItemContainer>
        <OrderItemImage>
          {imagePath && (
            <ImageStyle>
              <Image url={getImagePath(imagePath)} height={100} width={100} isFastImage />
            </ImageStyle>
          )}
          {itemBrand === 'TCP' && <ImageBrandStyle source={tcpImage} />}
          {itemBrand === 'GYM' && <ImageBrandStyle source={gymboreeImage} />}
        </OrderItemImage>
        <OrderItemContent spacingStyles="padding-right-SM padding-left-SM">
          <BodyCopy
            fontSize="fs14"
            fontWeight="semibold"
            textAlign="left"
            text={name}
            fontFamily="secondary"
          />

          {variantNo && (
            <ItemContentWrapper spacingStyles="margin-top-MED">
              <BodyCopy
                fontSize="fs14"
                fontFamily="secondary"
                fontWeight="semibold"
                text={getLabelValue(labels, 'lbl_orderDetails_itemnumber', 'orders')}
              />

              <BodyCopy fontSize="fs14" fontFamily="secondary" text={variantNo} />
            </ItemContentWrapper>
          )}

          <ItemContentWrapper spacingStyles="margin-top-MED">
            <BodyCopy
              fontSize="fs14"
              fontFamily="secondary"
              fontWeight="semibold"
              text={getLabelValue(labels, 'lbl_orderDetails_quantity', 'orders')}
            />

            <BodyCopy
              fontSize="fs14"
              fontFamily="secondary"
              text={isCanceledList ? quantityCanceled : quantity}
            />
          </ItemContentWrapper>
          {item && item.trackingInfo && item.trackingInfo.length > 0 && (
            <ItemContentWrapper spacingStyles="margin-top-MED">
              <BodyCopy
                fontSize="fs14"
                fontFamily="secondary"
                fontWeight="semibold"
                text={`${getLabelValue(labels, 'lbl_orders_orderStatus', 'orders')}: `}
              />

              <BodyCopy fontSize="fs14" fontFamily="secondary" text={item.trackingInfo[0].status} />
            </ItemContentWrapper>
          )}
        </OrderItemContent>
      </OrderItemContainer>
    </>
  );
};
OrderPreviewItem.propTypes = {
  className: PropTypes.string,
  currencySymbol: PropTypes.string.isRequired,
  orderGroup: PropTypes.shape({}).isRequired,
};

OrderPreviewItem.defaultProps = {
  className: '',
};

export default OrderPreviewItem;
