// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components';
import { BodyCopy, Anchor } from '../../../../../../common/atoms';

const StorePickupWrapper = styled.View`
  width: 100%;
`;

const StoreSection = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin: ${props => props.theme.spacing.ELEM_SPACING.LRG}
    ${props => props.theme.spacing.ELEM_SPACING.MED} 0;
`;

const StoreButtonText = styled(BodyCopy)`
  text-decoration: underline;
  color: ${props => props.theme.colorPalette.blue[800]};
  margin: 0 5px;
  text-transform: capitalize;
  font-size: ${props => props.theme.fonts.fontSize.body.bodytext.copy3}px;
  font-weight: 700;
`;

const StoreTimeWrapper = styled.View`
  align-items: center;
  flex-direction: row;
`;

const TooltipWrapper = styled.View`
  justify-content: center;
`;

const ToolTipIcon = styled.TouchableOpacity`
  margin-left: ${props => props.theme.spacing.ELEM_SPACING.XS};
`;

const StoreWrapper = styled.View`
  flex-direction: row;
  align-items: center;
  max-width: 60%;
`;

const ToolTipContentWrapper = styled.View`
  background: ${props => props.theme.colorPalette.text.lightgray};
  min-width: 80%;
  position: absolute;
  top: 50%;
  height: auto;
  border-radius: ${props => props.theme.spacing.ELEM_SPACING.XS};
`;

const ImageWrapper = styled.View`
  position: absolute;
  right: 0;
`;

const StyledTouchableOpacity = styled.TouchableOpacity`
  padding-top: 17px;
  padding-right: 14px;
`;

const HeaderSection = styled.View`
  padding: 14px;
  border-bottom-width: 1px;
  border-bottom-color: ${props => props.theme.colorPalette.gray[500]};
`;

const BodySection = styled.View`
  padding: 14px 14px 26px;
`;

const StyledCrossImage = styled.Image`
  width: 14px;
  height: 15px;
`;

const SafeAreaViewStyle = styled.SafeAreaView`
  align-items: center;
  flex: 1;
  background: rgba(0, 0, 0, 0.5);
`;

const ModalOutsideTouchable = styled.TouchableWithoutFeedback`
  flex: 1;
`;

const ModalOverlay = styled.View`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background: rgba(0, 0, 0, 0.5);
`;

const StyledAnchor = styled(Anchor)`
  background-color: ${props => props.theme.colors.BLACK};
  padding: 9px 34px;
  align-items: center;
  border-radius: ${props => props.theme.spacing.ELEM_SPACING.XS_6};
  overflow: hidden;
  text-transform: uppercase;
  font-size: ${props => props.theme.fonts.fontSize.body.bodytext.copy2}px;
`;

const PickupText = styled.View`
  font-size: ${props => props.theme.fonts.fontSize.body.bodytext.copy3}px;
  font-weight: 700;
  align-items: center;
  flex-direction: row;
  margin-right: ${props => props.theme.spacing.ELEM_SPACING.XS};
`;

export {
  StorePickupWrapper,
  StoreSection,
  StoreButtonText,
  TooltipWrapper,
  ToolTipIcon,
  ToolTipContentWrapper,
  StoreWrapper,
  StyledAnchor,
  PickupText,
  ModalOverlay,
  SafeAreaViewStyle,
  ModalOutsideTouchable,
  ImageWrapper,
  StyledTouchableOpacity,
  StyledCrossImage,
  HeaderSection,
  BodySection,
  StoreTimeWrapper,
};
