// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { getIconPath, getLabelValue } from '@tcp/core/src/utils/utils';
import Anchor from '@tcp/core/src/components/common/atoms/Anchor';
import Notification from '../../OrderNotification';
import { Image, BodyCopy } from '../../../../../../common/atoms';
import styles from '../styles/OrdersListItemHeader.style';
import internalEndpoints from '../../../../common/internalEndpoints';
import {
  getMessageBoldText,
  getMessageNormalText,
  getOrderTypeText,
} from './OrdersListItemHeader.utils';
import HelpWithOrderModal from '../../../../../../common/organisms/HelpWithOrder/HelpWithOrderModal/HelpWithOrderModal.view';

/**
 * This component will render OrdersListItemNew component
 * @param { string, object, boolean }
 */
const OrdersListItemHeader = ({
  className,
  orderDate,
  orderNumber,
  isEcomOrder,
  showNotification,
  labels,
  expiredItemShipment,
  helpWithOrderLabels,
  cshReturnItemOlderDays,
  orderDateWithoutFormatting,
  isCSHEnabled,
  refundEligibleShipmentStatus,
  ...otherProps
}) => {
  const messageBoldText = getMessageBoldText(labels, isEcomOrder);
  const messageNormalText = getMessageNormalText(labels, isEcomOrder);
  const orderTypeText = getOrderTypeText(labels, isEcomOrder, expiredItemShipment);
  const [isHelpWithOrderOpen, setHelpWithOrderOpen] = useState(false);
  return (
    <BodyCopy component="div" className={className}>
      <div className="top-section">
        <BodyCopy className="order-type-logo" component="div">
          <Image
            src={`${
              isEcomOrder ? getIconPath('status-shipping-icon') : getIconPath('order-pickup')
            }`}
            alt=""
          />
        </BodyCopy>
        <BodyCopy component="div" className="order-type-information">
          <BodyCopy component="div" className="order-type-text">
            {orderTypeText}
          </BodyCopy>
          <BodyCopy component="div" className="order-number">
            {`#${orderNumber}`}
          </BodyCopy>
        </BodyCopy>
        <BodyCopy component="div" className="right-section">
          <BodyCopy
            data-locator="order-date-value"
            className="order-date  date"
            fontSize={['fs14', 'fs12', 'fs12']}
            component="div"
          >
            {orderDate}
          </BodyCopy>
          <div className="bottom-right-section">
            <Anchor
              to={`${internalEndpoints.orderPage.link}&orderId=${orderNumber}`}
              asPath={`${internalEndpoints.orderPage.path}/${orderNumber}`}
            >
              <BodyCopy
                fontSizeVariation="large"
                underline
                anchorVariation="primary"
                fontSize="fs10"
                component="p"
                fontFamily="secondary"
                className="view-details"
              >
                {`${getLabelValue(labels, 'lbl_orders_view_details', 'orders')}`}
              </BodyCopy>
            </Anchor>

            <BodyCopy
              className="order-help-cta"
              onClick={() => {
                setHelpWithOrderOpen(true);
              }}
              fontSize="fs10"
            >
              {`${getLabelValue(labels, 'lbl_order_help', 'orders')}`}
            </BodyCopy>

            {isHelpWithOrderOpen && (
              <HelpWithOrderModal
                setHelpWithOrderOpen={setHelpWithOrderOpen}
                isHelpWithOrderOpen={isHelpWithOrderOpen}
                helpWithOrderLabels={helpWithOrderLabels}
                orderDate={orderDate}
                cshReturnItemOlderDays={cshReturnItemOlderDays}
                orderDateWithoutFormatting={orderDateWithoutFormatting}
                orderId={orderNumber}
                isCSHEnabled={isCSHEnabled}
                refundEligibleShipmentStatus={refundEligibleShipmentStatus}
                {...otherProps}
              />
            )}
          </div>
        </BodyCopy>
      </div>
      {showNotification ? (
        <BodyCopy component="div" className="notification-section">
          <Notification
            className="notification"
            status="error"
            colSize={{ large: 12, medium: 8, small: 6 }}
            isErrorTriangleIcon
            message={`${internalEndpoints.orderPage.link}&orderId=${orderNumber}`}
            messageBoldText={messageBoldText}
            messageNormalText={messageNormalText}
          />
        </BodyCopy>
      ) : null}
    </BodyCopy>
  );
};

OrdersListItemHeader.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  className: PropTypes.string,
  orderDate: PropTypes.string.isRequired,
  orderNumber: PropTypes.string.isRequired,
  isEcomOrder: PropTypes.bool.isRequired,
  showNotification: PropTypes.bool.isRequired,
  orderSummary: PropTypes.shape({}).isRequired,
  expiredItemShipment: PropTypes.bool,
};

OrdersListItemHeader.defaultProps = {
  className: '',
  expiredItemShipment: false,
};

export default withStyles(OrdersListItemHeader, styles);
export { OrdersListItemHeader as OrdersListItemHeaderVanilla };
