// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.2);
  font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
  background-color: ${(props) => props.theme.colors.WHITE};
  @media ${(props) => props.theme.mediaQuery.smallOnly} {
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  }
  .cancelled-status {
    color: ${(props) => props.theme.colors.NOTIFICATION.ERROR};
    text-transform: capitalize;
    font-weight: ${(props) => props.theme.fonts.fontWeight.bold};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy4}px;
    }
  }
  .success-status {
    color: ${(props) => props.theme.colorPalette.green[500]};
    text-transform: capitalize;
    font-weight: ${(props) => props.theme.fonts.fontWeight.bold};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy4}px;
    }
  }
  .partially-cancelled {
    color: ${(props) => props.theme.colors.TEXT.DARKERGRAY};
    text-transform: capitalize;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy4}px;
    }
  }
  .order-items-container {
    display: flex;
    padding: 0 ${(props) => props.theme.spacing.ELEM_SPACING.MED}
      ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    flex-wrap: wrap;
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
  }
  .cancelled-items-container {
    opacity: 0.5;
  }
  .order-item-image {
    width: 112px;
    max-height: 130px;
    margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    display: inline-block;
    position: relative;
    overflow: hidden;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: 60px;
      max-height: 70px;
      margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    }
    img {
      height: 100%;
      width: auto;
    }
  }
  .item-quantity {
    position: absolute;
    height: 30px;
    width: 30px;
    background: ${(props) => props.theme.colorPalette.blue.E100};
    color: #fff;
    border-radius: 50%;
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy2}px;
    right: 0;
    bottom: 0;
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      height: 16px;
      width: 16px;
      font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy1}px;
    }
  }

  .order-status-container {
    display: flex;
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    padding: ${(props) => props.theme.spacing.ELEM_SPACING.LRG}
      ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    position: relative;
    &:before {
      height: 1px;
      background: ${(props) => props.theme.colorPalette.text.lightgray};
      content: '';
      position: absolute;
      left: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
      top: 0;
      right: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    }
  }
  .first-shipment {
    &:before {
      display: none;
    }
  }
  .order-status-logo {
    display: flex;
    height: 38px;
    width: 38px;
    align-self: flex-start;
    margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      height: 24px;
      width: 24px;
    }
    img {
      width: 100%;
    }
  }
  .order-status-text {
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy5}px;
    flex: 1;
    display: flex;
    flex-wrap: wrap;
    flex-direction: column;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy4}px;
    }
    p {
      display: flex;
      flex: 1;
      align-items: center;
      text-transform: capitalize;
      font-weight: ${(props) => props.theme.fonts.fontWeight.bold};
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        display: inline-block;
        margin-right: 8px;
        flex: auto;
      }
    }
  }
  .shipment-count {
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    font-weight: ${(props) => props.theme.fonts.fontWeight.normal};
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy3}px;
    color: ${(props) => props.theme.colors.TEXT.DARKERGRAY};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy2}px;
    }
  }
  .shipment-devider {
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    font-weight: ${(props) => props.theme.fonts.fontWeight.normal};
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy3}px;
    margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
    margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy2}px;
    }
  }
  .shipment-edd {
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    font-weight: ${(props) => props.theme.fonts.fontWeight.normal};
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy3}px;
    color: ${(props) => props.theme.colorPalette.green[500]};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy2}px;
    }
  }
  .order-preview-footer {
    display: flex;
    padding: 0 ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      flex-direction: column;
    }
  }
  .order-total {
    display: flex;
  }
  .progress-bar-container {
    flex: 1;
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
  }
  .track-order-container {
    display: flex;
    align-items: flex-end;
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    text-align: right;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      position: relative;
      top: -${(props) => props.theme.spacing.ELEM_SPACING.XS};
    }
  }
  .track-order-click-tracker {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      flex: 1;
      display: flex;
      align-items: center;
      justify-content: space-between;
    }
  }
  .order-trackingNumber {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      max-width: 138px;
      line-height: 1;
    }
  }
  .track-order-cta {
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    padding: 10px 30px;
    border-radius: 6px;
    background-color: ${(props) => props.theme.colors.PRIMARY.DARK};
    text-transform: uppercase;
    color: ${(props) => props.theme.colors.WHITE};
    cursor: pointer;
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy2}px;
    font-weight: ${(props) => props.theme.fonts.fontWeight.extrabold};
    min-height: 0;
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      font-size: 10px;
      padding: 8px 36px;
      margin-top: 0;
      margin-left: auto;
    }
  }
  .directions-cta {
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    padding: 10px 30px;
    border-radius: 6px;
    background-color: ${(props) => props.theme.colors.PRIMARY.DARK};
    text-transform: uppercase;
    color: ${(props) => props.theme.colors.WHITE};
    cursor: pointer;
    font-weight: ${(props) => props.theme.fonts.fontWeight.bold};
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy2}px;
    min-height: 0;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      font-size: 10px;
      padding: 8px 36px;
    }
  }
  .accordian-wrap {
    overflow: hidden;
    max-height: 440px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      max-height: 350px;
    }
  }
  .expanded {
    max-height: none;
  }
  .accordian-toggle-button {
    width: 100%;
    max-height: 47px;
    background: ${(props) => props.theme.colors.WHITE};
    border: none;
    cursor: pointer;
    outline: none;
    img {
      transition: all 0.2s ease;
      transform: rotate(90deg);
      height: 47px;
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        height: 27px;
      }
    }
    box-shadow: 0 -2px 4px 0 rgba(0, 0, 0, 0.16);
  }
  .toggle-button-expanded {
    img {
      transition: all 0.2s ease;
      transform: rotate(-90deg);
    }
  }
  .hidden-items {
    opacity: 0.89;
    background-color: ${(props) => props.theme.colors.WHITE};
    position: absolute;
    top: 0;
    right: 0;
    left: 0;
    bottom: 0;
    font-weight: ${(props) => props.theme.fonts.fontWeight.bold};
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy7}px;
    display: flex;
    align-items: center;
    justify-content: center;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy2}px;
    }
  }
  .store-data-container {
    text-align: right;
    flex: none;
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy4}px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      display: flex;
      align-items: end;
      justify-content: space-between;
      font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy2}px;
      width: 100%;
    }
  }

  .store-data {
    padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    }
  }
  .store-address-text {
    text-align: right;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      text-align: left;
    }
  }
  .store-addresssline {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      display: block;
    }
  }
`;

export default styles;
