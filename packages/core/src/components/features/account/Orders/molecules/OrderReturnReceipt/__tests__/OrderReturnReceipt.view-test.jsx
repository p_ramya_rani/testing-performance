// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { OrderReturnReceiptVanilla } from '../views/OrderReturnReceipt.view';

jest.mock('@tcp/core/src/utils', () => ({
  getViewportInfo: jest.fn().mockImplementation(() => {
    return { isDesktop: true };
  }),
  getAPIConfig: () => ({
    brandId: 'tcp',
  }),
  isClient: jest.fn(),
  getIconPath: jest.fn(),
  isMobileApp: jest.fn(),
  getPaymentDetails: jest.fn(),
}));

describe('Order Summary Details component', () => {
  const props = {
    className: 'sc-cpmKsF hbZcvM sc-eXNvrr jdKOHR',
    printTextClassName: 'order-details-print-cta',
    orderDetailsData: {
      orderNumber: '6005056842',
      orderDate: '2020-06-26 01:49:01.719',
      pickUpExpirationDate: null,
      pickedUpDate: 'N/A',
      shippedDate: 'N/A',
      orderStatus: 'order in process',
      status: 'lbl_orders_statusOrderReceived',
      trackingNumber: 'N/A',
      isBopisOrder: false,
      isBossOrder: false,
      orderType: 'ECOM',
      bossMaxDate: null,
      bossMinDate: null,
      summary: {
        currencySymbol: '$',
        totalItems: 1,
        subTotal: 12.25,
        purchasedItems: 0,
        shippedItems: 0,
        canceledItems: 0,
        returnedItems: 0,
        returnedTotal: 0,
        couponsTotal: 0,
        shippingTotal: 0,
        totalTax: 0,
        grandTotal: 12.25,
      },
      appliedGiftCards: [],
      canceledItems: [],
      returnedInitiatedItems: [
        {
          status: 'return initiated',
          orderStatus: 'return initiated',
          isCSHReturn: 'true',
          returnInitiatedDate: '10/19/2021 10:35:14',
          items: [
            {
              itemInfo: {
                listPrice: 9.95,
                offerPrice: 9.95,
                linePrice: 19.9,
                quantity: 2,
                quantityCanceled: 0,
                quantityShipped: 2,
                quantityReturned: 0,
                quantityRefunded: 0,
                quantityOOS: 0,
                itemBrand: 'TCP',
                taxUnitPrice: 0.7,
              },
              productInfo: {
                appeasementApplied: false,
                fit: null,
                pdpUrl:
                  'http://uatlive1.childrensplace.com/us/p/Toddler-Girls-Uniform-Short-Sleeve-Ruffle-Pique-Polo-2100620-1301',
                name: 'Toddler Girls Uniform Ruffle Pique Polo',
                imagePath:
                  'https://test1.theplace.com/image/upload/ecom/assets/products/tcp/2100620/2100620_1301.jpg',
                thumbnailImgPath: '2100620/2100620_1301.jpg',
                upc: '00193511028165',
                lineNumber: '1518191081',
                variantNo: '2100620033',
                size: '3T',
                color: {
                  name: 'ROSE DUST',
                  imagePath: '2100620/2100620_1301_swatch.jpg',
                },
              },
              trackingInfo: [
                {
                  carrier: null,
                  quantity: 2,
                  shipDate: null,
                  carrierDeliveryDate: null,
                  shipVia: null,
                  status: 'Return Initiated',
                  trackingUrl: null,
                  trackingNbr: null,
                  refundAmount: 0,
                  refundDate: null,
                  appeasement: 0,
                  actualDeliveryDate: null,
                  returnInitiatedDate: '10/19/2021 10:35:14',
                  isCSHReturn: 'true',
                },
                {
                  carrier: 'UPS',
                  quantity: 2,
                  shipDate: '11/10/21 00:000',
                  carrierDeliveryDate: null,
                  shipVia: 'UGNR',
                  status: 'Delivered',
                  trackingUrl:
                    'https://childrensplace.narvar.com/childrensplace/tracking/ups?tracking_numbers=7NW64107Y600306638_3&service=UG&ozip=35968&dzip=46350&locale=en_US&order_number=344134957&order_date=2021-10-19T02:00:00.000-04:00&ship_date=2021-10-19T02:57:24.000-04:00',
                  trackingNbr: '7NW64107Y600306638_3',
                  refundAmount: 0,
                  refundDate: null,
                  appeasement: 0,
                  actualDeliveryDate: '2021-10-19 12:19:000',
                  returnInitiatedDate: null,
                  isCSHReturn: null,
                },
              ],
            },
          ],
        },
      ],
      returnedItems: [
        {
          trackingNumber: '7NW64107Y600306642_3',
          trackingUrl:
            'https://childrensplac.narvar.com/childrensplace/tracking/ups?tracking_numbers=7NW64107Y600306642_3&service=UG&ozip=35968&dzip=46350&locale=en_US&order_number=344134958&order_date=2021-10-19T02:07:00.000-04:00&ship_date=2021-10-19T02:38:42.000-04:00',
          shippedDate: '11/10/21 00:000',
          refundDate: null,
          refundAmount: 0,
          status: 'order returned',
          orderStatus: 'order returned',
          items: [
            {
              itemInfo: {
                listPrice: 19.95,
                offerPrice: 19.95,
                linePrice: 19.95,
                quantity: 1,
                quantityCanceled: 0,
                quantityShipped: 3,
                quantityReturned: 1,
                quantityRefunded: 0,
                quantityOOS: 0,
                itemBrand: 'TCP',
                taxUnitPrice: 1.4,
              },
              productInfo: {
                appeasementApplied: false,
                fit: null,
                pdpUrl:
                  'http://uatlive1.childrensplace.com/us/p/Boys-Active-Fleece-Jogger-Pants-3016287-7R',
                name: 'Boys Active Fleece Jogger Pants',
                imagePath:
                  'https://test1.theplace.com/image/upload/ecom/assets/products/tcp/3016287/3016287_7R.jpg',
                thumbnailImgPath: '3016287/3016287_7R.jpg',
                upc: '00194936100757',
                lineNumber: '1518191089',
                variantNo: '3016287007',
                size: 'XS (4)',
                color: {
                  name: 'REDWOOD',
                  imagePath: '3016287/3016287_7R_swatch.jpg',
                },
              },
              trackingInfo: [
                {
                  carrier: 'UPS',
                  quantity: 1,
                  shipDate: '11/10/21 00:00',
                  carrierDeliveryDate: null,
                  shipVia: 'UGNR',
                  status: 'Order Returned',
                  trackingUrl:
                    'https://childrensplace.narvar.com/childrensplace/tracking/ups?tracking_numbers=7NW64107Y600306642_3&service=UG&ozip=35968&dzip=46350&locale=en_US&order_number=344134958&order_date=2021-10-19T02:07:00.000-04:00&ship_date=2021-10-19T02:38:42.000-04:00',
                  trackingNbr: '7NW64107Y600306642_3',
                  refundAmount: 0,
                  refundDate: null,
                  appeasement: 0,
                  actualDeliveryDate: '2021-10-19 12:19:00',
                  returnInitiatedDate: null,
                  isCSHReturn: 'false',
                },
                {
                  carrier: 'UPS',
                  quantity: 3,
                  shipDate: '11/10/21 00:00',
                  carrierDeliveryDate: null,
                  shipVia: 'UGNR',
                  status: 'Delivered',
                  trackingUrl:
                    'https://childrensplace.narvar.com/childrensplace/tracking/ups?tracking_numbers=7NW64107Y600306642_3&service=UG&ozip=35968&dzip=46350&locale=en_US&order_number=344134958&order_date=2021-10-19T02:07:00.000-04:00&ship_date=2021-10-19T02:38:42.000-04:00',
                  trackingNbr: '7NW64107Y600306642_3',
                  refundAmount: 0,
                  refundDate: null,
                  appeasement: 0,
                  actualDeliveryDate: '2021-10-19 12:19:00',
                  returnInitiatedDate: null,
                  isCSHReturn: null,
                },
              ],
            },
          ],
        },
      ],

      purchasedItems: [
        {
          items: [
            {
              itemInfo: {
                listPrice: 12.25,
                offerPrice: 12.25,
                linePrice: 12.25,
                quantity: 1,
                quantityCanceled: 0,
                quantityShipped: 0,
                quantityReturned: 0,
                quantityOOS: 0,
                itemBrand: 'TCP',
              },
              productInfo: {
                fit: null,
                pdpUrl:
                  'http://uatlive1.childrensplace.com/us/p/Boys-Basic-Straight-Stretch-Jeans---Drift-Wash-2110267-DB',
                name: 'Boys Basic Straight Stretch Jeans',
                imagePath:
                  'https://test1.theplace.com/image/upload/ecom/assets/products/tcp/2110267/2110267_DB.jpg',
                upc: '00191755267999',
                size: '10',
                color: {
                  name: 'DENIM',
                  imagePath: '2110267/2110267_DB_swatch.jpg',
                },
              },
              trackingInfo: [
                {
                  carrier: 'UPS',
                  quantity: 1,
                  status: 'Order In Process',
                  trackingUrl:
                    'https://childrensplace.narvar.com/childrensplace/tracking/ups?order_number=6005056842&order_date=2020-06-26T01:49:01-04:00&dzip=07094&locale=en_US&type=p',
                },
              ],
            },
          ],
          status: 'order received',
          orderStatus: 'order received',
          trackingNumber: null,
          trackingUrl:
            'https://childrensplace.narvar.com/childrensplace/tracking/ups?order_number=6005056842&order_date=2020-06-26T01:49:01-04:00&dzip=07094&locale=en_US&type=p',
        },
      ],
      outOfStockItems: [],
      checkout: {
        shippingAddress: {
          firstName: 'New',
          lastName: 'Add',
          addressLine1: '500 Plaza Dr',
          addressLine2: '',
          city: 'Secaucus',
          state: 'NJ',
          zipCode: '07094',
          country: 'US',
        },
        pickUpStore: null,
      },
    },
    ordersLabels: {
      lbl_orders_orderDate: 'Order Date',
      lbl_orderDetails_orderNumber: 'Order Number',
      lbl_orderDetails_upc: 'UPC: ',
      lbl_orders_return_receipt: 'RETURN RECEIPT',
      lbl_orders_return_by: 'Return By:',
      lbl_orders_shipment: 'Shipment',
      lbl_order_receipt_message_heading: 'COVID-19 RETURN POLICY UPDATE:',
      lbl_order_receipt_message_information:
        'In order to accommodate during this difficult time, we have temporarily extended our return policy. Purchases made in February through April 2020, can be returned through July 1, 2020.',
    },
    orderReturnDays: 0,
  };

  it('should renders correctly', () => {
    const component = shallow(<OrderReturnReceiptVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders correctly with return date', () => {
    const customProps = {
      ...props,
      orderReturnDays: 45,
    };
    const component = shallow(<OrderReturnReceiptVanilla {...customProps} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders correctly with purchasedItemsAll', () => {
    const customProps = {
      ...props,
      orderReturnDays: 45,
      orderDetailsData: {
        ...props.orderDetailsData,
        purchasedItemsAll: [...props.orderDetailsData.purchasedItems],
      },
    };
    const component = shallow(<OrderReturnReceiptVanilla {...customProps} />);
    expect(component).toMatchSnapshot();
  });
});
