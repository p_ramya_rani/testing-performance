// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { OrderShipmentProgressVanilla } from '../views/OrdersShipmentProgress.view';

describe('OrdersListItem component', () => {
  it('should renders correctly', () => {
    const props = {
      labels: {},
      orderItem: {
        orderTotal: 10.5,
      },
    };
    const component = shallow(<OrderShipmentProgressVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('should not render edd', () => {
    const props = {
      labels: {},
      progressSteps: [
        {
          name: 'test',
          isReached: false,
          edd: 'edd date',
        },
      ],
      showEdd: false,
    };
    const component = shallow(<OrderShipmentProgressVanilla {...props} />);
    expect(component.find('.edd')).toHaveLength(0);
    expect(component).toMatchSnapshot();
  });
  it('should render edd', () => {
    const props = {
      labels: {},
      progressSteps: [
        {
          name: 'test',
          isReached: false,
          edd: 'edd date',
        },
      ],
      showEdd: true,
    };
    const component = shallow(<OrderShipmentProgressVanilla {...props} />);
    expect(component.find('.edd')).toHaveLength(1);
    expect(component).toMatchSnapshot();
  });
});
