// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { OrderListWithDetails, openStoreDirections } from '../OrderListWithDetails.view';
import { getItemsWithTargetStatus, getProgressBarSteps } from '../OrderListWithDetails.utils';

const props = {
  labels: {},
  className: '',
  ordersListItems: [],
  isEcomOrder: true,
};

describe('Order Items component', () => {
  it('should renders correctly', () => {
    const component = shallow(<OrderListWithDetails {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('openStoreDirections should be called', () => {
    const mockStore = {
      shippingAddress: {
        addressLine1: 'SECAUCUS OUTLET|210 MEADOWLAND PARKWAY',
        addressLine2: '',
        addressType: 'BOPIS',
        altEmail: '',
        altFirstName: '',
        altLastName: '',
        city: 'SECAUCUS',
        country: 'United States',
        email: 'THIRUMALAYERUVA@GMAIL.COM',
        firstName: 'Thirumala',
        lastName: 'Yeruva',
        phone: '2012234849',
        state: 'NJ',
        storeHours: [
          {
            availability: [
              {
                from: '2021-01-04T10:00:00',
                status: 'opened',
                to: '2021-01-04T19:00:00',
              },
            ],
            nick: 'monday',
          },
          {
            availability: [
              {
                from: '2021-01-05T10:00:00',
                status: 'opened',
                to: '2021-01-05T19:00:00',
              },
            ],
            nick: 'tuesday',
          },
          {
            availability: [
              {
                from: '2021-01-06T10:00:00',
                status: 'opened',
                to: '2021-01-06T19:00:00',
              },
            ],
            nick: 'Three Kings Day',
          },
          {
            availability: [
              {
                from: '2021-01-07T10:00:00',
                status: 'opened',
                to: '2021-01-07T19:00:00',
              },
            ],
            nick: 'thursday',
          },
          {
            availability: [
              {
                from: '2021-01-08T10:00:00',
                status: 'opened',
                to: '2021-01-08T19:00:00',
              },
            ],
            nick: 'friday',
          },
          {
            availability: [
              {
                from: '2021-01-09T10:00:00',
                status: 'opened',
                to: '2021-01-09T19:00:00',
              },
            ],
            nick: 'saturday',
          },
          {
            availability: [
              {
                from: '2021-01-10T10:00:00',
                status: 'opened',
                to: '2021-01-10T19:00:00',
              },
            ],
            nick: 'sunday',
          },
          {
            availability: [
              {
                from: '2021-01-11T10:00:00',
                status: 'opened',
                to: '2021-01-11T19:00:00',
              },
            ],
            nick: 'monday',
          },
          {
            availability: [
              {
                from: '2021-01-12T10:00:00',
                status: 'opened',
                to: '2021-01-12T19:00:00',
              },
            ],
            nick: 'tuesday',
          },
        ],
        storeName: '4298 ',
        zipCode: '07094',
      },
      shippingInfo: null,
      shippingMethod: 'Customer Pick up',
    };
    let result = '';
    global.open = (url) => {
      result = url;
      return url;
    };
    const expected =
      'https://maps.google.com/maps?daddr=SECAUCUS OUTLET|210 MEADOWLAND PARKWAY,%20SECAUCUS,%20NJ,%2007094';
    openStoreDirections(mockStore);
    expect(result).toEqual(expected);
  });
});

describe('orderlistwithdetails utils', () => {
  it('should return PreparingItems', () => {
    const preparing = 'preparing your order';
    const purchasedItems = [
      {
        items: [
          {
            trackingInfo: [
              {
                status: preparing,
                quantity: 1,
              },
            ],
          },
        ],
      },
    ];
    const result = [
      {
        items: [
          {
            trackingInfo: [
              {
                status: preparing,
                quantity: 1,
              },
            ],
          },
        ],
        packageStatus: preparing,
        shipmentStatus: 'lbl_orders_preparing',
      },
    ];
    const items = getItemsWithTargetStatus(purchasedItems, preparing);
    expect(items).toEqual(result);
  });

  it('should return expired items', () => {
    const pickupExpired = 'pickup expired';
    const purchasedItems = [
      {
        items: [
          {
            trackingInfo: [
              {
                status: pickupExpired,
                quantity: 1,
              },
            ],
          },
        ],
      },
    ];
    const result = [
      {
        items: [
          {
            trackingInfo: [
              {
                status: pickupExpired,
                quantity: 1,
              },
            ],
          },
        ],
        packageStatus: pickupExpired,
        shipmentStatus: 'lbl_orders_heading_store_pickup_expired',
      },
    ];
    const items = getItemsWithTargetStatus(purchasedItems, pickupExpired);
    expect(items).toEqual(result);
  });

  it('should return boss progress steps', () => {
    const labels = {
      orders: {
        lbl_orders_processing: 'processing',
        lbl_orders_statusItemsReadyForPickup: 'ready for pick up',
        lbl_orders_statusItemsPickedUp: 'picked up',
      },
    };
    const status = 'picked up';
    const isEcomOrder = false;
    const isBOSSOrder = true;
    const result = getProgressBarSteps(labels, status, isEcomOrder, isBOSSOrder);
    const expected = [
      { name: 'processing', isReached: true },
      {
        name: 'ready for pick up',
        isReached: true,
      },
      {
        name: 'picked up',
        isReached: true,
      },
    ];
    expect(result).toEqual(expected);
  });
});
