// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import OrderListStorePickup from '../views/OrderListStorePickup.view.native';

const props = {
  labels: {},
  isCancelledItems: false,
  orderSummary: {
    shippingAddress: '123 main st, secaucus, NJ - 10307',
  },
  storeAddress: {
    addressLine1: 'Secaucus outlet',
  },
};

describe('OrderListStorePickup View component', () => {
  it('should renders correctly', () => {
    const component = shallow(<OrderListStorePickup {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders cancelled order correctly', () => {
    const newProps = {
      ...props,
      isCancelledItems: true,
    };
    const component = shallow(<OrderListStorePickup {...newProps} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders modal falsy correctly', () => {
    const newProps = {
      ...props,
      isStoreModalOpen: false,
      setStoreModalOpen: () => {},
    };
    const component = shallow(<displayStoreDetails {...newProps} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders modal truthy correctly', () => {
    const newProps = {
      ...props,
      isStoreModalOpen: true,
      setStoreModalOpen: () => {},
    };
    const component = shallow(<displayStoreDetails {...newProps} />);
    expect(component).toMatchSnapshot();
  });
});
