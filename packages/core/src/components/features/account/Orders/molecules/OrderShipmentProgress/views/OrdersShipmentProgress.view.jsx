// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { Image, BodyCopy } from '@tcp/core/src/components/common/atoms';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import styles from '../styles/OrderShipmentProgress.style';

const showPlaceholder = (showEdd, edd, iconPath) => {
  return showEdd && !edd && !iconPath;
};

/**
 * This component will render OrdersListItemNew component
 * @param { string, object, boolean }
 */
const OrderShipmentProgress = ({ className, progressSteps, showEdd }) => {
  const totalCount = progressSteps.length || 0;
  return (
    <BodyCopy component="div" className={className}>
      <BodyCopy component="div" className="progress">
        <ul className="list-container">
          {progressSteps &&
            progressSteps.length &&
            progressSteps.map((step, i) => {
              const { name, isReached = false, edd, iconPath } = step;
              const placeholderText = '';
              const listClassName = `checkpoint ${isReached ? 'reached' : ''} ${
                i === 0 ? 'first-step' : ''
              } ${i + 1 === totalCount ? 'last-step' : ''} step_${i + 1}`;
              return (
                <li className={listClassName}>
                  <BodyCopy component="div" className="heading">
                    <p className="status-name">{name}</p>
                    {showEdd && edd && <p className="edd">{edd}</p>}
                    {iconPath && iconPath.length && (
                      <div className="in-transit-icon">
                        <Image alt="In transit" src={iconPath} />
                      </div>
                    )}
                    {showPlaceholder(showEdd, edd, iconPath) && (
                      <p className="placeholder">{placeholderText}</p>
                    )}
                  </BodyCopy>
                  <BodyCopy component="div" className="bullet-step" />
                </li>
              );
            })}
        </ul>
      </BodyCopy>
    </BodyCopy>
  );
};

OrderShipmentProgress.propTypes = {
  className: PropTypes.string,
  progressSteps: PropTypes.shape([]),
  showEdd: PropTypes.bool,
};

OrderShipmentProgress.defaultProps = {
  className: '',
  progressSteps: {},
  showEdd: false,
};

export default withStyles(OrderShipmentProgress, styles);
export { OrderShipmentProgress as OrderShipmentProgressVanilla };
