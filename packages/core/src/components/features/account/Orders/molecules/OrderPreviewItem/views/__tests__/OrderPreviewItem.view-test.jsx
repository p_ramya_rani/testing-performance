// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { OrderPreviewItemVanilla } from '../OrderPreviewItem.view';

describe('Order Items component', () => {
  it('should renders correctly', () => {
    const props = {
      item: {
        productInfo: {
          name: 'test product',
          imagePath: 'test',
        },
        itemInfo: {
          itemBrand: 'TCP',
          quantity: 1,
          quantityCanceled: false,
        },
      },
      ordersLabels: {},
      isCanceledList: {},
    };
    const component = shallow(<OrderPreviewItemVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
