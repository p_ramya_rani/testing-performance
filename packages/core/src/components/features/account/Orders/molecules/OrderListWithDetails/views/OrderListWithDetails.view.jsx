// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { getIconPath, getLabelValue } from '@tcp/core/src/utils/utils';
import { isMobileApp, isClient, getViewportInfo } from '@tcp/core/src/utils';
import googleMapConstants from '@tcp/core/src/constants/googleMap.constants';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { Image, DamImage, Anchor } from '@tcp/core/src/components/common/atoms';
import styles from '../styles/OrderListWithDetails.style';
import OrdersListItemHeader from '../../OrdersListItemHeader/views/OrdersListItemHeader.view';
import OrderShipmentProgress from '../../OrderShipmentProgress/views/OrdersShipmentProgress.view';
import internalEndpoints from '../../../../common/internalEndpoints';
import OrderStatusContainer, {
  TrackingCTA,
} from '../../OrderStatusContainer/views/OrderStatusContainer.view';
import {
  getProgressBarSteps,
  displayUpcomingOrderView,
  showNotificationMessage,
  buildShipments,
  getTotalCount,
  getQuantityToShow,
  getItemsWithTargetStatus,
  getIsItemShipped,
} from './OrderListWithDetails.utils';
import constants from '../../../Orders.constants';

const imgConfig = [`t_plp_img_m`, `t_plp_img_t`, `t_plp_img_d`];
const { STATUS_CONSTANTS = {} } = constants;
const {
  SHIPPED,
  ITEMS_PICKED_UP,
  ITEM_READY_FOR_PICK_UP,
  PREPARING_YOUR_ORDER,
  PROCESSING,
  PICKUP_EXPIRED,
  DELIVERED,
  IN_TRANSIT,
  OUT_FOR_DELIVERY,
  RETURN_INITIATED,
  RETURN_COMPLETED,
  ORDER_BEING_PROCESSED,
} = STATUS_CONSTANTS;

/**
 * This function component use for return the Order item list based on group
 * can be passed in the component.
 * @param otherProps - otherProps object used for showing Order Item list
 */
export const openStoreDirections = (orderSummary) => {
  const { shippingAddress } = orderSummary;
  const { addressLine1, city, state, zipCode } = shippingAddress;
  if (isMobileApp()) {
    const store = {
      basicInfo: { address: shippingAddress },
    };
    this.mapHandler(store);
  }
  if (window) {
    window.open(
      `${googleMapConstants.OPEN_STORE_DIR_WEB}${addressLine1},%20${city},%20${state},%20${zipCode}`,
      '_blank',
      'noopener'
    );
  }
};

const getMaxQuantityToBeShown = (isMobile) => (isMobile ? 5 : 8);

const ImageComp = (imgData, index, itemBrand, quantityToShow, wrappedItem, itemsHidden) => {
  return (
    <>
      <DamImage
        imgConfigs={imgConfig}
        imgData={imgData}
        isProductImage
        data-locator="order_item_image"
        key={index.toString()}
        itemBrand={itemBrand}
      />
      {quantityToShow && quantityToShow > 1 && (
        <span className="item-quantity">{quantityToShow}</span>
      )}
      {wrappedItem && itemsHidden && <span className="hidden-items">{`+${itemsHidden + 1}`}</span>}
    </>
  );
};

const showShipmentInfo = (
  {
    orderStatus,
    labels,
    isEcomOrder,
    displayUpcomingOrdersView,
    isMobile,
    orderTrackingUrl,
    orderSummary,
    totalCount,
    isBOSSOrder,
    orderDate,
    expectedDeliveryDate,
  },
  shipmentsObj = [],
  isCancelledItems = false,
  shipmentIndex = 1,
  storeAddress = {},
  orderNumber
) => {
  const maxQuantityToBeShown = getMaxQuantityToBeShown(isMobile);
  return (
    !!shipmentsObj &&
    !!shipmentsObj.length &&
    shipmentsObj.map((shipment) => {
      const {
        items,
        shipmentStatus: orderShipmentStatus = orderStatus,
        trackingUrl = orderTrackingUrl,
        shipDate,
        shippedDate,
        actualDeliveryDate,
        deliveryDate,
        carrierDeliveryDate,
        packageStatus,
        orderTracking: orderTrackingNumber,
      } = shipment || {};

      if (!items || !items.length) {
        return null;
      }
      const edd = {
        shipDate,
        actualDeliveryDate,
        orderDate,
      };
      const statusText = getLabelValue(labels, orderShipmentStatus, 'orders');
      const progressSteps = getProgressBarSteps(labels, statusText, isEcomOrder, isBOSSOrder, edd);
      const isPartiallyCancelled = isCancelledItems && shipmentIndex > 1;
      const upcomingOrdersView = displayUpcomingOrderView(
        displayUpcomingOrdersView,
        statusText,
        isCancelledItems
      );

      const isItemShipped = getIsItemShipped(statusText);
      const getImageWithUrl = (
        pdpUrl,
        imgData,
        index,
        itemBrand,
        quantityToShow,
        wrappedItem,
        itemsHidden
      ) => {
        return (
          <>
            {pdpUrl === null ? (
              ImageComp(imgData, index, itemBrand, quantityToShow, wrappedItem, itemsHidden)
            ) : (
              <Anchor
                to={`${internalEndpoints.orderPage.link}&orderId=${orderNumber}`}
                asPath={`${internalEndpoints.orderPage.path}/${orderNumber}`}
              >
                {ImageComp(imgData, index, itemBrand, quantityToShow, wrappedItem, itemsHidden)}
              </Anchor>
            )}
          </>
        );
      };

      return (
        <>
          <OrderStatusContainer
            upcomingOrdersView={upcomingOrdersView}
            orderShipmentStatus={orderShipmentStatus}
            labels={labels}
            isMobile={isMobile}
            isEcomOrder={isEcomOrder}
            orderTrackingUrl={trackingUrl}
            orderSummary={orderSummary}
            totalCount={totalCount}
            isCancelledItems={isCancelledItems}
            index={shipmentIndex}
            isPartiallyCancelled={isPartiallyCancelled}
            storeAddress={storeAddress}
            expectedDeliveryDate={expectedDeliveryDate}
            deliveryDate={deliveryDate}
            carrierDeliveryDate={carrierDeliveryDate}
            isItemShipped={isItemShipped}
            shipDate={shippedDate}
            packageStatus={packageStatus}
          />

          <div
            className={`order-items-container ${
              isCancelledItems ? 'cancelled-items-container' : ''
            }`}
          >
            {items &&
              items.length &&
              items.map((item, index) => {
                const currentIndex = index + 1;
                const showItem = currentIndex <= maxQuantityToBeShown;
                if (!showItem) return null;
                const {
                  itemInfo: { quantity = 0, quantityCanceled = 0, itemBrand } = {},
                  productInfo: { thumbnailImgPath, name, pdpUrl },
                  trackingInfo = [],
                } = item;
                const imgData = {
                  alt: name,
                  url: thumbnailImgPath,
                };
                const wrappedItem = currentIndex === maxQuantityToBeShown;
                const itemsHidden = items.length - maxQuantityToBeShown;
                const quantityToShow = getQuantityToShow(
                  quantity,
                  quantityCanceled,
                  isCancelledItems,
                  statusText,
                  trackingInfo
                );
                return (
                  <div className="order-item-image">
                    {!wrappedItem ? (
                      getImageWithUrl(
                        pdpUrl,
                        imgData,
                        index,
                        itemBrand,
                        quantityToShow,
                        wrappedItem,
                        itemsHidden
                      )
                    ) : (
                      <Anchor
                        to={`${internalEndpoints.orderPage.link}&orderId=${orderNumber}`}
                        asPath={`${internalEndpoints.orderPage.path}/${orderNumber}`}
                      >
                        {ImageComp(
                          imgData,
                          index,
                          itemBrand,
                          quantityToShow,
                          wrappedItem,
                          itemsHidden
                        )}
                      </Anchor>
                    )}
                  </div>
                );
              })}
          </div>
          {!upcomingOrdersView && (
            <div className="order-preview-footer">
              <div className="progress-bar-container">
                <OrderShipmentProgress progressSteps={progressSteps} showEdd={isEcomOrder} />
              </div>

              {statusText.toLowerCase() !== RETURN_INITIATED &&
                statusText.toLowerCase() !== RETURN_COMPLETED &&
                packageStatus.toLowerCase() !== ORDER_BEING_PROCESSED && (
                  <TrackingCTA
                    isEcomOrder={isEcomOrder}
                    orderTrackingUrl={trackingUrl}
                    orderSummary={orderSummary}
                    labels={labels}
                    orderTrackingNumber={orderTrackingNumber}
                    isMobile={isMobile}
                  />
                )}
            </div>
          )}
        </>
      );
    })
  );
};

export const OrderContainer = ({
  order,
  labels,
  upcomingOrdersView,
  helpWithOrderLabels,
  cshReturnItemOlderDays,
  setOrderResendModalOpen,
  isCSHEnabled,
}) => {
  const [isExpanded, setIsExpanded] = useState(false);

  const toggleExpansion = () => {
    setIsExpanded(!isExpanded);
  };

  let isMobile = false;

  if (isClient()) {
    ({ isMobile } = getViewportInfo());
  }

  const {
    orderSummary = {},
    orderDate = '',
    orderDateWithoutFormatting = '',
    orderNumber = '',
    orderTotal = 0,
    currencySymbol = '',
    isEcomOrder = false,
    orderTrackingUrl = '',
    orderStatus = '',
    isBOSSOrder = false,
    expectedDeliveryDate,
  } = order;
  const {
    purchasedItems = [],
    canceledItems = [{ items: [] }],
    shippingAddress = {},
  } = orderSummary;
  let processingShipment = [];
  let shippedShipment = [];
  let pickedUpShipment = [];
  let readyForPickUpShipment = [];
  let preparingItemShipment = [];
  let deliveredItemShipment = [];
  let inTransitItemShipment = [];
  let outForDeliveryItemShipment = [];
  let returnInitiated = [];
  let returnCompleted = [];
  let orderBeingProcessedShipment = [];
  const expiredItemShipment = getItemsWithTargetStatus(canceledItems, PICKUP_EXPIRED);

  if (purchasedItems.length) {
    processingShipment = getItemsWithTargetStatus(purchasedItems, PROCESSING);
    orderBeingProcessedShipment = getItemsWithTargetStatus(purchasedItems, ORDER_BEING_PROCESSED);
    shippedShipment = getItemsWithTargetStatus(purchasedItems, SHIPPED);
    pickedUpShipment = getItemsWithTargetStatus(purchasedItems, ITEMS_PICKED_UP);
    readyForPickUpShipment = getItemsWithTargetStatus(purchasedItems, ITEM_READY_FOR_PICK_UP);
    preparingItemShipment = getItemsWithTargetStatus(purchasedItems, PREPARING_YOUR_ORDER);
    deliveredItemShipment = getItemsWithTargetStatus(purchasedItems, DELIVERED);
    inTransitItemShipment = getItemsWithTargetStatus(purchasedItems, IN_TRANSIT);
    outForDeliveryItemShipment = getItemsWithTargetStatus(purchasedItems, OUT_FOR_DELIVERY);
    returnInitiated = getItemsWithTargetStatus(purchasedItems, RETURN_INITIATED);
    returnCompleted = getItemsWithTargetStatus(purchasedItems, RETURN_COMPLETED);
  }

  let totalCount =
    processingShipment.length +
    orderBeingProcessedShipment.length +
    shippedShipment.length +
    pickedUpShipment.length +
    readyForPickUpShipment.length +
    preparingItemShipment.length +
    deliveredItemShipment.length +
    inTransitItemShipment.length +
    outForDeliveryItemShipment.length +
    returnInitiated.length +
    returnCompleted.length;

  totalCount = canceledItems[0].items.length ? totalCount + 1 : totalCount;

  const storeAddress = !isEcomOrder ? shippingAddress : '';

  const displayUpcomingOrdersView = displayUpcomingOrderView(
    upcomingOrdersView,
    getLabelValue(labels, orderStatus, 'orders')
  );

  const showNotification = showNotificationMessage(
    totalCount,
    canceledItems,
    isEcomOrder,
    expiredItemShipment
  );

  const shipments = buildShipments(
    totalCount,
    purchasedItems,
    [...processingShipment, ...orderBeingProcessedShipment],
    shippedShipment,
    pickedUpShipment,
    readyForPickUpShipment,
    {
      canceledItems,
      preparingItemShipment,
      deliveredItemShipment,
      inTransitItemShipment,
      outForDeliveryItemShipment,
      returnInitiated,
      returnCompleted,
    }
  );

  const totalCountForAccordian = shipments?.length;
  totalCount = getTotalCount(
    shipments?.length,
    canceledItems?.[0]?.items?.length,
    returnInitiated?.length,
    returnCompleted?.length
  );

  const shipmentInfoObj = {
    orderStatus,
    labels,
    isEcomOrder,
    displayUpcomingOrdersView,
    isMobile,
    orderTrackingUrl,
    orderSummary,
    totalCount,
    isBOSSOrder,
    orderDate,
    expectedDeliveryDate,
  };
  const refundEligibleShipmentStatus = {
    deliveredItemShipment,
    returnInitiated,
    returnCompleted,
  };
  return (
    <>
      <div>
        <OrdersListItemHeader
          orderDate={orderDate}
          orderDateWithoutFormatting={orderDateWithoutFormatting}
          orderNumber={orderNumber}
          orderTotal={orderTotal}
          currencySymbol={currencySymbol}
          isEcomOrder={isEcomOrder}
          orderSummary={orderSummary}
          openStoreDirections={openStoreDirections}
          storeAddress={storeAddress}
          labels={labels}
          showNotification={showNotification}
          expiredItemShipment={expiredItemShipment.length}
          helpWithOrderLabels={helpWithOrderLabels}
          cshReturnItemOlderDays={cshReturnItemOlderDays}
          setOrderResendModalOpen={setOrderResendModalOpen}
          isCSHEnabled={isCSHEnabled}
          refundEligibleShipmentStatus={refundEligibleShipmentStatus}
        />
      </div>
      <div
        className={`${totalCountForAccordian > 2 ? 'accordian-wrap' : ''} ${
          isExpanded ? 'expanded' : ''
        }`}
      >
        {!!shipments &&
          shipments.map((obj, shipmentIndex) => {
            const { shipment, isCancelledItems } = obj;
            if (isCancelledItems) {
              shipmentInfoObj.displayUpcomingOrdersView = true;
            }
            return showShipmentInfo(
              shipmentInfoObj,
              shipment,
              isCancelledItems,
              shipmentIndex + 1,
              storeAddress,
              orderNumber
            );
          })}
      </div>
      {totalCountForAccordian > 2 && (
        <button
          onClick={toggleExpansion}
          className={`accordian-toggle-button ${isExpanded ? 'toggle-button-expanded' : ''}`}
        >
          <Image src={getIconPath('carousel-big-carrot')} />
        </button>
      )}
    </>
  );
};

OrderContainer.propTypes = {
  order: PropTypes.shape({}).isRequired,
  labels: PropTypes.shape({}).isRequired,
  upcomingOrdersView: PropTypes.bool,
};

OrderContainer.defaultProps = {
  upcomingOrdersView: false,
};

/**
 * This function component use for return the Order item list based on group
 * can be passed in the component.
 * @param otherProps - otherProps object used for showing Order Item list
 */
const OrderListWithDetails = ({
  className,
  ordersListItems,
  upcomingOrdersView,
  labels,
  helpWithOrderLabels,
  cshReturnItemOlderDays,
  setOrderResendModalOpen,
  isCSHEnabled,
}) => {
  return (
    <>
      {!!ordersListItems &&
        !!ordersListItems.length &&
        ordersListItems.map((order) => {
          return (
            <div className={className}>
              <OrderContainer
                order={order}
                labels={labels}
                upcomingOrdersView={upcomingOrdersView}
                helpWithOrderLabels={helpWithOrderLabels}
                cshReturnItemOlderDays={cshReturnItemOlderDays}
                setOrderResendModalOpen={setOrderResendModalOpen}
                isCSHEnabled={isCSHEnabled}
              />
            </div>
          );
        })}
    </>
  );
};
OrderListWithDetails.propTypes = {
  className: PropTypes.string,
  ordersListItems: PropTypes.shape({}).isRequired,
  labels: PropTypes.shape({}).isRequired,
  upcomingOrdersView: PropTypes.bool,
};

OrderListWithDetails.defaultProps = {
  className: '',
  upcomingOrdersView: false,
};
export default withStyles(OrderListWithDetails, styles);
export { OrderListWithDetails };
