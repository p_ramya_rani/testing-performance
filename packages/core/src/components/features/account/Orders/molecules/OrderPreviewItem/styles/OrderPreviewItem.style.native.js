// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';
import { ViewWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';

const OrderItemContainer = styled.View`
  display: flex;
  flex-direction: row;
  padding-top: ${props => props.theme.spacing.ELEM_SPACING.XL};
`;

const OrderItemImage = styled.View`
  align-items: center;
  width: 33%;
`;

const OrderItemContent = styled(ViewWithSpacing)`
  width: 67%;
`;

const ItemContentWrapper = styled(ViewWithSpacing)`
  display: flex;
  flex-direction: row;
`;

const ImageStyle = styled.View`
  margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.SM};
  height: 100px;
`;

const ImageBrandStyle = styled.Image`
  width: 65px;
  height: 22px;
  /* stylelint-disable-next-line */
  resize-mode: contain;
`;

export {
  ImageStyle,
  ImageBrandStyle,
  OrderItemContainer,
  OrderItemImage,
  ItemContentWrapper,
  OrderItemContent,
};
