// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { OrdersListItemHeaderVanilla } from '../views/OrdersListItemHeader.view';

describe('OrdersListItem component', () => {
  it('should renders correctly', () => {
    const props = {
      labels: {},
      orderItem: {
        orderTotal: 10.5,
      },
    };
    const component = shallow(<OrdersListItemHeaderVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should render notification', () => {
    const props = {
      labels: {},
      orderItem: {
        orderTotal: 10.5,
      },
      showNotification: true,
    };
    const component = shallow(<OrdersListItemHeaderVanilla {...props} />);
    expect(component.find('.notification')).toHaveLength(1);
  });

  it('should not render notification', () => {
    const props = {
      labels: {},
      orderItem: {
        orderTotal: 10.5,
      },
      showNotification: false,
    };
    const component = shallow(<OrdersListItemHeaderVanilla {...props} />);
    expect(component.find('.notification')).toHaveLength(0);
  });
  it('should renders modal correctly', () => {
    const component = shallow(<OrdersListItemHeaderVanilla isHelpWithOrderOpen />);
    expect(component).toMatchSnapshot();
  });
});
