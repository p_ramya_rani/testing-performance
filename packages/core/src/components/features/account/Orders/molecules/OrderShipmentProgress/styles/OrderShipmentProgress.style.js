// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  .progress {
    margin: 22px 20px ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    ${(props) => (props.showEdd ? `margin-top:50px;` : '')}
  }
  .progress .heading {
    margin-bottom: 8px;
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy3}px;
    left: -20px;
    bottom: 90%;
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    color: ${(props) => props.theme.colorPalette.gray['600']};
    position: absolute;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy2}px;
      max-width: 60px;
      white-space: break-spaces;
      text-align: center;
    }
    ${(props) => (props.showEdd ? `bottom:120%;text-align:center;` : '')}
  }
  .step_2 .heading,
  .step_4 .heading {
    left: -30px;
  }
  .list-container,
  .checkpoint {
    white-space: nowrap;
    text-align: center;
    position: relative;
    display: flex;
    width: 100%;
    flex: 1;
    max-width: 534px;
  }
  .last-step {
    flex: 0;
  }
  .checkpoint .bullet-step {
    text-align: center;
    background: ${(props) => props.theme.colorPalette.gray[500]};
    width: 22px;
    height: 22px;
    color: ${(props) => props.theme.colorPalette.gray[500]};
    border-radius: 50%;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
      height: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    }
  }
  .bullet-step::after {
    content: '';
    background: ${(props) => props.theme.colorPalette.gray[500]};
    height: 6px;
    width: 100%;
    display: block;
    position: absolute;
    top: 40%;
    left: -50%;
    z-index: 0;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      height: 3px;
      top: 40%;
    }
  }
  .reached .bullet-step,
  .reached .bullet-step::after {
    background: ${(props) => props.theme.colorPalette.blue.C900};
    color: ${(props) => props.theme.colorPalette.blue.C900};
    z-index: 1;
  }
  .first-step .bullet-step::after {
    left: 1px;
    width: 61%;
  }
  .last-step .bullet-step::after {
    left: -200px;
    z-index: 0;
    width: 200px;
  }
  .reached .heading {
    color: ${(props) => props.theme.colorPalette.gray['900']};
  }
  .edd {
    text-align: center;
    color: ${(props) => props.theme.colorPalette.gray['600']};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      min-width: 70px;
      font-size: 11px;
      left: -12%;
      position: relative;
    }
  }
  .placeholder {
    text-align: center;
    opacity: 0;
    height: 19px;
    min-width: 78px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      min-width: 70px;
      font-size: 11px;
      left: -12%;
      position: relative;
    }
  }
  .in-transit-icon {
    display: flex;
    justify-content: center;
    img {
      height: 20px;
      width: 30px;
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        height: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
        width: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
      }
    }
  }
`;

export default styles;
