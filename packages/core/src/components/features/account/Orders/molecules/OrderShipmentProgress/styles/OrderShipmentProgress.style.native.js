// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components';
import { BodyCopy } from '../../../../../../common/atoms';

const getLeftBar = (index, isReached, colorPalette) => {
  if (index === 0) {
    return colorPalette.white;
  }
  return isReached ? colorPalette.blue.C900 : colorPalette.gray[500];
};

const getRightBar = (index, isReached, totalCount, colorPalette) => {
  if (index + 1 === totalCount) {
    return colorPalette.white;
  }
  return isReached ? colorPalette.blue.C900 : colorPalette.gray[500];
};

const OrderProgressWrapper = styled.View`
  margin: -${(props) => props.theme.spacing.ELEM_SPACING.XS} 0px ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
  flex-direction: row;
  width: 100%;
`;

const ProgressStatusList = styled.View`
  align-items: center;
  flex: 1;
  width: 100%;
`;

const ProgressBar = styled.View`
  flex-direction: row;
  position: absolute;
  bottom: ${(props) => (props.isSingleLine ? '13%' : '10%')};
  width: 100%;
`;

const ProgressStep = styled(BodyCopy)`
  width: 70px;
  flex-wrap: wrap;
  margin-bottom: 8px;
  min-height: ${(props) => (props.isSingleLine ? '0px' : '30px')};
  text-align: center;
`;

const ProgressLeftBar = styled(BodyCopy)`
  background-color: ${(props) =>
    getLeftBar(props.index, props.isReached, props.theme.colorPalette)};
  height: 3;
  width: 50%;
  align-items: center;
`;

const ProgressRightBar = styled(BodyCopy)`
  background-color: ${(props) =>
    getRightBar(props.index, props.isReached, props.totalCount, props.theme.colorPalette)};
  height: 3;
  width: 50%;
  align-items: center;
`;

const ProgressBarIcon = styled(BodyCopy)`
  width: 12;
  height: 12;
  border-radius: 6;
  background-color: ${(props) =>
    props.isReached ? props.theme.colorPalette.blue.C900 : props.theme.colorPalette.gray[500]};
  overflow: hidden;
  z-index: 1;
`;

export {
  OrderProgressWrapper,
  ProgressStatusList,
  ProgressBar,
  ProgressStep,
  ProgressLeftBar,
  ProgressRightBar,
  ProgressBarIcon,
};
