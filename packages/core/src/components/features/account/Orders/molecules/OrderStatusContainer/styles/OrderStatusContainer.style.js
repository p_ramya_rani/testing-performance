// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  .store-section {
    display: flex;
    align-items: center;
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy3}px;
  }
  .store {
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    cursor: pointer;
    text-decoration: underline;
    color: ${(props) => props.theme.colorPalette.blue['800']};
    font-weight: ${(props) => props.theme.typography.fontWeights.bold};
    font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy3}px;
    margin-left: 2px;
  }
  .store-details-anchor {
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    height: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    width: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    cursor: pointer;
  }
  .order-status-text {
    .return-status {
      text-transform: none;
    }
  }

  .order-status-logo {
    .return-status-logo {
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
      }
    }
  }
`;

export default styles;
