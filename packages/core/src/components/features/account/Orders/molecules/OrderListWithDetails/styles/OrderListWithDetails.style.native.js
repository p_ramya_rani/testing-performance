// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components';
import { Anchor, BodyCopy } from '@tcp/core/src/components/common/atoms';

const OrderListWrapper = styled.View`
  border-width: 0.5px;
  border-color: rgba(0, 0, 0, 0.15);
  margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.MED};
  background-color: ${props => props.theme.colors.WHITE};
`;

const OrderLogoAndStatus = styled.View`
  flex-direction: row;
`;

const OrderStatusAccordian = styled.View`
  ${props =>
    props.isShowAccordion
      ? `overflow: hidden;
    ${!props.isExpanded ? `max-height: ${props.theme.spacing.LAYOUT_SPACING.XXXL};` : ''}`
      : ''}
`;

const OrderItemsWrapper = styled.View`
  flex-direction: row;
  padding: 0 ${props => props.theme.spacing.ELEM_SPACING.MED};
`;
const OrderItemImage = styled.View`
  margin: 15px ${props => props.theme.spacing.ELEM_SPACING.XS} 25px 0;
  opacity: ${props => (props.isCancelledItems ? 0.5 : 1)};
`;

const QuantityWrapper = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
  background-color: ${props => props.theme.colorPalette.blue.E100};
  width: ${props => props.theme.spacing.ELEM_SPACING.MED};
  height: ${props => props.theme.spacing.ELEM_SPACING.MED};
  border-radius: ${props => props.theme.spacing.ELEM_SPACING.XS};
  overflow: hidden;
  position: absolute;
  bottom: 0;
  right: 0;
`;

const MoreOverlay = styled(BodyCopy)`
  position: absolute;
  top: 35%;
  left: 35%;
`;

const AccordionButton = styled(Anchor)`
  width: 100%;
  text-align: center;
  align-items: center;
  justify-content: center;
`;

const AccordionContent = styled.View`
  align-items: center;
  height: ${props => props.theme.spacing.ELEM_SPACING.XL};
  width: 100%;
  justify-content: center;
  box-shadow: 0 -${props => props.theme.spacing.ELEM_SPACING.XXXS} ${props =>
      props.theme.spacing.ELEM_SPACING.XXS} rgba(0, 0, 0, 0.16);
  background-color: ${props => props.theme.colors.WHITE};
`;

const StyledTrackAnchor = styled(Anchor)`
  background-color: ${props => props.theme.colors.BLACK};
  padding: 9px 34px;
  align-items: center;
  border-radius: 6px;
  overflow: hidden;
  text-transform: uppercase;
  font-size: ${props => props.theme.fonts.fontSize.body.bodytext.copy1}px;
  margin-left: auto;
  margin-right: ${props => props.theme.spacing.ELEM_SPACING.SM};
`;

const TrackOrderContainer = styled.View`
  position: relative;
  top: -${props => props.theme.spacing.ELEM_SPACING.XS};
  margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.SM};
  display: flex;
  align-items: center;
  flex-direction: row;
  justify-content: space-between;
`;

const StyledTrackNumberAnchor = styled(Anchor)`
  text-transform: uppercase;
  font-size: ${props => props.theme.fonts.fontSize.body.bodytext.copy1}px;
  margin-left: auto;
  color: ${props => props.theme.colorPalette.gray[900]};
`;

const OrderTrackingNumber = styled.View`
  max-width: 190px;
  line-height: 1;
  margin-left: ${props => props.theme.spacing.ELEM_SPACING.SM};
`;

export {
  OrderListWrapper,
  OrderStatusAccordian,
  OrderItemsWrapper,
  OrderItemImage,
  QuantityWrapper,
  MoreOverlay,
  AccordionButton,
  AccordionContent,
  OrderLogoAndStatus,
  StyledTrackAnchor,
  TrackOrderContainer,
  StyledTrackNumberAnchor,
  OrderTrackingNumber,
};
