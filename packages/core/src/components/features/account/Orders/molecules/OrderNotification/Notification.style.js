// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const NotificationStyle = css`
  align-items: flex-start;
  display: flex;
  font-size: 16px;
  background-color: ${props => props.theme.colors.PRIMARY.PALEGRAY};
  @media ${props => props.theme.mediaQuery.smallOnly} {
    font-size: 12px;
  }
  .notification__image {
    height: 24px;
  }
  b {
    font-weight: 800;
  }
`;
export default NotificationStyle;
