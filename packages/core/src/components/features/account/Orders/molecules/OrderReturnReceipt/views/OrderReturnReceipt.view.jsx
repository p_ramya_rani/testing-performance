// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { Row, Col, Image, BodyCopy } from '@tcp/core/src/components/common/atoms';
import Barcode from '@tcp/core/src/components/common/molecules/Barcode';
import {
  getLabelValue,
  convertToISOString,
  getFormatedOrderDate,
  getFormatedOrderTime,
} from '@tcp/core/src/utils/utils';
import { getIconPath, getPaymentDetails } from '@tcp/core/src/utils';
import { convertHtml } from '@tcp/core/src/components/features/CnC/LoyaltyBanner/util/utility';
import {
  showShipmentBlock,
  isOrderPickedUp,
  isValidReceiptItem,
} from '../container/OrderReturnReceipt.utils';
import styles from '../styles/OrderReturnReceipt.style';

const PageShipmentCount = 2;
class OrderReturnReceipt extends PureComponent {
  /**
   * @method getProductInfo
   * @description render product information row for the item param
   * @param {*} item - product information
   */
  getProductInfo = (item, isShipment) => {
    const {
      productInfo: { name, variantNo },
      itemInfo: { offerPrice, quantity, quantityShipped },
    } = item;
    const { ordersLabels } = this.props;
    const qty = isShipment ? quantityShipped : quantity;

    return (
      <Row className="product-info-container">
        <Row className="product-info">
          <Col colSize={{ small: 6, medium: 8, large: 12 }}>{this.getReturnCopyText(name)}</Col>
        </Row>
        <Row className="product-info">
          <Col colSize={{ small: 4, medium: 5, large: 8 }}>
            <BodyCopy
              component="p"
              color="gray.900"
              fontFamily="secondary"
              fontSize="fs14"
              textAlign="center"
              className="product-detail"
            >
              {`${getLabelValue(ordersLabels, 'lbl_orderDetails_itemnumber')} ${variantNo}`}
            </BodyCopy>
          </Col>
          <Col colSize={{ small: 2, medium: 3, large: 4 }} className="price-info">
            {qty && qty > 1 && (
              <BodyCopy
                component="p"
                color="gray.900"
                fontFamily="secondary"
                fontSize="fs14"
                fontWeight="semibold"
                textAlign="center"
                className="product-detail"
              >
                {`${qty} X`}
              </BodyCopy>
            )}
            <BodyCopy
              component="p"
              color="gray.900"
              fontFamily="secondary"
              fontSize="fs14"
              fontWeight="semibold"
              textAlign="center"
              className="product-detail"
            >
              {`$${offerPrice}`}
            </BodyCopy>
          </Col>
        </Row>
      </Row>
    );
  };

  /**
   * @method renderReturnMessage
   * @description render info message, for instance covid message for return days.
   */
  renderReturnMessage = () => {
    const { ordersLabels } = this.props;
    return (
      <>
        <Row className="order-return-message-header">
          <Col colSize={{ small: 6, medium: 8, large: 12 }}>
            <BodyCopy
              component="p"
              color="gray.900"
              fontFamily="secondary"
              fontSize="fs15"
              fontWeight="black"
              textAlign="left"
              className="product-detail"
            >
              {`${getLabelValue(ordersLabels, 'lbl_order_receipt_message_heading')}`}
            </BodyCopy>
          </Col>
        </Row>
        <Row className="order-return-message-info">
          <Col colSize={{ small: 6, medium: 8, large: 12 }}>
            <BodyCopy
              component="p"
              color="gray.900"
              fontFamily="secondary"
              fontSize="fs14"
              textAlign="left"
              className="product-detail"
            >
              {`${getLabelValue(ordersLabels, 'lbl_order_receipt_message_information')}`}
            </BodyCopy>
          </Col>
        </Row>
      </>
    );
  };

  /**
   * @method renderShipmentOrderInfo
   * @description render shipment blocks for the receipt
   * @param {*} orderGroup - items for particular shipment
   * @param {*} orderNumber
   * @param {*} index
   */
  renderShipmentOrderInfo = (orderGroup, orderNumber, index) => {
    const { items } = orderGroup;
    const {
      orderDetailsData: { orderStatus },
      ordersLabels,
    } = this.props;
    return (
      <div className="order-shipment-wrapper">
        <div className="shipment-index-container">
          <BodyCopy
            component="p"
            color="gray.900"
            fontFamily="secondary"
            fontSize="fs14"
            fontWeight="semibold"
            textAlign="center"
            className="product-detail"
          >
            {isOrderPickedUp(orderStatus)
              ? `${getLabelValue(ordersLabels, 'lbl_orders_statusItemsPickedUp')}`
              : `${getLabelValue(ordersLabels, 'lbl_orderDetails_shipment')} ${index + 1}`}
          </BodyCopy>
        </div>
        <div className="order-shipment-container">
          <BodyCopy component="div" className="orderdetails-barcode" textAlign="center">
            <Barcode
              className="barcode"
              value={orderNumber}
              barcodeId={orderNumber}
              height={47}
              displayValue={false}
              renderer="svg"
              width="2.5"
            />
          </BodyCopy>
          {items && items.map((item) => this.getProductInfo(item, true))}
        </div>
      </div>
    );
  };

  getReturnCopyText = (text) => {
    return (
      <BodyCopy
        component="p"
        color="gray.900"
        fontFamily="secondary"
        fontSize="fs14"
        fontWeight="extrabold"
        textAlign="center"
        className="product-detail"
      >
        {text}
      </BodyCopy>
    );
  };

  getReturnItemsCopyText = (text) => {
    return (
      <BodyCopy
        component="p"
        color="gray.900"
        fontFamily="secondary"
        fontSize="fs16"
        fontWeight="extrabold"
        textAlign="center"
        className="product-detail elem-mb-XXS underline"
      >
        {text}
      </BodyCopy>
    );
  };

  getReturnValueText = (text) => {
    return (
      <BodyCopy
        component="p"
        color="gray.900"
        fontFamily="secondary"
        fontSize="fs14"
        textAlign="center"
        className="product-detail"
      >
        {text}
      </BodyCopy>
    );
  };

  /**
   * @method renderHeaderOrderDetails
   * @description render receipt header information - title, order number, dates
   * @param {*} orderDetailsData
   */
  renderHeaderOrderDetails = (orderDetailsData) => {
    const { ordersLabels, orderReturnDays, userAccountId, isLoggedIn } = this.props;
    const { orderNumber, orderDate } = orderDetailsData || {};

    const orderPlacedDate = new Date(convertToISOString(orderDate));
    const orderDateParsed = new Date(orderPlacedDate);
    orderDateParsed.setDate(orderDateParsed.getDate() + orderReturnDays);
    const paymentDetails = getPaymentDetails(orderDetailsData);
    const paymentText = paymentDetails ? paymentDetails.join(', ') : '';
    return (
      <Row fullBleed className="order-return-header-container">
        <Row className="order-return-header">
          {this.getReturnCopyText(
            `${getLabelValue(ordersLabels, 'lbl_orderDetails_orderNumber')}:`
          )}
          {this.getReturnValueText(orderNumber)}
        </Row>
        <Row className="order-return-header">
          {this.getReturnCopyText(`${getLabelValue(ordersLabels, 'lbl_orders_orderDate')}:`)}
          <BodyCopy
            component="p"
            color="gray.900"
            fontFamily="secondary"
            fontSize="fs14"
            textAlign="center"
            className="product-detail"
          >
            {getFormatedOrderDate(orderPlacedDate)}
            {
              <>
                <BodyCopy
                  component="span"
                  fontSize="fs14"
                  className="elem-mr-XXS elem-ml-XXS"
                  fontFamily="secondary"
                >
                  {getLabelValue(ordersLabels, 'lbl_orderDetails_at')}
                </BodyCopy>
                <BodyCopy component="span" fontSize="fs14" fontFamily="secondary">
                  {getFormatedOrderTime(orderPlacedDate)}
                </BodyCopy>
              </>
            }
          </BodyCopy>
        </Row>
        {paymentText && (
          <Row className="order-return-header">
            {this.getReturnCopyText(`${getLabelValue(ordersLabels, 'lbl_order_return_payment')}:`)}
            {this.getReturnValueText(paymentText)}
          </Row>
        )}
        {isLoggedIn && (
          <Row className="order-return-header">
            {this.getReturnCopyText(`${getLabelValue(ordersLabels, 'lbl_order_return_member')}:`)}
            {this.getReturnValueText(userAccountId)}
          </Row>
        )}
        {orderReturnDays > 0 && (
          <>
            <Row className="order-return-header">
              {this.getReturnCopyText(
                `${getLabelValue(ordersLabels, 'lbl_orderDetails_return_by')}:`
              )}
              <Col colSize={{ small: 4, medium: 6, large: 10 }}>
                <BodyCopy
                  component="p"
                  color="gray.900"
                  fontFamily="secondary"
                  fontSize="fs14"
                  textAlign="center"
                  className="product-detail"
                >
                  {getFormatedOrderDate(orderDateParsed)}
                </BodyCopy>
              </Col>
            </Row>
          </>
        )}
        {!orderReturnDays && this.renderReturnMessage()}
      </Row>
    );
  };

  renderPagingIndex = (page, totalPage) => {
    return (
      <>
        <div className="page-footer-index">
          <BodyCopy component="span" fontSize="fs14" fontFamily="secondary">
            {`Page ${page} of ${totalPage}`}
          </BodyCopy>
        </div>
      </>
    );
  };

  isPagingRequired = (purchasedItems) =>
    purchasedItems && purchasedItems.length > PageShipmentCount;

  getTotalPage = (purchasedItems) =>
    purchasedItems ? Math.round(purchasedItems.length / PageShipmentCount) : 1;

  renderDetailsWithPaging = (orderDetailsData, purchasedItems) => {
    const { orderNumber } = orderDetailsData || {};
    const updatedPurchasedItems = purchasedItems.slice(PageShipmentCount, purchasedItems.length);
    return (
      <>
        <div className="order-return-wrapper add-top-margin">
          <div className="order-return-container add-bottom-padding">
            {updatedPurchasedItems.map((orderGroup, index) => {
              return this.renderOrderDetails(orderGroup, orderNumber, index + PageShipmentCount);
            })}
          </div>
        </div>
      </>
    );
  };

  getReturnedItems = (orderGroup) => {
    const { items } = orderGroup;
    if (items) return items.map((item) => this.getProductInfo(item, false));
    return null;
  };

  renderReturnedItems = (returnedItems, headingLabel) => (
    <div className="order-process-wrapper">
      <Row className="product-info-container">
        <Row className="product-info">
          <Col colSize={{ small: 6, medium: 8, large: 12 }}>
            {this.getReturnItemsCopyText(headingLabel)}
          </Col>
        </Row>
      </Row>
      {returnedItems.map((items) => this.getReturnedItems(items))}
    </div>
  );

  /**
   * @method renderOrderDetails
   * @description decides to whether render shipment block or individual products when they are not
   * shipped yet
   * @param {*} orderGroup
   * @param {*} orderNumber
   * @param {*} index
   */
  renderOrderDetails = (orderGroup, orderNumber, index) => {
    const { items, orderStatus } = orderGroup;
    const { orderDetailsData } = this.props;
    if (showShipmentBlock(orderDetailsData, orderGroup)) {
      return this.renderShipmentOrderInfo(orderGroup, orderNumber, index);
    }
    const { isBossOrder, isBopisOrder } = orderDetailsData;
    if (isBossOrder || isBopisOrder) {
      return (
        <div className="order-process-wrapper">
          {items && orderStatus && items.map((item) => this.getProductInfo(item, false))}
        </div>
      );
    }
    if (isValidReceiptItem(orderDetailsData, orderGroup)) {
      return (
        <div className="order-process-wrapper">
          {items && items.map((item) => this.getProductInfo(item, false))}
        </div>
      );
    }
    return false;
  };

  render() {
    const { className, orderDetailsData, ordersLabels } = this.props;
    const {
      purchasedItems,
      orderNumber,
      purchasedItemsAll,
      returnedInitiatedItems,
      returnedItems,
    } = orderDetailsData || {};
    const allItems =
      purchasedItemsAll && purchasedItemsAll.length ? purchasedItemsAll : purchasedItems;
    return (
      <div className={className}>
        <div className="order-return-wrapper">
          <div className="order-return-container">
            <div className="return-brand">
              <Image
                className="brand-icon"
                aria-hidden="true"
                src={getIconPath(`tcp-gym-black-logo`)}
              />
            </div>
            <BodyCopy
              component="p"
              color="gray.900"
              fontFamily="secondary"
              fontSize="fs16"
              fontWeight="extrabold"
              textAlign="center"
              className="order-return-title"
            >
              {getLabelValue(ordersLabels, 'lbl_orders_return_receipt')}
            </BodyCopy>
            {this.renderHeaderOrderDetails(orderDetailsData)}
            {allItems &&
              allItems.length > 0 &&
              allItems.map((items, index) => this.renderOrderDetails(items, orderNumber, index))}
            {returnedInitiatedItems?.length > 0 &&
              this.renderReturnedItems(
                returnedInitiatedItems,
                getLabelValue(ordersLabels, 'lbl_orders_returns_initiated')
              )}

            {returnedItems?.length > 0 &&
              this.renderReturnedItems(
                returnedItems,
                getLabelValue(ordersLabels, 'lbl_orders_returns_completed')
              )}
            <div className="order-return-address-container">
              <Row className="order-return-address-header">
                <Col colSize={{ small: 6, medium: 8, large: 12 }}>
                  <BodyCopy
                    component="p"
                    color="gray.900"
                    fontFamily="secondary"
                    fontSize="fs15"
                    fontWeight="black"
                    textAlign="left"
                    className="product-detail"
                  >
                    {' '}
                    {`${getLabelValue(ordersLabels, 'lbl_return_address_receipt_us_heading')}`}
                  </BodyCopy>
                </Col>
              </Row>
              <Row className="order-return-address-info">
                <Col colSize={{ small: 6, medium: 8, large: 10 }}>
                  <BodyCopy
                    component="p"
                    color="gray.900"
                    fontFamily="secondary"
                    fontSize="fs14"
                    textAlign="left"
                    className="product-detail"
                  >
                    {' '}
                    {convertHtml(getLabelValue(ordersLabels, 'lbl_return_address_receipt_us'))}
                  </BodyCopy>
                </Col>
              </Row>
              <Row className="order-return-address-header">
                <Col colSize={{ small: 6, medium: 8, large: 12 }}>
                  <BodyCopy
                    component="p"
                    color="gray.900"
                    fontFamily="secondary"
                    fontSize="fs15"
                    fontWeight="black"
                    textAlign="left"
                    className="product-detail"
                  >
                    {`${getLabelValue(ordersLabels, 'lbl_return_address_receipt_ca_heading')}`}
                  </BodyCopy>
                </Col>
              </Row>
              <Row className="order-return-address-info">
                <Col colSize={{ small: 6, medium: 8, large: 12 }}>
                  <BodyCopy
                    component="p"
                    color="gray.900"
                    fontFamily="secondary"
                    fontSize="fs14"
                    textAlign="left"
                    className="product-detail"
                  >
                    {convertHtml(getLabelValue(ordersLabels, 'lbl_return_address_receipt_ca'))}
                  </BodyCopy>
                </Col>
              </Row>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

OrderReturnReceipt.propTypes = {
  className: PropTypes.string,
  orderDetailsData: PropTypes.shape({}),
  ordersLabels: PropTypes.shape({}),
  orderReturnDays: PropTypes.number,
  userAccountId: PropTypes.string,
  isLoggedIn: PropTypes.bool,
};

OrderReturnReceipt.defaultProps = {
  className: '',
  ordersLabels: {},
  orderDetailsData: {},
  orderReturnDays: 0,
  userAccountId: '',
  isLoggedIn: false,
};

export default withStyles(OrderReturnReceipt, styles);
export { OrderReturnReceipt as OrderReturnReceiptVanilla };
