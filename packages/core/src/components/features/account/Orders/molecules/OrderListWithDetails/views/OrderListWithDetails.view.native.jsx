// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { FlatList, View } from 'react-native';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import createThemeColorPalette from '@tcp/core/styles/themes/createThemeColorPalette';
import { BodyCopy, Image, DamImage } from '@tcp/core/src/components/common/atoms';
import {
  getOrderStatus,
  showLogoAndStatusCheck,
} from '@tcp/core/src/components/features/account/Orders/molecules/OrderStatusContainer/views/OrderStatusContainer.utils';
import { shouldRenderStoreData } from '@tcp/core/src/components/features/account/Orders/molecules/OrderStatusContainer/views/OrderStatusContainer.view.native';
import OrdersListItemHeader from '../../OrdersListItemHeader/views/OrdersListItemHeader.view';
import OrderShipmentProgress from '../../OrderShipmentProgress/views/OrdersShipmentProgress.view';
import OrderStatusContainer from '../../OrderStatusContainer/views/OrderStatusContainer.view.native';
import {
  getProgressBarSteps,
  displayUpcomingOrderView,
  showNotificationMessage,
  getCancelledItemsLength,
  VISIBLE_IMAGE_COUNT,
  buildShipments,
  getQuantityToShow,
  getItemsWithTargetStatus,
  getIsItemShipped,
  getTotalCount,
} from './OrderListWithDetails.utils';
import {
  OrderListWrapper,
  OrderStatusAccordian,
  OrderItemsWrapper,
  OrderItemImage,
  QuantityWrapper,
  MoreOverlay,
  AccordionButton,
  AccordionContent,
  StyledTrackAnchor,
  TrackOrderContainer,
  StyledTrackNumberAnchor,
  OrderTrackingNumber,
} from '../styles/OrderListWithDetails.style.native';
import constants from '../../../Orders.constants';

const { STATUS_CONSTANTS = {} } = constants;
const {
  SHIPPED,
  ITEMS_PICKED_UP,
  ITEM_READY_FOR_PICK_UP,
  PREPARING_YOUR_ORDER,
  PROCESSING,
  PICKUP_EXPIRED,
  DELIVERED,
  IN_TRANSIT,
  OUT_FOR_DELIVERY,
  RETURN_INITIATED,
  RETURN_COMPLETED,
} = STATUS_CONSTANTS;

const downArrow = require('../../../../../../../../../mobileapp/src/assets/images/right-carousel-caret.png');

const colorPalette = createThemeColorPalette();

const getTrimmedTrackingNum = (orderTrackingNumber) =>
  orderTrackingNumber && orderTrackingNumber.toUpperCase().trim();

const processSteps = (progressSteps, status) => (
  <OrderShipmentProgress
    progressSteps={progressSteps}
    isReturnOrder={status === RETURN_COMPLETED || status === RETURN_INITIATED}
  />
);

export const showShipmentInfo = (
  {
    orderStatus,
    labels,
    isEcomOrder,
    isUpcomingOrdersView,
    orderTrackingUrl,
    totalCount,
    orderSummary,
    isBOSSOrder,
    expectedDeliveryDate,
  },
  shipmentsObj = [],
  isCancelledItems = false,
  shipmentIndex = 1,
  storeAddress = {}
) => {
  return (
    shipmentsObj.length > 0 &&
    shipmentsObj.map((shipment) => {
      const {
        items,
        shipmentStatus: orderShipmentStatus = orderStatus,
        trackingUrl = orderTrackingUrl,
        shippedDate,
        deliveryDate,
        carrierDeliveryDate,
        packageStatus,
        orderTracking: orderTrackingNumber,
      } = shipment || {};
      const itemsCount = items.length;

      if (!items || !itemsCount) {
        return null;
      }
      const statusText = getLabelValue(labels, orderShipmentStatus, 'orders');
      const progressSteps = getProgressBarSteps(labels, statusText, isEcomOrder, isBOSSOrder);
      const isPartiallyCancelled = isCancelledItems && shipmentIndex > 1;
      const upcomingOrdersView = displayUpcomingOrderView(
        isUpcomingOrdersView,
        statusText,
        isCancelledItems
      );

      const status = statusText.toLowerCase();
      const isOrderSuccess = getOrderStatus(status);
      const showLogoAndStatus = showLogoAndStatusCheck(
        isUpcomingOrdersView,
        isPartiallyCancelled,
        isEcomOrder,
        isOrderSuccess,
        status
      );
      const orderTracking = getTrimmedTrackingNum(orderTrackingNumber);
      const isItemShipped = getIsItemShipped(statusText);

      return (
        <>
          <OrderStatusContainer
            isUpcomingOrdersView={upcomingOrdersView}
            orderShipmentStatus={orderShipmentStatus}
            labels={labels}
            isEcomOrder={isEcomOrder}
            totalCount={totalCount}
            index={shipmentIndex}
            orderSummary={orderSummary}
            isPartiallyCancelled={isPartiallyCancelled}
            storeAddress={storeAddress}
            isCancelledItems={isCancelledItems}
            isItemShipped={isItemShipped}
            expectedDeliveryDate={expectedDeliveryDate}
            deliveryDate={deliveryDate}
            carrierDeliveryDate={carrierDeliveryDate}
            shipDate={shippedDate}
            packageStatus={packageStatus}
          />
          <OrderItemsWrapper>
            {items.map((item, imageIndex) => {
              if (imageIndex > VISIBLE_IMAGE_COUNT) return null;
              const {
                itemInfo: { quantity = 0, quantityCanceled = 0, itemBrand } = {},
                productInfo: { thumbnailImgPath },
                trackingInfo = [],
              } = item;
              const isLastVisibleImage = imageIndex === VISIBLE_IMAGE_COUNT;
              const ImageStyle = {
                opacity: isLastVisibleImage ? 0.15 : 1,
                width: 60,
                height: 70,
                backgroundColor: colorPalette.white,
              };
              const quantityToShow = getQuantityToShow(
                quantity,
                quantityCanceled,
                isCancelledItems,
                statusText,
                trackingInfo
              );

              return (
                <OrderItemImage isCancelledItems={isCancelledItems}>
                  <DamImage
                    customStyle={ImageStyle}
                    imgConfig="t_plp_img_m"
                    url={thumbnailImgPath}
                    isProductImage
                    isFastImage
                    resizeMode="contain"
                    data-locator="order_item_image"
                    key={imageIndex.toString()}
                    itemBrand={itemBrand}
                  />
                  {quantityToShow && quantityToShow > 1 && !isLastVisibleImage ? (
                    <QuantityWrapper>
                      <BodyCopy
                        fontFamily="secondary"
                        color="white"
                        fontSize="fs10"
                        text={quantityToShow}
                      />
                    </QuantityWrapper>
                  ) : null}
                  {isLastVisibleImage && (
                    <MoreOverlay
                      fontFamily="secondary"
                      color="black"
                      fontSize="fs14"
                      fontWeight="bold"
                      text={`+${itemsCount - VISIBLE_IMAGE_COUNT}`}
                    />
                  )}
                </OrderItemImage>
              );
            })}
          </OrderItemsWrapper>
          {!upcomingOrdersView && (
            <>
              {processSteps(progressSteps, status)}

              {statusText.toLowerCase() !== RETURN_INITIATED &&
                statusText.toLowerCase() !== RETURN_COMPLETED &&
                isEcomOrder && (
                  <TrackOrderContainer>
                    <View>
                      {orderTracking && orderTracking !== 'N/A' && (
                        <OrderTrackingNumber>
                          <BodyCopy
                            fontSize="fs12"
                            fontFamily="secondary"
                            fontWeight="extrabold"
                            text={getLabelValue(labels, 'lbl_orders_trackingId', 'orders')}
                          />
                          <StyledTrackNumberAnchor
                            url={trackingUrl}
                            fontFamily="secondary"
                            fontSizeVariation="medium"
                            text={orderTracking}
                            underline
                          />
                        </OrderTrackingNumber>
                      )}
                    </View>
                    <StyledTrackAnchor
                      url={trackingUrl}
                      anchorVariation="white"
                      fontWeight="extrabold"
                      fontFamily="secondary"
                      fontSizeVariation="medium"
                      text={`${getLabelValue(labels, 'lbl_orderDetails_track', 'orders')}`}
                    />
                  </TrackOrderContainer>
                )}

              {shouldRenderStoreData(
                {
                  isEcomOrder,
                  showLogoAndStatus,
                  orderSummary,
                  labels,
                  storeAddress,
                  isCancelledItems,
                },
                true
              )}
            </>
          )}
        </>
      );
    })
  );
};

export const OrderContainer = ({
  order,
  labels,
  upcomingOrdersView,
  navigation,
  helpWithOrderLabels,
  cshReturnItemOlderDays,
  isCSHAppEnabled,
  setOrderResendModalOpen,
}) => {
  const [isExpanded, setIsExpanded] = useState(false);

  const toggleExpansion = () => {
    setIsExpanded(!isExpanded);
  };

  const {
    orderSummary = {},
    orderDate = '',
    orderDateWithoutFormatting = '',
    orderNumber = '',
    isEcomOrder = false,
    orderTrackingUrl = '',
    orderStatus = '',
    isBOSSOrder,
    expectedDeliveryDate,
  } = order;
  const {
    purchasedItems = [],
    canceledItems = [{ items: [] }],
    shippingAddress = {},
  } = orderSummary;
  const cancelledItemsLength = getCancelledItemsLength(canceledItems);
  if (!purchasedItems.length && !cancelledItemsLength) return null;
  let processingShipment = [];
  let shippedShipment = [];
  let pickedUpShipment = [];
  let readyForPickUpShipment = [];
  let preparingItemShipment = [];
  let deliveredItemShipment = [];
  let inTransitItemShipment = [];
  let outForDeliveryItemShipment = [];
  let returnInitiated = [];
  let returnCompleted = [];
  const expiredItemShipment = getItemsWithTargetStatus(canceledItems, PICKUP_EXPIRED);

  if (purchasedItems.length) {
    processingShipment = getItemsWithTargetStatus(purchasedItems, PROCESSING);
    shippedShipment = getItemsWithTargetStatus(purchasedItems, SHIPPED);
    pickedUpShipment = getItemsWithTargetStatus(purchasedItems, ITEMS_PICKED_UP);
    readyForPickUpShipment = getItemsWithTargetStatus(purchasedItems, ITEM_READY_FOR_PICK_UP);
    preparingItemShipment = getItemsWithTargetStatus(purchasedItems, PREPARING_YOUR_ORDER);
    deliveredItemShipment = getItemsWithTargetStatus(purchasedItems, DELIVERED);
    inTransitItemShipment = getItemsWithTargetStatus(purchasedItems, IN_TRANSIT);
    outForDeliveryItemShipment = getItemsWithTargetStatus(purchasedItems, OUT_FOR_DELIVERY);
    returnInitiated = getItemsWithTargetStatus(purchasedItems, RETURN_INITIATED);
    returnCompleted = getItemsWithTargetStatus(purchasedItems, RETURN_COMPLETED);
  }
  let totalCount =
    processingShipment.length +
    shippedShipment.length +
    pickedUpShipment.length +
    readyForPickUpShipment.length +
    preparingItemShipment.length +
    deliveredItemShipment.length +
    inTransitItemShipment.length +
    outForDeliveryItemShipment.length +
    returnInitiated.length +
    returnCompleted.length;

  totalCount = canceledItems[0].items.length ? totalCount + 1 : totalCount;

  const storeAddress = !isEcomOrder ? shippingAddress : '';

  const isUpcomingOrdersView = displayUpcomingOrderView(
    upcomingOrdersView,
    getLabelValue(labels, orderStatus, 'orders')
  );

  const showNotification = showNotificationMessage(
    totalCount,
    canceledItems,
    isEcomOrder,
    expiredItemShipment
  );

  const shipments = buildShipments(
    totalCount,
    purchasedItems,
    processingShipment,
    shippedShipment,
    pickedUpShipment,
    readyForPickUpShipment,
    {
      canceledItems,
      preparingItemShipment,
      deliveredItemShipment,
      inTransitItemShipment,
      outForDeliveryItemShipment,
      returnInitiated,
      returnCompleted,
    }
  );

  totalCount = getTotalCount(
    shipments?.length,
    canceledItems?.[0]?.items?.length,
    returnInitiated?.length,
    returnCompleted?.length
  );

  const shipmentInfoObj = {
    orderStatus,
    labels,
    isEcomOrder,
    isUpcomingOrdersView,
    orderTrackingUrl,
    orderSummary,
    totalCount,
    isBOSSOrder,
    expectedDeliveryDate,
  };

  const refundEligibleShipmentStatus = {
    deliveredItemShipment,

    returnInitiated,

    returnCompleted,
  };

  if (!shipments.length) return null;

  return (
    <OrderListWrapper>
      <OrdersListItemHeader
        orderDate={orderDate}
        orderDateWithoutFormatting={orderDateWithoutFormatting}
        orderNumber={orderNumber}
        isEcomOrder={isEcomOrder}
        orderSummary={orderSummary}
        labels={labels}
        navigation={navigation}
        showNotification={showNotification}
        expiredItemShipment={expiredItemShipment.length}
        helpWithOrderLabels={helpWithOrderLabels}
        cshReturnItemOlderDays={cshReturnItemOlderDays}
        isCSHAppEnabled={isCSHAppEnabled}
        setOrderResendModalOpen={setOrderResendModalOpen}
        refundEligibleShipmentStatus={refundEligibleShipmentStatus}
      />
      <OrderStatusAccordian isShowAccordion={totalCount > 2} isExpanded={isExpanded}>
        {shipments.map((obj, shipmentIndex) => {
          const { shipment, isCancelledItems } = obj;
          if (isCancelledItems) {
            shipmentInfoObj.isUpcomingOrdersView = true;
          }
          return showShipmentInfo(
            shipmentInfoObj,
            shipment,
            isCancelledItems,
            shipmentIndex + 1,
            storeAddress
          );
        })}
      </OrderStatusAccordian>
      {totalCount > 2 && (
        <AccordionButton
          fontSizeVariation="medium"
          fontFamily="secondary"
          anchorVariation="primary"
          noLink
          to="/#"
          onPress={toggleExpansion}
        >
          <AccordionContent>
            <Image
              style={{ transform: [{ rotate: isExpanded ? '180deg' : '0deg' }] }}
              height="10px"
              width="33px"
              source={downArrow}
            />
          </AccordionContent>
        </AccordionButton>
      )}
    </OrderListWrapper>
  );
};

OrderContainer.propTypes = {
  order: PropTypes.shape({}).isRequired,
  labels: PropTypes.shape({}).isRequired,
  upcomingOrdersView: PropTypes.bool,
  navigation: PropTypes.func.isRequired,
};

OrderContainer.defaultProps = {
  upcomingOrdersView: false,
};

/**
 * This function component use for return the Order item list based on group
 * can be passed in the component.
 * @param otherProps - otherProps object used for showing Order Item list
 */
const OrderListWithDetails = ({
  ordersListItems,
  upcomingOrdersView,
  labels,
  navigation,
  helpWithOrderLabels,
  cshReturnItemOlderDays,
  isCSHAppEnabled,
  setOrderResendModalOpen,
}) => {
  return (
    <>
      {ordersListItems && ordersListItems.length ? (
        <FlatList
          data={ordersListItems}
          initialNumToRender={3}
          removeClippedSubviews
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => (
            <OrderContainer
              order={item}
              labels={labels}
              navigation={navigation}
              upcomingOrdersView={upcomingOrdersView}
              helpWithOrderLabels={helpWithOrderLabels}
              cshReturnItemOlderDays={cshReturnItemOlderDays}
              isCSHAppEnabled={isCSHAppEnabled}
              setOrderResendModalOpen={setOrderResendModalOpen}
            />
          )}
        />
      ) : null}
    </>
  );
};
OrderListWithDetails.propTypes = {
  ordersListItems: PropTypes.shape({}).isRequired,
  ordersLabels: PropTypes.shape({}).isRequired,
  labels: PropTypes.shape({}).isRequired,
  upcomingOrdersView: PropTypes.bool,
  navigation: PropTypes.func.isRequired,
};

OrderListWithDetails.defaultProps = {
  upcomingOrdersView: false,
};
export default OrderListWithDetails;
export { OrderListWithDetails };
