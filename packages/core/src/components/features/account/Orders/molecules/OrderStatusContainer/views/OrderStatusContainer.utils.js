// 9fbef606107a605d69c0edbcd8029e5d
import { getLabelValue } from '@tcp/core/src/utils/utils';
import constants from '../../../Orders.constants';

const { STATUS_CONSTANTS = {} } = constants;
const {
  DELIVERED,
  CANCELLED,
  ORDER_CANCELLED,
  ORDER_PICKED_UP,
  ITEMS_PICKED_UP,
  ORDER_CANCELED,
  PICKUP_EXPIRED,
  PICKUP_ITEM_EXPIRED,
} = STATUS_CONSTANTS;

export const getOrderStatus = status => {
  return status === ORDER_PICKED_UP || status === ITEMS_PICKED_UP || status === DELIVERED;
};

export const showLogoAndStatusCheck = status => {
  return status !== PICKUP_EXPIRED && status !== PICKUP_ITEM_EXPIRED;
};

export const updateStatusText = (orderStatus, labels, isPartiallyCancelled) => {
  const status = (orderStatus && orderStatus.toLowerCase().trim()) || '';
  if (isPartiallyCancelled) {
    return getLabelValue(labels, 'lbl_orders_statusOrderPartiallyCancelled', 'orders');
  }
  if (status === CANCELLED || status === ORDER_CANCELLED || status === ORDER_CANCELED) {
    return getLabelValue(labels, 'lbl_orders_statusShippingCancelled', 'orders');
  }
  return status;
};
