// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { getLabelValue, mobileOrderDateFormatted } from '@tcp/core/src/utils/utils';
import constants from '@tcp/core/src/components/features/account/Orders/Orders.constants';
import { Image, BodyCopy } from '@tcp/core/src/components/common/atoms';
import {
  getShipmentIcon,
  getStatusTextClass,
  getRCStatus,
} from '@tcp/core/src/components/features/account/OrderDetails/organism/OrderDetailsWrapper/views/OrderDetailsWrapper.utils';
import {
  OrderStatusWrapper,
  OrderStatusImage,
  OrderStatusText,
  ShipmentText,
  OrderLogoAndStatus,
  PickupFromText,
} from '../styles/OrderStatusContainer.style.native';
import {
  getOrderStatus,
  showLogoAndStatusCheck,
  updateStatusText,
} from './OrderStatusContainer.utils';
import OrderListStorePickup from '../../OrderListStorePickup/views/OrderListStorePickup.view.native';

const pickedUpIcon = require('../../../../../../../../../mobileapp/src/assets/images/order-delivered-colored-filled.png');
const shippingCancelled = require('../../../../../../../../../mobileapp/src/assets/images/cancelled.png');
const outForDelivery = require('../../../../../../../../../mobileapp/src/assets/images/out-for-delivery.png');
const processingPreparing = require('../../../../../../../../../mobileapp/src/assets/images/processing-preparing.png');
const readyForPickup = require('../../../../../../../../../mobileapp/src/assets/images/ready-for-pickup.png');
const shipped = require('../../../../../../../../../mobileapp/src/assets/images/shipped.png');
const returns = require('../../../../../../../../../mobileapp/src/assets/images/return_initiated.png');

const { STATUS_CONSTANTS = {} } = constants;
const { RETURN_INITIATED, RETURN_COMPLETED, PROCESSING, ORDER_IN_PROCESS } = STATUS_CONSTANTS;

const iconsObj = {
  pickedUpIcon,
  shippingCancelled,
  inTransit: shipped,
  outForDelivery,
  partiallyReturned: shippingCancelled,
  processingPreparing,
  readyForPickup,
  shipped,
  returns,
};

export const shouldRenderStoreData = (
  { isEcomOrder, showLogoAndStatus, orderSummary, labels, storeAddress, isCancelledItems },
  showStore
) => {
  return (
    !isEcomOrder &&
    !showLogoAndStatus && (
      <OrderListStorePickup
        orderSummary={orderSummary}
        labels={labels}
        isCancelledItems={isCancelledItems}
        storeAddress={storeAddress}
        showStore={showStore}
      />
    )
  );
};

const renderEstimatedDate = (label, displayDate) => {
  return label && displayDate ? (
    <>
      <BodyCopy fontFamily="secondary" fontSize="fs14" color="gray.800" text=" | " />
      <BodyCopy
        fontFamily="secondary"
        fontSize="fs14"
        color="green.500"
        text={`${label} ${displayDate}`}
      />
    </>
  ) : null;
};

const getIsShowTracking = (isUpcomingOrdersView, isPartiallyCancelled, isEcomOrder) =>
  !isUpcomingOrdersView && !isPartiallyCancelled && isEcomOrder;

const getDate = (date) => (date ? mobileOrderDateFormatted(date) : '');

const isReturnItem = (status) => !!(status === RETURN_COMPLETED || status === RETURN_INITIATED);

const getStatus = (status, labels) => {
  switch (status) {
    case RETURN_INITIATED:
      return getLabelValue(labels, 'lbl_orders_returns_initiated', 'orders');
    case RETURN_COMPLETED:
      return getLabelValue(labels, 'lbl_orders_returns_completed', 'orders');
    case PROCESSING:
    case ORDER_IN_PROCESS:
      return getLabelValue(labels, 'lbl_order_being_processed', 'orders');
    default:
      return status;
  }
};

const OrderStatusContainer = (props) => {
  const {
    isUpcomingOrdersView,
    orderShipmentStatus,
    labels,
    isEcomOrder,
    totalCount,
    index,
    isPartiallyCancelled,
    orderSummary,
    storeAddress,
    isCancelledItems,
    expectedDeliveryDate,
    deliveryDate,
    carrierDeliveryDate,
    shipDate,
    packageStatus,
  } = props;

  let status = getLabelValue(labels, orderShipmentStatus, 'orders') || '';
  status = status.toLowerCase();
  status = updateStatusText(status, labels, isPartiallyCancelled);
  const isOrderSuccess = getOrderStatus(status);
  const showLogoAndStatus = showLogoAndStatusCheck(status);
  const isShowTracking = getIsShowTracking(isUpcomingOrdersView, isPartiallyCancelled, isEcomOrder);
  const statusTextClass = getStatusTextClass(status);
  const { addressLine1 = '' } = storeAddress;
  const storeName = addressLine1.split('|')[0];

  const actualDeliveryDate = getDate(deliveryDate);
  const carrierDeliveryDateValue = getDate(carrierDeliveryDate);
  const shipDateValue = getDate(shipDate);

  const { label, displayDate } =
    getRCStatus(
      packageStatus,
      expectedDeliveryDate,
      actualDeliveryDate,
      shipDateValue,
      carrierDeliveryDateValue,
      labels?.orders
    ) || {};
  const ShipmentBodyTextWrapper = () => {
    return !isReturnItem(status) && !isUpcomingOrdersView && isEcomOrder ? (
      <OrderLogoAndStatus>
        <BodyCopy
          fontFamily="secondary"
          fontSize="fs14"
          color="gray.800"
          text={`${getLabelValue(
            labels,
            'lbl_orderDetails_shipment',
            'orders'
          )} ${index} of ${totalCount}`}
        />
        {renderEstimatedDate(label, displayDate)}
      </OrderLogoAndStatus>
    ) : null;
  };
  return (
    <OrderStatusWrapper
      isShowBorder={index > 1}
      showLogoAndStatus={showLogoAndStatus}
      isShowTracking={isShowTracking}
    >
      {!isEcomOrder &&
        !showLogoAndStatus &&
        shouldRenderStoreData({
          isEcomOrder,
          showLogoAndStatus,
          orderSummary,
          labels,
          storeAddress,
          isCancelledItems,
        })}
      {showLogoAndStatus && (
        <OrderLogoAndStatus>
          <OrderStatusImage>
            <Image
              height="24px"
              width="24px"
              source={getShipmentIcon(status?.toLowerCase()?.trim(), iconsObj)}
            />
          </OrderStatusImage>
          <View>
            <ShipmentText
              fontFamily="secondary"
              fontSize="fs16"
              fontWeight="bold"
              statusTextClass={statusTextClass}
              isReturn={status === RETURN_INITIATED || status === RETURN_COMPLETED}
              text={getStatus(status, labels)}
            />
            <ShipmentBodyTextWrapper />
            {isOrderSuccess && !isPartiallyCancelled && !isEcomOrder && (
              <PickupFromText
                fontFamily="secondary"
                fontSize="fs14"
                color="gray.800"
                text={`${getLabelValue(labels, 'lbl_orders_pickup_from', 'orders')} ${storeName}`}
              />
            )}
          </View>
        </OrderLogoAndStatus>
      )}
      {isShowTracking && (
        <OrderStatusText showLogoAndStatus={showLogoAndStatus}>
          {!showLogoAndStatus && <ShipmentBodyTextWrapper />}
        </OrderStatusText>
      )}
    </OrderStatusWrapper>
  );
};

OrderStatusContainer.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  isUpcomingOrdersView: PropTypes.bool,
  orderShipmentStatus: PropTypes.string.isRequired,
  isEcomOrder: PropTypes.bool.isRequired,
  totalCount: PropTypes.number.isRequired,
  index: PropTypes.number.isRequired,
  isPartiallyCancelled: PropTypes.bool.isRequired,
  orderSummary: PropTypes.shape({}).isRequired,
  storeAddress: PropTypes.shape({}).isRequired,
  isCancelledItems: PropTypes.bool,
  expectedDeliveryDate: PropTypes.string,
  deliveryDate: PropTypes.string,
  carrierDeliveryDate: PropTypes.string,
  shipDate: PropTypes.string,
  packageStatus: PropTypes.string,
};

OrderStatusContainer.defaultProps = {
  isUpcomingOrdersView: false,
  isCancelledItems: false,
  expectedDeliveryDate: '',
  deliveryDate: '',
  carrierDeliveryDate: '',
  shipDate: '',
  packageStatus: '',
};

export default OrderStatusContainer;
export { OrderStatusContainer as VanillaOrderStatusContainer };
