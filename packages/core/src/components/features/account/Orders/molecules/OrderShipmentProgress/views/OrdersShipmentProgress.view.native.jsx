// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import {
  OrderProgressWrapper,
  ProgressStatusList,
  ProgressBar,
  ProgressStep,
  ProgressLeftBar,
  ProgressRightBar,
  ProgressBarIcon,
} from '../styles/OrderShipmentProgress.style.native';

const getProgressBarIcon = (isReached, index, isReturnOrder) => {
  if (isReturnOrder) {
    return (
      <>
        {index === 0 && <ProgressBarIcon isReached={isReached} className="bullet-step" text="" />}
        {index === 3 && <ProgressBarIcon isReached={isReached} className="bullet-step" text="" />}
      </>
    );
  }
  return <ProgressBarIcon isReached={isReached} className="bullet-step" text="" />;
};

const OrderShipmentProgress = ({ progressSteps, isReturnOrder }) => {
  const totalCount = progressSteps.length || 0;
  const isSingleValue = totalCount;
  return (
    <OrderProgressWrapper>
      {totalCount &&
        progressSteps.map((step, index) => {
          const { name, isReached = false } = step;
          return (
            <ProgressStatusList>
              <ProgressStep
                isTriple={totalCount === 3}
                isSingleLine={totalCount < isSingleValue}
                text={name}
                fontSize="fs12"
                className="heading"
                color={isReached ? 'gray.900' : 'gray.500'}
              />
              {getProgressBarIcon(isReached, index, isReturnOrder)}
              <ProgressBar isSingleLine={totalCount < isSingleValue}>
                <ProgressLeftBar text="" index={index} isReached={isReached} />
                <ProgressRightBar
                  text=""
                  index={index}
                  isReached={isReached}
                  totalCount={totalCount}
                />
              </ProgressBar>
            </ProgressStatusList>
          );
        })}
    </OrderProgressWrapper>
  );
};

OrderShipmentProgress.propTypes = {
  progressSteps: PropTypes.shape([]),
};

OrderShipmentProgress.defaultProps = {
  progressSteps: [],
};

export default OrderShipmentProgress;
export { OrderShipmentProgress as OrderShipmentProgressVanilla };
