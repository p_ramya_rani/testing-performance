// 9fbef606107a605d69c0edbcd8029e5d 
import React, { PureComponent } from 'react';
import { PropTypes } from 'prop-types';
import OrderReturnReceipt from '../views/OrderReturnReceipt.view';

/**
 * This Class component use for return the Order Details data
 * can be passed in the component.
 * @param state - initial state of selectedActivity set to be null
 */
export class OrderReturnReceiptContainer extends PureComponent {
  /**
   * @function render  Used to render the JSX of the component
   * @param    {[Void]} function does not accept anything.
   * @return   {[Object]} JSX of the component
   */
  render() {
    return <OrderReturnReceipt {...this.props} />;
  }
}

OrderReturnReceiptContainer.propTypes = {
  orderDetailsData: PropTypes.shape({}),
};

OrderReturnReceiptContainer.defaultProps = {
  orderDetailsData: {},
};

export default OrderReturnReceiptContainer;
