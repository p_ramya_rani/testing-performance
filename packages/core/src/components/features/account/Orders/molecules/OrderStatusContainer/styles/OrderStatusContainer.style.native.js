// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components';
import { BodyCopy } from '@tcp/core/src/components/common/atoms';

const OrderStatusWrapper = styled.View`
  flex-direction: row;
  ${(props) =>
    props.isShowTracking || props.showLogoAndStatus
      ? `margin: ${props.theme.spacing.ELEM_SPACING.LRG}
  ${props.theme.spacing.ELEM_SPACING.MED} 0
  ${props.theme.spacing.ELEM_SPACING.MED};`
      : ''}
  align-items: center;
  justify-content: ${(props) => (props.showLogoAndStatus ? 'space-between' : 'flex-end')};
  ${(props) =>
    props.isShowBorder
      ? `border-top-width: 1px;
      border-top-color: ${props.theme.colorPalette.gray[500]};
      padding-top: 15px;
      margin-top: 0;`
      : ''}
`;

const OrderLogoAndStatus = styled.View`
  flex-direction: row;
`;

const OrderStatusImage = styled.View`
  margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
`;
const OrderStatusText = styled.View`
  flex-direction: row;
  justify-content: space-between;
  width: ${(props) => (props.showLogoAndStatus ? '32%' : '100%')};
  align-items: center;
`;

const TrackOrderWrapper = styled.View`
  align-items: center;
`;

const getStatusColor = (props) => {
  const { statusTextClass, isReturn } = props;
  if (statusTextClass === 'cancelled-status') return props.theme.colorPalette.red[500];
  if (statusTextClass === 'success-status') return props.theme.colors.CUSTOM.ORDER_SUCCESS;
  if (isReturn) return props.theme.colors.TEXT.DARK;
  return props.theme.colorPalette.gray[800];
};

const ShipmentText = styled(BodyCopy)`
  text-transform: ${(props) => (props.isReturn ? 'none' : 'capitalize')};
  margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  color: ${(props) => getStatusColor(props)};
`;

const PickupFromText = styled(BodyCopy)`
  text-transform: capitalize;
`;

export {
  OrderStatusWrapper,
  OrderStatusImage,
  OrderStatusText,
  TrackOrderWrapper,
  ShipmentText,
  OrderLogoAndStatus,
  PickupFromText,
};
