/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import { getLabelValue, getIconPath, getAPIConfig } from '@tcp/core/src/utils/utils';
import { getTranslateDateInformation, isMobileApp } from '@tcp/core/src/utils';
import constants from '../../../Orders.constants';

const { STATUS_CONSTANTS = {} } = constants;
const {
  SHIPPED,
  ORDER_SHIPPED,
  CANCELLED,
  ORDER_CANCELLED,
  ORDER_PICKED_UP,
  ITEMS_PICKED_UP,
  ORDER_IN_PROCESS,
  ITEMS_READY_FOR_PICKUP,
  ITEM_READY_FOR_PICK_UP,
  PREPARING_YOUR_ORDER,
  ORDER_PREPARING,
  ORDER_CANCELED,
  PROCESSING,
  PICKUP_EXPIRED,
  PICKUP_ITEM_EXPIRED,
  DELIVERED,
  IN_TRANSIT,
  OUT_FOR_DELIVERY,
  NA,
  PARTIALLY_SHIPPED,
  RETURN_INITIATED,
  RETURN_COMPLETED,
  REFUNDED,
  ORDER_BEING_PROCESSED,
} = STATUS_CONSTANTS;

const bossOrderProgressBarSteps = (labels, status) => {
  return [
    { name: getLabelValue(labels, 'lbl_orders_processing', 'orders'), isReached: true },
    {
      name: getLabelValue(labels, 'lbl_orders_statusItemsReadyForPickup', 'orders'),
      isReached: status === ITEMS_READY_FOR_PICKUP || status === ITEMS_PICKED_UP,
    },
    {
      name: getLabelValue(labels, 'lbl_orders_statusItemsPickedUp', 'orders'),
      isReached: status === ITEMS_PICKED_UP,
    },
  ];
};

const bopisOrderProgressBarSteps = (labels, status) => {
  return [
    { name: getLabelValue(labels, 'lbl_orders_processing', 'orders'), isReached: true },
    {
      name: getLabelValue(labels, 'lbl_orders_preparing', 'orders'),
      isReached:
        status === ORDER_PREPARING ||
        status === ITEMS_READY_FOR_PICKUP ||
        status === ITEMS_PICKED_UP,
    },
    {
      name: getLabelValue(labels, 'lbl_orders_statusItemsReadyForPickup', 'orders'),
      isReached: status === ITEMS_READY_FOR_PICKUP || status === ITEMS_PICKED_UP,
    },
    {
      name: getLabelValue(labels, 'lbl_orders_statusItemsPickedUp', 'orders'),
      isReached: status === ITEMS_PICKED_UP,
    },
  ];
};

const getDeliveredStatusStep = (labels, status, actualDeliveryDate) => {
  return {
    name:
      status === DELIVERED
        ? getLabelValue(labels, 'lbl_orders_heading_delivered', 'orders')
        : getLabelValue(labels, 'lbl_orders_heading_out_for_delivery', 'orders'),
    isReached: status === DELIVERED || status === OUT_FOR_DELIVERY,
    edd: status === DELIVERED ? actualDeliveryDate : null,
  };
};

const ecomOrderProgressBarSteps = (labels, status, edd) => {
  const { shipDate = '', actualDeliveryDate, orderDate } = edd || {};
  return [
    {
      name: getLabelValue(labels, 'lbl_orders_statusOrderReceived', 'orders'),
      isReached: true,
      edd: orderDate,
    },
    {
      name: getLabelValue(labels, 'lbl_orders_shipped', 'orders'),
      isReached:
        status === SHIPPED ||
        status === DELIVERED ||
        status === IN_TRANSIT ||
        status === OUT_FOR_DELIVERY,
      edd: shipDate,
    },
    {
      name: getLabelValue(labels, 'lbl_orders_heading__in_transit', 'orders'),
      isReached: status === IN_TRANSIT || status === DELIVERED || status === OUT_FOR_DELIVERY,
      iconPath: getIconPath('in-transit'),
    },
    getDeliveredStatusStep(labels, status, actualDeliveryDate),
  ];
};

export const getIsItemShipped = (status) =>
  status === SHIPPED ||
  status === DELIVERED ||
  status === IN_TRANSIT ||
  status === OUT_FOR_DELIVERY;

export const getProgressBarSteps = (labels, orderStatus, isEcomOrder, isBOSSOrder, edd) => {
  const status = (orderStatus && orderStatus.toLowerCase().trim()) || '';
  if (!status.length) {
    return [];
  }

  if (status === RETURN_COMPLETED || status === RETURN_INITIATED) {
    if (isMobileApp()) {
      return [
        {
          name: getLabelValue(labels, 'lbl_orders_return_initiated', 'orders'),
          isReached: status === RETURN_INITIATED || status === RETURN_COMPLETED,
        },
        {
          name: '',
          isReached: true,
        },
        {
          name: '',
          isReached: status === RETURN_COMPLETED,
        },
        {
          name: getLabelValue(labels, 'lbl_orders_return_completed', 'orders'),
          isReached: status === RETURN_COMPLETED,
        },
      ];
    }
    return [
      {
        name: getLabelValue(labels, 'lbl_orders_return_initiated', 'orders'),
        isReached: status === RETURN_INITIATED || status === RETURN_COMPLETED,
      },
      {
        name: getLabelValue(labels, 'lbl_orders_return_completed', 'orders'),
        isReached: status === RETURN_COMPLETED,
      },
    ];
  }

  if (isBOSSOrder) {
    return bossOrderProgressBarSteps(labels, status);
  }

  if (!isEcomOrder) {
    return bopisOrderProgressBarSteps(labels, status);
  }

  return ecomOrderProgressBarSteps(labels, status, edd);
};

export const getTotalCount = (
  shipmentsLength,
  canceledItemsLength,
  returnInitiatedLength = 0,
  returnCompletedLength = 0
) => {
  let length = shipmentsLength;
  if (canceledItemsLength > 0) {
    length -= 1;
  }
  if (returnInitiatedLength > 0) {
    length -= 1;
  }
  if (returnCompletedLength > 0) {
    length -= 1;
  }
  return length;
};

export const displayUpcomingOrderView = (
  isUpcomingOrdersView,
  orderStatus = '',
  isCancelledItems = false
) => {
  const statusLowerCased = orderStatus.toLowerCase().trim();

  return (
    statusLowerCased === CANCELLED ||
    statusLowerCased === ORDER_CANCELLED ||
    statusLowerCased === ORDER_PICKED_UP ||
    statusLowerCased === ORDER_CANCELED ||
    statusLowerCased === ITEMS_PICKED_UP ||
    statusLowerCased === PICKUP_ITEM_EXPIRED ||
    isCancelledItems
  );
};

export const filterMapping = {
  [ORDER_BEING_PROCESSED]: {
    statusToMatch: [ORDER_BEING_PROCESSED],
    statusToSet: 'lbl_order_being_processed',
  },
  [PROCESSING]: {
    statusToMatch: [PROCESSING, ORDER_IN_PROCESS],
    statusToSet: 'lbl_orders_processing',
  },
  [SHIPPED]: {
    statusToMatch: [SHIPPED, ORDER_SHIPPED, PARTIALLY_SHIPPED],
    statusToSet: 'lbl_orders_statusOrderShipped',
  },
  [ITEMS_PICKED_UP]: {
    statusToMatch: [ITEMS_PICKED_UP],
    statusToSet: 'lbl_orders_statusItemsPickedUp',
  },
  [ITEM_READY_FOR_PICK_UP]: {
    statusToMatch: [ITEM_READY_FOR_PICK_UP],
    statusToSet: 'lbl_orders_statusItemsReadyForPickup',
  },
  [PREPARING_YOUR_ORDER]: {
    statusToMatch: [PREPARING_YOUR_ORDER],
    statusToSet: 'lbl_orders_preparing',
  },
  [PICKUP_EXPIRED]: {
    statusToMatch: [PICKUP_EXPIRED, PICKUP_ITEM_EXPIRED],
    statusToSet: 'lbl_orders_heading_store_pickup_expired',
  },
  [DELIVERED]: {
    statusToMatch: [DELIVERED],
    statusToSet: 'lbl_orders_heading_delivered',
  },
  [IN_TRANSIT]: {
    statusToMatch: [IN_TRANSIT],
    statusToSet: 'lbl_orders_heading__in_transit',
  },
  [OUT_FOR_DELIVERY]: {
    statusToMatch: [OUT_FOR_DELIVERY],
    statusToSet: 'lbl_orders_heading_out_for_delivery',
  },
  [RETURN_INITIATED]: {
    statusToMatch: [RETURN_INITIATED],
    statusToSet: 'lbl_orders_return_initiated',
  },
  [RETURN_COMPLETED]: {
    statusToMatch: [RETURN_COMPLETED],
    statusToSet: 'lbl_orders_return_completed',
  },
  [REFUNDED]: {
    statusToMatch: [REFUNDED],
    statusToSet: 'lbl_orders_return_completed',
  },
  [NA]: {
    statusToMatch: [NA],
    statusToSet: 'N/A',
  },
};

const getOrderItemType = (items, statusArray) => {
  return items.filter((item) => {
    const { trackingInfo = [] } = item;
    return trackingInfo.some((info) => {
      const { status = '', quantity = 0, isCSHReturn = 'false' } = info || {};
      let statusText = status.toLowerCase().trim();
      statusText =
        statusText === RETURN_INITIATED && isCSHReturn === 'false' ? RETURN_COMPLETED : statusText;
      return quantity && statusArray.includes(statusText);
    });
  });
};

export const getItemsWithTargetStatus = (orderlist, targetStatus) => {
  const { statusToMatch = '', statusToSet = '' } = filterMapping[targetStatus] || {};
  let itemsArr = [];
  return orderlist
    .filter((obj) => {
      let { items } = obj;
      items = getOrderItemType(items, statusToMatch);
      if (items && items.length > 0) {
        itemsArr = items;
        return true;
      }
      return false;
    })
    .map((shipment) => {
      return {
        ...shipment,
        items: itemsArr,
        shipmentStatus: statusToSet,
        packageStatus: targetStatus,
      };
    });
};

export const showNotificationMessage = (
  totalCount,
  canceledItems,
  isEcomOrder,
  expiredItemShipment = []
) => {
  return (
    (totalCount > 1 && canceledItems[0].items.length) ||
    (!isEcomOrder &&
      canceledItems[0].items.length &&
      (totalCount > 1 || expiredItemShipment.length))
  );
};

export const getCancelledItemsLength = (canceledItems) => {
  return (
    canceledItems && canceledItems[0] && canceledItems[0].items && canceledItems[0].items.length
  );
};

/**
 * @function getTranslatedDate
 * @summary
 * @param {String} orderdate -
 * @return formatted date
 */
export const getTranslatedDate = (dateStr, longMonth) => {
  const { language } = getAPIConfig();
  const dateObj = getTranslateDateInformation(
    dateStr,
    language,
    {
      weekday: 'short',
    },
    {
      month: longMonth ? 'long' : 'short',
    }
  );
  return `${dateObj.month} ${dateObj.date}, ${dateObj.year}`;
};
/**
 * @description - This method creates seperate shipments based on trackingNumber
 */
const processMultiLineShipments = (shipments = [], shippedShipment = [], targetStatus = '') => {
  const { statusToMatch = [] } = filterMapping[targetStatus] || {};
  const uniqueShipments = {};
  const trackingNbrs = [];
  shippedShipment.forEach((shipment) => {
    const { items = [] } = shipment || {};
    items.forEach((item) => {
      const { trackingInfo = [] } = item || {};
      trackingInfo.forEach((info) => {
        const { trackingNbr, status = '', quantity = 0 } = info || {};
        const statusText = status && status.toLowerCase().trim();
        const trackingNumber = trackingNbr || 'n/a';
        if (quantity && trackingNumber && statusToMatch.includes(statusText)) {
          if (!uniqueShipments[trackingNumber]) {
            uniqueShipments[trackingNumber] = [];
          }
          uniqueShipments[trackingNumber].push(item);
          trackingNbrs.push(trackingNumber);
        }
      });
    });
  });
  const uniqueTrackingNbrs = [...new Set(trackingNbrs)];
  uniqueTrackingNbrs.forEach((trackingNbr) => {
    const items = uniqueShipments[trackingNbr] || [];
    const itemsArray = [];
    items.forEach((item) => {
      const { trackingInfo: itemTrackingInfo = [] } = item || {};
      const trackingInfoFiltered = itemTrackingInfo.filter((info) => {
        const { trackingNbr: targetTrackingNbr, status = '' } = info || {};
        const statusText = status.toLowerCase().trim();
        return targetTrackingNbr === trackingNbr && statusToMatch.includes(statusText);
      });
      const obj = { ...item, trackingInfo: trackingInfoFiltered };
      itemsArray.push(obj);
    });
    const { trackingInfo = [] } = itemsArray[0] || {};
    const {
      trackingUrl = '',
      shipDate,
      actualDeliveryDate,
      carrierDeliveryDate,
      trackingNbr: orderTracking,
    } = trackingInfo[0] || {};
    shipments.push({
      shipment: [
        {
          items: itemsArray,
          shipmentStatus: targetStatus,
          packageStatus: targetStatus,
          trackingUrl,
          orderTracking,
          shippedDate: shipDate ? shipDate.replace(/-/g, '/') : null,
          shipDate: shipDate ? getTranslatedDate(shipDate.replace(/-/g, '/')) : null,
          deliveryDate: actualDeliveryDate,
          carrierDeliveryDate,
          actualDeliveryDate: actualDeliveryDate
            ? getTranslatedDate(actualDeliveryDate.replace(/-/g, '/'))
            : null,
        },
      ],
      isCancelledItems: false,
    });
  });
  return shipments;
};

const pushToShipment = (shipmentsArray = [], shipmentToAdd = []) => {
  const processedShipment = shipmentsArray;
  if (shipmentToAdd.length) {
    processedShipment.push({
      shipment: shipmentToAdd,
      isCancelledItems: false,
    });
  }
  return processedShipment;
};

export const buildShipments = (totalCount, purchasedItems, processingShipment, ...others) => {
  const [
    shippedShipment,
    pickedUpShipment,
    readyForPickUpShipment,
    {
      canceledItems,
      preparingItemShipment,
      deliveredItemShipment,
      inTransitItemShipment,
      outForDeliveryItemShipment,
      returnInitiated,
      returnCompleted,
    },
  ] = others;
  let shipments = [];

  shipments = pushToShipment(shipments, processingShipment);
  if (deliveredItemShipment.length) {
    shipments = processMultiLineShipments(shipments, deliveredItemShipment, DELIVERED);
  }
  if (shippedShipment.length) {
    shipments = processMultiLineShipments(shipments, shippedShipment, SHIPPED);
  }
  shipments = pushToShipment(shipments, pickedUpShipment);
  shipments = pushToShipment(shipments, readyForPickUpShipment);
  shipments = pushToShipment(shipments, preparingItemShipment);

  if (inTransitItemShipment.length) {
    shipments = processMultiLineShipments(shipments, inTransitItemShipment, IN_TRANSIT);
  }
  if (outForDeliveryItemShipment.length) {
    shipments = processMultiLineShipments(shipments, outForDeliveryItemShipment, OUT_FOR_DELIVERY);
  }
  shipments = pushToShipment(shipments, returnInitiated);
  shipments = pushToShipment(shipments, returnCompleted);
  if (getCancelledItemsLength(canceledItems)) {
    shipments.push({
      shipment: canceledItems,
      isCancelledItems: true,
    });
  }

  if (!totalCount && purchasedItems) {
    shipments.push({
      shipment: purchasedItems,
      isCancelledItems: true,
    });
  }
  return shipments;
};

export const VISIBLE_IMAGE_COUNT = 4;

export const getLogoIconPath = (isOrderSuccess, isPartiallyCancelled) => {
  let logoIconPath = isOrderSuccess
    ? getIconPath('order-delivered')
    : getIconPath('status-shipping-cancelled');
  if (isPartiallyCancelled) {
    logoIconPath = getIconPath('order-partially-cancelled');
  }
  return logoIconPath;
};

export const getStatusTextClass = (isOrderSuccess, isPartiallyCancelled) => {
  let statusTextClass = isOrderSuccess ? 'success-status' : 'cancelled-status';
  if (isPartiallyCancelled) {
    statusTextClass = 'partially-cancelled';
  }
  return statusTextClass;
};

export const getQuantityToShow = (
  quantity,
  quantityCanceled,
  isCancelledItems,
  statusText = '',
  trackingInfo = []
) => {
  if (isCancelledItems && quantityCanceled) {
    return quantityCanceled;
  }
  let quantityToShow = 0;
  trackingInfo.forEach((info) => {
    const { status = '', quantity: itemQuantity = 0 } = info || {};
    const statusTextForMatch = status.toLowerCase().trim();
    if (statusTextForMatch === statusText.toLowerCase().trim()) {
      quantityToShow = itemQuantity;
    }
  });
  if (quantityToShow) {
    return quantityToShow;
  }

  return quantity - quantityCanceled;
};
