// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import OrderShipmentProgress from '../views/OrdersShipmentProgress.view.native';

describe('OrdersListItem component', () => {
  it('should renders correctly', () => {
    const props = {
      progressSteps: [
        { name: 'Order Received', isReached: true },
        { name: 'Processing', isReached: true },
        { name: 'Shippeed', isReached: false },
      ],
    };
    const component = shallow(<OrderShipmentProgress {...props} />);
    expect(component).toMatchSnapshot();
  });
});
