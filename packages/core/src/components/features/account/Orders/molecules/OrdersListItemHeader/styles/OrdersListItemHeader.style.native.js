// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components';
import { BodyCopy, Anchor } from '../../../../../../common/atoms';

const HeaderWrapper = styled.View``;

const HeaderTopSection = styled.View`
  background-color: ${(props) =>
    props.theme.isGymboree
      ? props.theme.colorPalette.orange[100]
      : props.theme.colorPalette.blue[100]};
  flex-direction: row;
  padding: ${(props) => props.theme.spacing.ELEM_SPACING.MED}
    ${(props) => props.theme.spacing.ELEM_SPACING.SM}
    ${(props) => props.theme.spacing.ELEM_SPACING.MED}
    ${(props) => props.theme.spacing.ELEM_SPACING.MED};
`;

const OrderTypeSection = styled.View`
  margin: 0 ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy6}px;
`;

const NotificationSection = styled.View`
  background-color: ${(props) => props.theme.colors.TEXT.PALEGRAY};
  flex-direction: row;
  padding: ${(props) => props.theme.spacing.ELEM_SPACING.XS} 14px;
`;

const BottomSection = styled.View`
  align-items: flex-end;
  justify-content: flex-end;
`;

const OrderMainView = styled.View`
  flex-direction: row;
  align-items: flex-end;
  justify-content: flex-end;
`;

const OrderTotal = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: flex-end;
  padding: 5px 10px;
  font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy_small}px;
  font-weight: ${(props) => props.theme.typography.fontWeights.extrabold};
`;

const StyledTrackAnchor = styled(Anchor)`
  background-color: ${(props) => props.theme.colors.BLACK};
  padding: 7px 10px;
  align-items: center;
  border-radius: 6px;
  overflow: hidden;
  text-transform: uppercase;
  font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy_small}px;
  margin-left: auto;
`;

const StoreSection = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin: ${(props) => props.theme.spacing.ELEM_SPACING.LRG}
    ${(props) => props.theme.spacing.ELEM_SPACING.MED} 0;
`;

const StoreButtonText = styled(BodyCopy)`
  text-decoration: underline;
  color: ${(props) => props.theme.colorPalette.blue[800]};
  margin: 0 5px;
  text-transform: capitalize;
  font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy3}px;
  font-weight: 700;
`;

const ShippingWrapper = styled.View`
  flex-direction: row;
  flex: 1;
`;

const PriceWrapper = styled.View`
  align-self: center;
  justify-content: flex-end;
  align-items: flex-end;
`;

const NotificationText = styled.View`
  padding-left: 14px;
  flex: 1;
`;

const StoreTimeWrapper = styled.View`
  align-items: center;
  flex-direction: row;
`;

const TextWrapper = styled.Text`
  font-weight: 700;
  font-family: ${(props) => props.theme.typography.fonts.secondary};
  font-size: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
`;

const ArrowWrapper = styled.View`
  padding-left: 6px;
  align-self: center;
`;

const TooltipWrapper = styled.View`
  justify-content: center;
`;

const ToolTipIcon = styled.TouchableOpacity`
  margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
`;

const StoreWrapper = styled.View`
  flex-direction: row;
  align-items: center;
  max-width: 60%;
`;

const ToolTipContentWrapper = styled.View`
  background: ${(props) => props.theme.colorPalette.text.lightgray};
  min-width: 80%;
  position: absolute;
  top: 50%;
  height: auto;
  border-radius: 8px;
`;

const ImageWrapper = styled.View`
  position: absolute;
  right: 0;
`;

const StyledTouchableOpacity = styled.TouchableOpacity`
  padding-top: 17px;
  padding-right: 14px;
`;

const HeaderSection = styled.View`
  padding: 14px;
  border-bottom-width: 1px;
  border-bottom-color: ${(props) => props.theme.colorPalette.gray[500]};
`;

const BodySection = styled.View`
  padding: 14px 14px 26px;
`;

const StyledCrossImage = styled.Image`
  width: 14px;
  height: 15px;
`;

const SafeAreaViewStyle = styled.SafeAreaView`
  align-items: center;
  flex: 1;
  background: rgba(0, 0, 0, 0.5);
`;

const ModalOutsideTouchable = styled.TouchableWithoutFeedback`
  flex: 1;
`;

const ModalOverlay = styled.View`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background: rgba(0, 0, 0, 0.5);
`;

const OrderTypeText = styled(BodyCopy)`
  text-transform: uppercase;
`;

const StyledAnchor = styled(Anchor)`
  background-color: ${(props) => props.theme.colors.BLACK};
  padding: 9px 34px;
  align-items: center;
  border-radius: 6px;
  overflow: hidden;
  text-transform: uppercase;
  font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy2}px;
`;

const PickupText = styled.View`
  font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy3}px;
  font-weight: 700;
  align-items: center;
  flex-direction: row;
  margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
`;

export {
  HeaderWrapper,
  HeaderTopSection,
  OrderTypeSection,
  NotificationSection,
  BottomSection,
  StoreSection,
  OrderTotal,
  StoreButtonText,
  ShippingWrapper,
  PriceWrapper,
  NotificationText,
  TextWrapper,
  ArrowWrapper,
  TooltipWrapper,
  ToolTipIcon,
  ToolTipContentWrapper,
  StoreWrapper,
  OrderTypeText,
  StyledAnchor,
  PickupText,
  SafeAreaViewStyle,
  ModalOutsideTouchable,
  ModalOverlay,
  ImageWrapper,
  StyledTouchableOpacity,
  StyledCrossImage,
  HeaderSection,
  BodySection,
  StoreTimeWrapper,
  StyledTrackAnchor,
  OrderMainView,
};
