// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  ${props => (props.isPageEmpty ? 'position:absolute;top: 36px;width: 100%;' : '')}
  .top-order-links {
    text-align: left;
    margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.XL};
    @media ${props => props.theme.mediaQuery.medium} {
      text-align: right;
      margin-bottom: 0;
    }
  }
  .top-order-links-new {
    font-size: 16px;
    text-align: center;
    background-color: ${props => props.theme.colors.WHITE};
    padding: ${props => props.theme.spacing.ELEM_SPACING.MED}
      ${props => props.theme.spacing.ELEM_SPACING.MED} 0;
    @media ${props => props.theme.mediaQuery.smallOnly} {
      font-size: 12px;
      text-align: center;
      padding-top: 23px;
      ${props => (props.isPageEmpty ? 'text-align: left;padding-bottom: 23px;' : '')}
    }
    ${props => (props.isPageEmpty ? 'text-align: right;' : '')}
  }
  .space {
    margin: 0 30px;
    @media ${props => props.theme.mediaQuery.smallOnly} {
      margin: 0 2px;
    }
  }
`;

export default styles;
