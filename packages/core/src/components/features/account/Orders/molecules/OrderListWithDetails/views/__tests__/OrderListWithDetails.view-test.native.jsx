// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import OrderPreviewItemsListNew from '../OrderListWithDetails.view.native';

const props = {
  labels: {},
  className: '',
  ordersListItems: [],
};
describe('Order Items component', () => {
  it('should renders correctly', () => {
    const component = shallow(<OrderPreviewItemsListNew {...props} />);
    expect(component).toMatchSnapshot();
  });
});
