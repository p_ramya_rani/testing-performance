// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

const ButtonWrapper = styled.View`
  width: 75%;
  justify-content: flex-start;
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.LRG};
`;

export default ButtonWrapper;
