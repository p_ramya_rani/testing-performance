// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import {
  getLabelValue,
  capitalize,
  capitalizeWords,
  isMobileApp,
  parseStoreHours,
  hasTrackingNum,
  getTrimmedTrackingNum,
  orderDateFormatted,
} from '@tcp/core/src/utils/utils';
import { Image, BodyCopy, Button, Anchor } from '@tcp/core/src/components/common/atoms';
import toTimeString from '@tcp/core/src/utils/formatTime';
import { parseDate } from '@tcp/core/src/utils/parseDate';
import { STORE_DETAILS_LABELS } from '@tcp/core/src/components/common/organisms/PickupStoreModal/PickUpStoreModal.constants';
import ClickTracker from '@tcp/web/src/components/common/atoms/ClickTracker';
import googleMapConstants from '@tcp/core/src/constants/googleMap.constants';
import {
  getLogoIconPath,
  getStatusTextClass,
  getRCStatus,
  getStatusLogoClass,
} from '@tcp/core/src/components/features/account/OrderDetails/organism/OrderDetailsWrapper/views/OrderDetailsWrapper.utils';
import constants from '@tcp/core/src/components/features/account/Orders/Orders.constants';
import styles from '../styles/OrderStatusContainer.style';

import { showLogoAndStatusCheck, updateStatusText } from './OrderStatusContainer.utils';

const { STATUS_CONSTANTS = {} } = constants;
const { RETURN_INITIATED, RETURN_COMPLETED, PROCESSING, ORDER_IN_PROCESS } = STATUS_CONSTANTS;
export const todayTiming = (storeHours) => {
  const hours = parseStoreHours(storeHours);
  const now = new Date();
  return hours.find((day) => {
    const { openIntervals = [{ fromHour: '' }] } = day || {};
    const openingHour = parseDate(openIntervals[0].fromHour);
    return now.getDay() === openingHour.getDay();
  });
};

export const GetTooltipContent = ({
  storeAddressLine = '',
  city = '',
  zipCode,
  storeName = '',
  state,
  phone,
  storeHours,
  labels,
}) => {
  const storename = capitalize(storeName);
  const addressLine = capitalize(storeAddressLine);
  const cityName = capitalize(city);
  let today;

  if (storeHours) {
    today = todayTiming(storeHours);
  }

  return (
    <div>
      <BodyCopy fontFamily="secondary" color="text.primary" fontWeight="semibold" fontSize="fs16">
        {storename}
      </BodyCopy>
      <BodyCopy fontFamily="secondary" color="text.secondary" fontSize="fs12">
        {addressLine}
      </BodyCopy>
      <BodyCopy fontFamily="secondary" color="text.secondary" fontSize="fs12">
        {cityName}
        {STORE_DETAILS_LABELS.COMMA_ONE}
        {STORE_DETAILS_LABELS.SPACE_ONE}
        {state}
        {STORE_DETAILS_LABELS.SPACE_ONE}
        {zipCode}
      </BodyCopy>
      <BodyCopy fontFamily="secondary" color="text.secondary" fontSize="fs12">
        {phone}
      </BodyCopy>
      {today && (
        <BodyCopy fontSize="fs14" fontFamily="secondary">
          {`${getLabelValue(labels, 'lbl_orders_openToday', 'orders')}: ${toTimeString(
            parseDate(today.openIntervals[0].fromHour)
          )} - ${toTimeString(parseDate(today.openIntervals[0].toHour))}`}
        </BodyCopy>
      )}
    </div>
  );
};

GetTooltipContent.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  storeAddressLine: PropTypes.string.isRequired,
  city: PropTypes.string.isRequired,
  zipCode: PropTypes.string.isRequired,
  storeName: PropTypes.string.isRequired,
  state: PropTypes.string.isRequired,
  phone: PropTypes.string.isRequired,
  storeHours: PropTypes.shape({}).isRequired,
};

export const openStoreDirections = (orderSummary) => {
  const { shippingAddress } = orderSummary;
  const { addressLine1, city, state, zipCode } = shippingAddress;
  if (isMobileApp()) {
    const store = {
      basicInfo: { address: shippingAddress },
    };
    this.mapHandler(store);
  }
  if (window) {
    window.open(
      `${googleMapConstants.OPEN_STORE_DIR_WEB}${addressLine1},%20${city},%20${state},%20${zipCode}`,
      '_blank',
      'noopener'
    );
  }
};

export const getStoreAddress = (orderSummary) => {
  const { shippingAddress } = orderSummary;
  const { addressLine1, city, state, zipCode } = shippingAddress;
  const storeAddressArr = addressLine1.split('|');
  const addressLineTwo = storeAddressArr.length > 1 ? storeAddressArr[1] : '';
  return (
    <>
      <span>{`${capitalizeWords(addressLineTwo)}, `}</span>
      <span className="store-addresssline">
        {`${capitalizeWords(city)}, ${capitalize(state)} ${capitalize(zipCode)}`}
      </span>
    </>
  );
};

const renderStoreAddress = (labels, today) => {
  return (
    today && (
      <BodyCopy color="text.primary" className="store-address-text" fontFamily="secondary">
        {`${getLabelValue(labels, 'lbl_orders_openToday', 'orders')}: ${toTimeString(
          parseDate(today && today.openIntervals[0].fromHour)
        )} - ${toTimeString(parseDate(today && today.openIntervals[0].toHour))}`}
      </BodyCopy>
    )
  );
};

export const TrackingCTA = ({
  isEcomOrder,
  orderTrackingUrl,
  orderSummary,
  labels,
  upcomingOrdersView,
  isPartiallyCancelled,
  orderTrackingNumber,
  isMobile,
}) => {
  const { shippingAddress } = orderSummary;
  const today = shippingAddress.storeHours ? todayTiming(shippingAddress.storeHours) : null;
  const trackingNumber = getTrimmedTrackingNum(orderTrackingNumber);
  return (
    <>
      {!upcomingOrdersView && !isPartiallyCancelled ? (
        <div className="track-order-container">
          {isEcomOrder ? (
            <ClickTracker
              clickData={{
                customEvents: ['event118'],
                eventName: 'trackingnumberclick',
                pageName: 'myplace:orders',
                pageShortName: 'myplace:orders',
              }}
              className="track-order-click-tracker"
            >
              {hasTrackingNum(trackingNumber) && (
                <>
                  <BodyCopy className="order-trackingNumber">
                    <BodyCopy
                      component="span"
                      fontSize={isMobile ? 'fs12' : 'fs14'}
                      fontFamily="secondary"
                      fontWeight="extrabold"
                    >
                      {getLabelValue(labels, 'lbl_orders_trackingId', 'orders')}
                    </BodyCopy>
                    <Anchor
                      component="span"
                      fontSizeVariation={isMobile ? 'medium' : 'large'}
                      fontFamily="secondary"
                      target="_blank"
                      to={orderTrackingUrl}
                      underline
                    >
                      {trackingNumber}
                    </Anchor>
                  </BodyCopy>
                </>
              )}
              <Anchor
                to={orderTrackingUrl}
                anchorVariation="button"
                className="track-order-cta"
                target="_blank"
              >
                {`${getLabelValue(labels, 'lbl_orderDetails_track', 'orders')}`}
              </Anchor>
            </ClickTracker>
          ) : (
            <div className="store-data-container">
              <div className="store-data">
                <BodyCopy
                  fontFamily="secondary"
                  className="store-address-text"
                  color="text.secondary"
                >
                  {getStoreAddress(orderSummary)}
                </BodyCopy>
                {renderStoreAddress(labels, today)}
              </div>
              <Button
                className="directions-cta"
                type="button"
                aria-label="GetDirections"
                onClick={() => openStoreDirections(orderSummary)}
              >
                {`${getLabelValue(labels, 'lbl_orderDetails_direction', 'orders')}`}
              </Button>
            </div>
          )}
        </div>
      ) : null}
    </>
  );
};

TrackingCTA.propTypes = {
  isEcomOrder: PropTypes.bool.isRequired,
  orderTrackingUrl: PropTypes.string.isRequired,
  orderSummary: PropTypes.shape({}).isRequired,
  labels: PropTypes.shape({}).isRequired,
  upcomingOrdersView: PropTypes.bool.isRequired,
  isPartiallyCancelled: PropTypes.bool.isRequired,
  orderTrackingNumber: PropTypes.string,
  isMobile: PropTypes.bool,
};

TrackingCTA.defaultProps = {
  orderTrackingNumber: 'N/A',
  isMobile: false,
};

const displayShipmentCount = (upcomingOrdersView, isEcomOrder, isCancelledItems) => {
  return isEcomOrder && !isCancelledItems;
};

const getDate = (date) => (date ? orderDateFormatted(date) : '');

const isReturnItem = (status) => !!(status === RETURN_COMPLETED || status === RETURN_INITIATED);

const getStatus = (status, labels) => {
  switch (status) {
    case RETURN_INITIATED:
      return getLabelValue(labels, 'lbl_orders_returns_initiated', 'orders');
    case RETURN_COMPLETED:
      return getLabelValue(labels, 'lbl_orders_returns_completed', 'orders');
    case PROCESSING:
    case ORDER_IN_PROCESS:
      return getLabelValue(labels, 'lbl_order_being_processed', 'orders');
    default:
      return status;
  }
};

const OrderStatusContainer = ({
  className,
  upcomingOrdersView,
  orderShipmentStatus,
  labels,
  isEcomOrder,
  orderSummary,
  totalCount,
  index,
  isPartiallyCancelled,
  isCancelledItems,
  storeAddress,
  expectedDeliveryDate,
  deliveryDate,
  carrierDeliveryDate,
  shipDate,
  packageStatus,
}) => {
  let status = getLabelValue(labels, orderShipmentStatus, 'orders') || '';
  status = status.toLowerCase();
  status = updateStatusText(status, labels, isPartiallyCancelled)?.toLowerCase();
  const showLogoAndStatus = showLogoAndStatusCheck(status);
  const logoIconPath = getLogoIconPath(status);
  const statusTextClass = getStatusTextClass(status);
  const statusLogoClass = getStatusLogoClass(status);
  const { addressLine1 = '|' } = storeAddress;
  const [addressLineOne] = addressLine1.split('|');
  const storeName = capitalizeWords(addressLineOne);
  const displayStoreDetailsAnchor = () => {
    return (
      <BodyCopy component="div" className="store-section" fontSize="fs16" fontWeight="bold">
        <BodyCopy
          component="div"
          className="store"
          onClick={() => openStoreDirections(orderSummary)}
        >
          {storeName}
        </BodyCopy>
      </BodyCopy>
    );
  };
  const actualDeliveryDate = getDate(deliveryDate);
  const carrierDeliveryDateValue = getDate(carrierDeliveryDate);
  const shipDateValue = getDate(shipDate);

  const { label, displayDate } =
    getRCStatus(
      packageStatus,
      expectedDeliveryDate,
      actualDeliveryDate,
      shipDateValue,
      carrierDeliveryDateValue,
      labels?.orders
    ) || {};
  return (
    <div className={`${className} order-status-container ${index === 1 ? 'first-shipment' : ''}`}>
      {!isEcomOrder && !showLogoAndStatus && displayStoreDetailsAnchor()}
      {showLogoAndStatus && (
        <div className="order-status-logo">
          <Image src={logoIconPath} className={statusLogoClass} alt="" />
        </div>
      )}
      <div className="order-status-text">
        {showLogoAndStatus && (
          <>
            <BodyCopy component="p" className={statusTextClass} fontSize="fs18">
              {getStatus(status, labels)}
            </BodyCopy>
            {!isEcomOrder && status !== RETURN_COMPLETED && status !== RETURN_INITIATED && (
              <BodyCopy component="p" className="shipment-count">
                {`${getLabelValue(labels, 'lbl_orders_pickup_from', 'orders')} ${storeName}`}
              </BodyCopy>
            )}
          </>
        )}
        {!isReturnItem(status) &&
          displayShipmentCount(upcomingOrdersView, isEcomOrder, isCancelledItems) && (
            <BodyCopy component="p" className="shipment-count">
              {`${getLabelValue(
                labels,
                'lbl_orderDetails_shipment',
                'orders'
              )} ${index} of ${totalCount}`}

              {label && displayDate && (
                <>
                  <BodyCopy component="span" className="shipment-devider">
                    |
                  </BodyCopy>
                  <BodyCopy component="span" className="shipment-edd">
                    {`${label} `}
                    {displayDate}
                  </BodyCopy>
                </>
              )}
            </BodyCopy>
          )}
      </div>
    </div>
  );
};

OrderStatusContainer.propTypes = {
  className: PropTypes.string.isRequired,
  upcomingOrdersView: PropTypes.bool.isRequired,
  orderShipmentStatus: PropTypes.string.isRequired,
  labels: PropTypes.shape({}).isRequired,
  isEcomOrder: PropTypes.bool.isRequired,
  orderSummary: PropTypes.shape({}).isRequired,
  totalCount: PropTypes.number.isRequired,
  index: PropTypes.number.isRequired,
  isPartiallyCancelled: PropTypes.bool.isRequired,
  isCancelledItems: PropTypes.bool.isRequired,
  storeAddress: PropTypes.shape({}).isRequired,
  expectedDeliveryDate: PropTypes.string,
};

OrderStatusContainer.defaultProps = {
  expectedDeliveryDate: '',
};

export default withStyles(OrderStatusContainer, styles);
export { OrderStatusContainer as VanillaOrderStatusContainer };
