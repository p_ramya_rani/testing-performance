// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';
import { BodyCopy, Button } from '@tcp/core/src/components/common/atoms';

const UnderlineStyle = styled.View`
  height: 3px;
  background-color: ${props => props.theme.colorPalette.black};
`;

const NewOrderListViewWrapper = styled.View`
  background-color: ${props =>
    props.isOrderPresent ? props.theme.colorPalette.gray[300] : props.theme.colors.WHITE};
  margin: 0 -14px;
`;

const EmptyOrdersListWrapper = styled.View`
  align-items: center;
`;

const StyledHeading = styled.View`
  background-color: ${props => props.theme.colors.WHITE};
  padding: ${props => props.theme.spacing.ELEM_SPACING.XL}
    ${props => props.theme.spacing.ELEM_SPACING.MED}
    ${props => props.theme.spacing.ELEM_SPACING.LRG};
`;

const Heading = styled.View`
  width: 100%;
  padding-top: 8px;
  border-bottom-width: 4px;
  border-bottom-color: ${props => props.theme.colors.PRIMARY.DARK};
`;

const HeadingText = styled(BodyCopy)`
  text-transform: uppercase;
`;

const ButtonWrapper = styled.View`
  background-color: ${props => props.theme.colors.WHITE};
  justify-content: center;
  flex-direction: row;
  padding: ${props => props.theme.spacing.ELEM_SPACING.MED} 0 11px;
`;

const LoadMoreButton = styled(Button)`
  padding: 5px 0;
`;

const OrderHistorySpacing = styled.View`
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.LRG};
`;

export {
  UnderlineStyle,
  NewOrderListViewWrapper,
  StyledHeading,
  Heading,
  HeadingText,
  ButtonWrapper,
  LoadMoreButton,
  OrderHistorySpacing,
  EmptyOrdersListWrapper,
};
