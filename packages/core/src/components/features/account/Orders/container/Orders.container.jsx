// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getHelpWithOrdersLabels } from '@tcp/core/src/components/common/organisms/HelpWithOrder/container/HelpWithOrder.selectors';
import {
  getCSHReturnItemOlderDays,
  getIsCSHAppEnabled,
  getIsCSHEnabled,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import { setClickAnalyticsData, trackClick } from '@tcp/core/src/analytics/actions';
import {
  getNewOrdersListItem,
  getOrderListFetchingState,
  getPreviewOrderId,
  getShowOrderListWithItemDetails,
  getUpcomingOrders,
  getLoadMoreState,
  getAccountOrdersAPISwitch,
} from './Orders.selectors';
import { setOrderResendModalOpen } from '../../../../common/organisms/ResendOrderConfirmation/container/ResendOrderConfirmation.actions';
import {
  getAllItems,
  getMostRecentOrders,
  getOrderDetailsDataFetchingState,
  getOrderLookUpAPISwitch,
} from '../../OrderDetails/container/OrderDetails.selectors';
import { getSiteId } from '../../../../../utils';
import OrderListComponent from '../views';
import { getOrdersList } from './Orders.actions';
import constants from '../Orders.constants';
import { getOrderDetails } from '../../OrderDetails/container/OrderDetails.actions';
import { getLabels } from '../../Account/container/Account.selectors';

/**
 * This component will render OrdersContainer component
 * @param { object, Array }
 */
export class OrdersContainer extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      pageNum: '0',
      showNewSkeleton: true,
    };
    this.loadMorePos = 0;
  }

  componentDidMount() {
    const {
      fetchOrders,
      previewOrderId,
      getOrderDetailsAction,
      showOrderListWithItemDetails,
      accountOrdersAPISwitch,
      orderLookUpAPISwitch,
    } = this.props;
    const { pageNum } = this.state;
    let listPayload = getSiteId();
    if (showOrderListWithItemDetails) {
      listPayload = {
        reqParams: {
          itemdetails: true,
          pageNumber: pageNum,
          pageSize: constants.ORDER_SIZE,
        },
        accountOrdersAPISwitch,
      };
    }
    fetchOrders(listPayload);

    if (!showOrderListWithItemDetails && previewOrderId) {
      const payload = {
        orderId: previewOrderId,
        isRecentOrder: true,
        orderLookUpAPISwitch,
      };
      getOrderDetailsAction(payload);
    }
  }

  componentDidUpdate(prevProps) {
    const {
      previewOrderId,
      getOrderDetailsAction,
      showOrderListWithItemDetails,
      orderLookUpAPISwitch,
    } = this.props;
    const prevPreviewOrderId = prevProps.previewOrderId;
    if (!showOrderListWithItemDetails && previewOrderId && prevPreviewOrderId !== previewOrderId) {
      const payload = {
        orderId: previewOrderId,
        orderLookUpAPISwitch,
      };
      getOrderDetailsAction(payload);
    }
  }

  calculatePositionToScroll = () => {
    const loadMoreCta = document.querySelector('.load-more-cta');
    const showCondensedHeader = document.querySelector('.show-condensed-header');
    const { pageYOffset = 0 } = window || {};
    this.loadMorePos =
      (loadMoreCta && loadMoreCta.getBoundingClientRect().top) +
      pageYOffset -
      (showCondensedHeader && showCondensedHeader.getBoundingClientRect().height) -
      30;
  };

  /**
   * This function will fetch more orders data
   */
  loadMoreOrders = () => {
    const { fetchOrders, accountOrdersAPISwitch } = this.props;
    const { pageNum } = this.state;
    this.setState(
      {
        pageNum: Number(pageNum) + 1,
        showNewSkeleton: false,
      },
      () => {
        const { pageNum: pageNumUpdated } = this.state;
        fetchOrders({
          reqParams: {
            itemdetails: true,
            pageNumber: pageNumUpdated,
            pageSize: 5,
          },
          fromLoadMore: true,
          accountOrdersAPISwitch,
        });
      }
    );
  };

  /**
   * This function will trigger the to get country specific orders
   * @param {string, string} - siteId companyId
   */
  filterLinkHandler = (siteId, companyId) => {
    const { fetchOrders, showOrderListWithItemDetails, accountOrdersAPISwitch } = this.props;
    let payload = { siteId, currentSiteId: companyId, accountOrdersAPISwitch };
    if (showOrderListWithItemDetails) {
      payload = {
        ...payload,
        reqParams: {
          itemdetails: true,
          pageNumber: '0',
          pageSize: constants.ORDER_SIZE,
        },
      };
      this.setState({
        pageNum: '0',
      });
    }
    fetchOrders(payload);
  };

  render() {
    const {
      labels,
      ordersListItems,
      navigation,
      handleComponentChange,
      componentProps,
      orderItems,
      recentOrderDetails,
      isMostRecentOrderFetching,
      isMostRecentOrderDetailFetching,
      showOrderListWithItemDetails,
      upcomingOrders,
      isLoadMoreActive,
      helpWithOrderLabels,
      cshReturnItemOlderDays,
      isCSHAppEnabled,
      setOrderResendModal,
      isCSHEnabled,
    } = this.props;
    const { showNewSkeleton } = this.state;
    return (
      <OrderListComponent
        labels={labels}
        onFilterLink={this.filterLinkHandler}
        showOrderListWithItemDetails={showOrderListWithItemDetails}
        ordersListItems={ordersListItems}
        upcomingOrders={upcomingOrders}
        navigation={navigation}
        handleComponentChange={handleComponentChange}
        componentProps={componentProps}
        orderItems={orderItems}
        recentOrderDetails={recentOrderDetails}
        isMostRecentOrderFetching={isMostRecentOrderFetching}
        isMostRecentOrderDetailFetching={isMostRecentOrderDetailFetching}
        loadMoreOrders={this.loadMoreOrders}
        isLoadMoreActive={isLoadMoreActive}
        showNewSkeleton={showNewSkeleton}
        helpWithOrderLabels={helpWithOrderLabels}
        cshReturnItemOlderDays={cshReturnItemOlderDays}
        isCSHAppEnabled={isCSHAppEnabled}
        setOrderResendModalOpen={setOrderResendModal}
        isCSHEnabled={isCSHEnabled}
      />
    );
  }
}

export const mapStateToProps = (state, ownProps) => ({
  labels: getLabels(state),
  ordersListItems: getNewOrdersListItem(state),
  orderItems: getAllItems(state),
  recentOrderDetails: getMostRecentOrders(state),
  isMostRecentOrderFetching: getOrderListFetchingState(state),
  isMostRecentOrderDetailFetching: getOrderDetailsDataFetchingState(state),
  previewOrderId: getPreviewOrderId(state),
  showOrderListWithItemDetails: getShowOrderListWithItemDetails(state, ownProps),
  upcomingOrders: getUpcomingOrders(state),
  isLoadMoreActive: getLoadMoreState(state),
  accountOrdersAPISwitch: getAccountOrdersAPISwitch(state),
  orderLookUpAPISwitch: getOrderLookUpAPISwitch(state),
  helpWithOrderLabels: getHelpWithOrdersLabels(state),
  cshReturnItemOlderDays: getCSHReturnItemOlderDays(state),
  isCSHAppEnabled: getIsCSHAppEnabled(state),
  isCSHEnabled: getIsCSHEnabled(state),
});

export const mapDispatchToProps = (dispatch) => ({
  fetchOrders: (payload) => {
    dispatch(getOrdersList(payload));
  },
  setOrderResendModal: (payload) => {
    dispatch(setOrderResendModalOpen(payload));
  },
  trackAnalyticsClick: (eventData, payload) => {
    dispatch(setClickAnalyticsData(eventData));
    const timer = setTimeout(() => {
      dispatch(trackClick(payload));
      clearTimeout(timer);
    }, 100);
  },
  getOrderDetailsAction: (payload) => {
    dispatch(getOrderDetails(payload));
  },
});

OrdersContainer.propTypes = {
  fetchOrders: PropTypes.func,
  labels: PropTypes.shape({}).isRequired,
  ordersListItems: PropTypes.shape([]),
  navigation: PropTypes.shape({}).isRequired,
  handleComponentChange: PropTypes.func,
  componentProps: PropTypes.shape({}),
  orderItems: PropTypes.shape([]),
  getOrderDetailsAction: PropTypes.func.isRequired,
  isMostRecentOrderFetching: PropTypes.bool,
  isMostRecentOrderDetailFetching: PropTypes.bool,
  previewOrderId: PropTypes.string,
  showOrderListWithItemDetails: PropTypes.bool,
  upcomingOrders: PropTypes.shape([]),
  isLoadMoreActive: PropTypes.bool,
  recentOrderDetails: PropTypes.shape([]),
  accountOrdersAPISwitch: PropTypes.bool,
  orderLookUpAPISwitch: PropTypes.bool,
  helpWithOrderLabels: PropTypes.shape({}),
  cshReturnItemOlderDays: PropTypes.shape({}),
  isCSHAppEnabled: PropTypes.bool,
  setOrderResendModal: PropTypes.func,
};

OrdersContainer.defaultProps = {
  fetchOrders: () => {},
  ordersListItems: [],
  recentOrderDetails: [],
  handleComponentChange: () => {},
  componentProps: {},
  orderItems: [],
  isMostRecentOrderFetching: false,
  isMostRecentOrderDetailFetching: false,
  previewOrderId: null,
  showOrderListWithItemDetails: false,
  upcomingOrders: [],
  isLoadMoreActive: false,
  accountOrdersAPISwitch: false,
  orderLookUpAPISwitch: false,
  helpWithOrderLabels: {},
  cshReturnItemOlderDays: {},
  isCSHAppEnabled: false,
  setOrderResendModal: () => {},
};

export default connect(mapStateToProps, mapDispatchToProps)(OrdersContainer);
