// 9fbef606107a605d69c0edbcd8029e5d 
import { fromJS } from 'immutable';
import {
  getOrdersListState,
  getOrderListFetchingState,
  getPreviewOrderId,
  getShowOrderListWithItemDetails,
  getAccountOrdersAPISwitch,
} from '../Orders.selectors';

describe('#pointsHistoryData selector', () => {
  it('#getOrdersListState should return OrdersListState state', () => {
    const OrdersListState = fromJS({
      ordersList: [],
    });
    const state = {
      Orders: OrdersListState,
      isFatching: false,
    };

    expect(getOrdersListState(state)).toEqual(OrdersListState.get('ordersList'));

    expect(getOrderListFetchingState(state)).toEqual(OrdersListState.get('isFatching'));
  });

  it('#getPreviewOrderId should return orderId of the first order', () => {
    const OrdersListState = fromJS({
      ordersList: null,
      isFetching: false,
    });

    const state = {
      Orders: OrdersListState,
      isFatching: false,
    };

    state.Orders = state.Orders.set('ordersList', {
      orders: [
        {
          orderNumber: '1111',
        },
      ],
    });

    expect(getPreviewOrderId(state)).toEqual('1111');
  });

  it('#getPreviewOrderId should return null if orders list is empty', () => {
    const OrdersListState = fromJS({
      ordersList: null,
      isFetching: false,
    });

    const state = {
      Orders: OrdersListState,
      isFatching: false,
    };

    expect(getPreviewOrderId(state)).toBeNull();
  });

  it('#getShowOrderListWithItemDetails should return true', () => {
    const session = {
      siteDetails: {
        IS_ORDERLIST_ITEMDETAILS_REQUIRED: 1,
      },
    };
    const state = {
      session,
    };
    expect(getShowOrderListWithItemDetails(state)).toBeTruthy();
  });

  it('#getShowOrderListWithItemDetails should return false', () => {
    const session = {
      siteDetails: {
        IS_ORDERLIST_ITEMDETAILS_REQUIRED: 0,
      },
    };
    const state = {
      session,
    };
    expect(getShowOrderListWithItemDetails(state)).toBeFalsy();
  });

  it('#getShowOrderListWithItemDetails should return true for showOrderListDetails in the url', () => {
    const session = {
      siteDetails: {
        IS_ORDERLIST_ITEMDETAILS_REQUIRED: 1,
      },
    };
    const state = {
      session,
    };

    const props = {
      router: {
        asPath: 'orders?showOrderListDetails=true',
      },
    };
    expect(getShowOrderListWithItemDetails(state, props)).toBeTruthy();
  });

  it('#getAccountOrdersAPISwitch should return true', () => {
    const session = {
      siteDetails: {
        IS_ORDERLIST_MICROSERVICE_ACTIVE: 1,
      },
    };
    const state = {
      session,
    };
    expect(getAccountOrdersAPISwitch(state)).toBeTruthy();
  });

  it('#getAccountOrdersAPISwitch should return false', () => {
    const session = {
      siteDetails: {
        IS_ORDERLIST_MICROSERVICE_ACTIVE: 0,
      },
    };
    const state = {
      session,
    };
    expect(getAccountOrdersAPISwitch(state)).toBeFalsy();
  });
});
