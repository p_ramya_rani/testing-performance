// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { OrdersContainer, mapDispatchToProps } from '../Orders.container';

describe('Orders Container', () => {
  it('should render OrdersContainer', () => {
    const tree = shallow(
      <OrdersContainer labels={{}} onFilterLink={jest.fn()} ordersListItems={[]} />
    );
    expect(tree).toMatchSnapshot();
  });

  it('should return an action fetchOrders which will call dispatch function on execution', () => {
    const dispatch = jest.fn();
    const dispatchProps = mapDispatchToProps(dispatch);
    dispatchProps.fetchOrders();
    expect(dispatch.mock.calls).toHaveLength(1);
  });

  it('should return getOrderDetailsAction to call dispatch function on execution', () => {
    const dispatch = jest.fn();
    const dispatchProps = mapDispatchToProps(dispatch);
    dispatchProps.getOrderDetailsAction();
    expect(dispatch.mock.calls).toHaveLength(1);
  });

  it('should update pageNumber when call loadMoreOrders', () => {
    const wrapper = shallow(
      <OrdersContainer labels={{}} onFilterLink={jest.fn()} ordersListItems={[]} />
    );
    wrapper.setState({ pageNum: '0' });
    wrapper.instance().loadMoreOrders();
    expect(wrapper.state('pageNum')).toBe(1);
  });
});
