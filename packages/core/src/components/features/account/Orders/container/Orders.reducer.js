// 9fbef606107a605d69c0edbcd8029e5d 
import { fromJS } from 'immutable';
import LOGOUT_CONSTANTS from '@tcp/core/src/components/features/account/Logout/LogOut.constants';
import constants from '../Orders.constants';

const initialState = fromJS({
  ordersList: null,
  isFetching: false,
  isLoadMoreActive: false,
  recentOrderDetails: null,
});

const OrdersReducer = (state = initialState, action) => {
  switch (action.type) {
    case constants.SHOW_LOADER:
    case constants.GET_ORDERS_LIST:
      return state.set('isFetching', true);
    case constants.SET_ORDERS_LIST:
      return state.set('ordersList', action.payload);
    case constants.HIDE_LOADER:
      return state.set('isFetching', false);
    case constants.HIDE_LOAD_MORE:
      return state.set('isLoadMoreActive', false);
    case constants.SHOW_LOAD_MORE:
      return state.set('isLoadMoreActive', true);
    case constants.SET_RECENT_ORDERDETAILS:
      return state.set('recentOrderDetails', action.payload);

    case LOGOUT_CONSTANTS.LOGOUT_APP:
      return initialState;
    default:
      // TODO: currently when initial state is hydrated on browser, List is getting converted to an JS Array
      if (state instanceof Object) {
        return fromJS(state);
      }
      return state;
  }
};

export default OrdersReducer;
