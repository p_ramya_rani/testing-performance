// 9fbef606107a605d69c0edbcd8029e5d 
import constants from '../Orders.constants';

/**
 * @function getOrdersList
 * action creator for type: GET_ORDERS_LIST
 */
export const getOrdersList = payload => ({
  type: constants.GET_ORDERS_LIST,
  payload,
});

/**
 * @function setOrdersList
 * action creator for type: SET_ORDERS_LIST
 */
export const setOrdersList = payload => {
  return {
    type: constants.SET_ORDERS_LIST,
    payload,
  };
};

export const setRecentOrderDetails = payload => {
  return {
    type: constants.SET_RECENT_ORDERDETAILS,
    payload,
  };
};

export const showLoader = () => {
  return {
    type: constants.SHOW_LOADER,
  };
};

export const hideLoader = () => {
  return {
    type: constants.HIDE_LOADER,
  };
};

/**
 * @function hideLoadMore
 * action creator for type: HIDE_LOAD_MORE
 */
export const hideLoadMore = payload => ({
  type: constants.HIDE_LOAD_MORE,
  payload,
});

export const showLoadMore = payload => ({
  type: constants.SHOW_LOAD_MORE,
  payload,
});
