// 9fbef606107a605d69c0edbcd8029e5d 
import { createSelector } from 'reselect';
import { parseBoolean, isMobileApp } from '@tcp/core/src/utils';
import { ORDERS_REDUCER_KEY } from '../../../../../constants/reducer.constants';

const getState = state => state[ORDERS_REDUCER_KEY];

/**
 * Selector function to return redux error
 * @param { object } state reduxStore state
 */
export const getOrdersListState = createSelector(
  getState,
  state => state && state.get('ordersList')
);

export const getOrdersState = createSelector(
  getOrdersListState,
  orderListState => orderListState && orderListState.orders
);

export const getOrderListFetchingState = state => {
  return getState(state).get('isFetching');
};

export const getLoadMoreState = state => {
  return getState(state).get('isLoadMoreActive');
};

export const getPreviewOrderId = createSelector(
  getOrdersListState,
  orderListState => {
    if (orderListState && orderListState.orders && orderListState.orders.length > 0) {
      return orderListState.orders[0].orderNumber;
    }
    return null;
  }
);

export const getUpcomingOrders = createSelector(
  getOrdersListState,
  orderListState => {
    if (orderListState && orderListState.orders) {
      return orderListState.orders.filter(order => order.isActiveOrder);
    }
    return null;
  }
);

export const getShowOrderListWithItemDetails = (state, props) => {
  if (
    props &&
    props.router &&
    props.router.asPath &&
    props.router.asPath.includes('showOrderListDetails')
  ) {
    return true;
  }
  if (isMobileApp()) {
    return (
      state.session &&
      state.session.siteDetails &&
      parseBoolean(state.session.siteDetails.IS_ORDERLIST_ITEMDETAILS_REQUIRED_APP)
    );
  }
  return (
    state.session &&
    state.session.siteDetails &&
    parseBoolean(state.session.siteDetails.IS_ORDERLIST_ITEMDETAILS_REQUIRED)
  );
};

export const getAccountOrdersAPISwitch = state => {
  return (
    state.session &&
    state.session.siteDetails &&
    parseBoolean(state.session.siteDetails.IS_ORDERLIST_MICROSERVICE_ACTIVE)
  );
};

export const getRecentOrderDetailsState = createSelector(
  getState,
  state => state && state.get('recentOrderDetails')
);

const getSortedList = pastOrdersListItems => {
  // Sort order list items in descending order
  if (pastOrdersListItems && pastOrdersListItems.length > 1) {
    return pastOrdersListItems.sort(
      (prev, next) => new Date(next.orderDate).getTime() - new Date(prev.orderDate).getTime()
    );
  }
  return pastOrdersListItems;
};

const getOldOrderList = orderList => {
  return orderList ? orderList.filter(order => !order.isActiveOrder) : [];
};

export const getNewOrdersListItem = (state, props) => {
  const newOrderHistory = getShowOrderListWithItemDetails(state, props);
  const ordersListItemData = getOrdersState(state) || [];

  return newOrderHistory ? getOldOrderList(ordersListItemData) : getSortedList(ordersListItemData);
};
