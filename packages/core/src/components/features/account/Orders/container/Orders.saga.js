// 9fbef606107a605d69c0edbcd8029e5d 
import { call, takeLatest, put, select } from 'redux-saga/effects';
import logger from '@tcp/core/src/utils/loggerInstance';
import constants from '../Orders.constants';
import {
  setOrdersList,
  showLoader,
  hideLoader,
  hideLoadMore,
  showLoadMore,
} from './Orders.actions';
import { getOrdersState } from './Orders.selectors';
import { getOrderHistory } from '../../../../../services/abstractors/account';

/**
 * @function getMyOrders
 * @description This function will call getOrderHistory Abstractor to get order history data
 */

export function* getMyOrders({ payload }) {
  const {
    fromLoadMore,
    loadMoreCallback,
    siteId,
    currentSiteId,
    reqParams = {},
    accountOrdersAPISwitch = false,
  } = payload;
  const orderPageSize = reqParams.pageSize || constants.ORDER_SIZE;
  const orderPageNum = reqParams.pageNumber || 0;
  try {
    yield put(showLoader());

    const orders = yield call(
      getOrderHistory,
      siteId,
      currentSiteId,
      reqParams,
      accountOrdersAPISwitch
    );

    if (
      reqParams.itemdetails &&
      (orders && Number(orders.totalRecords)) > Number(orderPageSize) * (Number(orderPageNum) + 1)
    ) {
      yield put(showLoadMore());
    } else {
      yield put(hideLoadMore());
    }

    if (fromLoadMore) {
      let ordersData = yield select(getOrdersState);

      if (!ordersData) {
        ordersData = [];
      }
      ordersData = ordersData.concat(orders.orders);
      yield put(setOrdersList({ totalPages: orders.totalPages, orders: ordersData }));
    } else {
      yield put(setOrdersList(orders));
    }
    if (loadMoreCallback) {
      loadMoreCallback();
    }
  } catch (e) {
    logger.error('getOrderHistory error', {
      error: e,
      extraData: {
        component: 'Orders Saga - getMyOrders',
        payloadRecieved: payload,
      },
    });
  } finally {
    yield put(hideLoader());
  }
}

/**
 * @function OrdersSaga
 * @description watcher function for fetch orders.
 */
export function* OrdersSaga() {
  yield takeLatest(constants.GET_ORDERS_LIST, getMyOrders);
}

export default OrdersSaga;
