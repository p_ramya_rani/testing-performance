import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import LoaderSkelton from '@tcp/core/src/components/common/molecules/LoaderSkelton';
import {
  UnderlineStyle,
  NewOrderListViewWrapper,
  StyledHeading,
  Heading,
  HeadingText,
  ButtonWrapper,
  LoadMoreButton,
  OrderHistorySpacing,
  EmptyOrdersListWrapper,
} from '../styles/OrdersList.style.native';
import EmptyOrdersList from '../molecules/EmptyOrdersList';
import RecentOrders from '../molecules/RecentOrders';
import PastOrders from '../molecules/PastOrders';
import OrderPreviewItemsList from '../molecules/OrderPreviewItemsList';
import RecentOrdersSkeleton from '../skeleton/RecentOrdersSkeleton.view.native';
import OrderPreviewItemsListSkeleton from '../skeleton/OrderPreviewItemsListSkeleton.view.native';
import OrderListWithDetails from '../molecules/OrderListWithDetails';

const NewOrderListViewSkeleton = ({ isMostRecentOrderFetching, showNewSkeleton }) => {
  return (
    isMostRecentOrderFetching &&
    showNewSkeleton && (
      <>
        <LoaderSkelton width="100%" height="300px" />
        <OrderHistorySpacing>
          <LoaderSkelton width="100%" height="300px" />
        </OrderHistorySpacing>
        <OrderHistorySpacing>
          <LoaderSkelton width="100%" height="300px" />
        </OrderHistorySpacing>
      </>
    )
  );
};

const NewOrderListView = (props) => {
  const {
    upcomingOrders,
    labels,
    ordersListItems,
    navigation,
    loadMoreOrders,
    isLoadMoreActive,
    isMostRecentOrderFetching,
    showNewSkeleton,
    helpWithOrderLabels,
    cshReturnItemOlderDays,
    isCSHAppEnabled,
    setOrderResendModalOpen,
  } = props;
  const isOrderPresent = ordersListItems && ordersListItems.length > 0;
  const isUpcomingPresent = upcomingOrders && upcomingOrders.length > 0;
  const isOrderUpcomingPresent = isOrderPresent || isUpcomingPresent;

  const OrderHistoryHeader = () => {
    return (
      <StyledHeading>
        <HeadingText
          fontSize="fs16"
          fontWeight="extrabold"
          text={getLabelValue(labels, 'lbl_orders_heading_history', 'orders')}
        />
        <Heading />
      </StyledHeading>
    );
  };
  return (
    <NewOrderListViewWrapper isOrderPresent={isOrderUpcomingPresent}>
      {isUpcomingPresent && (
        <React.Fragment>
          <StyledHeading>
            <HeadingText
              fontSize="fs16"
              fontWeight="extrabold"
              text={getLabelValue(labels, 'lbl_orders_heading_upcoming', 'orders')}
            />
            <Heading />
          </StyledHeading>
          <OrderListWithDetails
            ordersListItems={upcomingOrders}
            labels={labels}
            upcomingOrdersView
            navigation={navigation}
            helpWithOrderLabels={helpWithOrderLabels}
            cshReturnItemOlderDays={cshReturnItemOlderDays}
            isCSHAppEnabled={isCSHAppEnabled}
            setOrderResendModalOpen={setOrderResendModalOpen}
          />
        </React.Fragment>
      )}
      {isOrderPresent && (
        <>
          <OrderHistoryHeader />
          <OrderListWithDetails
            ordersListItems={ordersListItems}
            labels={labels}
            navigation={navigation}
            helpWithOrderLabels={helpWithOrderLabels}
            cshReturnItemOlderDays={cshReturnItemOlderDays}
            isCSHAppEnabled={isCSHAppEnabled}
            setOrderResendModalOpen={setOrderResendModalOpen}
          />
          <NewOrderListViewSkeleton
            isMostRecentOrderFetching={isMostRecentOrderFetching}
            showNewSkeleton={!showNewSkeleton}
          />
        </>
      )}
      {isLoadMoreActive && isOrderUpcomingPresent && (
        <ButtonWrapper>
          <LoadMoreButton
            fontFamily="secondary"
            width="200px"
            fontSize="fs14"
            text={getLabelValue(labels, 'lbl_orders_load_more', 'orders')}
            fill="WHITE"
            onPress={loadMoreOrders}
            buttonVariation="fixed-width"
            disableButton={isMostRecentOrderFetching}
          />
        </ButtonWrapper>
      )}
      {!isMostRecentOrderFetching && !isOrderUpcomingPresent && (
        <View>
          <OrderHistoryHeader />
          <EmptyOrdersListWrapper>
            <EmptyOrdersList labels={labels} navigation={navigation} />
          </EmptyOrdersListWrapper>
        </View>
      )}
      <NewOrderListViewSkeleton
        isMostRecentOrderFetching={isMostRecentOrderFetching}
        showNewSkeleton={showNewSkeleton}
      />
    </NewOrderListViewWrapper>
  );
};

NewOrderListView.propTypes = {
  upcomingOrders: PropTypes.shape([]),
  labels: PropTypes.shape({}).isRequired,
  ordersListItems: PropTypes.shape([]),
  navigation: PropTypes.shape({}).isRequired,
  loadMoreOrders: PropTypes.func.isRequired,
  isLoadMoreActive: PropTypes.bool,
  isMostRecentOrderFetching: PropTypes.bool,
  showNewSkeleton: PropTypes.bool,
};

NewOrderListView.defaultProps = {
  upcomingOrders: [],
  ordersListItems: [],
  isLoadMoreActive: false,
  isMostRecentOrderFetching: false,
  showNewSkeleton: false,
};

export const OrdersList = ({
  labels,
  ordersListItems,
  navigation,
  handleComponentChange,
  componentProps,
  recentOrderDetails,
  isMostRecentOrderFetching,
  isMostRecentOrderDetailFetching,
  showOrderListWithItemDetails,
  upcomingOrders,
  loadMoreOrders,
  isLoadMoreActive,
  showNewSkeleton,
  helpWithOrderLabels,

  cshReturnItemOlderDays,
  isCSHAppEnabled,
  setOrderResendModalOpen,
}) => {
  return showOrderListWithItemDetails ? (
    NewOrderListView({
      upcomingOrders,
      labels,
      ordersListItems,
      navigation,
      loadMoreOrders,
      isLoadMoreActive,
      isMostRecentOrderFetching,
      showNewSkeleton,
      helpWithOrderLabels,
      cshReturnItemOlderDays,
      isCSHAppEnabled,
      setOrderResendModalOpen,
    })
  ) : (
    <React.Fragment>
      <StyledHeading>
        <BodyCopy
          fontSize="fs16"
          fontWeight="extrabold"
          text={getLabelValue(labels, 'lbl_orders_heading', 'orders')}
        />
      </StyledHeading>
      <UnderlineStyle />
      {isMostRecentOrderFetching && <RecentOrdersSkeleton labels={labels} />}
      {!isMostRecentOrderFetching && (
        <RecentOrders
          labels={labels}
          ordersListItems={ordersListItems}
          navigation={navigation}
          handleComponentChange={handleComponentChange}
          componentProps={componentProps}
        />
      )}
      {isMostRecentOrderDetailFetching && <OrderPreviewItemsListSkeleton />}
      {!isMostRecentOrderDetailFetching && recentOrderDetails && recentOrderDetails.length > 0 && (
        <OrderPreviewItemsList
          labels={labels}
          navigation={navigation}
          items={recentOrderDetails}
          orderNumber={ordersListItems[0].orderNumber}
        />
      )}
      {ordersListItems && ordersListItems.length > 1 ? (
        <PastOrders
          labels={labels}
          ordersListItems={ordersListItems}
          navigation={navigation}
          handleComponentChange={handleComponentChange}
          componentProps={componentProps}
        />
      ) : null}
    </React.Fragment>
  );
};

OrdersList.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  navigation: PropTypes.shape({}).isRequired,
  ordersListItems: PropTypes.shape([]).isRequired,
  handleComponentChange: PropTypes.func,
  componentProps: PropTypes.shape({}),
  recentOrderDetails: PropTypes.shape([]).isRequired,
  isMostRecentOrderFetching: PropTypes.bool,
  isMostRecentOrderDetailFetching: PropTypes.bool,
  upcomingOrders: PropTypes.shape([]),
  showOrderListWithItemDetails: PropTypes.bool,
  loadMoreOrders: PropTypes.func,
  isLoadMoreActive: PropTypes.bool,
  showNewSkeleton: PropTypes.bool,
};
OrdersList.defaultProps = {
  handleComponentChange: () => {},
  componentProps: {},
  isMostRecentOrderFetching: false,
  isMostRecentOrderDetailFetching: false,
  showOrderListWithItemDetails: false,
  upcomingOrders: [],
  loadMoreOrders: () => {},
  isLoadMoreActive: false,
  showNewSkeleton: false,
};

export default OrdersList;
