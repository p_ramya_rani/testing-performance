// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { OrdersList, OrderListContent } from '../OrdersList.view';

describe('OrdersList View component', () => {
  it('should renders correctly', () => {
    const props = {
      labels: {},
      ordersListItems: {},
    };
    const component = shallow(<OrdersList {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('OrderListContent should render correctly', () => {
    const props = {
      labels: {},
      ordersListItems: {},
      showOrderListWithItemDetails: true,
    };
    const component = shallow(<OrderListContent {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('OrdersList should render correctly', () => {
    const props = {
      labels: {},
      ordersListItems: {},
      showOrderListWithItemDetails: true,
    };
    const component = shallow(<OrdersList {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('should render load more button', () => {
    const props = {
      labels: {},
      isLoadMoreActive: true,
      showOrderListWithItemDetails: true,
      ordersListItems: [{}, {}],
    };
    const component = shallow(<OrderListContent {...props} />);
    expect(component.find('.load-more')).toHaveLength(1);
  });

  it('should hide load more button', () => {
    const props = {
      labels: {},
      isLoadMoreActive: false,
      showOrderListWithItemDetails: true,
      ordersListItems: [],
    };
    const component = shallow(<OrderListContent {...props} />);
    expect(component.find('.load-more')).toHaveLength(0);
  });

  it('should renders correctly when props are true', () => {
    const props = {
      isMostRecentOrderFetching: true,
      isMostRecentOrderDetailFetching: true,
      isLoadMoreActive: true,
    };
    const component = shallow(<OrdersList {...props} />);
    expect(component).toMatchSnapshot();
  });
});
