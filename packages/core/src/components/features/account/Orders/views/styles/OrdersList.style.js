// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  ${props =>
    props.showOrderListWithItemDetails
      ? `background-color: ${props.theme.colorPalette.gray['300']};
      @media ${props.theme.mediaQuery.smallOnly} {
        margin-left: -14px;
        margin-right: -14px;
      }
      @media ${props.theme.mediaQuery.mediumOnly} {
        margin: -${props.theme.spacing.ELEM_SPACING.XL} -15px 0;
        padding: ${props.theme.spacing.ELEM_SPACING.XL} 15px 0;
      }
      @media ${props.theme.mediaQuery.large} {
        background-color: ${props.theme.colors.WHITE};
      }`
      : ''}
  .load-more {
    cursor: pointer;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    background-color: ${props => props.theme.colors.WHITE};
    padding-top: ${props => props.theme.spacing.ELEM_SPACING.MED};
    -webkit-tap-highlight-color: transparent;
    @media ${props => props.theme.mediaQuery.smallOnly} {
      padding-top: ${props => props.theme.spacing.ELEM_SPACING.XXL};
    }
    .load-more-cta {
      width: 200px;
    }
  }
  .load-more-cta:focus {
    background-color: ${props => props.theme.colors.WHITE};
    outline: none;
  }
  .load-more-cta:hover {
    border: solid 1px ${props => props.theme.colors.TEXT.GRAY};
    background-color: ${props => props.theme.colors.PRIMARY.PALEGRAY};
  }
  ${props => (props.showOrderListWithItemDetails ? 'min-height:600px;' : '')}
  .page-heading {
    text-transform: uppercase;
    background-color: ${props => props.theme.colors.WHITE};
  }
  .empty-orders-list-wrapper {
    padding-top: 50px;
    @media ${props => props.theme.mediaQuery.smallOnly} {
      padding-top: 100px;
    }
  }
  position: relative;
`;

export default styles;
