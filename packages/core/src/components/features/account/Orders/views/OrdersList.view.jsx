// 9fbef606107a605d69c0edbcd8029e5d
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { Button } from '@tcp/core/src/components/common/atoms';
import FormPageHeading from '../../common/molecule/FormPageHeading';
import RecentOrders from '../molecules/RecentOrders';
import OrdersLinks from '../molecules/OrdersLinks';
import PastOrders from '../molecules/PastOrders';
import OrderPreviewItemsList from '../molecules/OrderPreviewItemsList';
import RecentOrdersSkeleton from '../skeleton/RecentOrdersSkeleton.view';
import styles from './styles/OrdersList.style';
import OrderPreviewItemsListSkeleton from '../skeleton/OrderPreviewItemsListSkeleton.view';
import OrderListWithDetails from '../molecules/OrderListWithDetails';
import EmptyOrdersList from '../molecules/EmptyOrdersList';

/**
 * This component will render OrdersList component
 * @param { string, object }
 */
const isShowPreviewList = (isMostRecentOrderDetailFetching, orderItems, ordersListItems) => {
  return (
    !isMostRecentOrderDetailFetching &&
    orderItems &&
    orderItems.length > 0 &&
    ordersListItems &&
    ordersListItems.length
  );
};
export const OrderListContent = ({
  ordersListItems,
  labels,
  isLoadMoreActive,
  loadMoreOrders,
  showOrderListWithItemDetails,
  isMostRecentOrderFetching,
  upcomingOrdersPresent,
  helpWithOrderLabels,
  cshReturnItemOlderDays,
  setOrderResendModalOpen,
  isCSHEnabled,
  ...otherprops
}) => {
  const orderListItemsPresent = ordersListItems && ordersListItems.length;
  return (
    <>
      {orderListItemsPresent ? (
        <FormPageHeading
          className="elem-mb-XL page-heading"
          heading={getLabelValue(labels, 'lbl_orders_heading_history', 'orders')}
        />
      ) : null}

      {orderListItemsPresent || upcomingOrdersPresent ? (
        <div className="bottom-section-container">
          {orderListItemsPresent ? (
            <OrderListWithDetails
              ordersListItems={ordersListItems}
              helpWithOrderLabels={helpWithOrderLabels}
              cshReturnItemOlderDays={cshReturnItemOlderDays}
              setOrderResendModalOpen={setOrderResendModalOpen}
              isCSHEnabled={isCSHEnabled}
              labels={labels}
            />
          ) : null}
          {isLoadMoreActive && (
            <div className="load-more">
              <Button
                className="load-more-cta"
                type="button"
                aria-label="LoadMore"
                fill="WHITE"
                fontSize="fs12"
                fontWeight="extrabold"
                color="darkergray"
                buttonVariation="fixed-width"
                onClick={loadMoreOrders}
                disabled={isMostRecentOrderFetching}
              >
                {getLabelValue(labels, 'lbl_orders_load_more', 'orders')}
              </Button>
            </div>
          )}
        </div>
      ) : (
        <div className="empty-orders-list-wrapper">
          <EmptyOrdersList labels={labels} />
        </div>
      )}
      <OrdersLinks
        labels={labels}
        showOrderListWithItemDetails={showOrderListWithItemDetails}
        isPageEmpty={!orderListItemsPresent && !upcomingOrdersPresent}
        helpWithOrderLabels={helpWithOrderLabels}
        cshReturnItemOlderDays={cshReturnItemOlderDays}
        setOrderResendModalOpen={setOrderResendModalOpen}
        isCSHEnabled={isCSHEnabled}
        {...otherprops}
      />
    </>
  );
};

OrderListContent.propTypes = {
  isLoadMoreActive: PropTypes.bool,
  showOrderListWithItemDetails: PropTypes.bool,
  upcomingOrdersPresent: PropTypes.bool.isRequired,
  ordersListItems: PropTypes.shape([]).isRequired,
  loadMoreOrders: PropTypes.func.isRequired,
  labels: PropTypes.shape({}).isRequired,
  isMostRecentOrderFetching: PropTypes.bool,
};
OrderListContent.defaultProps = {
  isLoadMoreActive: true,
  showOrderListWithItemDetails: true,
  isMostRecentOrderFetching: false,
};
const OrdersList = ({
  labels,
  orderItems,
  ordersListItems,
  isMostRecentOrderFetching,
  isMostRecentOrderDetailFetching,
  showOrderListWithItemDetails,
  className,
  upcomingOrders,
  loadMoreOrders,
  isLoadMoreActive,
  helpWithOrderLabels,
  cshReturnItemOlderDays,
  setOrderResendModalOpen,
  isCSHEnabled,
  ...otherprops
}) => {
  useEffect(() => {
    if (showOrderListWithItemDetails) {
      window.scrollTo(0, 0);
    }
  }, []);
  if (showOrderListWithItemDetails) {
    const upcomingOrdersPresent = upcomingOrders && upcomingOrders.length;
    return (
      <div className={className}>
        {upcomingOrdersPresent ? (
          <>
            <FormPageHeading
              className="elem-mb-XL page-heading"
              heading={getLabelValue(labels, 'lbl_orders_heading_upcoming', 'orders')}
            />
            <OrderListWithDetails
              ordersListItems={upcomingOrders}
              labels={labels}
              helpWithOrderLabels={helpWithOrderLabels}
              cshReturnItemOlderDays={cshReturnItemOlderDays}
              upcomingOrdersView
              setOrderResendModalOpen={setOrderResendModalOpen}
              isCSHEnabled={isCSHEnabled}
            />
          </>
        ) : null}
        <OrderListContent
          loadMoreOrders={loadMoreOrders}
          ordersListItems={ordersListItems}
          labels={labels}
          upcomingOrdersPresent={upcomingOrdersPresent}
          isLoadMoreActive={isLoadMoreActive}
          {...otherprops}
          showOrderListWithItemDetails={showOrderListWithItemDetails}
          isMostRecentOrderFetching={isMostRecentOrderFetching}
          helpWithOrderLabels={helpWithOrderLabels}
          cshReturnItemOlderDays={cshReturnItemOlderDays}
          setOrderResendModalOpen={setOrderResendModalOpen}
          isCSHEnabled={isCSHEnabled}
        />
      </div>
    );
  }
  return (
    <React.Fragment>
      <FormPageHeading
        className="elem-mb-XL myAccountRightView"
        heading={getLabelValue(labels, 'lbl_orders_heading', 'orders')}
        data-locator="OrdersListPageLbl"
      />
      <OrdersLinks labels={labels} {...otherprops} />
      {isMostRecentOrderFetching && <RecentOrdersSkeleton labels={labels} />}
      {!isMostRecentOrderFetching && (
        <RecentOrders labels={labels} ordersListItems={ordersListItems} />
      )}
      {isMostRecentOrderDetailFetching && <OrderPreviewItemsListSkeleton />}

      {isShowPreviewList(isMostRecentOrderDetailFetching, orderItems, ordersListItems) ? (
        <OrderPreviewItemsList
          labels={labels}
          items={orderItems.slice(0, 3)}
          orderNumber={ordersListItems[0].orderNumber}
          trackingUrl={ordersListItems[0].orderTrackingUrl}
        />
      ) : null}
      {ordersListItems && ordersListItems.length > 1 ? (
        <PastOrders labels={labels} ordersListItems={ordersListItems} />
      ) : null}
    </React.Fragment>
  );
};

OrdersList.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  ordersListItems: PropTypes.shape([]).isRequired,
  orderItems: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  isMostRecentOrderFetching: PropTypes.bool,
  isMostRecentOrderDetailFetching: PropTypes.bool,
  showOrderListWithItemDetails: PropTypes.bool,
  className: PropTypes.string,
  loadMoreOrders: PropTypes.func.isRequired,
  upcomingOrders: PropTypes.shape([]),
  isLoadMoreActive: PropTypes.bool,
};

OrdersList.defaultProps = {
  isMostRecentOrderFetching: false,
  isMostRecentOrderDetailFetching: false,
  showOrderListWithItemDetails: false,
  className: '',
  upcomingOrders: [],
  isLoadMoreActive: true,
};

export default withStyles(OrdersList, styles);
export { OrdersList };
