// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { OrdersList } from '../OrdersList.view.native';
import { ButtonWrapper } from '../../styles/OrdersList.style.native';

const ordersListItems = [
  {
    currencySymbol: '$',
    isBOSSOrder: false,
    isCanadaOrder: false,
    isEcomOrder: true,
    orderDate: 'Oct 4, 2019',
    orderNumber: '7000037172',
    orderStatus: 'lbl_orders_statusNa',
    orderTotal: '$24.95',
    orderTracking: 'N/A',
    orderTrackingUrl:
      'https://childrensplace.narvar.com/childrensplace/tracking/ups?order_number=7000037172&order_date=2019-10-04T07:22:00-04:00&dzip=10036&locale=en_US&type=p',
  },
  {
    currencySymbol: '$',
    isBOSSOrder: false,
    isCanadaOrder: false,
    isEcomOrder: true,
    orderDate: 'Oct 4, 2019',
    orderNumber: '7000036213',
    orderStatus: 'lbl_orders_statusNa',
    orderTotal: '$83.45',
    orderTracking: 'N/A',
    orderTrackingUrl:
      'https://childrensplace.narvar.com/childrensplace/tracking/ups?order_number=7000036213&order_date=2019-10-04T07:17:00-04:00&dzip=10036&locale=en_US&type=p',
  },
];

describe('OrdersList View component', () => {
  it('should renders correctly', () => {
    const props = {
      labels: {},
      ordersListItems: [{}],
    };
    const component = shallow(<OrdersList {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders correctly', () => {
    const props = {
      labels: {},
      ordersListItems,
    };
    const component = shallow(<OrdersList {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders correctly upcoming orders and order history', () => {
    const props = {
      labels: {},
      ordersListItems,
      showOrderListWithItemDetails: true,
      upcomingOrders: ordersListItems,
    };
    const component = shallow(<OrdersList {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders correctly upcoming orders', () => {
    const props = {
      labels: {},
      ordersListItems: [{}],
      showOrderListWithItemDetails: true,
      upcomingOrders: ordersListItems,
    };
    const component = shallow(<OrdersList {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders load more button', () => {
    const props = {
      labels: {},
      isLoadMoreActive: true,
      showOrderListWithItemDetails: true,
      ordersListItems,
    };
    const component = shallow(<OrdersList {...props} />);
    expect(component.find(ButtonWrapper)).toHaveLength(1);
  });

  it('should hide load more button', () => {
    const props = {
      labels: {},
      isLoadMoreActive: false,
      showOrderListWithItemDetails: true,
      ordersListItems: [],
    };
    const component = shallow(<OrdersList {...props} />);
    expect(component.find(ButtonWrapper)).toHaveLength(0);
  });
  it('should renders new order history correctly', () => {
    const props = {
      labels: {},
      isLoadMoreActive: true,
      showOrderListWithItemDetails: true,
      ordersListItems,
    };
    const component = shallow(<OrdersList {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('should render new empty order history correctly', () => {
    const props = {
      labels: {},
      showOrderListWithItemDetails: true,
      ordersListItems: [],
      isMostRecentOrderFetching: false,
    };
    const component = shallow(<OrdersList {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('should render new loader correctly', () => {
    const props = {
      labels: {},
      showOrderListWithItemDetails: true,
      ordersListItems: [],
      isMostRecentOrderFetching: true,
      showNewSkeleton: true,
    };
    const component = shallow(<OrdersList {...props} />);
    expect(component).toMatchSnapshot();
  });
});
