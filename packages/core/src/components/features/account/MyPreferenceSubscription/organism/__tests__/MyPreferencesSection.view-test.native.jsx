// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { MyPrefrenceSectionVanilla } from '../MyPreferencesSection.view.native';

describe('ProfileInformation component', () => {
  it('should render correctly', () => {
    const labels = {};

    const component = shallow(<MyPrefrenceSectionVanilla labels={labels} />);
    expect(component).toMatchSnapshot();
  });
});
