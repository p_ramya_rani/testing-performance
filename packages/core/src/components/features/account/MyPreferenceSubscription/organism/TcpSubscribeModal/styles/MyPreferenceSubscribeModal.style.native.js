// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

export const RichTextWrapper = styled.View`
  min-height: 170px;
  overflow: hidden;
  margin-bottom: 10px;
`;

export const ButtonWrapper = styled.View`
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.LRG};
`;

export default RichTextWrapper;
