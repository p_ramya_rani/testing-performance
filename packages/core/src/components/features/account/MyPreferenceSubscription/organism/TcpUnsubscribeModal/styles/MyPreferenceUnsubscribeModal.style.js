// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

export default css`
  .myPreferenceModalWrapper {
    margin: 0 50px;
  }

  .disclaimer-sub-text {
    margin-bottom: ${props => props.theme.spacing.LAYOUT_SPACING.LRG};
  }
`;
