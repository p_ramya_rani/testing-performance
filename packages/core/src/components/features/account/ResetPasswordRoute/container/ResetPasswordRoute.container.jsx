// 9fbef606107a605d69c0edbcd8029e5d 
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { routerPush } from '@tcp/core/src/utils';
import ResetPassword from '@tcp/core/src/components/features/account/ResetPassword';
import { getGlobalLabels } from '../../Account/container/Account.selectors';
import ResetPasswordPageContainer from '../views/ResetPasswordPage';
import { getUrlParameter } from '../../../../../utils';

const backToLogin = () => {
  routerPush('/home?target=login', '/home/login');
};
export class ResetPasswordRouteContainer extends PureComponent {
  static propTypes = {
    labels: PropTypes.shape({}).isRequired,
  };

  state = {
    logonId: null,
    loading: true,
  };

  componentDidMount() {
    const emailParam = getUrlParameter('email');
    if (emailParam.length > 0) {
      this.setState({ logonId: atob(emailParam), loading: false });
    } else {
      this.setState({ loading: false });
    }
  }

  render() {
    const { labels } = this.props;
    const { logonId, loading } = this.state;
    return (
      <>
        {!loading && (
          <>
            {!logonId && <ResetPasswordPageContainer labels={labels} />}
            {logonId && (
              <ResetPassword
                labels={labels.password}
                fullPageView
                logonId={logonId}
                backToLoginAction={backToLogin}
                showResendCta
              />
            )}
          </>
        )}
      </>
    );
  }
}

export const mapStateToProps = state => ({
  labels: getGlobalLabels(state),
});

export default connect(mapStateToProps)(ResetPasswordRouteContainer);
