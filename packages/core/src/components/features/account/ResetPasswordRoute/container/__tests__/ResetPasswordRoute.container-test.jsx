// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import ResetPassword from '@tcp/core/src/components/features/account/ResetPassword';
import { ResetPasswordRouteContainer } from '../ResetPasswordRoute.container';
import ResetPasswordPageContainer from '../../views/ResetPasswordPage';

describe('ResetPasswordRoute container', () => {
  let component;
  beforeEach(() => {
    const props = {
      labels: {},
    };
    component = shallow(<ResetPasswordRouteContainer {...props} />);
  });

  it('should render ResetPassword Page', () => {
    expect(component.find(ResetPasswordPageContainer)).toHaveLength(1);
  });

  it('should render ResetPassword component', () => {
    component.setState({ logonId: true });
    expect(component.find(ResetPassword)).toHaveLength(1);
  });
});
