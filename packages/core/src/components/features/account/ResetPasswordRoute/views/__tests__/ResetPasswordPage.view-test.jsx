// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import ResetPasswordPage from '../ResetPasswordPage';

describe('ResetPassword component', () => {
  const props = {
    labels: {
      lbl_ResetPassword_backLogin: 'back to login',
      lbl_ResetPassword_requirementNote: 'note:',
      lbl_ResetPassword_requirementTips_1: 'requirement tip 1',
      lbl_ResetPassword_requirementTips_2: 'requirement tip 2',
      lbl_ResetPassword_requirementTips_3: 'requirement tip 3',
    },
    successMessage: 'SUCCESS',
    errorMessage: 'ERROR',
    onSubmit: () => {},
    onBack: () => {},
  };
  it('should renders correctly', () => {
    const component = shallow(<ResetPasswordPage {...props} />);
    expect(component).toMatchSnapshot();
  });
});
