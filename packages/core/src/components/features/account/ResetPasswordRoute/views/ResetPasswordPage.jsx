// 9fbef606107a605d69c0edbcd8029e5d 
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ForgotPasswordContainer from '@tcp/core/src/components/features/account/ForgotPassword/container/ForgotPassword.container';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import styles from './styles/ResetPasswordPage.style';
import BodyCopy from '../../../../common/atoms/BodyCopy';

class ResetPasswordPage extends PureComponent {
  render() {
    const { labels, className } = this.props;
    return (
      <React.Fragment>
        <BodyCopy component="div" className={className}>
          <ForgotPasswordContainer
            showForgotPasswordForm={this.showLoginForm}
            labels={labels}
            fullPageView
          />
        </BodyCopy>
      </React.Fragment>
    );
  }
}

ResetPasswordPage.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  className: PropTypes.string.isRequired,
};

export default withStyles(ResetPasswordPage, styles);
