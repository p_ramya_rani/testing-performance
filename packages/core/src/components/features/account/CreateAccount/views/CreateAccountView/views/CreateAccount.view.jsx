// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '../../../../../../common/hoc/withStyles';
import CreateAccounPage from '../../../organisms/CreateAccountPage';
import Styles from '../styles/CreateAccount.style';

class CreateAccount extends React.Component {
  static propTypes = {
    className: PropTypes.string,
    createAccountAction: PropTypes.func.isRequired,
    isIAgreeChecked: PropTypes.bool.isRequired,
    hideShowPwd: PropTypes.bool.isRequired,
    confirmHideShowPwd: PropTypes.bool.isRequired,
    labels: PropTypes.shape({}).isRequired,
    error: PropTypes.shape({}).isRequired,
    onAlreadyHaveAnAccountClick: PropTypes.func.isRequired,
    onRequestClose: PropTypes.func.isRequired,
    openModal: PropTypes.func.isRequired,
    formErrorMessage: PropTypes.shape({}).isRequired,
    isUserLoggedIn: PropTypes.bool.isRequired,
    userplccCardNumber: PropTypes.string.isRequired,
    userplccCardId: PropTypes.string.isRequired,
    isLoading: PropTypes.bool.isRequired,
    isResetPassCodeValidationEnabled: PropTypes.bool.isRequired,
    closeOverlay: PropTypes.func.isRequired,
    existingUserInformation: PropTypes.shape({}),
    fullPageView: PropTypes.bool,
    formSubmissionTriggered: PropTypes.bool,
    showSkeleton: PropTypes.bool,
  };

  static defaultProps = {
    className: '',
    existingUserInformation: {},
    fullPageView: false,
    formSubmissionTriggered: false,
    showSkeleton: false,
  };

  showForgotPasswordForm = () => {
    const { openModal } = this.props;
    openModal({
      component: 'login',
      componentProps: {
        currentForm: 'forgotPassword',
      },
    });
  };

  render() {
    const {
      className,
      createAccountAction,
      isIAgreeChecked,
      hideShowPwd,
      confirmHideShowPwd,
      labels,
      error,
      onAlreadyHaveAnAccountClick,
      onRequestClose,
      isUserLoggedIn,
      formErrorMessage,
      userplccCardNumber,
      userplccCardId,
      isLoading,
      closeOverlay,
      isResetPassCodeValidationEnabled,
      existingUserInformation,
      fullPageView,
      formSubmissionTriggered,
      showSkeleton,
    } = this.props;
    return (
      <div className={className}>
        <CreateAccounPage
          className={className}
          createAccountAction={createAccountAction}
          labels={labels}
          isIAgreeChecked={isIAgreeChecked}
          hideShowPwd={hideShowPwd}
          confirmHideShowPwd={confirmHideShowPwd}
          error={error}
          onAlreadyHaveAnAccountClick={onAlreadyHaveAnAccountClick}
          onRequestClose={onRequestClose}
          showForgotPasswordForm={this.showForgotPasswordForm}
          isUserLoggedIn={isUserLoggedIn}
          formErrorMessage={formErrorMessage}
          userplccCardNumber={userplccCardNumber}
          userplccCardId={userplccCardId}
          isLoading={isLoading}
          closeOverlay={closeOverlay}
          isResetPassCodeValidationEnabled={isResetPassCodeValidationEnabled}
          existingUserInformation={existingUserInformation}
          fullPageView={fullPageView}
          formSubmissionTriggered={formSubmissionTriggered}
          showSkeleton={showSkeleton}
        />
      </div>
    );
  }
}

export default withStyles(CreateAccount, Styles);
export { CreateAccount as CreateAccountVanilla };
