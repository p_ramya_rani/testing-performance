// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const Styles = css`
  .forgotPasswordWrapper {
    padding: ${props => props.theme.spacing.ELEM_SPACING.XXL};
  }
`;

export default Styles;
