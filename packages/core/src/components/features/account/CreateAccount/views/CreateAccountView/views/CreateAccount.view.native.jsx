// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { View } from 'react-native';
import PropTypes, { shape } from 'prop-types';
import createThemeColorPalette from '@tcp/core/styles/themes/createThemeColorPalette';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import withStyles from '../../../../../../common/hoc/withStyles';
import CreateAccounPage from '../../../organisms/CreateAccountPage';
import ForgotPasswordContainer from '../../../../ForgotPassword/container/ForgotPassword.container';
import ResetPassword from '../../../../ResetPassword';
import { Styles } from '../styles/CreateAccount.style.native';
import {
  FormStyleView,
  DescriptionStyle,
} from '../../../../LoginPage/molecules/LoginForm/LoginForm.style.native';
import CustomButton from '../../../../../../common/atoms/Button';
import BodyCopy from '../../../../../../common/atoms/BodyCopy';

const colorPallete = createThemeColorPalette();

class CreateAccount extends React.Component {
  static propTypes = {
    className: PropTypes.string,
    createAccountAction: PropTypes.func,
    isIAgreeChecked: PropTypes.string,
    hideShowPwd: PropTypes.string,
    confirmHideShowPwd: PropTypes.string,
    labels: {},
    error: {},
    onAlreadyHaveAnAccountClick: PropTypes.func,
    onRequestClose: PropTypes.func,
    showLogin: PropTypes.func.isRequired,
    userplccCardNumber: PropTypes.string,
    userplccCardId: PropTypes.string,
    toastMessage: PropTypes.func,
    passwordLabels: shape({}).isRequired,
    formErrorMessage: shape({}).isRequired,
    updateHeader: PropTypes.func,
    showCheckoutModal: PropTypes.func.isRequired,
    isResetPassCodeValidationEnabled: PropTypes.bool.isRequired,
    existingUserInformation: PropTypes.shape({}),
    loyaltyPageName: PropTypes.string,
    isLoading: PropTypes.bool.isRequired,
  };

  static defaultProps = {
    isIAgreeChecked: '',
    className: '',
    confirmHideShowPwd: '',
    createAccountAction: '',
    hideShowPwd: '',
    labels: PropTypes.shape({}),
    error: {},
    onAlreadyHaveAnAccountClick: () => {},
    onRequestClose: () => {},
    userplccCardNumber: '',
    userplccCardId: '',
    toastMessage: () => {},
    updateHeader: () => {},
    existingUserInformation: {},
    loyaltyPageName: 'account',
  };

  constructor(props) {
    super(props);
    this.state = {
      resetPassword: false,
      newPassword: false,
    };
  }

  showForgotPasswordForm = () => {
    const { resetPassword } = this.state;
    this.setState({
      resetPassword: !resetPassword,
    });
  };

  toggleCheckoutModal = () => {
    const { showCheckoutModal } = this.props;
    showCheckoutModal();
    this.showForgotPasswordForm();
  };

  showNewPassword = () => {
    const { newPassword, resetPassword } = this.state;
    this.setState({
      newPassword: !newPassword,
    });
    if (resetPassword) this.showForgotPasswordForm(); // if user is on forgot password then dismiss it
  };

  showCreateAccountCTA = (isResetPassCodeValidationEnabled, resetPassword, newPassword) =>
    !(isResetPassCodeValidationEnabled && (resetPassword || newPassword));

  render() {
    const {
      className,
      createAccountAction,
      isIAgreeChecked,
      hideShowPwd,
      confirmHideShowPwd,
      labels,
      error,
      onAlreadyHaveAnAccountClick,
      onRequestClose,
      showLogin,
      userplccCardNumber,
      userplccCardId,
      toastMessage,
      formErrorMessage,
      passwordLabels,
      updateHeader,
      isResetPassCodeValidationEnabled,
      existingUserInformation,
      loyaltyPageName,
      isLoading,
    } = this.props;

    const { resetPassword, newPassword } = this.state;
    return (
      <View className={className}>
        {!resetPassword && (
          <CreateAccounPage
            className={className}
            createAccountAction={createAccountAction}
            labels={labels}
            isIAgreeChecked={isIAgreeChecked}
            hideShowPwd={hideShowPwd}
            confirmHideShowPwd={confirmHideShowPwd}
            error={error}
            onAlreadyHaveAnAccountClick={onAlreadyHaveAnAccountClick}
            onRequestClose={onRequestClose}
            showForgotPasswordForm={this.showForgotPasswordForm}
            showLogin={showLogin}
            userplccCardNumber={userplccCardNumber}
            formErrorMessage={formErrorMessage}
            userplccCardId={userplccCardId}
            toastMessage={toastMessage}
            passwordLabels={passwordLabels}
            existingUserInformation={existingUserInformation}
            loyaltyPageName={loyaltyPageName}
            isLoading={isLoading}
          />
        )}
        {resetPassword && (
          <>
            <ForgotPasswordContainer
              onRequestClose={onRequestClose}
              showForgotPasswordForm={this.showForgotPasswordForm}
              labels={labels}
              showLogin={showLogin}
              updateHeader={updateHeader}
              toastMessage={toastMessage}
              resetPassword={resetPassword}
              showNewPassword={this.showNewPassword}
              isResetPassCodeValidationEnabled={isResetPassCodeValidationEnabled}
            />
            {this.showCreateAccountCTA(
              isResetPassCodeValidationEnabled,
              resetPassword,
              newPassword
            ) && (
              <FormStyleView>
                <DescriptionStyle>
                  <BodyCopy
                    fontFamily="secondary"
                    fontWeight="regular"
                    fontSize="fs12"
                    color="gray.900"
                    text={getLabelValue(labels, 'lbl_login_createAccountHelp_1', 'login')}
                  />
                  <BodyCopy
                    fontFamily="secondary"
                    fontWeight="regular"
                    fontSize="fs12"
                    color="gray.900"
                    text={getLabelValue(labels, 'lbl_login_createAccountHelp_2', 'login')}
                  />
                </DescriptionStyle>
                <CustomButton
                  color={colorPallete.text.secondary}
                  fill="WHITE"
                  type="submit"
                  data-locator=""
                  text={getLabelValue(labels, 'lbl_login_createAccountCTA', 'login')}
                  onPress={this.toggleCheckoutModal}
                />
              </FormStyleView>
            )}
          </>
        )}

        {newPassword && (
          <ResetPassword
            onRequestClose={onRequestClose}
            labels={labels.password}
            showLogin={showLogin}
            showNewPassword={this.showNewPassword}
            isResetPassCodeValidationEnabled={isResetPassCodeValidationEnabled}
            updateHeader={updateHeader}
          />
        )}
      </View>
    );
  }
}

export default withStyles(CreateAccount, Styles);
export { CreateAccount as CreateAccountVanilla };
