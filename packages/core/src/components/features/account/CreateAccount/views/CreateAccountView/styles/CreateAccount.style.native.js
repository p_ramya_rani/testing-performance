// 9fbef606107a605d69c0edbcd8029e5d 
import styled, { css } from 'styled-components';

const Styles = css`
  padding: 2px;
`;

const ForgotPwdWrapper = styled.View`
  display: flex;
  justify-content: ${'center'};
  align-items: center;
  padding-top: 10px;
`;

export { Styles, ForgotPwdWrapper };
