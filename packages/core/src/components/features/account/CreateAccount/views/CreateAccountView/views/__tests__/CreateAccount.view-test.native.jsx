// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { CreateAccountVanilla } from '../CreateAccount.view.native';

describe('Create Account Page', () => {
  const props = {
    isIAgreeChecked: '',
    confirmHideShowPwd: '',
    createAccountAction: '',
    hideShowPwd: '',
    labels: {},
    error: {},
    onAlreadyHaveAnAccountClick: () => {},
    onRequestClose: () => {},
    userplccCardNumber: '',
    userplccCardId: '',
    toastMessage: () => {},
    updateHeader: () => {},
  };
  it('should render correctly', () => {
    const tree = shallow(<CreateAccountVanilla {...props} createAccountAction={jest.fn()} />);
    tree.setState({ resetPassword: false });
    expect(tree).toMatchSnapshot();
  });

  it('should render correctly Forgot Password', () => {
    const tree = shallow(<CreateAccountVanilla {...props} createAccountAction={jest.fn()} />);
    tree.setState({ resetPassword: true });
    expect(tree).toMatchSnapshot();
  });

  it('should render correctly OTP Forgot Password view', () => {
    const tree = shallow(
      <CreateAccountVanilla createAccountAction={jest.fn()} isResetPassCodeValidationEnabled />
    );
    tree.setState({ resetPassword: true });
    expect(tree).toMatchSnapshot();
  });
});
