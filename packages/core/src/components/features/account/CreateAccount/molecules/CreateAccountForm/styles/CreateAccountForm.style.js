// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const Styles = css`
  .remember-me-text {
    display: block;
  }
  .already-account {
    font-size: ${props => props.theme.typography.fontSizes.fs14};
    color: ${props => props.theme.colorPalette.gray[900]};
  }
  .i-agree-checkbox {
    padding-top: 10px;
  }
  .checkbox-hide-show {
    background: ${props => props.theme.colors.WHITE};
  }
  .tooltip-bubble {
    li {
      text-align: left;

      &:before {
        content: '-';
        text-indent: -5px;
        margin-right: ${props => props.theme.spacing.ELEM_SPACING.XXXS};
      }
    }
  }
  .Checkbox__error {
    margin-top: ${props => props.theme.spacing.ELEM_SPACING.XXS};
    padding-left: 37px;
  }
  #checkbox__error__iAgree {
    margin-top: 0px;
  }
  .checkbox-hide-show label {
    margin-top: ${props => props.theme.spacing.ELEM_SPACING.XXXS};
    min-height: 0px;
  }
  .CheckBox__text {
    line-height: 10px;
  }
`;

export default Styles;
