// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { PropTypes } from 'prop-types';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import Anchor from '@tcp/core/src/components/common/atoms/Anchor';
import BrandLogo from '@tcp/web/src/components/common/atoms/BrandLogo';
import { routerPush, getBrand } from '@tcp/core/src/utils';
import { Image, BodyCopy } from '../../../../../../common/atoms';
import { getIconPath, isCanada, getImageFilePath } from '../../../../../../../utils';
import config from '../../../config';

/**
 * @param {string} props : props for CustomIcon
 * @return {JSX} IconClass : Return jsx icon component
 * @desc This method based on the props generate icon component.
 */

const showForgotPassword = (
  showForgotPasswordForm,
  isResetPassCodeValidationEnabled,
  closeOverlay
) => {
  if (isResetPassCodeValidationEnabled) {
    closeOverlay();
    routerPush(`/change-password?page=reset`, `/change-password?page=reset`);
  } else {
    showForgotPasswordForm();
  }
};

const CreateAccountTopSectionFullPage = (props) => {
  const { labels, showForgotPasswordForm, closeOverlay, isResetPassCodeValidationEnabled } = props;
  const brand = getBrand();

  return (
    <BodyCopy component="div" className="elem-pr-XS elem-pl-XS elem-pt-XXXL">
      {!isCanada() && (
        <BodyCopy component="div" className="img-parent align-center">
          {config[brand] && (
            <>
              <BrandLogo
                alt={getLabelValue(labels, `lbl_createAccount_brand_alt_${brand}`, 'registration')}
                className="header-brand__home-logo--brand"
                dataLocator={config[brand].dataLocator}
                imgSrc={`${getImageFilePath()}/${config[brand].imgSrc}`}
              />
              <span className="logo-seperator" />
            </>
          )}
          <BodyCopy component="div" className="my-rewards-img-wrapper">
            <Image
              className="tcp_carousel__play"
              src={getIconPath('my-place-rewards')}
              alt={getLabelValue(labels, 'lbl_createAccount_myPlaceRewards', 'registration')}
            />
          </BodyCopy>
        </BodyCopy>
      )}

      <BodyCopy component="div" className="bordered elem-pt-MED labels-wrapper">
        <BodyCopy
          fontFamily="secondary"
          fontSize="fs16"
          textAlign="center"
          fontWeight="black"
          className="elem-pt-MED"
        >
          {getLabelValue(labels, 'lbl_createAccount_top_section_title', 'registration')}
        </BodyCopy>
        {isCanada() && (
          <BodyCopy fontSize="fs14" fontWeight="black" textAlign="center" className="elem-pb-LRG">
            {getLabelValue(labels, 'lbl_createAccount_title', 'registration')}
          </BodyCopy>
        )}
        <BodyCopy
          className="elem-pb-XS"
          fontFamily="secondary"
          fontSize="fs14"
          textAlign="center"
          fontWeight="semibold"
        >
          <span>{getLabelValue(labels, 'lbl_createAccount_createA', 'registration')}</span>
          <span>{getLabelValue(labels, 'lbl_createAccount_myPlaceRewards', 'registration')}</span>
          <span>{getLabelValue(labels, 'lbl_createAccount_earnPoints', 'registration')}</span>
        </BodyCopy>
      </BodyCopy>

      {!isCanada() && (
        <>
          <BodyCopy
            fontFamily="secondary"
            fontSize="fs16"
            textAlign="center"
            className=""
            fontWeight="black"
          >
            {getLabelValue(labels, 'lbl_createAccount_spendPoint', 'registration')}
          </BodyCopy>
          <BodyCopy
            fontFamily="secondary"
            fontSize="fs16"
            textAlign="center"
            fontWeight="black"
            className="elem-pb-XS"
          >
            {getLabelValue(labels, 'lbl_createAccount_pointReward', 'registration')}
          </BodyCopy>
        </>
      )}

      <BodyCopy component="div" className="bordered elem-pb-MED signed-up-in-store">
        <BodyCopy fontFamily="secondary" fontSize="fs14" textAlign="center" fontWeight="semibold">
          <p>{getLabelValue(labels, 'lbl_createAccount_signedUp', 'registration')}</p>
          <p>{getLabelValue(labels, 'lbl_createAccount_onlineAccCreated', 'registration')}</p>
          <BodyCopy
            component="div"
            fontFamily="secondary"
            fontSize="fs14"
            textAlign="center"
            fontWeight="semibold"
          >
            <Anchor
              underline
              className="reset-password"
              onClick={(e) => {
                e.preventDefault();
                showForgotPassword(
                  showForgotPasswordForm,
                  isResetPassCodeValidationEnabled,
                  closeOverlay
                );
              }}
            >
              {getLabelValue(labels, 'lbl_createAccount_resetPassword', 'registration')}
            </Anchor>
          </BodyCopy>
        </BodyCopy>
      </BodyCopy>
    </BodyCopy>
  );
};

CreateAccountTopSectionFullPage.propTypes = {
  showForgotPasswordForm: PropTypes.func,
  closeOverlay: PropTypes.func,
  labels: PropTypes.shape({
    heading: PropTypes.string,
    subHeading: PropTypes.string,
    description: PropTypes.string,
  }),
  isResetPassCodeValidationEnabled: PropTypes.bool,
};

CreateAccountTopSectionFullPage.defaultProps = {
  showForgotPasswordForm: () => {},
  closeOverlay: () => {},
  labels: {
    heading: 'Welcome Back',
    subHeading: 'Log in to earn points for MY PLACE REWARDS ',
    description: `Signed up in store?\nAn online account has been created with your email! Click here to reset your password.`,
  },
  isResetPassCodeValidationEnabled: false,
};

export default CreateAccountTopSectionFullPage;
