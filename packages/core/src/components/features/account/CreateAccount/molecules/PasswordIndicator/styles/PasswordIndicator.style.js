// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const Styles = css`
  .tick-icon {
    height: 20px;
    width: 20px;
    padding-right: 8px;
  }

  .password-indicator {
    display: flex;
    padding-bottom: 5px;
  }

  .cue-text {
    display: flex;
    align-items: center;
  }
`;

export default Styles;
