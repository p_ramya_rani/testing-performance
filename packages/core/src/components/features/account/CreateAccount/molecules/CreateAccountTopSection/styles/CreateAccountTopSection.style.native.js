// 9fbef606107a605d69c0edbcd8029e5d
import styled, { css } from 'styled-components/native';

const getPageStyle = () => {
  return `
  margin: auto;
  justify-content: ${'center'};
  `;
};
const SectionStyle = css`
  ${getPageStyle}
`;

const CenterAlignWrapper = styled.View`
  display: flex;
  justify-content: ${'center'};
  align-items: center;
`;

const HeadingTextWrapper = styled.View`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  align-items: center;
  justify-content: ${'center'};
`;

const LabelsWrapper = styled.View`
  padding-top: ${(props) => props.theme.spacing.LAYOUT_SPACING.XXS};
  margin: 0 auto;
`;

const TextAlignCenter = styled.Text`
  text-align: center;
`;

const ViewAlignCenter = styled.View`
  text-align: center;
`;

const TopSectionWrapper = styled.View`
  padding-top: ${(props) => props.theme.spacing.LAYOUT_SPACING.SM};
`;

const PointsWrapper = styled.View`
  padding-top: 10px;
`;

const ResetWrapper = styled.View`
  padding-top: 10px;
  padding-bottom: 9px;
`;

const ResetPassword = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: ${'center'};
`;

export {
  SectionStyle,
  CenterAlignWrapper,
  LabelsWrapper,
  TextAlignCenter,
  TopSectionWrapper,
  PointsWrapper,
  ResetWrapper,
  ViewAlignCenter,
  ResetPassword,
  HeadingTextWrapper,
};
