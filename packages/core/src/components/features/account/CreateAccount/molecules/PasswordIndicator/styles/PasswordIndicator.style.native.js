// 9fbef606107a605d69c0edbcd8029e5d 
import styled, { css } from 'styled-components';

const Styles = css`
  display: flex;
`;

const PasswordIndicatorView = styled.View`
  padding-bottom: 5px;
  flex-direction: row;
`;

export { Styles, PasswordIndicatorView };
