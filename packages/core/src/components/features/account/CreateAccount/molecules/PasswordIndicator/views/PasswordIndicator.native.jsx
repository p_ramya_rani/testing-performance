// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import ImageComp from '../../../../../../common/atoms/Image';
import BodyCopy from '../../../../../../common/atoms/BodyCopy';

import withStyles from '../../../../../../common/hoc/withStyles';
import { Styles, PasswordIndicatorView } from '../styles/PasswordIndicator.style.native';

import CircleCheckGreen from '../../../../../../../../../mobileapp/src/assets/images/circle-check-green.png';
import CircleCheck from '../../../../../../../../../mobileapp/src/assets/images/circle-check.png';

const ELEMENTS_CONFIG = [
  {
    REGEX_EXP: /^(?=.*?[A-Z]).*/g,
    LABEL_KEY: 'lbl_resetPassword_requirementTips2',
  },
  {
    REGEX_EXP: /^(?=.*?[a-z]).*/g,
    LABEL_KEY: 'lbl_resetPassword_requirementTips5',
  },
  {
    REGEX_EXP: /^(.*\d.*)/g,
    LABEL_KEY: 'lbl_resetPassword_requirementTips3',
  },
  {
    REGEX_EXP: /^(.{8,})$/g,
    LABEL_KEY: 'lbl_resetPassword_requirementTips1',
  },
  {
    REGEX_EXP: /^(.*[!@#$%^_&*()<>?.-].*)/g,
    LABEL_KEY: 'lbl_resetPassword_requirementTips4',
  },
];

const bodycopyStyle = {
  marginTop: 2,
};

class PasswordIndicator extends React.Component {
  static propTypes = {
    labels: PropTypes.string.isRequired,
    passwordValue: PropTypes.string,
  };

  static defaultProps = {
    passwordValue: null,
  };

  getValidationIcon = (passwordValue, regexExp) => {
    if (passwordValue && passwordValue.match(regexExp)) {
      return CircleCheckGreen;
    }
    return CircleCheck;
  };

  generateElements = (elConfig) => {
    const { labels, passwordValue } = this.props;
    return (
      <PasswordIndicatorView>
        <ImageComp
          alt="validationIcon"
          source={this.getValidationIcon(passwordValue, elConfig.REGEX_EXP)}
          height={16}
          width={16}
          marginRight={5}
        />
        <BodyCopy
          component="span"
          fontSize="fs12"
          color="gray.900"
          fontFamily="secondary"
          style={bodycopyStyle}
          text={getLabelValue(labels, elConfig.LABEL_KEY, 'password')}
        />
      </PasswordIndicatorView>
    );
  };

  render() {
    return <View>{ELEMENTS_CONFIG.map((elConfig) => this.generateElements(elConfig))}</View>;
  }
}

export default withStyles(PasswordIndicator, Styles);
export { PasswordIndicator as PasswordIndicatorVanilla };
