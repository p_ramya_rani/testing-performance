// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { View } from 'react-native';
import { PropTypes } from 'prop-types';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import BodyCopy from '../../../../../../common/atoms/BodyCopy';
import withStyles from '../../../../../../common/hoc/withStyles.native';
import ImageComp from '../../../../../../common/atoms/Image';
import TheMarketPlaceLogo from '../../../../../../../../../mobileapp/src/assets/images/my-place-rewards-horz.png';
import {
  SectionStyle,
  CenterAlignWrapper,
  LabelsWrapper,
  TopSectionWrapper,
  ResetWrapper,
  HeadingTextWrapper,
} from '../styles/CreateAccountTopSection.style.native';
import Anchor from '../../../../../../common/atoms/Anchor';

/**
 * @param {string} props : props for CustomIcon
 * @return {JSX} IconClass : Return jsx icon component
 * @desc This method based on the props generate icon component.
 */

class CreateAccountTopSection extends React.PureComponent {
  showForgotPassword = () => {
    const { showForgotPasswordForm } = this.props;
    showForgotPasswordForm();
  };

  render() {
    const { labels } = this.props;
    return (
      <View {...this.props}>
        <TopSectionWrapper>
          <CenterAlignWrapper>
            <ImageComp source={TheMarketPlaceLogo} width={306} height={25} />
          </CenterAlignWrapper>
          <LabelsWrapper className="labels-wrapper">
            <HeadingTextWrapper>
              <BodyCopy
                fontSize="fs14"
                component="span"
                fontFamily="secondary"
                color="gray.900"
                fontWeight="semibold"
                textAlign="center"
                text={getLabelValue(labels, 'lbl_createAccount_createA', 'registration')}
              />
              <BodyCopy
                fontSize="fs14"
                component="span"
                textAlign="center"
                color="gray.900"
                fontWeight="semibold"
                fontFamily="secondary"
                text={getLabelValue(labels, 'lbl_createAccount_myPlaceRewards', 'registration')}
              />
              <BodyCopy
                fontSize="fs14"
                component="span"
                textAlign="center"
                color="gray.900"
                fontWeight="semibold"
                fontFamily="secondary"
                text={getLabelValue(labels, 'lbl_createAccount_earnPoints', 'registration')}
              />
            </HeadingTextWrapper>
            <ResetWrapper>
              <BodyCopy
                fontSize="fs14"
                textAlign="center"
                fontFamily="secondary"
                color="gray.900"
                fontWeight="semibold"
                text={getLabelValue(labels, 'lbl_createAccount_signedUp', 'registration')}
              />

              <BodyCopy
                fontSize="fs14"
                textAlign="center"
                fontFamily="secondary"
                color="gray.900"
                fontWeight="semibold"
                text={getLabelValue(labels, 'lbl_createAccount_onlineAccCreated', 'registration')}
              />
              <Anchor
                class="clickhere"
                fontSizeVariation="large"
                anchorVariation="primary"
                fontWeightVariation="semibold"
                text={getLabelValue(labels, 'lbl_createAccount_resetPassword', 'registration')}
                underline
                onPress={this.showForgotPassword}
              />
            </ResetWrapper>
          </LabelsWrapper>
        </TopSectionWrapper>
      </View>
    );
  }
}

CreateAccountTopSection.propTypes = {
  labels: {
    registration: {
      lbl_createAccount_createA: PropTypes.string,
      lbl_createAccount_myPlaceRewards: PropTypes.string,
      lbl_createAccount_earnPoints: PropTypes.string,
      lbl_createAccount_spendPoint: PropTypes.string,
      lbl_createAccount_pointReward: PropTypes.string,
      lbl_createAccount_signedUp: PropTypes.string,
      lbl_createAccount_onlineAccCreated: PropTypes.string,
    },
  },
  showForgotPasswordForm: PropTypes.bool.isRequired,
};

CreateAccountTopSection.defaultProps = {
  labels: PropTypes.shape({
    registration: {
      lbl_createAccount_createA: '',
      lbl_createAccount_myPlaceRewards: '',
      lbl_createAccount_earnPoints: '',
      lbl_createAccount_spendPoint: '',
      lbl_createAccount_pointReward: '',
      lbl_createAccount_signedUp: '',
      lbl_createAccount_onlineAccCreated: '',
    },
  }),
};

export default withStyles(CreateAccountTopSection, SectionStyle);
export { CreateAccountTopSection as CreateAccountTopSectionVanilla };
