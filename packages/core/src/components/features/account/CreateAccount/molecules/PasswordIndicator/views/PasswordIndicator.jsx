// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { getLabelValue, getIconPath } from '@tcp/core/src/utils/utils';
import { Image, BodyCopy } from '@tcp/core/src/components/common/atoms';

import withStyles from '../../../../../../common/hoc/withStyles';
import styles from '../styles/PasswordIndicator.style';

const ELEMENTS_CONFIG = [
  {
    REGEX_EXP: /^(?=.*?[A-Z]).*/g,
    LABEL_KEY: 'lbl_resetPassword_requirementTips2',
  },
  {
    REGEX_EXP: /^(?=.*?[a-z]).*/g,
    LABEL_KEY: 'lbl_resetPassword_requirementTips5',
  },
  {
    REGEX_EXP: /^(.*\d.*)/g,
    LABEL_KEY: 'lbl_resetPassword_requirementTips3',
  },
  {
    REGEX_EXP: /^(.{8,})$/g,
    LABEL_KEY: 'lbl_resetPassword_requirementTips1',
  },
  {
    REGEX_EXP: /^(.*[!@#$%^_&*()<>?.-].*)/g,
    LABEL_KEY: 'lbl_resetPassword_requirementTips4',
  },
];

class PasswordIndicator extends React.Component {
  static propTypes = {
    className: PropTypes.string.isRequired,
    labels: PropTypes.string.isRequired,
    passwordValue: PropTypes.string,
  };

  static defaultProps = {
    passwordValue: null,
  };

  getValidationIcon = (passwordValue, regexExp) => {
    if (passwordValue && passwordValue.match(regexExp)) {
      return getIconPath('circle-check-green');
    }
    return getIconPath('circle-check');
  };

  generateElements = (elConfig) => {
    const { labels, passwordValue } = this.props;
    return (
      <div className="password-indicator">
        <Image
          alt="validationIcon"
          className="tick-icon"
          src={this.getValidationIcon(passwordValue, elConfig.REGEX_EXP)}
        />

        <BodyCopy component="span" fontSize="fs12" fontFamily="secondary" className="cue-text">
          {getLabelValue(labels, elConfig.LABEL_KEY, 'password')}
        </BodyCopy>
      </div>
    );
  };

  render() {
    const { className } = this.props;
    return (
      <div className={className}>
        {ELEMENTS_CONFIG.map((elConfig) => this.generateElements(elConfig))}
      </div>
    );
  }
}

export default withStyles(PasswordIndicator, styles);
export { PasswordIndicator as PasswordIndicatorVanilla };
