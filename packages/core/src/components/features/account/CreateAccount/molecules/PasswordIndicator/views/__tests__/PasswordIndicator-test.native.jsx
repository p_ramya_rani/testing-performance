// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
// eslint-disable-next-line import/no-named-as-default
import { PasswordIndicatorVanilla } from '../PasswordIndicator.native';

describe('Password Indicator component', () => {
  it('should renders correctly', () => {
    const props = {
      labels: {
        password: {
          lbl_passwordCue_upperCase: 'Must have at least 1 uppercase letter',
          lbl_passwordCue_digit: 'Must contain 1 number',
          lbl_passwordCue_length: 'Must be at least 8 characters long',
          lbl_passwordCue_specialChar: 'Must contain 1 special character: !@#$%^&*()<>?',
        },
      },
      passwordValue: 'Tes1212?',
    };
    const component = shallow(<PasswordIndicatorVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
