// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import { View } from 'react-native';
import { reduxForm, Field, SubmissionError, change } from 'redux-form';
import PropTypes, { shape } from 'prop-types';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import briteVerifyStatusExtraction from '@tcp/core/src/services/abstractors/common/briteVerifyStatusExtraction';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy/views/BodyCopy.native';
import CustomSwitch from '@tcp/core/src/components/common/atoms/Switch/Switch.native';
import TextBox from '../../../../../../common/atoms/TextBox';
import withStyles from '../../../../../../common/hoc/withStyles.native';

import {
  Styles,
  ParentView,
  ButtonWrapper,
  AlreadyAccountWrapper,
  PasswordWrapper,
  ConfirmHideShowField,
  PasswordIndicatorWrapper,
  BodyTextWrapper,
  SwitchStyle,
  CustomInputCheckBoxWrapper,
} from '../styles/CreateAccountForm.style.native';
import InputCheckbox from '../../../../../../common/atoms/InputCheckbox';
import CustomButton from '../../../../../../common/atoms/ButtonRedesign';
import Anchor from '../../../../../../common/atoms/Anchor';
import createValidateMethod from '../../../../../../../utils/formValidation/createValidateMethod';
import getStandardConfig from '../../../../../../../utils/formValidation/validatorStandardConfig';
import {
  formatPhoneNumber,
  removeSpaces,
} from '../../../../../../../utils/formValidation/phoneNumber';
import PasswordIndicator from '../../PasswordIndicator/index';
import ClickTracker from '../../../../../../../../../mobileapp/src/components/common/atoms/ClickTracker';
import names from '../../../../../../../constants/eventsName.constants';

const {
  touchStatusWrapperStyle,
  switchInActiveTextStyle,
  switchActiveTextStyle,
  switchInnerCircleStyle,
  switchOuterCircleStyle,
} = SwitchStyle;

class CreateAccountForm extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      passwordType: 'password',
      confirmPasswordType: 'password',
      passwordFieldFocus: false,
      passwordValue: null,
      invalidEmail: '',
      briteVerifyStatus: '',
      switchValue: false,
    };
  }

  showLoginSection = () => {
    const { showLogin } = this.props;
    showLogin();
  };

  getSwitchValue = (value) => {
    this.setState({ switchValue: value });
    const { dispatch } = this.props;
    if (dispatch) {
      dispatch(change('myCreateAccountForm', 'toggleValue', value));
    }
  };

  getBiometricStatus = (status, labels) => {
    if (status) {
      return getLabelValue(labels, 'lbl_createAccount_useTouchId', 'registration');
    }
    return getLabelValue(labels, 'lbl_createAccount_useFaceId', 'registration');
  };

  changeType = (e, type) => {
    e.preventDefault();
    const { passwordType, confirmPasswordType } = this.state;
    if (type === 'password') {
      this.setState({
        passwordType: passwordType === 'password' ? 'text' : 'password',
      });
    }
    if (type === 'confirmPassword') {
      this.setState({
        confirmPasswordType: confirmPasswordType === 'password' ? 'text' : 'password',
      });
    }
  };

  shouldCallSubmit = (prev, next) => prev && next && prev === next;

  submitForm = (payload) => {
    const { handleSubmitForm, formErrorMessage } = this.props;
    const { emailAddress } = payload;
    const { invalidEmail, briteVerifyStatus } = this.state;
    const isSubmit = this.shouldCallSubmit(invalidEmail, emailAddress);
    if (isSubmit) {
      return handleSubmitForm({ ...payload, emailValidationStatus: briteVerifyStatus });
    }
    return briteVerifyStatusExtraction(emailAddress)
      .then((status) => {
        if (!isSubmit && status && status.indexOf('invalid') !== -1) {
          this.setState({ invalidEmail: emailAddress, briteVerifyStatus: status });
          return Promise.reject();
        }
        handleSubmitForm({ ...payload, emailValidationStatus: status });
        return Promise.resolve();
      })
      .catch(() => {
        const error = { emailAddress: formErrorMessage.lbl_err_email_req };
        throw new SubmissionError({ ...error, _error: error });
      });
  };

  render() {
    const {
      labels,
      handleSubmit,
      getTouchStatus,
      userplccCardNumber,
      userplccCardId,
      onRequestClose,
      initialValues,
      loyaltyPageName,
      pristine,
      submitting,
      invalid,
      isLoading,
    } = this.props;
    const { passwordType, passwordFieldFocus, passwordValue } = this.state;

    const { switchValue } = this.state;
    const getPlccLbl = getLabelValue(
      labels,
      'lbl_createAccount_plcc_checkbox_Text',
      'registration'
    ).replace('#number', `${userplccCardNumber}`);

    const shouldSignUpButtonDisabled = invalid || submitting || pristine;

    const touchStatus = getTouchStatus === 'TouchID' || getTouchStatus === true;

    return (
      <View {...this.props}>
        <ParentView>
          <Field
            label={getLabelValue(labels, 'lbl_createAccount_firstName', 'registration')}
            name="firstName"
            id="firstName"
            type="text"
            showAnimatedBorder
            component={TextBox}
            dataLocator="firstName"
          />
          <Field
            label={getLabelValue(labels, 'lbl_createAccount_lastName', 'registration')}
            name="lastName"
            id="lastName"
            type="text"
            showAnimatedBorder
            component={TextBox}
            dataLocator="lastName"
          />
          <Field
            label={getLabelValue(labels, 'lbl_createAccount_phoneNumber', 'registration')}
            name="phoneNumber"
            id="phoneNumber"
            type="text"
            component={TextBox}
            showAnimatedBorder
            dataLocator="phoneNumber"
            normalize={formatPhoneNumber}
            keyboardType="phone-pad"
          />
          <Field
            label={getLabelValue(labels, 'lbl_createAccount_zipCode', 'registration')}
            name="noCountryZip"
            id="ZipCode"
            type="text"
            component={TextBox}
            showAnimatedBorder
            dataLocator="Zip-Code"
            keyboardType="number-pad"
            normalize={removeSpaces}
          />
          <Field
            label={getLabelValue(labels, 'lbl_createAccount_emailAddress', 'registration')}
            name="emailAddress"
            id="emailAddress"
            autoCapitalize="none"
            type="text"
            showAnimatedBorder
            keyboardType="email-address"
            component={TextBox}
            dataLocator="emailAddress"
            normalize={removeSpaces}
          />
          <PasswordWrapper>
            <Field
              label={getLabelValue(labels, 'lbl_createAccount_password', 'registration')}
              name="password"
              id="password"
              type={passwordType}
              component={TextBox}
              showAnimatedBorder
              dataLocator="password"
              valid
              shouldSignUpButtonDisabled
              secureTextEntry={passwordType === 'password'}
              onChange={(e, newValue) => {
                this.setState({ passwordValue: newValue });
              }}
              onPasswordFocus={() => {
                if (!passwordFieldFocus) {
                  this.setState({ passwordFieldFocus: true });
                }
              }}
            />
            {passwordFieldFocus ? (
              <ConfirmHideShowField>
                <Anchor
                  fontSizeVariation="medium"
                  fontFamily="secondary"
                  anchorVariation="lightGray"
                  fontWeightVariation="semibold"
                  onPress={(e) => this.changeType(e, 'password')}
                  noLink
                  to="/#"
                  dataLocator="hide-show-pwd"
                  underline
                  text={
                    passwordType === 'password'
                      ? getLabelValue(labels, 'lbl_createAccount_show', 'registration')
                      : getLabelValue(labels, 'lbl_createAccount_hide', 'registration')
                  }
                  hitSlop={{ top: 3, left: 10, bottom: 10, right: 10 }}
                />
              </ConfirmHideShowField>
            ) : null}
            {passwordFieldFocus ? (
              <PasswordIndicatorWrapper>
                <PasswordIndicator passwordValue={passwordValue} labels={labels} />
              </PasswordIndicatorWrapper>
            ) : null}
          </PasswordWrapper>

          {/* CHECKBOXES */}

          {!!(userplccCardNumber && userplccCardId) && (
            <Field
              inputVariation="inputVariation-1"
              name="plcc_checkbox"
              component={InputCheckbox}
              dataLocator="plcc_checkbox"
              disabled={false}
              rightText={getPlccLbl}
              isChecked={initialValues.plcc_checkbox}
              marginTop={13}
              accessibilityText={`${getLabelValue(
                labels,
                'lbl_plccCheckbox_app_accessibilityText',
                'registration'
              )}`}
            />
          )}

          <CustomInputCheckBoxWrapper>
            <Field
              inputVariation="inputVariation-1"
              name="iAgree"
              component={InputCheckbox}
              dataLocator="iAgree"
              disabled={false}
              checkboxAlignTop
              fontSize="fs10"
              color="gray.900"
              textMargin="8px"
              rightText={`${getLabelValue(
                labels,
                'lbl_createAccount_termsConditions_app',
                'registration'
              )} ${getLabelValue(
                labels,
                'lbl_createAccount_termsConditions_1_app',
                'registration'
              )}`}
              marginTop={13}
              onRequestClose={onRequestClose}
              useEspot
              accessibilityText={`${getLabelValue(
                labels,
                'lbl_createAccount_termsConditions_app_accessibilityText',
                'registration'
              )}`}
            />
          </CustomInputCheckBoxWrapper>

          <View style={touchStatusWrapperStyle}>
            <CustomSwitch
              value={switchValue}
              onValueChange={(val) => this.getSwitchValue(val)}
              disabled={false}
              activeText="YES"
              inActiveText="NO"
              circleSize={25}
              barHeight={34}
              circleBorderWidth={0}
              backgroundActive="#254f6e"
              backgroundInactive="#d8d8d8"
              circleActiveColor="#ffffff"
              circleInActiveColor="#ffffff"
              inactiveTextStyle={switchInActiveTextStyle}
              activeTextStyle={switchActiveTextStyle}
              changeValueImmediately // if rendering inside circle, change state immediately or wait for animation to complete
              innerCircleStyle={switchInnerCircleStyle} // style for inner animated circle for what you (may) be rendering inside the circle
              outerCircleStyle={switchOuterCircleStyle} // style for outer animated circle
              renderActiveText
              renderInActiveText
              switchLeftPx={2} // denominator for logic when sliding to TRUE position. Higher number = more space from RIGHT of the circle to END of the slider
              switchRightPx={2} // denominator for logic when sliding to FALSE position. Higher number = more space from LEFT of the circle to BEGINNING of the slider
              switchWidthMultiplier={3.1} // multiplied by the `circleSize` prop to calculate total width of the Switch
              switchBorderRadius={17} // Sets the border Radius of the switch slider. If unset, it remains the circleSize.
            />
            <BodyTextWrapper>
              {touchStatus ? (
                <BodyCopy
                  fontSize="fs14"
                  fontWeight="regular"
                  fontFamily="secondary"
                  color="gray.900"
                  marginLeft="12px"
                  width="343"
                  alignSelf="stretch"
                  text={getLabelValue(labels, 'lbl_createAccount_useTouchId', 'registration')}
                />
              ) : (
                <View />
              )}
              {getTouchStatus === 'FaceID' && (
                <BodyCopy
                  fontSize="fs14"
                  fontWeight="regular"
                  fontFamily="secondary"
                  color="gray.900"
                  marginLeft="12px"
                  width="343"
                  alignSelf="stretch"
                  text={getLabelValue(labels, 'lbl_createAccount_useFaceId', 'registration')}
                />
              )}
            </BodyTextWrapper>
          </View>

          <ButtonWrapper>
            <CustomButton
              text={getLabelValue(labels, 'lbl_createAccount_createAccount', 'registration')}
              borderRadius="16px"
              onPress={handleSubmit(this.submitForm)}
              fill="BLUE"
              paddings="7px"
              fontWeight="bold"
              width="100%"
              disableButton={shouldSignUpButtonDisabled}
              fontSize="fs16"
              showLoadingSpinner={isLoading}
            />
          </ButtonWrapper>
          <AlreadyAccountWrapper>
            <ClickTracker
              as={Anchor}
              clickData={{
                customEvents: ['event126'],
                loyaltyLocation: loyaltyPageName,
              }}
              name={names.screenNames.loyalty_login_click}
              fontSizeVariation="large"
              anchorVariation="primary"
              fontWeightVariation="semibold"
              text={getLabelValue(labels, 'lbl_createAccount_alreadyAccount', 'registration')}
              onPress={this.showLoginSection}
              underline
            />
          </AlreadyAccountWrapper>
        </ParentView>
      </View>
    );
  }
}

const validateMethod = createValidateMethod(
  getStandardConfig([
    'firstName',
    'lastName',
    'phoneNumber',
    'noCountryZip',
    'emailAddress',
    'password',
    'confirmPassword',
  ])
);

CreateAccountForm.propTypes = {
  labels: PropTypes.shape({
    email: PropTypes.string,
    password: PropTypes.string,
    rememberMe: PropTypes.string,
    saveMyRewards: PropTypes.string,
    login: PropTypes.string,
    createAccount: PropTypes.string,
  }),
  handleSubmit: PropTypes.func,
  handleSubmitForm: PropTypes.func,
  onPwdHideShowClick: PropTypes.func,
  hideShowPwd: PropTypes.bool,
  onConfirmPwdHideShowClick: PropTypes.func,
  onRequestClose: PropTypes.func,
  confirmHideShowPwd: PropTypes.bool,
  userplccCardNumber: PropTypes.string,
  formErrorMessage: shape({}).isRequired,
  userplccCardId: PropTypes.string,
  showLogin: PropTypes.bool.isRequired,
  getTouchStatus: PropTypes.func.isRequired,
  initialValues: PropTypes.shape({}),
  loyaltyPageName: PropTypes.string,
  formValues: PropTypes.shape.isRequired,
  pristine: PropTypes.bool,
  submitting: PropTypes.bool,
  invalid: PropTypes.bool,
  valid: PropTypes.bool,
  isLoading: PropTypes.bool.isRequired,
};

CreateAccountForm.defaultProps = {
  labels: {
    lbl_createAccount_firstName: 'First Name',
    lbl_createAccount_lastName: 'Last Name',
    lbl_createAccount_phoneNumber: 'Phone Number',
    lbl_createAccount_zipCode: 'Zip Code',
    lbl_createAccount_emailAddress: 'Email Address',
    lbl_createAccount_password: 'Password',
    lbl_createAccount_confirmPassword: 'Confirm Password',
    lbl_createAccount_useTouchId: 'Use Touch ID',
    lbl_createAccount_useFaceId: 'Use Face ID',
  },
  handleSubmit: () => {},
  handleSubmitForm: () => {},
  onPwdHideShowClick: () => {},
  hideShowPwd: false,
  onConfirmPwdHideShowClick: () => {},
  onRequestClose: () => {},
  confirmHideShowPwd: false,
  userplccCardId: '',
  userplccCardNumber: '',
  initialValues: {},
  loyaltyPageName: 'account',
  pristine: false,
  submitting: false,
  invalid: false,
  valid: false,
};

export default reduxForm({
  form: 'myCreateAccountForm',
  ...validateMethod,
  enableReinitialize: true,
})(withStyles(CreateAccountForm, Styles));
export { CreateAccountForm as CreateAccountFormVanilla };
