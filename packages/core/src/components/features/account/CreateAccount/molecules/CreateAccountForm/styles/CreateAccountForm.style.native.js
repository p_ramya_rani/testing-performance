// 9fbef606107a605d69c0edbcd8029e5d
import styled, { css } from 'styled-components';

const Styles = css`
  padding: 2px;
`;

const ParentView = styled.View`
  margin: 16px;
`;

const ButtonWrapper = styled.View`
  padding-top: 32px;
`;

const AlreadyAccountWrapper = styled.View`
  padding-top: 23px;
`;

const PasswordWrapper = styled.View`
  position: relative;
`;

const PasswordIndicatorWrapper = styled.View`
  position: relative;
`;

const HideShowField = styled.View`
  position: absolute;
  right: 0;
  top: 16px;
  border-bottom-width: 1px;
  border-bottom-color: black;
`;

const ConfirmPasswordWrapper = styled.View`
  position: relative;
`;

const HideShowFieldStyle = (props) =>
  `
  background: ${props.theme.colorPalette.white};
  height:20px; /* 18px not available in spacing variable*/
  position: absolute;
  right: 17;
  top: ${props.theme.spacing.ELEM_SPACING.LRG_2};
  `;

const ConfirmHideShowField = styled.View`
  ${HideShowFieldStyle}
`;

const IconContainer = styled.View`
  position: absolute;
  right: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  width: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
`;

const BodyTextWrapper = styled.View`
  right: -12px;
  top: 10px;
`;

const CustomInputCheckBoxWrapper = styled.View`
  padding: 0px ${(props) => props.theme.spacing.ELEM_SPACING.SM}
    ${(props) => props.theme.spacing.ELEM_SPACING.XS};
`;

const SwitchStyle = {
  touchStatusWrapperStyle: {
    flexDirection: 'row',
    flex: 1,
  },

  switchInActiveTextStyle: {
    fontSize: 14,
    fontFamily: 'Nunito-Bold',
    color: '#505050',
    paddingLeft: 1,
  },

  switchActiveTextStyle: {
    paddingRight: 2,
  },

  switchInnerCircleStyle: {
    marginLeft: 5,
    marginRight: 2,
  },

  switchOuterCircleStyle: {
    width: 73,
  },
};

export {
  Styles,
  ParentView,
  ButtonWrapper,
  AlreadyAccountWrapper,
  PasswordWrapper,
  HideShowField,
  ConfirmPasswordWrapper,
  ConfirmHideShowField,
  IconContainer,
  PasswordIndicatorWrapper,
  BodyTextWrapper,
  SwitchStyle,
  CustomInputCheckBoxWrapper,
};
