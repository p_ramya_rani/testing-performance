import React from 'react';
import { shallow } from 'enzyme';
import { routerPush, isCanada } from '@tcp/core/src/utils';
import CreateAccountTopSectionFullPage from '../CreateAccountTopSectionFullPage';

jest.mock('@tcp/core/src/utils', () => ({
  isMobileApp: jest.fn(),
  routerPush: jest.fn(),
  getAPIConfig: jest.fn(),
  isClient: jest.fn(),
  getIconPath: jest.fn(),
  getStaticFilePath: jest.fn(),
  getBrand: () => 'tcp',
  isCanada: jest.fn(),
  getImageFilePath: jest.fn(),
}));

describe('Create Account component', () => {
  const props = {
    showForgotPasswordForm: jest.fn(),
    closeOverlay: jest.fn(),
    labels: {
      heading: 'Welcome Back',
      subHeading: 'Log in to earn points for MY PLACE REWARDS ',
      description: `Signed up in store?\nAn online account has been created with your email! Click here to reset your password.`,
    },
    isResetPassCodeValidationEnabled: false,
  };
  const mockedRouterPush = jest.fn();

  it('should renders correctly', () => {
    const component = shallow(<CreateAccountTopSectionFullPage {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should call routerpush with change-password on reset', () => {
    routerPush.mockImplementation(mockedRouterPush);
    const showForgotPassword = jest.fn();
    const component = shallow(<CreateAccountTopSectionFullPage {...props} />);
    component.setProps({ isResetPassCodeValidationEnabled: true });
    component.find("[className='reset-password']").simulate('click', { preventDefault: jest.fn() });
    showForgotPassword.mockImplementation();
    expect(mockedRouterPush).toBeCalledWith(
      `/change-password?page=reset`,
      `/change-password?page=reset`
    );
  });

  it('should call showForgotPasswordForm if isResetPassCodeValidationEnabled is false', () => {
    const component = shallow(<CreateAccountTopSectionFullPage {...props} />);
    component.find("[className='reset-password']").simulate('click', { preventDefault: jest.fn() });
    expect(props.showForgotPasswordForm).toBeCalled();
  });

  it('should render component when isCanada is true', () => {
    isCanada.mockImplementation(() => true);
    const component = shallow(<CreateAccountTopSectionFullPage {...props} />);
    expect(component).toMatchSnapshot();
  });
});
