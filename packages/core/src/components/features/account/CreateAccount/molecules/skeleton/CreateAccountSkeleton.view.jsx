// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import LoaderSkelton from '@tcp/core/src/components/common/molecules/LoaderSkelton';
import { BodyCopy } from '../../../../../common/atoms';

const CreateAccountSkeleton = () => {
  return (
    <BodyCopy component="div" className="elem-pr-XS elem-pl-XS elem-pt-XXXL">
      <BodyCopy component="div" className="img-parent align-center">
        <LoaderSkelton width="172px" height="70px" />
        <span className="logo-seperator" />
        <BodyCopy component="div" className="my-rewards-img-wrapper">
          <LoaderSkelton width="172px" height="70px" />
        </BodyCopy>
      </BodyCopy>

      <BodyCopy component="div" className="bordered elem-pt-MED labels-wrapper">
        <BodyCopy className="elem-pb-XS" textAlign="center">
          <span>
            <LoaderSkelton />
          </span>
          <span>
            <LoaderSkelton />
          </span>
          <span>
            <LoaderSkelton />
          </span>
        </BodyCopy>
      </BodyCopy>

      <BodyCopy textAlign="center">
        <LoaderSkelton />
      </BodyCopy>
      <BodyCopy textAlign="center" className="elem-pb-XS">
        <LoaderSkelton />
      </BodyCopy>

      <BodyCopy component="div" className="elem-pb-MED">
        <BodyCopy textAlign="center">
          <p>
            <LoaderSkelton />
          </p>
          <p>
            <LoaderSkelton />
          </p>
        </BodyCopy>
      </BodyCopy>
    </BodyCopy>
  );
};

export default CreateAccountSkeleton;
export { CreateAccountSkeleton as CreateAccountSkeletonVanilla };
