// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import { getLoginPayload, setLoyaltyLocation } from '@tcp/core/src/constants/analytics';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import ClickTracker from '@tcp/web/src/components/common/atoms/ClickTracker';
import TextBox from '@tcp/core/src/components/common/atoms/TextBox';
import Row from '@tcp/core/src/components/common/atoms/Row';
import Col from '@tcp/core/src/components/common/atoms/Col';
import Button from '@tcp/core/src/components/common/atoms/Button';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import InputCheckbox from '@tcp/core/src/components/common/atoms/InputCheckbox';
import Anchor from '@tcp/core/src/components/common/atoms/Anchor';
import Espot from '@tcp/core/src/components/common/molecules/Espot';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import { getViewportInfo, isClient } from '@tcp/core/src/utils';
import { scrollToFirstError } from '@tcp/core/src/components/features/CnC/Checkout/util/utility';
import briteVerifyStatusExtraction from '@tcp/core/src/services/abstractors/common/briteVerifyStatusExtraction';
import Styles from '../styles/CreateAccountForm.style';
import createValidateMethod from '../../../../../../../utils/formValidation/createValidateMethod';
import getStandardConfig from '../../../../../../../utils/formValidation/validatorStandardConfig';
import { isCanada } from '../../../../../../../utils';
import {
  formatPhoneNumber,
  removeSpaces,
} from '../../../../../../../utils/formValidation/phoneNumber';
import constants from '../../../CreateAccount.constants';
import PasswordIndicator from '../../PasswordIndicator/index';

const UseFocus = () => {
  const htmlElRef = useRef(null);
  const setFocus = () => {
    // eslint-disable-next-line no-unused-expressions
    htmlElRef.current && htmlElRef.current.focus();
  };

  return [htmlElRef, setFocus];
};

const shouldCallSubmit = (prev, next) => prev && next && prev === next;

export const focusFirstFieldForDesktop = (setFirstNameFocus, isUserLoggedIn) => {
  const { isMobile = false } = getViewportInfo() || {};
  if (isClient() && !isUserLoggedIn && !isMobile) {
    setFirstNameFocus();
  }
};

const handleMouseDown = (e) => {
  const passwordField = document.querySelector('#password');
  if (document.activeElement === passwordField && e?.target?.type === 'checkbox') {
    e.target.click();
  }
};

// eslint-disable-next-line import/no-mutable-exports
let CreateAccountForm = ({
  isMakeDefaultDisabled,
  labels,
  hideShowPwd,
  handleSubmit,
  onAlreadyHaveAnAccountClick,
  className,
  userplccCardNumber,
  userplccCardId,
  onSubmit,
  formErrorMessage,
  isUserLoggedIn,
}) => {
  const [passwordFieldFocus, setPasswordFieldFocus] = useState(false);
  const [passwordValue, setPasswordValue] = useState(null);
  const [invalidEmail, setInvalidEmail] = useState(null);
  const [briteVerifyStatus, setBriteVerifyStatus] = useState(null);
  const [inputRef, setInputFocus] = UseFocus();
  const [firstNameRef, setFirstNameFocus] = UseFocus();
  const getPlccLbl = getLabelValue(
    labels,
    'lbl_createAccount_plcc_checkbox_Text',
    'registration'
  ).replace('#number', `${userplccCardNumber}`);

  useEffect(() => {
    document.removeEventListener('mousedown', handleMouseDown);
    document.addEventListener('mousedown', handleMouseDown);
    return () => {
      document.removeEventListener('mousedown', handleMouseDown);
    };
  }, []);

  const submitForm = (payload) => {
    const { emailAddress } = payload;
    if (shouldCallSubmit(invalidEmail, emailAddress)) {
      return onSubmit({ ...payload, emailValidationStatus: briteVerifyStatus });
    }
    return briteVerifyStatusExtraction(emailAddress)
      .then((status) => {
        if (!invalidEmail && status && status.indexOf('invalid') !== -1) {
          setInvalidEmail(emailAddress);
          setBriteVerifyStatus(status);
          return Promise.reject();
        }
        onSubmit({ ...payload, emailValidationStatus: status });
        return Promise.resolve();
      })
      .catch(() => {
        const error = { emailAddress: formErrorMessage.lbl_err_email_req };
        throw new SubmissionError({ ...error, _error: error });
      });
  };

  useEffect(() => {
    focusFirstFieldForDesktop(setFirstNameFocus, isUserLoggedIn);
  }, []);

  return (
    <div className={`${className} elem-pt-MED`}>
      <form onSubmit={handleSubmit(submitForm)}>
        <Row fullBleed className="row-form-wrapper">
          <Col ignoreGutter={{ small: true, medium: true, large: true }} colSize={{ small: 6 }}>
            <Field
              placeholder={getLabelValue(labels, 'lbl_createAccount_firstName', 'registration')}
              name="firstName"
              id="firstName"
              component={TextBox}
              dataLocator="first-name-field"
              enableSuccessCheck={false}
              inputRef={firstNameRef}
            />
          </Col>
          <Col ignoreGutter={{ small: true, medium: true, large: true }} colSize={{ small: 6 }}>
            <Field
              placeholder={getLabelValue(labels, 'lbl_createAccount_lastName', 'registration')}
              name="lastName"
              id="lastName"
              component={TextBox}
              dataLocator="last name-field"
              enableSuccessCheck={false}
            />
          </Col>
          <Col ignoreGutter={{ small: true, medium: true, large: true }} colSize={{ small: 6 }}>
            <Field
              placeholder={getLabelValue(labels, 'lbl_createAccount_phoneNumber', 'registration')}
              name="phoneNumber"
              id="phoneNumber"
              type="tel"
              component={TextBox}
              maxLength={50}
              dataLocator="phone-number-field"
              enableSuccessCheck={false}
              normalize={formatPhoneNumber}
            />
          </Col>
          <Col ignoreGutter={{ small: true, medium: true, large: true }} colSize={{ small: 6 }}>
            <Field
              placeholder={
                isCanada()
                  ? getLabelValue(labels, 'lbl_addEditAddress_postalCode', 'addEditAddress')
                  : getLabelValue(labels, 'lbl_createAccount_zipCode', 'registration')
              }
              name="noCountryZip"
              id="noCountryZip"
              component={TextBox}
              dataLocator="zip-code-field"
              enableSuccessCheck={false}
              normalize={removeSpaces}
            />
          </Col>
          <Col ignoreGutter={{ small: true, medium: true, large: true }} colSize={{ small: 6 }}>
            <Field
              placeholder={getLabelValue(labels, 'lbl_createAccount_emailAddress', 'registration')}
              name="emailAddress"
              id="emailAddress"
              component={TextBox}
              dataLocator="email-address-field"
              enableSuccessCheck={false}
              normalize={removeSpaces}
            />
          </Col>
          <Col
            className="position-relative"
            ignoreGutter={{ small: true, medium: true, large: true }}
            colSize={{ small: 6 }}
          >
            <Field
              placeholder={getLabelValue(labels, 'lbl_createAccount_password', 'registration')}
              name="password"
              id="password"
              type={hideShowPwd ? 'text' : 'password'}
              component={TextBox}
              dataLocator="password-field"
              checkMarkRightMargin="40px"
              enableSuccessCheck
              inputRef={inputRef}
              onBlur={() => {
                setPasswordFieldFocus(false);
              }}
              onChange={(e, newValue) => {
                setPasswordValue(newValue);
              }}
              onFocus={() => {
                if (!passwordFieldFocus) {
                  setPasswordFieldFocus(true);
                }
              }}
            />
            <span className="hide-show show-hide-icons">
              <Col
                className="checkbox-hide-show"
                ignoreGutter={{ small: true, medium: true, large: true }}
                colSize={{ small: 6 }}
              >
                <Field
                  name="hideShowPwd"
                  component={InputCheckbox}
                  dataLocator="hide-show-checkbox"
                  enableSuccessCheck={false}
                  onChange={() => {
                    setInputFocus();
                  }}
                >
                  {hideShowPwd
                    ? getLabelValue(labels, 'lbl_createAccount_hide', 'registration')
                    : getLabelValue(labels, 'lbl_createAccount_show', 'registration')}
                </Field>
              </Col>
            </span>
          </Col>
          {passwordFieldFocus ? (
            <Col
              className="position-relative"
              ignoreGutter={{ small: true, medium: true, large: true }}
              colSize={{ small: 6 }}
            >
              <PasswordIndicator passwordValue={passwordValue} labels={labels} />
            </Col>
          ) : null}

          {userplccCardNumber && userplccCardId && (
            <Col
              className="plcc_checkbox elem-pb-MED"
              ignoreGutter={{ small: true, medium: true, large: true }}
              colSize={{ small: 6 }}
            >
              <Field
                name="plcc_checkbox"
                component={InputCheckbox}
                dataLocator="plcc_checkbox"
                alignCheckbox="top"
              >
                <BodyCopy fontFamily="secondary" fontSize="fs10">
                  <Espot richTextHtml={getPlccLbl} isNativeView={false} />
                </BodyCopy>
              </Field>
            </Col>
          )}
          <Col
            className="i-agree-checkbox elem-pb-MED"
            ignoreGutter={{ small: true, medium: true, large: true }}
            colSize={{ small: 6 }}
          >
            <Field
              name="iAgree"
              component={InputCheckbox}
              dataLocator="i-agree-checkbox"
              disabled={isMakeDefaultDisabled}
              alignCheckbox="top"
              genericCheckboxStyle
            >
              <BodyCopy fontFamily="secondary" fontSize="fs10">
                <Espot
                  richTextHtml={`${getLabelValue(
                    labels,
                    'lbl_createAccount_termsConditions',
                    'registration'
                  )} ${getLabelValue(
                    labels,
                    'lbl_createAccount_termsConditions_1',
                    'registration'
                  )}`}
                  isNativeView={false}
                />
              </BodyCopy>
            </Field>
          </Col>
          <Col
            className="elem-pb-MED"
            ignoreGutter={{ small: true, medium: true, large: true }}
            colSize={{ small: 6 }}
          >
            <Field
              name="rememberMe"
              component={InputCheckbox}
              dataLocator="remember-me-checkbox"
              disabled={isMakeDefaultDisabled}
            >
              <BodyCopy fontFamily="secondary" className="remember-me-text" fontSize="fs10">
                {getLabelValue(labels, 'lbl_createAccount_rememberMe', 'registration')}
              </BodyCopy>

              <BodyCopy fontFamily="secondary" fontSize="fs10">
                {getLabelValue(labels, 'lbl_createAccount_rememberMeHelpText', 'registration')}
              </BodyCopy>
            </Field>
          </Col>
          <Col
            ignoreGutter={{ small: true, medium: true, large: true }}
            colSize={{ small: 6 }}
            className="card__btn--medium create-account-btn"
          >
            <Button
              buttonVariation="fixed-width"
              fill="BLUE"
              type="submit"
              data-locator="create-account-btn"
            >
              {getLabelValue(labels, 'lbl_createAccount_createAccount', 'registration')}
            </Button>
          </Col>
          <Col
            ignoreGutter={{ small: true, medium: true, large: true }}
            colSize={{ small: 6 }}
            className="already-account align-center"
          >
            <ClickTracker
              onClick={() => {
                setLoyaltyLocation('Registration');
              }}
              clickData={getLoginPayload('Registration')}
            >
              <Anchor
                fontSizeVariation="large"
                className="already-account"
                onClick={onAlreadyHaveAnAccountClick}
              >
                {getLabelValue(labels, 'lbl_createAccount_alreadyAccount', 'registration')}
              </Anchor>
            </ClickTracker>
          </Col>
        </Row>
      </form>
    </div>
  );
};

const validateMethod = createValidateMethod(
  getStandardConfig([
    'firstName',
    'lastName',
    'phoneNumber',
    'noCountryZip',
    'emailAddress',
    'password',
    'iAgree',
  ])
);

CreateAccountForm = reduxForm({
  form: constants.CREATE_ACCOUNT_FORM, // a unique identifier for this form
  ...validateMethod,
  enableReinitialize: true,
  onSubmitFail: (errors) => scrollToFirstError(errors),
})(CreateAccountForm);

CreateAccountForm.propTypes = {
  isMakeDefaultDisabled: PropTypes.string.isRequired,
  handleSubmit: PropTypes.string.isRequired,
  labels: PropTypes.string.isRequired,
  hideShowPwd: PropTypes.bool.isRequired,
  onAlreadyHaveAnAccountClick: PropTypes.string.isRequired,
  className: PropTypes.string.isRequired,
  userplccCardNumber: PropTypes.string.isRequired,
  userplccCardId: PropTypes.string.isRequired,
  onSubmit: PropTypes.func.isRequired,
  formErrorMessage: PropTypes.shape({}),
  isUserLoggedIn: PropTypes.bool,
};

CreateAccountForm.defaultProps = {
  formErrorMessage: {
    lbl_err_email_req: 'Please enter a valid email address',
  },
  isUserLoggedIn: false,
};

export default withStyles(CreateAccountForm, Styles);
export { CreateAccountForm as CreateAccountFormVanilla };
