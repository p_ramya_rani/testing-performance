// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { getViewportInfo } from '@tcp/core/src/utils';
import { CreateAccountFormVanilla, focusFirstFieldForDesktop } from '../CreateAccountForm';

jest.mock('@tcp/core/src/utils', () => ({
  isMobileWeb: jest.fn(),
  getViewportInfo: jest.fn(),
  getAPIConfig: () => ({
    brandId: 'tcp',
  }),
  isCanada: jest.fn(),
  isClient: jest.fn().mockImplementation(() => true),
  getIconPath: jest.fn(),
  isMobileApp: jest.fn(),
}));

const setFirstNameFocusMocked = jest.fn();

describe('CreateAccountForm', () => {
  it('should render correctly', () => {
    const tree = shallow(<CreateAccountFormVanilla />);
    expect(tree).toMatchSnapshot();
  });

  it('should render correctly with onSubmit and formErrorMessage', () => {
    const props = {
      onSubmit: jest.fn(),
      formErrorMessage: {},
    };
    const tree = shallow(<CreateAccountFormVanilla {...props} />);
    expect(tree).toMatchSnapshot();
  });

  it('should not call setFirstNameFocusMocked for mobile web', () => {
    getViewportInfo.mockImplementation(() => ({ isMobile: true }));
    focusFirstFieldForDesktop(setFirstNameFocusMocked);
    expect(setFirstNameFocusMocked).not.toBeCalled();
  });

  it('should call setFirstNameFocusMocked for desktop', () => {
    getViewportInfo.mockImplementation(() => ({ isMobile: false }));
    focusFirstFieldForDesktop(setFirstNameFocusMocked);
    expect(setFirstNameFocusMocked).toBeCalled();
  });
});
