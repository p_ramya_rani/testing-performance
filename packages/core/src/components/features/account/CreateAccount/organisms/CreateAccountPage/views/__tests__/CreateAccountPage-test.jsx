// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { isCanada } from '@tcp/core/src/utils';
import { CreateAccounPageVanilla } from '../CreateAccounPage';

jest.mock('@tcp/core/src/utils', () => ({
  isMobileWeb: jest.fn(),
  getViewportInfo: jest.fn(),
  getAPIConfig: () => ({
    brandId: 'tcp',
  }),
  isCanada: jest.fn(),
  isMobileApp: jest.fn(),
  isClient: jest.fn().mockImplementation(() => true),
  getIconPath: jest.fn(),
  getStaticFilePath: jest.fn(),
  getImageFilePath: jest.fn(),
}));

describe('Create Account Page', () => {
  it('should render correctly for US', () => {
    isCanada.mockImplementation(() => false);
    const mockedcreateAccountAction = jest.fn();
    const tree = shallow(
      <CreateAccounPageVanilla
        createAccountAction={mockedcreateAccountAction}
        labels={{ lbl_createAccount_hide: 'hide', registration: {} }}
        className=""
      />
    );
    tree.instance().handleSubmitForm();
    expect(mockedcreateAccountAction).toBeCalled();
    expect(tree).toMatchSnapshot();
  });

  it('should render correctly for canada', () => {
    isCanada.mockImplementation(() => true);
    const tree = shallow(
      <CreateAccounPageVanilla
        labels={{ lbl_createAccount_hide: 'hide', registration: {} }}
        className=""
      />
    );
    expect(tree).toMatchSnapshot();
  });

  it('should render correctly with existingUserInformation', () => {
    isCanada.mockImplementation(() => false);
    const tree = shallow(
      <CreateAccounPageVanilla
        labels={{ lbl_createAccount_hide: 'hide', registration: {} }}
        className=""
        existingUserInformation={{
          firstName: 'test',
          lastName: 'test',
          zipCode: '0000',
          emailAddress: 'test@test.com',
          phoneNumber: '0000000000',
        }}
      />
    );
    expect(tree).toMatchSnapshot();
  });

  it('should render correctly with showSkeleton', () => {
    isCanada.mockImplementation(() => false);
    const tree = shallow(
      <CreateAccounPageVanilla
        labels={{ lbl_createAccount_hide: 'hide', registration: {} }}
        className=""
        existingUserInformation={{
          firstName: 'test',
          lastName: 'test',
          zipCode: '0000',
          emailAddress: 'test@test.com',
          phoneNumber: '0000000000',
        }}
        showSkeleton
      />
    );
    expect(tree).toMatchSnapshot();
  });
});
