// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import { isCanada } from '@tcp/core/src/utils';
import Notification from '../../../../../../common/molecules/Notification';
import withStyles from '../../../../../../common/hoc/withStyles';
import styles from '../styles/CreateAccounPage.style';
import CreateAccountForm from '../../../molecules/CreateAccountForm';
import PasswordRequirement from '../../../../ResetPassword/molecules/PasswordRequirement';
import CreateAccountTopSection from '../../../molecules/CreateAccountTopSection';
import CreateAccountSkeleton from '../../../molecules/skeleton/CreateAccountSkeleton.view';
import BodyCopy from '../../../../../../common/atoms/BodyCopy';

class CreateAccounPage extends React.Component {
  static propTypes = {
    createAccountAction: PropTypes.func,
    className: PropTypes.string,
    labels: PropTypes.shape({}),
    isIAgreeChecked: PropTypes.bool,
    hideShowPwd: PropTypes.bool,
    confirmHideShowPwd: PropTypes.bool,
    error: PropTypes.string,
    onAlreadyHaveAnAccountClick: PropTypes.func,
    showForgotPasswordForm: PropTypes.func,
    isUserLoggedIn: PropTypes.bool.isRequired,
    formErrorMessage: PropTypes.shape({}).isRequired,
    userplccCardNumber: PropTypes.string.isRequired,
    userplccCardId: PropTypes.string.isRequired,
    closeOverlay: PropTypes.func,
    isResetPassCodeValidationEnabled: PropTypes.bool.isRequired,
    existingUserInformation: PropTypes.shape({}),
    fullPageView: PropTypes.bool,
    formSubmissionTriggered: PropTypes.bool,
    showSkeleton: PropTypes.bool,
  };

  static defaultProps = {
    createAccountAction: () => {},
    className: '',
    labels: {
      lbl_createAccount_hide: 'hide',
    },
    isIAgreeChecked: false,
    hideShowPwd: false,
    confirmHideShowPwd: false,
    error: {},
    onAlreadyHaveAnAccountClick: () => {},
    showForgotPasswordForm: () => {},
    closeOverlay: () => {},
    existingUserInformation: {},
    fullPageView: false,
    formSubmissionTriggered: false,
    showSkeleton: false,
  };

  constructor(props) {
    super(props);
    this.parentRef = React.createRef();
    this.handleSubmitForm = this.handleSubmitForm.bind(this);
    this.handleSubmitCalled = false;
  }

  componentDidMount() {
    const { showSkeleton } = this.props;
    if (this.parentRef && !showSkeleton) {
      setTimeout(() => {
        this.parentRef.current.scrollIntoView();
      }, 500);
    }
    // adding 500 to test on safari
  }

  componentDidUpdate(prevProps) {
    const { isUserLoggedIn, error } = this.props;
    const container = document.getElementById('dialogContent');
    const userLoggedIn = !prevProps.isUserLoggedIn && isUserLoggedIn;
    const errorTriggered = error && !prevProps.error;

    // Scrolling up to see the success end error message
    if ((userLoggedIn || errorTriggered) && container) {
      container.scrollTo(0, 0);
    }

    if ((userLoggedIn || errorTriggered) && this.parentRef) {
      this.parentRef.current.scrollIntoView();
    }
  }

  handleSubmitForm(payload) {
    const { createAccountAction } = this.props;
    this.handleSubmitCalled = true;
    createAccountAction(payload);
  }

  render() {
    const {
      className,
      labels,
      isIAgreeChecked,
      hideShowPwd,
      confirmHideShowPwd,
      error,
      onAlreadyHaveAnAccountClick,
      showForgotPasswordForm,
      isUserLoggedIn,
      formErrorMessage,
      userplccCardNumber,
      userplccCardId,
      closeOverlay,
      isResetPassCodeValidationEnabled,
      existingUserInformation = {},
      fullPageView,
      formSubmissionTriggered,
      showSkeleton,
    } = this.props;

    const {
      firstName = '',
      lastName = '',
      zipCode: noCountryZip = '',
      emailAddress = '',
      phoneNumber = '',
    } = existingUserInformation;

    const emailAddressLowerCased = emailAddress.toLowerCase();
    const showSkeletonUI = !this.handleSubmitCalled && showSkeleton;
    return (
      <div className={className} ref={this.parentRef}>
        <div className="parent-wrapper">
          {showSkeletonUI && <CreateAccountSkeleton />}
          {!showSkeletonUI && (
            <CreateAccountTopSection
              labels={labels}
              showForgotPasswordForm={showForgotPasswordForm}
              closeOverlay={closeOverlay}
              isResetPassCodeValidationEnabled={isResetPassCodeValidationEnabled}
              fullPageView={fullPageView}
            />
          )}
          {error && !isUserLoggedIn && (
            <div className="elem-pl-LRG elem-pr-LRG elem-pt-LRG">
              <BodyCopy
                fontFamily={['secondary']}
                fontWeight="semibold"
                fontSize="fs12"
                color="error"
              >
                {error}
              </BodyCopy>
            </div>
          )}

          {isUserLoggedIn && formSubmissionTriggered && (
            <div className="elem-pl-LRG elem-pr-LRG elem-pt-LRG">
              <Notification
                status="success"
                colSize={{ large: 12, medium: 8, small: 6 }}
                message={getLabelValue(labels, 'lbl_createAccount_succcessMsg', 'registration')}
              />
            </div>
          )}
          {!showSkeletonUI && (
            <CreateAccountForm
              className={className}
              labels={labels}
              onSubmit={this.handleSubmitForm}
              isIAgreeChecked={isIAgreeChecked}
              hideShowPwd={hideShowPwd}
              initialValues={{
                rememberMe: true,
                plcc_checkbox: !isCanada(),
                firstName,
                lastName,
                noCountryZip,
                emailAddress: emailAddressLowerCased,
                phoneNumber,
                confirmEmailAddress: emailAddressLowerCased,
              }}
              confirmHideShowPwd={confirmHideShowPwd}
              onAlreadyHaveAnAccountClick={onAlreadyHaveAnAccountClick}
              tooltipContent={<PasswordRequirement labels={labels.password} />}
              formErrorMessage={formErrorMessage}
              userplccCardNumber={userplccCardNumber}
              userplccCardId={userplccCardId}
              fullPageView={fullPageView}
              isUserLoggedIn={isUserLoggedIn}
            />
          )}
        </div>
      </div>
    );
  }
}

export default withStyles(CreateAccounPage, styles);
export { CreateAccounPage as CreateAccounPageVanilla };
