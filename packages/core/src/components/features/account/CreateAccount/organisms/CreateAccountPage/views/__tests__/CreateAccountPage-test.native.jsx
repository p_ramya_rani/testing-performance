// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { CreateAccounPageVanilla } from '../CreateAccounPage.native';

jest.mock('@tcp/core/src/utils', () => ({
  isMobileWeb: jest.fn(),
  getViewportInfo: jest.fn(),
  getAPIConfig: () => ({
    brandId: 'tcp',
  }),
  isMobileApp: jest.fn(),
  isClient: jest.fn(),
  getIconPath: jest.fn(),
  isCanada: jest.fn().mockImplementation(() => false),
}));

describe('Create Account Page', () => {
  it('should render correctly for US', () => {
    const mockedcreateAccountAction = jest.fn();
    const tree = shallow(
      <CreateAccounPageVanilla
        createAccountAction={mockedcreateAccountAction}
        labels={{ lbl_createAccount_hide: 'hide', registration: {} }}
        className=""
      />
    );
    expect(tree).toMatchSnapshot();
  });

  it('should render correctly with existingUserInformation', () => {
    const mockedcreateAccountAction = jest.fn();
    const tree = shallow(
      <CreateAccounPageVanilla
        createAccountAction={mockedcreateAccountAction}
        labels={{ lbl_createAccount_hide: 'hide', registration: {} }}
        className=""
        existingUserInformation={{
          firstName: 'test',
          lastName: 'test',
          zipCode: '0000',
          emailAddress: 'test@test.com',
          phoneNumber: '0000000000',
        }}
      />
    );
    expect(tree).toMatchSnapshot();
  });
});
