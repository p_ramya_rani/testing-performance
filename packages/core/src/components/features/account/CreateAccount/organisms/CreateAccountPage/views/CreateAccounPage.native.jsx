// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { withTheme } from 'styled-components/native';
import isEmpty from 'lodash/isEmpty';
import { View, ScrollView } from 'react-native';
import PropTypes, { shape } from 'prop-types';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import { isCanada, getValueFromAsyncStorage } from '@tcp/core/src/utils';
import withStyles from '../../../../../../common/hoc/withStyles';
import { Styles } from '../styles/CreateAccounPage.style.native';
import CreateAccountForm from '../../../molecules/CreateAccountForm';
import CreateAccountTopSection from '../../../molecules/CreateAccountTopSection';
import {
  setUserLoginDetails,
  resetTouchPassword,
  touchIDCheck,
  isSupportedTouch,
} from '../../../../LoginPage/container/loginUtils/keychain.utils.native';
import withKeyboardAvoidingView from '../../../../../../common/hoc/withKeyboardAvoidingView.native';
import { setValueInAsyncStorage } from '../../../../../../../utils/utils.app';

const USER_ACCOUNT_KEY = 'user_account';
class CreateAccounPage extends React.Component {
  static propTypes = {
    createAccountAction: PropTypes.func,
    labels: {},
    isIAgreeChecked: PropTypes.bool,
    onRequestClose: PropTypes.func,
    error: PropTypes.string,
    showForgotPasswordForm: PropTypes.func,
    showLogin: PropTypes.func.isRequired,
    userplccCardNumber: PropTypes.string,
    userplccCardId: PropTypes.string,
    formErrorMessage: shape({}).isRequired,
    toastMessage: PropTypes.func,
    passwordLabels: PropTypes.shape({}).isRequired,
    existingUserInformation: PropTypes.shape({}),
    loyaltyPageName: PropTypes.string,
    isLoading: PropTypes.bool.isRequired,
  };

  static defaultProps = {
    createAccountAction: () => {},
    labels: PropTypes.shape({}),
    isIAgreeChecked: false,
    onRequestClose: () => {},
    showForgotPasswordForm: () => {},
    error: {},
    userplccCardNumber: '',
    userplccCardId: '',
    toastMessage: () => {},
    existingUserInformation: {},
    loyaltyPageName: 'account',
  };

  constructor(props) {
    super(props);
    this.handleSubmitForm = this.handleSubmitForm.bind(this);
    this.state = {
      hideShowPwd: false,
      confirmHideShowPwd: false,
      getTouchStatus: false,
      userLocalData: {},
    };
    this.onPwdHideShowClick = this.onPwdHideShowClick.bind(this);
    this.onConfirmPwdHideShowClick = this.onConfirmPwdHideShowClick.bind(this);
    isSupportedTouch().then((biometryType) => {
      this.setState({ getTouchStatus: biometryType });
    });
  }

  componentDidMount() {
    this.getUserInfoFromStorage();
  }

  componentDidUpdate(prevProps) {
    const { error, toastMessage } = this.props;
    if (!prevProps.error && error) {
      toastMessage(error);
    }
  }

  onPwdHideShowClick = (value) => {
    this.setState({ hideShowPwd: value });
  };

  onConfirmPwdHideShowClick = (value) => {
    this.setState({ confirmHideShowPwd: value });
  };

  getUserInfoFromStorage = () => {
    getValueFromAsyncStorage(USER_ACCOUNT_KEY).then((userData) => {
      if (userData) {
        this.setState({
          userLocalData: JSON.parse(userData) || {},
        });
      }
    });
  };

  // when account is cceate handle submit will submit the form
  handleSubmitForm(payload) {
    const { createAccountAction, toastMessage, labels } = this.props;
    if (payload.iAgree) {
      createAccountAction(payload);
      resetTouchPassword();
      setUserLoginDetails(payload.emailAddress, payload.password);
      setValueInAsyncStorage('appFirstForSBP', 'true');
      isSupportedTouch().then((biometryType) => {
        if (biometryType && payload.toggleValue) {
          touchIDCheck();
        }
      });
    } else {
      toastMessage(getLabelValue(labels, 'lbl_err_iagree', 'formValidation'));
    }
  }

  render() {
    const {
      labels,
      isIAgreeChecked,
      onRequestClose,
      showForgotPasswordForm,
      showLogin,
      formErrorMessage,
      userplccCardNumber,
      userplccCardId,
      passwordLabels,
      existingUserInformation = {},
      loyaltyPageName,
      isLoading,
    } = this.props;

    const { hideShowPwd, confirmHideShowPwd, getTouchStatus, userLocalData } = this.state;
    const initialUserData = !isEmpty(existingUserInformation)
      ? existingUserInformation
      : userLocalData;
    const {
      firstName = '',
      lastName = '',
      zipCode: noCountryZip = '',
      emailAddress = '',
      phoneNumber = '',
    } = initialUserData;
    const emailAddressLowerCased = emailAddress.toLowerCase();
    return (
      <ScrollView
        showsVerticalScrollIndicator={false}
        {...this.props}
        keyboardShouldPersistTaps="handled"
      >
        <View>
          <CreateAccountTopSection
            showForgotPasswordForm={showForgotPasswordForm}
            labels={labels}
            showLogin={showLogin}
          />

          <CreateAccountForm
            getTouchStatus={getTouchStatus}
            labels={labels}
            handleSubmitForm={this.handleSubmitForm}
            onPwdHideShowClick={this.onPwdHideShowClick}
            hideShowPwd={hideShowPwd}
            initialValues={{
              plcc_checkbox: !isCanada(),
              firstName,
              lastName,
              noCountryZip,
              emailAddress: emailAddressLowerCased,
              phoneNumber,
            }}
            onConfirmPwdHideShowClick={this.onConfirmPwdHideShowClick}
            confirmHideShowPwd={confirmHideShowPwd}
            isIAgreeChecked={isIAgreeChecked}
            onRequestClose={onRequestClose}
            showLogin={showLogin}
            userplccCardNumber={userplccCardNumber}
            formErrorMessage={formErrorMessage}
            userplccCardId={userplccCardId}
            passwordLabels={passwordLabels}
            loyaltyPageName={loyaltyPageName}
            isLoading={isLoading}
          />
        </View>
      </ScrollView>
    );
  }
}

export default withStyles(withKeyboardAvoidingView(withTheme(CreateAccounPage)), Styles);
export { CreateAccounPage as CreateAccounPageVanilla };
