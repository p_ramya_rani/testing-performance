// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { routerPush } from '@tcp/core/src/utils';
import { CreateAccountContainer, mapDispatchToProps } from '../CreateAccount.container';
import CreateAccountView from '../../views/CreateAccountView';

jest.mock('@tcp/core/src/utils', () => ({
  isMobileApp: jest.fn(),
  routerPush: jest.fn(),
  getAPIConfig: jest.fn(),
  isClient: jest.fn(),
  getIconPath: jest.fn(),
  getStaticFilePath: jest.fn(),
  getImageFilePath: jest.fn(),
}));

describe('CreateAccount', () => {
  const props = {
    setLoginModalMountState: () => {},
    showLogin: () => {},
    formErrorMessage: {},
  };
  const mockedRouterPush = jest.fn();
  it('should render CreateAccountView component', () => {
    const tree = shallow(<CreateAccountContainer {...props} />);
    expect(tree.is(CreateAccountView)).toBeTruthy();
  });

  it('should call routerpush with home on success', () => {
    routerPush.mockImplementation(mockedRouterPush);
    const tree = shallow(<CreateAccountContainer {...props} />);
    const wrapperInstance = tree.instance();
    wrapperInstance.handleSuccessRedirection();
    expect(mockedRouterPush).toBeCalledWith('/', '/home');
  });

  it('should call routerpush with home on success after landiing from confirmation page', () => {
    routerPush.mockImplementation(mockedRouterPush);
    const props1 = { ...props, router: { query: { successtarget: '/checkout/confirmation' } } };
    const tree1 = shallow(<CreateAccountContainer {...props1} />);
    const wrapperInstance = tree1.instance();
    wrapperInstance.handleSuccessRedirection();
    expect(mockedRouterPush).toBeCalledWith('/', '/home');
  });

  it('should call routerpush with home on success after landiing from login page', () => {
    routerPush.mockImplementation(mockedRouterPush);
    const props1 = { ...props, router: { query: { successtarget: '/home/login' } } };
    const tree1 = shallow(<CreateAccountContainer {...props1} />);
    const wrapperInstance = tree1.instance();
    wrapperInstance.handleSuccessRedirection();
    expect(mockedRouterPush).toBeCalledWith('/', '/home');
  });

  it('should call openOverlay on success for fullpageview', () => {
    const mockedOpenOverlay = jest.fn();
    routerPush.mockImplementation(mockedRouterPush);
    const newProps = {
      ...props,
      fullPageView: true,
      router: { query: { successtarget: 'referrer' }, openOverlay: mockedOpenOverlay },
    };
    const tree = shallow(<CreateAccountContainer {...newProps} />);
    const wrapperInstance = tree.instance();
    wrapperInstance.handleSuccessRedirection();
    expect(mockedOpenOverlay).not.toBeCalled();
  });

  describe('#instance', () => {
    let wrapperInstance;
    let wrapper;
    beforeEach(() => {
      wrapper = shallow(<CreateAccountContainer {...props} />);
      wrapperInstance = wrapper.instance();
    });

    it('openModal should call setLoginModalMountState action if prop is present', () => {
      const setLoginModalMountState = jest.fn();
      wrapper.setProps({
        setLoginModalMountState,
      });
      wrapperInstance.openModal({});
      expect(setLoginModalMountState).toBeCalled();
    });

    it('openModal should call openOverlay action if prop is present', () => {
      const openOverlay = jest.fn();
      wrapper.setProps({
        setLoginModalMountState: null,
        openOverlay,
      });
      wrapperInstance.openModal({});
      expect(openOverlay).toBeCalled();
    });

    it('onAlreadyHaveAnAccountClick should call setLoginModalMountState action if prop is present', () => {
      const setLoginModalMountState = jest.fn();
      wrapper.setProps({
        setLoginModalMountState,
      });
      wrapperInstance.onAlreadyHaveAnAccountClick({
        preventDefault: () => {},
      });
      expect(setLoginModalMountState).toBeCalled();
    });

    it('onAlreadyHaveAnAccountClick should call openOverlay action if prop is present', () => {
      const openOverlay = jest.fn();
      wrapper.setProps({
        setLoginModalMountState: null,
        openOverlay,
      });
      wrapperInstance.onAlreadyHaveAnAccountClick({
        preventDefault: () => {},
      });
      expect(openOverlay).toBeCalled();
    });

    it('should set formSubmissionTriggered to true on submission', () => {
      const openOverlay = jest.fn();
      wrapper.setProps({
        setLoginModalMountState: null,
        openOverlay,
      });
      wrapperInstance.submitCreateAccountForm({});
      expect(wrapperInstance.formSubmissionTriggered).toBeTruthy();
    });
  });

  describe('#mapDispatchToProps', () => {
    it('should return an action createAccountAction which will call dispatch function on execution', () => {
      const dispatch = jest.fn();
      const dispatchProps = mapDispatchToProps(dispatch);
      dispatchProps.createAccountAction();
      expect(dispatch.mock.calls).toHaveLength(1);
    });

    it('should return an action closeOverlay which will call dispatch function on execution', () => {
      const dispatch = jest.fn();
      const dispatchProps = mapDispatchToProps(dispatch);
      dispatchProps.closeOverlay();
      expect(dispatch.mock.calls).toHaveLength(1);
    });

    it('should return an action openOverlay which will call dispatch function on execution', () => {
      const dispatch = jest.fn();
      const dispatchProps = mapDispatchToProps(dispatch);
      dispatchProps.openOverlay();
      expect(dispatch.mock.calls).toHaveLength(1);
    });

    it('should return an action resetAccountError which will call dispatch function on execution', () => {
      const dispatch = jest.fn();
      const dispatchProps = mapDispatchToProps(dispatch);
      dispatchProps.resetAccountError();
      expect(dispatch.mock.calls).toHaveLength(1);
    });

    it('#toastMessage', () => {
      const dispatch = jest.fn();
      const dispatchProps = mapDispatchToProps(dispatch);
      dispatchProps.toastMessage();
      expect(dispatch.mock.calls).toHaveLength(1);
    });
  });
});
