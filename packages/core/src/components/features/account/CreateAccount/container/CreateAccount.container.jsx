// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import logger from '@tcp/core/src/utils/loggerInstance';
import { toastMessageInfo } from '@tcp/core/src/components/common/atoms/Toast/container/Toast.actions';
import confirmationSelectors from '@tcp/core/src/components/features/CnC/Confirmation/container/Confirmation.selectors';
import { isMobileApp } from '@tcp/core/src/utils';
import { md5 } from '@tcp/core/src/components/features/browse/ProductListing/molecules/ProductReviews/encoding';
import { setLocalStorage } from '@tcp/core/src/utils/localStorageManagement';
import { routerPush, getUrlParameter } from '../../../../../utils';
import CreateAccountView from '../views/CreateAccountView';
import { createAccount, resetCreateAccountErr } from './CreateAccount.actions';
import {
  getIAgree,
  getHideShowPwd,
  getConfirmHideShowPwd,
  getLabels,
  getErrorMessage,
  getPasswordLabels,
  getLoadingState,
  getAccessibilityLabels,
} from './CreateAccount.selectors';
import {
  getUserLoggedInState,
  getplccCardId,
  getplccCardNumber,
  getUserEmail,
} from '../../User/container/User.selectors';
import { API_CONFIG } from '../../../../../services/config';
import {
  closeOverlayModal,
  openOverlayModal,
} from '../../OverlayModal/container/OverlayModal.actions';
import { getResetPassCodeValidationFlow } from '../../ResetPassword/container/ResetPassword.selectors';

import { getFormValidationErrorMessages } from '../../Account/container/Account.selectors';
import { setClickAnalyticsData, trackClick } from '../../../../../analytics/actions';
import { getPageNameFromState } from '../../../../../reduxStore/selectors/session.selectors';

const noop = () => {};

export class CreateAccountContainer extends React.Component {
  static propTypes = {
    className: PropTypes.string,
    createAccountAction: PropTypes.func,
    hideShowPwd: PropTypes.bool,
    confirmHideShowPwd: PropTypes.bool,
    error: PropTypes.string,
    openOverlay: PropTypes.func,
    onRequestClose: PropTypes.func,
    isIAgreeChecked: PropTypes.bool,
    resetAccountError: PropTypes.func,
    labels: PropTypes.shape({}),
    isUserLoggedIn: PropTypes.bool,
    closeOverlay: PropTypes.func,
    navigation: PropTypes.shape({}),
    setLoginModalMountState: PropTypes.bool.isRequired,
    showLogin: PropTypes.func.isRequired,
    formErrorMessage: PropTypes.shape({}).isRequired,
    userplccCardNumber: PropTypes.string.isRequired,
    userplccCardId: PropTypes.string.isRequired,
    toastMessage: PropTypes.func,
    passwordLabels: PropTypes.shape({}).isRequired,
    isLoading: PropTypes.bool.isRequired,
    updateHeader: PropTypes.func,
    showCheckoutModal: PropTypes.func.isRequired,
    isResetPassCodeValidationEnabled: PropTypes.bool.isRequired,
    existingUserInformation: PropTypes.shape({}),
    fullPageView: PropTypes.bool,
    userEmail: PropTypes.string,
    router: PropTypes.shape({}),
    isRegisteredUserCallDone: PropTypes.bool,
    loyaltyPageName: PropTypes.string,
  };

  static defaultProps = {
    className: '',
    createAccountAction: noop,
    hideShowPwd: false,
    confirmHideShowPwd: false,
    error: '',
    openOverlay: noop,
    onRequestClose: noop,
    isIAgreeChecked: false,
    resetAccountError: noop,
    labels: {},
    closeOverlay: noop,
    isUserLoggedIn: false,
    navigation: {},
    toastMessage: () => {},
    updateHeader: () => {},
    existingUserInformation: {},
    fullPageView: false,
    userEmail: '',
    router: {},
    isRegisteredUserCallDone: false,
    loyaltyPageName: 'account',
  };

  formSubmissionTriggered = false;

  shouldRedirect = false;

  constructor(props) {
    super(props);
    import('../../../../../utils')
      .then(({ navigateToNestedRoute }) => {
        this.hasMobileApp = isMobileApp;
        this.hasNavigateToNestedRoute = navigateToNestedRoute;
      })
      .catch(error => {
        logger.error('error: ', error);
      });
  }

  componentDidUpdate(prevProps) {
    const { isUserLoggedIn, closeOverlay, onRequestClose, userEmail } = this.props;
    if (!prevProps.isUserLoggedIn && isUserLoggedIn) {
      this.shouldRedirect = true;
      if (this.hasMobileApp()) {
        onRequestClose({ getComponentId: { login: '', createAccount: '' } });
      } else {
        if (userEmail && window && window._dataLayer) {
          const hash = md5(userEmail.toLowerCase()).toUpperCase();
          setLocalStorage({ key: 'email_hash', value: hash });
          /* eslint no-underscore-dangle: 0 */
          window._dataLayer.email_hash = hash;
        }
        const closeOverlayTimeout = setTimeout(() => {
          closeOverlay();
          clearTimeout(closeOverlayTimeout);
        }, API_CONFIG.overlayTimeout);
        const redirectonsuccess = getUrlParameter('redirectOnSuccess');
        if (redirectonsuccess) {
          routerPush('/account?id=profile', redirectonsuccess);
        } else {
          this.handleSuccessRedirection();
        }
      }
    }
  }

  componentWillUnmount() {
    const { resetAccountError } = this.props;
    resetAccountError();
  }

  handleSuccessRedirection = () => {
    const { openOverlay, fullPageView, router } = this.props;
    const { successtarget = '/home' } = (router && router.query) || {};

    if (fullPageView) {
      const timeoutValue = !this.formSubmissionTriggered ? 0 : API_CONFIG.overlayTimeout;
      const closeOverlayTimeout = setTimeout(() => {
        if (
          successtarget === '/home' ||
          successtarget.indexOf('confirmation') !== -1 ||
          successtarget.indexOf('login') !== -1
        ) {
          routerPush('/', '/home');
        } else if (window.history && window.history.back) {
          window.history.back();
        }
        openOverlay({
          component: 'accountDrawer',
          variation: 'primary',
        });
        clearTimeout(closeOverlayTimeout);
      }, timeoutValue);
    } else {
      routerPush('/', '/home');
    }
  };

  onAlreadyHaveAnAccountClick = e => {
    e.preventDefault();
    const { openOverlay, setLoginModalMountState, fullPageView } = this.props;
    const { pathname = 'referrer' } = (window && window.location) || {};
    if (setLoginModalMountState) {
      setLoginModalMountState({
        component: 'login',
      });
    } else if (fullPageView) {
      routerPush(`/home?target=login&successtarget=${pathname}`, '/home/login');
    } else {
      openOverlay({
        component: 'login',
        variation: 'primary',
      });
    }
  };

  openModal = params => {
    const { openOverlay, setLoginModalMountState } = this.props;
    if (setLoginModalMountState) {
      setLoginModalMountState(params);
    } else {
      openOverlay(params);
    }
  };

  submitCreateAccountForm = payload => {
    const { createAccountAction } = this.props;
    this.formSubmissionTriggered = true;
    createAccountAction(payload);
  };

  render() {
    const {
      className,
      isIAgreeChecked,
      hideShowPwd,
      confirmHideShowPwd,
      error,
      onRequestClose,
      labels,
      showLogin,
      isUserLoggedIn,
      formErrorMessage,
      userplccCardNumber,
      userplccCardId,
      toastMessage,
      passwordLabels,
      isLoading,
      updateHeader,
      showCheckoutModal,
      closeOverlay,
      isResetPassCodeValidationEnabled,
      existingUserInformation,
      fullPageView,
      isRegisteredUserCallDone,
      loyaltyPageName,
    } = this.props;
    const showSkeleton =
      fullPageView &&
      (!isRegisteredUserCallDone || (this.shouldRedirect && !this.formSubmissionTriggered));
    return (
      <CreateAccountView
        className={className}
        createAccountAction={this.submitCreateAccountForm}
        labels={labels}
        isIAgreeChecked={isIAgreeChecked}
        hideShowPwd={hideShowPwd}
        confirmHideShowPwd={confirmHideShowPwd}
        error={error}
        onAlreadyHaveAnAccountClick={this.onAlreadyHaveAnAccountClick}
        onRequestClose={onRequestClose}
        openModal={this.openModal}
        showLogin={showLogin}
        isUserLoggedIn={isUserLoggedIn}
        formErrorMessage={formErrorMessage}
        userplccCardNumber={userplccCardNumber}
        userplccCardId={userplccCardId}
        toastMessage={toastMessage}
        passwordLabels={passwordLabels}
        isLoading={isLoading}
        updateHeader={updateHeader}
        showCheckoutModal={showCheckoutModal}
        closeOverlay={closeOverlay}
        isResetPassCodeValidationEnabled={isResetPassCodeValidationEnabled}
        existingUserInformation={existingUserInformation}
        fullPageView={fullPageView}
        formSubmissionTriggered={this.formSubmissionTriggered}
        showSkeleton={showSkeleton}
        loyaltyPageName={loyaltyPageName}
      />
    );
  }
}

export const mapStateToProps = state => {
  return {
    isIAgreeChecked: getIAgree(state),
    hideShowPwd: getHideShowPwd(state),
    confirmHideShowPwd: getConfirmHideShowPwd(state),
    isUserLoggedIn: getUserLoggedInState(state),
    userplccCardNumber: getplccCardNumber(state),
    userplccCardId: getplccCardId(state),
    error: getErrorMessage(state),
    labels: {
      ...getLabels(state),
      ...getAccessibilityLabels(state),
    },
    formErrorMessage: getFormValidationErrorMessages(state),
    passwordLabels: getPasswordLabels(state),
    isLoading: getLoadingState(state),
    isResetPassCodeValidationEnabled: getResetPassCodeValidationFlow(state),
    existingUserInformation: confirmationSelectors.getInitialCreateAccountValues(state),
    userEmail: getUserEmail(state),
    loyaltyPageName: getPageNameFromState(state),
  };
};

export const mapDispatchToProps = dispatch => {
  return {
    createAccountAction: payload => {
      dispatch(createAccount(payload));
    },
    closeOverlay: () => {
      dispatch(closeOverlayModal());
    },
    openOverlay: payload => {
      dispatch(openOverlayModal(payload));
    },
    resetAccountError: () => {
      dispatch(resetCreateAccountErr());
    },
    toastMessage: error => {
      dispatch(toastMessageInfo(error));
    },
    trackAnalyticsClick: (payload, eventData) => {
      dispatch(setClickAnalyticsData(eventData));
      const timer = setTimeout(() => {
        dispatch(trackClick(payload));
        clearTimeout(timer);
      }, 100);
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateAccountContainer);
