/* eslint-disable sonarjs/cognitive-complexity */
// 9fbef606107a605d69c0edbcd8029e5d
import { takeLatest, call, put, delay, take, select } from 'redux-saga/effects';
import { setClickAnalyticsData, trackClick, trackFormError } from '@tcp/core/src/analytics/actions';
import { updateMiniBagFlag } from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.actions';
import { getOrderEmailAddress } from '@tcp/core/src/components/features/CnC/Confirmation/container/Confirmation.selectors';
import { setValueInAsyncStorage } from '@tcp/core/src/utils';
import { md5 } from '@tcp/core/src/components/features/browse/ProductListing/molecules/ProductReviews/encoding';
import { getIsSNJEnabled } from '@tcp/core/src/reduxStore/selectors/session.selectors';
import {
  setCreateAccountTempPass,
  setCreateAdsAccountFailed,
} from '@tcp/core/src/components/features/browse/ApplyCardPage/container/ApplyCard.actions';
import CREATE_ACCOUNT_CONSTANTS from '../CreateAccount.constants';
import CONSTANTS from '../../User/User.constants';
import { getUserInfo } from '../../User/container/User.actions';
import { navigateXHRAction } from '../../NavigateXHR/container/NavigateXHR.action';
import { createAccountErr, setLoadingState } from './CreateAccount.actions';
import { createAccountApi } from '../../../../../services/abstractors/account';
import { setCreateAccountSuccess } from '../../../CnC/Confirmation/container/Confirmation.actions';
import CONFIRMATION_CONSTANTS from '../../../CnC/Confirmation/Confirmation.constants';
import briteVerifyStatusExtraction from '../../../../../services/abstractors/common/briteVerifyStatusExtraction';
import { getplccCardId, getUserId } from '../../User/container/User.selectors';
import names from '../../../../../constants/eventsName.constants';
import { getPageNameFromState } from '../../../../../reduxStore/selectors/session.selectors';
import { isMobileApp } from '../../../../../utils';
import { loginSFCCSaga } from '../../LoginPage/container/LoginPage.saga';

const getErrorMessage = (res) => {
  let errorMessageRecieved = '';
  errorMessageRecieved = res && res.body && res.body.errors && res.body.errors[0].errorMessage;
  return {
    errorMessage: errorMessageRecieved,
  };
};

export const checkoutForOrderSession = (orderEmailId) => {
  return orderEmailId ? orderEmailId.trim().length > 0 : false;
};

const setValueInAsyncStorageForMobile = (emailAddress) => {
  if (isMobileApp() && emailAddress) {
    const emailHash = md5(emailAddress.toLowerCase()).toUpperCase();
    setValueInAsyncStorage('email_hash', emailHash);
  }
};

const setUsernameInAsyncStorage = (username) => {
  if (isMobileApp() && username) {
    setValueInAsyncStorage('username', username);
  }
};

function* setAnalyticsData() {
  let pageName = '';
  if (isMobileApp()) {
    pageName = yield select(getPageNameFromState);
  } else {
    pageName = window && window.loyaltyLocation;
  }
  yield put(
    setClickAnalyticsData({
      eventName: 'create account',
      customEvents: ['event13', 'event14', 'event127'],
      pageNavigationText: `${pageName || ''}-create account`,
      clickEvent: true,
      loyaltyLocation: pageName,
    })
  );
}
/* eslint-disable max-statements */
export function* createsaga({ payload }) {
  const orderEmailId = yield select(getOrderEmailAddress);
  const plccCardId = yield select(getplccCardId);
  const isSNJEnabled = yield select(getIsSNJEnabled);
  const { firstName: username } = payload;
  const userId = yield select(getUserId);
  const updatedPayload = plccCardId && payload.plcc_checkbox ? { ...payload, plccCardId } : payload;
  updatedPayload.userId = userId;
  updatedPayload.ordersession = checkoutForOrderSession(orderEmailId);
  yield put(setLoadingState({ isLoading: true }));
  try {
    const { emailAddress, emailValidationStatus, isApplyPlccCardFlow } = payload;
    let emailValidationStatusForApi = null;
    if (emailValidationStatus) {
      emailValidationStatusForApi = emailValidationStatus;
    } else {
      emailValidationStatusForApi = yield call(briteVerifyStatusExtraction, emailAddress);
    }
    const res = yield call(createAccountApi, {
      ...updatedPayload,
      emailValidationStatusForApi,
      isSNJEnabled,
    });
    yield call(setAnalyticsData);
    /* istanbul ignore else */
    if (res.body) {
      if (res.body.errors) {
        const resErr = getErrorMessage(res);
        if (isApplyPlccCardFlow) {
          yield put(setCreateAdsAccountFailed(true));
        }
        return yield put(createAccountErr(resErr));
      }
      if (isApplyPlccCardFlow) {
        updatedPayload.password = res.body.randomPwd;
        yield put(setCreateAccountTempPass(res.body.randomPwd));
      }
      setValueInAsyncStorageForMobile(emailAddress);
      setUsernameInAsyncStorage(username);

      if (payload.isOrderConfirmation) {
        yield put(setCreateAccountSuccess(true));
        yield delay(CONFIRMATION_CONSTANTS.SUCCESS_MESSAGE_TIME_MS);
        yield put(setCreateAccountSuccess(false));
      }
      yield put(navigateXHRAction());
      yield put(setLoadingState({ isLoading: false }));
      if (isSNJEnabled) {
        updatedPayload.useSFCCAPI = true;
        yield call(loginSFCCSaga, { payload: updatedPayload });
      }
      yield put(
        getUserInfo({
          ignoreCache: true,
        })
      );
      // Trgigger analytics event after register user call done
      yield take(CONSTANTS.SET_IS_REGISTERED_USER_CALL_DONE);

      return yield put(
        trackClick({
          name: isMobileApp() ? names.screenNames.register_success : 'siteRegistration_e13',
          module: 'account',
        })
      );
    }
    const resErr = getErrorMessage(res);
    yield put(updateMiniBagFlag(true));
    return yield put(createAccountErr(resErr));
  } catch (err) {
    const { errorCode, errorMessage } = err;
    const { isApplyPlccCardFlow } = payload;
    if (isApplyPlccCardFlow) {
      yield put(setCreateAdsAccountFailed(true));
    }

    const list3VarValues = { ...payload };
    if (errorCode) {
      list3VarValues[`createAccountForm:${errorCode.toLowerCase()}`] = errorCode;
    }

    yield put(
      trackFormError({
        formName: CREATE_ACCOUNT_CONSTANTS.CREATE_ACCOUNT_FORM,
        formData: list3VarValues,
      })
    );

    yield put(setLoadingState({ isLoading: false }));
    return yield put(
      createAccountErr({
        errorCode,
        errorMessage,
      })
    );
  }
}
/* eslint-enable max-statements */

export function* CreateAccountSaga() {
  yield takeLatest(CREATE_ACCOUNT_CONSTANTS.CREATE_AN_ACCOUNT, createsaga);
}

export default CreateAccountSaga;
