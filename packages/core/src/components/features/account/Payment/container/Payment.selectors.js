// 9fbef606107a605d69c0edbcd8029e5d 
import { createSelector } from 'reselect';

const PLCC_CARD = 'PLACE CARD';

export const getPLCCPaymentEnabled = state => {
  return state.session && state.session.siteDetails && state.session.siteDetails.ADSPLCC === '1';
};

export const getCardListState = (state, checkIsPLCCPaymentEnabled = false) => {
  const cardList = state.PaymentReducer.get('cardList');
  if (checkIsPLCCPaymentEnabled && !getPLCCPaymentEnabled(state)) {
    return cardList && cardList.filter(card => card.ccType !== PLCC_CARD);
  }
  return cardList;
};

export const getCreditDebitCards = createSelector(
  [getCardListState],
  creditCardList =>
    creditCardList &&
    creditCardList.filter(card => card.ccType !== 'GiftCard' && card.ccType !== 'VENMO')
);

export const getGiftCards = createSelector(
  [getCardListState],
  creditCardList => creditCardList && creditCardList.filter(card => card.ccType === 'GiftCard')
);

export const getVenmoCards = createSelector(
  [getCardListState],
  creditCardList => creditCardList && creditCardList.filter(card => card.ccType === 'VENMO')
);

export const getCreditCardDefault = createSelector(
  [getCardListState],
  creditCardList =>
    creditCardList &&
    creditCardList.filter(
      card => card.defaultInd === true && card.ccType !== 'VENMO' && card.ccType !== 'GiftCard'
    )
);

export const getMyPlaceRewardCreditCard = createSelector(
  [getCardListState],
  creditCardList => creditCardList && creditCardList.filter(card => card.ccType === PLCC_CARD)
);

export const getCardListFetchingState = state => {
  return state.PaymentReducer.get('isFetching');
};

export const getShowNotificationState = state => {
  return state.PaymentReducer.get('showNotification');
};

export const getShowNotificationCaptchaState = state => {
  return state.PaymentReducer.get('showNotificationCaptcha');
};

export const deleteModalOpenState = state => {
  return state.PaymentReducer.get('deleteModalMountedState');
};

export const showUpdatedNotificationOnModalState = state => {
  return state.PaymentReducer.get('showUpdatedNotificationOnModal');
};

export const checkbalanceValue = state => {
  return state.PaymentReducer.get('giftcardBalance');
};

export const getPaymentBannerContentId = state => {
  let paymentBannerContentId;
  if (state.Labels.account.paymentGC && Array.isArray(state.Labels.account.paymentGC.referred)) {
    state.Labels.account.paymentGC.referred.forEach(label => {
      if (label.name === 'paymentGCTopBanner') paymentBannerContentId = label.contentId;
    });
  }
  return paymentBannerContentId;
};

export const getPaymentBannerRichTextSelector = state => {
  return state.PaymentReducer.get('paymentBannerRichText');
};

export const getLabels = state => state.Labels.account;
