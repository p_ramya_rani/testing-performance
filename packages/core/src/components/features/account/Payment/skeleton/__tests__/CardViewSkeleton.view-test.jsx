// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { CardViewSkeletonVanilla } from '../CardViewSkeleton.view';

describe('CardViewSkeleton component', () => {
  it('should renders correctly', () => {
    const props = {
      className: 'sample-class',
    };
    const component = shallow(<CardViewSkeletonVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
