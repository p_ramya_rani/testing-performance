// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import AddGiftCardComponent from '../AddGiftCard.view';

describe('Add gift card component', () => {
  it('should render component correctly', () => {
    const props = {
      labels: {
        paymentGC: {},
        common: {},
      },
      addGiftCardResponse: 'success',
      onAddGiftCardClick: jest.fn(),
    };
    const component = shallow(<AddGiftCardComponent {...props} />);
    expect(component).toMatchSnapshot();
  });
});
