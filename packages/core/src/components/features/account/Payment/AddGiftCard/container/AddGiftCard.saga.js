// 9fbef606107a605d69c0edbcd8029e5d 
import { call, put, takeLatest } from 'redux-saga/effects';
import { trackFormError } from '@tcp/core/src/analytics/actions';
import { setLoaderState } from '@tcp/core/src/components/common/molecules/Loader/container/Loader.actions';
import ADD_GIFT_CARD_CONSTANTS from '../AddGiftCard.constants';
import { addGiftCardFailure, addGiftCardSuccess } from './AddGiftCard.actions';
import { clearCardListTTL } from '../../container/Payment.actions';
import { addGiftCardApi } from '../../../../../../services/abstractors/account';

export function* addGiftCard({ payload }) {
  const { giftCardNumber, cardPin, recaptchaToken } = payload;
  const requiredList3Val = { giftCardNumber, cardPin, recaptchaToken };
  try {
    yield put(setLoaderState(true));
    const response = yield call(addGiftCardApi, payload);
    yield put(setLoaderState(false));

    if (response && response.body) {
      yield put(clearCardListTTL());
      return yield put(addGiftCardSuccess());
    }
    yield put(
      trackFormError({
        formName: ADD_GIFT_CARD_CONSTANTS.ADD_GIFT_CARD_FORM,
        formData: requiredList3Val,
      })
    );
    return yield put(addGiftCardFailure());
  } catch (err) {
    yield put(
      trackFormError({
        formName: ADD_GIFT_CARD_CONSTANTS.ADD_GIFT_CARD_FORM,
        formData: requiredList3Val,
      })
    );

    let error = {};
    /* istanbul ignore else */
    yield put(setLoaderState(false));
    error = err;
    if (error && error.response) {
      return yield put(addGiftCardFailure(error.response.body.errors[0]));
    }
    return null;
  }
}

export function* AddGiftCardSaga() {
  yield takeLatest(ADD_GIFT_CARD_CONSTANTS.ADD_GIFT_CARD_REQUEST, addGiftCard);
}
