// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  .cardList__heading {
    margin-top: 0;
    margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.SM};
    line-height: 2.8;
    @media ${props => props.theme.mediaQuery.smallOnly} {
      margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.XXS};
    }
  }
  .cardList__col {
    margin-bottom: ${props => props.theme.spacing.LAYOUT_SPACING.XS};
  }
  .cardList__ccAddCta {
    margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.LRG};
    @media ${props => props.theme.mediaQuery.smallOnly} {
      width: 100%;
    }
  }
  .giftcardList__col {
    width: 100%;
  }
  .payment__addBtn__cont {
    @media ${props => props.theme.mediaQuery.smallOnly} {
      text-align: center;
    }
  }
`;

export default styles;
