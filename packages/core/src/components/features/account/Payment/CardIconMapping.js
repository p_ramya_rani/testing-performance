// 9fbef606107a605d69c0edbcd8029e5d 
const cardIconMapping = {
  DISC: 'disc-small',
  MC: 'mc-small',
  Amex: 'amex-small',
  Visa: 'visa-small',
  GC: 'gift-card-small',
  'PLACE CARD': 'place-card-small',
  'MY PLACE REWARDS CREDIT CARD': 'place-card-small',
  VENMO: 'venmo-blue-acceptance-mark',
};
export default cardIconMapping;
