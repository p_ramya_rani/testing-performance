// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import ACCOUNT_CONSTANTS from '@tcp/core/src/components/features/account/Account/Account.constants';
import ClickTracker from '@tcp/web/src/components/common/atoms/ClickTracker';
import Heading from '../../../../common/atoms/Heading';
import EmptyCard from '../../common/molecule/EmptyCard/views/EmptyCard.view';
import Button from '../../../../common/atoms/Button';
import withStyles from '../../../../common/hoc/withStyles';
import styles from '../styles/CardList.style';
import Row from '../../../../common/atoms/Row';
import Col from '../../../../common/atoms/Col';
import { CardView } from './Card.view';
import Router from 'next/router'; //eslint-disable-line
import utils from '../../../../../utils';

const onAddGiftCardClick = () => {
  utils.routerPush(
    '/account?id=payment&subSection=add-gift-card',
    '/account/payment/add-gift-card'
  );
};

const GiftCardList = ({
  labels,
  giftCardList,
  className,
  setDeleteModalMountState,
  deleteModalMountedState,
  onDeleteCard,
  showUpdatedNotificationOnModal,
  onGetBalanceCard,
  checkbalanceValueInfo,
  showNotification,
  showNotificationCaptcha,
  setSelectedCard,
  commonLabels,
  isRecapchaEnabled,
}) => {
  const clickEventdata = {
    customEvents: [],
    eventName: ACCOUNT_CONSTANTS.ACCOUNT_ANALYTICS.navigationName.payment_addGiftCard,
    pageNavigationText: ACCOUNT_CONSTANTS.ACCOUNT_ANALYTICS.navigationText.payment_addGiftCard,
  };

  return (
    <div className={className}>
      <Heading
        variant="h6"
        className="cardList__heading"
        dataLocator="payment-gcAndMerchandiseCards"
      >
        {getLabelValue(labels, 'lbl_payment_gcHeading', 'paymentGC')}
      </Heading>
      {giftCardList.size === 0 && (
        <EmptyCard labels={labels} icon="gift_card" alt="gift card icon" prefix="GC" />
      )}
      <Row fullBleed>
        <Col
          colSize={{
            large: 4,
            medium: 4,
            small: 6,
          }}
          className={`${giftCardList.size !== 0 ? 'payment__addBtn__cont' : ''}`}
        >
          <ClickTracker clickData={clickEventdata}>
            <Button
              buttonVariation="fixed-width"
              fill="BLUE"
              dataLocator="payment-addagiftcard"
              className="cardList__ccAddCta"
              onClick={onAddGiftCardClick}
            >
              {giftCardList.size === 0
                ? getLabelValue(labels, 'lbl_payment_GCEmptyAddBtn', 'paymentGC')
                : getLabelValue(labels, 'lbl_payment_addBtn', 'paymentGC')}
            </Button>
          </ClickTracker>
        </Col>
      </Row>
      {giftCardList.size !== 0 && (
        <CardView
          labels={labels}
          commonLabels={commonLabels}
          deleteModalMountedState={deleteModalMountedState}
          setDeleteModalMountState={setDeleteModalMountState}
          onDeleteCard={onDeleteCard}
          showUpdatedNotificationOnModal={showUpdatedNotificationOnModal}
          cardList={giftCardList}
          onGetBalanceCard={onGetBalanceCard}
          checkbalanceValueInfo={checkbalanceValueInfo}
          showNotification={showNotification}
          showNotificationCaptcha={showNotificationCaptcha}
          setSelectedCard={setSelectedCard}
          isRecapchaEnabled={isRecapchaEnabled}
        />
      )}
    </div>
  );
};

GiftCardList.defaultProps = {
  deleteModalMountedState: false,
  commonLabels: {},
};

GiftCardList.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  commonLabels: PropTypes.shape({}),
  giftCardList: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  className: PropTypes.string.isRequired,
  setDeleteModalMountState: PropTypes.func.isRequired,
  deleteModalMountedState: PropTypes.bool,
  onDeleteCard: PropTypes.func.isRequired,
  showUpdatedNotificationOnModal: PropTypes.string.isRequired,
  onGetBalanceCard: PropTypes.func.isRequired,
  checkbalanceValueInfo: PropTypes.string.isRequired,
  showNotification: PropTypes.bool.isRequired,
  showNotificationCaptcha: PropTypes.bool.isRequired,
  setSelectedCard: PropTypes.string.isRequired,
  isRecapchaEnabled: PropTypes.bool.isRequired,
};

export default withStyles(GiftCardList, styles);
export { GiftCardList as GiftCardListVanilla };
