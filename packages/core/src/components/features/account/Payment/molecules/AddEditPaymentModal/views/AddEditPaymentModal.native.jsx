// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import ModalNative from '@tcp/core/src/components/common/molecules/Modal';
import AddEditCreditCard from '@tcp/core/src/components/features/account/AddEditCreditCard/container/AddEditCreditCard.container.native';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import { ModalViewWrapper } from '../AddEditPaymentModal.style.native';

export class AddEditPaymentModal extends React.PureComponent {
  static propTypes = {
    labels: PropTypes.shape({}),
    dto: PropTypes.shape({}),
    toggleModal: PropTypes.shape({}),
    updateCardList: PropTypes.func,
    setUpdateModalMountedState: PropTypes.func.isRequired,
    isEdit: PropTypes.bool.isRequired,
    selectedCard: PropTypes.shape({}).isRequired,
  };

  static defaultProps = {
    labels: { paymentGC: { lbl_payment_editCCHeading: '', lbl_payment_addCCHeading: '' } },
    dto: {},
    toggleModal: {},
    updateCardList: {},
  };

  render() {
    const {
      labels,
      toggleModal,
      setUpdateModalMountedState,
      dto,
      updateCardList,
      isEdit,
      selectedCard,
    } = this.props;
    return (
      <ModalNative
        isOpen={setUpdateModalMountedState}
        onRequestClose={toggleModal}
        heading={
          isEdit
            ? getLabelValue(labels, 'lbl_payment_editCCHeading', 'paymentGC')
            : getLabelValue(labels, 'lbl_payment_addCCHeading', 'paymentGC')
        }
        headingFontFamily="secondary"
      >
        <ModalViewWrapper>
          <AddEditCreditCard
            labels={labels}
            isEdit={isEdit}
            onClose={toggleModal}
            dto={dto}
            updateCardList={updateCardList}
            selectedCard={selectedCard}
          />
        </ModalViewWrapper>
      </ModalNative>
    );
  }
}

export default AddEditPaymentModal;
