// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable sonarjs/cognitive-complexity */
import { all, call, takeLatest, put, select, delay } from 'redux-saga/effects';
import logger from '@tcp/core/src/utils/loggerInstance';
import {
  getRegisteredUserInfoCachingInterval,
  getIsBagCarouselEnabled,
  getIsSNJEnabled,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import { submitUserSurvey } from '@tcp/core/src/services/abstractors/account/UpdateProfileInfo';
import { setLoaderState } from '@tcp/core/src/components/common/molecules/Loader/container/Loader.actions';
import { getBagCarouselModuleFromAsyncData } from '@tcp/core/src/components/common/molecules/BagCarouselModule/container/BagCarouselModule.actions';
import { updateProfileSuccess } from '@tcp/core/src/components/features/account/MyProfile/container/MyProfile.actions';
import { getFavoriteStoreActn } from '@tcp/core/src/components/features/storeLocator/StoreLanding/container/StoreLanding.actions';
import { setDefaultWishListIdActn } from '@tcp/core/src/components/features/browse/Favorites/container/Favorites.actions';
import { getOrderDetail } from '@tcp/core/src/components/features/account/LoginPage/container/LoginPage.actions';
import countryListAbstractor from '../../../../../services/abstractors/bootstrap/countryList';
import {
  setCountry,
  setCurrency,
  setLanguage,
  setCountryName,
} from '../../../../../reduxStore/actions';
import CONSTANTS from '../User.constants';
import { setUserInfo, setIsRegisteredUserCallDone, setIsFetching } from './User.actions';
import { getProfile } from '../../../../../services/abstractors/account';
import { validateReduxCache } from '../../../../../utils/cache.util';
import {
  getSiteId,
  isMobileApp,
  routerPush,
  getAPIConfig,
  isCookiePresent,
  readCookie,
  removeURLParameter,
} from '../../../../../utils';
import { setLocalStorage } from '../../../../../utils/localStorageManagement';
import { API_CONFIG } from '../../../../../services/config';
import { defaultCountries, defaultCurrencies } from '../../../../../constants/site.constants';
import { getNavigateXHR } from '../../../../../services/abstractors/account/NavigateXHR';
import { setNavigateCookieToken } from '../../NavigateXHR/container/NavigateXHR.action';

const getDataSetActions = (payload, response, userCachingTTL) => {
  const dataSetActions = [];
  const { country, currency } = response;

  response.userCachingTTL = userCachingTTL;

  if (country) {
    dataSetActions.push(put(setCountry(country)));
  }

  dataSetActions.push(
    put(setCurrency({ currency })),
    put(setUserInfo(response)),
    put(setIsRegisteredUserCallDone(true))
  );

  return dataSetActions;
};

export function* setCountryCurrency(response) {
  const { country, currency } = response;
  const [us, ca] = defaultCountries;
  const [currencyAttributesUS, currencyAttributesCA] = defaultCurrencies;
  let currencyAttributes = {};
  let countryAttributes = {};
  if (country === us.id) {
    currencyAttributes = currencyAttributesUS;
    countryAttributes = us;
  } else if (country === ca.id) {
    currencyAttributes = currencyAttributesCA;
    countryAttributes = ca;
  } else {
    const isShippingCookie = readCookie('iShippingCookie');
    if (isShippingCookie) {
      const [countryId, , currencyID, exchangevalue, merchantMargin] = (
        isShippingCookie || 'US||USD||'
      ).split('|');
      const countryObj = {
        id: countryId,
      };
      const currencyObj = {
        id: currencyID,
      };
      const exchangeRate = {
        exchangevalue,
        merchantMargin,
      };
      currencyAttributes = { ...currencyObj, ...exchangeRate };
      countryAttributes = countryObj;
    } else {
      const res = yield call(countryListAbstractor.getData, country);
      const countryList = res && res.data.countryList;
      const currentCountry = countryList.length && countryList[0];

      const { country: countryObj, currency: currencyObj, exchangeRate } = currentCountry;
      currencyAttributes = { ...currencyObj, ...exchangeRate };
      countryAttributes = countryObj;
    }
  }

  yield put(
    setCurrency({
      currency,
      currencyAttributes,
    })
  );
  yield put(setCountryName((countryAttributes && countryAttributes.displayName) || ''));
}

export const checkForNonGenericUser = () => {
  return new Promise(async (resolve) => {
    if (isMobileApp()) {
      resolve(
        !!(await isCookiePresent('WC_PERSISTENT')) || !!(await isCookiePresent('WC_USERACTIVITY_'))
      );
    } else {
      resolve(isCookiePresent('WC_PERSISTENT') || isCookiePresent('WC_USERACTIVITY_'));
    }
  });
};

const moveToHomePage = (response) => {
  const siteId = getSiteId();
  const { CA_CONFIG_OPTIONS: apiConfig, siteIds } = API_CONFIG;
  const { country } = response;
  if (!isMobileApp() && country === siteIds.ca.toUpperCase() && siteId !== apiConfig.siteId) {
    routerPush(window.location, '/home', null, siteId);
  }
};
const getSourceOfItem = () => {
  let isSourceSF = false;
  const cartItems = readCookie('cartItems');
  if (cartItems.indexOf('SF|') > -1) {
    isSourceSF = true;
  }
  return isSourceSF;
};
function* getCrossDomainCookie({ ct, router }) {
  if (ct) {
    const { asPath, pathname, query } = router || {};
    const { brandId } = getAPIConfig();
    const isSNJEnabled = yield select(getIsSNJEnabled);
    yield call(getNavigateXHR, { ct, isSNJEnabled });
    setLocalStorage({ key: `${brandId && brandId.toLowerCase()}_ct`, value: ct });
    yield put(
      setNavigateCookieToken({
        encodedcookie: ct,
      })
    );
    const isSourceSF = getSourceOfItem();
    if (isSNJEnabled && isSourceSF) {
      yield put(getOrderDetail());
    }
    const updatedAspath = removeURLParameter(asPath, 'ct');
    router.push({ pathname, query }, `${updatedAspath}`);
  }
}

// eslint-disable-next-line complexity
export function* getUserInfoSaga({ payload = {} } = {}) {
  try {
    const {
      loadFavStore = false,
      ct = '',
      router,
      source = 'login',
      removePersistentCookie,
      reloadUserInfo = false,
    } = payload;
    if (!reloadUserInfo) {
      yield put(setIsFetching(true));
      yield put(setIsRegisteredUserCallDone(false));
    }
    const userCachingTTL = yield select(getRegisteredUserInfoCachingInterval);
    yield call(getCrossDomainCookie, { ct, router });
    if (ct) yield delay(100);
    const isNonGenericUser = yield call(checkForNonGenericUser);
    if (isNonGenericUser) {
      const response = yield call(getProfile, {
        pageId: 'myAccount',
        source,
      });
      const { language, defaultWishListId, isLoggedin } = response;
      if (isMobileApp() && isLoggedin && removePersistentCookie) {
        removePersistentCookie();
      }

      if (defaultWishListId) {
        yield put(setDefaultWishListIdActn(response.defaultWishListId));
      }
      const dataSetActions = getDataSetActions(payload, response, userCachingTTL);

      yield all(dataSetActions);

      /**
       * Below code is to get currency attributes based on
       * current country returned from getRegisteredUserInfo API
       */
      if (!isMobileApp()) {
        yield call(setCountryCurrency, response);
      }

      if (language) {
        yield put(setLanguage(language));
      }

      if (!reloadUserInfo) {
        moveToHomePage(response);
      }

      if (loadFavStore) {
        yield put(
          getFavoriteStoreActn({
            ignoreCache: true,
          })
        );
      }
    } else {
      const anonymousUser = {
        userId: -1002,
        isGuest: true,
        isRemembered: false,
        isExpressEligible: false,
        userProfileState: {},
      };
      const [country, , currency] = !isMobileApp()
        ? (readCookie('iShippingCookie') || 'US||USD').split('|')
        : ['US', null, 'USD'];
      yield put(setCountry(country));
      yield put(setCurrency({ currency }));
      yield put(setUserInfo(anonymousUser));
      if (!isMobileApp()) {
        yield call(setCountryCurrency, { country, currency });
      }
      yield put(setIsRegisteredUserCallDone(true));
    }

    if (isMobileApp()) {
      const isBagCarouselEnabled = yield select(getIsBagCarouselEnabled);
      if (isBagCarouselEnabled) {
        yield put(getBagCarouselModuleFromAsyncData());
      }
    }
  } catch (err) {
    if (isMobileApp()) {
      const isBagCarouselEnabled = yield select(getIsBagCarouselEnabled);
      if (isBagCarouselEnabled) {
        yield put(getBagCarouselModuleFromAsyncData());
      }
    }
    yield put(setIsRegisteredUserCallDone(true));
    yield put(setIsFetching(false));
    logger.error('Error: error in fetching user profile information');
  }
}

function* setSurveyAnswersSaga(data) {
  yield put(setLoaderState(true));
  try {
    yield call(submitUserSurvey, data);
    yield call(getUserInfoSaga);
    yield put(updateProfileSuccess('successMessage'));
    yield put(setLoaderState(false));
  } catch (err) {
    yield put(setLoaderState(false));
    yield null;
  }
}

export function* UserSaga() {
  const cachedUserInfo = validateReduxCache(getUserInfoSaga);
  yield takeLatest(CONSTANTS.GET_USER_INFO, cachedUserInfo);
  yield takeLatest(CONSTANTS.SET_SURVEY_ANSWERS, setSurveyAnswersSaga);
}

export default UserSaga;
