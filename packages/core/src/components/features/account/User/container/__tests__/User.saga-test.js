// 9fbef606107a605d69c0edbcd8029e5d 
import { all, put } from 'redux-saga/effects';
import { isMobileApp, isCookiePresent } from '@tcp/core/src/utils';
import { getUserInfoSaga, checkForNonGenericUser } from '../User.saga';
import { setUserInfo, setIsRegisteredUserCallDone } from '../User.actions';
import { setCurrency } from '../../../../../../reduxStore/actions';

jest.mock('@tcp/core/src/utils', () => {
  const originalModule = jest.requireActual('@tcp/core/src/utils');
  return {
    ...originalModule,
    isMobileApp: jest.fn(),
    isCookiePresent: jest.fn(),
    readCookie: jest.fn().mockImplementation(() => 'US||USD'),
  };
});

describe('User saga', () => {
  describe('getUserInfoSaga', () => {
    let gen;
    beforeEach(() => {
      gen = getUserInfoSaga({});
      gen.next();
      gen.next();
      gen.next();
    });

    it('should dispatch setUserInfo action for success resposnse', () => {
      const currency = 'USD';
      const response = {
        firstName: 'test',
        currency,
      };
      gen.next(true);
      gen.next(true);
      gen.next(true);
      const putDescriptor = gen.next(response).value;
      expect(putDescriptor).toEqual(
        all([
          put(setCurrency({ currency })),
          put(setUserInfo(response)),
          put(setIsRegisteredUserCallDone(true)),
        ])
      );
    });

    it('should set Generic User Data', () => {
      const anonymousUser = {
        userId: -1002,
        isGuest: true,
        isRemembered: false,
        isExpressEligible: false,
        userProfileState: {},
      };
      gen.next(false);
      gen.next(false);
      gen.next();
      gen.next();
      const putDescriptor = put(setUserInfo(anonymousUser));
      expect(gen.next().value).toEqual(putDescriptor);
    });
  });

  describe('checkForNonGenericUser', () => {
    it('should returns a generic user as false', () => {
      isMobileApp.mockImplementation(() => false);
      isCookiePresent.mockImplementation(() => false);
      expect(checkForNonGenericUser()).resolves.toBeFalsy();
    });

    it('should returns a non generic user as true', () => {
      isMobileApp.mockImplementation(() => false);
      isCookiePresent.mockImplementation(() => true);
      expect(checkForNonGenericUser()).resolves.toBeTruthy();
    });

    it('should returns a generic user as false for Mobile app', () => {
      isMobileApp.mockImplementation(() => true);
      isCookiePresent.mockImplementation(() => false);
      expect(checkForNonGenericUser()).resolves.toBeFalsy();
    });

    it('should returns a generic user as true for Mobile app', () => {
      isMobileApp.mockImplementation(() => true);
      isCookiePresent.mockImplementation(() => true);
      expect(checkForNonGenericUser()).resolves.toBeFalsy();
    });
  });
});
