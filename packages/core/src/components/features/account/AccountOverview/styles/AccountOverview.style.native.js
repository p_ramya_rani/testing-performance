// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

const LegalStyleLinkStyles = {
  height: 100,
  marginTop: 20,
};

const CopyrightText = styled.Text`
  margin-top: ${props => props.theme.spacing.LAYOUT_SPACING.MED};
  color: ${props => props.theme.colorPalette.gray[600]};
  font-size: ${props => props.theme.fonts.fontSize.anchor.medium}px;
`;

const ReferenceText = styled.Text`
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.MED};
  color: ${props => props.theme.colorPalette.gray[600]};
  font-size: ${props => props.theme.fonts.fontSize.anchor.medium}px;
`;

const UnderlineStyle = styled.View`
  height: 1px;
  background-color: ${props => props.theme.colorPalette.gray[500]};
  margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.MED};
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.MED};
`;

const ChatContainer = styled.View`
  margin: ${props => (props.isUserLoggedIn ? '33px 0 10px 0' : '10px 0')};
`;

export { LegalStyleLinkStyles, CopyrightText, UnderlineStyle, ReferenceText, ChatContainer };
