// 9fbef606107a605d69c0edbcd8029e5d
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getCardList } from '@tcp/core/src/components/features/account/Payment/container/Payment.actions';
import { toggleApplyNowModal } from '@tcp/core/src/components/common/molecules/ApplyNowPLCCModal/container/ApplyNowModal.actions';
import accountConstants from '@tcp/core/src/components/features/account/Account/Account.constants';
import AccountOverviewComponent from '../views/AccountOverview.view';
import getLinks, { getIsChatBotDisabled } from './AccountOverview.selectors';
import { setTrackOrderModalMountedState } from '../../TrackOrder/container/TrackOrder.actions';
import { getCouponList } from '../../../CnC/common/organism/CouponAndPromos/container/Coupon.actions';
import { setClickAnalyticsData, trackClick } from '../../../../../analytics/actions';
import { getPageNameFromState } from '../../../../../reduxStore/selectors/session.selectors';

const getAccountOverviewLabels = labels => {
  return (labels && labels.accountOverview) || {};
};

const getAccountCommonLabels = labels => {
  return (labels && labels.common) || {};
};

const getFooterLabels = labels => {
  return labels && labels.footerDefault;
};

const AccountOverviewContainer = ({
  labels,
  openTrackOrder,
  openApplyNowModal,
  fetchCoupons,
  getCardListAction,
  isUserLoggedIn,
  globalLabels,
  ...otherProps
}) => {
  const overviewLabels = getAccountOverviewLabels(labels);
  const commonLabels = getAccountCommonLabels(labels);
  const footerLabels = getFooterLabels(globalLabels);

  useEffect(() => {
    if (isUserLoggedIn) {
      fetchCoupons();
      getCardListAction();
    }
  }, [isUserLoggedIn]);

  return (
    <AccountOverviewComponent
      labels={overviewLabels}
      commonLabels={commonLabels}
      openTrackOrder={openTrackOrder}
      openApplyNowModal={openApplyNowModal}
      isUserLoggedIn={isUserLoggedIn}
      footerLabels={footerLabels}
      {...otherProps}
    />
  );
};

AccountOverviewContainer.propTypes = {
  labels: PropTypes.shape({
    accountOverview: PropTypes.shape({}),
  }),
  openTrackOrder: PropTypes.func.isRequired,
  openApplyNowModal: PropTypes.func.isRequired,
  fetchCoupons: PropTypes.func.isRequired,
  getCardListAction: PropTypes.func.isRequired,
  isUserLoggedIn: PropTypes.bool,
  globalLabels: PropTypes.shape({}),
  loyaltyPageName: PropTypes.string,
};

AccountOverviewContainer.defaultProps = {
  labels: {
    accountOverview: {},
  },
  globalLabels: {},
  isUserLoggedIn: false,
  loyaltyPageName: 'account',
};

export const mapStateToProps = state => {
  return {
    accountFooterLinks: getLinks(state, accountConstants.FOOTER_LINKS),
    legalLinks: getLinks(state, accountConstants.LEGAL_LINKS),
    disableChatBotABTest: getIsChatBotDisabled(state),
    loyaltyPageName: getPageNameFromState(state),
  };
};

export const mapDispatchToProps = dispatch => {
  return {
    openTrackOrder: payload => {
      dispatch(setTrackOrderModalMountedState(payload));
    },
    openApplyNowModal: payload => {
      dispatch(toggleApplyNowModal(payload));
    },
    fetchCoupons: () => {
      dispatch(getCouponList());
    },
    getCardListAction: () => {
      dispatch(getCardList());
    },
    trackClickAction: data => {
      dispatch(trackClick(data));
    },
    setClickAnalyticsDataAction: data => {
      dispatch(setClickAnalyticsData(data));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AccountOverviewContainer);
export { AccountOverviewContainer as AccountOverviewContainerVanilla };
