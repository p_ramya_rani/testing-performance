// 9fbef606107a605d69c0edbcd8029e5d 
import { getABtestFromState } from '@tcp/core/src/utils';

const getLinks = (state, footerLbl) => state.SubNavigation[footerLbl];

export const getIsChatBotDisabled = state => {
  return getABtestFromState(state.AbTest).isChatBotDisabled;
};

export default getLinks;
