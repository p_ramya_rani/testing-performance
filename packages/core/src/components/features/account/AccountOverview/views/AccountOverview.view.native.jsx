// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import { View, Platform } from 'react-native';
import { encrypt } from 'react-native-simple-encryption';
import PropTypes from 'prop-types';
import MyPlaceRewardsOverviewTile from '@tcp/core/src/components/features/account/common/organism/MyPlaceRewardsOverviewTile';
import MyWalletTile from '@tcp/core/src/components/features/account/common/organism/MyWalletTile';
import EarnExtraPointsOverview from '@tcp/core/src/components/features/account/common/organism/EarnExtraPointsOverview';
import { getLabelValue } from '@tcp/core/src/utils';
import { getReferenceId } from '@tcp/core/src/utils/utils.app';
import AsyncStorage from '@react-native-community/async-storage';
import CookieManager from 'react-native-cookies';

import Panel from '../../../../common/molecules/Panel';
import PaymentTile from '../../common/organism/PaymentTile';
import LegalLinks from '../../../../common/molecules/LegalLinks';
import MyPlaceRewardsCreditCard from '../../common/organism/MyPlaceRewardsCreditCard';
import AddressOverviewTile from '../../common/organism/AddressOverviewTile';
import OrdersTile from '../../common/organism/OrdersTile';
import MyPreferencesTile from '../../common/organism/MyPreferencesTile';
import {
  LegalStyleLinkStyles,
  CopyrightText,
  UnderlineStyle,
  ReferenceText,
  ChatContainer,
} from '../styles/AccountOverview.style.native';
import ProfileInfoContainer from '../../common/organism/ProfileInfoTile';
import OrderNotification from '../../OrderNotification';
import FooterLinks from '../../common/molecule/FooterLinks';
import GuestLoginOverview from '../../common/molecule/GuestLoginModule';
import { ViewWithSpacing } from '../../../../common/atoms/styledWrapper';
import HelpChat from '../../../../common/atoms/HelpChatButton/views/HelpChat';

class AccountOverview extends PureComponent {
  componentDidUpdate(prevPops) {
    const {
      navigation: { prevNavigation },
    } = prevPops;
    const { isUserLoggedIn, navigation, handleComponentChange } = this.props;
    const customNavigateToPagePrev =
      prevNavigation && prevNavigation.getParam('customNavigateToPage');
    const customNavigateToPage = navigation && navigation.getParam('customNavigateToPage');
    if (prevPops.isUserLoggedIn !== isUserLoggedIn && isUserLoggedIn && Platform.OS === 'ios')
      // save cookies in the async storage for ios
      CookieManager.getAll().then(res => {
        Object.keys(res).forEach(key => {
          if (key.startsWith('WC_') && key !== 'WC_PERSISTENT') {
            AsyncStorage.setItem(key, encrypt(key, res[key].value));
          }
        });
      });

    if (
      customNavigateToPagePrev !== customNavigateToPage &&
      customNavigateToPage !== null &&
      isUserLoggedIn
    ) {
      handleComponentChange(customNavigateToPage);
      navigation.setParams({ customNavigateToPage: null });
    }
  }

  render() {
    const {
      isUserLoggedIn,
      labels,
      commonLabels,
      handleComponentChange,
      navigation,
      openApplyNowModal,
      accountFooterLinks,
      legalLinks,
      footerLabels,
      disableChatBotABTest,
      trackClickAction,
      setClickAnalyticsDataAction,
      loyaltyPageName,
    } = this.props;
    const commonVariables = {
      pageName: loyaltyPageName,
    };

    const loginAnalyticsData = {
      ...commonVariables,
      customEvents: ['event126'],
      loyaltyLocation: loyaltyPageName,
    };

    const joinAnalyticsData = {
      ...commonVariables,
      customEvents: ['event125'],
      loyaltyLocation: loyaltyPageName,
    };

    const applyNowAnalyticsData = {
      ...commonVariables,
      customEvents: ['event129'],
      loyaltyLocation: loyaltyPageName,
    };

    return (
      <>
        {isUserLoggedIn && (
          <React.Fragment>
            <ViewWithSpacing spacingStyles="margin-top-MED">
              <OrderNotification orderNotificationExist />
            </ViewWithSpacing>
            <Panel title={getLabelValue(labels, 'lbl_overview_profileInformationHeading')}>
              <ProfileInfoContainer labels={labels} handleComponentChange={handleComponentChange} />
            </Panel>
            <Panel title={getLabelValue(labels, 'lbl_overview_myPlaceRewardsHeading')}>
              <MyPlaceRewardsOverviewTile
                labels={labels}
                commonLabels={commonLabels}
                handleComponentChange={handleComponentChange}
              />
            </Panel>
            <Panel title={getLabelValue(labels, 'lbl_overview_earnPointsHeading')}>
              <EarnExtraPointsOverview handleComponentChange={handleComponentChange} />
            </Panel>
            <Panel title={getLabelValue(labels, 'lbl_overview_myWalletHeading')}>
              <MyWalletTile
                labels={labels}
                commonLabels={commonLabels}
                handleComponentChange={handleComponentChange}
              />
            </Panel>
            <Panel title={getLabelValue(labels, 'lbl_overview_ordersHeading')}>
              <OrdersTile
                labels={labels}
                navigation={navigation}
                handleComponentChange={handleComponentChange}
              />
            </Panel>
            <Panel title={getLabelValue(labels, 'lbl_overview_myPlaceRewardsCardHeading')}>
              <MyPlaceRewardsCreditCard
                labels={labels}
                handleComponentChange={handleComponentChange}
              />
            </Panel>
            <Panel title={getLabelValue(labels, 'lbl_overview_myPreferencesHeading')}>
              <MyPreferencesTile labels={labels} handleComponentChange={handleComponentChange} />
            </Panel>
            <Panel title={getLabelValue(labels, 'lbl_overview_addressBookHeading')}>
              <AddressOverviewTile labels={labels} handleComponentChange={handleComponentChange} />
            </Panel>

            <Panel title={getLabelValue(labels, 'lbl_overview_paymentHeading')}>
              <PaymentTile labels={labels} handleComponentChange={handleComponentChange} />
            </Panel>
          </React.Fragment>
        )}
        <GuestLoginOverview
          isUserLoggedIn={isUserLoggedIn}
          labels={labels}
          navigation={navigation}
          setClickAnalyticsDataAction={setClickAnalyticsDataAction}
          trackClickAction={trackClickAction}
          loginAnalyticsData={loginAnalyticsData}
          joinAnalyticsData={joinAnalyticsData}
          loyaltyPageName={loyaltyPageName}
        />
        {accountFooterLinks && accountFooterLinks.length > 0 ? (
          <FooterLinks
            isUserLoggedIn={isUserLoggedIn}
            labels={labels}
            navigation={navigation}
            handleComponentChange={handleComponentChange}
            openApplyNowModal={openApplyNowModal}
            footerLinks={accountFooterLinks}
            showDivider
            applyNowAnalyticsData={applyNowAnalyticsData}
          />
        ) : null}
        {disableChatBotABTest ? null : (
          <ChatContainer isUserLoggedIn={isUserLoggedIn}>
            <HelpChat
              labels={{ chatNow: getLabelValue(labels, 'lbl_overview_chat_now') }}
              isUserLoggedIn={isUserLoggedIn}
              navigation={navigation}
            />
          </ChatContainer>
        )}
        <UnderlineStyle />
        <View style={LegalStyleLinkStyles}>
          <LegalLinks links={legalLinks} navigation={navigation} />
        </View>
        <CopyrightText>{getLabelValue(labels, 'lbl_overview_copyrightTxt')}</CopyrightText>
        <ReferenceText>
          {`${getLabelValue(footerLabels, 'lbl_footerDefault_referenceId')} ${getReferenceId()}`}
        </ReferenceText>
      </>
    );
  }
}

AccountOverview.propTypes = {
  labels: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
  handleComponentChange: PropTypes.func.isRequired,
  isUserLoggedIn: PropTypes.bool.isRequired,
  commonLabels: PropTypes.shape({}),
  navigation: PropTypes.shape({}).isRequired,
  openApplyNowModal: PropTypes.func.isRequired,
  accountFooterLinks: PropTypes.shape([]).isRequired,
  legalLinks: PropTypes.shape([]).isRequired,
  footerLabels: PropTypes.shape({}).isRequired,
  disableChatBotABTest: PropTypes.bool,
  trackClickAction: PropTypes.func,
  setClickAnalyticsDataAction: PropTypes.func,
  loyaltyPageName: PropTypes.string,
};

AccountOverview.defaultProps = {
  labels: {
    lbl_overview_help: '',
    lbl_overview_app_settings: '',
    lbl_overview_refer_friend: '',
    lbl_overview_purchase_giftCards: '',
    lbl_overview_manage_creditCard: '',
    lbl_overview_apply_today: '',
    lbl_overview_myFavoritesHeading: '',
    lbl_overview_createAccount: '',
    lbl_overview_login_text: '',
    lbl_overview_join_text: '',
    lbl_overview_logout_heading_Text_2: '',
    lbl_overview_logout_heading_Text_1: '',
    lbl_overview_myPlaceRewardsCardHeading: '',
    lbl_overview_myPreferencesHeading: '',
    lbl_overview_paymentHeading: '',
    lbl_overview_addressBookHeading: '',
    lbl_overview_profileInformationHeading: '',
    lbl_overview_ordersHeading: '',
    lbl_overview_earnPointsHeading: '',
    lbl_overview_myWalletHeading: '',
    lbl_overview_myPlaceRewardsHeading: '',
  },
  commonLabels: {},
  disableChatBotABTest: false,
  trackClickAction: () => {},
  setClickAnalyticsDataAction: () => {},
  loyaltyPageName: 'account',
};

export default AccountOverview;
