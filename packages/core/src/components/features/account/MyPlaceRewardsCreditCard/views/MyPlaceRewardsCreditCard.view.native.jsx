// 9fbef606107a605d69c0edbcd8029e5d 
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import {
  UrlHandler,
  redirectToInAppView,
  validateExternalUrl,
} from '@tcp/core/src/utils/utils.app';
import {
  ViewWithSpacing,
  BodyCopyWithSpacing,
} from '@tcp/core/src/components/common/atoms/styledWrapper';
import names from '../../../../../constants/eventsName.constants';
import { Button, RichText } from '../../../../common/atoms';
import {
  ImageContainer,
  ButtonWrapper,
  StyledAnchor,
  BottomContainer,
  StyledImage,
  HeaderContainer,
  RichTextSpacing,
} from '../styles/MyPlaceRewardsCreditCard.style.native';
import ClickTracker from '../../../../../../../mobileapp/src/components/common/atoms/ClickTracker';
import EspotContainer from '../../../../common/molecules/Espot';

const headerImage = require('../../../../../../../mobileapp/src/assets/images/tcp-plcc.png');

const AnalyticsData = {
  pageName: 'myplace:rewardscreditcard',
  pageType: 'myplace',
  pageSection: 'myplace',
  pageSubSection: 'myplace',
  customEvents: 'event113',
};

export class MyPlaceRewardsCreditCard extends PureComponent {
  openManageCreditCardLink = () => {
    const { labels, navigation } = this.props;
    const url = getLabelValue(labels, 'lbl_PLCCModal_applyAcceptOfferLink');
    if (url && navigation) {
      redirectToInAppView(url, navigation);
    } else if (validateExternalUrl(url)) {
      UrlHandler(url);
    }
  };

  render() {
    const { labels, navigation, openApplyNowModal, mprccBenefits, storeId } = this.props;
    return (
      <>
        <HeaderContainer>
          <BodyCopyWithSpacing
            color="gray.900"
            fontFamily="primary"
            fontSize="fs38"
            textAlign="center"
            fontWeight="black"
            text={getLabelValue(labels, 'lbl_PLCCModal_applyNowHeaderText')}
            spacingStyles="padding-top-LRG"
          />
          <BodyCopyWithSpacing
            color="gray.900"
            fontFamily="primary"
            fontSize="fs12"
            fontWeight="black"
            text={getLabelValue(labels, 'lbl_PLCCModal_applyNowHeaderTextSuffix')}
            spacingStyles="padding-top-XXXS"
          />
        </HeaderContainer>

        <ImageContainer>
          <StyledImage source={headerImage} width="70%" height="166px" />
        </ImageContainer>
        <RichTextSpacing>
          <RichText source={{ html: getLabelValue(labels, 'lbl_PLCCModal_applyNowSubText') }} />
        </RichTextSpacing>
        <ViewWithSpacing spacingStyles="margin-top-SM margin-bottom-LRG">
          <ButtonWrapper>
            <ClickTracker
              as={Button}
              clickData={{ customEvents: ['event113'] }}
              name={names.screenNames.manage_plcc}
              module="account"
              fill="BLUE"
              type="submit"
              color="white"
              text={getLabelValue(labels, 'lbl_PLCCModal_applyNowCTA')}
              width="100%"
              onPress={() => {
                navigation.navigate('ApplyNow');
                openApplyNowModal({ isModalOpen: false, isPLCCModalOpen: true });
              }}
            />
          </ButtonWrapper>
          <ButtonWrapper>
            <ClickTracker
              as={Button}
              clickData={{ ...AnalyticsData, storeId }}
              name={names.screenNames.manage_plcc}
              module="account"
              fill="WHITE"
              type="submit"
              text={getLabelValue(labels, 'lbl_PLCCForm_manageCreditCardAccount')}
              width="100%"
              onPress={this.openManageCreditCardLink}
            />
          </ButtonWrapper>
        </ViewWithSpacing>

        <EspotContainer richTextHtml={mprccBenefits} isNativeView={false} />

        <BottomContainer>
          <BodyCopyWithSpacing
            fontSize="fs10"
            fontFamily="secondary"
            text={getLabelValue(labels, 'lbl_PLCCModal_linksTextPrefix')}
            spacingStyles="margin-right-XS"
          />

          <StyledAnchor
            url={getLabelValue(labels, 'lbl_PLCCModal_detailsLink')}
            navigation={navigation}
            target="_blank"
            fontSizeVariation="medium"
            anchorVariation="primary"
            underline
            text={getLabelValue(labels, 'lbl_PLCCForm_details')}
          />

          <StyledAnchor
            url={getLabelValue(labels, 'lbl_PLCCModal_faqLink')}
            target="_blank"
            navigation={navigation}
            locator="plcc_faq"
            fontSizeVariation="medium"
            anchorVariation="primary"
            underline
            text={getLabelValue(labels, 'lbl_PLCCModal_faqText')}
          />

          <StyledAnchor
            url={getLabelValue(labels, 'lbl_PLCCModal_rewardsProgramLink')}
            target="_blank"
            navigation={navigation}
            data-locator="plcc_rewards_terms"
            fontSizeVariation="medium"
            anchorVariation="primary"
            underline
            text={getLabelValue(labels, 'lbl_PLCCModal_rewardsProgramText')}
          />
        </BottomContainer>
      </>
    );
  }
}

MyPlaceRewardsCreditCard.propTypes = {
  labels: PropTypes.shape({}),
  openApplyNowModal: PropTypes.func.isRequired,
  navigation: PropTypes.func.isRequired,
  mprccBenefits: PropTypes.string.isRequired,
  storeId: PropTypes.string.isRequired,
};

MyPlaceRewardsCreditCard.defaultProps = {
  labels: {},
};

export default MyPlaceRewardsCreditCard;
