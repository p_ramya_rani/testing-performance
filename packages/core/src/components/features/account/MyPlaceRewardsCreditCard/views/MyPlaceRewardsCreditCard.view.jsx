// 9fbef606107a605d69c0edbcd8029e5d 
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { getLabelValue, getLocator } from '@tcp/core/src/utils/utils';
import ACCOUNT_CONSTANTS from '@tcp/core/src/components/features/account/Account/Account.constants';
import ClickTracker from '@tcp/web/src/components/common/atoms/ClickTracker';
import FormPageHeading from '../../common/molecule/FormPageHeading';
import { RichText, BodyCopy, Button, Anchor, Row, Col } from '../../../../common/atoms';
import styles from '../styles/MyPlaceRewardsCreditCard.style';
import withStyles from '../../../../common/hoc/withStyles';
import ApplyNowPLCCModal from '../../../../common/molecules/ApplyNowPLCCModal';
import { openWindow } from '../../../../../utils/utils.web';
import EspotContainer from '../../../../common/molecules/Espot';

export class MyPlaceRewardsCreditCard extends PureComponent {
  openManageCreditCardLink = e => {
    e.preventDefault();
    const { labels, setAnalyticsVars } = this.props;
    setAnalyticsVars();
    openWindow(getLabelValue(labels, 'lbl_PLCCModal_applyAcceptOfferLink'), '_blank', 'noopener');
  };

  render() {
    const { labels, className, isPLCCModalOpen, openPLCCModal, mprccBenefits } = this.props;
    return (
      <div className={className}>
        <FormPageHeading
          heading={getLabelValue(labels, 'lbl_PLCCForm_rewardsCardHeading')}
          data-locator="mprcc-header"
        />
        <Row fullBleed>
          <Col
            colSize={{
              small: 6,
              medium: 8,
              large: 8,
            }}
            offsetLeft={{
              large: 2,
            }}
          >
            <div>
              <BodyCopy
                fontFamily="primary"
                fontSize="fs36"
                fontWeight="black"
                textAlign="center"
                className="Benefit_Heading"
              >
                {getLabelValue(labels, 'lbl_PLCCModal_applyNowHeaderText')}
                <BodyCopy
                  fontFamily="primary"
                  fontSize="fs12"
                  fontWeight="black"
                  textAlign="center"
                  component="sup"
                  className="Benefit_Heading_Suffix"
                >
                  {getLabelValue(labels, 'lbl_PLCCModal_applyNowHeaderTextSuffix')}
                </BodyCopy>
              </BodyCopy>

              <div className="header-image" data-locator="mprcc-tcpgymimage" />

              <RichText
                className="apply-now-subtext"
                data-locator={getLocator('ship_to_text_2')}
                richTextHtml={getLabelValue(labels, 'lbl_PLCCModal_applyNowSubText')}
              />

              <BodyCopy component="div" className="button_wrapper elem-mt-XL elem-mb-XL">
                <ClickTracker
                  clickData={{
                    eventName: ACCOUNT_CONSTANTS.ACCOUNT_ANALYTICS.navigationName.plcc_apply,
                    pageType: 'myplace',
                    pageSection: 'myplace',
                    pageSubSection: 'myplace',
                    pageName: 'myplace:rewardscreditcard',
                    customEvents: ['event112'],
                    pageNavigationText:
                      ACCOUNT_CONSTANTS.ACCOUNT_ANALYTICS.navigationText.plcc_apply,
                  }}
                >
                  <Button
                    buttonVariation="fixed-width"
                    fill="BLUE"
                    type="submit"
                    fontSize="fs14"
                    onClick={openPLCCModal}
                    fontWeight="semibold"
                    data-locator="mprcc-applyCTA"
                  >
                    {getLabelValue(labels, 'lbl_PLCCModal_applyNowCTA')}
                  </Button>
                </ClickTracker>

                <ClickTracker
                  clickData={{
                    eventName: ACCOUNT_CONSTANTS.ACCOUNT_ANALYTICS.navigationName.plcc_manage,
                    pageName: 'myplace:rewardscreditcard',
                    pageType: 'myplace',
                    pageSection: 'myplace',
                    pageSubSection: 'myplace',
                    customEvents: ['event113'],
                    pageNavigationText:
                      ACCOUNT_CONSTANTS.ACCOUNT_ANALYTICS.navigationText.plcc_manage,
                  }}
                >
                  <Button
                    buttonVariation="fixed-width"
                    fill="WHITE"
                    type="submit"
                    fontSize="fs14"
                    className="elem-mt-MED"
                    onClick={this.openManageCreditCardLink}
                    fontWeight="semibold"
                    data-locator="mprcc-managecreditcardCTA"
                  >
                    {getLabelValue(labels, 'lbl_PLCCForm_manageCreditCardAccount')}
                  </Button>
                </ClickTracker>
              </BodyCopy>
            </div>

            {/* <div className="table-image" /> */}
            <EspotContainer richTextHtml={mprccBenefits} isNativeView={false} />

            <div className="footerLinks">
              <BodyCopy component="span" fontSize="fs12" fontFamily="secondary">
                {getLabelValue(labels, 'lbl_PLCCModal_linksTextPrefix')}
              </BodyCopy>
              <Anchor
                className="elem-ml-SM"
                url={getLabelValue(labels, 'lbl_PLCCModal_detailsLink')}
                target="_blank"
                fontSizeVariation="large"
                anchorVariation="primary"
                underline
                data-locator="mprcc-detailslnk"
              >
                {getLabelValue(labels, 'lbl_PLCCForm_details')}
              </Anchor>
              <Anchor
                className="elem-ml-XL"
                url={getLabelValue(labels, 'lbl_PLCCModal_faqLink')}
                target="_blank"
                data-locator="mprcc-FAQlnk"
                fontSizeVariation="large"
                anchorVariation="primary"
                underline
              >
                {getLabelValue(labels, 'lbl_PLCCModal_faqText')}
              </Anchor>
              <Anchor
                className="elem-ml-XL"
                url={getLabelValue(labels, 'lbl_PLCCModal_rewardsProgramLink')}
                target="_blank"
                data-locator="mprcc-rewardteemlnk"
                fontSizeVariation="large"
                anchorVariation="primary"
                underline
              >
                {getLabelValue(labels, 'lbl_PLCCModal_rewardsProgramText')}
              </Anchor>
            </div>
            {isPLCCModalOpen && <ApplyNowPLCCModal isPLCCModalOpen={isPLCCModalOpen} />}
          </Col>
        </Row>
      </div>
    );
  }
}

MyPlaceRewardsCreditCard.propTypes = {
  labels: PropTypes.shape({}),
  className: PropTypes.string.isRequired,
  isPLCCModalOpen: PropTypes.bool,
  openPLCCModal: PropTypes.func.isRequired,
  mprccBenefits: PropTypes.string.isRequired,
  setAnalyticsVars: PropTypes.func.isRequired,
};

MyPlaceRewardsCreditCard.defaultProps = {
  labels: {},
  isPLCCModalOpen: false,
};

export default withStyles(MyPlaceRewardsCreditCard, styles);
