// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { MyPlaceRewardsCreditCard } from '../MyPlaceRewardsCreditCard.view.native';

describe('MyPlaceRewardsCreditCard View component', () => {
  it('should renders correctly', () => {
    const props = {
      labels: {},
    };
    const component = shallow(<MyPlaceRewardsCreditCard {...props} />);
    expect(component).toMatchSnapshot();
  });
});
