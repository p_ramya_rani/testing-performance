// 9fbef606107a605d69c0edbcd8029e5d 
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { handleClickData } from '@tcp/core/src/components/features/CnC/LoyaltyBanner/molecules/LoyaltyFooterSection/views/LoyaltyFooterSection';
import { getUserType } from '@tcp/core/src/components/features/browse/ApplyCardPage/utils/utility';
import { setClickAnalyticsData } from '@tcp/core/src/analytics/actions';
import { resetPLCCResponse } from '@tcp/core/src/components/features/browse/ApplyCardPage/container/ApplyCard.actions';
import { getMPRCCBenefits } from '@tcp/core/src/components/features/account/Account/container/Account.selectors';
import MyPlaceRewardsCreditCard from '../views';
import { getLabels } from './MyPlaceRewardsCreditCard.selectors';
import { getIsPLCCModalOpen } from '../../../../common/molecules/ApplyNowPLCCModal/container/ApplyNowModal.selectors';
import { toggleApplyNowModal } from '../../../../common/molecules/ApplyNowPLCCModal/container/ApplyNowModal.actions';
import { isGuest } from '../../../CnC/Checkout/container/Checkout.selector';
import { isPlccUser, getDefaultStore } from '../../User/container/User.selectors';

export class MyPlaceRewardsCreditCardContainer extends PureComponent {
  openPLCCModal = e => {
    e.preventDefault();
    const { toggleModal } = this.props;
    this.setAnalyticsVars();
    toggleModal({ isModalOpen: false, isPLCCModalOpen: true });
  };

  setAnalyticsVars = () => {
    const { isGuestUser, isPlcc } = this.props;
    const userType = getUserType(isGuestUser, isPlcc);
    const data = handleClickData(userType, 'account');
    setClickAnalyticsData({ ...data });
  };

  render() {
    const {
      labels,
      isPLCCModalOpen,
      openApplyNowModal,
      navigation,
      mprccBenefits,
      storeId,
    } = this.props;
    return (
      <MyPlaceRewardsCreditCard
        labels={labels}
        isPLCCModalOpen={isPLCCModalOpen}
        openPLCCModal={this.openPLCCModal}
        openApplyNowModal={openApplyNowModal}
        navigation={navigation}
        mprccBenefits={mprccBenefits}
        setAnalyticsVars={this.setAnalyticsVars}
        storeId={storeId}
      />
    );
  }
}

MyPlaceRewardsCreditCardContainer.propTypes = {
  labels: PropTypes.shape({}),
  isPLCCModalOpen: PropTypes.bool,
  toggleModal: PropTypes.func.isRequired,
  openApplyNowModal: PropTypes.func.isRequired,
  navigation: PropTypes.func.isRequired,
  mprccBenefits: PropTypes.string,
  isGuestUser: PropTypes.bool,
  isPlcc: PropTypes.bool,
  storeId: PropTypes.string.isRequired,
};

MyPlaceRewardsCreditCardContainer.defaultProps = {
  labels: {},
  isPLCCModalOpen: false,
  mprccBenefits: '',
  isGuestUser: true,
  isPlcc: false,
};

const mapDispatchToProps = dispatch => {
  return {
    toggleModal: payload => {
      dispatch(toggleApplyNowModal(payload));
    },
    openApplyNowModal: payload => {
      dispatch(toggleApplyNowModal(payload));
      dispatch(resetPLCCResponse(payload));
    },
  };
};

export const mapStateToProps = state => {
  return {
    isGuestUser: isGuest(state),
    isPlcc: isPlccUser(state),
    labels: getLabels(state),
    isPLCCModalOpen: getIsPLCCModalOpen(state),
    mprccBenefits: getMPRCCBenefits(state),
    storeId: getDefaultStore(state),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MyPlaceRewardsCreditCardContainer);

export { MyPlaceRewardsCreditCardContainer as MyPlaceRewardsCreditCardContainerVanilla };
