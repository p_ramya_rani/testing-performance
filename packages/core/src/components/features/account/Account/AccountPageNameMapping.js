// 9fbef606107a605d69c0edbcd8029e5d
// TODO: Need to check if there is some other approach for pageName

const AccountPageNameMapping = {
  'address-book': {
    pageName: 'myplace:address book',
    pageNavigationText: 'my account-address book',
  },
  'account-overview': {
    pageName: 'myplace:accountoverview',
    pageNavigationText: 'my account-account overview',
  },
  'add-new-address': {
    pageName: '',
    pageNavigationText: '',
  },
  'edit-address': {
    pageName: '',
    pageNavigationText: '',
  },
  'add-gift-card': {
    pageName: '',
    pageNavigationText: '',
  },
  payment: {
    pageName: 'myplace:payment',
    pageNavigationText: 'my account-payment & gift cards',
  },
  'add-credit-card': {
    pageName: 'myplace:payment:add credit card',
    pageNavigationText: '',
  },
  'edit-credit-card': {
    pageName: '',
    pageNavigationText: '',
  },
  'place-rewards': {
    pageName: 'myplace:place rewards',
    pageNavigationText: 'my account-my place rewards',
  },
  rewardsCreditCard: {
    pageName: 'myplace:rewardscreditcard',
    pageNavigationText: 'my account-my place rewards credit card',
  },
  wallet: {
    pageName: 'myplace:wallet',
    pageNavigationText: 'my account-my wallet',
  },
  profile: {
    pageName: 'myplace:profile',
    pageNavigationText: 'my account-profile information',
  },
  'change-password': {
    pageName: '',
    pageNavigationText: '',
  },
  'edit-personal-info': {
    pageName: '',
    pageNavigationText: '',
  },
  'birthday-savings': {
    pageName: '',
    pageNavigationText: '',
  },
  'edit-mailing-address': {
    pageName: '',
    pageNavigationText: '',
  },
  'edit-aboutyou-info': {
    pageName: '',
    pageNavigationText: '',
  },
  'my-preference': {
    pageName: 'myplace:my preference',
    pageNavigationText: 'my account-my preferences',
  },
  'points-history': {
    pageName: 'myplace:place rewards:points history',
    pageNavigationText: '',
  },
  'extra-points': {
    pageName: 'myplace:extra points',
    pageNavigationText: 'my account-earn extra points',
  },
  'points-claim': {
    pageName: '',
    pageNavigationText: '',
  },
  orders: {
    pageName: 'myplace:orders',
    pageNavigationText: 'my account-orders',
  },
  favorites: {
    pageName: 'myplace:my favorites',
    pageNavigationText: 'my account-my favorites',
  },
  'order-details': {
    pageName: '',
    pageNavigationText: '',
  },
};

export default AccountPageNameMapping;
