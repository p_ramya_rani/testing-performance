// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

const StyledKeyboardAvoidingView = styled.KeyboardAvoidingView`
  display: flex;
`;

const StyledScrollView = styled.ScrollView`
  display: flex;
`;

export { StyledKeyboardAvoidingView, StyledScrollView };
