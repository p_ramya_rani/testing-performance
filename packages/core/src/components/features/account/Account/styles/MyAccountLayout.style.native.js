// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components/native';

const ParentContainer = css`
  display: flex;
  margin-right: 14px;
  margin-bottom: 19px;
  margin-left: 14px;
`;

export default ParentContainer;
