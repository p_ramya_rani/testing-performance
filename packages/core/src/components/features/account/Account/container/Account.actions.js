// 9fbef606107a605d69c0edbcd8029e5d 
import { loadComponentLabelsData } from '@tcp/core/src/reduxStore/actions';
import { LABELS } from '@tcp/core/src/reduxStore/constants';
import { isMobileApp } from '../../../../../utils';

import ACCOUNT_CONSTANTS from '../Account.constants';

export const getAccountNavigationList = () => ({
  type: ACCOUNT_CONSTANTS.GET_ACCOUNT_NAVIGATION_LIST,
});

export const setAccountNavigationList = accountNav => ({
  type: ACCOUNT_CONSTANTS.SET_ACCOUNT_NAVIGATION_LIST,
  accountNav,
});

export const getModuleXContent = referred => ({
  type: ACCOUNT_CONSTANTS.GET_MODULEX_CONTENT,
  referred,
});

export const setModuleXContent = content => ({
  type: ACCOUNT_CONSTANTS.SET_MODULEX_CONTENT,
  content,
});

export const showLoader = () => ({
  type: ACCOUNT_CONSTANTS.SHOW_LOADER,
});

export const hideLoader = () => ({
  type: ACCOUNT_CONSTANTS.HIDE_LOADER,
});

export const initActions = isMobileApp()
  ? [loadComponentLabelsData({ category: LABELS.account })]
  : [args => loadComponentLabelsData({ category: LABELS.account, ...args })];
