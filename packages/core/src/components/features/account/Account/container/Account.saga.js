// 9fbef606107a605d69c0edbcd8029e5d 
import { call, takeLatest, put } from 'redux-saga/effects';
import ACCOUNT_CONSTANTS from '../Account.constants';

import { validateReduxCache } from '../../../../../utils/cache.util';
import { getAPIConfig, isMobileApp } from '../../../../../utils';
import {
  defaultBrand,
  defaultChannel,
  defaultCountry,
  MobileChannel,
} from '../../../../../services/api.constants';
import {
  setAccountNavigationList,
  showLoader,
  setModuleXContent,
  hideLoader,
} from './Account.actions';
import { getModuleX } from '../../../../../services/abstractors/common/moduleXComposite';
import accountNavigationAbstractor from '../../../../../services/abstractors/account/AccountNavigation/index';

export function* getAccountNavigationList() {
  const apiConfig = getAPIConfig();
  const isMobileChannel = isMobileApp() || apiConfig.isAppChannel;
  const channel = isMobileChannel ? MobileChannel : defaultChannel;

  const queryParams = {
    brand: (apiConfig && apiConfig.brandIdCMS) || defaultBrand,
    channel,
    country: (apiConfig && apiConfig.siteIdCMS) || defaultCountry,
  };

  try {
    yield put(showLoader());
    const accountNav = yield call(
      accountNavigationAbstractor.getData,
      'AccountNavigation',
      queryParams
    );
    yield put(setAccountNavigationList(accountNav));
  } catch (err) {
    yield null;
  } finally {
    yield put(hideLoader());
  }
}

const getCidsMap = referred => {
  return referred.reduce((result, current) => {
    return {
      ...result,
      [current.contentId]: current.name,
    };
  }, {});
};

export function* fetchModuleX({ referred }) {
  try {
    const cidMap = getCidsMap(referred);
    const cids = Object.keys(cidMap);
    if (cids.length > 0) {
      const result = yield call(getModuleX, referred);
      yield put(setModuleXContent(result));
    }
  } catch (err) {
    yield null;
  }
}

export function* AccountSaga() {
  const cachedAccountNavigationList = validateReduxCache(getAccountNavigationList);
  yield takeLatest(ACCOUNT_CONSTANTS.GET_ACCOUNT_NAVIGATION_LIST, cachedAccountNavigationList);
  yield takeLatest(ACCOUNT_CONSTANTS.GET_MODULEX_CONTENT, fetchModuleX);
}

export default AccountSaga;
