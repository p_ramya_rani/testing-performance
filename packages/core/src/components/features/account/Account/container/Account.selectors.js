// 9fbef606107a605d69c0edbcd8029e5d 
import { createSelector } from 'reselect';
import { getLabelValue } from '@tcp/core/src/utils';

export const getLabels = state => {
  return state.Labels.account;
};

export const getGlobalLabels = state => {
  return state.Labels && state.Labels.global;
};

export const getCommonLabels = state => {
  return state.Labels && state.Labels.account && state.Labels.account.common;
};

export const getAccountNavigationState = state => {
  return state.AccountReducer.get('accountNavigation');
};

export const getAccountNavigationFetchingState = state => {
  return state.AccountReducer.get('isFetching');
};

export const getAccountRichTextContent = state => {
  return state.AccountReducer.get('richTextContent');
};

export const getMPRCCBenefits = state => {
  return state.AccountReducer.getIn(['richTextContent', 'mprccBenefits'], '');
};

export const getResetPasswordSuccessMsg = state => {
  return state.AccountReducer.getIn(['richTextContent', 'reset_password_check_email_message'], '');
};

export const getErrorMessages = state => {
  return state.Labels.global;
};

export const getAccessibilityLabels = state => {
  return {
    lbl_social_twitter: getLabelValue(
      state.Labels,
      'lbl_social_twitter',
      'accessibility',
      'global'
    ),
    lbl_social_facebook: getLabelValue(
      state.Labels,
      'lbl_social_facebook',
      'accessibility',
      'global'
    ),
    lbl_social_instagram: getLabelValue(
      state.Labels,
      'lbl_social_instagram',
      'accessibility',
      'global'
    ),
    lbl_user_icon: getLabelValue(state.Labels, 'lbl_user_icon', 'accessibility', 'global'),
    lbl_sms_enabled_icon: getLabelValue(
      state.Labels,
      'lbl_sms_enabled_icon',
      'accessibility',
      'global'
    ),
    lbl_sms_disabled_icon: getLabelValue(
      state.Labels,
      'lbl_sms_disabled_icon',
      'accessibility',
      'global'
    ),
    lbl_push_enabled_icon: getLabelValue(
      state.Labels,
      'lbl_push_enabled_icon',
      'accessibility',
      'global'
    ),
    lbl_push_disabled_icon: getLabelValue(
      state.Labels,
      'lbl_push_disabled_icon',
      'accessibility',
      'global'
    ),
    lbl_bag_icon: getLabelValue(state.Labels, 'cartIconButton', 'accessibility', 'global'),
  };
};

export const getFormValidationErrorMessages = createSelector(
  [getErrorMessages, getLabels],
  (global, account) => {
    let result = {};
    if (global) {
      result = {
        ...global.formValidation,
      };
    }
    if (account) {
      result = {
        ...result,
        ...account.myPlaceRewards,
      };
    }
    return result;
  }
);
