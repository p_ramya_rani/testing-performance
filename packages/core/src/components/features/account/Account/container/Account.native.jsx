// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { loadComponentLabelsData, getSubNavigationData } from '@tcp/core/src/reduxStore/actions';
import { LABELS } from '@tcp/core/src/reduxStore/constants';
import { getUserInfo } from '@tcp/core/src/components/features/account/User/container/User.actions';
import {
  trackForterNavigation,
  ForterNavigationType,
} from '@tcp/core/src/utils/forter.util.native';

import MyAccountLayout from '../views/MyAccountLayout.view';
import AccountComponentNativeMapping from '../AccountComponentMapping.native';
import accountPageNameMapping from '../AccountPageNameMapping';
import {
  StyledKeyboardAvoidingView,
  StyledScrollView,
} from '../styles/MyAccountContainer.style.native';
import constants from '../Account.constants';
import { getLabels, getAccountNavigationState, getGlobalLabels } from './Account.selectors';
import { getUserLoggedInState } from '../../User/container/User.selectors';
import { isMobileApp, navigateToNestedRoute } from '../../../../../utils/utils.app';
import { trackPageView, setClickAnalyticsData } from '../../../../../analytics/actions';
import { getAccountNavigationList, getModuleXContent } from './Account.actions';

/**
 * @function Account The Account component is the main container for the account section
 * This component includes the layout view, it passes the MyAccountLayout with the mainContent to be rendered
 * on th right side.
 * NOTE: Which ever new component that gets added for drop down nav, needs an entry in AccountComponentMappingNative file.
 */

const navConfigMap = {
  payment: 'paymentGiftCardsPageMobile',
  'place-rewards': 'myPlaceRewardsMobile',
  'account-overview': 'accountOverviewMobile',
  profile: 'profileInformationMobile',
  wallet: 'myWalletPageMobile',
  'extra-points': 'earnExtraPointsPageMobile',
  'points-history': 'pointHistoryPageMobile',
  'my-preference': 'myPreferencePageMobile',
  'points-claim': 'PointsClaimPageMobile',
  orders: 'myOrdersPageMobile',
  'address-book': 'addressBookMobile',
  favorites: 'myFavoritePageMobile',
  rewardsCreditCard: 'myPlaceRewardsCCPageMobile',
};

export class Account extends React.PureComponent {
  static propTypes = {
    labels: PropTypes.shape({}),
    globalLabels: PropTypes.shape({}),
    component: PropTypes.string,
    isUserLoggedIn: PropTypes.bool,
    closeOverlay: PropTypes.func,
    getAccountNavigationAction: PropTypes.func,
    navigation: PropTypes.shape({}),
    fetchLabels: PropTypes.func,
    fetchFooterLinks: PropTypes.func,
    trackPageLoad: PropTypes.func,
    getAccountModuleXContent: PropTypes.func,
    dispatchUserInfo: PropTypes.func,
    screenProps: PropTypes.shape({}),
  };

  static defaultProps = {
    labels: {},
    screenProps: {},
    globalLabels: {},
    component: '',
    isUserLoggedIn: false,
    closeOverlay: () => {},
    getAccountNavigationAction: () => {},
    navigation: {},
    fetchLabels: () => {},
    fetchFooterLinks: () => {},
    trackPageLoad: () => {},
    getAccountModuleXContent: () => {},
    dispatchUserInfo: () => {},
  };

  constructor(props) {
    super(props);
    const { component } = this.props;
    this.state = {
      component,
      navData: [],
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (
      props.accountNavigation &&
      props.accountNavigation.accountNav &&
      props.accountNavigation.accountNav.length > 0 &&
      state.navData.length === 0
    ) {
      return {
        navData: props.accountNavigation.accountNav.map((nav) => ({
          ...nav,
          ...{
            value: navConfigMap[nav.component],
          },
        })),
      };
    }

    return null;
  }

  componentDidMount() {
    const {
      getAccountNavigationAction,
      fetchLabels,
      fetchFooterLinks,
      labels,
      getAccountModuleXContent,
      screenProps,
    } = this.props;
    getAccountNavigationAction();
    if (typeof labels === 'object' && labels.common && labels.common.referred) {
      getAccountModuleXContent(labels.common.referred);
    }
    fetchFooterLinks([constants.FOOTER_LINKS, constants.LEGAL_LINKS]);
    fetchLabels({ category: LABELS.account });
    this.triggerPageLoadEvent();
    screenProps.checkFirst('AccountStack');
    trackForterNavigation('AccountPage', ForterNavigationType.ACCOUNT, {}, true);
  }

  componentDidUpdate(prevProps, prevState) {
    const { isUserLoggedIn, closeOverlay, labels, getAccountModuleXContent, dispatchUserInfo } =
      this.props;
    const { component } = this.state;
    const hasMobile = isMobileApp();
    if (!prevProps.isUserLoggedIn && isUserLoggedIn && !hasMobile) {
      closeOverlay();
    }

    if (labels !== prevProps.labels && labels.common && labels.common.referred) {
      getAccountModuleXContent(labels.common.referred);
    }

    if (prevProps.isUserLoggedIn && !isUserLoggedIn) {
      this.scrollView.scrollTo({ x: 0, y: 0, animated: false });
    }

    if (prevState.component !== component) {
      this.scrollView.scrollTo({ x: 0, y: 0, animated: false });
      this.triggerPageLoadEvent();
      dispatchUserInfo(); // for internal route change in Account
    }
  }

  triggerPageLoadEvent = () => {
    const { component } = this.state;
    const { trackPageLoad } = this.props;

    const { pageNavigationText = '', pageName = '' } = accountPageNameMapping[component] || {};

    if (pageName) {
      trackPageLoad({
        currentScreen: 'account',
        pageNavigationText,
        pageData: {
          pageName,
          pageSection: 'myplace',
          pageSubSection: 'myplace',
          pageType: 'myplace',
        },
      });
    }
  };

  /**
   *  @function getComponent takes component and return the component that is required on the drop down click.
   */
  getComponent = (component) => {
    const componentObject = {
      paymentGiftCardsPageMobile: 'paymentGiftCardsPageMobile',
      myPlaceRewardsMobile: 'myPlaceRewardsMobile',
      accountOverviewMobile: 'accountOverviewMobile',
      profileInformationMobile: 'profileInformationMobile',
      myWalletPageMobile: 'myWalletPageMobile',
      earnExtraPointsPageMobile: 'earnExtraPointsPageMobile',
      pointsHistoryMobile: 'pointHistoryPageMobile',
      myPreferencePageMobile: 'myPreferencePageMobile',
      PointsClaimPageMobile: 'PointsClaimPageMobile',
      myOrdersPageMobile: 'myOrdersPageMobile',
      addressBookMobile: 'addressBookMobile',
      orderDetailsPageMobile: 'orderDetailsPageMobile',
      myFavoritePageMobile: 'myFavoritePageMobile',
      myPlaceRewardsCCPageMobile: 'myPlaceRewardsCCPageMobile',
    };
    if (componentObject[component]) {
      return componentObject[component];
    }
    return 'addressBookMobile';
  };

  /**
   *  @function handleComponentChange triggered when dropdown clicked
   */
  handleComponentChange = (component, otherProps, isPageNavigation) => {
    if (isPageNavigation) {
      const { navigation } = this.props;
      navigation.navigate(component);
      return;
    }
    const componentName = this.getComponent(component);
    this.setState({
      component: componentName,
      componentProps: otherProps,
    });
  };

  navigattePage() {
    const { navigation } = this.props;
    navigateToNestedRoute(navigation, 'HomeStack', 'home');
  }

  /**
   * @function render  Used to render the JSX of the component
   * @param    {[Void]} function does not accept anything.
   * @return   {[Object]} JSX of the component
   */
  render() {
    const { component, componentProps, navData } = this.state;
    const { labels, isUserLoggedIn, navigation, globalLabels } = this.props;
    return (
      <StyledKeyboardAvoidingView behavior="padding" enabled keyboardVerticalOffset={82}>
        <StyledScrollView
          ref={(ref) => {
            this.scrollView = ref;
          }}
          keyboardShouldPersistTaps="handled"
        >
          <MyAccountLayout
            navData={navData}
            component={this.getComponent(component)}
            componentProps={componentProps}
            mainContent={AccountComponentNativeMapping[component]}
            handleComponentChange={this.handleComponentChange}
            labels={labels}
            globalLabels={globalLabels}
            isUserLoggedIn={isUserLoggedIn}
            navigation={navigation}
          />
        </StyledScrollView>
      </StyledKeyboardAvoidingView>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    labels: getLabels(state),
    globalLabels: getGlobalLabels(state),
    isUserLoggedIn: getUserLoggedInState(state),
    accountNavigation: getAccountNavigationState(state),
  };
};

export const mapDispatchToProps = (dispatch) => {
  return {
    getAccountNavigationAction: () => {
      dispatch(getAccountNavigationList());
    },
    fetchLabels: (payload) => {
      dispatch(loadComponentLabelsData(payload));
    },
    fetchFooterLinks: (payload) => {
      dispatch(getSubNavigationData(payload));
    },
    trackPageLoad: (payload) => {
      dispatch(
        setClickAnalyticsData({
          customEvents: ['event80'],
          pageNavigationText: payload.pageNavigationText,
        })
      );
      dispatch(trackPageView(payload));
      setTimeout(() => {
        dispatch(setClickAnalyticsData({}));
      }, 100);
    },
    getAccountModuleXContent: (referred) => {
      dispatch(getModuleXContent(referred));
    },
    dispatchUserInfo: (payload) => {
      dispatch(getUserInfo(payload));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Account);
export { Account as AccountVanilla };
