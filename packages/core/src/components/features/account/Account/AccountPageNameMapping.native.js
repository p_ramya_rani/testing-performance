// 9fbef606107a605d69c0edbcd8029e5d
// TODO: Need to check if there is some other approach for pageName

const AccountPageNameMapping = {
  addressBookMobile: {
    pageName: 'myplace:address book',
    pageNavigationText: 'my account-address book',
  },
  accountOverviewMobile: {
    pageName: 'myplace:accountoverview',
    pageNavigationText: 'my account-account overview',
  },
  paymentGiftCardsPageMobile: {
    pageName: 'myplace:payment',
    pageNavigationText: 'my account-payment & gift cards',
  },
  myPlaceRewardsCCPageMobile: {
    pageName: 'myplace:rewardscreditcard',
    pageNavigationText: 'my account-my place rewards credit card',
  },
  myPlaceRewardsMobile: {
    pageName: 'myplace:place rewards',
    pageNavigationText: 'my account-my place rewards',
  },
  myWalletPageMobile: {
    pageName: 'myplace:wallet',
    pageNavigationText: 'my account-my wallet',
  },
  profileInformationMobile: {
    pageName: 'myplace:profile',
    pageNavigationText: 'my account-profile information',
  },
  myPreferencePageMobile: {
    pageName: 'myplace:my preference',
    pageNavigationText: 'my account-my preferences',
  },
  earnExtraPointsPageMobile: {
    pageName: 'myplace:extra points',
    pageNavigationText: 'my account-earn extra points',
  },
  myOrdersPageMobile: {
    pageName: 'myplace:orders',
    pageNavigationText: 'my account-orders',
  },
  myFavoritePageMobile: {
    pageName: 'myplace:my favorites',
    pageNavigationText: 'my account-my favorites',
  },
};

export default AccountPageNameMapping;
