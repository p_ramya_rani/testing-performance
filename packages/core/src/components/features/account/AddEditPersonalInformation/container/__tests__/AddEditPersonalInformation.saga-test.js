// 9fbef606107a605d69c0edbcd8029e5d 
import { put, takeLatest } from 'redux-saga/effects';
import { UpdateProfile, UpdateProfileSaga } from '../AddEditPersonalInformation.saga';
import { updateProfileError } from '../AddEditPersonalInformation.actions';
import { updateProfileSuccess } from '../../../MyProfile/container/MyProfile.actions';
import constants from '../../AddEditPersonalInformation.constants';

describe('UpdateProfile saga', () => {
  describe('UpdateProfile', () => {
    let gen;
    beforeEach(() => {
      gen = UpdateProfile({ payload: {} });
      gen.next();
      gen.next();
    });

    it('should dispatch updateProfileSuccess action for success response', () => {
      const response = 'success';
      gen.next(response);
      gen.next();
      gen.next();
      const putDescriptor = gen.next().value;
      expect(putDescriptor).toEqual(put(updateProfileSuccess(response)));
    });

    it('should dispatch updateProfileError action if response is error', () => {
      const responseError = {
        errorCode: 'ASSOCIATE_ID_DOES_NOT_EXIST',
        errorMessage: {
          _error: 'The Associate ID you entered does not exist. Please try again.',
        },
        errorResponse: {
          errorCode: '8004',
          errorKey: 'ASSOCIATE_ID_DOES_NOT_EXIST',
          errorMessage: 'The given associate ID is incorrect.',
        },
      };
      gen.throw(responseError);
      gen.next();
      const putDescriptor = gen.next().value;
      expect(putDescriptor).toEqual(put(updateProfileError(responseError.errorResponse)));
    });

    it('should dispatch updateProfileError action if email is already in use', () => {
      const responseError = {
        errorResponse: {
          errors: [
            {
              errorCode: 'EMAIL_ADDRESS_ALREADY_EXIST',
              errorKey: '_TCP_EMAIL_ADDRESS_ALREADY_EXIST',
              errorMessage:
                'An account with this email address already exists. Please try logging in with your account or using a different email address',
              status: 'FAIL',
            },
          ],
        },
      };
      gen.throw(responseError);
      gen.next();
      const putDescriptor = gen.next().value;
      expect(putDescriptor).toEqual(
        put(updateProfileError({ errorKey: '_TCP_EMAIL_ADDRESS_ALREADY_EXIST' }))
      );
    });
  });

  describe('UpdateProfileSaga', () => {
    it('should return correct takeLatest effect', () => {
      const generator = UpdateProfileSaga();
      const takeLatestDescriptor = generator.next().value;
      const expected = takeLatest(constants.UPDATE_PROFILE, UpdateProfile);
      expect(takeLatestDescriptor).toEqual(expected);
    });
  });
});
