// 9fbef606107a605d69c0edbcd8029e5d
import { call, takeLatest, put, take, select } from 'redux-saga/effects';
import { setLoaderState } from '@tcp/core/src/components/common/molecules/Loader/container/Loader.actions';
import getErrorList from '@tcp/core/src/components/features/CnC/BagPage/container/Errors.selector';
import { trackFormError } from '@tcp/core/src/analytics/actions';
import { getFormattedError } from '@tcp/core/src/utils/errorMessage.util';
import constants from '../AddEditPersonalInformation.constants';
import { getUserInfo } from '../../User/container/User.actions';
import CONSTANTS from '../../User/User.constants';
import { updateProfileError } from './AddEditPersonalInformation.actions';
import { updateProfileSuccess } from '../../MyProfile/container/MyProfile.actions';
import { UpdateProfileInfo } from '../../../../../services/abstractors/account';

export function* UpdateProfile({ payload }) {
  yield put(setLoaderState(true));
  try {
    const res = yield call(UpdateProfileInfo, payload);
    yield put(setLoaderState(false));
    yield put(
      getUserInfo({
        ignoreCache: true,
      })
    );
    yield take(CONSTANTS.SET_USER_INFO);
    return yield put(updateProfileSuccess(res));
  } catch (error) {
    const { firstName, lastName, email, phone, userBirthday, airmiles } = payload;
    const birthMonth = userBirthday && userBirthday.split('|')[0];
    const requiredList3Val = { firstName, lastName, email, phone, birthMonth, airmiles };

    yield put(
      trackFormError({
        formName: constants.ADD_PROFILE_INFORMATION_FORM,
        formData: requiredList3Val,
      })
    );
    yield put(setLoaderState(false));

    if (error && error.errorResponse && error.errorResponse.errors) {
      const { errorKey = '' } = error.errorResponse.errors[0] || {};
      return yield put(updateProfileError({ errorKey }));
    }

    if (error && error.errorResponse) {
      return yield put(updateProfileError(error.errorResponse));
    }
    const errorMapping = yield select(getErrorList);
    const errorMessage = getFormattedError(error, errorMapping);
    const errorKey = errorMessage && errorMessage.errorCodes;
    return yield put(updateProfileError({ errorKey }));
  }
}

export function* UpdateProfileSaga() {
  yield takeLatest(constants.UPDATE_PROFILE, UpdateProfile);
}

export default UpdateProfileSaga;
