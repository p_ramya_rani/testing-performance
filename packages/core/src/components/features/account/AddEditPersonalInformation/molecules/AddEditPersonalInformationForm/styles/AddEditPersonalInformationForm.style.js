// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  @media ${props => props.theme.mediaQuery.smallOnly} {
    .addEditPersonalBirthday {
      margin-top: ${props => props.theme.spacing.ELEM_SPACING.MED};
    }
    .addEditPersonalPhoneNumber {
      margin-top: ${props => props.theme.spacing.ELEM_SPACING.MED};
    }
    .addEditPersonallastName {
      margin-top: ${props => props.theme.spacing.ELEM_SPACING.MED};
    }
  }
  .select__input {
    padding-top: ${props => props.theme.spacing.ELEM_SPACING.XS};
  }

  @media ${props => props.theme.mediaQuery.mediumOnly} {
    .AddEditPersonalInformationForm_cancel {
      margin-left: 0px;
    }

    .Cancel_Submit_Button {
      justify-content: center;
    }
  }

  @media ${props => props.theme.mediaQuery.smallMax} {
    .AddEditPersonalInformationForm_cancel {
      order: 2;
    }

    .AddEditPersonalInformationForm_update {
      order: 1;
      margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.MED};
    }
  }
`;
export default styles;
