// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';

const InputGridStyle = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
`;

const CellStyles = styled.Text`
  padding: 20px 0;
  width: 12%;
  height: 65px;
  margin: 8px;
  text-align: center;
  font-size: 22px;
  color: ${(props) => props.theme.colorPalette.black};
  background-color: ${(props) => props.theme.colorPalette.gray[300]};
  border-radius: 6px;
  border-width: 1px;
  border-color: ${(props) =>
    props.isFocused ? props.theme.colorPalette.green[500] : props.theme.colors.PRIMARY.GRAY};
`;

export { InputGridStyle, CellStyles };
