// 9fbef606107a605d69c0edbcd8029e5d
import React, { Component } from 'react';
import { View, TextInput, InteractionManager, AppState } from 'react-native';
import PropTypes from 'prop-types';
import { InputGridStyle, CellStyles } from '../styles/OTPInput.style.native';

const TextInputStyle = { width: 0, height: 0 };

class OTPInput extends Component {
  textInput = null;

  state = {
    internalVal: '',
    appState: AppState.currentState,
  };

  componentDidMount() {
    this.focusInputWithKeyboard();
    AppState.addEventListener('change', this.handleAppStateChange);
  }

  componentDidUpdate(prevProps) {
    const { handleResetPinField, onChange } = this.props;
    if (this.props.resetPinField !== prevProps.resetPinField) {
      onChange('');
      this.setState({ internalVal: '' });
      handleResetPinField(false);
    }
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this.handleAppStateChange);
  }

  handleAppStateChange = (nextAppState) => {
    const { appState } = this.state;
    if (appState.match(/inactive|background/) && nextAppState === 'active') {
      this.inputRef().blur();
      setTimeout(() => this.focusInputWithKeyboard(), 100);
    }
    this.setState({ appState: nextAppState });
  };

  handleChangeText = (val) => {
    const { onChange } = this.props;

    onChange(val);
    this.setState({ internalVal: val });
  };

  focusInputWithKeyboard() {
    InteractionManager.runAfterInteractions(() => {
      this.focus();
    });
  }

  inputRef() {
    return this.textInput;
  }

  focus() {
    this.inputRef().focus();
  }

  clear() {
    this.setState({ internalVal: '' });
  }

  render() {
    const { otpLength } = this.props;

    const { internalVal } = this.state;

    return (
      <View>
        <TextInput
          ref={(input) => {
            this.textInput = input;
          }}
          onChangeText={this.handleChangeText}
          style={TextInputStyle}
          value={internalVal}
          minLength={otpLength}
          maxLength={otpLength}
          returnKeyType="done"
          keyboardType="numeric"
          autoFocus
        />
        <InputGridStyle>
          {Array(otpLength)
            .fill()
            .map((_, index) => (
              <>
                <CellStyles
                  key={`otp-input-${index.toString()}`}
                  onPress={() => this.textInput.focus()}
                  index={index}
                  isFocused={
                    (internalVal && index === internalVal.length) || (!index && !internalVal)
                  }
                >
                  {internalVal && internalVal.length > index ? internalVal[index] : ' '}
                </CellStyles>
              </>
            ))}
        </InputGridStyle>
      </View>
    );
  }
}
OTPInput.propTypes = {
  onChange: PropTypes.func,
  otpLength: PropTypes.number,
};

OTPInput.defaultProps = {
  onChange: () => {},
  otpLength: 6,
};

export default OTPInput;
export { OTPInput };
