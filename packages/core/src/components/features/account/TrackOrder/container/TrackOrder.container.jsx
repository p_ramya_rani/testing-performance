// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import { getEnableFullPageAuth } from '@tcp/web/src/components/features/content/HomePage/HomePage.selectors';
import CUSTOMER_HELP_CONSTANTS, {
  CUSTOMER_HELP_ROUTES,
  CUSTOMER_HELP_PAGE_TEMPLATE,
} from '@tcp/core/src/components/features/account/CustomerHelp/CustomerHelp.constants';
import { ifElseExecute } from '@tcp/core/src/utils';
import { getFormValidationErrorMessages } from '../../Account/container/Account.selectors';
import { openOverlayModal } from '../../OverlayModal/container/OverlayModal.actions';
import {
  setTrackOrderModalMountedState,
  setErrorInfoNull,
  setTrackOrderInitiator,
} from './TrackOrder.actions';
import internalEndpoints from '../../common/internalEndpoints';
import { getOrderDetails } from '../../OrderDetails/container/OrderDetails.actions';
import { getOrderLookUpAPISwitch } from '../../OrderDetails/container/OrderDetails.selectors';
import TrackOrderView from '../views';
import {
  getLabels,
  getOrderLabels,
  getErrorMessage,
  getTrackOrderMountedState,
  getEmailId,
  getOrderId,
  getOrderDetail,
  getShowNotificationState,
  getTrackOrderInitiator,
} from './TrackOrder.selectors';
import { getUserLoggedInState } from '../../User/container/User.selectors';
import { routerPush } from '../../../../../utils';

export class TrackOrderContainer extends React.PureComponent {
  constructor(props) {
    super(props);
    import('../../../../../utils')
      .then(({ isMobileApp, navigateToNestedRoute }) => {
        this.hasMobileApp = isMobileApp;
        this.hasNavigateToNestedRoute = navigateToNestedRoute;
      })
      .catch(error => {
        console.log('error: ', error);
      });
  }

  componentDidUpdate() {
    const { orderDetailResponse, isUserLoggedIn } = this.props;
    const isSuccess = orderDetailResponse && orderDetailResponse.get('success');
    if (isSuccess) {
      this.trackOrderDetail(
        orderDetailResponse.get('orderId'),
        orderDetailResponse.get('encryptedEmailAddress'),
        isUserLoggedIn
      );
    }
  }

  mobileAppOrderDetailNavigation = (orderId, orderLabels, navigation) => {
    const router = {
      query: {
        orderId,
      },
    };
    navigation.navigate('OrderDetailPage', {
      title: `${getLabelValue(orderLabels, 'lbl_orderDetail_heading', 'orders')} #${orderId}`,
      router,
      backTo: 'Account',
    });
  };

  handleOrderDetailNavigation = (orderId, encryptedEmailAddress) => {
    const { navigation, orderLabels } = this.props;
    if (this.hasMobileApp()) {
      const router = {
        query: {
          orderId,
        },
      };
      navigation.navigate('OrderDetailPage', {
        title: `${getLabelValue(orderLabels, 'lbl_orderDetail_heading', 'orders')} #${orderId}`,
        router,
        backTo: 'Account',
      });
    } else {
      routerPush(
        `${internalEndpoints.trackOrder.link}&orderId=${orderId}&email=${encryptedEmailAddress}`,
        `${internalEndpoints.trackOrder.path}/${orderId}/${encryptedEmailAddress}`
      );
    }
  };

  handleCSHNavigation = orderId => {
    const { resetTrackOrderInitiator, navigation } = this.props;
    if (this.hasMobileApp()) {
      navigation.navigate(CUSTOMER_HELP_PAGE_TEMPLATE.TELL_US_MORE, {
        orderId,
        fromPage: 'TrackOrder',
      });
    } else {
      resetTrackOrderInitiator();
      routerPush(
        `${CUSTOMER_HELP_ROUTES.TELL_US_MORE.to}&orderId=${orderId}`,
        `${CUSTOMER_HELP_ROUTES.TELL_US_MORE.asPath}/${orderId}`
      );
    }
  };

  /**
   * TO DO - Integrate with Account POD guest order detail feature
   * @param {string} orderId - order id
   * @param {string} encryptedEmailAddress - encrypted user email address
   * @param {boolean} isGuest - check if it is guest login or signed in user.
   */
  trackOrderDetail = (orderId = '', encryptedEmailAddress = '', isUserLoggedIn = false) => {
    const {
      setTrackOrderModalMountState,
      trackOrderInitiator,
      trackOrderInitiatorInApp,
    } = this.props;

    const trackOrderModalInitiator = trackOrderInitiator || trackOrderInitiatorInApp;

    setTrackOrderModalMountState({ state: false });
    if (!isUserLoggedIn) {
      ifElseExecute(
        () => this.handleCSHNavigation(orderId),
        () => this.handleOrderDetailNavigation(orderId, encryptedEmailAddress),
        trackOrderModalInitiator === CUSTOMER_HELP_CONSTANTS.CUSTOMER_HELP
      );
    }
  };

  handleSubmit(e) {
    e.preventDefault();
    const {
      onSubmit,
      emailId,
      orderId,
      orderLookUpAPISwitch,
      trackOrderInitiator,
      trackOrderInitiatorInApp,
    } = this.props;
    const trackOrderModalInitiator = trackOrderInitiator || trackOrderInitiatorInApp;
    const payloadArgs = {
      orderId,
      emailAddress: emailId,
      isSaveEmail: true,
      fromCSH: trackOrderModalInitiator === CUSTOMER_HELP_CONSTANTS.CUSTOMER_HELP,
      orderLookUpAPISwitch,
    };
    if (emailId && orderId) onSubmit(payloadArgs);
  }

  render() {
    const {
      errorMessage,
      isUserLoggedIn,
      labels,
      openLoginOverlay,
      trackOrderMountedState,
      trackOrderInitiator,
      trackOrderInitiatorInApp,
      setTrackOrderModalMountState,
      showNotification,
      onChangeForm,
      handleToggle,
      navigation,
      formErrorMessage,
      showFullPageAuth,
      resetTrackOrderInitiator,
    } = this.props;
    return (
      <TrackOrderView
        labels={labels}
        errorMessage={errorMessage}
        isGuestUser={isUserLoggedIn}
        onSubmit={e => this.handleSubmit(e)}
        openLoginOverlay={openLoginOverlay}
        openState={trackOrderMountedState}
        trackOrderInitiator={trackOrderInitiator || trackOrderInitiatorInApp}
        setModalMountState={setTrackOrderModalMountState}
        className="TrackOrder__Modal"
        showNotification={showNotification}
        onChangeForm={onChangeForm}
        handleToggle={handleToggle}
        navigation={navigation}
        formErrorMessage={formErrorMessage}
        showFullPageAuth={showFullPageAuth}
        resetTrackOrderInitiator={resetTrackOrderInitiator}
      />
    );
  }
}

export const mapStateToProps = state => {
  return {
    labels: getLabels(state),
    orderLabels: getOrderLabels(state),
    errorMessage: getErrorMessage(state),
    isUserLoggedIn: getUserLoggedInState(state),
    trackOrderMountedState: getTrackOrderMountedState(state),
    trackOrderInitiator: getTrackOrderInitiator(state),
    emailId: getEmailId(state),
    orderId: getOrderId(state),
    orderDetailResponse: getOrderDetail(state),
    showNotification: getShowNotificationState(state),
    formErrorMessage: getFormValidationErrorMessages(state),
    showFullPageAuth: getEnableFullPageAuth(state),
    orderLookUpAPISwitch: getOrderLookUpAPISwitch(state),
  };
};

export const mapDispatchToProps = dispatch => {
  return {
    onSubmit: payload => {
      dispatch(getOrderDetails(payload));
    },
    onChangeForm: () => {
      dispatch(setErrorInfoNull());
    },
    openLoginOverlay: payload => {
      dispatch(openOverlayModal(payload));
    },
    setTrackOrderModalMountState: payload => {
      dispatch(setTrackOrderModalMountedState(payload));
    },
    resetTrackOrderInitiator: () => {
      dispatch(setTrackOrderInitiator());
    },
  };
};

TrackOrderContainer.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  emailId: PropTypes.string.isRequired,
  orderId: PropTypes.string.isRequired,
  orderDetailResponse: PropTypes.shape({}).isRequired,
  isUserLoggedIn: PropTypes.bool.isRequired,
  errorMessage: PropTypes.string.isRequired,
  labels: PropTypes.shape({
    trackOrder: PropTypes.shape({}),
  }).isRequired,
  orderLabels: PropTypes.shape({
    trackOrder: PropTypes.shape({}),
  }).isRequired,
  openLoginOverlay: PropTypes.func.isRequired,
  trackOrderMountedState: PropTypes.func.isRequired,
  setTrackOrderModalMountState: PropTypes.func.isRequired,
  showNotification: PropTypes.string.isRequired,
  onChangeForm: PropTypes.func.isRequired,
  handleToggle: PropTypes.func,
  navigation: PropTypes.shape({}),
  formErrorMessage: PropTypes.shape({}),
  showFullPageAuth: PropTypes.bool,
  trackOrderInitiator: PropTypes.string.isRequired,
  trackOrderInitiatorInApp: PropTypes.string.isRequired,
  resetTrackOrderInitiator: PropTypes.func.isRequired,
  orderLookUpAPISwitch: PropTypes.bool,
};

TrackOrderContainer.defaultProps = {
  handleToggle: () => null,
  navigation: {},
  formErrorMessage: {},
  showFullPageAuth: false,
  orderLookUpAPISwitch: false,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TrackOrderContainer);
