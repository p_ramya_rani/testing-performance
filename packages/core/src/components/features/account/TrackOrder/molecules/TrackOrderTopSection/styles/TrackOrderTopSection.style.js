// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  .trackorder__modal__overlayheading {
    text-align: center;
    margin-top: 24px;
  }

  .trackorder__modal__overlaysubheading {
    text-align: center;
    margin-bottom: 24px;
  }
`;

export default styles;
