// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  .trackOrder-trackOrdercta {
    height: 51px;
    margin-top: 20px;
    font-weight: ${props => props.theme.fonts.fontWeight.semiBold};
  }
`;

export default styles;
