// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  .generic__error__message {
    display: flex;
    flex-wrap: wrap;
  }

  .trackorder__modal__contactus {
    padding: 0 4px;
  }
`;

export default styles;
