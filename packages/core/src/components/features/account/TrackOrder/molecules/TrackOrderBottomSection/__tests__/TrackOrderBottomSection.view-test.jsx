// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { routerPush } from '@tcp/core/src/utils';
import { TrackOrderBottomSectionVanilla } from '../views/TrackOrderBottomSection.view';

jest.mock('@tcp/core/src/utils', () => ({
  isMobileApp: jest.fn(),
  routerPush: jest.fn(),
  getAPIConfig: jest.fn(),
  isClient: jest.fn(),
  getIconPath: jest.fn(),
  getStaticFilePath: jest.fn(),
}));

describe('TrackOrderBottomSection component', () => {
  const props = {
    labels: {
      trackOrder: {},
    },
    className: '',
    setModalMountState: jest.fn(),
    openLoginOverlay: jest.fn(),
  };
  const mockedRouterPush = jest.fn();
  const event = {
    preventDefault: jest.fn(),
  };
  it('should renders correctly', () => {
    const component = shallow(<TrackOrderBottomSectionVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should not call openLoginOverlay from  handleDefaultLinkClick method', () => {
    const tree = shallow(<TrackOrderBottomSectionVanilla {...props} showFullPageAuth />);
    routerPush.mockImplementation(mockedRouterPush);
    const componentInstance = tree.instance();
    componentInstance.handleDefaultLinkClick(event);
    expect(props.openLoginOverlay).not.toBeCalled();
  });

  it('calling handleDefaultLinkClick method', () => {
    const tree = shallow(<TrackOrderBottomSectionVanilla {...props} />);
    const componentInstance = tree.instance();
    componentInstance.handleDefaultLinkClick(event);
    expect(props.setModalMountState).toBeCalled();
  });
});
