// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

export const RichTextWrapper = styled.View`
  width: 100%;
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.LRG};
  overflow: hidden;
`;

export const contentHeight = { minHeight: 600 };

export const StyledAnchorWrapper = styled.View`
  justify-content: center;
  flex-direction: row;
  margin-bottom: ${props => props.theme.spacing.LAYOUT_SPACING.XL};
  margin-top: ${props => props.theme.spacing.LAYOUT_SPACING.LRG};
`;

export const AnchorLeftMargin = styled.View`
  margin-left: ${props => props.theme.spacing.ELEM_SPACING.XXL};
`;
