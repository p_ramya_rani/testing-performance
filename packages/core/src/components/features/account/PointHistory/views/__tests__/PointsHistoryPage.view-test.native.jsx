// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { PointsHistoryPageView } from '../PointsHistoryPage.view.native';

describe('PointHistoryPage component', () => {
  it('should renders correctly', () => {
    const props = {
      labels: {},
    };
    const component = shallow(<PointsHistoryPageView {...props} />);
    expect(component).toMatchSnapshot();
  });
});
