// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { ScrollView } from 'react-native';
import Espot from '@tcp/core/src/components/common/molecules/Espot';
import { ViewWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';
import { getLabelValue } from '@tcp/core/src/utils';
import ContentLink from '@tcp/core/src/components/features/account/common/molecule/ContentLink';
import PointsHistoryList from '../../common/organism/PointsHistory';

import {
  RichTextWrapper,
  StyledAnchorWrapper,
  AnchorLeftMargin,
} from '../styles/PointsHistoryPage.view.style.native';

/**
 * This component will render PointsHistoryPage component
 * @param { object } labels
 */
export const PointsHistoryPageView = props => {
  const { labels, richTextContent, accountLabels } = props;
  return (
    <ScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="handled">
      <ViewWithSpacing spacingStyles="margin-left-SM margin-right-SM">
        <PointsHistoryList view="edit" labels={labels} showFullHistory />
        <RichTextWrapper dataLocator="points_history_rte">
          <Espot isNativeView={false} richTextHtml={richTextContent} />
        </RichTextWrapper>
        <StyledAnchorWrapper>
          <ContentLink
            fontSizeVariation="medium"
            underline
            urlKey="lbl_rewardprogram_link"
            anchorVariation="primary"
            dataLocator="my-rewards-program-details"
            text={getLabelValue(accountLabels, 'lbl_prefrence_program_details', 'preferences')}
          />
          <AnchorLeftMargin>
            <ContentLink
              fontSizeVariation="medium"
              underline
              urlKey="lbl_termcondition_link"
              anchorVariation="primary"
              dataLocator="my-rewards-tnc"
              text={getLabelValue(accountLabels, 'lbl_prefrence_term_codition', 'preferences')}
            />
          </AnchorLeftMargin>
        </StyledAnchorWrapper>
      </ViewWithSpacing>
    </ScrollView>
  );
};

PointsHistoryPageView.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  richTextContent: PropTypes.string,
  accountLabels: PropTypes.shape({}).isRequired,
  navigation: PropTypes.shape({}).isRequired,
};

PointsHistoryPageView.defaultProps = {
  richTextContent: '',
};

export default PointsHistoryPageView;
