// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { PointsHistoryPageContainer } from '../PointsHistoryPage.container';

describe('PointsHistoryPage container', () => {
  it('should render correctly', () => {
    const labels = {};
    const tree = shallow(<PointsHistoryPageContainer labels={labels} />);
    expect(tree).toMatchSnapshot();
  });
});
