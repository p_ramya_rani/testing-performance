// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';
import { getImageFilePath } from '@tcp/core/src/utils';

const CouponDetailModalStyle = css`
  @media ${(props) => props.theme.mediaQuery.medium} {
    margin: 0 54px;
  }
  .earnPointsModal_title {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  }
  .imageSizeSingle {
    width: 70px;
    height: 80px;
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    background-size: contain;
    background-repeat: no-repeat;
  }

  .buttonWrapper {
    margin-left: ${(props) => props.theme.spacing.LAYOUT_SPACING.SM};
    margin-right: ${(props) => props.theme.spacing.LAYOUT_SPACING.SM};
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
    @media ${(props) => props.theme.mediaQuery.medium} {
      margin-left: ${(props) => props.theme.spacing.LAYOUT_SPACING.XL};
      margin-right: ${(props) => props.theme.spacing.LAYOUT_SPACING.XL};
    }
  }

  .earnExtraPointsTileImage {
    height: 90px;
    display: flex;
    justify-content: space-around;
    align-items: center;
  }

  .AppDownload,
  .Gymboree_AppDownload {
    background-image: url('${(props) => getImageFilePath(props)}/app_download.svg');
  }
  .ProductReview,
  .Gymboree_ProductReview {
    background-image: url('${(props) => getImageFilePath(props)}/review.png');
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
  }
  .FacebookLink,
  .Gymboree_FacebookLink {
    background-image: url('${(props) => getImageFilePath(props)}/facebook.png');
  }
  .InstagramLink,
  .Gymboree_InstagramLink {
    background-image: url('${(props) => getImageFilePath(props)}/instagram.png');
  }
  .ChildProfile {
    background-image: url('${(props) => getImageFilePath(props)}/child-birthday-profile.png');
  }
  .SMSOptIn,
  .Gymboree_SMSOptIn {
    background-image: url('${(props) => getImageFilePath(props)}/sms.svg');
  }
  .AddMailingAddress {
    background-image: url('${(props) => getImageFilePath(props)}/mailingAddress.svg');
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
  }
  .AddFavoriteStore {
    background-image: url('${(props) => getImageFilePath(props)}/store.svg');
  }
  .AddShopperType {
    background-image: url('${(props) => getImageFilePath(props)}/feedback_survey.svg');
  }
  .AddBirthDate {
    background-image: url('${(props) => getImageFilePath(props)}/birthdays.svg');
  }

  .FeedbackSurvey {
    background-image: url('${(props) => getImageFilePath(props)}/oval.svg');
  }

  .TwitterLink,
  .Gymboree_TwitterLink {
    background-image: url('${(props) => getImageFilePath(props)}/twitter.png');
  }
  .TwitterPhoto,
  .Gymboree_TwitterPhoto {
    background-image: url('${(props) => getImageFilePath(props)}/share-twitter.png');
  }
`;

export default CouponDetailModalStyle;
