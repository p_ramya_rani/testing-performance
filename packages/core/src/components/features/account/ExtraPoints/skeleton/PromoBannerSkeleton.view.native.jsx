// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import LoaderSkelton from '@tcp/core/src/components/common/molecules/LoaderSkelton';

const PromoBannerSkeleton = () => {
  return <LoaderSkelton height="280px" />;
};

export default PromoBannerSkeleton;
