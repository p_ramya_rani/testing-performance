// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import PromoBannerSkeleton from '../PromoBannerSkeleton.view';

describe('PromoBannerSkeleton component', () => {
  it('should renders correctly', () => {
    const props = {
      className: 'sample-class',
    };
    const component = shallow(<PromoBannerSkeleton {...props} />);
    expect(component).toMatchSnapshot();
  });
});
