// 9fbef606107a605d69c0edbcd8029e5d 
const AppDownloadImage = require('../../../../../../mobileapp/src/assets/images/download-app.png');
const ProductReviewImage = require('../../../../../../mobileapp/src/assets/images/review.png');
const FacebookLinkImage = require('../../../../../../mobileapp/src/assets/images/facebook.png');
const InstagramLinkImage = require('../../../../../../mobileapp/src/assets/images/instagram.png');
const ChildProfileImage = require('../../../../../../mobileapp/src/assets/images/child-birthday-profile.png');
const SMSOptInImage = require('../../../../../../mobileapp/src/assets/images/sms.png');
const AddMailingAddressImage = require('../../../../../../mobileapp/src/assets/images/mailingAddress.png');
const AddFavoriteStoreImage = require('../../../../../../mobileapp/src/assets/images/store.png');
const AddShopperTypeImage = require('../../../../../../mobileapp/src/assets/images/survey.png');
const AddBirthDateImage = require('../../../../../../mobileapp/src/assets/images/birthday.png');
const FeedbackSurveyImage = require('../../../../../../mobileapp/src/assets/images/oval.png');
const TwitterLinkImage = require('../../../../../../mobileapp/src/assets/images/twitter.png');
const TwitterPhotoImage = require('../../../../../../mobileapp/src/assets/images/share-twitter.png');

/**
 * @sourceMap - sourceMap object for images path
 */

const sourceMap = {
  AppDownload: AppDownloadImage,
  Gymboree_AppDownload: AppDownloadImage,
  ProductReview: ProductReviewImage,
  Gymboree_ProductReview: ProductReviewImage,
  FacebookLink: FacebookLinkImage,
  Gymboree_FacebookLink: FacebookLinkImage,
  InstagramLink: InstagramLinkImage,
  Gymboree_InstagramLink: InstagramLinkImage,
  ChildProfile: ChildProfileImage,
  SMSOptIn: SMSOptInImage,
  Gymboree_SMSOptIn: SMSOptInImage,
  AddMailingAddress: AddMailingAddressImage,
  AddFavoriteStore: AddFavoriteStoreImage,
  AddShopperType: AddShopperTypeImage,
  AddBirthDate: AddBirthDateImage,
  FeedbackSurvey: FeedbackSurveyImage,
  TwitterLink: TwitterLinkImage,
  Gymboree_TwitterLink: TwitterLinkImage,
  TwitterPhoto: TwitterPhotoImage,
  Gymboree_TwitterPhoto: TwitterPhotoImage,
};

export default sourceMap;
