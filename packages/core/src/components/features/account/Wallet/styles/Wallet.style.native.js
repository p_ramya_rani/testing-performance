// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

const WalletLayout = styled.View`
  display: flex;
  margin-top: 19px;
  margin-right: 14px;
  margin-bottom: 19px;
  margin-left: 14px;
`;

export default WalletLayout;
