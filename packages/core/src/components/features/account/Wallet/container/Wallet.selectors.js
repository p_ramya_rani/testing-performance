// 9fbef606107a605d69c0edbcd8029e5d 
import constants from '../Wallet.constants';

export const getAccountOverviewLabels = labels => {
  return (labels && labels.accountOverview) || {};
};

export const getWalletFooterLinks = state => state.SubNavigation[constants.WALLET_FOOTER_LINKS];

export default getAccountOverviewLabels;
