// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { ResetPasswordForm } from '../ResetPasswordForm.view';

describe('ResetPasswordForm component', () => {
  it('should renders correctly in initial state', () => {
    const props = {
      labels: {},
      pristine: false,
      successMessage: '',
      errorMessage: '',
      showNotification: false,
      isFormSubmitting: false,
      handleSubmit: jest.fn(),
      resetPasswordErrorMessage: '',
    };
    const component = shallow(<ResetPasswordForm {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders correctly with success and error', () => {
    const props = {
      labels: {},
      pristine: false,
      successMessage: 'success',
      errorMessage: 'error',
      showNotification: false,
      isFormSubmitting: false,
      handleSubmit: jest.fn(),
      resetPasswordErrorMessage: '',
    };
    const component = shallow(<ResetPasswordForm {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should call when hovering mouse down', () => {
    const props = {
      labels: {},
      pristine: false,
      successMessage: 'success',
      errorMessage: 'error',
      showNotification: false,
      isFormSubmitting: false,
      handleSubmit: jest.fn(),
      resetPasswordErrorMessage: '',
    };
    const component = shallow(<ResetPasswordForm {...props} />);
    const wrapper = component.find("[dataLocator='login-logincta']");
    expect(wrapper.length).toBe(1);
    wrapper.simulate('mousedown', { preventDefault: jest.fn() });
  });

  it('increment counter correctlry', () => {
    const props = {
      labels: {},
      pristine: false,
      successMessage: 'success',
      errorMessage: 'error',
      showNotification: false,
      isFormSubmitting: false,
      handleSubmit: jest.fn(),
      resetPasswordErrorMessage: '',
    };
    const setPasswordFieldFocus = jest.fn();
    const setPassword = jest.fn();
    const useStateSpy = jest.spyOn(React, 'useState');
    const component = shallow(<ResetPasswordForm {...props} />);
    const wrapper = component.find("[dataLocator='login-passwordfield']").at(0);
    expect(wrapper.length).toBe(1);
    wrapper.simulate('focus');
    useStateSpy.mockImplementation((init) => [init, setPasswordFieldFocus]);
    wrapper.simulate('change');
    useStateSpy.mockImplementation((init) => [init, setPassword]);
    wrapper.simulate('blur');
    useStateSpy.mockImplementation((init) => [init, setPasswordFieldFocus]);
  });

  it('should submit the form', () => {
    const handleSubmit = { preventDefault: () => {} };
    const props = {
      labels: {},
      pristine: false,
      successMessage: 'success',
      errorMessage: 'error',
      showNotification: false,
      isFormSubmitting: false,
      handleSubmit: jest.fn(),
      resetPasswordErrorMessage: '',
    };
    const component = shallow(<ResetPasswordForm {...props} />);
    expect(component.find("[name='ResetPasswordForm']").length).toBe(1);
    component.find("[name='ResetPasswordForm']").simulate('submit', handleSubmit);
  });
});
