// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const style = css`
  width: 320px;
  margin: 0 auto;
  padding: 24px 0;
  .email-value {
    line-height: 18px;
    font-weight: ${(props) => props.theme.fonts.fontWeight.semiBold};
  }
  .email-label {
    line-height: 18px;
    color: ${(props) => props.isResetViaText && props.theme.colorPalette.blue.C900};
  }
`;

export default style;
