// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';
import { getIconPath } from '@tcp/core/src/utils';

const style = css`
  .password-field-focus {
    margin-bottom: 0;
  }
  &.full-page-form {
    width: 320px;
    margin: 0 auto;
    @media ${(props) => props.theme.mediaQuery.large} {
      width: ${(props) => props.isResetViaText && '345px'};
    }
  }
  .resend-otp-cta {
    background: none;
    border: none;
    cursor: pointer;
    outline: none;
    text-decoration: underline;
    color: ${(props) => props.theme.colorPalette.blue.C900};
  }
  .reset-pass-button {
    border-radius: ${(props) => props.isResetViaText && props.theme.spacing.ELEM_SPACING.XS_6};
    margin-top: ${(props) => props.isResetViaText && '15px'};
  }
  .recaptcha-error-wrapper {
    display: flex;
    flex-direction: row;
    padding-bottom: 4px;
    .reset-warning-icon {
      background: transparent url('${(props) => getIconPath('circle-alert-fill', props)}') no-repeat
        0 0;
      background-size: contain;
      border: none;
      height: 14px;
      width: 16px;
      margin-right: 7px;
      flex-shrink: 0;
    }
  }
  ${(props) =>
    props.isResetViaText &&
    `
      .reset-pass-field{
        margin-top: 30px;
        .rightAlignedContent{
          right: 12px;
          top: 18px;
          text-transform: capitalize;
          text-align: right;
          width: 50px;
        }
        .TextBox__input{
          border: 1px solid ${props.theme.colorPalette.blue.C900};
        }
      }
    `}
`;

export default style;
