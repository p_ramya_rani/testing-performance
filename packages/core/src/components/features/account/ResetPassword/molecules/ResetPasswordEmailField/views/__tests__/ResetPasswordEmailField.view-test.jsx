// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { ResetPasswordEmailFieldVanilla } from '../ResetPasswordEmailField.view';

describe('PasswordRequirement component', () => {
  it('should render correctly', () => {
    const props = {
      email: 'test@test.com',
    };
    const component = shallow(<ResetPasswordEmailFieldVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
