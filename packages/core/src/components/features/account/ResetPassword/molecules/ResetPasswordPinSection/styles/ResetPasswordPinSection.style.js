// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const style = css`
  width: ${(props) => (props.isResetViaText ? '316px' : props.theme.spacing.LAYOUT_SPACING.XXXL)};
  margin: 0 auto;
  input {
    font-size: 28px;
    background-color: ${(props) =>
      props.isResetViaText ? props.theme.colors.WHITE : props.theme.colors.PRIMARY.PALEGRAY};
    padding: 14px 4px;
    border-radius: 6px;
    border: 1px solid
      ${(props) =>
        props.isResetViaText
          ? props.theme.colorPalette.blue[200]
          : props.theme.colors.PRIMARY.GRAY};
    outline: 0;
    &:focus {
      border-radius: 6px;
      border: 1px solid
        ${(props) =>
          !props.isResetViaText
            ? props.theme.colors.PRIMARY.GRAY
            : props.theme.colorPalette.blue[200]};
      outline: 0;
    }
  }
  .valid-otp {
    border: 1px solid ${(props) => props.theme.colors.NOTIFICATION.SUCCESS};
  }
  .invalid-otp {
    border: 1px solid ${(props) => props.theme.colors.NOTIFICATION.ERROR};
  }
  .seperator {
    opacity: 0;
    width: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  }
  .error-text {
    color: ${(props) => props.theme.colors.NOTIFICATION.ERROR};
  }
  .enter-code-label {
    line-height: 18px;
    margin-bottom: 6px;
    color: ${(props) => props.isResetViaText && props.theme.colorPalette.blue.C900};
  }
  .otp-container {
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  }
`;

export default style;
