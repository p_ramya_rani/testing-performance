// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import { reduxForm, Field } from 'redux-form';
import get from 'lodash/get';
import RecaptchaModal from '@tcp/core/src/components/common/molecules/recaptcha/recaptchaModal.native';
import constants from '@tcp/core/src/components/features/account/ResetPassword/container/ResetPassword.utils';
import Button from '../../../../../../common/atoms/Button';
import createValidateMethod from '../../../../../../../utils/formValidation/createValidateMethod';
import getStandardConfig from '../../../../../../../utils/formValidation/validatorStandardConfig';
import {
  PasswordWrapper,
  ConfirmPasswordWrapper,
  ConfirmHideShowField,
  ResetPasswordWrapper,
  PasswordIndicatorWrapper,
} from '../styles/ResetPasswordForm.style.native';
import InputCheckbox from '../../../../../../common/atoms/InputCheckbox';
import { BodyCopyWithSpacing } from '../../../../../../common/atoms/styledWrapper';
import PasswordIndicator from '../../../../CreateAccount/molecules/PasswordIndicator/index';
import TextBox from '../../../../../../common/atoms/TextBox';

const showErrorMessage = (message) => {
  return (
    <BodyCopyWithSpacing
      fontSize="fs12"
      fontWeight="semibold"
      textAlign="center"
      color="red.500"
      text={message}
      spacingStyles="margin-bottom-XL"
    />
  );
};

const showSuccessMessage = (message) => (
  <BodyCopyWithSpacing
    fontSize="fs12"
    fontWeight="semibold"
    textAlign="center"
    color="green.500"
    text={message}
    spacingStyles="margin-bottom-XL"
  />
);

const showPasswordIndicator = (passwordValue, labels) => (
  <PasswordIndicatorWrapper>
    <PasswordIndicator passwordValue={passwordValue} labels={labels} />
  </PasswordIndicatorWrapper>
);

const getIsShowError = (resetPasswordErrorMessage, showNotification, isRecaptchaError) =>
  resetPasswordErrorMessage && showNotification && isRecaptchaError;

const ShowErrorMessageRecaptcha = (
  resetPasswordErrorMessage,
  showNotification,
  isRecaptchaError,
  isShowRecaptchaError,
  labels
) => {
  const isShowError = getIsShowError(resetPasswordErrorMessage, showNotification, isRecaptchaError);
  return isShowError || isShowRecaptchaError ? (
    <BodyCopyWithSpacing
      fontSize="fs12"
      fontWeight="semibold"
      textAlign="center"
      color="red.500"
      text={
        isShowRecaptchaError
          ? getLabelValue(labels, 'lbl_resetPassword_check_recaptcha')
          : resetPasswordErrorMessage
      }
      spacingStyles="margin-bottom-XL"
    />
  ) : null;
};

export const ResetPasswordForm = ({
  labels,
  successMessage,
  handleSubmit,
  resetPasswordErrorMessage,
  showNotification,
  onPwdHideShowClick,
  hideShowPwd,
  onConfirmPwdHideShowClick,
  confirmHideShowPwd,
  invalid,
  showRecaptcha,
  isResetPassCodeValidationEnabled,
  setRecaptchaCode,
  onAppBackToLogin,
  otpValue,
  selectedPassResetOption,
  isRecaptchaError,
  isShowRecaptchaError,
  setIsShowRecaptchaError,
}) => {
  const isSmsSelected = selectedPassResetOption === 'sms';
  const [passwordFieldFocus, setPasswordFieldFocus] = useState(false);
  const [passwordValue, setPasswordValue] = useState(null);
  const [resetClicked, setResetClicked] = useState(false);
  const [customError, setCustomError] = useState('');
  const [recaptchaModalMountedState, setRecaptchaModalMountedState] = useState(true);

  const PasswordFocus = () => {
    setPasswordFieldFocus(true);
  };

  const enableResetState = () => {
    if (resetClicked) {
      setResetClicked(false);
    }
    setRecaptchaModalMountedState(true);
  };

  const setRecaptchaModalMountStateMethod = () => {
    setRecaptchaModalMountedState(!recaptchaModalMountedState);
  };

  const onMessage = (event) => {
    if (event && event.nativeEvent.data) {
      const value = get(event, 'nativeEvent.data', '');
      setRecaptchaCode(value);
      setRecaptchaModalMountStateMethod();
    }
  };

  const onResetSubmit = (e) => {
    const { OTP_LENGTH } = constants;
    if (isResetPassCodeValidationEnabled && otpValue.length < OTP_LENGTH) {
      setCustomError(
        getLabelValue(
          labels,
          isSmsSelected
            ? 'lbl_resetPassword_page_check_code_phone'
            : 'lbl_resetPassword_page_check_code'
        )
      );
    } else {
      handleSubmit(e);
      setResetClicked(true);
      // Validate recaptcha for app again, as this is a modal, so need to check captcha validatity again.
      setTimeout(() => {
        if (isResetPassCodeValidationEnabled) {
          setRecaptchaModalMountedState(true);
          setIsShowRecaptchaError(false);
        }
      }, 5000);
      setCustomError('');
    }
  };

  const showRecaptchaView = () => (
    <React.Fragment>
      {showRecaptcha && (
        <RecaptchaModal
          onMessage={onMessage}
          setRecaptchaModalMountedState={recaptchaModalMountedState}
          toggleRecaptchaModal={setRecaptchaModalMountStateMethod}
          onClose={setRecaptchaModalMountStateMethod}
        />
      )}
    </React.Fragment>
  );

  const showPasswordWrapper = () => (
    <PasswordWrapper
      onPress={() => {
        PasswordFocus();
      }}
    >
      <Field
        label={getLabelValue(labels, 'lbl_resetPassword_password')}
        name="password"
        id="password"
        type="text"
        component={TextBox}
        dataLocator="password"
        secureTextEntry={!hideShowPwd}
        onChange={(e, newValue) => {
          setPasswordValue(newValue);
        }}
        onBlur={() => {
          setPasswordFieldFocus(false);
        }}
        onPasswordFocus={() => {
          PasswordFocus();
        }}
      />

      <ConfirmHideShowField>
        <Field
          name="hide-show-pwd"
          component={InputCheckbox}
          dataLocator="hide-show-pwd"
          disabled={false}
          rightText={
            hideShowPwd
              ? getLabelValue(labels, 'lbl_changePassword_hide')
              : getLabelValue(labels, 'lbl_changePassword_show')
          }
          onClick={onPwdHideShowClick}
          hideCheckboxIcon
        />
      </ConfirmHideShowField>
    </PasswordWrapper>
  );

  const showConfirmPasswordWrapper = () => (
    <ConfirmPasswordWrapper>
      <Field
        label={getLabelValue(labels, 'lbl_resetPassword_confirmPassword')}
        name="confirmPassword"
        id="confirmPassword"
        type="text"
        component={TextBox}
        dataLocator="confirmPassword"
        secureTextEntry={!confirmHideShowPwd}
        onChange={() => enableResetState()}
      />
      <ConfirmHideShowField>
        <Field
          name="hide-show-confirm-pwd"
          component={InputCheckbox}
          dataLocator="hide-show-confirm-pwd"
          disabled={false}
          rightText={
            confirmHideShowPwd
              ? getLabelValue(labels, 'lbl_changePassword_hide')
              : getLabelValue(labels, 'lbl_changePassword_show')
          }
          onClick={onConfirmPwdHideShowClick}
          hideCheckboxIcon
        />
      </ConfirmHideShowField>
    </ConfirmPasswordWrapper>
  );

  const showErrorMessageView = () => {
    return (resetPasswordErrorMessage && showNotification) || customError
      ? showErrorMessage(customError || resetPasswordErrorMessage)
      : null;
  };

  useEffect(() => {
    if (successMessage && onAppBackToLogin && isResetPassCodeValidationEnabled) {
      onAppBackToLogin({ fromResetView: true });
    }
    setCustomError('');
  }, [selectedPassResetOption, successMessage]);

  return (
    <ResetPasswordWrapper>
      {successMessage && showSuccessMessage(labels[`lbl_resetPassword_${successMessage}`])}
      {!isRecaptchaError && showErrorMessageView()}
      {showPasswordWrapper()}
      {passwordFieldFocus && showPasswordIndicator(passwordValue, labels)}
      {showConfirmPasswordWrapper()}
      {ShowErrorMessageRecaptcha(
        resetPasswordErrorMessage,
        showNotification,
        isRecaptchaError,
        isShowRecaptchaError,
        labels
      )}
      <Button
        fill="BLUE"
        type="submit"
        buttonVariation="variable-width"
        onPress={onResetSubmit}
        text={getLabelValue(labels, 'lbl_resetPassword_resetCta')}
        disableButton={resetClicked && !invalid && !resetPasswordErrorMessage}
      />

      {isResetPassCodeValidationEnabled && showRecaptchaView()}
    </ResetPasswordWrapper>
  );
};

ResetPasswordForm.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  successMessage: PropTypes.string.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  resetPasswordErrorMessage: PropTypes.string.isRequired,
  otpValue: PropTypes.string,
  showNotification: PropTypes.bool.isRequired,
  onPwdHideShowClick: PropTypes.func,
  hideShowPwd: PropTypes.bool,
  invalid: PropTypes.bool.isRequired,
  showRecaptcha: PropTypes.bool,
  isResetPassCodeValidationEnabled: PropTypes.bool,
  onConfirmPwdHideShowClick: PropTypes.func,
  confirmHideShowPwd: PropTypes.func,
  setRecaptchaCode: PropTypes.func,
  onAppBackToLogin: PropTypes.func,
};

ResetPasswordForm.defaultProps = {
  onPwdHideShowClick: () => {},
  hideShowPwd: false,
  showRecaptcha: false,
  onConfirmPwdHideShowClick: () => {},
  confirmHideShowPwd: false,
  isResetPassCodeValidationEnabled: false,
  setRecaptchaCode: () => {},
  onAppBackToLogin: () => {},
  otpValue: '',
};

const validateMethod = createValidateMethod(getStandardConfig(['password', 'confirmPassword']));

export default reduxForm({
  form: 'ResetPasswordNativeForm', // a unique identifier for this form
  enableReinitialize: true,
  ...validateMethod,
})(ResetPasswordForm);
