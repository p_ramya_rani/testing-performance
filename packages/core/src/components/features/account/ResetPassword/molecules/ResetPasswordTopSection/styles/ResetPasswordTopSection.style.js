// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const style = css`
  &.top-section-wrapper {
    max-width: 360px;
    margin: 0 auto;
  }
  .secondary-header-text {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      text-align: left;
      margin-left: 0;
    }
    font-size: ${(props) => (props.isResetViaText ? props.theme.typography.fontSizes.fs14 : '')};
    margin-top: ${(props) => (props.isResetViaText ? props.theme.spacing.ELEM_SPACING.XS : '')};

    a {
      color: ${(props) => props.theme.colorPalette.gray[900]};
      font-size: ${(props) => (props.isResetViaText ? props.theme.typography.fontSizes.fs14 : '')};
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      padding: ${(props) => props.isResetViaText && '0 31px'};
    }
  }
  .primary-header-text {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      text-align: left;
    }
  }
`;

export default style;
