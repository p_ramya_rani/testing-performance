// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import OtpInput from 'react-otp-input';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import withStyles from '../../../../../../common/hoc/withStyles';
import styles from '../styles/ResetPasswordPinSection.style';
import BodyCopy from '../../../../../../common/atoms/BodyCopy';

const getLabelByResetOption = (isSmsSelected, labels) => {
  return isSmsSelected
    ? getLabelValue(labels, 'lbl_resetPassword_page_check_code_phone')
    : getLabelValue(labels, 'lbl_resetPassword_page_check_code');
};

export const ResetPasswordPinSection = ({
  className,
  setpasswordValidationCode,
  passwordValidationCodeValid,
  labels,
  errorMessage,
  isResetViaText,
  resetPinField,
  setResetPinField,
  isSmsSelected,
  isRecaptchaError,
}) => {
  const [otp, setOtp] = useState('');
  useEffect(() => {
    setpasswordValidationCode(otp);
    if (resetPinField) {
      setOtp('');
      setResetPinField(false);
    }
  }, [otp, resetPinField]);

  const getActiveClassName = () => {
    if (errorMessage && !isRecaptchaError && errorMessage.size > 0) {
      return 'invalid-otp';
    }
    if (otp && otp.length === 0) {
      return '';
    }
    if (otp && otp.length === 6) {
      return 'valid-otp';
    }
    if (!passwordValidationCodeValid) {
      return 'invalid-otp';
    }
    return '';
  };

  return (
    <div id="resetpassword" className={className}>
      <BodyCopy component="p" fontSize="fs14" fontWeight="bold" className="enter-code-label">
        {isResetViaText
          ? getLabelValue(labels, 'lbl_verify_otp')
          : getLabelValue(labels, 'lbl_resetPassword_page_enter_code')}
      </BodyCopy>
      <OtpInput
        value={otp}
        onChange={setOtp}
        numInputs={6}
        separator={<span className="seperator">-</span>}
        inputStyle={getActiveClassName()}
        containerStyle="otp-container"
        isInputNum
        shouldAutoFocus
      />
      {!passwordValidationCodeValid && (
        <BodyCopy component="p" fontSize="fs12" fontWeight="bold" className="error-text elem-mb-XL">
          {getLabelByResetOption(isSmsSelected, labels)}
        </BodyCopy>
      )}
    </div>
  );
};

ResetPasswordPinSection.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  className: PropTypes.string,
  setpasswordValidationCode: PropTypes.func.isRequired,
  passwordValidationCodeValid: PropTypes.bool.isRequired,
  errorMessage: PropTypes.string.isRequired,
  isRecaptchaError: PropTypes.bool,
};

ResetPasswordPinSection.defaultProps = {
  className: '',
  isRecaptchaError: false,
};

export default withStyles(ResetPasswordPinSection, styles);
export { ResetPasswordPinSection as ResetPasswordPinSectionVanilla };
