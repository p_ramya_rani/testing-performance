// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

const leftAignWrapper = () => {
  return `
    align-items:flex-start;
    margin-bottom:20px;
    flex-direction: row;
  `;
};

const FloatWrapper = styled.View`
  ${leftAignWrapper}
`;

const CustomIconWrapper = styled.TouchableOpacity`
  align-self: center;
  margin-right: ${props => props.theme.spacing.ELEM_SPACING.XXXS};
`;

const PasswordDescriptionWrapper = styled.View`
  margin: 0px ${props => props.theme.spacing.ELEM_SPACING.MED};
`;
const StyledTouchableOpacity = styled.TouchableOpacity`
  align-self: center;
  align-items: flex-end;
  flex: 1;
`;
const StyledCrossImage = styled.Image`
  width: ${props => props.theme.spacing.ELEM_SPACING.MED};
`;
export {
  CustomIconWrapper,
  FloatWrapper,
  PasswordDescriptionWrapper,
  StyledTouchableOpacity,
  StyledCrossImage,
};
