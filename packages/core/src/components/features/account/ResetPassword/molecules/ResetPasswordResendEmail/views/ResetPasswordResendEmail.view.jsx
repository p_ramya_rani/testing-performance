// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '../../../../../../common/hoc/withStyles';
import styles from '../styles/ResetPasswordResendEmail.style';

export const ResetPasswordResendEmail = ({
  className,
  resendEmail,
  lblCodeNotReceived,
  lblResentCode,
}) => {
  return (
    <div className={className}>
      <p className="resend-code-text">
        {lblCodeNotReceived}
        <button onClick={resendEmail} className="resent-code-cta">
          {lblResentCode}
        </button>
      </p>
    </div>
  );
};

ResetPasswordResendEmail.propTypes = {
  className: PropTypes.string.isRequired,
  resendEmail: PropTypes.func.isRequired,
  labels: PropTypes.shape({}).isRequired,
};

export default withStyles(ResetPasswordResendEmail, styles);
export { ResetPasswordResendEmail as ResetPasswordResendEmailVanilla };
