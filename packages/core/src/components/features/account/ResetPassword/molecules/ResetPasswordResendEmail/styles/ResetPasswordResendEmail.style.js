// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const style = css`
  width: 322px;
  margin: 0 auto;
  button {
    background: none;
    border: none;
    cursor: pointer;
    text-decoration: underline;
    color: ${(props) => props.theme.colorPalette.blue.C900};
    outline: none;
  }
  .resend-code-text {
    font-size: ${(props) =>
      props.isResetViaText
        ? props.theme.typography.fontSizes.fs14
        : props.theme.typography.fontSizes.fs12};
    text-align: center;
    .resent-code-cta {
      padding-left: 4px;
      color: ${(props) => props.theme.colorPalette.gray[900]};
    }
  }
`;

export default style;
