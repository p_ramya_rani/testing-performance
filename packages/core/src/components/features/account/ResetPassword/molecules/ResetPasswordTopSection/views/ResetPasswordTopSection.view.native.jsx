// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import BodyCopy from '../../../../../../common/atoms/BodyCopy';
import Anchor from '../../../../../../common/atoms/Anchor';
import PasswordRequirement from '../../PasswordRequirement';
import CustomIcon from '../../../../../../common/atoms/Icon';
import { ICON_NAME } from '../../../../../../common/atoms/Icon/Icon.constants';
import {
  CustomIconWrapper,
  FloatWrapper,
  PasswordDescriptionWrapper,
  StyledTouchableOpacity,
  StyledCrossImage,
} from '../styles/ResetPasswordTopSection.style.native';

const closeIcon = require('../../../../../../../../../mobileapp/src/assets/images/close.png');

export const ResetPasswordTopSection = ({ labels, onBackClick, onRequestClose, isSmsSelected }) => {
  return (
    <>
      <FloatWrapper>
        <CustomIconWrapper>
          <CustomIcon
            name={ICON_NAME.chevronLeft}
            size="fs14"
            color="blue.800"
            isButton
            onPress={() => onBackClick()}
          />
        </CustomIconWrapper>
        <Anchor
          fontSizeVariation="xlarge"
          anchorVariation="secondary"
          text={getLabelValue(labels, 'lbl_resetPassword_backLogin')}
          onPress={onBackClick}
          className="floatLt"
        />
        <StyledTouchableOpacity
          onPress={onRequestClose}
          accessibilityRole="button"
          accessibilityLabel="close"
        >
          <StyledCrossImage source={closeIcon} />
        </StyledTouchableOpacity>
      </FloatWrapper>
      <BodyCopy
        textAlign="center"
        fontFamily="secondary"
        fontSize="fs16"
        fontWeight="black"
        text={
          isSmsSelected
            ? getLabelValue(labels, 'lbl_resetPassword_otp_sent_to_phone')
            : getLabelValue(labels, 'lbl_resetPassword_heading')
        }
      />
      <PasswordDescriptionWrapper>
        <PasswordRequirement labels={labels} alignCenter />
      </PasswordDescriptionWrapper>
    </>
  );
};

ResetPasswordTopSection.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  onBackClick: PropTypes.func.isRequired,
  onRequestClose: PropTypes.func.isRequired,
  isResetViaText: PropTypes.bool.isRequired,
};

export default ResetPasswordTopSection;
