// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components';

const ConfirmPasswordWrapper = styled.View`
  position: relative;
`;

const PasswordWrapper = styled.TouchableOpacity`
  position: relative;
`;

const PasswordIndicatorWrapper = styled.View`
  position: relative;
  top: -20px;
`;

const HideShowFieldStyle = props =>
  `
  width:42px;
  background: ${props.theme.colorPalette.white};
  height:20px; /* 18px not available in spacing variable*/
  position: absolute;
  right: 0;
  top:18px; /* 18px not available in spacing variable */
  border-bottom-width: 1px;
  border-bottom-color: black;
  `;

const ConfirmHideShowField = styled.View`
  ${HideShowFieldStyle}
`;

const IconContainer = styled.View`
  position: absolute;
  right: ${props => props.theme.spacing.ELEM_SPACING.XS};
  width: ${props => props.theme.spacing.ELEM_SPACING.MED};
`;

const ResetPasswordWrapper = styled.View`
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.XS};
`;

export {
  PasswordWrapper,
  ConfirmPasswordWrapper,
  ConfirmHideShowField,
  IconContainer,
  ResetPasswordWrapper,
  PasswordIndicatorWrapper,
};
