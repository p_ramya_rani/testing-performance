// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import { reduxForm, Field } from 'redux-form';
import BodyCopy from '../../../../../../common/atoms/BodyCopy';
import Button from '../../../../../../common/atoms/Button';
import TextBox from '../../../../../../common/atoms/TextBox';
import withStyles from '../../../../../../common/hoc/withStyles';
import PasswordField from '../../../../common/molecule/PasswordField';
import createValidateMethod from '../../../../../../../utils/formValidation/createValidateMethod';
import getStandardConfig from '../../../../../../../utils/formValidation/validatorStandardConfig';
import PasswordIndicator from '../../../../CreateAccount/molecules/PasswordIndicator';
import Recaptcha from '../../../../../../common/molecules/recaptcha/recaptcha';
import styles from '../styles/ResetPasswordForm.style';

const RESET_PASSWORD_FORM = 'ResetPasswordForm';

const UseFocus = () => {
  const htmlElRef = useRef(null);
  const setFocus = () => {
    const elem = htmlElRef && htmlElRef.current;
    // Set Focus at the end of the text instead of start
    const focusTimeout = setTimeout(() => {
      elem.focus();
      if (elem.setSelectionRange) {
        const { length } = elem.value;
        elem.setSelectionRange(length, length);
      }
      clearTimeout(focusTimeout);
    }, 0);
  };

  return [htmlElRef, setFocus];
};

const showNewCodeSentMessage = (newCodeSent, labels) => {
  return (
    <>
      {newCodeSent && (
        <BodyCopy fontSize="fs12" fontWeight="semibold" color="green.500" className="elem-mb-XL">
          {getLabelValue(labels, 'lbl_resetPassword_new_code_sent')}
        </BodyCopy>
      )}
    </>
  );
};

const showSuccessMessage = (successMessage, labels, fullPageView) => {
  return (
    <>
      {!fullPageView && successMessage && (
        <BodyCopy fontSize="fs12" fontWeight="semibold" color="green.500" className="elem-mb-XL">
          {labels[`lbl_resetPassword_${successMessage}`]}
        </BodyCopy>
      )}
    </>
  );
};

const showErrorMessageOTP = (
  resetPasswordErrorMessage,
  showNotification,
  isRecaptchaError,
  codeExpired,
  resendCode,
  labels
) => {
  return resetPasswordErrorMessage && showNotification && !isRecaptchaError ? (
    <BodyCopy fontSize="fs12" fontWeight="semibold" color="red.500" className="elem-mb-XL">
      {resetPasswordErrorMessage}
      {codeExpired && (
        <button type="button" onClick={resendCode} className="resend-otp-cta">
          {getLabelValue(labels, 'lbl_resetPassword_resend_code')}
        </button>
      )}
    </BodyCopy>
  ) : null;
};

const getIsShowError = (resetPasswordErrorMessage, showNotification, isRecaptchaError) =>
  resetPasswordErrorMessage && showNotification && isRecaptchaError;

const ShowErrorMessageRecaptcha = (
  resetPasswordErrorMessage,
  showNotification,
  isRecaptchaError,
  isShowRecaptchaError,
  labels
) => {
  const isShowError = getIsShowError(resetPasswordErrorMessage, showNotification, isRecaptchaError);
  return isShowError || isShowRecaptchaError ? (
    <div className="recaptcha-error-wrapper">
      <div className={isShowRecaptchaError ? 'reset-warning-icon' : ''} aria-disabled="true" />
      <BodyCopy fontSize="fs12" fontWeight="semibold" color="red.500" className="elem-mb-XL">
        {isShowRecaptchaError
          ? getLabelValue(labels, 'lbl_resetPassword_check_recaptcha')
          : resetPasswordErrorMessage}
      </BodyCopy>
    </div>
  ) : null;
};

const handleFormSubmission = (
  showRecaptcha,
  isTokenValid,
  setIsShowRecaptchaError,
  handleSubmit
) => {
  if (showRecaptcha && !isTokenValid) {
    setIsShowRecaptchaError(true);
  } else {
    setIsShowRecaptchaError(false);
    handleSubmit();
  }
};

export const ResetPasswordForm = ({
  className,
  labels,
  pristine,
  successMessage,
  handleSubmit,
  resetPasswordErrorMessage,
  showNotification,
  isFormSubmitting,
  fullPageView,
  change,
  showRecaptcha,
  codeExpired,
  resetStateAction,
  resendEmail,
  verificationCodeSent,
  setVerificationCodeSent,
  resentMessageThreshold,
  selectedPassResetOption,
  isResetViaText,
  isRecaptchaError,
  isShowRecaptchaError,
  setIsShowRecaptchaError,
}) => {
  const [passwordFieldFocus, setPasswordFieldFocus] = useState(false);
  const [passwordValue, setPasswordValue] = useState(null);
  const [inputRef, setInputFocus] = UseFocus();
  const isSmsSelected = selectedPassResetOption === 'sms';
  const isSmsSelectedClass = isSmsSelected ? 'hide-show-icon' : '';
  const [newCodeSent, setNewCodeSent] = useState(verificationCodeSent);
  const [isTokenValid, setIsTokenValid] = useState(false);

  const recaptchaElem = useRef(null);

  const resetReCaptcha = () => {
    if (recaptchaElem && recaptchaElem.current) {
      recaptchaElem.current.reset();
      setIsTokenValid(false);
      change('recaptchaToken', '');
    }
  };

  const resendCode = () => {
    resetStateAction();
    resendEmail();
    setNewCodeSent(true);
    const timer = setTimeout(() => {
      setNewCodeSent(false);
      setVerificationCodeSent(false);
      clearTimeout(timer);
    }, resentMessageThreshold);
  };

  useEffect(() => {
    if (verificationCodeSent) {
      setNewCodeSent(true);
      const newCodeTimer = setTimeout(() => {
        setNewCodeSent(false);
        setVerificationCodeSent(false);
        clearTimeout(newCodeTimer);
      }, resentMessageThreshold);
    }
    resetReCaptcha();
  }, [verificationCodeSent, resetPasswordErrorMessage]);

  return (
    <form
      name={RESET_PASSWORD_FORM}
      id={RESET_PASSWORD_FORM}
      noValidate
      onSubmit={(e) => {
        e.preventDefault();
        handleFormSubmission(showRecaptcha, isTokenValid, setIsShowRecaptchaError, handleSubmit);
      }}
      className={`${className} ${fullPageView ? 'full-page-form' : ''}`}
    >
      {showSuccessMessage(successMessage, labels, fullPageView)}
      {showErrorMessageOTP(
        resetPasswordErrorMessage,
        showNotification,
        isRecaptchaError,
        codeExpired,
        resendCode,
        labels
      )}
      {showNewCodeSentMessage(newCodeSent, labels)}
      <Field
        id="password"
        placeholder={getLabelValue(labels, 'lbl_resetPassword_password')}
        name="password"
        component={PasswordField}
        dataLocator="login-passwordfield"
        errorDataLocator="login-passworderror"
        showSuccessCheck={false}
        enableSuccessCheck={false}
        className={`reset-pass-field ${isSmsSelectedClass} ${
          passwordFieldFocus ? 'password-field-focus' : `elem-mb-SM`
        }`}
        inputRef={inputRef}
        setInputFocus={setInputFocus}
        onBlur={() => {
          setPasswordFieldFocus(false);
        }}
        onChange={(e, newValue) => {
          setPasswordValue(newValue);
        }}
        onFocus={() => {
          if (!passwordFieldFocus) {
            setPasswordFieldFocus(true);
          }
        }}
        spc={isResetViaText}
      />
      {passwordFieldFocus ? (
        <PasswordIndicator passwordValue={passwordValue} labels={labels} />
      ) : null}

      <Field
        id="confirmPassword"
        placeholder={getLabelValue(labels, 'lbl_resetPassword_confirmPassword')}
        name="confirmPassword"
        component={PasswordField}
        dataLocator="login-passwordfield"
        errorDataLocator="login-passworderror"
        showSuccessCheck={false}
        enableSuccessCheck={false}
        className={`reset-pass-field elem-mb-SM ${isSmsSelectedClass}`}
        spc={isResetViaText}
      />
      <BodyCopy component="div">
        {showRecaptcha && (
          <>
            <Recaptcha
              verifyCallback={(token) => {
                setIsTokenValid(true);
                change('recaptchaToken', token);
              }}
              expiredCallback={() => {
                setIsTokenValid(false);
                change('recaptchaToken', '');
              }}
              ref={recaptchaElem}
            />
            <Field
              component={TextBox}
              type="hidden"
              name="recaptchaToken"
              id="recaptchaToken"
              data-locator="login-recaptchcb"
            />
          </>
        )}
      </BodyCopy>
      {ShowErrorMessageRecaptcha(
        resetPasswordErrorMessage,
        showNotification,
        isRecaptchaError,
        isShowRecaptchaError,
        labels
      )}
      <BodyCopy component="div" textAlign="center" className="elem-mb-LRG">
        <Button
          fill="BLUE"
          type="submit"
          buttonVariation="fixed-width"
          dataLocator="login-logincta"
          fullWidth
          className="elem-mb-XS reset-pass-button"
          disabled={pristine || isFormSubmitting || successMessage}
          onMouseDown={(e) => e.preventDefault()}
          form={RESET_PASSWORD_FORM}
        >
          {getLabelValue(labels, 'lbl_resetPassword_resetCta')}
        </Button>
      </BodyCopy>
    </form>
  );
};

ResetPasswordForm.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  pristine: PropTypes.bool.isRequired,
  successMessage: PropTypes.string.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  className: PropTypes.string,
  resetPasswordErrorMessage: PropTypes.string.isRequired,
  showNotification: PropTypes.bool.isRequired,
  isFormSubmitting: PropTypes.bool.isRequired,
  fullPageView: PropTypes.bool,
  change: PropTypes.func,
  showRecaptcha: PropTypes.bool,
  codeExpired: PropTypes.bool,
  resetStateAction: PropTypes.func,
  resendEmail: PropTypes.func,
  verificationCodeSent: PropTypes.bool,
  setVerificationCodeSent: PropTypes.func,
  resentMessageThreshold: PropTypes.number,
  isRecaptchaError: PropTypes.bool,
  setIsShowRecaptchaError: PropTypes.func,
  isShowRecaptchaError: PropTypes.bool,
};

ResetPasswordForm.defaultProps = {
  className: '',
  fullPageView: false,
  change: () => {},
  showRecaptcha: false,
  codeExpired: false,
  resetStateAction: () => {},
  resendEmail: () => {},
  verificationCodeSent: false,
  setVerificationCodeSent: () => {},
  resentMessageThreshold: 3000,
  isRecaptchaError: false,
  setIsShowRecaptchaError: () => {},
  isShowRecaptchaError: false,
};

const validateMethod = createValidateMethod(getStandardConfig(['password', 'confirmPassword']));

export default reduxForm({
  form: 'ResetPasswordForm', // a unique identifier for this form
  enableReinitialize: true,
  ...validateMethod,
})(withStyles(ResetPasswordForm, styles));
