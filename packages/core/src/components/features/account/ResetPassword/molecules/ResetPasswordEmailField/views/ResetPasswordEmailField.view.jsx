// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { getLabelValue, getMaskedEmail } from '@tcp/core/src/utils/utils';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import withStyles from '../../../../../../common/hoc/withStyles';
import styles from '../styles/ResetPasswordEmailField.style';

export const ResetPasswordEmailField = ({
  className,
  email,
  labels,
  isSmsSelected,
  maskedPhoneNumber,
}) => {
  return (
    <div className={className}>
      <BodyCopy
        component="p"
        fontSize="fs14"
        fontWeight="bold"
        className="email-label"
        color="TEXT.DARK"
      >
        {isSmsSelected
          ? getLabelValue(labels, 'lbl_forgotPassword_phoneNumber')
          : getLabelValue(labels, 'lbl_resetPassword_page_email_label')}
      </BodyCopy>
      <BodyCopy component="p" fontSize="fs18" className="email-value" color="TEXT.DARKERGRAY">
        {isSmsSelected ? maskedPhoneNumber : getMaskedEmail(email)}
      </BodyCopy>
    </div>
  );
};

ResetPasswordEmailField.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  className: PropTypes.string,
  email: PropTypes.string.isRequired,
  isSmsSelected: PropTypes.bool,
  maskedPhoneNumber: PropTypes.bool,
};

ResetPasswordEmailField.defaultProps = {
  className: '',
  isSmsSelected: false,
  maskedPhoneNumber: '',
};

export default withStyles(ResetPasswordEmailField, styles);
export { ResetPasswordEmailField as ResetPasswordEmailFieldVanilla };
