// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import withStyles from '../../../../../../common/hoc/withStyles';
import styles from '../styles/ResetPasswordTopSection.style';
import BodyCopy from '../../../../../../common/atoms/BodyCopy';
import Anchor from '../../../../../../common/atoms/Anchor';
import PasswordRequirement from '../../PasswordRequirement';

const ResetPasswordFullPageTopSection = ({ className, labels, isSmsSelected, isResetViaText }) => {
  const getCustomerServiceLink = () => {
    return (
      <Anchor
        anchorVariation="secondary"
        fontSizeVariation="medium"
        underline
        text={getLabelValue(labels, 'lbl_customer_service')}
        target="_blank"
        to="/help-center/contact-us"
      />
    );
  };
  const subText = () => {
    return isResetViaText ? (
      <>
        {getLabelValue(labels, 'lbl_resetPassword_email_sub_heading')}
        {getCustomerServiceLink()}
      </>
    ) : (
      getLabelValue(labels, 'lbl_resetPassword_page_heading_secondary')
    );
  };
  return (
    <BodyCopy className={`${className} top-section-wrapper`}>
      <BodyCopy
        textAlign="center"
        fontFamily="secondary"
        fontSize="fs28"
        fontWeight="semibold"
        className="primary-header-text"
        color="TEXT.DARK"
      >
        {isSmsSelected
          ? getLabelValue(labels, 'lbl_resetPassword_otp_sent_to_phone')
          : getLabelValue(labels, 'lbl_resetPassword_page_heading')}
      </BodyCopy>
      <BodyCopy
        fontWeight="regular"
        fontFamily="secondary"
        textAlign="center"
        className="elem-mb-SM secondary-header-text reset-pass-sec-text"
        fontSize="fs12"
        color="TEXT.DARKERGRAY"
      >
        {isSmsSelected ? (
          <>
            {`${getLabelValue(labels, 'lbl_otp_sent_to_phone_content1_app')} `}
            {getCustomerServiceLink()}
          </>
        ) : (
          subText()
        )}
      </BodyCopy>
    </BodyCopy>
  );
};

ResetPasswordFullPageTopSection.propTypes = {
  className: PropTypes.string.isRequired,
  labels: PropTypes.shape({}).isRequired,
};

export const ResetPasswordTopSection = ({
  className,
  labels,
  onBack,
  fullPageView,
  isSmsSelected,
  isResetViaText,
}) => {
  if (fullPageView) {
    return (
      <ResetPasswordFullPageTopSection
        className={className}
        labels={labels}
        isSmsSelected={isSmsSelected}
        isResetViaText={isResetViaText}
      />
    );
  }

  return (
    <BodyCopy className={className}>
      <BodyCopy className="elem-mb-LRG">
        <Anchor
          onClick={onBack}
          fontSizeVariation="xlarge"
          anchorVariation="secondary"
          dataLocator="addnewaddress-back"
          className="elem-mb-LRG"
        >
          <span className="left-arrow"> </span>
          {getLabelValue(labels, 'lbl_resetPassword_backLogin')}
        </Anchor>
      </BodyCopy>
      <BodyCopy
        textAlign="center"
        fontFamily="secondary"
        fontSize="fs16"
        fontWeight="black"
        className="elem-mb-XS"
      >
        {getLabelValue(labels, 'lbl_resetPassword_heading')}
      </BodyCopy>
      <BodyCopy component="div" className="password-required-msg">
        <PasswordRequirement labels={labels} />
      </BodyCopy>
    </BodyCopy>
  );
};

ResetPasswordTopSection.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  onBack: PropTypes.func.isRequired,
  className: PropTypes.string,
  fullPageView: PropTypes.bool,
};

ResetPasswordTopSection.defaultProps = {
  className: '',
  fullPageView: false,
};

export default withStyles(ResetPasswordTopSection, styles);
export { ResetPasswordTopSection as ResetPasswordTopSectionVanilla };
