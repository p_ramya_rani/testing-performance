// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { ResetPasswordForm } from '../ResetPasswordForm.view.native';

describe('ResetPasswordForm component', () => {
  const props = {
    labels: {},
    pristine: false,
    successMessage: '',
    errorMessage: '',
    otpValue: '',
    isResetPassCodeValidationEnabled: false,
  };

  it('should renders correctly in initial state', () => {
    const component = shallow(<ResetPasswordForm {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders correctly with success and error', () => {
    const errorProps = {
      labels: {},
      pristine: false,
      successMessage: 'success',
      errorMessage: 'error',
    };
    const component = shallow(<ResetPasswordForm {...errorProps} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders with new OTP View', () => {
    const component = shallow(<ResetPasswordForm {...props} isResetPassCodeValidationEnabled />);
    expect(component).toMatchSnapshot();
  });

  it('should renders with new OTP View with recaptcha', () => {
    const component = shallow(
      <ResetPasswordForm {...props} isResetPassCodeValidationEnabled showRecaptcha />
    );
    expect(component).toMatchSnapshot();
  });
});
