// 9fbef606107a605d69c0edbcd8029e5d
import { call, takeLatest, put, select } from 'redux-saga/effects';
import { getIsSNJEnabled } from '@tcp/core/src/reduxStore/selectors/session.selectors';
import { phoneLookup } from '@tcp/core/src/services/abstractors/account/phoneLookup';
import { ForgotPassword } from '@tcp/core/src/components/features/account/ForgotPassword/container/ForgotPassword.saga';
import constants from '../ResetPassword.constants';
import {
  resetPasswordError,
  resetPasswordSuccess,
  resetPasswordSubmitStart,
  resetPasswordSubmitStop,
} from './ResetPassword.actions';

import { resetPassword } from '../../../../../services/abstractors/account/ResetPassword';
import { forgotPassword } from '../../../../../services/abstractors/account/ForgotPassword';
import {
  getResetPasswordSuccess,
  showPhoneResetOption,
  setMaskedPhoneNumber,
  sendOTPError,
  getResetPasswordFail,
  passResetOptionSelector,
} from '../../ForgotPassword/container/ForgotPassword.actions';

export function* ResetPassword({ payload }) {
  try {
    let body = payload;
    let apiWrapper = resetPassword;
    if (payload.logonIdResend) {
      body = {
        formFlag: 'true',
        isPasswordReset: 'true',
        logonId: payload.logonIdResend,
        reLogonURL: 'ChangePassword',
        target: payload.target,
      };
      apiWrapper = forgotPassword;
      yield call(apiWrapper, body);
      return false;
    }
    yield put(resetPasswordSubmitStart());

    const isSNJEnabled = yield select(getIsSNJEnabled);
    if (isSNJEnabled) {
      body.isSNJEnabled = isSNJEnabled;
    }
    const res = yield call(apiWrapper, body);
    yield put(resetPasswordSubmitStop());
    return yield put(resetPasswordSuccess(res));
  } catch (err) {
    let error;
    /* istanbul ignore else */
    yield put(resetPasswordSubmitStop());

    if (err?.errorResponse?.errors) {
      [error] = err.errorResponse.errors;
    } else if (err?.response) {
      [error] = err.response.body.errors;
    } else if (err?.errorResponse) {
      error = err.errorResponse;
    }

    // Mule api returns errorKey whereas MS api returns errorCode
    // TODO Remove this after completely moved to MS api
    if (error?.errorCode) {
      error.errorKey = error.errorCode;
    } else if (error?.errorKey) {
      error.errorCode = error.errorKey;
    }

    if (error?.errorCode) {
      return yield put(resetPasswordError(error));
    }
    return null;
  }
}

export function* fetchPhoneLookup({ payload }) {
  try {
    const res = yield call(phoneLookup, payload);
    if (!res?.error) {
      if (res?.phoneNumber === '') {
        yield put(passResetOptionSelector('email'));
        return yield call(ForgotPassword, { payload: { ...payload, target: 'email' } });
      }
      yield put(getResetPasswordSuccess({ state: true }));
      yield put(showPhoneResetOption(true));
      return yield put(setMaskedPhoneNumber(res.phoneNumber));
    }
    if (res?.error === 'NOT_FOUND') {
      return yield put(sendOTPError(true));
    }
    return null;
  } catch (err) {
    let error = {};
    error = err;
    if (error && error.errorResponse && error.errorResponse.errors) {
      return yield put(resetPasswordError(error.errorResponse.errors[0]));
    }
    if (error && error.response) {
      return yield put(resetPasswordError(error.response.body.errors[0]));
    }
    if (error && error.errorResponse) {
      return yield put(resetPasswordError(error.errorResponse));
    }
    return yield put(getResetPasswordFail({ state: true }));
  }
}

export function* ResetPasswordSaga() {
  yield takeLatest(constants.RESET_PASSWORD, ResetPassword);
  yield takeLatest(constants.FETCH_PHONE_LOOKUP, fetchPhoneLookup);
}

export default ResetPasswordSaga;
