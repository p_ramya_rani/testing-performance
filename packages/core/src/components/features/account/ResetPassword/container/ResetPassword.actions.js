// 9fbef606107a605d69c0edbcd8029e5d
import constants from '../ResetPassword.constants';

export const resetPassword = (payload) => ({
  type: constants.RESET_PASSWORD,
  payload,
});

export const resetPasswordSuccess = (payload) => ({
  type: constants.RESET_PASSWORD_SUCCESS,
  payload,
});

export const resetPasswordError = (payload) => ({
  type: constants.RESET_PASSWORD_ERROR,
  payload,
});

export const resetState = () => ({
  type: constants.RESET_STATE,
});

export const resetPasswordSubmitStart = () => ({
  type: constants.RESET_PASSWORD_SUBMIT_START,
});

export const resetPasswordSubmitStop = () => ({
  type: constants.RESET_PASSWORD_SUBMIT_STOP,
});

export const fetchPhone = (payload) => ({
  type: constants.FETCH_PHONE_LOOKUP,
  payload,
});
