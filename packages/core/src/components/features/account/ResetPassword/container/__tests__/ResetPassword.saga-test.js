// 9fbef606107a605d69c0edbcd8029e5d
import { put, takeLatest } from 'redux-saga/effects';
import { ResetPassword, ResetPasswordSaga } from '../ResetPassword.saga';
import { resetPasswordSuccess, resetPasswordSubmitStop } from '../ResetPassword.actions';
import constants from '../../ResetPassword.constants';

describe('ResetPassword saga', () => {
  describe('ResetPassword', () => {
    let gen;
    beforeEach(() => {
      gen = ResetPassword({ payload: { resendEmail: false } });
      gen.next();
      gen.next();
      gen.next();
    });

    it('should dispatch resetPasswordSuccess action for success response', () => {
      const response = 'success';
      gen.next(response);
      const putDescriptor = gen.next().value;
      expect(putDescriptor).toEqual(put(resetPasswordSuccess(response)));
    });

    it('should dispatch resetPasswordSubmitStop action if response is error', () => {
      const putDescriptor = gen.throw({ response: { body: { errors: ['test'] } } }).value;
      expect(putDescriptor).toEqual(put(resetPasswordSubmitStop()));
    });
  });

  describe('ResetPasswordSaga', () => {
    it('should return correct takeLatest effect', () => {
      const generator = ResetPasswordSaga();
      const takeLatestDescriptor = generator.next().value;
      const expected = takeLatest(constants.RESET_PASSWORD, ResetPassword);
      expect(takeLatestDescriptor).toEqual(expected);
    });
  });
});
