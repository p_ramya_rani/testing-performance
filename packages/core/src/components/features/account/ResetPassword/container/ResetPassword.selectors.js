// 9fbef606107a605d69c0edbcd8029e5d
import { createSelector } from 'reselect';
import { parseBoolean } from '@tcp/core/src/utils/badge.util';
import { getIsSNJEnabled } from '@tcp/core/src/reduxStore/selectors/session.selectors';
import { RESET_PASSWORD_REDUCER_KEY } from '../../../../../constants/reducer.constants';
import { getErrorSelector } from '../../../../../utils/utils';
import { getIsResetRecaptchaEnabled } from '../../../../../reduxStore/selectors/session.selectors';

const getState = (state) => state[RESET_PASSWORD_REDUCER_KEY];

export const getError = createSelector(getState, (state) => state && state.get('error'));

export const getSuccess = createSelector(getState, (state) => state && state.get('success'));

export const getShowNotificationState = createSelector(
  getState,
  (resp) => resp && resp.get('showNotification')
);

export const getLabels = (state) => state.Labels.global;

export const getResetLabels = createSelector(getLabels, (labels) => labels && labels.password);

export const getResetPasswordErrorMessage = createSelector(
  [getError, getResetLabels],
  (state, labels) => {
    return getErrorSelector(state, labels, 'lbl_resetPassword');
  }
);

export const getIsFormSubmitting = createSelector(
  getState,
  (state) => state && state.get('formSubmitting')
);

export const getResetRecaptchaThresold = (state) => {
  return state.session &&
    state.session.siteDetails &&
    state.session.siteDetails.RESET_PASSWORD_RETRY_THRESHOLD_CAPTCHA
    ? +state.session.siteDetails.RESET_PASSWORD_RETRY_THRESHOLD_CAPTCHA
    : 3;
};

export const showResetRecaptcha = createSelector(
  [getError, getIsResetRecaptchaEnabled, getResetRecaptchaThresold],
  (ErrorState, ResetRecaptchaEnabled, ResetRecaptchaThresold) =>
    ErrorState &&
    ResetRecaptchaEnabled &&
    parseInt(ErrorState.getIn(['errorParameters', 'retriesCount']) || 0, 10) >=
      ResetRecaptchaThresold
);

export const isCodeExpired = createSelector(
  getError,
  (ErrorState) =>
    ErrorState &&
    ErrorState.get(getIsSNJEnabled ? 'errorCode' : 'errorKey') ===
      '_TCP_RESET_PASSWORD_CODE_EXPIRED'
);

export const getResentMessageHideThresold = (state) => {
  return state.session &&
    state.session.siteDetails &&
    state.session.siteDetails.RESET_PASSWORD_MESSAGE_DISPLAY_WINDOW
    ? +state.session.siteDetails.RESET_PASSWORD_MESSAGE_DISPLAY_WINDOW * 1000
    : 3000;
};

export const getResetPassCodeValidationFlow = (state) => {
  return state.session &&
    state.session.siteDetails &&
    state.session.siteDetails.RESET_PASSWORD_CODE_VALIDATION_FLOW_ENABLED
    ? parseBoolean(state.session.siteDetails.RESET_PASSWORD_CODE_VALIDATION_FLOW_ENABLED)
    : false;
};

export const isRecaptchaNotCalled = createSelector(
  getError,
  (ErrorState) =>
    ErrorState &&
    (ErrorState.get('errorCode') === '_TCP_RESET_PASSWORD_RECAPTCHA_NOT_CALLED' ||
      ErrorState.get('errorCode') === 'RECAPTCHA_FAILED' ||
      ErrorState.get('errorCode') === 'DUPLICATE_RECAPTCHA_TOKEN')
);
