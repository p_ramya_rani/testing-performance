// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  getError,
  getSuccess,
  getResetPasswordErrorMessage,
  getShowNotificationState,
  getIsFormSubmitting,
  showResetRecaptcha,
  isCodeExpired,
  getResentMessageHideThresold,
  isRecaptchaNotCalled,
} from './ResetPassword.selectors';
import { getLogonId } from '../../ForgotPassword/container/ForgotPassword.selectors';
import ResetPasswordComponent from '../views';
import { resetPassword, resetState } from './ResetPassword.actions';
import { isMobileApp } from '../../../../../utils';
import constants from '../ResetPassword.constants';

export class ResetPasswordContainer extends PureComponent {
  static propTypes = {
    successMessage: PropTypes.string.isRequired,
    errorMessage: PropTypes.string.isRequired,
    resetStateAction: PropTypes.func.isRequired,
    resetPasswordAction: PropTypes.func.isRequired,
    backToLoginAction: PropTypes.func.isRequired,
    labels: PropTypes.shape({}).isRequired,
    queryParams: PropTypes.shape({
      email: PropTypes.string,
      page: PropTypes.string,
      em: PropTypes.string,
      logonPasswordOld: PropTypes.string,
    }),
    resetPasswordErrorMessage: PropTypes.string.isRequired,
    showNotification: PropTypes.bool.isRequired,
    showNewPassword: PropTypes.func,
    showLogin: PropTypes.func,
    updateHeader: PropTypes.func,
    hideTopSection: PropTypes.bool,
    resetChangePasswordState: PropTypes.func,
    onRequestClose: PropTypes.func,
    onAppBackToLogin: PropTypes.func,
    isFormSubmitting: PropTypes.bool.isRequired,
    fullPageView: PropTypes.bool,
    logonId: PropTypes.string,
    showRecaptcha: PropTypes.bool,
    codeExpired: PropTypes.bool,
    isResetPassCodeValidationEnabled: PropTypes.bool,
    resentMessageThreshold: PropTypes.number,
    showOnPlccModal: PropTypes.bool,
  };

  static defaultProps = {
    showNewPassword: () => {},
    showLogin: () => {},
    updateHeader: () => {},
    resetChangePasswordState: () => {},
    hideTopSection: true,
    onRequestClose: () => {},
    onAppBackToLogin: () => {},
    fullPageView: false,
    logonId: '',
    showRecaptcha: false,
    codeExpired: false,
    queryParams: {
      email: '',
      page: '',
      em: '',
      logonPasswordOld: '',
    },
    isResetPassCodeValidationEnabled: false,
    resentMessageThreshold: 3000,
    showOnPlccModal: false,
  };

  constructor(props) {
    super(props);
    this.state = {
      hideShowPwd: false,
      confirmHideShowPwd: false,
      passwordValidationCode: '',
      passwordValidationCodeValid: 'na',
      recaptchaCode: '',
      isShowRecaptchaError: false,
    };
  }

  componentDidUpdate(prevProps) {
    const { successMessage } = this.props;
    if (successMessage && !prevProps.successMessage) {
      if (isMobileApp()) {
        const backClickTimeout = setTimeout(() => {
          this.onBackClick();
          clearTimeout(backClickTimeout);
        }, 2000);
      } else {
        const backHandlerTimeout = setTimeout(() => {
          this.backHandler();
          clearTimeout(backHandlerTimeout);
        }, 2000);
      }
    }
  }

  componentWillUnmount() {
    const { resetChangePasswordState, resetStateAction } = this.props;
    if (resetChangePasswordState) {
      resetChangePasswordState();
    }
    resetStateAction();
  }

  setIsShowRecaptchaError = (isShowRecaptchaError) => {
    this.setState({ isShowRecaptchaError });
  };

  resetPassword = ({ password, confirmPassword, recaptchaToken }) => {
    const {
      resetPasswordAction,
      queryParams,
      logonId,
      fullPageView,
      resetStateAction,
      isResetPassCodeValidationEnabled,
      selectedPassResetOption,
    } = this.props;
    const { passwordValidationCode, recaptchaCode } = this.state;
    const recaptchaValue = recaptchaToken || recaptchaCode;
    const { em } = queryParams;
    let emailId = logonId;
    if (isMobileApp() && em) {
      emailId = isResetPassCodeValidationEnabled ? logonId : em;
    }
    if (fullPageView && passwordValidationCode.length !== constants.OTP_LENGTH) {
      this.setState({ passwordValidationCodeValid: false });
    } else {
      resetPasswordAction({
        newPassword: password,
        logonPasswordVerify: confirmPassword,
        passwordValidationCode,
        logonId: emailId,
        recaptchaToken: recaptchaValue,
        target: selectedPassResetOption,
        ...queryParams,
      });
    }
    resetStateAction();
  };

  setRecaptchaCode = (code) => {
    this.setState({ recaptchaCode: code });
  };

  backHandler = (e) => {
    if (e) {
      e.preventDefault();
    }
    const { resetStateAction, backToLoginAction } = this.props;
    resetStateAction();
    backToLoginAction();
  };

  onBackClick = () => {
    const { showLogin, showNewPassword } = this.props;
    if (showLogin && showNewPassword) {
      showLogin();
      showNewPassword();
    }
  };

  onPwdHideShowClick = (value) => {
    this.setState({ hideShowPwd: value });
  };

  onConfirmPwdHideShowClick = (value) => {
    this.setState({ confirmHideShowPwd: value });
  };

  setpasswordValidationCode = (value) => {
    this.setState({ passwordValidationCode: value });
    this.setPasswordValidationCodeStatus(value);
  };

  setPasswordValidationCodeStatus = (value) => {
    if (value && value.length === 6) {
      this.setState({ passwordValidationCodeValid: true });
    }
  };

  setPasswordValidationDefaultState = () => {
    this.setState({ passwordValidationCodeValid: 'na' });
  };

  render() {
    const {
      successMessage,
      errorMessage,
      labels,
      resetPasswordErrorMessage,
      showNotification,
      showLogin,
      showNewPassword,
      updateHeader,
      hideTopSection,
      onRequestClose,
      isFormSubmitting,
      fullPageView,
      logonId: email,
      showRecaptcha,
      resetPasswordAction,
      onAppBackToLogin,
      codeExpired,
      isResetPassCodeValidationEnabled,
      resetStateAction,
      resentMessageThreshold,
      showOnPlccModal,
      isResetViaText,
      selectedPassResetOption,
      maskedPhoneNumber,
      toggleIsShowOtpScreen,
      setPassResetOption,
      isRecaptchaError,
    } = this.props;
    const { hideShowPwd, confirmHideShowPwd, passwordValidationCodeValid, isShowRecaptchaError } =
      this.state;
    return (
      <ResetPasswordComponent
        onRequestClose={onRequestClose}
        successMessage={successMessage}
        errorMessage={errorMessage}
        onSubmit={this.resetPassword}
        onBack={this.backHandler}
        labels={labels}
        resetPasswordErrorMessage={resetPasswordErrorMessage}
        showNotification={showNotification}
        showLogin={showLogin}
        showNewPassword={showNewPassword}
        onBackClick={this.onBackClick}
        updateHeader={updateHeader}
        onPwdHideShowClick={this.onPwdHideShowClick}
        onConfirmPwdHideShowClick={this.onConfirmPwdHideShowClick}
        hideShowPwd={hideShowPwd}
        hideTopSection={hideTopSection}
        confirmHideShowPwd={confirmHideShowPwd}
        isFormSubmitting={isFormSubmitting}
        passwordValidationCodeValid={passwordValidationCodeValid}
        setpasswordValidationCode={this.setpasswordValidationCode}
        isResetPassCodeValidationEnabled={isResetPassCodeValidationEnabled}
        fullPageView={fullPageView}
        email={email}
        showRecaptcha={showRecaptcha}
        resetPasswordAction={resetPasswordAction}
        codeExpired={codeExpired}
        resetStateAction={resetStateAction}
        onAppBackToLogin={onAppBackToLogin}
        setRecaptchaCode={this.setRecaptchaCode}
        resentMessageThreshold={resentMessageThreshold}
        showOnPlccModal={showOnPlccModal}
        isResetViaText={isResetViaText}
        selectedPassResetOption={selectedPassResetOption}
        maskedPhoneNumber={maskedPhoneNumber}
        toggleIsShowOtpScreen={toggleIsShowOtpScreen}
        setPassResetOption={setPassResetOption}
        setPasswordValidationDefaultState={this.setPasswordValidationDefaultState}
        isRecaptchaError={isRecaptchaError}
        isShowRecaptchaError={isShowRecaptchaError}
        setIsShowRecaptchaError={this.setIsShowRecaptchaError}
      />
    );
  }
}

export const mapStateToProps = (state, props) => {
  const isRecaptchaError = isRecaptchaNotCalled(state);
  return {
    successMessage: getSuccess(state),
    errorMessage: getError(state),
    resetPasswordErrorMessage: getResetPasswordErrorMessage(state),
    showNotification: getShowNotificationState(state),
    isFormSubmitting: getIsFormSubmitting(state),
    logonId: props.logonId || getLogonId(state),
    showRecaptcha: showResetRecaptcha(state) || isRecaptchaError,
    codeExpired: isCodeExpired(state),
    resentMessageThreshold: getResentMessageHideThresold(state),
    isRecaptchaError,
  };
};

export const mapDispatchToProps = (dispatch) => ({
  resetPasswordAction: (payload) => {
    dispatch(resetPassword(payload));
  },
  resetStateAction: () => {
    dispatch(resetState());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(ResetPasswordContainer);
