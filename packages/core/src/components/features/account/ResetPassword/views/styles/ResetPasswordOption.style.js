// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const style = css`
  .radio-container {
    @media ${(props) => props.theme.mediaQuery.large} {
      padding: 0 31px;
    }
  }
  .rounded-corner {
    border-radius: ${(props) => props.theme.spacing.ELEM_SPACING.XS_6};
  }
  .sms-terms {
    padding: 0 0 ${(props) => props.theme.spacing.ELEM_SPACING.XXL}
      ${(props) => props.theme.spacing.ELEM_SPACING.XXL};
  }
  .sms-terms .CheckBox__input {
    height: 20px;
    width: 20px;
    &:before {
      border: 1px solid #1a1a1a;
      border-radius: 4px;
    }
    &:after {
      left: 8px;
      top: 3px;
    }
  }
`;

export default style;
