// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';

const getPageStyle = (props) => {
  const { theme } = props;
  return `
    margin: 0px ${theme.spacing.APP_LAYOUT_SPACING.XS} 0px ${theme.spacing.APP_LAYOUT_SPACING.XS};
    justify-content: center;
    ${props.showOnPlccModal ? 'margin: 0' : ''};
  `;
};

const ContainerWrapper = styled.View`
  ${getPageStyle}
`;

const ResendCodeWrapper = styled.View`
  display: flex;
  flex-direction: row;
  text-align: center;
  align-items: center;
  justify-content: center;
`;

const OTPWrapper = styled.View`
  margin-left: -${(props) => props.theme.spacing.ELEM_SPACING.XS};
`;

const RadioButtonWrapper = styled.View`
  margin: ${(props) => props.theme.spacing.APP_LAYOUT_SPACING.XS} 0;
  display: flex;
`;

const RadioButtonWrapperInner = styled.View`
  display: flex;
  margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
`;

const SmsConsentWrapper = styled.View`
  padding-right: ${(props) => props.theme.spacing.ELEM_SPACING.XXXL};
`;

const PassSubTextWrapper = styled.Text`
  text-align: left;
  width: 100%;
  margin-bottom: 0px;
  font-size: ${(props) => props.theme.typography.fontSizes.fs13};
  font-weight: ${(props) => props.theme.fonts.fontWeight.medium};
`;

const AnchorTextWrapper = styled.Text`
  text-decoration: underline;
`;

export {
  ContainerWrapper,
  ResendCodeWrapper,
  OTPWrapper,
  RadioButtonWrapper,
  RadioButtonWrapperInner,
  SmsConsentWrapper,
  PassSubTextWrapper,
  AnchorTextWrapper,
};
