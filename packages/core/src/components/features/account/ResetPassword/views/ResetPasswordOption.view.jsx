// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import LabeledRadioButton from '@tcp/core/src/components/common/atoms/LabeledRadioButton';
import { getLabelValue, getMaskedEmail } from '@tcp/core/src/utils/utils';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import InputCheckbox from '@tcp/core/src/components/common/atoms/InputCheckbox';
import { Button, BodyCopy } from '../../../../common/atoms';
import styles from './styles/ResetPasswordOption.style';

export const ResetPasswordOption = ({
  labels,
  setPassResetOption,
  selectedPassResetOption,
  isShowPhoneOption,
  triggerPassResetApi,
  email,
  maskedPhoneNumber,
  className,
  isFormSubmitting,
  showSmsTerm,
}) => {
  const [isSmsConsentChecked, setIsSmsConsentChecked] = useState(false);
  const handleOnChange = () => {
    setIsSmsConsentChecked(!isSmsConsentChecked);
  };

  const getSelectedResetOption = (option) => {
    return selectedPassResetOption === option;
  };

  const handleFormSubmit = () => {
    triggerPassResetApi({ target: selectedPassResetOption });
  };

  const shouldDisabled = (selectedOption, formSubmitting) => {
    if (!selectedOption || (selectedOption === 'sms' && !isSmsConsentChecked) || formSubmitting) {
      return true;
    }
    return false;
  };

  return (
    <div className={`${className} pass-reset-option-container`}>
      <BodyCopy
        textAlign="center"
        fontFamily="secondary"
        fontSize="fs28"
        fontWeight="semibold"
        color="TEXT.DARK"
        className="primary-header-text"
      >
        <span className="forgot-password-text">
          {getLabelValue(labels, 'lbl_forgotPassword_content1', 'password')}
        </span>
      </BodyCopy>
      <BodyCopy
        fontWeight="regular"
        fontFamily="secondary"
        textAlign="center"
        className="elem-mb-SM secondary-header-text"
        fontSize="fs12"
        color="TEXT.DARKERGRAY"
      >
        {getLabelValue(labels, 'lbl_resetPassword_content1', 'password')}
      </BodyCopy>
      <div className="radio-container">
        <BodyCopy component="div" className="elem-mb-LRG hide-on-mobile">
          <Field
            component={LabeledRadioButton}
            name="passResetOption"
            checked={getSelectedResetOption('email')}
            title={getLabelValue(labels, 'lbl_forgotPassword_emailAddress', 'password')}
            subtitle={getMaskedEmail(email)}
            className="pass-reset-options"
            onClick={() => setPassResetOption('email')}
          />
        </BodyCopy>

        {isShowPhoneOption && (
          <BodyCopy component="div" className="elem-mb-LRG">
            <Field
              component={LabeledRadioButton}
              name="passResetOption"
              checked={getSelectedResetOption('sms')}
              title={getLabelValue(labels, 'lbl_forgotPassword_phoneNumber', 'password')}
              subtitle={maskedPhoneNumber}
              className="pass-reset-options"
              onClick={() => setPassResetOption('sms')}
            />
          </BodyCopy>
        )}

        {isShowPhoneOption && (selectedPassResetOption === 'sms' || showSmsTerm) && (
          <BodyCopy component="div" className="sms-terms">
            <Field
              name="sms_checkbox"
              component={InputCheckbox}
              dataLocator="sms_checkbox-locator"
              checked={isSmsConsentChecked}
              onChange={handleOnChange}
            >
              <BodyCopy fontFamily="secondary" tag="span" fontSize="fs12">
                {getLabelValue(labels, 'lbl_sms_consent', 'password')}
              </BodyCopy>
            </Field>
          </BodyCopy>
        )}

        <Button
          fill="BLUE"
          type="submit"
          disabled={shouldDisabled(selectedPassResetOption, isFormSubmitting)}
          buttonVariation="fixed-width"
          onClick={() => handleFormSubmit()}
          className="rounded-corner"
        >
          {getLabelValue(labels, 'lbl_forgotPassword_page_next_btn', 'password')}
        </Button>
      </div>
    </div>
  );
};

ResetPasswordOption.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  setPassResetOption: PropTypes.func,
  selectedPassResetOption: PropTypes.string,
  isShowPhoneOption: PropTypes.bool,
  triggerPassResetApi: PropTypes.func,
  email: PropTypes.string,
  maskedPhoneNumber: PropTypes.bool,
};

ResetPasswordOption.defaultProps = {
  setPassResetOption: () => {},
  selectedPassResetOption: '',
  isShowPhoneOption: false,
  triggerPassResetApi: () => {},
  email: '',
  maskedPhoneNumber: '',
};

export default withStyles(ResetPasswordOption, styles);
export { ResetPasswordOption as ResetPasswordOptionVanilla };
