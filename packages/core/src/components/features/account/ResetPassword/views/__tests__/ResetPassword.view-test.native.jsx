// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { ResetPassword } from '../ResetPassword.view.native';

describe('ResetPassword component', () => {
  const props = {
    labels: {
      lbl_ResetPassword_backLogin: 'back to login',
      lbl_ResetPassword_requirementNote: 'note:',
      lbl_ResetPassword_requirementTips_1: 'requirement tip 1',
      lbl_ResetPassword_requirementTips_2: 'requirement tip 2',
      lbl_ResetPassword_requirementTips_3: 'requirement tip 3',
    },
    successMessage: 'SUCCESS',
    errorMessage: 'ERROR',
    onSubmit: () => {},
    onBack: () => {},
    updateHeader: jest.fn(),
    isResetPassCodeValidationEnabled: true,
    resentMessageThreshold: 3000,
    setpasswordValidationCode: jest.fn(),
    resetPasswordAction: jest.fn(),
    setRecaptchaCode: jest.fn(),
    onAppBackToLogin: jest.fn(),
    email: 'sapdev@yopmail.com',
  };
  it('should renders correctly', () => {
    const component = shallow(<ResetPassword {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('should renders correctly with otp view', () => {
    const component = shallow(<ResetPassword {...props} isResetPassCodeValidationEnabled />);
    expect(component).toMatchSnapshot();
  });
});
