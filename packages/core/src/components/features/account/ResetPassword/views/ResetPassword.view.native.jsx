// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { Text, Linking } from 'react-native';
import PropTypes from 'prop-types';
import {
  BodyCopyWithSpacing,
  ViewWithSpacing,
} from '@tcp/core/src/components/common/atoms/styledWrapper';
import { getLabelValue, getMaskedEmail } from '@tcp/core/src/utils/utils';
import constants from '@tcp/core/src/components/features/account/ResetPassword/container/ResetPassword.utils';
import ResetPasswordTopSection from '../molecules/ResetPasswordTopSection';
import ResetPasswordForm from '../molecules/ResetPasswordForm';
import LineComp from '../../../../common/atoms/Line';
import { Anchor } from '../../../../common/atoms';
import { getHelpCenterPageUrl } from '../../../../../utils/utils.app';
import {
  ContainerWrapper,
  ResendCodeWrapper,
  OTPWrapper,
  PassSubTextWrapper,
  AnchorTextWrapper,
} from './styles/ResetPassword.style.native';
import { OTPInput } from '../../OTPInput/views/OTPInput.view.native';

export class ResetPassword extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      newCodeSent: false,
      otpValue: '',
      resetPinField: false,
    };
  }

  resendEmail = (isSmsSelected) => {
    const { labels, maskedPhoneNumber } = this.props;
    return (
      <>
        <ViewWithSpacing spacingStyles="margin-top-XXL margin-bottom-MED">
          <ResendCodeWrapper>
            <BodyCopyWithSpacing
              textAlign="left"
              fontFamily="secondary"
              fontSize="fs12"
              text={
                isSmsSelected
                  ? getLabelValue(labels, 'lbl_phone_otp_not_received_content')
                  : getLabelValue(labels, 'lbl_resetPassword_code_not_received', 'password')
              }
              spacingStyles="margin-right-XXS"
            />
            <Anchor
              anchorVariation="secondary"
              fontSizeVariation="medium"
              fontSize="fs12"
              underline
              text={
                isSmsSelected
                  ? getLabelValue(labels, 'lbl_resend_it')
                  : getLabelValue(labels, 'lbl_resetPassword_resend_code', 'password')
              }
              onPress={this.resendCode}
            />
          </ResendCodeWrapper>
        </ViewWithSpacing>
        {isSmsSelected ? (
          <ViewWithSpacing spacingStyles="margin-top-SM margin-bottom-MED">
            <ResendCodeWrapper>
              <BodyCopyWithSpacing
                textAlign="left"
                fontFamily="secondary"
                fontSize="fs12"
                text={getLabelValue(labels, 'lbl_didnt_receive_otp_sent_to_phone')}
                spacingStyles="margin-right-XXS"
              />
              <Anchor
                anchorVariation="secondary"
                fontSizeVariation="medium"
                fontSize="fs12"
                underline
                text={getLabelValue(labels, 'lbl_send_code_via_email')}
                onPress={() => this.resendCodeViaOption('email')}
              />
            </ResendCodeWrapper>
          </ViewWithSpacing>
        ) : (
          <>
            {maskedPhoneNumber !== '' && (
              <ViewWithSpacing spacingStyles="margin-top-SM margin-bottom-MED">
                <ResendCodeWrapper>
                  <BodyCopyWithSpacing
                    textAlign="left"
                    fontFamily="secondary"
                    fontSize="fs12"
                    text={getLabelValue(labels, 'lbl_didnt_receive_otp_sent_to_phone')}
                    spacingStyles="margin-right-XXS"
                  />
                  <Anchor
                    anchorVariation="secondary"
                    fontSizeVariation="medium"
                    fontSize="fs12"
                    underline
                    text={getLabelValue(labels, 'lbl_send_code_via_phone')}
                    onPress={() => this.resendCodeViaOption('sms')}
                  />
                </ResendCodeWrapper>
              </ViewWithSpacing>
            )}
          </>
        )}
      </>
    );
  };

  sentNewCodeEmail = () => {
    const { labels } = this.props;
    return (
      <ViewWithSpacing spacingStyles="margin-top-XXL margin-bottom-MED">
        <ResendCodeWrapper>
          <BodyCopyWithSpacing
            textAlign="left"
            fontFamily="secondary"
            fontSize="fs12"
            text={getLabelValue(labels, 'lbl_resetPassword_new_code_sent', 'password')}
          />
        </ResendCodeWrapper>
      </ViewWithSpacing>
    );
  };

  resendCode = () => {
    const { resetPasswordAction, email, resentMessageThreshold, selectedPassResetOption } =
      this.props;
    resetPasswordAction({
      logonIdResend: email.toUpperCase().trim(),
      target: selectedPassResetOption,
    });
    this.setState({ newCodeSent: true });
    setTimeout(() => {
      this.setState({ newCodeSent: false });
    }, resentMessageThreshold);
  };

  resendCodeViaOption = (option) => {
    const { resetPasswordAction, email, resentMessageThreshold, setPassResetOption } = this.props;
    setPassResetOption(option);
    resetPasswordAction({
      logonIdResend: email.toUpperCase().trim(),
      target: option,
    });
    this.setState({ newCodeSent: true, resetPinField: true });
    setTimeout(() => {
      this.setState({ newCodeSent: false });
    }, resentMessageThreshold);
  };

  handleResetPinField = (payload) => {
    this.setState({ resetPinField: payload });
  };

  getOTPValue = (value) => {
    const { setpasswordValidationCode } = this.props;
    const { OTP_LENGTH } = constants;
    const { otpValue } = this.state;
    if (value && value.length === OTP_LENGTH) {
      setpasswordValidationCode(value);
      this.setState({ otpValue: value });
    } else if (value.length < OTP_LENGTH && otpValue.length === OTP_LENGTH) {
      this.setState({ otpValue: value });
    }
  };

  getCustomerServiceLink = () => {
    const { labels, isResetViaText } = this.props;
    return (
      <>
        {isResetViaText && (
          <AnchorTextWrapper>
            <Text onPress={() => Linking.openURL(getHelpCenterPageUrl())}>
              {getLabelValue(labels, 'lbl_customer_service')}
            </Text>
          </AnchorTextWrapper>
        )}
      </>
    );
  };

  showSuccessfullEmail = (isSmsSelected) => {
    const { labels, email, maskedPhoneNumber, isResetViaText } = this.props;
    const { resetPinField } = this.state;
    return (
      <React.Fragment>
        <BodyCopyWithSpacing
          textAlign="left"
          fontFamily="secondary"
          fontSize="fs28"
          text={
            isSmsSelected
              ? getLabelValue(labels, 'lbl_resetPassword_otp_sent_to_phone')
              : getLabelValue(labels, 'lbl_resetPassword_page_heading', 'password')
          }
          spacingStyles="margin-bottom-SM margin-top-LRG"
        />
        {isSmsSelected ? (
          <>
            <PassSubTextWrapper>
              <Text>
                {`${getLabelValue(labels, 'lbl_otp_sent_to_phone_content1_app', 'password')} `}
                {this.getCustomerServiceLink()}
              </Text>
            </PassSubTextWrapper>
          </>
        ) : (
          <>
            <PassSubTextWrapper>
              <Text>
                {isResetViaText
                  ? getLabelValue(labels, 'lbl_resetPassword_email_sub_heading', 'password')
                  : getLabelValue(labels, 'lbl_resetPassword_email_received', 'password')}
                {this.getCustomerServiceLink()}
              </Text>
            </PassSubTextWrapper>
            {!isResetViaText && (
              <BodyCopyWithSpacing
                textAlign="left"
                fontFamily="secondary"
                fontSize="fs13"
                text={getLabelValue(labels, 'lbl_resetPassword_check_junk_email', 'password')}
              />
            )}
          </>
        )}

        <BodyCopyWithSpacing
          textAlign="left"
          fontFamily="secondary"
          fontSize="fs14"
          text={
            isSmsSelected
              ? getLabelValue(labels, 'lbl_forgotPassword_phoneNumber', 'password')
              : getLabelValue(labels, 'lbl_forgotPassword_emailAddress', 'password')
          }
          spacingStyles="margin-top-LRG"
        />
        <BodyCopyWithSpacing
          textAlign="left"
          fontFamily="secondary"
          fontSize="fs18"
          text={isSmsSelected ? maskedPhoneNumber : getMaskedEmail(email)}
        />
        <BodyCopyWithSpacing
          textAlign="left"
          fontFamily="secondary"
          fontSize="fs14"
          text={
            isSmsSelected
              ? getLabelValue(labels, 'lbl_verify_otp')
              : getLabelValue(labels, 'lbl_resetPassword_page_enter_code')
          }
          spacingStyles="margin-top-LRG"
        />
        <OTPWrapper>
          <OTPInput
            onChange={this.getOTPValue}
            resetPinField={resetPinField}
            handleResetPinField={this.handleResetPinField}
          />
        </OTPWrapper>
      </React.Fragment>
    );
  };

  render() {
    const {
      labels,
      onBackClick,
      updateHeader,
      successMessage,
      errorMessage,
      onSubmit,
      showNotification,
      resetPasswordErrorMessage,
      onPwdHideShowClick,
      onConfirmPwdHideShowClick,
      hideShowPwd,
      confirmHideShowPwd,
      onRequestClose,
      setRecaptchaCode,
      showRecaptcha,
      isResetPassCodeValidationEnabled,
      onAppBackToLogin,
      showOnPlccModal,
      selectedPassResetOption,
      isResetViaText,
      isRecaptchaError,
      isShowRecaptchaError,
      setIsShowRecaptchaError,
    } = this.props;
    const { newCodeSent, otpValue } = this.state;
    const isSmsSelected = selectedPassResetOption === 'sms';

    if (updateHeader) updateHeader('change-password'); // hide the title and rule
    return (
      <ContainerWrapper showOnPlccModal={showOnPlccModal}>
        {!isResetPassCodeValidationEnabled && (
          <ResetPasswordTopSection
            onRequestClose={onRequestClose}
            labels={labels}
            onBackClick={onBackClick}
            isSmsSelected={isSmsSelected}
            isResetViaText={isResetViaText}
          />
        )}
        {isResetPassCodeValidationEnabled && this.showSuccessfullEmail(isSmsSelected)}
        <ResetPasswordForm
          labels={labels}
          successMessage={successMessage}
          errorMessage={errorMessage}
          onSubmit={onSubmit}
          onBackClick={onBackClick}
          resetPasswordErrorMessage={resetPasswordErrorMessage}
          showNotification={showNotification}
          onPwdHideShowClick={onPwdHideShowClick}
          onConfirmPwdHideShowClick={onConfirmPwdHideShowClick}
          hideShowPwd={hideShowPwd}
          confirmHideShowPwd={confirmHideShowPwd}
          setRecaptchaCode={setRecaptchaCode}
          showRecaptcha={showRecaptcha}
          onAppBackToLogin={onAppBackToLogin}
          isResetPassCodeValidationEnabled={isResetPassCodeValidationEnabled}
          otpValue={otpValue}
          selectedPassResetOption={selectedPassResetOption}
          isResetViaText={isResetViaText}
          isSmsSelected={isSmsSelected}
          isRecaptchaError={isRecaptchaError}
          isShowRecaptchaError={isShowRecaptchaError}
          setIsShowRecaptchaError={setIsShowRecaptchaError}
          fullPageView
        />
        {!newCodeSent && isResetPassCodeValidationEnabled && this.resendEmail(isSmsSelected)}
        {newCodeSent && isResetPassCodeValidationEnabled && this.sentNewCodeEmail()}
        {!isResetPassCodeValidationEnabled && <LineComp marginTop={28} />}
      </ContainerWrapper>
    );
  }
}

ResetPassword.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  onBackClick: PropTypes.func.isRequired,
  updateHeader: PropTypes.func.isRequired,
  successMessage: PropTypes.string.isRequired,
  errorMessage: PropTypes.string.isRequired,
  onSubmit: PropTypes.func.isRequired,
  resetPasswordErrorMessage: PropTypes.string.isRequired,
  showNotification: PropTypes.bool.isRequired,
  onPwdHideShowClick: PropTypes.func,
  hideShowPwd: PropTypes.bool,
  showRecaptcha: PropTypes.bool,
  isResetPassCodeValidationEnabled: PropTypes.bool,
  onConfirmPwdHideShowClick: PropTypes.func,
  confirmHideShowPwd: PropTypes.func,
  onRequestClose: PropTypes.func,
  setpasswordValidationCode: PropTypes.func,
  resetPasswordAction: PropTypes.func,
  setRecaptchaCode: PropTypes.func,
  onAppBackToLogin: PropTypes.func,
  resentMessageThreshold: PropTypes.number,
  email: PropTypes.string,
  showOnPlccModal: PropTypes.bool,
  selectedPassResetOption: PropTypes.string,
};

ResetPassword.defaultProps = {
  onRequestClose: () => {},
  onPwdHideShowClick: () => {},
  hideShowPwd: false,
  showRecaptcha: false,
  onConfirmPwdHideShowClick: () => {},
  confirmHideShowPwd: false,
  isResetPassCodeValidationEnabled: false,
  resentMessageThreshold: 3000,
  setpasswordValidationCode: () => {},
  resetPasswordAction: () => {},
  setRecaptchaCode: () => {},
  onAppBackToLogin: () => {},
  email: '',
  showOnPlccModal: false,
  selectedPassResetOption: '',
};

export default ResetPassword;
