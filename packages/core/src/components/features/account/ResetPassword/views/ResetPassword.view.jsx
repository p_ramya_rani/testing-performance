// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import ResetPasswordTopSection from '../molecules/ResetPasswordTopSection';
import ResetPasswordForm from '../molecules/ResetPasswordForm';
import ResetPasswordPinSection from '../molecules/ResetPasswordPinSection';
import ResetPasswordEmailField from '../molecules/ResetPasswordEmailField';
import ResetPasswordResendEmail from '../molecules/ResetPasswordResendEmail';

export const ResetPassword = ({
  labels,
  successMessage,
  errorMessage,
  onSubmit,
  onBack,
  showNotification,
  resetPasswordErrorMessage,
  isFormSubmitting,
  passwordValidationCodeValid,
  setpasswordValidationCode,
  fullPageView,
  email,
  showRecaptcha,
  resetPasswordAction,
  codeExpired,
  resetStateAction,
  resentMessageThreshold,
  selectedPassResetOption,
  maskedPhoneNumber,
  isResetViaText,
  setPassResetOption,
  setPasswordValidationDefaultState,
  isRecaptchaError,
  isShowRecaptchaError,
  setIsShowRecaptchaError,
}) => {
  const [verificationCodeSent, setVerificationCodeSent] = useState(false);
  const [resetPinField, setResetPinField] = useState(false);
  const resendEmail = () => {
    setVerificationCodeSent(true);
    resetPasswordAction({
      logonIdResend: email.toUpperCase().trim(),
      target: selectedPassResetOption,
    });
  };
  const resendViaOption = (option) => {
    setPasswordValidationDefaultState('na');
    setResetPinField(true);
    resetStateAction();
    setVerificationCodeSent(true);
    setPassResetOption(option);
    resetPasswordAction({
      logonIdResend: email.toUpperCase().trim(),
      target: option,
    });
  };

  const isSmsSelected = selectedPassResetOption === 'sms';
  return (
    <React.Fragment>
      <ResetPasswordTopSection
        labels={labels}
        onBack={onBack}
        className="elem-mb-XL"
        fullPageView={fullPageView}
        isSmsSelected={isSmsSelected}
        isResetViaText={isResetViaText}
      />
      {fullPageView && email && (
        <ResetPasswordEmailField
          isResetViaText={isResetViaText}
          email={email}
          labels={labels}
          isSmsSelected={isSmsSelected}
          maskedPhoneNumber={maskedPhoneNumber}
        />
      )}
      {fullPageView && (
        <ResetPasswordPinSection
          setpasswordValidationCode={setpasswordValidationCode}
          passwordValidationCodeValid={passwordValidationCodeValid}
          labels={labels}
          errorMessage={errorMessage}
          isResetViaText={isResetViaText}
          resetPinField={resetPinField}
          setResetPinField={setResetPinField}
          isSmsSelected={isSmsSelected}
          isRecaptchaError={isRecaptchaError}
        />
      )}
      <ResetPasswordForm
        labels={labels}
        successMessage={successMessage}
        errorMessage={errorMessage}
        onSubmit={onSubmit}
        onBack={onBack}
        resetPasswordErrorMessage={resetPasswordErrorMessage}
        showNotification={showNotification}
        isFormSubmitting={isFormSubmitting}
        passwordValidationCodeValid={passwordValidationCodeValid}
        fullPageView={fullPageView}
        showRecaptcha={showRecaptcha}
        codeExpired={codeExpired}
        resetStateAction={resetStateAction}
        resendEmail={resendEmail}
        verificationCodeSent={verificationCodeSent}
        setVerificationCodeSent={setVerificationCodeSent}
        resentMessageThreshold={resentMessageThreshold}
        selectedPassResetOption={selectedPassResetOption}
        isResetViaText={isResetViaText}
        isRecaptchaError={isRecaptchaError}
        setIsShowRecaptchaError={setIsShowRecaptchaError}
        isShowRecaptchaError={isShowRecaptchaError}
      />

      {fullPageView && !isSmsSelected && (
        <>
          <ResetPasswordResendEmail
            resendEmail={resendEmail}
            lblCodeNotReceived={getLabelValue(labels, 'lbl_phone_otp_not_received_content')}
            lblResentCode={getLabelValue(labels, 'lbl_resend_it')}
            isSmsSelected={isSmsSelected}
            isResetViaText={isResetViaText}
          />
          {isResetViaText && maskedPhoneNumber !== '' && (
            <div className="send-via-phone elem-mt-LRG">
              <ResetPasswordResendEmail
                resendEmail={() => resendViaOption('sms')}
                lblCodeNotReceived={getLabelValue(labels, 'lbl_didnt_receive_otp_sent_to_phone')}
                lblResentCode={getLabelValue(labels, 'lbl_send_code_via_phone')}
                isSmsSelected={isSmsSelected}
                isResetViaText={isResetViaText}
              />
            </div>
          )}
        </>
      )}

      {fullPageView && isSmsSelected && (
        <ResetPasswordResendEmail
          resendEmail={resendEmail}
          lblCodeNotReceived={getLabelValue(labels, 'lbl_phone_otp_not_received_content')}
          lblResentCode={getLabelValue(labels, 'lbl_resend_it')}
          isSmsSelected={isSmsSelected}
          isResetViaText={isResetViaText}
        />
      )}

      {fullPageView && isSmsSelected && (
        <div className="send-via-email elem-mt-LRG">
          <ResetPasswordResendEmail
            resendEmail={() => resendViaOption('email')}
            lblCodeNotReceived={getLabelValue(labels, 'lbl_didnt_receive_otp_sent_to_phone')}
            lblResentCode={getLabelValue(labels, 'lbl_send_code_via_email')}
            isSmsSelected={isSmsSelected}
            isResetViaText={isResetViaText}
          />
        </div>
      )}
    </React.Fragment>
  );
};

ResetPassword.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  successMessage: PropTypes.string.isRequired,
  errorMessage: PropTypes.string.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onBack: PropTypes.func.isRequired,
  resetPasswordErrorMessage: PropTypes.string.isRequired,
  showNotification: PropTypes.bool.isRequired,
  isFormSubmitting: PropTypes.bool.isRequired,
  passwordValidationCodeValid: PropTypes.bool,
  setpasswordValidationCode: PropTypes.func,
  fullPageView: PropTypes.bool,
  email: PropTypes.string,
  showRecaptcha: PropTypes.bool,
  resetPasswordAction: PropTypes.func,
  codeExpired: PropTypes.bool,
  resetStateAction: PropTypes.func,
  resentMessageThreshold: PropTypes.number,
  selectedPassResetOption: PropTypes.string,
  isRecaptchaError: PropTypes.bool,
  setIsShowRecaptchaError: PropTypes.func,
  isShowRecaptchaError: PropTypes.bool,
};

ResetPassword.defaultProps = {
  passwordValidationCodeValid: true,
  setpasswordValidationCode: () => {},
  fullPageView: false,
  email: '',
  showRecaptcha: false,
  resetPasswordAction: () => {},
  codeExpired: false,
  resetStateAction: () => {},
  resentMessageThreshold: 3000,
  selectedPassResetOption: '',
  isRecaptchaError: false,
  setIsShowRecaptchaError: () => {},
  isShowRecaptchaError: false,
};

export default ResetPassword;
