// 9fbef606107a605d69c0edbcd8029e5d
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import {
  BodyCopyWithSpacing,
  ViewWithSpacing,
} from '@tcp/core/src/components/common/atoms/styledWrapper';
import InputCheckbox from '@tcp/core/src/components/common/atoms/InputCheckbox';
import LabeledRadioButton from '@tcp/core/src/components/common/atoms/LabeledRadioButton';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import ButtonRedesign from '@tcp/core/src/components/common/atoms/ButtonRedesign';
import {
  RadioButtonWrapper,
  RadioButtonWrapperInner,
  SmsConsentWrapper,
} from './styles/ResetPassword.style.native';

export const ResetPasswordOption = ({
  labels,
  setPassResetOption,
  selectedPassResetOption,
  isShowPhoneOption,
  triggerPassResetApi,
  isFormSubmitting,
  maskedPhoneNumber,
  showSmsTerm,
}) => {
  useEffect(() => {
    // set selected option sms for mobile
    if (isShowPhoneOption) {
      setPassResetOption('sms');
    }
  }, [isShowPhoneOption]);

  const getSelectedResetOption = (option) => {
    return selectedPassResetOption === option;
  };

  const handleFormSubmit = () => {
    triggerPassResetApi({ target: selectedPassResetOption });
  };

  const [isSmsConsentChecked, setIsSmsConsentChecked] = useState(false);
  const handleOnChange = () => {
    setIsSmsConsentChecked(!isSmsConsentChecked);
  };

  const shouldDisabled = (formSubmitting) => {
    if (!isSmsConsentChecked || formSubmitting) {
      return true;
    }
    return false;
  };

  return (
    <>
      <BodyCopyWithSpacing
        text={getLabelValue(labels, 'lbl_forgotPassword_content1', 'password')}
        textAlign="left"
        fontFamily="secondary"
        fontSize="fs28"
        spacingStyles="margin-bottom-SM margin-top-LRG"
      />
      <BodyCopyWithSpacing
        textAlign="left"
        fontFamily="secondary"
        fontSize="fs14"
        text={getLabelValue(labels, 'lbl_resetPassword_content1', 'password')}
      />
      <ViewWithSpacing spacingStyles="margin-bottom-XL">
        <RadioButtonWrapper>
          {isShowPhoneOption && (
            <RadioButtonWrapperInner>
              <BodyCopyWithSpacing
                textAlign="left"
                fontFamily="secondary"
                fontSize="fs14"
                fontWeight="extrabold"
                color="blue.C900"
                spacingStyles="margin-top-LRG margin-bottom-SM margin-left-XL"
                text={getLabelValue(labels, 'lbl_forgotPassword_phoneNumber', 'password')}
              />
              <LabeledRadioButton
                name="passResetOption"
                checked={getSelectedResetOption('sms')}
                obj={{
                  label: maskedPhoneNumber,
                  value: getSelectedResetOption('sms'),
                }}
                onPress={() => setPassResetOption('sms')}
              />
            </RadioButtonWrapperInner>
          )}
        </RadioButtonWrapper>

        {isShowPhoneOption && (selectedPassResetOption === 'sms' || showSmsTerm) && (
          <ViewWithSpacing spacingStyles="margin-left-XL">
            <SmsConsentWrapper>
              <Field
                inputVariation="inputVariation-1"
                name="sms_checkbox"
                component={InputCheckbox}
                dataLocator="sms_checkbox-locator"
                isChecked={isSmsConsentChecked}
                onClick={handleOnChange}
                rightText={getLabelValue(labels, 'lbl_sms_consent', 'password')}
                textMargin="4px 0 0 0"
              />
            </SmsConsentWrapper>
          </ViewWithSpacing>
        )}
      </ViewWithSpacing>
      <ButtonRedesign
        fill="BLUE"
        type="submit"
        buttonVariation="fixed-width"
        onPress={() => handleFormSubmit()}
        text={getLabelValue(labels, 'lbl_forgotPassword_page_next_btn', 'password')}
        borderRadius="16px"
        fontFamily="secondary"
        fontWeight="bold"
        fontSize="fs16"
        paddings="8px"
        disableButton={shouldDisabled(isFormSubmitting)}
        showShadow
      />
    </>
  );
};

ResetPasswordOption.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  setPassResetOption: PropTypes.func,
  selectedPassResetOption: PropTypes.string,
  isShowPhoneOption: PropTypes.bool,
  triggerPassResetApi: PropTypes.func,
  maskedPhoneNumber: PropTypes.bool,
  isFormSubmitting: PropTypes.bool,
};

ResetPasswordOption.defaultProps = {
  setPassResetOption: () => {},
  selectedPassResetOption: '',
  isShowPhoneOption: false,
  triggerPassResetApi: () => {},
  maskedPhoneNumber: '',
  isFormSubmitting: false,
};

export default ResetPasswordOption;
