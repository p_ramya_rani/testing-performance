// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import CustomSelect from '../../../../../../common/molecules/CustomSelect';

const getActiveTitle = (options, value) => {
  const selectedOption = options.find(o => o.value === value);
  return selectedOption && selectedOption.title;
};

export const AddressDropdown = ({
  labels,
  options,
  input,
  selectListTitle,
  customSelectClassName,
  dataLocatorObj,
  selectedValue = '',
}) => {
  const selectedOption = selectedValue || input.value;
  return (
    <CustomSelect
      labels={labels}
      options={options}
      activeValue={selectedOption}
      activeTitle={getActiveTitle(options, selectedOption)}
      clickHandler={(e, value) => input.onChange(value)}
      selectListTitle={selectListTitle}
      customSelectClassName={customSelectClassName}
      dataLocatorObj={dataLocatorObj}
      displayShadow
    />
  );
};

AddressDropdown.propTypes = {
  options: PropTypes.shape([]).isRequired,
  input: PropTypes.shape({}).isRequired,
  selectListTitle: PropTypes.string.isRequired,
  customSelectClassName: PropTypes.string,
  selectedValue: PropTypes.string,
  dataLocatorObj: PropTypes.shape({}),
  labels: PropTypes.shape({}),
};
AddressDropdown.defaultProps = {
  customSelectClassName: '',
  selectedValue: '',
  dataLocatorObj: {
    heading: 'drop-down-heading',
    dropDownList: 'drop-down-list',
  },
  labels: {},
};

export default AddressDropdown;
