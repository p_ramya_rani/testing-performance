// 9fbef606107a605d69c0edbcd8029e5d 
import { put, takeLatest } from 'redux-saga/effects';
import { List } from 'immutable';
import { getSetBillingValuesActn } from '@tcp/core/src/components/features/CnC/Checkout/container/Checkout.action';
import {
  addCreditCardSaga,
  updateCreditCardSaga,
  AddEditCreditCardSaga,
} from '../AddEditCreditCard.saga';
import { addCreditCardSuccess, addCreditCardError } from '../AddEditCreditCard.actions';
import constants from '../AddEditCreditCard.constants';

describe('AddEditCreditCard saga', () => {
  describe('addCreditCardSaga', () => {
    it('should dispatch addCreditCardSuccess action for success resposnse', () => {
      const payload = {
        address: {},
        cardType: 'VISA',
        onFileAddressKey: '11111',
      };
      const addCreditCardGen = addCreditCardSaga({ payload });
      const response = {
        creditCardId: '12345',
      };
      addCreditCardGen.next();
      addCreditCardGen.next();
      addCreditCardGen.next(List([{}]));
      addCreditCardGen.next(
        List([
          {
            addressId: '11111',
            addressLine: ['addressline 1', ''],
          },
        ])
      );
      addCreditCardGen.next(response);
      addCreditCardGen.next();
      addCreditCardGen.next();
      addCreditCardGen.next();
      const putDescriptor = addCreditCardGen.next().value;
      expect(putDescriptor).toEqual(put(addCreditCardSuccess({ response })));
    });

    it('should dispatch addCreditCardError action for error resposnse two', () => {
      const payload = {
        address: {
          firstName: 'test',
          lastName: 'test',
        },
        cardType: 'VISA',
      };
      const addCreditCardGen = addCreditCardSaga({ payload });
      addCreditCardGen.next();
      addCreditCardGen.next();
      addCreditCardGen.next(List());
      addCreditCardGen.next(
        List([
          {
            addressId: '11111',
            addressLine: ['addressline 2', ''],
          },
        ])
      );
      addCreditCardGen.next('1234567890');
      addCreditCardGen.throw({ response: { body: { errors: ['test'] } } });
      addCreditCardGen.next();
      const putDescriptor = addCreditCardGen.next().value;
      expect(putDescriptor).toEqual(put(addCreditCardError('test')));
    });
  });

  describe('updateCreditCardSaga', () => {
    const formatBillingResponse = {
      onFileCardId: '12345', // credit card id from the card book
      paymentId: 'payMethodId',
      emailAddress: '',
      phoneNumber: 'billing_phone1',
      address: {
        onFileAddressKey: 'billing_nickName',
        onFileAddressId: 'addressId',
        sameAsShipping: false,
        firstName: 'billing_firstName',
        lastName: 'billing_lastName',
        addressLine1: 'billing_address1',
        addressLine2: 'billing_address2',
        city: 'billing_city',
        state: 'billing_state',
        zipCode: 'billing_zipCode',
        country: 'billing_country',
      },

      billing: {
        cardNumber: '',
        cardType: '',
        expMonth: '',
        expYear: '',
        cvv: '',
        isExpirationRequired: '',
        isCVVRequired: '',
      },
    };
    it('should dispatch addCreditCardSuccess action for success resposnse', () => {
      const payload = {
        address: {},
        cardType: 'VISA',
        onFileAddressKey: '11111',
      };
      const addCreditCardGen = updateCreditCardSaga({ payload });
      const response = {
        creditCardId: '12345',
      };
      addCreditCardGen.next();
      addCreditCardGen.next();
      addCreditCardGen.next(
        List([
          {
            addressId: '11111',
            addressLine: ['addressline 3', ''],
          },
        ])
      );
      addCreditCardGen.next(response);
      addCreditCardGen.next();
      addCreditCardGen.next();
      addCreditCardGen.next();
      const putDescriptor = addCreditCardGen.next().value;
      expect(putDescriptor).toEqual(put(addCreditCardSuccess({ response })));
    });

    it('should dispatch getSetBillingValuesActn before addCreditCardSuccess action for success resposnse for checkout', () => {
      const payload = {
        address: {},
        cardType: 'VISA',
        onFileAddressKey: '11111',
      };
      const fromCheckout = true;
      const addCreditCardGen = updateCreditCardSaga({ payload }, fromCheckout);
      const response = {
        creditCardId: '12345',
        pay_account: '',
        payMethodId: 'payMethodId',
        pay_expire_month: '',
        billing_country: 'billing_country',
        billing_zipCode: 'billing_zipCode',
        billing_state: 'billing_state',
        billing_city: 'billing_city',
        addressId: 'addressId',
        billing_nickName: 'billing_nickName',
        billing_firstName: 'billing_firstName',
        billing_lastName: 'billing_lastName',
        billing_address1: 'billing_address1',
        billing_address2: 'billing_address2',
        billing_phone1: 'billing_phone1',
        pay_expire_year: '',
      };
      addCreditCardGen.next();
      addCreditCardGen.next();
      addCreditCardGen.next(
        List([
          {
            addressId: '11111',
            addressLine: ['addressline 4', ''],
          },
        ])
      );
      addCreditCardGen.next(response);
      addCreditCardGen.next();
      addCreditCardGen.next();
      addCreditCardGen.next();
      const putDescriptor1 = addCreditCardGen.next().value;
      expect(putDescriptor1).toEqual(put(getSetBillingValuesActn({ ...formatBillingResponse })));
      const putDescriptor = addCreditCardGen.next().value;
      expect(putDescriptor).toEqual(put(addCreditCardSuccess({ response })));
    });

    it('should dispatch getSetBillingValuesActn before addCreditCardSuccess action for success resposnse for checkout with paypal', () => {
      const payload = {
        address: {},
        cardType: 'VISA',
        onFileAddressKey: '11111',
      };
      const fromCheckout = true;
      const addCreditCardGen = updateCreditCardSaga({ payload }, fromCheckout);
      const response = {
        pay_account: 'pay_account',
        payMethodId: 'Paypal',
        pay_expire_month: 'pay_expire_month',
        billing_country: 'billing_country',
        billing_zipCode: 'billing_zipCode',
        billing_state: 'billing_state',
        billing_city: 'billing_city',
        addressId: 'addressId',
        billing_nickName: 'billing_nickName',
        billing_firstName: 'billing_firstName',
        billing_lastName: 'billing_lastName',
        billing_address1: 'billing_address1',
        billing_address2: 'billing_address2',
        billing_phone1: 'billing_phone1',
        pay_expire_year: 'pay_expire_year',
      };
      addCreditCardGen.next();
      addCreditCardGen.next();
      addCreditCardGen.next(
        List([
          {
            addressId: '11111',
            addressLine: ['addressline 5', ''],
          },
        ])
      );
      addCreditCardGen.next(response);
      addCreditCardGen.next();
      addCreditCardGen.next();
      addCreditCardGen.next();
      const putDescriptor1 = addCreditCardGen.next().value;
      expect(putDescriptor1).toEqual(
        put(
          getSetBillingValuesActn({
            ...formatBillingResponse,
            onFileCardId: '',
            billing: { ...formatBillingResponse.billing, cardType: '' },
            paymentId: 'Paypal',
          })
        )
      );
      const putDescriptor = addCreditCardGen.next().value;
      expect(putDescriptor).toEqual(put(addCreditCardSuccess({ response })));
    });

    it('should dispatch addCreditCardError action for error resposnse', () => {
      const payload = {
        address: {
          firstName: 'test',
          lastName: 'test',
        },
        cardType: 'VISA',
      };
      const addCreditCardGen = updateCreditCardSaga({ payload });
      addCreditCardGen.next();
      addCreditCardGen.next(
        List([
          {
            addressId: '11111',
            addressLine: ['addressline 1', ''],
          },
        ])
      );
      addCreditCardGen.next('1234567890');
      addCreditCardGen.throw({ response: { body: { errors: ['test'] } } });
      addCreditCardGen.next();
      const putDescriptor = addCreditCardGen.next().value;
      expect(putDescriptor).toEqual(put(addCreditCardError('test')));
    });
  });

  describe('AddEditCreditCardSaga', () => {
    it('should return correct takeLatest effect', () => {
      const generator = AddEditCreditCardSaga();
      let takeLatestDescriptor = generator.next().value;
      expect(takeLatestDescriptor).toEqual(
        takeLatest(constants.ADD_CREDIT_CARD, addCreditCardSaga)
      );

      takeLatestDescriptor = generator.next().value;

      expect(takeLatestDescriptor).toEqual(
        takeLatest(constants.EDIT_CREDIT_CARD, updateCreditCardSaga)
      );
    });
  });
});
