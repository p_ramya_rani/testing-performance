// 9fbef606107a605d69c0edbcd8029e5d 
/* eslint-disable camelcase */
import { call, put, takeLatest, select } from 'redux-saga/effects';
import { setLoaderState } from '@tcp/core/src/components/common/molecules/Loader/container/Loader.actions';
import { trackFormError } from '@tcp/core/src/analytics/actions';
import { trackForterAction, ForterActionType } from '@tcp/core/src/utils/forter.util';
import logger from '@tcp/core/src/utils/loggerInstance';
import { getSetBillingValuesActn } from '@tcp/core/src/components/features/CnC/Checkout/container/Checkout.action';
import constants from './AddEditCreditCard.constants';
import { addCreditCardSuccess, addCreditCardError } from './AddEditCreditCard.actions';
import { addCreditCard, updateCreditCard } from '../../../../../services/abstractors/account';
import { getCreditDebitCards } from '../../Payment/container/Payment.selectors';
import { getAddressListState } from '../../AddressBook/container/AddressBook.selectors';
import { getUserPhoneNumber } from '../../User/container/User.selectors';
import {
  clearGetAddressListTTL,
  getAddressList,
} from '../../AddressBook/container/AddressBook.actions';
import { clearCardListTTL } from '../../Payment/container/Payment.actions';
import { isMobileApp } from '../../../../../utils';

function trackForterEvent(paymentInfo) {
  if (!paymentInfo.addressId && isMobileApp()) {
    trackForterAction(ForterActionType.TAP, 'NEW_CARD_ADDRESS');
  }
}

const getFormErrorPayload = payload => {
  const { cardNumber, expYear, expMonth, address } = payload;
  const {
    firstName,
    lastName,
    addressLine1,
    addressLine2,
    city,
    state,
    country,
    zipCode,
  } = address;

  return {
    cardNumber,
    expYear,
    expMonth,
    firstName,
    lastName,
    addressLine1,
    addressLine2,
    city,
    state,
    country,
    zipCode,
  };
};

export function* addCreditCardSaga({ payload }) {
  yield put(setLoaderState(true));
  try {
    const { address, cardType, onFileAddressKey, isDefault, ...otherPayloadProps } = payload;
    const ccBook = yield select(getCreditDebitCards);
    const addressList = yield select(getAddressListState);

    const isEmptyAccount = !ccBook || ccBook.size === 0;
    let addressEntry;

    if (onFileAddressKey) {
      addressEntry = addressList.find(add => add.addressId === onFileAddressKey);
    } else {
      addressEntry = Object.assign({}, address, {
        phoneNumber: yield select(getUserPhoneNumber),
      });
    }

    if (!addressEntry.addressLine1 && addressEntry.addressLine) {
      const { addressLine } = addressEntry;
      [addressEntry.addressLine1, addressEntry.addressLine2] = addressLine;
    }

    const paymentInfo = {
      ...otherPayloadProps,
      ...addressEntry,
      isDefault: isDefault || isEmptyAccount,
      addressId: addressEntry ? addressEntry.addressId : null,
      cardType,
      saveToAccount: true,
    };
    const response = yield call(addCreditCard, paymentInfo);
    yield put(clearGetAddressListTTL());
    yield put(clearCardListTTL());
    yield put(getAddressList());
    yield put(setLoaderState(false));
    return yield put(addCreditCardSuccess({ response }));
  } catch (err) {
    logger.error({
      error: err,
      extraData: {
        component: 'AddEditCreditCard Saga - addCreditCardSaga',
        payloadRecieved: payload,
      },
    });
    yield put(
      trackFormError({ formName: 'addEditCreditCard', formData: getFormErrorPayload(payload) })
    );
    let error = {};
    /* istanbul ignore else */
    yield put(setLoaderState(false));
    error = err;
    return yield put(addCreditCardError(error.response.body.errors[0]));
  }
}

function formatBillingInfoForRedux(response) {
  const {
    creditCardId,
    payMethodId,
    billing_country,
    billing_zipCode,
    billing_state,
    billing_city,
    addressId,
    billing_nickName,
    billing_firstName,
    billing_lastName,
    billing_address1,
    billing_address2,
    billing_phone1,
  } = response;
  return {
    onFileCardId: (creditCardId || '').toString(), // credit card id from the card book
    paymentId: payMethodId,
    emailAddress: '',
    phoneNumber: billing_phone1,
    address: {
      onFileAddressKey: billing_nickName,
      onFileAddressId: addressId && addressId.toString(),
      sameAsShipping: false,
      firstName: billing_firstName,
      lastName: billing_lastName,
      addressLine1: billing_address1,
      addressLine2: billing_address2,
      city: billing_city,
      state: billing_state,
      zipCode: billing_zipCode,
      country: billing_country,
    },

    billing: {
      cardNumber: '',
      cardType: '',
      expMonth: '',
      expYear: '',
      cvv: '',
      isExpirationRequired: '',
      isCVVRequired: '',
    },
  };
}

export function* updateCreditCardSaga({ payload }, fromCheckout) {
  yield put(setLoaderState(true));
  try {
    const { address, cardType, onFileAddressKey, ...otherPayloadProps } = payload;
    const addressList = yield select(getAddressListState);

    let addressEntry;

    if (onFileAddressKey) {
      addressEntry = addressList.find(add => add.addressId === onFileAddressKey);
    } else {
      addressEntry = Object.assign({}, address, {
        phoneNumber: yield select(getUserPhoneNumber),
      });
    }

    if (!addressEntry.addressLine1 && addressEntry.addressLine) {
      const { addressLine } = addressEntry;
      [addressEntry.addressLine1, addressEntry.addressLine2] = addressLine;
    }

    const paymentInfo = {
      ...otherPayloadProps,
      ...addressEntry,
      addressId: addressEntry ? addressEntry.addressId : null,
      cardType,
    };
    const response = yield call(updateCreditCard, paymentInfo);
    trackForterEvent(paymentInfo);
    yield put(clearGetAddressListTTL());
    yield put(clearCardListTTL());
    yield put(getAddressList());
    yield put(setLoaderState(false));
    if (fromCheckout) {
      yield put(getSetBillingValuesActn(formatBillingInfoForRedux(response)));
    }
    return yield put(addCreditCardSuccess({ response }));
  } catch (err) {
    logger.error({
      error: err,
      extraData: {
        component: 'AddEditCreditCard Saga - updateCreditCardSaga',
        payloadRecieved: payload,
      },
    });
    yield put(
      trackFormError({ formName: 'addEditCreditCard', formData: getFormErrorPayload(payload) })
    );
    yield put(setLoaderState(false));
    let error = {};
    /* istanbul ignore else */
    error = err;
    if (fromCheckout) {
      throw err;
    }
    if (error && error.response) {
      return yield put(addCreditCardError(error.response.body.errors[0]));
    }
    return null;
  }
}

export function* AddEditCreditCardSaga() {
  yield takeLatest(constants.ADD_CREDIT_CARD, addCreditCardSaga);
  yield takeLatest(constants.EDIT_CREDIT_CARD, updateCreditCardSaga);
}
export default AddEditCreditCardSaga;
