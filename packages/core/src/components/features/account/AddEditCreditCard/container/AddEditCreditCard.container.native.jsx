// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import { change } from 'redux-form';
import { trackPageView, setClickAnalyticsData } from '@tcp/core/src/analytics/actions';
import { toastMessageInfo } from '@tcp/core/src/components/common/atoms/Toast/container/Toast.actions';
import { getAddressList } from '../../AddressBook/container/AddressBook.actions';
import { verifyAddress } from '../../../../common/organisms/AddressVerification/container/AddressVerification.actions';
import {
  getCardType,
  getCreditCardById,
  getOnFileAddressKey,
  getAddEditCreditCardSuccess,
  getAddEditCreditCardError,
  getAddGiftCardErrorMessage,
  getshowNotification,
} from './AddEditCreditCard.selectors';
import { getFormValidationErrorMessages } from '../../Account/container/Account.selectors';
import constants from './AddEditCreditCard.constants';
import AddEditCreditCardComponent from '../views/AddEditCreditCard.view.native';
import { getAddressListState } from '../../AddressBook/container/AddressBook.selectors';
import { addCreditCard, editCreditCard } from './AddEditCreditCard.actions';
import { setDefaultPaymentSuccess } from '../../Payment/container/Payment.actions';
import { getCreditCardExpirationOptionMap } from './AddEditCreditCard.utils';
import { getAddEditAddressLabels } from '../../../../common/organisms/AddEditAddress/container/AddEditAddress.selectors';

export class AddEditCreditCard extends React.PureComponent {
  static propTypes = {
    creditCard: PropTypes.shape({}),
    cardType: PropTypes.string,
    onFileAddressKey: PropTypes.string,
    addressList: PropTypes.shape([]),
    isPLCCEnabled: PropTypes.bool,
    addEditCreditCardSuccess: PropTypes.shape({}),
    addEditCreditCardError: PropTypes.string,
    getAddressListAction: PropTypes.func.isRequired,
    addCreditCardAction: PropTypes.func.isRequired,
    editCreditCardAction: PropTypes.func.isRequired,
    showSuccessNotification: PropTypes.func.isRequired,
    labels: PropTypes.shape({}),
    onClose: PropTypes.func,
    updateCardList: PropTypes.func,
    dto: PropTypes.shape({}),
    isEdit: PropTypes.bool,
    selectedCard: PropTypes.shape({}),
    addressLabels: PropTypes.shape({}),
    addEditCreditCardErrorMsg: PropTypes.string,
    showNotification: PropTypes.bool.isRequired,
    toastMessage: PropTypes.func.isRequired,
    trackPageLoad: PropTypes.func,
    formErrorMessage: PropTypes.shape({}).isRequired,
    verifyAddressAction: PropTypes.func.isRequired,
  };

  static defaultProps = {
    cardType: '',
    addressList: List(),
    isPLCCEnabled: true,
    onFileAddressKey: '',
    addEditCreditCardSuccess: null,
    addEditCreditCardError: null,
    creditCard: null,
    labels: {},
    onClose: () => {},
    updateCardList: () => {},
    dto: {},
    isEdit: false,
    selectedCard: null,
    addressLabels: {},
    addEditCreditCardErrorMsg: '',
    trackPageLoad: () => {},
  };

  constructor(props) {
    super(props);
    this.state = {
      initialValues: {},
    };
    this.creditCardExpirationOptionMap = getCreditCardExpirationOptionMap();
    this.creditCardDetail = {};
  }

  componentDidMount() {
    const { addressList, getAddressListAction, isEdit, trackPageLoad } = this.props;
    if (addressList === null || !addressList.size) {
      getAddressListAction();
    }
    this.setInitialValues();
    if (!isEdit) {
      trackPageLoad({
        currentScreen: 'account',
        pageNavigationText: '',
        pageData: {
          pageName: 'myplace:payment:add credit card',
          pageSection: 'myplace',
          pageSubSection: 'myplace',
          pageType: 'myplace',
        },
      });
    }
  }

  componentDidUpdate(prevProps) {
    const {
      addEditCreditCardSuccess,
      showSuccessNotification,
      creditCard,
      addressList,
      onClose,
      updateCardList,
      addEditCreditCardErrorMsg,
      toastMessage,
      showNotification,
    } = this.props;
    const isAddressListUpdated = !prevProps.addressList && addressList;
    if (!prevProps.addEditCreditCardSuccess && addEditCreditCardSuccess) {
      showSuccessNotification();
      updateCardList();
      onClose();
    }

    if (!prevProps.showNotification && showNotification) {
      toastMessage(addEditCreditCardErrorMsg);
    }

    if (isAddressListUpdated || (!prevProps.creditCard && creditCard)) {
      this.setInitialValues();
    }
  }

  setInitialValues = () => {
    const { addressList } = this.props;
    if (addressList) {
      const initialValues = this.getInitialValues();
      this.setState({
        initialValues,
      });
    }
  };

  getExpirationRequiredFlag = () => {
    const { cardType } = this.props;

    return !cardType || cardType !== constants.ACCEPTED_CREDIT_CARDS['PLACE CARD'];
  };

  getInitialValuesForEditMode = creditCard => {
    if (creditCard) {
      let cardType = creditCard.ccBrand || creditCard.ccType;
      cardType = constants.ACCEPTED_CREDIT_CARDS[cardType.toUpperCase()];
      return {
        onFileAddressKey: creditCard.billingAddressId.toString(),
        cardType,
        cardNumber: creditCard.accountNo,
        expYear: creditCard.expYear.trim(),
        expMonth: creditCard.expMonth.trim(),
        address: {
          country: constants.COUNTRY_US,
          addressLine2: '',
        },
      };
    }

    return null;
  };

  getInitialValues = () => {
    const { addressList, selectedCard, isEdit } = this.props;
    let onFileAddressKey = '';

    if (addressList && addressList.size > 0) {
      const defaultBillingAddress = addressList.filter(address => address.primary === 'true');
      onFileAddressKey =
        defaultBillingAddress.size > 0 ? defaultBillingAddress.get(0).addressId : '';
    }

    if (isEdit) {
      return this.getInitialValuesForEditMode(selectedCard);
    }

    return {
      onFileAddressKey,
      address: {
        country: constants.COUNTRY_US,
        addressLine2: '',
      },
    };
  };

  formatPayload = payload => {
    const { addressLine1, addressLine2, zipCode, ...otherPayload } = payload || {};
    return {
      ...otherPayload,
      ...{
        address1: addressLine1,
        address2: addressLine2,
        zip: zipCode,
      },
    };
  };

  submitCreditCardDetails = payload => {
    const { creditCard, addCreditCardAction, editCreditCardAction } = this.props;
    if (creditCard && creditCard.creditCardId) {
      const userData = payload;
      userData.creditCardId = creditCard.creditCardId;
      userData.isDefault = creditCard.defaultInd;
      return editCreditCardAction(userData);
    }
    return addCreditCardAction(payload);
  };

  onCreditCardFormSubmit = data => {
    const { cardType, verifyAddressAction } = this.props;
    const payload = Object.assign(data, {
      cardType,
    });
    if (payload.onFileAddressKey) {
      return this.submitCreditCardDetails(payload);
    }
    this.creditCardDetail = payload;
    const formattedPayload = this.formatPayload(payload.address);
    return verifyAddressAction(formattedPayload);
  };

  submitAddressFormAction = payloadData => {
    if (!payloadData) {
      return this.submitCreditCardDetails(this.creditCardDetail);
    }
    const { address1, address2, zip, ...addressData } = payloadData;
    const payload = Object.assign(this.creditCardDetail, {
      address: {
        addressLine1: address1,
        addressLine2: address2,
        zipCode: zip,
        ...addressData,
      },
    });

    return this.submitCreditCardDetails(payload);
  };

  render() {
    const {
      creditCard,
      cardType,
      onFileAddressKey,
      addressList,
      isPLCCEnabled,
      addEditCreditCardError,
      labels,
      onClose,
      dto,
      isEdit,
      selectedCard,
      addressLabels,
      formErrorMessage,
    } = this.props;

    if (addressList === null) {
      return null;
    }

    const isExpirationRequired = this.getExpirationRequiredFlag();
    const { initialValues } = this.state;
    let creditCardType = cardType;
    if (selectedCard && !cardType) {
      creditCardType = selectedCard.ccBrand || selectedCard.ccType;
      creditCardType =
        (creditCardType && constants.ACCEPTED_CREDIT_CARDS[creditCardType.toUpperCase()]) || null;
    }

    return (
      <AddEditCreditCardComponent
        isEdit={isEdit}
        creditCard={creditCard}
        cardType={creditCardType}
        onFileAddressKey={onFileAddressKey}
        isPLCCEnabled={isPLCCEnabled}
        isExpirationRequired={isExpirationRequired}
        addressList={addressList}
        labels={labels}
        expMonthOptionsMap={this.creditCardExpirationOptionMap.monthsMap}
        expYearOptionsMap={this.creditCardExpirationOptionMap.yearsMap}
        initialValues={initialValues}
        addressLabels={labels}
        addressFormLabels={addressLabels.addressFormLabels}
        backToPaymentClick={this.backToPaymentClick}
        onSubmit={this.onCreditCardFormSubmit}
        errorMessage={addEditCreditCardError}
        onClose={onClose}
        dto={dto}
        showEmailAddress={false}
        formErrorMessage={formErrorMessage}
        selectedCard={selectedCard}
        cameraIcon
        submitAddressFormAction={this.submitAddressFormAction}
      />
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    creditCard: getCreditCardById(state, ownProps),
    addressList: getAddressListState(state, ownProps),
    cardType: getCardType(state, ownProps),
    onFileAddressKey: getOnFileAddressKey(state, ownProps),
    isPLCCEnabled: true,
    addEditCreditCardSuccess: getAddEditCreditCardSuccess(state),
    addEditCreditCardError: getAddEditCreditCardError(state),
    addressLabels: getAddEditAddressLabels(state),
    formErrorMessage: getFormValidationErrorMessages(state),
    showNotification: getshowNotification(state),
    addEditCreditCardErrorMsg: getAddGiftCardErrorMessage(state),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getAddressListAction: () => {
      dispatch(getAddressList());
    },
    addCreditCardAction: payload => {
      dispatch(addCreditCard(payload));
    },
    editCreditCardAction: payload => {
      dispatch(editCreditCard(payload));
    },
    showSuccessNotification: () => {
      dispatch(setDefaultPaymentSuccess());
    },
    toastMessage: palyoad => {
      dispatch(toastMessageInfo(palyoad));
    },
    trackPageLoad: payload => {
      dispatch(
        setClickAnalyticsData({
          customEvents: ['event80'],
          pageNavigationText: payload.pageNavigationText,
        })
      );
      dispatch(trackPageView(payload));
      setTimeout(() => {
        dispatch(setClickAnalyticsData({}));
      }, 100);
    },
    verifyAddressAction: payload => {
      dispatch(verifyAddress(payload));
    },
    updateReduxForm: (form, key, value) => dispatch(change(form, key, value)),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddEditCreditCard);
