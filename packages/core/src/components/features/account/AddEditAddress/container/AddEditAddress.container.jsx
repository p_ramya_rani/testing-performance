// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import { getAutocompleteTypesParam } from '@tcp/core/src/reduxStore/selectors/session.selectors';
import AddAddressComponent from '../views/AddEditAddress.view';
import { getAddressList } from '../../AddressBook/container/AddressBook.actions';
import { getAddressResponse, getAddressById } from './AddEditAddress.selectors';
import { routerPush, isMobileApp } from '../../../../../utils';

export class AddEditAddressContainer extends React.PureComponent {
  static propTypes = {
    addressResponse: PropTypes.shape({}),
    address: PropTypes.shape({}),
    getAddressListAction: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
    showHeading: PropTypes.bool.isRequired,
    labels: PropTypes.shape({}).isRequired,
    mapboxAutocompleteTypesParam: PropTypes.string,
  };

  componentDidUpdate() {
    const { addressResponse, getAddressListAction } = this.props;
    const isSuccess = addressResponse && addressResponse.get('addressId');
    if (isSuccess) {
      if (isMobileApp()) {
        getAddressListAction();
      } else this.backToAddressBookClick();
    }
  }

  backToAddressBookClick = () => {
    return routerPush('/account?id=address-book', '/account/address-book');
  };

  render() {
    const {
      addressResponse,
      address,
      labels,
      onCancel,
      showHeading,
      mapboxAutocompleteTypesParam,
    } = this.props;
    return (
      <AddAddressComponent
        addressResponse={addressResponse}
        isEdit={!!address}
        backToAddressBookClick={this.backToAddressBookClick}
        labels={labels}
        onCancel={onCancel}
        showHeading={showHeading}
        addressBookLabels={getLabelValue(labels, 'addressBook')}
        mapboxAutocompleteTypesParam={mapboxAutocompleteTypesParam}
      />
    );
  }
}

AddEditAddressContainer.defaultProps = {
  addressResponse: {},
  address: null,
  mapboxAutocompleteTypesParam: '',
};

export const mapDispatchToProps = dispatch => {
  return {
    getAddressListAction: () => {
      dispatch(getAddressList());
    },
  };
};

const mapStateToProps = (state, ownProps) => {
  return {
    addressResponse: getAddressResponse(state),
    address: getAddressById(state, ownProps),
    mapboxAutocompleteTypesParam: getAutocompleteTypesParam(state),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddEditAddressContainer);
