// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { CustomerHelpContainer } from '../container/CustomerHelpCommon.container';
import CustomerHelpView from '../views';

describe('Customer Help container', () => {
  it('should render Customer Help component', () => {
    const component = shallow(<CustomerHelpContainer />);
    expect(component.is(CustomerHelpView)).toBeTruthy();
  });
});
