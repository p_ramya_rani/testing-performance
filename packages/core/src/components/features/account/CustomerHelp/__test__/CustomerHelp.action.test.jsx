// 9fbef606107a605d69c0edbcd8029e5d
import {
  selectOrder,
  setSelectedItems,
  getShipmentStatus,
  setShipmentStatus,
  getItemLevelRefund,
  setItemLevelRefund,
  setResolution,
  clearSelectedItems,
  setResolutionModuleX,
  setCancelResolutionModuleX,
  fetchModifiedResolutionModuleX,
  fetchCancelledResolutionModuleX,
  showLoader,
  hideLoader,
  setWrongItemState,
  setTransitResolutionModuleX,
  fetchTransitHelpModuleX,
  setRefundStatus,
  getRefundStatus,
  getCancelStatus,
  setCancelStatus,
  getShipmentDelayedRefundStatus,
  setShipmentDelayedRefundStatus,
  setOrderStatusInTransit,
  setToggleCards,
  setSelectedShipmentCard,
  setFilteredList,
  getSavedSummary,
  setReasonCodes,
  getReasonCodes,
  setReturnMethod,
  clearReturnMethod,
  toggleReturnItemDisplay,
  fetchReturnPolicyModuleX,
  setReturnPolicyModuleX,
  setOrderArriveReasonCode,
  clearOrderArriveSelectedReason,
} from '../container/CustomerHelp.actions';
import CUSTOMER_HELP_CONSTANTS, { RETURN_ITEM_CONSTANT } from '../CustomerHelp.constants';

describe('Customer Help Action', () => {
  it('should call selectOrder action of Customer Help actions', () => {
    const selectOrderAction = selectOrder({});
    expect(selectOrderAction.type).toBe(CUSTOMER_HELP_CONSTANTS.SELECT_ORDER);
  });

  it('should call selectOrder action of Customer Help actions', () => {
    const setSelectedItemsAction = setSelectedItems();
    expect(setSelectedItemsAction.type).toBe(CUSTOMER_HELP_CONSTANTS.SET_SELECTED_ITEMS);
  });

  it('should call setReasonCode action of Customer Help actions', () => {
    const setResolutionAction = setResolution();
    expect(setResolutionAction.type).toBe(CUSTOMER_HELP_CONSTANTS.SET_RESOLUTION_CODE);
  });

  it('should call setReasonCodes action of Customer Help actions', () => {
    const setReasonCodesAction = setReasonCodes();
    expect(setReasonCodesAction.type).toBe(CUSTOMER_HELP_CONSTANTS.SET_REASON_CODES);
  });

  it('should call getReasonCodes action of Customer Help actions', () => {
    const getReasonCodesAction = getReasonCodes();
    expect(getReasonCodesAction.type).toBe(CUSTOMER_HELP_CONSTANTS.GET_REASON_CODES);
  });

  it('should call getShipmentStatus action of Customer Help actions', () => {
    const setSelectedItemsAction = getShipmentStatus();
    expect(setSelectedItemsAction.type).toBe(CUSTOMER_HELP_CONSTANTS.GET_SHIPMENT_STATUS);
  });
  it('should call setShipmentStatus action of Customer Help actions', () => {
    const setSelectedItemsAction = setShipmentStatus();
    expect(setSelectedItemsAction.type).toBe(CUSTOMER_HELP_CONSTANTS.SET_SHIPMENT_STATUS);
  });
  it('should call getItemLevelRefund action of Customer Help actions', () => {
    const payload = {};
    const response = {
      type: CUSTOMER_HELP_CONSTANTS.GET_ITEM_LEVEL_REFUND,
      payload,
    };
    expect(getItemLevelRefund(payload)).toEqual(response);
  });

  it('should call setItemLevelRefund action of Customer Help actions', () => {
    const payload = {};
    const response = {
      type: CUSTOMER_HELP_CONSTANTS.SET_ITEM_LEVEL_REFUND,
      payload,
    };
    expect(setItemLevelRefund(payload)).toEqual(response);
  });

  it('should call setResolutionModuleX action of Customer Help actions', () => {
    const payload = {};
    const response = {
      type: CUSTOMER_HELP_CONSTANTS.SET_RESOLUTION_MODULEX_CONTENT,
      payload,
    };
    expect(setResolutionModuleX(payload)).toEqual(response);
  });

  it('should call setCancelResolutionModuleX action of Customer Help actions', () => {
    const payload = {};
    const response = {
      type: CUSTOMER_HELP_CONSTANTS.SET_CANCEL_RESOLUTION_MODULEX_CONTENT,
      payload,
    };
    expect(setCancelResolutionModuleX(payload)).toEqual(response);
  });

  it('should call fetchModifiedResolutionModuleX action of Customer Help actions', () => {
    const payload = {};
    const response = {
      type: CUSTOMER_HELP_CONSTANTS.FETCH_RESOLUTION_MODULEX_CONTENT,
      payload,
    };
    expect(fetchModifiedResolutionModuleX(payload)).toEqual(response);
  });

  it('should call fetchModififetchCancelledResolutionModuleXedResolutionModuleX action of Customer Help actions', () => {
    const payload = {};
    const response = {
      type: CUSTOMER_HELP_CONSTANTS.FETCH_CANCEL_MODULEX_CONTENT,
      payload,
    };
    expect(fetchCancelledResolutionModuleX(payload)).toEqual(response);
  });

  it('should call fetchReturnPolicyModuleX ', () => {
    const payload = {};
    const response = {
      type: CUSTOMER_HELP_CONSTANTS.FETCH_RETURN_POLICY_MODULEX_CONTENT,
      payload,
    };
    expect(fetchReturnPolicyModuleX(payload)).toEqual(response);
  });

  it('clearSelectedItems', () => {
    expect(clearSelectedItems()).toEqual({ type: CUSTOMER_HELP_CONSTANTS.CLEAR_SELECTED_ITEMS });
  });

  it('should call showLoader action of Customer Help actions', () => {
    expect(showLoader()).toEqual({ type: CUSTOMER_HELP_CONSTANTS.SHOW_CSH_LOADER });
  });

  it('should call hideLoader action of Customer Help actions', () => {
    expect(hideLoader()).toEqual({ type: CUSTOMER_HELP_CONSTANTS.HIDE_CSH_LOADER });
  });

  it('should call setWrongItemState', () => {
    const payload = {};
    const response = {
      type: CUSTOMER_HELP_CONSTANTS.SET_WRONG_ITEM_STATE,
      payload,
    };
    expect(setWrongItemState(payload)).toEqual(response);
  });
  it('should call setTransitResolutionModuleX action of Customer Help actions', () => {
    expect(setTransitResolutionModuleX()).toEqual({
      type: CUSTOMER_HELP_CONSTANTS.SET_TRANSIT_RESOLUTION_MODULEX_CONTENT,
    });
  });

  it('should call fetchTransitHelpModuleX action of Customer Help actions', () => {
    expect(fetchTransitHelpModuleX()).toEqual({
      type: CUSTOMER_HELP_CONSTANTS.FETCH_TRANSIT_RESOLUTION_MODULEX_CONTENT,
    });
  });

  it('should call setRefundStatus action of Customer Help actions', () => {
    expect(setRefundStatus()).toEqual({ type: CUSTOMER_HELP_CONSTANTS.SET_REFUND_STATUS });
  });

  it('should call setReturnPolicyModuleX action', () => {
    expect(setReturnPolicyModuleX()).toEqual({
      type: CUSTOMER_HELP_CONSTANTS.SET_RETURN_POLICY_MODULEX_CONTENT,
    });
  });

  it('should call getRefundStatus action of Customer Help actions', () => {
    expect(getRefundStatus()).toEqual({ type: CUSTOMER_HELP_CONSTANTS.GET_REFUND_STATUS });
  });

  it('should call getCancelStatus action of Customer Help actions', () => {
    expect(getCancelStatus()).toEqual({ type: CUSTOMER_HELP_CONSTANTS.GET_CANCEL_STATUS });
  });
  it('should call setCancelStatus action of Customer Help actions', () => {
    expect(setCancelStatus()).toEqual({ type: CUSTOMER_HELP_CONSTANTS.SET_CANCEL_STATUS });
  });
  it('should call getShipmentDelayedRefundStatus action of Customer Help actions', () => {
    expect(getShipmentDelayedRefundStatus()).toEqual({
      type: CUSTOMER_HELP_CONSTANTS.GET_SHIPMENT_DELAYED_REFUND,
    });
  });
  it('should call setShipmentDelayedRefundStatus action of Customer Help actions', () => {
    expect(setShipmentDelayedRefundStatus()).toEqual({
      type: CUSTOMER_HELP_CONSTANTS.SET_SHIPMENT_DELAYED_REFUND,
    });
  });
  it('should call setOrderStatusInTransit', () => {
    const payload = {};
    const response = {
      type: CUSTOMER_HELP_CONSTANTS.ORDER_STATUS_IN_TRANSIT,
      payload,
    };
    expect(setOrderStatusInTransit(payload)).toEqual(response);
  });
  it('should call setToggleCards', () => {
    const payload = {};
    const response = {
      type: CUSTOMER_HELP_CONSTANTS.TOGGLE_CARDS,
      payload,
    };
    expect(setToggleCards(payload)).toEqual(response);
  });
  it('should call setSelectedShipmentCard', () => {
    const payload = {};
    const response = {
      type: CUSTOMER_HELP_CONSTANTS.SET_SELECTED_SHIPMENT_CARD,
      payload,
    };
    expect(setSelectedShipmentCard(payload)).toEqual(response);
  });
  it('should call setFilteredList', () => {
    const payload = {};
    const response = {
      type: CUSTOMER_HELP_CONSTANTS.SET_FILTERED_LIST,
      payload,
    };
    expect(setFilteredList(payload)).toEqual(response);
  });
  it('should call getSavedSummary', () => {
    const payload = {};
    const response = {
      type: CUSTOMER_HELP_CONSTANTS.SET_SUMMARY,
      payload,
    };
    expect(getSavedSummary(payload)).toEqual(response);
  });

  it('should call setReturnMethod', () => {
    expect(setReturnMethod()).toEqual({
      type: RETURN_ITEM_CONSTANT.SET_RETURN_METHOD,
      payload: undefined,
    });
  });

  it('should call clear return method', () => {
    expect(clearReturnMethod()).toEqual({
      type: RETURN_ITEM_CONSTANT.CLEAR_RETURN_METHOD,
    });
  });

  it('should call to check return Item display boolean', () => {
    expect(toggleReturnItemDisplay()).toEqual({
      type: RETURN_ITEM_CONSTANT.IS_RETURN_ITEM_DISPLAY,
      payload: undefined,
    });
  });
  it('should call order arrive ReasonCodes action of Customer Help actions', () => {
    const setReasonCodesAction = setOrderArriveReasonCode();
    expect(setReasonCodesAction.type).toBe(CUSTOMER_HELP_CONSTANTS.SET_ORDER_ARRIVE_REASON_CODE);
  });

  it('should call clear order arrive reason code', () => {
    expect(clearOrderArriveSelectedReason()).toEqual({
      type: CUSTOMER_HELP_CONSTANTS.CLEAR_ORDER_ARRIVE_REASON_CODE,
    });
  });
});
