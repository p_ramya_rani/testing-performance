// 9fbef606107a605d69c0edbcd8029e5d
import CustomerHelpReducer from '../container/CustomerHelp.reducer';
import CUSTOMER_HELP_CONSTANTS, {
  REASON_CODES_REDUX_KEY,
  RETURN_ITEM_CONSTANT,
} from '../CustomerHelp.constants';

describe('Customer Help Reducer', () => {
  const initialState = {
    selectedReasonCode: null,
    reasonCodes: [],
    returnMethod: null,
    isReturnItemDisplay: false,
  };
  it('should return default state', () => {
    const finalState = CustomerHelpReducer(initialState, {});
    expect(finalState.selectedReasonCode).toBe(initialState.selectedReasonCode);
  });
  it('should return modified state for SET_REFERRED_CONTENT action', () => {
    const action = {
      type: CUSTOMER_HELP_CONSTANTS.SET_REFERRED_CONTENT,
      payload: [
        {
          label: 'dummy',
          id: 'dummy',
        },
      ],
    };
    const finalState = CustomerHelpReducer(initialState, action);
    expect(finalState.reasonCodes.length).toBe(action.payload.length);
  });
  it('should return modified state for SET_REASON_CODES action', () => {
    const action = {
      type: CUSTOMER_HELP_CONSTANTS.SET_REASON_CODES,
      payload: {
        reduxKey: REASON_CODES_REDUX_KEY.TOP_LEVEL_REASON_CODES,
        reasonCodes: [{ id: 'dummy', label: 'dummy' }],
      },
    };
    const finalState = CustomerHelpReducer(initialState, action);
    expect(finalState[REASON_CODES_REDUX_KEY.TOP_LEVEL_REASON_CODES].length).toBe(
      action.payload.reasonCodes.length
    );
  });
  it('should return modified state for SET_REASON_CODE action', () => {
    const action = {
      type: CUSTOMER_HELP_CONSTANTS.SET_REASON_CODE,
      payload: {
        label: 'dummy',
        id: 'dummy',
      },
    };
    const finalState = CustomerHelpReducer(initialState, action);
    expect(finalState.selectedReasonCode.label).toBe(action.payload.label);
  });
  it('should return modified state for CLEAR_REASON_CODE action', () => {
    const action = {
      type: CUSTOMER_HELP_CONSTANTS.CLEAR_REASON_CODE,
    };
    const finalState = CustomerHelpReducer(initialState, action);
    expect(finalState.selectedReasonCode).toBeNull();
  });

  it('should return modified state for SET_ITEM_LEVEL_REFUND action', () => {
    const payload = {
      orderId: '123456',
      emailId: 'dummy@email.com',
      reasonCode: 'ITEM_MISSING',
      status: 'success',
      caseId: 'ID-1234',
      amountRefunded: '',
      items: [
        {
          lineNumber: '423242342',
          discountAmt: '4',
        },
        {
          lineNumber: '423242343',
          refundedAmt: '4',
        },
      ],
    };
    const action = {
      type: CUSTOMER_HELP_CONSTANTS.SET_ITEM_LEVEL_REFUND,
      payload,
    };
    const finalState = CustomerHelpReducer(initialState, action);
    expect(finalState.itemLevelRefund).toEqual(payload);
  });

  it('should clear the selected items', () => {
    const state = {
      selectedReasonCode: null,
      reasonCodes: [],
      selectedOrderItems: {},
    };
    const action = {
      type: CUSTOMER_HELP_CONSTANTS.SET_ITEM_LEVEL_REFUND,
    };
    const finalState = CustomerHelpReducer(state, action);
    expect(finalState.selectedOrderItems).toEqual({});
  });

  it('should be called when return method is clicked', () => {
    const payload = 'IN_STORE';
    const state = {
      selectedReasonCode: null,
      reasonCodes: [],
      selectedOrderItems: {},
      returnMethod: payload,
    };
    const action = {
      type: RETURN_ITEM_CONSTANT.SET_RETURN_METHOD,
      payload,
    };
    const finalState = CustomerHelpReducer(state, action);

    expect(finalState.returnMethod).toEqual(payload);
  });

  it('should be called when the Return Method step two component unmounts', () => {
    const state = {
      selectedReasonCode: null,
      reasonCodes: [],
      selectedOrderItems: {},
      returnMethod: null,
    };
    const action = {
      type: RETURN_ITEM_CONSTANT.CLEAR_RETURN_METHOD,
    };
    const finalState = CustomerHelpReducer(state, action);

    expect(finalState.returnMethod).toEqual(null);
  });

  it('should be called on step 1 continue button', () => {
    const payload = true;
    const state = {
      selectedReasonCode: null,
      reasonCodes: [],
      selectedOrderItems: {},
      returnMethod: null,
      isReturnItemDisplay: payload,
    };
    const action = {
      type: RETURN_ITEM_CONSTANT.IS_RETURN_ITEM_DISPLAY,
      payload,
    };
    const finalState = CustomerHelpReducer(state, action);

    expect(finalState.isReturnItemDisplay).toEqual(payload);
  });
  it('should return modified state for SET_ORDER_ARRIVE_REASON_CODE action', () => {
    const action = {
      type: CUSTOMER_HELP_CONSTANTS.SET_ORDER_ARRIVE_REASON_CODE,
      payload: {
        label: 'dummy',
        id: 'dummy',
      },
    };
    const finalState = CustomerHelpReducer(initialState, action);
    expect(finalState.selectedOrderArriveReasonCode.label).toBe(action.payload.label);
  });

  it('should return modified state for CLEAR_ORDER_ARRIVE_REASON_CODE action', () => {
    const action = {
      type: CUSTOMER_HELP_CONSTANTS.CLEAR_ORDER_ARRIVE_REASON_CODE,
    };
    const finalState = CustomerHelpReducer(initialState, action);
    expect(finalState.selectedOrderArriveReasonCode).toBeNull();
  });
});
