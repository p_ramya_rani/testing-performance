// 9fbef606107a605d69c0edbcd8029e5d
import {
  getCustomerHelpLabels,
  getReasonCodeList,
  getSelectedReasonCode,
  getSelectedOrderItems,
  getItemLevelReturns,
  getShipmentStatusData,
  getSelectedResolution,
  getResolutionModuleXContent,
  getCancelResolutionModuleXContent,
  getWrongItemState,
  getSelectedWrongItemResolution,
  getTransitHelpModuleXContent,
  getRefundStatusData,
  getCancelStatusData,
  getShipmentDelayedStatusData,
  getOrderResolutionContentId,
  getIsOrderStatusInTransit,
  getToggleCards,
  getSelectedShipmentCard,
  getFilteredList,
  getOrderSummary,
  getTopLevelReasonCodeList,
  getReasonCodeListAccountsAndCoupons,
  getReturnMethod,
  isReturnItemDisplay,
  getReturnPolicyModuleXContent,
  getReturnPolicyContentId,
  getOrderArriveSelectedReasonCode,
} from '../container/CustomerHelp.selectors';

describe('Customer Help Reducer', () => {
  it('should return labels', () => {
    const state = {
      Labels: {
        global: {
          helpCenter: 'helpCenter',
        },
      },
      reasonCodes: ['reasons'],
      selectedReasonCode: ['selectedReason'],
    };
    const labels = getCustomerHelpLabels(state);
    expect(labels).toBe(state.Labels.global.helpCenter);
  });
  it('should return modified state for SET_REFERRED_CONTENT action', () => {
    const state = {
      CustomerHelpReducer: {
        Labels: {
          global: {
            helpCenter: 'helpCenter',
          },
        },
        reasonCodes: ['reasons'],
        selectedReasonCode: ['selectedReason'],
      },
    };
    const reasonCodes = getReasonCodeList(state);
    expect(reasonCodes).toBe(state.CustomerHelpReducer.reasonCodes);
  });
  it('should return modified state for SET_REASON_CODE action', () => {
    const state = {
      CustomerHelpReducer: {
        Labels: {
          global: {
            helpCenter: 'helpCenter',
          },
        },
        reasonCodes: ['reasons'],
        selectedReasonCode: ['selectedReason'],
      },
    };
    const selectedReasonCodes = getSelectedReasonCode(state);
    expect(selectedReasonCodes).toBe(state.CustomerHelpReducer.selectedReasonCode);
  });

  it('should return modified state for SET_ORDER_ARRIVE_REASON_CODE action', () => {
    const state = {
      CustomerHelpReducer: {
        Labels: {
          global: {
            helpCenter: 'helpCenter',
          },
        },
        reasonCodes: ['reasons'],
        selectedOrderArriveReasonCode: ['selectedReason'],
      },
    };
    const selectedReasonCodes = getOrderArriveSelectedReasonCode(state);
    expect(selectedReasonCodes).toBe(state.CustomerHelpReducer.selectedOrderArriveReasonCode);
  });

  it('should return modified state for SET_REASON_CODES action', () => {
    const state = {
      CustomerHelpReducer: {
        Labels: {
          global: {
            helpCenter: 'helpCenter',
          },
        },
        topLevelReasonCodes: ['reasons'],
        accountsCouponsReasonCodes: ['reasons'],
        selectedReasonCode: ['selectedReason'],
      },
    };
    const topLevelReasonCodes = getTopLevelReasonCodeList(state);
    const accountsCouponsReasonCodes = getReasonCodeListAccountsAndCoupons(state);
    expect(topLevelReasonCodes).toBe(state.CustomerHelpReducer.topLevelReasonCodes);
    expect(accountsCouponsReasonCodes).toBe(state.CustomerHelpReducer.accountsCouponsReasonCodes);
  });

  it('should return selectedItems', () => {
    const lineNumber = '12345';
    const state = {
      CustomerHelpReducer: {
        Labels: {
          global: {
            helpCenter: 'helpCenter',
          },
        },
        selectedOrderItems: {
          [lineNumber]: 2,
        },
      },
    };
    const selectedItems = getSelectedOrderItems(state);
    expect(selectedItems).toBe(state.CustomerHelpReducer.selectedOrderItems);
  });

  it('should return getItemLevelReturns', () => {
    const state = {
      CustomerHelpReducer: {
        Labels: {
          global: {
            helpCenter: 'helpCenter',
          },
        },
        itemLevelRefund: {
          orderId: '123456',
          emailId: 'dummy@email.com',
          reasonCode: 'ITEM_MISSING',
          status: 'success',
          caseId: 'ID-1234',
          amountRefunded: '',
          items: [
            {
              lineNumber: '423242342',
              discountAmt: '4',
            },
            {
              lineNumber: '423242343',
              refundedAmt: '4',
            },
          ],
        },
      },
    };
    expect(getItemLevelReturns(state)).toBe(state.CustomerHelpReducer.itemLevelRefund);
  });
  it('should return shipment data', () => {
    const state = {
      CustomerHelpReducer: {
        ShipmentStatusData: {
          trackingDetails: [
            {
              trackingId: '420770079205590185641401861313',
              shipmentStatus: 'In-Transit',
            },
            {
              trackingId: '657895',
              shipmentStatus: 'Delivered',
            },
          ],
        },
      },
    };
    const ShipmentStatusData = getShipmentStatusData(state);
    expect(ShipmentStatusData).toBe(state.CustomerHelpReducer.ShipmentStatusData);
  });
  it('should return selectedResolution ', () => {
    const state = {
      CustomerHelpReducer: {
        selectedResolution: 'cancelOrder',
      },
    };
    const selectedResolution = getSelectedResolution(state);
    expect(selectedResolution).toBe(state.CustomerHelpReducer.selectedResolution);
  });

  it('should return selectedResolution ', () => {
    const state = {
      CustomerHelpReducer: {
        selectedWrongItemResolution: 'cancelOrder',
      },
    };
    const selectedWrongItemResolution = getSelectedWrongItemResolution(state);
    expect(selectedWrongItemResolution).toBe(state.CustomerHelpReducer.selectedWrongItemResolution);
  });
  it('should return getResolutionModuleXContent ', () => {
    const state = {
      CustomerHelpReducer: {
        resolutionModuleX: {
          name: 'moduleX',
          richText:
            '<p>Unfortunately, orders cannot be modified online or with Customer Service at this time.</p>',
        },
      },
    };
    const modifyResolutionContent = getResolutionModuleXContent(state);
    expect(modifyResolutionContent).toBe(state.CustomerHelpReducer.resolutionModuleX);
  });

  it('should return getCancelResolutionModuleXContent ', () => {
    const state = {
      CustomerHelpReducer: {
        cancelResolutionModuleX: {
          name: 'moduleX',
          richText:
            '<p>Unfortunately, orders cannot be modified online or with Customer Service at this time.</p>',
        },
      },
    };
    const modifyResolutionContent = getCancelResolutionModuleXContent(state);
    expect(modifyResolutionContent).toBe(state.CustomerHelpReducer.cancelResolutionModuleX);
  });

  it('should return getWrongItemState ', () => {
    const state = {
      CustomerHelpReducer: {
        wrongItemData: {
          comments: 'hello',
          someThingElse: 'true',
        },
      },
    };
    const wrongItemData = getWrongItemState(state);
    expect(wrongItemData).toBe(state.CustomerHelpReducer.wrongItemData);
  });
  it('should return getTransitHelpModuleXContent ', () => {
    const state = {
      CustomerHelpReducer: {
        transitHelpModuleX: {
          name: 'moduleX',
          richText: '<p>Things to try before that delivery date:</p>',
        },
      },
    };
    const transitHelpModuleXContent = getTransitHelpModuleXContent(state);
    expect(transitHelpModuleXContent).toBe(state.CustomerHelpReducer.transitHelpModuleX);
  });
  it('should return getRefundStatusData ', () => {
    const state = {
      CustomerHelpReducer: {
        refundStatusData: {
          orderId: '342710205',
          status: 'success',
          refundEligible: 'TRUE',
        },
      },
    };
    const refundStatus = getRefundStatusData(state);
    expect(refundStatus).toBe(state.CustomerHelpReducer.refundStatusData);
  });
  it('should return getCancelStatusData ', () => {
    const state = {
      CustomerHelpReducer: {
        cancelStatusData: {
          status: 'FAIL',
        },
      },
    };
    const refundStatus = getCancelStatusData(state);
    expect(refundStatus).toBe(state.CustomerHelpReducer.cancelStatusData);
  });
  it('should return getShipmentDelayedStatusData ', () => {
    const state = {
      CustomerHelpReducer: {
        shipmentDelayedRefundData: {
          status: 'FAIL',
        },
      },
    };
    const refundStatus = getShipmentDelayedStatusData(state);
    expect(refundStatus).toBe(state.CustomerHelpReducer.shipmentDelayedRefundData);
  });
  it('should return orderResolutionContentId ', () => {
    const state = {
      CustomerHelpReducer: {
        reasonCodes: ['reasons'],
        selectedReasonCode: { action: 'IN_TRANSIT' },
      },
      Labels: {
        global: {
          helpCenter: {
            referred: [
              {
                name: 'resolution_txt_in_transit',
                contentId: '1234',
              },
            ],
          },
        },
      },
    };
    const resolutionContentId = getOrderResolutionContentId(state);
    expect(resolutionContentId).toBe(state.Labels.global.helpCenter.referred[0]);
  });

  it('should return ReturnPolicyContentId ', () => {
    const state = {
      CustomerHelpReducer: {
        reasonCodes: ['reasons'],
        selectedReasonCode: { action: 'INITIATE_RETURN' },
      },
      Labels: {
        global: {
          helpCenter: {
            referred: [
              {
                name: 'returnPolicy',
                contentId: '1234',
              },
            ],
          },
        },
      },
    };
    const returnPolicyContentId = getReturnPolicyContentId(state);
    expect(returnPolicyContentId).toBe(
      state.Labels.global.helpCenter.referred.find((label) => label.name === `returnPolicy`)
    );
  });
});

it('should return getIsOrderStatusInTransit ', () => {
  const state = {
    CustomerHelpReducer: {
      isOrderStatusInTransit: false,
    },
  };
  const isOrderStatusInTransit = getIsOrderStatusInTransit(state);
  expect(isOrderStatusInTransit).toBe(state.CustomerHelpReducer.isOrderStatusInTransit);
});

it('should return getToggleCards ', () => {
  const state = {
    CustomerHelpReducer: {
      toggleCards: false,
    },
  };
  const toggleCards = getToggleCards(state);
  expect(toggleCards).toBe(state.CustomerHelpReducer.toggleCards);
});

it('should return getSelectedShipmentCard ', () => {
  const state = {
    CustomerHelpReducer: {
      selectedShipmentCard: {},
    },
  };
  const selectedShipmentCard = getSelectedShipmentCard(state);
  expect(selectedShipmentCard).toBe(state.CustomerHelpReducer.selectedShipmentCard);
});

it('should return getFilteredList ', () => {
  const state = {
    CustomerHelpReducer: {
      filteredList: [],
    },
  };
  const filteredList = getFilteredList(state);
  expect(filteredList).toBe(state.CustomerHelpReducer.filteredList);
});

it('should return getOrderSummary ', () => {
  const state = {
    CustomerHelpReducer: {
      orderSummary: {},
    },
  };
  const orderSummary = getOrderSummary(state);
  expect(orderSummary).toBe(state.CustomerHelpReducer.orderSummary);
});

it('should return returnPolicyModuleX ', () => {
  const state = {
    CustomerHelpReducer: {
      returnPolicyModuleX: {},
    },
  };
  const returnPolicyModuleX = getReturnPolicyModuleXContent(state);
  expect(returnPolicyModuleX).toBe(state.CustomerHelpReducer.returnPolicyModuleX);
});

it('Return Item should return ReturnMethod selected', () => {
  const state = {
    CustomerHelpReducer: {
      returnMethod: null,
    },
  };
  const returnMethod = getReturnMethod(state);

  expect(returnMethod).toBe(state.CustomerHelpReducer.returnMethod);
});

it('Return Item should return Is return item display boolean ', () => {
  const state = {
    CustomerHelpReducer: {
      isReturnItemDisplay: false,
    },
  };
  const isReturnItemDisplayVal = isReturnItemDisplay(state);

  expect(isReturnItemDisplayVal).toBe(state.CustomerHelpReducer.isReturnItemDisplay);
});
