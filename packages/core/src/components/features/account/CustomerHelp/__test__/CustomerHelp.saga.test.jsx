// 9fbef606107a605d69c0edbcd8029e5d
import {
  selectOrder,
  getItemsRefund,
  getShipmentStatusData,
  fetchResolutionModuleX,
  fetchCancelResolutionModuleX,
  fetchTransitResolutionModuleX,
  getRefundStatusData,
  getCancelStatusData,
  getShipmentDelayedRefundData,
  fetchReasonCodes,
  fetchReturnPolicyModuleX,
} from '../container/CustomerHelp.saga';

describe('Customer Help Saga', () => {
  it('should call selectOrder generator function of Customer Help saga', () => {
    const selectOrderGen = selectOrder({});
    selectOrderGen.next();
    selectOrderGen.next();
    selectOrderGen.next();
    selectOrderGen.next();
    selectOrderGen.next();
    selectOrderGen.next();
    expect(selectOrderGen.next().done).toBe(true);
  });

  it('should call selectOrder generator function of Customer Help saga with navigation | Mobile App', () => {
    const selectOrderGen = selectOrder({ navigation: {} });
    selectOrderGen.next();
    selectOrderGen.next();
    selectOrderGen.next();
    selectOrderGen.next();
    selectOrderGen.next();
    selectOrderGen.next();
    selectOrderGen.next();
    expect(selectOrderGen.next().done).toBe(true);
  });

  it('should call getItemsRefund generator function of Customer Help saga', () => {
    const itemRefund = getItemsRefund({ payload: {} });
    itemRefund.next();
    itemRefund.next();
    itemRefund.next();
    itemRefund.next();
    itemRefund.next();
    itemRefund.next();
    itemRefund.next();
    itemRefund.next();
    expect(itemRefund.next().done).toBe(true);
  });

  it('should call getCancelStatusData generator function of Customer Help saga', () => {
    const itemRefund = getCancelStatusData({ payload: {} });
    itemRefund.next();
    itemRefund.next();
    itemRefund.next();
    itemRefund.next();
    itemRefund.next();
    itemRefund.next();
    itemRefund.next();
    itemRefund.next();
    expect(itemRefund.next().done).toBe(true);
  });

  it('should call getShipmentStatusData of Customer Help saga', () => {
    const gen = getShipmentStatusData({ payload: {} });
    gen.next();
    gen.next();
    gen.next();
    gen.next();
    gen.next();
    expect(gen.next().done).toBe(true);
  });

  it('should call fetchResolutionModuleX of Customer Help saga', () => {
    const gen = fetchResolutionModuleX({ payload: {} });
    gen.next();
    gen.next();
    gen.next();
    expect(gen.next().done).toBe(true);
  });

  it('should call fetchCancelResolutionModuleX of Customer Help saga', () => {
    const gen = fetchCancelResolutionModuleX({ payload: {} });
    gen.next();
    gen.next();
    gen.next();
    expect(gen.next().done).toBe(true);
  });

  it('should call fetchReturnPolicyModuleX of Customer Help saga', () => {
    const gen = fetchReturnPolicyModuleX({ payload: {} });
    gen.next();
    gen.next();
    gen.next();
    expect(gen.next().done).toBe(true);
  });

  it('should call fetchTransitResolutionModuleX of Customer Help saga', () => {
    const gen = fetchTransitResolutionModuleX({ payload: {} });
    gen.next();
    gen.next();
    gen.next();
    expect(gen.next().done).toBe(true);
  });

  it('should call getRefundStatusData of Customer Help saga', () => {
    const gen = getRefundStatusData({ payload: {} });
    gen.next();
    gen.next();
    gen.next();
    gen.next();
    gen.next();
    expect(gen.next().done).toBe(true);
  });

  it('should call fetchreasonCodes of Customer Help saga', () => {
    const gen = fetchReasonCodes({ payload: {} });
    gen.next();
    gen.next();
    gen.next();
    expect(gen.next().done).toBe(true);
  });

  it('should call getShipmentDelayedRefundData of Customer Help saga', () => {
    const gen = getShipmentDelayedRefundData({ payload: {} });
    gen.next();
    gen.next();
    gen.next();
    gen.next();
    gen.next();
    gen.next();
    gen.next();
    gen.next();
    expect(gen.next().done).toBe(true);
  });
});
