// 9fbef606107a605d69c0edbcd8029e5d
import router from 'next/router';
import * as utilities from '../util/utility';

jest.mock('next/router', () => {
  return {
    __esModule: true,
    default: {
      push: jest.fn(),
    },
  };
});

describe('Utility functions', () => {
  it('move to a particular stage', () => {
    utilities.moveToStage('tell-us-more');
    expect(router.push).toBeCalled();
  });
});

describe('Utility function getAppeasedShipmentStatus', () => {
  it('should call getAppeasedShipmentStatus action of Customer Help utility', () => {
    const getAppeasedShipmentStatus = utilities.getAppeasedShipmentStatus({});
    expect(getAppeasedShipmentStatus).toBe(null);
  });
});

describe('Utility function getConditionalWarnMsg', () => {
  it('should call getConditionalWarnMsg action of Customer Help utility', () => {
    const getConditionalWarnMsg = utilities.getConditionalWarnMsg({});
    expect(getConditionalWarnMsg).toBe(null);
  });
});

describe('Utility function getWarningMsg', () => {
  it('should call getWarningMsg action of Customer Help utility', () => {
    const getWarningMsg = utilities.getWarningMsg({});
    expect(getWarningMsg).toBe(null);
  });
});

describe('Utility function getOrderTileItems', () => {
  it('should call getOrderTileItems action of Customer Help utility', () => {
    const getOrderTileItems = utilities.getOrderTileItems({});
    expect(getOrderTileItems).toBe(null);
  });
});

describe('Utility function getRefundedShipmentStatus', () => {
  it('should call getRefundedShipmentStatus action of Customer Help utility', () => {
    const getRefundedShipmentStatus = utilities.getWarningMsg({});
    expect(getRefundedShipmentStatus).toBe(null);
  });
});

describe('Utility function getMaxDeliveryDate', () => {
  it('should call getMaxDeliveryDate action of Customer Help utility', () => {
    const getMaxDeliveryDate = utilities.getWarningMsg({});
    expect(getMaxDeliveryDate).toBe(null);
  });
});
describe('Utility function isOldItem', () => {
  it('should call isOldItem action of Customer Help utility', () => {
    const isOldItem = utilities.getWarningMsg({});
    expect(isOldItem).toBe(null);
  });
});
