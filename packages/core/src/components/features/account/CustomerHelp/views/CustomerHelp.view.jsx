// 9fbef606107a605d69c0edbcd8029e5d
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { getIconPath, getLabelValue } from '@tcp/core/src/utils/utils';
import { Row, Col, BodyCopy, Anchor, Button } from '@tcp/core/src/components/common/atoms';
import { routerPush, smoothScrolling } from '@tcp/core/src/utils';
import withStyles from '../../../../common/hoc/withStyles';
import styles from '../styles/CustomerHelp.style';
import {
  CUSTOMER_HELP_PAGE_TEMPLATE,
  CUSTOMER_HELP_ROUTES,
  REASON_CODES,
  REASON_CODES_REDUX_KEY,
  REASON_CODES_REFERRED_INDEX,
} from '../CustomerHelp.constants';
import CustomerHelpProgressBar from '../organisms/CustomerHelpProgressIndicator';
import SelectOrder from '../organisms/SelectOrder';
import TellUsMore from '../organisms/TellUsMore';
import Review from '../organisms/Review';
import Confirmation from '../organisms/Confirmation';
import CustomerHelpLanding from '../organisms/CustomerHelpLanding';
import { routeToPage, isAccountAndCouponURL } from '../util/utility';
import AccountAndCoupons from '../organisms/AccountAndCoupons';
import ReasonCodeWithIcon from '../molecules/ReasonCodeWithIcon';
import OrderTileSkeleton from '../organisms/SelectOrder/skeleton';
import MergeAccountConfirmation from '../organisms/MergeAccountConfirmation';
import MERGE_ACCOUNT_CONSTANTS from '../organisms/MergeAccounts/MergeAccounts.constants';

const workFlowTemplates = {
  [CUSTOMER_HELP_PAGE_TEMPLATE.LANDING_PAGE]: CustomerHelpLanding,
  [CUSTOMER_HELP_PAGE_TEMPLATE.SELECT_ORDER]: SelectOrder,
  [CUSTOMER_HELP_PAGE_TEMPLATE.TELL_US_MORE]: TellUsMore,
  [CUSTOMER_HELP_PAGE_TEMPLATE.REVIEW]: Review,
  [CUSTOMER_HELP_PAGE_TEMPLATE.CONFIRMATION]: Confirmation,
  [CUSTOMER_HELP_PAGE_TEMPLATE.ACCOUNT_COUPONS]: AccountAndCoupons,
};

const getCurrentTemplate = (templateName) => workFlowTemplates[templateName];

const getTemplateName = (router) => {
  if (isAccountAndCouponURL(router)) {
    return CUSTOMER_HELP_PAGE_TEMPLATE.ACCOUNT_COUPONS;
  }
  return router?.query?.section || CUSTOMER_HELP_PAGE_TEMPLATE.LANDING_PAGE;
};

const getClass = (fullPageSize) => {
  return fullPageSize ? 'side-area' : '';
};

const isFullPageSize = (templateName) => {
  return (
    templateName === CUSTOMER_HELP_PAGE_TEMPLATE.REVIEW ||
    templateName === CUSTOMER_HELP_PAGE_TEMPLATE.CONFIRMATION
  );
};

const getLandingPageProps = (
  isLandingPage,
  isAccountAndCouponPage,
  isTellUsMorePage,
  setSelectedTopReasonCode,
  selectedTopReasonCode,
  topLevelReasonCodes,
  resetPasswordError
) => {
  return {
    ...((isLandingPage || isAccountAndCouponPage || isTellUsMorePage) && {
      setSelectedTopReasonCode,
      selectedTopReasonCode,
      topLevelReasonCodes,
      resetPasswordError,
    }),
  };
};

const getWorkFlowClass = (isSelectOrderPage) => (isSelectOrderPage ? 'full-width' : '');

const renderProgressBar = (orderLabels, isReviewPage, activeSection, selectedTopReasonCode) => {
  return (
    selectedTopReasonCode &&
    selectedTopReasonCode.action === REASON_CODES.EXISTING_ORDER && (
      <Row className="progressBarContainer">
        <Col
          className="progressBarArea"
          colSize={{ small: 6, medium: 8, large: 12 }}
          offsetLeft={{ large: 2 }}
          offsetRight={{ large: 2 }}
        >
          <CustomerHelpProgressBar activeSection={isReviewPage ? undefined : activeSection} />
        </Col>
        {isReviewPage && (
          <BodyCopy component="div" className="back-button-link">
            <Anchor
              to={CUSTOMER_HELP_ROUTES.TELL_US_MORE.to}
              asPath={CUSTOMER_HELP_ROUTES.TELL_US_MORE.path}
              fontSizeVariation="xlarge"
              anchorVariation="primary"
              data-locator="backLink"
            >
              <span className="left-arrow" />
              {getLabelValue(orderLabels?.orders, 'lbl_orderDetails_back')}
            </Anchor>
          </BodyCopy>
        )}
      </Row>
    )
  );
};

const renderSelectedHeader = (
  selectedTopReasonCode,
  labels,
  setSelectedTopReasonCode,
  setActiveSection,
  orderLabels,
  loader,
  setLoader
) => {
  const fallbackImg = getIconPath('essential-style-fallback');
  const helpIcon = getIconPath('question-circle');
  const orderIcon = getIconPath('package');
  const accountIcon = getIconPath('user-icon');

  const iconMap = {
    EXISTING_ORDER: orderIcon,
    ACCOUNT_AND_COUPONS: accountIcon,
    SOMETHING_ELSE: helpIcon,
  };
  return (
    selectedTopReasonCode && (
      <>
        <Row fullBleed>
          <Col colSize={{ small: 6, medium: 7, large: 12 }} className="elem-mb-SM">
            <div className="header-container top-header-container">
              <div className="header-title">
                <BodyCopy component="p" fontWeight="bold" fontFamily="secondary" fontSize="fs16">
                  {getLabelValue(labels, 'lbl_how_we_can_help', '')}
                </BodyCopy>
              </div>

              <Button
                className="edit-option"
                nohover
                type="button"
                link
                underline
                color="blue"
                onClick={() => {
                  setSelectedTopReasonCode(null);
                  setActiveSection(undefined);
                  if (loader) {
                    setLoader(false);
                  }
                  routeToPage(CUSTOMER_HELP_ROUTES.LANDING_PAGE);
                }}
              >
                {getLabelValue(orderLabels, 'lbl_edit_option', 'orders')}
              </Button>
            </div>
          </Col>
        </Row>
        <Row fullBleed>
          <ReasonCodeWithIcon
            title={selectedTopReasonCode.label}
            fallbackImg={fallbackImg}
            icon={iconMap[selectedTopReasonCode.action]}
            selectedReasonCodeClasses="selected-reason selected-item elem-mb-XL elem-mt-SM"
            containerId="selected-item"
            titleClasses="elem-pl-MED elem-pr-MED textColor"
            fontSize="fs14"
            fontWeight="semibold"
            titleTextColor="text.dark"
          />
        </Row>
      </>
    )
  );
};
const renderCSHWorkflowTemplate = ({
  fullPageSize,
  WorkFlowTemplate,
  props,
  selectedTopReasonCode,
  labels,
  setSelectedTopReasonCode,
  setActiveSection,
  orderLabels,
  isSelectOrderPage,
  loader,
  setLoader,
  landingPageProps,
  setSelectedAccountsReasonCode,
  selectedAccountsReasonCode,
}) => {
  return (
    <Row className={getClass(fullPageSize)}>
      {fullPageSize ? (
        <Col colSize={{ small: 6, medium: 8, large: 12 }} ignoreGutter={{ large: true }}>
          <WorkFlowTemplate {...props} />
        </Col>
      ) : (
        <>
          <Col
            colSize={{ small: 6, medium: 8, large: 9 }}
            offsetLeft={{ large: 1 }}
            offsetRight={{ large: 2 }}
            ignoreGutter={{ large: true }}
          >
            {renderSelectedHeader(
              selectedTopReasonCode,
              labels,
              setSelectedTopReasonCode,
              setActiveSection,
              orderLabels,
              loader,
              setLoader
            )}
          </Col>
          <Col
            colSize={{ small: 6, medium: 8, large: 9 }}
            offsetLeft={{ large: 1 }}
            offsetRight={{ large: 2 }}
            ignoreGutter={{ large: true }}
            className={getWorkFlowClass(isSelectOrderPage)}
          >
            {loader && (
              <Row fullBleed>
                <OrderTileSkeleton />
              </Row>
            )}
            <WorkFlowTemplate
              {...props}
              {...landingPageProps}
              setLoader={setLoader}
              setActiveSection={setActiveSection}
              setSelectedAccountsReasonCode={setSelectedAccountsReasonCode}
              selectedAccountsReasonCode={selectedAccountsReasonCode}
            />
          </Col>
        </>
      )}
    </Row>
  );
};

const noProgressHeading = (isConfirmationPage, selectedReasonCode) => {
  return isConfirmationPage && selectedReasonCode?.action === REASON_CODES.INITIATE_RETURN
    ? 'no-progress-margin'
    : '';
};

const CustomerHelp = (props) => {
  const {
    router,
    className,
    labels,
    fetchOrders,
    orderLabels,
    currentPageNumber,
    clearSelectedOrderItems,
    clearSelectedReasonCode,
    clearSelectedResolution,
    isLoggedIn,
    selectOrderAction,
    topLevelReasonCodes,
    getReasonCodeContentByContentId,
    resetPasswordError,
    accountStatus,
    mergeStatus,
    mergeError,
    mergeFromOtpFlow,
    clearMergeAccountFromOTP,
    mergeAccountReset,
    survivorEmailAddress,
    victimEmailAddress,
    isLiveChatEnabled,
    isInternationalShipping,
    resetEmailConfirmation,
    selectedReasonCode,
    trackAnalyticsPageView,
    trackAnalyticsClick,
  } = props;
  const [selectedTopReasonCode, setSelectedTopReasonCode] = useState(null);
  const [selectedAccountsReasonCode, setSelectedAccountsReasonCode] = useState(null);
  const templateName = getTemplateName(router);
  const WorkFlowTemplate = getCurrentTemplate(templateName);
  const fullPageSize = isFullPageSize(templateName);
  const isReviewPage = templateName === CUSTOMER_HELP_PAGE_TEMPLATE.REVIEW;
  const isConfirmationPage = templateName === CUSTOMER_HELP_PAGE_TEMPLATE.CONFIRMATION;
  const isSelectOrderPage = templateName === CUSTOMER_HELP_PAGE_TEMPLATE.SELECT_ORDER;
  const isTellUsMorePage = templateName === CUSTOMER_HELP_PAGE_TEMPLATE.TELL_US_MORE;
  const isLandingPage = templateName === CUSTOMER_HELP_PAGE_TEMPLATE.LANDING_PAGE;
  const isAccountAndCouponPage = templateName === CUSTOMER_HELP_PAGE_TEMPLATE.ACCOUNT_COUPONS;

  const [activeSection, setActiveSection] = useState(undefined);
  const [loader, setLoader] = useState(false);
  const landingPageProps = getLandingPageProps(
    isLandingPage,
    isAccountAndCouponPage,
    isTellUsMorePage,
    setSelectedTopReasonCode,
    selectedTopReasonCode,
    topLevelReasonCodes,
    resetPasswordError
  );
  useEffect(() => {
    if (
      typeof labels === 'object' &&
      labels?.referred[REASON_CODES_REFERRED_INDEX.TOP_LEVEL_REASON_CODES]
    ) {
      getReasonCodeContentByContentId({
        contentId: labels.referred[REASON_CODES_REFERRED_INDEX.TOP_LEVEL_REASON_CODES].contentId,
        reduxKey: REASON_CODES_REDUX_KEY.TOP_LEVEL_REASON_CODES,
      });
    }

    if (
      typeof labels === 'object' &&
      labels?.referred[REASON_CODES_REFERRED_INDEX.ACCOUNT_COUPONS_REASON_CODES]
    ) {
      getReasonCodeContentByContentId({
        contentId:
          labels.referred[REASON_CODES_REFERRED_INDEX.ACCOUNT_COUPONS_REASON_CODES].contentId,
        reduxKey: REASON_CODES_REDUX_KEY.ACCOUNT_COUPONS_REASON_CODES,
      });
    }
    if (loader) setLoader(false);
  }, []);

  useEffect(() => {
    if (selectedTopReasonCode && selectedTopReasonCode.action !== REASON_CODES.EXISTING_ORDER) {
      smoothScrolling('customer-help-support');
    }
  }, [selectedTopReasonCode]);

  useEffect(() => {
    if (fullPageSize && isLoggedIn !== null) {
      if (isLoggedIn) {
        selectOrderAction();
      } else {
        routerPush('/', '/home');
      }
    }
    if (isSelectOrderPage && isLoggedIn !== null) {
      selectOrderAction();
    }
  }, [isLoggedIn]);

  // to set reasoncode in case user is not logged in and select order or tellusmore section is loaded
  useEffect(() => {
    if (!isLoggedIn && (isSelectOrderPage || isTellUsMorePage)) {
      const reason =
        topLevelReasonCodes &&
        topLevelReasonCodes.find((rc) => rc.action === REASON_CODES.EXISTING_ORDER);
      if (reason) {
        setSelectedTopReasonCode(reason);
      }
    }
  }, [isLoggedIn, isSelectOrderPage, isTellUsMorePage]);

  useEffect(() => {
    if (router?.query?.section === CUSTOMER_HELP_PAGE_TEMPLATE.SELECT_ORDER) {
      const reasonCodeToSet =
        topLevelReasonCodes &&
        topLevelReasonCodes.filter((rcode) => {
          return rcode.action === REASON_CODES.EXISTING_ORDER;
        });
      setSelectedTopReasonCode(reasonCodeToSet[0]);
    }
  }, [isSelectOrderPage, topLevelReasonCodes]);

  useEffect(() => {
    const orderHistoryPayload = {
      reqParams: {
        itemdetails: true,
        pageNumber: currentPageNumber,
        pageSize: CUSTOMER_HELP_PAGE_TEMPLATE.ORDER_SIZE,
      },
    };

    if (currentPageNumber > 0) {
      orderHistoryPayload.fromLoadMore = true;
      fetchOrders(orderHistoryPayload);
    }

    return () => {
      clearSelectedOrderItems();
      clearSelectedReasonCode();
      clearSelectedResolution();
    };
  }, [currentPageNumber]);

  useEffect(() => {
    if (isLandingPage) {
      if (selectedTopReasonCode) {
        setSelectedTopReasonCode(null);
      }
      if (activeSection) {
        setActiveSection(undefined);
      }
    }
  }, [isLandingPage]);
  const showMergeConfirmationScreen =
    accountStatus !== MERGE_ACCOUNT_CONSTANTS.ACCOUNT_STATUS[0] || mergeError || mergeStatus;

  return (
    <div className={className} id="customer-help-support">
      <Row>
        <Col
          colSize={{ small: 6, medium: 8, large: 12 }}
          offsetLeft={{ large: 1 }}
          offsetRight={{ large: 2 }}
          className="heading"
        >
          <BodyCopy
            className={`set-align elem-mb-MED elem-mt-XXXS ${noProgressHeading(
              isConfirmationPage,
              selectedReasonCode
            )}`}
            component="p"
            fontWeight="bold"
            fontFamily="secondary"
            fontSize="fs18"
          >
            {getLabelValue(labels, 'lbl_csh_customer_service_support')}
          </BodyCopy>
        </Col>
      </Row>
      {showMergeConfirmationScreen && (
        <MergeAccountConfirmation
          labels={labels}
          mergeAccountReset={mergeAccountReset}
          accountStatus={accountStatus}
          mergeStatus={mergeStatus}
          mergeApiError={mergeError}
          mergeFromOtpFlow={mergeFromOtpFlow}
          clearMergeAccountFromOTP={clearMergeAccountFromOTP}
          survivorEmailAddress={survivorEmailAddress}
          victimEmailAddress={victimEmailAddress}
          isLiveChatEnabled={isLiveChatEnabled}
          isInternationalShipping={isInternationalShipping}
          resetEmailConfirmation={resetEmailConfirmation}
          setSelectedAccountsReasonCode={setSelectedAccountsReasonCode}
          trackAnalyticsPageView={trackAnalyticsPageView}
          trackAnalyticsClick={trackAnalyticsClick}
        />
      )}
      {!showMergeConfirmationScreen && (
        <>
          {!(isConfirmationPage && selectedReasonCode?.action === REASON_CODES.INITIATE_RETURN) &&
            renderProgressBar(orderLabels, isReviewPage, activeSection, selectedTopReasonCode)}
          {renderCSHWorkflowTemplate({
            fullPageSize,
            WorkFlowTemplate,
            props,
            selectedTopReasonCode,
            labels,
            setSelectedTopReasonCode,
            setActiveSection,
            orderLabels,
            isSelectOrderPage,
            loader,
            setLoader,
            landingPageProps,
            setSelectedAccountsReasonCode,
            selectedAccountsReasonCode,
          })}
        </>
      )}
    </div>
  );
};

CustomerHelp.propTypes = {
  className: PropTypes.string,
  accountStatus: PropTypes.string.isRequired,
  router: PropTypes.shape({}),
  labels: PropTypes.shape({}),
  currentPageNumber: PropTypes.number,
  fetchOrders: PropTypes.func,
  orderLabels: PropTypes.shape({}),
  clearSelectedOrderItems: PropTypes.func,
  clearSelectedReasonCode: PropTypes.func,
  clearSelectedResolution: PropTypes.func,
  getOrderDetailsAction: PropTypes.func.isRequired,
  isLoggedIn: PropTypes.bool,
  selectOrderAction: PropTypes.func,
  orderDetailsData: PropTypes.shape({}),
  mergeStatus: PropTypes.shape({}),
  mergeError: PropTypes.shape({}),
  isTellUsMorePage: PropTypes.bool,
  isLiveChatEnabled: PropTypes.bool.isRequired,
  topLevelReasonCodes: PropTypes.shape({}).isRequired,
  mergeAccountReset: PropTypes.func.isRequired,
  resetEmailConfirmation: PropTypes.func.isRequired,
  survivorEmailAddress: PropTypes.string,
  victimEmailAddress: PropTypes.string,
};
CustomerHelp.defaultProps = {
  className: '',
  router: {},
  labels: {},
  currentPageNumber: 0,
  fetchOrders: () => {},
  orderDetailsData: {},
  mergeStatus: {},
  mergeError: {},
  orderLabels: {},
  clearSelectedOrderItems: () => {},
  clearSelectedReasonCode: () => {},
  clearSelectedResolution: () => {},
  isLoggedIn: false,
  selectOrderAction: () => {},
  isTellUsMorePage: false,
  survivorEmailAddress: '',
  victimEmailAddress: '',
};

export default withStyles(CustomerHelp, styles);
export { CustomerHelp as CustomerHelpVanilla };
