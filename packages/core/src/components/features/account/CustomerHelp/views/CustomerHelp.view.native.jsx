// 9fbef606107a605d69c0edbcd8029e5d
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import { BodyCopyWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import { SelectOrder, TellUsMore, Review, Confirmation } from '../organisms';
import { CUSTOMER_HELP_PAGE_TEMPLATE } from '../CustomerHelp.constants';
import CustomerHelpProgressBar from '../organisms/CustomerHelpProgressIndicator/container/CustomerHelpProgressIndicator.container.native';

const workFlowTemplates = {
  [CUSTOMER_HELP_PAGE_TEMPLATE.SELECT_ORDER]: SelectOrder,
  [CUSTOMER_HELP_PAGE_TEMPLATE.TELL_US_MORE]: TellUsMore,
  [CUSTOMER_HELP_PAGE_TEMPLATE.REVIEW]: Review,
  [CUSTOMER_HELP_PAGE_TEMPLATE.CONFIRMATION]: Confirmation,
};

const CustomerHelp = props => {
  const { navigation, labels, fetchOrders, currentPageNumber } = props;
  const route = get(navigation, 'state.routeName', false);
  const section = route || CUSTOMER_HELP_PAGE_TEMPLATE.SELECT_ORDER;
  const WorkFlowTemplate = workFlowTemplates[section];

  useEffect(() => {
    const orderHistoryPayload = {
      reqParams: {
        itemdetails: true,
        pageNumber: currentPageNumber,
        pageSize: CUSTOMER_HELP_PAGE_TEMPLATE.ORDER_SIZE,
      },
    };

    if (currentPageNumber > 0) {
      orderHistoryPayload.fromLoadMore = true;
    }

    fetchOrders(orderHistoryPayload);
  }, [currentPageNumber]);

  return (
    <>
      <BodyCopyWithSpacing
        spacingStyles="margin-top-MED"
        text={getLabelValue(labels, 'lbl_order_help_capital')}
        className="set-align"
        component="p"
        fontWeight="bold"
        fontFamily="secondary"
        fontSize="fs16"
        textAlign="center"
      />
      {section !== CUSTOMER_HELP_PAGE_TEMPLATE.CONFIRMATION && (
        <CustomerHelpProgressBar activeStage={section} navigation={navigation} />
      )}
      <WorkFlowTemplate {...props} />
    </>
  );
};

CustomerHelp.propTypes = {
  navigation: PropTypes.shape({}),
  labels: PropTypes.shape({}).isRequired,
  currentPageNumber: PropTypes.number,
  fetchOrders: PropTypes.func,
};
CustomerHelp.defaultProps = {
  navigation: {},
  currentPageNumber: 0,
  fetchOrders: () => {},
};

export default CustomerHelp;
