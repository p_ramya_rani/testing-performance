// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { CustomerHelpVanilla } from '../CustomerHelp.view';

describe('Customer Help view', () => {
  it('should render Customer help view', () => {
    const component = shallow(<CustomerHelpVanilla />);
    expect(component).toMatchSnapshot();
  });
});
