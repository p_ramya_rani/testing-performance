// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

const SkeletonContainer = styled.View`
  height: 58px;
  display: flex;
  margin: ${props => props.theme.spacing.ELEM_SPACING.XS}
    ${props => props.theme.spacing.ELEM_SPACING.XS};
`;

export default SkeletonContainer;
