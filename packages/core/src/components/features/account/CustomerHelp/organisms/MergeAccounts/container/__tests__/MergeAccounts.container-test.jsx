// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import MergeAccountsContainer from '../MergeAccounts.container';

describe('MergeAccountsContainer', () => {
  const props = {
    labels: {},
  };
  it('should render correctly', () => {
    const component = shallow(<MergeAccountsContainer {...props} />);
    expect(component).toMatchSnapshot();
  });
});
