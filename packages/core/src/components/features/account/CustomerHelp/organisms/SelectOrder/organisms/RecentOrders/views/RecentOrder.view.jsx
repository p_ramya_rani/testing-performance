// 9fbef606107a605d69c0edbcd8029e5d
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import toLower from 'lodash/toLower';
import { Row, Col, BodyCopy, Image } from '@tcp/core/src/components/common/atoms';
import { getLabelValue, getSiteId, getIconPath, isClient } from '@tcp/core/src/utils/utils';
import {
  CUSTOMER_HELP_ROUTES,
  ORDER_ITEM_STATUS,
  CUSTOMER_HELP_PAGE_TEMPLATE,
} from '@tcp/core/src/components/features/account/CustomerHelp/CustomerHelp.constants';
import { routerPush, smoothScrolling } from '@tcp/core/src/utils';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { getOrderTileItems } from '../../../../../util/utility';
import OrderTile from '../../../../../molecules/OrderTile';
import OrderTileSkeleton from '../../../skeleton';
import styles from '../styles/RecentOrder.style';

const scrollTo = (ordersListItems, currentPageNumber, seeMore) => {
  let section = '';
  if (ordersListItems && ordersListItems.orders && ordersListItems.orders.length > 1) {
    const pageNumber = currentPageNumber * CUSTOMER_HELP_PAGE_TEMPLATE.ORDER_SIZE + 1;
    section = seeMore && currentPageNumber > 0 ? `pageNumber_${pageNumber}` : 'orderTileHeader';
    if (section) {
      setTimeout(() => {
        smoothScrolling(section);
      }, 100);
    }
  }
};

const RecentOrders = ({
  ordersListItems,
  orderLabels,
  labels,
  className,
  cshForMonthsToDisplay,
  clearSelectedReasonCode,
  clearSelectedResolution,
  clearSelectedOrderItems,
  isFetching,
  fromSelectOrder,
  setOrderStatusInTransit,
  setToggleCards,
  setSelectedShipmentCard,
  setFilteredList,
  seeMore,
  currentPageNumber,
}) => {
  const navigateToTellUsMore = (item) => {
    routerPush(
      `${CUSTOMER_HELP_ROUTES.TELL_US_MORE.to}&orderId=${item.orderNumber}`,
      `${CUSTOMER_HELP_ROUTES.TELL_US_MORE.asPath}/${item.orderNumber}`
    );
  };

  const handleOrderItems = (e, item) => {
    const { isEcomOrder, isOlderItem, status, grandTotal } = getOrderTileItems({
      item,
      cshForMonthsToDisplay,
    });
    const { CANCELLED, CANCELED } = ORDER_ITEM_STATUS;
    const isOrderCancelled =
      toLower(status) === toLower(CANCELLED) || toLower(status) === toLower(CANCELED);
    const grandTotalNonZero = !!grandTotal;
    if (isEcomOrder && !isOlderItem && !isOrderCancelled && grandTotalNonZero) {
      clearSelectedReasonCode();
      clearSelectedResolution();
      clearSelectedOrderItems();
      setOrderStatusInTransit(null);
      setToggleCards(false);
      setSelectedShipmentCard(null);
      setFilteredList(null);
      navigateToTellUsMore(item);
    }

    e.stopPropagation();
  };
  const customerServiceHref = () => {
    return `/${getSiteId()}${getLabelValue(labels, 'lbl_contact_us_href', 'orders')}`;
  };

  if (!isClient()) {
    return null;
  }

  useEffect(() => {
    if (
      ordersListItems &&
      ordersListItems.orders &&
      ordersListItems.orders.length > 1 &&
      !seeMore
    ) {
      setTimeout(() => {
        smoothScrolling('orderTileHeader');
      }, 100);
    }
  }, []);

  useEffect(() => {
    scrollTo(ordersListItems, currentPageNumber, seeMore);
  }, [ordersListItems]);

  const getPageId = (index) => {
    return index > 0 && index % CUSTOMER_HELP_PAGE_TEMPLATE.ORDER_SIZE === 0
      ? `pageNumber_${index + 1}`
      : '';
  };
  return (
    <div className={className}>
      {ordersListItems && ordersListItems.orders && ordersListItems.orders.length > 1 ? (
        <>
          <Row fullBleed id="orderTileHeader">
            <Col className="orderTileHeader" colSize={{ small: 6, medium: 4, large: 5 }}>
              <BodyCopy component="p" fontWeight="bold" fontFamily="secondary" fontSize="fs16">
                {getLabelValue(labels, 'lbl_your_order')}
              </BodyCopy>
            </Col>
          </Row>
          <Row fullBleed>
            {!isFetching ? (
              ordersListItems.orders.map((item, index) => (
                <Col
                  id={getPageId(index)}
                  colSize={{ small: 6, medium: 3, large: 5 }}
                  className="elem-mt-MED bigger-container"
                  onClick={(e) => handleOrderItems(e, item)}
                >
                  <OrderTile
                    orderLabels={orderLabels}
                    orderDetailsData={getOrderTileItems({ item, cshForMonthsToDisplay })}
                    labels={labels}
                    fromSelectOrder={fromSelectOrder}
                  />
                </Col>
              ))
            ) : (
              <Row fullBleed>
                <OrderTileSkeleton />
              </Row>
            )}
          </Row>
        </>
      ) : (
        <Row fullBleed>
          <Col colSize={{ small: 6, medium: 3, large: 5 }} className="no-order-content">
            <Image
              className="empty-box"
              width="56.2px"
              height="53.4px"
              src={getIconPath('no-order')}
              alt="empty-box"
            />
            <BodyCopy
              component="p"
              className="no-order-message"
              textAlign="center"
              fontWeight="semibold"
              fontSize="fs14"
              fontFamily="secondary"
              color="text.darkgray"
            >
              {getLabelValue(labels, 'lbl_no_orders_initial', 'orders')}
              &nbsp;
              <a href={customerServiceHref()}>
                {getLabelValue(labels, 'lbl_contact_us', 'orders')}
              </a>
              &nbsp;
              {getLabelValue(labels, 'lbl_no_orders_ending', 'orders')}
            </BodyCopy>
          </Col>
        </Row>
      )}
    </div>
  );
};

RecentOrders.propTypes = {
  ordersListItems: PropTypes.shape([]).isRequired,
  orderLabels: PropTypes.shape({}).isRequired,
  labels: PropTypes.shape({}).isRequired,
  className: PropTypes.string,
  cshForMonthsToDisplay: PropTypes.number,
  clearSelectedOrderItems: PropTypes.func,
  clearSelectedReasonCode: PropTypes.func,
  clearSelectedResolution: PropTypes.func,
  isFetching: PropTypes.bool,
  fromSelectOrder: PropTypes.bool,
  setOrderStatusInTransit: PropTypes.func,
  setToggleCards: PropTypes.func,
  setSelectedShipmentCard: PropTypes.func,
  setFilteredList: PropTypes.func,
};
RecentOrders.defaultProps = {
  className: '',
  cshForMonthsToDisplay: 3,
  clearSelectedReasonCode: () => {},
  clearSelectedOrderItems: () => {},
  clearSelectedResolution: () => {},
  isFetching: false,
  fromSelectOrder: false,
  setOrderStatusInTransit: () => {},
  setToggleCards: () => {},
  setSelectedShipmentCard: () => {},
  setFilteredList: () => {},
};

export default withStyles(RecentOrders, styles);
export { RecentOrders as RecentOrdersVanilla };
