// 9fbef606107a605d69c0edbcd8029e5d
import { fromJS, Map } from 'immutable';
import emailConfirmationReducer from '../EmailConfirmation.reducer';
import EMAIL_CONFIRMATION_CONSTANTS from '../../EmailConfirmation.constants';

describe('Email confirmation Reducer', () => {
  const initialState = fromJS({
    isFetching: false,
    error: null,
    emailConfirmationSuccess: null,
  });

  it('should return empty merge account email confirmation as default state', () => {
    expect(emailConfirmationReducer(undefined, {}).get('emailConfirmationSuccess')).toBeNull();
    expect(emailConfirmationReducer(undefined, {}).get('error')).toBeNull();
    expect(emailConfirmationReducer(undefined, {}).get('isFetching')).toBeFalsy();
  });

  it('should be called on merge account email confirmation request isfetching', () => {
    const payload = {};
    expect(
      emailConfirmationReducer(initialState, {
        type: EMAIL_CONFIRMATION_CONSTANTS.EMAIL_CONFIRMATION_REQUEST,
        payload,
      })
    ).toEqual(
      Map({
        isFetching: true,
        error: null,
        emailConfirmationSuccess: null,
      })
    );
  });

  it('should be called on merge account email confirmation loader set', () => {
    const payload = true;
    expect(
      emailConfirmationReducer(initialState, {
        type: EMAIL_CONFIRMATION_CONSTANTS.SET_EMAIL_CONFIRMATION_LOADER,
        payload,
      })
    ).toEqual(
      Map({
        isFetching: true,
        error: null,
        emailConfirmationSuccess: null,
      })
    );
  });

  it('should be called on merge account email confirmation success', () => {
    const payload = {
      status: 'SUCCESS',
      timestamp: '2021-09-06T08:56:34.528516',
      data: {
        status: 'Active',
        email: 'test@test.com',
      },
    };
    expect(
      emailConfirmationReducer(initialState, {
        type: EMAIL_CONFIRMATION_CONSTANTS.EMAIL_CONFIRMATION_SUCCESS,
        payload,
      })
    ).toEqual(
      Map({
        isFetching: false,
        error: null,
        emailConfirmationSuccess: payload,
      })
    );
  });

  it('should be called on merge account email confirmation failed', () => {
    const err = fromJS({
      status: 'ERROR',
      timestamp: '2021-09-07T08:38:37.264499',
      errors: [
        {
          errorCode: 'Internal Server Error',
          errorMessage: 'Error occured while fetching the members details from Brierley',
          errorParameters: null,
          errorType: null,
        },
      ],
      exceptionType: 'RUNTIME_EXCEPTION',
    });
    expect(
      emailConfirmationReducer(initialState, {
        type: EMAIL_CONFIRMATION_CONSTANTS.EMAIL_CONFIRMATION_FAILED,
        payload: err,
      })
    ).toEqual(
      Map({
        error: err,
        emailConfirmationSuccess: null,
        isFetching: false,
      })
    );
  });

  it('should be called on merge account email confirmation state reset', () => {
    const payload = {};
    expect(
      emailConfirmationReducer(initialState, {
        type: EMAIL_CONFIRMATION_CONSTANTS.RESET_EMAIL_CONFIRMATION,
        payload,
      })
    ).toEqual(Map(initialState));
  });
});
