// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { Anchor, BodyCopy, Image } from '@tcp/core/src/components/common/atoms';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import { getIconPath } from '@tcp/core/src/utils';
import CUSTOMER_HELP_CONSTANTS, {
  REASON_CODES,
  GC_IN_ORDER,
} from '@tcp/core/src/components/features/account/CustomerHelp/CustomerHelp.constants';
import { formatAmount } from '../../../../../util/utility';

const getReturnReadyLabels = (labels, orderId) => {
  const returnReadyLabel = getLabelValue(labels, 'lbl_returns_return_ready');
  const splitted = returnReadyLabel.split('&&');
  if (splitted.length <= 1)
    return (
      <BodyCopy
        className="confirmation-labels order-details-link"
        fontSize="fs14"
        fontWeight="extrabold"
        fontFamily="secondary"
      >
        {returnReadyLabel}
      </BodyCopy>
    );
  return (
    <>
      <BodyCopy
        className="confirmation-labels"
        fontSize="fs14"
        fontWeight="extrabold"
        fontFamily="secondary"
      >
        {splitted[0] + orderId + splitted[1]}
      </BodyCopy>
      <Anchor target="_blank" to="/help-center/returns-exchanges#returnsandexchanges">
        <BodyCopy
          className="confirmation-labels order-details-link"
          fontSize="fs14"
          fontWeight="extrabold"
          fontFamily="secondary"
        >
          {getLabelValue(labels, 'lbl_returns_return_policy')}
        </BodyCopy>
      </Anchor>
      <BodyCopy
        className="confirmation-labels"
        fontSize="fs14"
        fontWeight="extrabold"
        fontFamily="secondary"
      >
        {splitted[2]}
      </BodyCopy>
    </>
  );
};

const ReviewBasicDetails = ({
  className,
  labels,
  refundTotal,
  currencySymbol,
  caseId,
  headingText,
  confirmationText,
  action,
  imageIcon,
  errorLabel,
  selectedReturnMethod,
  orderDetailsData,
  isLoggedIn,
}) => {
  return (
    <div>
      <BodyCopy component="div" className={className}>
        <BodyCopy className="elem-mb-SM refund-successful">
          <Image alt="close" src={getIconPath(imageIcon)} height="40px" width="40px" />
        </BodyCopy>
        <BodyCopy
          fontSize="fs24"
          fontWeight="extrabold"
          fontFamily="secondary"
          className={`elem-mb-SM refund-successful  ${
            errorLabel === GC_IN_ORDER ? 'refund-review-text' : 'success-text'
          }`}
        >
          {headingText}
        </BodyCopy>

        <BodyCopy
          component="div"
          className="refund-successful confirmation-message elem-mb-XXS"
          textAlign="center"
        >
          {confirmationText && (
            <BodyCopy
              className="confirmation-labels"
              fontSize="fs14"
              fontWeight="extrabold"
              fontFamily="secondary"
            >
              {confirmationText}
            </BodyCopy>
          )}

          {action === REASON_CODES.INITIATE_RETURN && (
            <>
              {getReturnReadyLabels(labels, orderDetailsData?.orderNumber)}
              {selectedReturnMethod === CUSTOMER_HELP_CONSTANTS.RETURN_METHOD.BY_MAIL && (
                <BodyCopy
                  className="confirmation-labels"
                  fontSize="fs14"
                  fontWeight="extrabold"
                  fontFamily="secondary"
                >
                  {getLabelValue(labels, 'lbl_returns_ship_success_one')}
                </BodyCopy>
              )}
              {selectedReturnMethod === CUSTOMER_HELP_CONSTANTS.RETURN_METHOD.IN_STORE && (
                <BodyCopy
                  className="confirmation-labels"
                  fontSize="fs14"
                  fontWeight="extrabold"
                  fontFamily="secondary"
                >
                  {getLabelValue(labels, 'lbl_returns_store_success_one')}
                </BodyCopy>
              )}
              <Anchor
                target="_blank"
                to={
                  isLoggedIn
                    ? `/account/orders/order-details/${orderDetailsData?.orderNumber}`
                    : `/track-order/${orderDetailsData?.orderNumber}/${encodeURIComponent(
                        orderDetailsData?.encryptedEmailAddress
                      )}`
                }
              >
                <BodyCopy
                  className="confirmation-labels order-details-link"
                  fontSize="fs14"
                  fontWeight="extrabold"
                  fontFamily="secondary"
                >
                  {getLabelValue(labels, 'lbl_returns_order_details')}
                </BodyCopy>
              </Anchor>
              <BodyCopy
                className="confirmation-labels"
                fontSize="fs14"
                fontWeight="extrabold"
                fontFamily="secondary"
              >
                {getLabelValue(labels, 'lbl_returns_success_two')}
              </BodyCopy>
            </>
          )}

          {errorLabel !== GC_IN_ORDER && action !== REASON_CODES.INITIATE_RETURN && (
            <BodyCopy
              className="confirmation-labels"
              fontSize="fs14"
              fontWeight="extrabold"
              fontFamily="secondary"
            >
              {getLabelValue(labels, 'lbl_confirmation_thanks')}
            </BodyCopy>
          )}
        </BodyCopy>

        {action !== REASON_CODES.CANCEL_MODIFY && caseId && (
          <BodyCopy
            component="div"
            fontSize="fs12"
            fontWeight="extra"
            className="elem-mb-SM elem-mt-XXS refund-successful"
            fontFamily="secondary"
          >
            {`${getLabelValue(labels, 'lbl_review_under_case')}${caseId}`}
          </BodyCopy>
        )}

        {action !== REASON_CODES.INITIATE_RETURN && (
          <BodyCopy component="div" className="refund-successful">
            <BodyCopy
              className="elem-mr-SM"
              fontSize="fs14"
              fontWeight="extra"
              fontFamily="secondary"
            >
              {`${
                action === REASON_CODES.CANCEL_MODIFY
                  ? getLabelValue(labels, 'lbl_order_total')
                  : getLabelValue(labels, 'lbl_refund_total')
              }:`}
            </BodyCopy>

            <BodyCopy
              className="elem-ml-SM"
              fontSize="fs18"
              fontWeight="extrabold"
              fontFamily="secondary"
            >
              {formatAmount(refundTotal, currencySymbol)}
            </BodyCopy>
          </BodyCopy>
        )}
      </BodyCopy>
    </div>
  );
};
ReviewBasicDetails.propTypes = {
  className: PropTypes.string,
  reviewDisplay: PropTypes.shape({}).isRequired,
  labels: PropTypes.shape({}).isRequired,
  refundTotal: PropTypes.number.isRequired,
  currencySymbol: PropTypes.string.isRequired,
  caseId: PropTypes.string.isRequired,
  headingText: PropTypes.string,
  confirmationText: PropTypes.string,
  action: PropTypes.string,
  imageIcon: PropTypes.string,
  errorLabel: PropTypes.string,
  selectedReturnMethod: PropTypes.string,
  orderDetailsData: PropTypes.shape({}),
  isLoggedIn: PropTypes.bool,
};

ReviewBasicDetails.defaultProps = {
  className: '',
  headingText: '',
  confirmationText: '',
  action: '',
  imageIcon: '',
  errorLabel: '',
  selectedReturnMethod: '',
  orderDetailsData: null,
  isLoggedIn: false,
};

export default ReviewBasicDetails;
