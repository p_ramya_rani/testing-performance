// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { CustomerHelpProgressIndicator } from '../CustomerHelpProgressIndicator.view.native';

describe('Customer Help Progress Indicator', () => {
  it('should render Customer Help Progress Indicator', () => {
    const props = {
      activeStage: 'select-order',
      moveToCustomerHelpStage: jest.fn(),
      availableStages: ['select-order', 'tell-us-more', 'review'],
      labels: {},
    };
    const component = shallow(<CustomerHelpProgressIndicator {...props} />);
    expect(component).toMatchSnapshot();
  });
});
