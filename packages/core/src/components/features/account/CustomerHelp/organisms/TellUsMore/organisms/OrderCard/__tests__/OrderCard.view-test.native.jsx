// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import OrderCard from '../views/OrderCard.view.native';
import { TellusMoreContext } from '../../../views/TellUsMore.view.native';

describe('Order Items component', () => {
  const props = {
    item: {
      productInfo: {
        name: 'test product',
        imagePath: 'test',
        color: 'red',
        fit: 'test',
        size: 'test',
        upc: 'test',
        pdpUrl: 'test',
      },
      itemInfo: {
        linePrice: 2.15,
        itemBrand: 'TCP',
        quantity: 1,
        quantityCanceled: false,
        listPrice: 2.25,
        offerPrice: 2.25,
      },
      trackingInfo: [{ status: 'Order Shipped' }],
    },
    currencySymbol: '$',
    isShowWriteReview: 'true',
    ordersLabels: {},
  };
  const continueButtonRef = {
    current: {
      measure: jest.fn(),
    },
  };
  const scrollToTarget = jest.fn();

  it('should renders correctly', () => {
    const component = shallow(
      <TellusMoreContext.Provider value={{ continueButtonRef, scrollToTarget }}>
        <OrderCard {...props} />
      </TellusMoreContext.Provider>
    );
    expect(component).toMatchSnapshot();
  });

  it('should renders selected State', () => {
    const updatedProps = { ...props, selected: true };
    const component = shallow(
      <TellusMoreContext.Provider value={{ continueButtonRef, scrollToTarget }}>
        <OrderCard {...updatedProps} />
      </TellusMoreContext.Provider>
    );
    expect(component).toMatchSnapshot();
  });

  it('should render with returned status', () => {
    const { item } = props;
    const trackingInfo = [{ status: 'Order Returned' }];
    const updatedProps = { ...props, item: { ...item, trackingInfo } };
    const component = shallow(
      <TellusMoreContext.Provider value={{ continueButtonRef, scrollToTarget }}>
        <OrderCard {...updatedProps} />
      </TellusMoreContext.Provider>
    );
    expect(component).toMatchSnapshot();
  });

  it('should render with Canceled status', () => {
    const { item } = props;
    const trackingInfo = [{ status: 'Order Canceled' }];
    const updatedProps = { ...props, item: { ...item, trackingInfo } };
    const component = shallow(
      <TellusMoreContext.Provider value={{ continueButtonRef, scrollToTarget }}>
        <OrderCard {...updatedProps} />
      </TellusMoreContext.Provider>
    );
    expect(component).toMatchSnapshot();
  });

  it('should render with item refunded', () => {
    const { item } = props;
    const trackingInfo = [{ status: 'item refunded' }];
    const updatedProps = { ...props, item: { ...item, trackingInfo } };
    const component = shallow(
      <TellusMoreContext.Provider value={{ continueButtonRef, scrollToTarget }}>
        <OrderCard {...updatedProps} />
      </TellusMoreContext.Provider>
    );
    expect(component).toMatchSnapshot();
  });

  it('should render with In-Transit status', () => {
    const { item } = props;
    const trackingInfo = [{ status: 'Order Returned' }];
    const updatedProps = { ...props, item: { ...item, trackingInfo }, orderStatus: 'In Transit' };
    const component = shallow(
      <TellusMoreContext.Provider value={{ continueButtonRef, scrollToTarget }}>
        <OrderCard {...updatedProps} />
      </TellusMoreContext.Provider>
    );
    expect(component).toMatchSnapshot();
  });
});
