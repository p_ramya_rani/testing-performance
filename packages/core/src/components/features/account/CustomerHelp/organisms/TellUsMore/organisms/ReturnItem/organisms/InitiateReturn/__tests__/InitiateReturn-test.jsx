import React from 'react';
import { shallow } from 'enzyme';
import { InitiateReturnVanilla } from '../views/InitiateReturn.view';

describe('Return Method View', () => {
  const props = {
    className: '',
    setReturnMethod: () => {},
    selectedReturnMethod: '',
    clearReturnMethod: () => {},
    labels: {},
    orderDetailsData: {},
    orderLabels: {},
  };
  it('should render correctly', () => {
    const component = shallow(<InitiateReturnVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
