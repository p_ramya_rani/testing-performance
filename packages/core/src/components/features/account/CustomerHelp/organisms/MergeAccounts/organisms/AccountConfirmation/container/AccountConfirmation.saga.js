// 9fbef606107a605d69c0edbcd8029e5d
import { call, takeLatest, put } from 'redux-saga/effects';
import { mergeAccountValidateMember } from '../../../../../../../../../services/abstractors/customerHelp/customerHelp';
import { isMobileApp } from '../../../../../../../../../utils';
import {
  setMergeAccountButtonLoader,
  setNumberOfRetries,
  setAccountStatus,
  mergeAccountSuccess,
} from '../../../container/MergeAccounts.actions';
import MERGE_ACCOUNT_CONSTANTS from '../../../MergeAccounts.constants';
import constants from '../AccountConfirmation.constants';
import { accountConfirmationFailure } from './AccountConfirmation.actions';

function resetRecaptcha() {
  if (!isMobileApp()) {
    try {
      // eslint-disable-next-line no-unused-expressions
      window?.grecaptcha?.reset();
    } catch (err) {
      return err;
    }
  }
  return null;
}

export function* validateMember({ payload }) {
  const { firstName, lastName, noCountryZip: zipCode, victimEmailAddress } = payload;

  try {
    yield put(setMergeAccountButtonLoader(true));
    const response = yield call(mergeAccountValidateMember, {
      firstName,
      lastName,
      zipCode,
      emailId: victimEmailAddress,
    });

    if (response && response.data) {
      const { numberOfRetries, accountStatus, status, mergeAccountResponse } = response.data;
      if (numberOfRetries >= 0) yield put(setNumberOfRetries(numberOfRetries));
      if (accountStatus === 0 || accountStatus === 1)
        yield put(setAccountStatus(MERGE_ACCOUNT_CONSTANTS.ACCOUNT_STATUS[accountStatus]));
      if (status === constants.ACCOUNT_VALID && mergeAccountResponse) {
        return yield put(mergeAccountSuccess(mergeAccountResponse));
      }

      if (status === constants.ACCOUNT_INVALID) {
        return yield put(accountConfirmationFailure(status));
      }
    }
    return null;
  } catch (err) {
    return yield put(mergeAccountSuccess(err));
  } finally {
    yield put(setMergeAccountButtonLoader(false));
    resetRecaptcha();
  }
}

export function* AccountConfirmationSaga() {
  yield takeLatest(constants.VALIDATE_MEMBER, validateMember);
}

export default AccountConfirmationSaga;
