// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { EmailConfirmationSectionVanilla } from '../EmailConfirmationSection.view';

describe('EmailConfirmationSection component', () => {
  const props = {
    onSubmit: jest.fn(),
    labels: {},
    resetEmailConfirmation: jest.fn(),
    formErrorMessage: {},
  };
  it('should renders correctly', () => {
    const component = shallow(<EmailConfirmationSectionVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
