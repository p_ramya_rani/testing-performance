// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  .reason-code-skeleton-rectangle {
    min-width: 330px;
    height: 58px;
    display: flex;
    align-items: center;
    justify-content: center;
    border-top: 1px solid ${props => props.theme.colorPalette.gray[500]};
  }
`;

export default styles;
