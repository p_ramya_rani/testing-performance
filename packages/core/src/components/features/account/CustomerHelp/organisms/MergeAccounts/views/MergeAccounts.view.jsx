// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import ShortUnderLine from '@tcp/core/src/components/common/atoms/ShortUnderLine';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import { routerPush } from '@tcp/core/src/utils';
import { BodyCopy, Button } from '@tcp/core/src/components/common/atoms';
import styles from '../styles/MergeAccounts.styles';
import AccountInfo from '../../AccountInfo';
import EmailConfirmation from '../../EmailConfirmation';
import AccountConfirmation from '../organisms/AccountConfirmation';
import MergeAccountOtpContainer from '../organisms/MergeAccountsOtp';
import MergeConfirmationModal from '../organisms/MergeConfirmationModal';
import { CUSTOMER_HELP_DIRECT_LINK_TEMPLATE } from '../../../CustomerHelp.constants';

const renderValidationScreens = (
  showOtp,
  setShowOtp,
  survivorEmailAddress,
  victimEmailAddress,
  labels,
  trackAnalyticsPageView,
  trackAnalyticsClick
) => {
  return showOtp ? (
    <MergeAccountOtpContainer
      setShowOtp={setShowOtp}
      survivorEmailAddress={survivorEmailAddress}
      victimEmailAddress={victimEmailAddress}
      labels={labels}
      trackAnalyticsPageView={trackAnalyticsPageView}
      trackAnalyticsClick={trackAnalyticsClick}
    />
  ) : (
    <AccountConfirmation
      labels={labels}
      setShowOtp={setShowOtp}
      victimEmailAddress={victimEmailAddress}
      survivorEmailAddress={survivorEmailAddress}
      trackAnalyticsPageView={trackAnalyticsPageView}
      trackAnalyticsClick={trackAnalyticsClick}
    />
  );
};

const renderGuestUserScreen = (labels, pathname) => {
  return (
    <>
      <BodyCopy
        component="div"
        className="login-to-continue elem-mt-LRG_1"
        fontFamily="secondary"
        fontSize="fs16"
        fontWeight="bold"
      >
        {getLabelValue(labels, 'lbl_merge_login_to_merge_accounts')}
      </BodyCopy>
      <ShortUnderLine />
      <div className="content">
        <BodyCopy
          component="div"
          fontFamily="secondary"
          fontSize={['fs14', 'fs12', 'fs14']}
          fontWeight="semibold"
          className=" description elem-mt-SM"
        >
          {getLabelValue(labels, 'lbl_merge_description_part1', 'helpCenter')}
          <br />
          <br />
          {getLabelValue(labels, 'lbl_merge_description_part2')}
        </BodyCopy>
        <BodyCopy component="div" className="bottom-section">
          <Button
            fill="BLUE"
            type="submit"
            buttonVariation="variable-width"
            className="login elem-mt-MED"
            onClick={() => routerPush(`?target=login&successtarget=${pathname}`, '/home/login')}
          >
            {getLabelValue(labels, 'lbl_merge_log_in')}
          </Button>
        </BodyCopy>
        <BodyCopy
          component="div"
          fontSize="fs12"
          fontWeight="semibold"
          fontFamily="secondary"
          className="create-new-account elem-mt-LRG"
        >
          {getLabelValue(labels, 'lbl_merge_dont_have_an_account')}
        </BodyCopy>
        <BodyCopy component="div" className="elem-mt-XXS">
          <BodyCopy
            className="create-new-account"
            component="span"
            fontSize="fs12"
            fontWeight="semibold"
            fontFamily="secondary"
          >
            <span
              className="create-one"
              role="button"
              tabIndex="0"
              onClick={() =>
                routerPush(`?target=login&successtarget=${pathname}`, '/home/register')
              }
              onKeyDown={() =>
                routerPush(`?target=login&successtarget=${pathname}`, '/home/register')
              }
            >
              {getLabelValue(labels, 'lbl_merge_create_one')}
            </span>
            &nbsp;
            {getLabelValue(labels, 'lbl_merge_now_to_start_earning_points')}
          </BodyCopy>
        </BodyCopy>
      </div>
    </>
  );
};
const MergeAccounts = ({
  className,
  labels,
  isLoggedIn,
  survivorEmailAddress,
  victimEmailAddress,
  showConfirmationModal,
  setShowConfirmationModal,
  setIsMergeConfirmed,
  trackAnalyticsPageView,
  trackAnalyticsClick,
}) => {
  const [showOtp, setShowOtp] = useState(true);
  const pathname = CUSTOMER_HELP_DIRECT_LINK_TEMPLATE.MERGE_MYPLACE_ACCOUNTS;

  return (
    <div className={`${className} merge-accounts-container`}>
      {isLoggedIn ? (
        <>
          <AccountInfo labels={labels} />
          <EmailConfirmation labels={labels} setShowOtp={setShowOtp} />

          {victimEmailAddress &&
            renderValidationScreens(
              showOtp,
              setShowOtp,
              survivorEmailAddress,
              victimEmailAddress,
              labels,
              trackAnalyticsPageView,
              trackAnalyticsClick
            )}
        </>
      ) : (
        renderGuestUserScreen(labels, pathname)
      )}
      <MergeConfirmationModal
        isModalOpen={showConfirmationModal}
        closeModal={() => setShowConfirmationModal(false)}
        labels={labels}
        victimEmailAddress={victimEmailAddress}
        setIsMergeConfirmed={setIsMergeConfirmed}
        trackAnalyticsPageView={trackAnalyticsPageView}
        trackAnalyticsClick={trackAnalyticsClick}
      />
    </div>
  );
};
MergeAccounts.propTypes = {
  className: PropTypes.string,
  labels: PropTypes.shape({}).isRequired,
  isLoggedIn: PropTypes.bool.isRequired,
  survivorEmailAddress: PropTypes.string.isRequired,
  victimEmailAddress: PropTypes.string.isRequired,
  showConfirmationModal: PropTypes.bool.isRequired,
  setShowConfirmationModal: PropTypes.func.isRequired,
  setIsMergeConfirmed: PropTypes.func.isRequired,
  trackAnalyticsPageView: PropTypes.func.isRequired,
  trackAnalyticsClick: PropTypes.func.isRequired,
};
MergeAccounts.defaultProps = {
  className: '',
};
export default withStyles(MergeAccounts, styles);
export { MergeAccounts as MergeAccountsVanilla };
