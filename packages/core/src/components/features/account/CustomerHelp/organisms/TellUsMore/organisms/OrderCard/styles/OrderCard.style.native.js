// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';
import { ViewWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';
import { BodyCopy, Image } from '@tcp/core/src/components/common/atoms';

const OrderCardContainer = styled.TouchableOpacity`
  position: relative;
  display: flex;
  border-radius: 12px;
  ${props =>
    props.selected
      ? `border: 2px solid ${props.theme.colorPalette.blue[800]}`
      : ` border: 1px solid ${props.theme.colorPalette.gray[600]}`};
  padding-bottom: ${props => props.theme.spacing.ELEM_SPACING.XS};
  margin: ${props => props.theme.spacing.ELEM_SPACING.XS}
    ${props => props.theme.spacing.ELEM_SPACING.XS};
  ${props => (props.hazy ? 'opacity: 0.5' : '')};
  min-height: 220px;
`;

const ImageContainer = styled.View`
  display: flex;
  flex-direction: row;
`;

const ImageWrapper = styled.View`
  margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.XL};
  position: relative;
`;

const ItemsImage = styled(Image)`
  padding: ${props => props.theme.spacing.ELEM_SPACING.XS};
  ${props => (props.lastImage ? 'opacity: 0.15' : '')};
`;

const ItemCount = styled(BodyCopy)`
  color: ${props => props.theme.colorPalette.white};
`;

const ItemCountWrapper = styled.View`
  border-radius: 10px;
  right: 8px;
  position: absolute;
  background-color: ${props => props.theme.colorPalette.blue.C900};
  top: 50px;
  width: 18px;
  height: 18px;
`;

const TotalItems = styled(BodyCopy)`
  right: 35px;
  position: absolute;
  text-align: right;
  top: 35px;
  width: 100%;
`;

const StackedContainer = styled.View`
  display: flex;
  flex-direction: column;
  align-items: center;
  position: relative;
`;

const commonStackStyle = () => {
  return {
    'border-bottom': `solid 1px ${props => props.theme.colorPalette.gray[600]}`,
    width: '95%',
    position: 'absolute',
    top: '-15px',
    height: '24px',
    'border-radius': '12px',
  };
};

const FirstStack = styled.View`
  ${{ ...commonStackStyle() }}
`;

const SecondStack = styled.View`
  width: 90%;
  top: -5px;
  ${{ ...commonStackStyle() }}
`;

const FullBleedView = styled(ViewWithSpacing)`
  width: 100%;
  margin-right: 0;
  margin-left: 0;
  ${props => (props.centered ? `justify-content: center;` : ``)}
`;

export {
  OrderCardContainer,
  ImageContainer,
  ImageWrapper,
  ItemCount,
  TotalItems,
  StackedContainer,
  FirstStack,
  SecondStack,
  ItemsImage,
  FullBleedView,
  ItemCountWrapper,
};
