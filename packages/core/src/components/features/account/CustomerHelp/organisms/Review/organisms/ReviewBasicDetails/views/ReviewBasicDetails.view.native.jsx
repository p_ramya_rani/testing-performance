// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { BodyCopy, Image } from '@tcp/core/src/components/common/atoms';
import {
  ViewWithSpacing,
  BodyCopyWithSpacing,
} from '@tcp/core/src/components/common/atoms/styledWrapper';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import {
  REASON_CODES,
  GC_IN_ORDER,
} from '@tcp/core/src/components/features/account/CustomerHelp/CustomerHelp.constants';
import { formatAmount } from '../../../../../util/utility';
import { Container, RowContainer } from '../../../../Confirmation/styles/Confirmation.style.native';

const ReviewBasicDetails = ({
  labels,
  refundTotal,
  currencySymbol,
  caseId,
  headingText,
  confirmationText,
  action,
  imageIcon,
  errorLabel,
}) => {
  return (
    <ViewWithSpacing spacingStyles="margin-top-MED">
      <Container>
        <Image alt="close" source={imageIcon} height="40px" width="40px" />

        <BodyCopyWithSpacing
          spacingStyles="margin-top-SM"
          text={headingText}
          fontSize="fs24"
          fontWeight="extrabold"
          fontFamily="secondary"
          color={errorLabel === GC_IN_ORDER ? 'yellow.700' : 'green.500'}
        />

        <BodyCopyWithSpacing
          spacingStyles="margin-top-SM"
          text={confirmationText}
          fontSize="fs14"
          fontWeight="extrabold"
          fontFamily="secondary"
        />

        {errorLabel !== GC_IN_ORDER && (
          <BodyCopy
            text={getLabelValue(labels, 'lbl_confirmation_thanks')}
            fontSize="fs14"
            fontWeight="extrabold"
            fontFamily="secondary"
          />
        )}

        {action !== REASON_CODES.CANCEL_MODIFY && !!caseId && (
          <BodyCopyWithSpacing
            spacingStyles="margin-top-MED"
            text={`${getLabelValue(labels, 'lbl_review_under_case')}${caseId}`}
            fontSize="fs12"
            fontFamily="secondary"
          />
        )}

        <RowContainer spacingStyles="margin-top-MED padding-left-LRG padding-right-LRG">
          <BodyCopy
            text={`${
              action === REASON_CODES.CANCEL_MODIFY
                ? getLabelValue(labels, 'lbl_order_total')
                : getLabelValue(labels, 'lbl_refund_total')
            }:`}
            fontSize="fs14"
            fontFamily="secondary"
          />
          <BodyCopy
            text={formatAmount(refundTotal, currencySymbol)}
            fontSize="fs18"
            fontWeight="extrabold"
            fontFamily="secondary"
          />
        </RowContainer>
      </Container>
    </ViewWithSpacing>
  );
};
ReviewBasicDetails.propTypes = {
  reviewDisplay: PropTypes.shape({}).isRequired,
  labels: PropTypes.shape({}).isRequired,
  refundTotal: PropTypes.number.isRequired,
  currencySymbol: PropTypes.string.isRequired,
  caseId: PropTypes.string.isRequired,
  headingText: PropTypes.string,
  confirmationText: PropTypes.string,
  action: PropTypes.string,
  imageIcon: PropTypes.string,
  errorLabel: PropTypes.string,
};

ReviewBasicDetails.defaultProps = {
  headingText: '',
  confirmationText: '',
  action: '',
  imageIcon: '',
  errorLabel: '',
};

export default ReviewBasicDetails;
