// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { ResolutionVanilla } from '../views/Resolution.view';

describe('Missing/Wrong order resolution', () => {
  const props = {
    labels: {
      lbl_how_to_solve: 'How can we resolve this?',
    },
    selectedResolution: 'refundItems',
  };

  it('should render Resolution option', () => {
    const component = shallow(<ResolutionVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
