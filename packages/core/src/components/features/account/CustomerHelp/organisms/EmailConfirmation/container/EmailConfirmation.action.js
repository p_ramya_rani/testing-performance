// 9fbef606107a605d69c0edbcd8029e5d
import EMAIL_CONFIRMATION_CONSTANTS from '../EmailConfirmation.constants';

export const emailConfirmationRequest = (payload) => ({
  type: EMAIL_CONFIRMATION_CONSTANTS.EMAIL_CONFIRMATION_REQUEST,
  payload,
});

export const emailConfirmationFailure = (payload) => ({
  type: EMAIL_CONFIRMATION_CONSTANTS.EMAIL_CONFIRMATION_FAILED,
  payload,
});

export const emailConfirmationSuccess = (payload) => ({
  type: EMAIL_CONFIRMATION_CONSTANTS.EMAIL_CONFIRMATION_SUCCESS,
  payload,
});

export const resetEmailConfirmationRequest = () => ({
  type: EMAIL_CONFIRMATION_CONSTANTS.RESET_EMAIL_CONFIRMATION,
});

export const setEmailConfirmationLoader = (payload) => ({
  type: EMAIL_CONFIRMATION_CONSTANTS.SET_EMAIL_CONFIRMATION_LOADER,
  payload,
});
