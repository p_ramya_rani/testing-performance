// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import EmailConfirmationSection from '../organisms/EmailConfirmationSection';

const EmailConfirmation = ({
  onSubmit,
  formErrorMessage,
  labels,
  emailConfirmationError,
  fullPage,
  emailConfirmationLoader,
  emailConfirmationSuccess,
  resetEmailConfirmation,
  setShowOtp,
  mergeAccountReset,
}) => (
  <EmailConfirmationSection
    onSubmit={onSubmit}
    labels={labels}
    formErrorMessage={formErrorMessage}
    emailConfirmationError={emailConfirmationError}
    fullPage={fullPage}
    emailConfirmationLoader={emailConfirmationLoader}
    emailConfirmationSuccess={emailConfirmationSuccess}
    resetEmailConfirmation={resetEmailConfirmation}
    setShowOtp={setShowOtp}
    mergeAccountReset={mergeAccountReset}
  />
);

EmailConfirmation.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  formErrorMessage: PropTypes.shape({}).isRequired,
  emailConfirmationError: PropTypes.string,
  labels: PropTypes.shape({}).isRequired,
  fullPage: PropTypes.bool,
  emailConfirmationLoader: PropTypes.bool,
  emailConfirmationSuccess: PropTypes.shape({}),
  resetEmailConfirmation: PropTypes.func.isRequired,
  setShowOtp: PropTypes.func.isRequired,
  mergeAccountReset: PropTypes.func.isRequired,
};

EmailConfirmation.defaultProps = {
  emailConfirmationError: '',
  fullPage: false,
  emailConfirmationLoader: false,
  emailConfirmationSuccess: {},
};

export default EmailConfirmation;

export { EmailConfirmation as EmailConfirmationVanilla };
