// 9fbef606107a605d69c0edbcd8029e5d
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Anchor } from '@tcp/core/src/components/common/atoms';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import RecentOrders from '../organisms/RecentOrders';
import styles from '../styles/SelectOrder.style';

const SelectOrder = ({
  ordersListItems,
  orderLabels,
  labels,
  cshForMonthsToDisplay,
  clearSelectedOrderItems,
  clearSelectedReasonCode,
  clearSelectedResolution,
  isFetching,
  className,
  isLoadMoreActive,
  setCurrentPage,
  currentPageNumber,
  setOrderStatusInTransit,
  setToggleCards,
  setSelectedShipmentCard,
  setFilteredList,
  setLoader,
  setActiveSection,
}) => {
  useEffect(() => {
    setLoader(false);
    setActiveSection(undefined);
  }, []);
  const [seeMore, setSeeMore] = useState(false);

  return (
    <div className={className}>
      <RecentOrders
        labels={labels}
        orderLabels={orderLabels}
        ordersListItems={ordersListItems}
        cshForMonthsToDisplay={cshForMonthsToDisplay}
        clearSelectedOrderItems={clearSelectedOrderItems}
        clearSelectedReasonCode={clearSelectedReasonCode}
        clearSelectedResolution={clearSelectedResolution}
        setOrderStatusInTransit={setOrderStatusInTransit}
        setToggleCards={setToggleCards}
        setSelectedShipmentCard={setSelectedShipmentCard}
        isFetching={isFetching}
        fromSelectOrder
        setFilteredList={setFilteredList}
        seeMore={seeMore}
        currentPageNumber={currentPageNumber}
      />

      {isLoadMoreActive && !isFetching && (
        <Anchor
          className="see-more-link"
          underline
          fontFamily="secondary"
          fontSize="fs16"
          noLink
          onClick={(e) => {
            setSeeMore(true);
            setCurrentPage(currentPageNumber + 1);
            e.preventDefault();
          }}
        >
          {getLabelValue(orderLabels, 'lbl_see_more', 'orders')}
        </Anchor>
      )}
    </div>
  );
};

SelectOrder.propTypes = {
  ordersListItems: PropTypes.shape([]).isRequired,
  orderLabels: PropTypes.shape({}).isRequired,
  labels: PropTypes.shape({}).isRequired,
  cshForMonthsToDisplay: PropTypes.number,
  clearSelectedReasonCode: PropTypes.func,
  clearSelectedOrderItems: PropTypes.func,
  clearSelectedResolution: PropTypes.func,
  setCurrentPage: PropTypes.func,
  isFetching: PropTypes.bool,
  className: PropTypes.string,
  isLoadMoreActive: PropTypes.bool,
  currentPageNumber: PropTypes.number,
  setOrderStatusInTransit: PropTypes.func,
  setToggleCards: PropTypes.func,
  setSelectedShipmentCard: PropTypes.func,
  setFilteredList: PropTypes.func,
  setLoader: PropTypes.func,
  setActiveSection: PropTypes.func,
};
SelectOrder.defaultProps = {
  cshForMonthsToDisplay: 3,
  clearSelectedOrderItems: () => {},
  clearSelectedReasonCode: () => {},
  clearSelectedResolution: () => {},
  isFetching: false,
  className: '',
  isLoadMoreActive: false,
  setCurrentPage: () => {},
  currentPageNumber: 0,
  setOrderStatusInTransit: () => {},
  setToggleCards: () => {},
  setSelectedShipmentCard: () => {},
  setFilteredList: () => {},
  setLoader: () => {},
  setActiveSection: () => {},
};

export default withStyles(SelectOrder, styles);
export { SelectOrder as SelectOrderVanilla };
