// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { BodyCopy, Row, Col } from '@tcp/core/src/components/common/atoms';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import formatAmount from '../../../../../../OrderDetails/organism/OrderSummaryDetails/utils';

/**
 * This function component use for return the OrderShippingDetails
 * can be passed in the component.
 * @param orderLabels - ordersLabels object used for showing Orders Labels
 */

const getShippingAmount = (isCancelOrderPage, labels, shippingTotal, currencySymbol) => {
  if (isCancelOrderPage) {
    return (
      <Row fullBleed className="elem-mb-XS">
        <Col colSize={{ large: 7, medium: 4, small: 3 }}>
          <BodyCopy fontFamily="secondary" fontSize="fs14" color="black">
            {`${getLabelValue(labels, 'lbl_shipping')}:`}
          </BodyCopy>
        </Col>
        <Col colSize={{ large: 5, medium: 4, small: 3 }}>
          <BodyCopy fontFamily="secondary" fontSize="fs14" textAlign="right" color="black">
            {formatAmount(shippingTotal, currencySymbol)}
          </BodyCopy>
        </Col>
      </Row>
    );
  }
  return null;
};

const RefundSummaryDetails = ({
  className,
  orderLabels,
  orderDetailsData,
  labels,
  totalBeforeTax,
  tax,
  refundTotal,
  itemsCount,
  orderStatus,
  isCancelOrderPage,
  shippingTotal,
  isReviewPage,
  isPaidAndShippingCase,
}) => {
  const { summary } = orderDetailsData || {};
  const { currencySymbol } = summary || {};

  /**
   * @function return  Used to render the JSX of the component
   * @param    {[Void]} function does not accept anything.
   * @return   {[Object]} JSX of the component
   */
  return (
    <BodyCopy component="div" className={className}>
      <BodyCopy component="div">
        <BodyCopy
          fontSize="fs16"
          fontWeight="extrabold"
          fontFamily="secondary"
          className="elem-mb-XS"
          color="black"
        >
          {orderStatus}
        </BodyCopy>
        <BodyCopy className=" title-underline" />
      </BodyCopy>

      {/* showing items */}
      {!isPaidAndShippingCase ? (
        <>
          <Row fullBleed className="elem-mb-XS">
            <Col colSize={{ large: 7, medium: 4, small: 3 }}>
              <BodyCopy fontFamily="secondary" fontSize="fs14" color="black">
                {`${getLabelValue(orderLabels, 'lbl_orders_items')} (${itemsCount}):`}
              </BodyCopy>
            </Col>
            <Col colSize={{ large: 5, medium: 4, small: 3 }}>
              <BodyCopy fontFamily="secondary" fontSize="fs14" textAlign="right" color="black">
                {formatAmount(totalBeforeTax, currencySymbol)}
              </BodyCopy>
            </Col>
          </Row>

          {/* Total Before Tax */}
          <Row fullBleed className="elem-mb-XS">
            <Col colSize={{ large: 7, medium: 4, small: 3 }}>
              <BodyCopy fontFamily="secondary" fontSize="fs14" color="black">
                {`${getLabelValue(labels, 'lbl_total_before_tax')}:`}
              </BodyCopy>
            </Col>
            <Col colSize={{ large: 5, medium: 4, small: 3 }}>
              <BodyCopy fontFamily="secondary" fontSize="fs14" textAlign="right" color="black">
                {formatAmount(totalBeforeTax, currencySymbol)}
              </BodyCopy>
            </Col>
          </Row>

          {getShippingAmount(isCancelOrderPage, labels, shippingTotal, currencySymbol)}
          {/*  Tax  */}
          <Row fullBleed className="elem-mb-XS">
            <Col colSize={{ large: 7, medium: 4, small: 3 }}>
              <BodyCopy fontFamily="secondary" fontSize="fs14" color="black">
                {`${getLabelValue(orderLabels, 'lbl_orders_tax')}:`}
              </BodyCopy>
            </Col>
            <Col colSize={{ large: 5, medium: 4, small: 3 }}>
              <BodyCopy fontFamily="secondary" fontSize="fs14" textAlign="right" color="black">
                {formatAmount(tax, currencySymbol)}
              </BodyCopy>
            </Col>
          </Row>
        </>
      ) : (
        <>
          <Row fullBleed className="elem-mb-XS">
            <Col colSize={{ large: 7, medium: 4, small: 3 }}>
              <BodyCopy fontFamily="secondary" fontSize="fs14" color="black">
                {`${getLabelValue(labels, 'lbl_shipping')}:`}
              </BodyCopy>
            </Col>
            <Col colSize={{ large: 5, medium: 4, small: 3 }}>
              <BodyCopy fontFamily="secondary" fontSize="fs14" textAlign="right" color="black">
                {isReviewPage
                  ? formatAmount(shippingTotal, currencySymbol)
                  : formatAmount(totalBeforeTax, currencySymbol)}
              </BodyCopy>
            </Col>
          </Row>
          {!isReviewPage && (
            <Row fullBleed className="elem-mb-XS">
              <Col colSize={{ large: 7, medium: 4, small: 3 }}>
                <BodyCopy fontFamily="secondary" fontSize="fs14" color="black">
                  {`${getLabelValue(orderLabels, 'lbl_orders_tax')}:`}
                </BodyCopy>
              </Col>
              <Col colSize={{ large: 5, medium: 4, small: 3 }}>
                <BodyCopy fontFamily="secondary" fontSize="fs14" textAlign="right" color="black">
                  {formatAmount(tax, currencySymbol)}
                </BodyCopy>
              </Col>
            </Row>
          )}
        </>
      )}

      {/* Showing Total */}
      <Row fullBleed className="elem-mt-SM">
        <Col colSize={{ large: 7, medium: 4, small: 3 }}>
          <BodyCopy fontSize="fs14" fontWeight="extrabold" fontFamily="secondary" color="black">
            {`${
              isCancelOrderPage
                ? getLabelValue(labels, 'lbl_order_total')
                : getLabelValue(labels, 'lbl_refund_total')
            }:`}
          </BodyCopy>
        </Col>
        <Col colSize={{ large: 5, medium: 4, small: 3 }}>
          <BodyCopy
            fontSize="fs18"
            fontWeight="extrabold"
            fontFamily="secondary"
            textAlign="right"
            color="black"
          >
            {isPaidAndShippingCase && isReviewPage
              ? formatAmount(shippingTotal, currencySymbol)
              : formatAmount(refundTotal, currencySymbol)}
          </BodyCopy>
        </Col>
      </Row>
    </BodyCopy>
  );
};
RefundSummaryDetails.propTypes = {
  className: PropTypes.string,
  orderLabels: PropTypes.shape({
    lbl_orderDetails_orderSummary: PropTypes.string,
  }),
  orderDetailsData: PropTypes.shape({}),
  labels: PropTypes.shape({}),
  selectedOrderItems: PropTypes.shape({}),
  totalBeforeTax: PropTypes.number,
  tax: PropTypes.number,
  refundTotal: PropTypes.number,
  itemsCount: PropTypes.number,
  orderStatus: PropTypes.string,
  isCancelOrderPage: PropTypes.bool,
  shippingTotal: PropTypes.number,
  isReviewPage: PropTypes.bool,
  isPaidAndShippingCase: PropTypes.bool,
};

RefundSummaryDetails.defaultProps = {
  className: '',
  orderLabels: {
    lbl_orderDetails_orderSummary: '',
  },
  orderDetailsData: {},
  labels: {},
  selectedOrderItems: {},
  totalBeforeTax: {},
  tax: {},
  refundTotal: {},
  itemsCount: {},
  orderStatus: '',
  isCancelOrderPage: false,
  shippingTotal: {},
  isReviewPage: false,
  isPaidAndShippingCase: false,
};

export default RefundSummaryDetails;
