// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';
import { Anchor } from '@tcp/core/src/components/common/atoms';

export const CustomerServiceHelpContainer = styled.View`
  padding-top: ${props => props.theme.spacing.ELEM_SPACING.MED};
  padding-bottom: ${props => props.theme.spacing.APP_LAYOUT_SPACING.XS};
  padding-right: ${props => props.theme.spacing.APP_LAYOUT_SPACING.XXS};
  padding-left: ${props => props.theme.spacing.APP_LAYOUT_SPACING.XXS};
  background: ${props => props.theme.colors.PRIMARY.PALEGRAY};
`;

export const RowContainer = styled.View`
  flex-direction: row;
`;

export const CustomerHelpHeading = styled.View`
  margin: ${props => props.theme.spacing.APP_LAYOUT_SPACING.XXS};
`;

export const CustomerHelpText = styled.Text`
  color: ${props =>
    props.link ? props.theme.colorPalette.blue[800] : props.theme.colorPalette.black};
  font-size: ${props => props.fontSize || props.theme.typography.fontSizes.fs14};
  font-family: ${props => props.fontFamily || props.theme.typography.fonts.secondary};
  font-weight: ${props => props.fontWeight || props.theme.fonts.fontWeight.semiBold};
`;

export const BlueAnchor = styled(Anchor)`
  color: ${props => props.theme.colorPalette.blue[800]};
`;
