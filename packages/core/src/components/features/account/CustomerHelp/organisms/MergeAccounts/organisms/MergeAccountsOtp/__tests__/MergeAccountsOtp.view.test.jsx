import React from 'react';
import { shallow } from 'enzyme';
import { MergeAccountOtpVanilla } from '../views/MergeAccountsOtp.view';

describe('MergeAccountOtp template', () => {
  const props = {
    labels: {},
    className: '',
    handleOtpSubmission: () => {},
    otpDetail: {},
    resendMessageThreshold: 3000,
    showRecaptcha: false,
    victimEmailAddress: '',
    otpUniqueKey: '',
    setShowOtp: () => {},
    getOtpOnEmail: () => {},
  };
  it('should render correctly', () => {
    const component = shallow(<MergeAccountOtpVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
