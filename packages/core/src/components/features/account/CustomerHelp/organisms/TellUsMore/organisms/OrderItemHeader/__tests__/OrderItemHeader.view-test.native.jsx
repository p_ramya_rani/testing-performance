// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { OrderItemHeaderVanilla } from '../views/OrderItemHeader.view.native';

describe('OrderItemHeader Component', () => {
  const props = {
    shippedDate: 'May, 2019',
    trackingNumber: '1234567',
    trackingUrl: '/test',
  };
  it('should render OrderItemHeader Native Component', () => {
    const component = shallow(<OrderItemHeaderVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
