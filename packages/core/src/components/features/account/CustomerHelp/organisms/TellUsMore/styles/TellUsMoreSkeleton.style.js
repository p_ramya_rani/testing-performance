// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  .order-tile-container {
    margin-bottom: ${props => props.theme.spacing.LAYOUT_SPACING.LRG};
  }
`;

export default styles;
