// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { CouponDetailsVanilla } from '../views/CouponDetails.view';

describe('CouponDetails Template', () => {
  const props = {
    className: 'couponDetails',
    labels: {},
    coupon: {
      id: 'Y00105579',
      status: 'available',
      isExpiring: false,
      title: '$10 off $50 Gymboree ONLY',
      detailsOpen: false,
      expirationDate: '12/31/99',
      effectiveDate: '7/31/19',
      details: null,
      legalText: '$10 off $50 Gymboree ONLY',
      isStarted: true,
      error: '',
      promotionType: 'public',
      expirationDateTimeStamp: '9999-12-31T18:29:5.999Z',
    },
  };
  it('should not  render correctly', () => {
    const component = shallow(<CouponDetailsVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('should not  render correctly', () => {
    const component = shallow(<CouponDetailsVanilla isApplied {...props} />);
    expect(component).toMatchSnapshot();
  });
});
