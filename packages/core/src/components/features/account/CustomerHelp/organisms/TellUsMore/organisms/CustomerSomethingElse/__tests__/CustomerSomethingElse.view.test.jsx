// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { CustomerSomethingElseVanilla } from '../views/CustomerSomethingElse.view';

describe('Customer Service Help View', () => {
  const props = {
    labels: {
      lbl_additional_details: 'Additional details',
    },
  };

  it('should render CustomerSomethingElse', () => {
    const component = shallow(<CustomerSomethingElseVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
