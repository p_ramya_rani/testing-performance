/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import differenceInHours from 'date-fns/differenceInHours';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { Row, Col, BodyCopy, Image, Anchor } from '@tcp/core/src/components/common/atoms';
import Button from '@tcp/core/src/components/common/atoms/Button';
import {
  moveToStage,
  getRefundedShipmentStatus,
} from '@tcp/core/src/components/features/account/CustomerHelp/util/utility';
import {
  getIconPath,
  getLabelValue,
  configureInternalNavigationFromCMSUrl,
  smoothScrolling,
} from '@tcp/core/src/utils';
import ModuleX from '@tcp/core/src/components/common/molecules/ModuleX';
import CustomerServiceHelp from '../../CustomerServiceHelp';
import CouponSelfHelp from '../../CouponSelfHelp';
import CustomerSomethingElse from '../../CustomerSomethingElse';
import styles from '../styles/Resolution.style';
import {
  missingOrWrongItemsResolutionConst,
  cancelNModifyResolutionConst,
  delayedResolutionConst,
  wrongItemsResolutionConst,
  CUSTOMER_HELP_PAGE_TEMPLATE,
  REASON_CODES,
  shipmentResolutionConst,
  SHIPMENT_METHODS,
} from '../../../../../CustomerHelp.constants';

const { refundItems, resendItemWithFreeShipping, connectWithCustomerSpecialist } =
  missingOrWrongItemsResolutionConst;

const { refundShippingFee } = delayedResolutionConst;

const { refundShipment, resendShipmentWithFreeShipping } = shipmentResolutionConst;

const { cancelOrder, modifyOrder } = cancelNModifyResolutionConst;
const { somethingElse, receiveDifferentItem, receiveWrongColorItem, receiveWrongSizeItem } =
  wrongItemsResolutionConst;

const isWrongItemResolution = (resolution) => {
  return (
    resolution === wrongItemsResolutionConst.somethingElse ||
    resolution === wrongItemsResolutionConst.receiveDifferentItem ||
    resolution === wrongItemsResolutionConst.receiveWrongColorItem ||
    resolution === wrongItemsResolutionConst.receiveWrongSizeItem
  );
};
const handleWrongItemUseCase = (labels, others) => {
  const { selectedResolution, setWrongItemData, wrongItemData } = others;

  switch (selectedResolution) {
    case somethingElse:
    case receiveDifferentItem:
    case receiveWrongColorItem:
    case receiveWrongSizeItem:
      return (
        <CustomerSomethingElse
          className="customer-something-else"
          setWrongItemData={setWrongItemData}
          wrongItemData={wrongItemData}
          labels={labels}
          isSomethingElse={selectedResolution === somethingElse}
        />
      );

    default:
      return null;
  }
};

const refundEligibleReasons = [
  REASON_CODES.TRACKING_NUMBER_SHOW_IN_TRANSIT_FOR_LONG_TIME,
  REASON_CODES.PACKAGE_DELIVERED_BUT_NOT_RECEIVED,
  REASON_CODES.PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME,
];

const getContinueButton = (selectedResolution, isCancellable) => {
  switch (selectedResolution) {
    case refundItems:
    case refundShippingFee:
    case refundShipment:
      return selectedResolution;
    case cancelOrder:
      return isCancellable === 'y' ? selectedResolution : 'order-non-cancellable';
    default:
      return '';
  }
};

const paidShippingRefundNonEligility = ({
  action,
  shippingForm,
  isPaidNExpediteRefundEnable,
  isDeliveredShipmentAppeased,
}) => {
  const { PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME } = REASON_CODES;
  const { STANDARD } = SHIPMENT_METHODS;
  const nonStandardShipment =
    action === PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME && shippingForm !== STANDARD;
  return (
    (nonStandardShipment && isPaidNExpediteRefundEnable === false) ||
    (nonStandardShipment && isDeliveredShipmentAppeased)
  );
};

const getShippingMethodVerified = ({ action, shippingForm }) => {
  const { PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME } = REASON_CODES;
  const { STANDARD, GROUND } = SHIPMENT_METHODS;
  return (
    action === PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME &&
    shippingForm === STANDARD &&
    shippingForm === GROUND
  );
};

const getConditionForNonRefund = ({
  action,
  refundEligibility,
  isOrderStatusTransit,
  hoursDifference,
  shippingForm,
  isPaidNExpediteRefundEnable,
  isDeliveredShipmentAppeased,
}) => {
  const { PACKAGE_DELIVERED_BUT_NOT_RECEIVED } = REASON_CODES;
  return (
    (action !== PACKAGE_DELIVERED_BUT_NOT_RECEIVED &&
      refundEligibleReasons.includes(action) &&
      refundEligibility &&
      refundEligibility.toLowerCase() === 'false' &&
      !isOrderStatusTransit) ||
    (action === PACKAGE_DELIVERED_BUT_NOT_RECEIVED && hoursDifference <= 48) ||
    paidShippingRefundNonEligility({
      action,
      shippingForm,
      isPaidNExpediteRefundEnable,
      isDeliveredShipmentAppeased,
    }) ||
    getShippingMethodVerified({ action, shippingForm })
  );
};

const checkNonRefundEligibility = ({
  refundData,
  action,
  isOrderStatusTransit,
  orderDelivereyDate,
  shippingMethod,
  isPaidNExpediteRefundEnable,
  isDeliveredShipmentAppeased,
}) => {
  const shippingForm = shippingMethod && shippingMethod.toLowerCase();
  const refundEligibility = refundData && refundData.refundEligible;
  let hoursDifference;
  const daysSla = orderDelivereyDate ? new Date(orderDelivereyDate) : null;
  if (daysSla) {
    hoursDifference = differenceInHours(new Date(), daysSla);
  }
  if (
    getConditionForNonRefund({
      action,
      refundEligibility,
      isOrderStatusTransit,
      hoursDifference,
      shippingForm,
      isPaidNExpediteRefundEnable,
      isDeliveredShipmentAppeased,
    })
  ) {
    return true;
  }
  return false;
};

export const getSelectedResolutionAction = (labels, others) => {
  const {
    selectedResolution,
    resolutionModuleXContent,
    cancelResolutionModuleXContent,
    selectedReasonCode,
    orderDetailsData,
    hideButton,
    selectedOrderItems,
    isLiveChatEnabled,
    isInternationalShipping,
    trackAnalyticsClick,
    selectedReturnMethod,
  } = others;

  const { isCancellable } = orderDetailsData || {};
  const continueButton = getContinueButton(selectedResolution, isCancellable);
  switch (selectedResolution) {
    case continueButton:
      return (
        !hideButton && (
          <Row fullBleed className="elem-mt-LRG align-center">
            <Col colSize={{ small: 6, medium: 4, large: 4 }} className="continue-btn">
              <Button
                onClick={() => {
                  moveToStage(CUSTOMER_HELP_PAGE_TEMPLATE.REVIEW);
                }}
                buttonVariation="fixed-width"
                className="cta-btn continue-button"
                fill="BLUE"
              >
                {getLabelValue(labels, 'lbl_help_refund_action_btn')}
              </Button>
            </Col>
          </Row>
        )
      );
    case resendItemWithFreeShipping:
    case resendShipmentWithFreeShipping:
      return (
        <CustomerServiceHelp
          labels={labels}
          helpMessage={getLabelValue(labels, 'lbl_resend_with_free_shipping_help')}
          selectedReasonCode={selectedReasonCode}
          orderDetailsData={orderDetailsData}
          selectedOrderItems={selectedOrderItems}
          isLiveChatEnabled={isLiveChatEnabled}
          isInternationalShipping={isInternationalShipping}
          trackAnalyticsClick={trackAnalyticsClick}
          selectedReturnMethod={selectedReturnMethod}
        />
      );

    case modifyOrder:
      return (
        <div className="modify-order-area">
          <Row fullBleed className="elem-ml-SM elem-mr-SM" id="step-7">
            <Col colSize={{ small: 6, medium: 4, large: 4 }} className="info-icon-container">
              <Image width="20px" height="20px" src={getIconPath('info-icon')} alt="info" />
            </Col>
            <Col
              colSize={{ small: 6, medium: 4, large: 4 }}
              className="elem-mt-LRG module-container"
            >
              {Object.keys(resolutionModuleXContent).length > 0 && (
                <ModuleX richTextList={[{ text: resolutionModuleXContent.richText }]} />
              )}
            </Col>
          </Row>
        </div>
      );

    case connectWithCustomerSpecialist:
      return (
        <CustomerServiceHelp
          labels={labels}
          helpMessage={getLabelValue(labels, 'lbl_connect_with_customer_service_specialist_help')}
          selectedReasonCode={selectedReasonCode}
          orderDetailsData={orderDetailsData}
          selectedOrderItems={selectedOrderItems}
          isLiveChatEnabled={isLiveChatEnabled}
          isInternationalShipping={isInternationalShipping}
          trackAnalyticsClick={trackAnalyticsClick}
          selectedReturnMethod={selectedReturnMethod}
        />
      );
    default:
      if (continueButton === 'order-non-cancellable') {
        return (
          <div className="modify-order-area">
            <Row fullBleed className="elem-ml-SM elem-mr-SM" id="step-7">
              <Col colSize={{ small: 6, medium: 4, large: 4 }} className="info-icon-container">
                <Image width="20px" height="20px" src={getIconPath('info-icon')} alt="info" />
              </Col>
              <Col
                colSize={{ small: 6, medium: 4, large: 4 }}
                className="elem-mt-LRG module-container"
              >
                {Object.keys(cancelResolutionModuleXContent).length > 0 && (
                  <ModuleX richTextList={[{ text: cancelResolutionModuleXContent.richText }]} />
                )}
              </Col>
            </Row>
          </div>
        );
      }
      return handleWrongItemUseCase(labels, others);
  }
};

const getSelectedOptionsHelpMessage = (selectedOption, labels) => {
  let label;
  if (selectedOption) {
    if (selectedOption === refundItems) {
      label = getLabelValue(labels, 'lbl_refund_items_help');
    } else if (selectedOption === refundShippingFee) {
      label = getLabelValue(labels, 'lbl_refund_shipping_fee_msg');
    } else if (selectedOption === refundShipment) {
      label = getLabelValue(labels, 'lbl_refund_shipment_help');
    }
  }

  return (
    <>
      {label && (
        <Row fullBleed id="step-7">
          <Col colSize={{ small: 12 }}>
            <BodyCopy
              component="p"
              fontWeight="semibold"
              fontSize="fs14"
              fontFamily="secondary"
              color="black"
              className="elem-pl-XS elem-pr-XS selected-help-message"
            >
              {label}
            </BodyCopy>
          </Col>
        </Row>
      )}
    </>
  );
};

const enableHelpTextHeadingPaidNExpedite = ({
  nonRefundable,
  isOrderStatusTransit,
  shippingMethod,
}) => {
  const { STANDARD, GROUND } = SHIPMENT_METHODS;
  const shippingForm = shippingMethod && shippingMethod.toLowerCase();
  const noExpressDelivery = shippingForm === STANDARD || shippingForm === GROUND;
  if (nonRefundable || isOrderStatusTransit || noExpressDelivery) {
    return false;
  }
  return true;
};

const getWrongItemHeading = ({ isWrongItems, labels }) => {
  if (isWrongItems) {
    return getLabelValue(labels, 'lbl_wrong_item_help', '');
  }
  return getLabelValue(labels, 'lbl_how_to_solve', '');
};

const getDeliveredHeading = ({ labels, nonRefundable, selectedShipmentCard }) => {
  if (!nonRefundable && selectedShipmentCard) {
    return getLabelValue(labels, 'lbl_how_to_solve', '');
  }
  return null;
};

const getResolutionHeading = (
  action,
  labels,
  selectedShipmentCard,
  resolutionList,
  isSomethingElse,
  ...others
) => {
  const [nonRefundable, isOrderStatusTransit, shippingMethod, isWrongItems, stepId] = others;
  const {
    CANCEL_MODIFY,
    PACKAGE_DELIVERED_BUT_NOT_RECEIVED,
    TRACKING_NUMBER_SHOW_IN_TRANSIT_FOR_LONG_TIME,
    PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME,
  } = REASON_CODES;
  let label = '';

  switch (action) {
    case CANCEL_MODIFY:
      label = getLabelValue(labels, 'lbl_select_an_option_title', '');
      break;
    case TRACKING_NUMBER_SHOW_IN_TRANSIT_FOR_LONG_TIME: {
      label = selectedShipmentCard ? getLabelValue(labels, 'lbl_how_to_solve', '') : null;
      break;
    }
    case PACKAGE_DELIVERED_BUT_NOT_RECEIVED: {
      label = getDeliveredHeading({ labels, nonRefundable, selectedShipmentCard });
      break;
    }
    case PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME:
      label = enableHelpTextHeadingPaidNExpedite({
        nonRefundable,
        isOrderStatusTransit,
        shippingMethod,
      })
        ? getLabelValue(labels, 'lbl_how_to_solve', '')
        : '';
      break;

    default:
      label = getWrongItemHeading({ isWrongItems, labels });
      break;
  }

  if (isSomethingElse) {
    label = getLabelValue(labels, 'lbl_connect_directly_specialist', '');
  }

  const id = isSomethingElse ? 'step-4' : stepId;
  return (
    resolutionList && (
      <div className="header-title" id={id}>
        <BodyCopy
          component="p"
          fontWeight="bold"
          fontFamily="secondary"
          fontSize="fs16"
          className="elem-mt-XXL elem-mb-MED"
        >
          {label}
        </BodyCopy>

        {isSomethingElse && (
          <BodyCopy
            component="p"
            fontFamily="secondary"
            fontSize="fs14"
            className="elem-mt-XXL elem-mb-MED elem-pl-XS elem-pr-XS select-option"
          >
            {getLabelValue(labels, 'lbl_select_an_option', '')}
          </BodyCopy>
        )}
      </div>
    )
  );
};

const getResolutionUpperContent = (selectedOption, labels, expectedDeliveryDate) => {
  switch (selectedOption) {
    case REASON_CODES.CANCEL_MODIFY:
      return getLabelValue(labels, 'lbl_select_an_option_title', '');
    case REASON_CODES.TRACKING_NUMBER_SHOW_IN_TRANSIT_FOR_LONG_TIME: {
      return (
        <BodyCopy component="p" fontWeight="bold" fontFamily="secondary" fontSize="fs16">
          {getLabelValue(labels, 'lbl_resolution_transit_long_time', '')}
          {expectedDeliveryDate && <span className="elem-ml-XXS">{expectedDeliveryDate}</span>}
        </BodyCopy>
      );
    }
    case REASON_CODES.PACKAGE_DELIVERED_BUT_NOT_RECEIVED: {
      return (
        <BodyCopy component="p" fontWeight="bold" fontFamily="secondary" fontSize="fs16">
          {getLabelValue(labels, 'lbl_resolution_package_delivered_not_received', '')}
        </BodyCopy>
      );
    }
    default:
      return (
        <BodyCopy component="p" fontWeight="bold" fontFamily="secondary" fontSize="fs16">
          {getLabelValue(labels, 'lbl_how_to_solve', '')}
        </BodyCopy>
      );
  }
};

const renderSelectedResolution = (labels, others) => {
  const {
    selectedWrongItemResolution,
    selectedResolution,
    selectedReasonCode,
    resolutionList,
    isLoggedIn,
    orderDetailsData,
    selectedOrderItems,
    isLiveChatEnabled,
    isInternationalShipping,
    orderLabels,
    walletLabels,
    trackAnalyticsClick,
    selectedReturnMethod,
  } = others;

  const { action } = selectedReasonCode || {};
  const isSomethingElse = action === REASON_CODES.SOMETHING_ELSE;
  const couponSelfHelpLabels = { ...labels, ...walletLabels };

  return (
    <>
      {selectedResolution || (selectedWrongItemResolution && !isSomethingElse) ? (
        <>
          {getSelectedOptionsHelpMessage(selectedResolution, labels)}
          {getSelectedResolutionAction(labels, others)}
        </>
      ) : (
        <>
          {isSomethingElse && resolutionList && (
            <CustomerServiceHelp
              labels={labels}
              isSomethingElse
              selectedReasonCode={selectedReasonCode}
              orderDetailsData={orderDetailsData}
              selectedOrderItems={selectedOrderItems}
              isLiveChatEnabled={isLiveChatEnabled}
              isInternationalShipping={isInternationalShipping}
              trackAnalyticsClick={trackAnalyticsClick}
              selectedReturnMethod={selectedReturnMethod}
            />
          )}
          {selectedReasonCode.action === REASON_CODES.COUPON_ISSUE && (
            <CouponSelfHelp
              orderLabels={orderLabels}
              isLoggedIn={isLoggedIn}
              labels={{ ...couponSelfHelpLabels, ...labels }}
            />
          )}
        </>
      )}
    </>
  );
};

const renderAllResolution = (
  resolutionList,
  isSomethingElse,
  selectedResolution,
  selectedWrongItemResolution,
  handleSelection,
  isOrderStatusTransit,
  ...others
) => {
  const [labels, nonRefundable] = others;
  if (resolutionList && !isSomethingElse && !isOrderStatusTransit && !nonRefundable) {
    return (
      <Row fullBleed className="resolution-space">
        {Object.keys(resolutionList).map((resolution) => {
          const resolutionText = getLabelValue(labels, resolutionList[resolution], '');
          return (
            <Col
              colSize={{ small: 6, medium: 4, large: 4 }}
              className={`reason-code-rectangle resolution elem-mb-LRG ${
                selectedResolution === resolution || selectedWrongItemResolution === resolution
                  ? 'selected-item'
                  : ''
              }`}
              onClick={(e) => handleSelection(e, resolutionText)}
              id={resolution}
            >
              <BodyCopy
                component="p"
                textAlign="left"
                fontWeight="semibold"
                fontSize="fs14"
                fontFamily="secondary"
                color="text.dark"
                className="elem-pl-MED elem-pr-MED"
              >
                {resolutionText}
              </BodyCopy>
            </Col>
          );
        })}
      </Row>
    );
  }
  return null;
};

const getPaidShippingHelpText = (
  labels,
  action,
  refundData,
  shippingMethod,
  isOrderStatusTransit
) => {
  const { STANDARD, GROUND } = SHIPMENT_METHODS;
  const shippingForm = shippingMethod && shippingMethod.toLowerCase();
  const noExpressDelivery = shippingForm === STANDARD || shippingForm === GROUND;
  if (noExpressDelivery) {
    return getLabelValue(labels, 'lbl_no_express_selected', '');
  }
  return isOrderStatusTransit ? getLabelValue(labels, 'lbl_order_must_be_delivered', '') : '';
};

const getNoExpressDeliveryVerified = ({
  noExpressDelivery,
  isDeliveredShipmentAppeased,
  action,
}) => {
  const { PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME } = REASON_CODES;
  return (
    (noExpressDelivery && action === PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME) ||
    (isDeliveredShipmentAppeased && action === PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME)
  );
};

const getPaidShippingUnsuccessFull = ({
  isOrderStatusTransit,
  selectedReasonCode,
  nonRefundable,
  action,
  shippingForm,
  isDeliveredShipmentAppeased,
}) => {
  const { STANDARD, GROUND } = SHIPMENT_METHODS;
  const { PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME } = REASON_CODES;
  const noExpressDelivery = shippingForm === STANDARD || shippingForm === GROUND;
  if (
    (isOrderStatusTransit &&
      selectedReasonCode &&
      action === PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME) ||
    (nonRefundable && action === PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME) ||
    getNoExpressDeliveryVerified({ noExpressDelivery, isDeliveredShipmentAppeased, action })
  ) {
    return true;
  }
  return false;
};

const renderPaidShippingHelp = (
  labels,
  orderLabels,
  isOrderStatusTransit,
  selectOrderAction,
  selectedReasonCode,
  refundData,
  ...others
) => {
  const [shippingMethod, nonRefundable, isDeliveredShipmentAppeased] = others;
  const { STANDARD, GROUND } = SHIPMENT_METHODS;
  const { action } = selectedReasonCode || {};
  const shippingForm = shippingMethod && shippingMethod.toLowerCase();
  const noExpressDelivery = shippingForm === STANDARD || shippingForm === GROUND;
  const infoIconCondition = noExpressDelivery || isOrderStatusTransit;
  const unSuccessFull = getPaidShippingUnsuccessFull({
    isOrderStatusTransit,
    selectedReasonCode,
    nonRefundable,
    action,
    shippingForm,
    isDeliveredShipmentAppeased,
  });
  if (unSuccessFull) {
    return (
      <div className="resolution-content-area elem-mt-LRG">
        <div className="resolution-container">
          <BodyCopy
            component="p"
            fontFamily="secondary"
            fontSize="fs16"
            fontWeight="bold"
            className="alignCards"
          >
            {getLabelValue(labels, 'lbl_how_to_solve', '')}
          </BodyCopy>
          <Row fullBleed className="help-text-container">
            {infoIconCondition && (
              <Image
                width="24px"
                height="24px"
                src={getIconPath('info-icon')}
                alt="info"
                className="elem-mt-XS icon-container-shipping"
              />
            )}
            <BodyCopy
              component="p"
              fontFamily="secondary"
              fontSize="fs16"
              className="elem-mt-XS elem-ml-XXL help-text"
            >
              {getPaidShippingHelpText(
                labels,
                action,
                refundData,
                shippingMethod,
                isOrderStatusTransit
              )}
            </BodyCopy>
          </Row>
          {(noExpressDelivery || !isOrderStatusTransit) && <CustomerServiceHelp labels={labels} />}
          <Row fullBleed centered>
            <div className="review-button">
              <div className="review-submit-align">
                <Button
                  className="footer-button "
                  fontSize="fs14"
                  fontWeight="extrabold"
                  buttonVariation="variable-width"
                  fill="WHITE"
                  onClick={selectOrderAction}
                >
                  {getLabelValue(orderLabels?.orders, 'lbl_help_another_order')}
                </Button>
              </div>
            </div>
          </Row>
        </div>
      </div>
    );
  }
  return null;
};

const Resolution = ({ labels, className, ...others }) => {
  const {
    resolutionList,
    setResolution,
    selectedResolution,
    resolutionModuleXContent,
    selectedReasonCode,
    transitHelpModuleXContent,
    selectedShipmentCard,
    refundData,
    selectOrderAction,
    selectedWrongItemResolution,
    isOrderStatusTransit,
    orderLabels,
    orderDetailsData,
    trackAnalyticsClick,
    setWrongItemData,
    orderDelivereyDate,
    clickAnalyticsData,
    wrongItemResolution,
    isPaidNExpediteRefundEnable,
    clearSelectedResolution,
    stepId,
  } = others;

  useEffect(() => {
    if (selectedResolution) {
      setTimeout(() => {
        smoothScrolling('step-7');
      }, 100);
    }
  }, [selectedResolution]);

  useEffect(() => {
    if (selectedWrongItemResolution) {
      setTimeout(() => {
        smoothScrolling('somethingElseContainer');
      }, 100);
    }
  }, [selectedWrongItemResolution]);

  let step1 = '';
  let clickPath = '';

  const { action } = selectedReasonCode || {};
  const { shippingMethod, orderItems = [] } = orderDetailsData || {};
  const { expectedDeliveryDate } = refundData || {};

  const isDeliveredShipmentAppeased = getRefundedShipmentStatus({ orderItems });

  const handleSelection = (e, resolutionText) => {
    const selectedResolutionName = e.target.id || e.currentTarget.id;

    if (!wrongItemResolution) {
      step1 = resolutionText;
      clickPath = `${clickAnalyticsData?.orderHelpSelectedReason}|${resolutionText}`;
    } else {
      step1 = clickAnalyticsData?.orderHelpSelectedResolution;
      clickPath = `${clickAnalyticsData?.orderHelpSelectedReason}|${step1}|${resolutionText}`;
    }

    if (!clickPath.includes(undefined)) {
      trackAnalyticsClick({
        orderHelpSelectedReason: clickAnalyticsData?.orderHelpSelectedReason,
        orderHelpSelectedResolution: step1,
        orderHelp_path_cd128: clickPath,
      });
    }
    if (
      action === REASON_CODES.RECEIVED_WRONG_ITEM &&
      isWrongItemResolution(selectedResolutionName)
    ) {
      setWrongItemData({});
      clearSelectedResolution();
      setResolution({ selectedWrongItemResolution: selectedResolutionName });
    } else {
      setResolution({ selectedResolution: selectedResolutionName });
    }
  };

  const isSomethingElse = action === REASON_CODES.SOMETHING_ELSE;
  const isWrongItems = action === REASON_CODES.RECEIVED_WRONG_ITEM;
  const href = getLabelValue(labels, 'lbl_email_us_href');
  const nonRefundable = checkNonRefundEligibility({
    refundData,
    action,
    isOrderStatusTransit,
    orderDelivereyDate,
    shippingMethod,
    isPaidNExpediteRefundEnable,
    isDeliveredShipmentAppeased,
  });
  return (
    selectedReasonCode?.action !== REASON_CODES.WHEN_WILL_MY_ORDER_ARRIVE && (
      <div className={`${className} `} id="step-5">
        {!nonRefundable &&
          getResolutionHeading(
            action,
            labels,
            selectedShipmentCard,
            resolutionList,
            isSomethingElse,
            nonRefundable,
            isOrderStatusTransit,
            shippingMethod,
            isWrongItems,
            stepId
          )}
        {selectedShipmentCard && nonRefundable && (
          <div className="resolution-content-area">
            <div className="resolution-container">
              <BodyCopy
                component="p"
                fontFamily="secondary"
                fontSize="fs16"
                fontWeight="bold"
                className="elem-ml-SM"
              >
                {getLabelValue(labels, 'lbl_how_to_solve', '')}
              </BodyCopy>

              <Row fullBleed>
                <Col colSize={{ small: 6, medium: 4, large: 4 }} className="info-icon-container">
                  <Image width="20px" height="20px" src={getIconPath('info-icon')} alt="info" />
                </Col>
                <Col
                  colSize={{ small: 6, medium: 4, large: 4 }}
                  className="elem-mt-LRG module-x-container"
                >
                  {getResolutionUpperContent(
                    selectedReasonCode.action,
                    labels,
                    expectedDeliveryDate
                  )}
                </Col>
              </Row>

              <Row fullBleed>
                <Col
                  colSize={{ small: 6, medium: 4, large: 4 }}
                  className="elem-mt-SM module-container-transit"
                >
                  {Object.keys(resolutionModuleXContent).length > 0 && (
                    <ModuleX richTextList={[{ text: transitHelpModuleXContent.richText }]} />
                  )}
                </Col>
              </Row>

              <Row fullBleed>
                <Col
                  colSize={{ small: 6, medium: 4, large: 4 }}
                  className="module-container-transit"
                >
                  <BodyCopy component="p" fontFamily="secondary" fontSize="fs14">
                    {`${getLabelValue(labels, 'lbl_further_assistance', '')}`}
                    <Anchor
                      component="a"
                      to={configureInternalNavigationFromCMSUrl(href)}
                      asPath={href}
                      fontWeight="semibold"
                      fontSize="fs14"
                      fontFamily="secondary"
                      color="text.dark"
                      underline
                    >
                      {` ${getLabelValue(labels, 'lbl_contact_customer_service', '')}`}
                    </Anchor>
                  </BodyCopy>
                </Col>
              </Row>

              <Row className="help-with-another-btn" fullBleed centered>
                <Col colSize={{ large: 12, medium: 8, small: 6 }}>
                  <div className="review-button">
                    <div className="review-submit-align">
                      <Button
                        className="footer-button "
                        fontSize="fs14"
                        fontWeight="extrabold"
                        buttonVariation="variable-width"
                        fill="WHITE"
                        onClick={selectOrderAction}
                      >
                        {getLabelValue(orderLabels?.orders, 'lbl_help_another_order')}
                      </Button>
                    </div>
                  </div>
                </Col>
              </Row>
            </div>
          </div>
        )}

        {renderAllResolution(
          resolutionList,
          isSomethingElse,
          selectedResolution,
          selectedWrongItemResolution,
          handleSelection,
          isOrderStatusTransit,
          labels,
          nonRefundable
        )}
        {renderPaidShippingHelp(
          labels,
          orderLabels,
          isOrderStatusTransit,
          selectOrderAction,
          selectedReasonCode,
          refundData,
          shippingMethod,
          nonRefundable,
          isDeliveredShipmentAppeased
        )}
        {renderSelectedResolution(labels, others)}
      </div>
    )
  );
};

Resolution.propTypes = {
  orderDetailsData: PropTypes.shape({}).isRequired,
  selectedReasonCode: PropTypes.shape({}),
  labels: PropTypes.shape({}).isRequired,
  walletLabels: PropTypes.shape({}).isRequired,
  className: PropTypes.string,
  resolutionList: PropTypes.shape({}),
  setResolution: PropTypes.func.isRequired,
  selectedResolution: PropTypes.string,
  resolutionModuleXContent: PropTypes.shape({}),
  cancelResolutionModuleXContent: PropTypes.shape({}),
  transitHelpModuleXContent: PropTypes.shape({}),
  selectedShipmentCard: PropTypes.string,
  refundData: PropTypes.shape({}),
  selectOrderAction: PropTypes.func,
  clearSelectedResolution: PropTypes.func,
  isLoggedIn: PropTypes.bool,
  isLiveChatEnabled: PropTypes.bool.isRequired,
  selectedReturnMethod: PropTypes.string,
};

Resolution.defaultProps = {
  className: '',
  resolutionList: {},
  selectedResolution: '',
  resolutionModuleXContent: {},
  cancelResolutionModuleXContent: {},
  selectedReasonCode: {},
  transitHelpModuleXContent: {},
  selectedShipmentCard: '',
  refundData: {},
  selectOrderAction: () => {},
  clearSelectedResolution: () => {},
  isLoggedIn: false,
  selectedReturnMethod: '',
};

export default withStyles(Resolution, styles);
export { Resolution as ResolutionVanilla };
