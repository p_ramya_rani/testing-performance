import React from 'react';
import parseISO from 'date-fns/parseISO';
import format from 'date-fns/format';
import isValid from 'date-fns/isValid';
import { isOldItem } from '@tcp/core/src/components/features/account/CustomerHelp/util/utility';
import { Col, Row, BodyCopy, Button } from '../../../../../../../../common/atoms';
import { getFormatedOrderDate } from '../../../../../../../../../utils/utils';
import { getTotalItemsCount, getResolutionComponent } from '../../../views/TellUsMore.view';
import { convertToISOString, getLabelValue } from '../../../../../../../../../utils';
import ReasonCode from '../../ReasonCode';
import ReasonCodeSkeleton from '../../../skeleton/ReasonCodeSkeleton.view';
import Resolution from '../../Resolution';
import {
  FILTERED_REASON_CODES_ON_SHIPPMENT_STATUS,
  REASON_CODES,
} from '../../../../../CustomerHelp.constants';

export const getOrderItem = ({
  orderDetailsData,
  orderItems,
  cshReturnItemOlderDays,
  cshForMonthsToDisplay,
  shipmentData,
}) => {
  const {
    orderType,
    orderNumber,
    orderDate,
    encryptedEmailAddress,
    status,
    orderStatus,
    shippingMethod,
    summary,
    isBopisOrder,
    isBossOrder,
    purchasedItems,
    canceledItems,
  } = orderDetailsData || {};
  const { imagePath, name } =
    (orderItems && orderItems.length > 0 && orderItems[0].productInfo) || {};
  const ItemsCount = getTotalItemsCount(orderItems);
  const isoParsedDate = parseISO(orderDate);
  const orderDateParsed = isValid(isoParsedDate) && format(isoParsedDate, 'MM/dd/yyyy');
  const isOlderItem = isOldItem(new Date(convertToISOString(orderDate)), cshForMonthsToDisplay);
  const isOlderInitiateReturn = isOldItem(
    new Date(convertToISOString(orderDate)),
    cshReturnItemOlderDays
  );
  const { currencySymbol, grandTotal } = summary || {};
  const parsedDate = new Date(orderDateParsed);
  return {
    orderNumber,
    orderDate: getFormatedOrderDate(parsedDate),
    currencySymbol,
    grandTotal,
    productName: name,
    imagePath,
    orderType,
    encryptedEmailAddress,
    ItemsCount,
    orderStatus,
    status,
    shippingMethod,
    isOlderItem,
    isBopisOrder,
    isBossOrder,
    purchasedItems,
    shipmentData,
    canceledItems,
    isOlderInitiateReturn,
  };
};
export const getReasonCodeComponent = ({
  reasons,
  setSelectedDeliveredReasonCode,
  selectedDeliveredReasonCode,
  clearSelectedResolution,
  labels,
  isLoggedIn,
  orderDetailsData,
  refundData,
  trackAnalyticsClick,
  shipmentData,
  getRefundStatus,
  preReasonFlag,
  orderItems,
  cshReturnItemOlderDays,
  cshForMonthsToDisplay,
  helpstep1,
}) => {
  const { isOlderInitiateReturn } = getOrderItem({
    orderDetailsData,
    orderItems,
    cshReturnItemOlderDays,
    cshForMonthsToDisplay,
    shipmentData,
  });
  if (reasons?.length > 0) {
    return (
      <Row fullBleed>
        <Col colSize={{ small: 6, medium: 8, large: 12 }}>
          <ReasonCode
            reasonList={reasons}
            setSelectedReasonCode={setSelectedDeliveredReasonCode}
            selectedReasonCode={selectedDeliveredReasonCode}
            clearSelectedReasonCode={() => setSelectedDeliveredReasonCode(null)}
            clearSelectedResolution={() => clearSelectedResolution()}
            labels={labels}
            isLoggedIn={isLoggedIn}
            orderDetailsData={orderDetailsData}
            refundData={refundData}
            trackAnalyticsClick={trackAnalyticsClick}
            shipmentData={shipmentData}
            getRefundStatus={getRefundStatus}
            scrollClassName="reason-codes-scroll"
            preReasonFlag={preReasonFlag}
            isOlderItem={isOlderInitiateReturn}
            helpstep1={helpstep1}
          />
        </Col>
      </Row>
    );
  }
  return (
    <Row fullBleed>
      <ReasonCodeSkeleton />
    </Row>
  );
};

export const renderResolution = ({
  selectedDeliveredReasonCode,
  selectedResolution,
  setResolution,
  refundData,
  selectedOrderItems,
  selectedShipmentCard,
  isPaidNExpediteRefundEnable,
  labels,
  resolutionModuleXContent,
  cancelResolutionModuleXContent,
  transitHelpModuleXContent,
  selectOrderAction,
  estimatedDeliveryDate,
  setWrongItemData,
  wrongItemData,
  isOrderStatusTransit,
  orderLabels,
  orderDetailsData,
  trackAnalyticsClick,
  orderDelivereyDate,
  clickAnalyticsData,
  clearSelectedResolution,
  isLoggedIn,
  isLiveChatEnabled,
  isInternationalShipping,
  selectedWrongItemResolution,
}) => {
  return (
    selectedDeliveredReasonCode && (
      <Row fullBleed className="elem-mt-MED">
        <Col colSize={{ small: 6, medium: 8, large: 12 }}>
          <Resolution
            selectedResolution={
              selectedDeliveredReasonCode.action === REASON_CODES.RECEIVED_WRONG_ITEM
                ? selectedWrongItemResolution
                : selectedResolution
            }
            selectedWrongItemResolution={selectedWrongItemResolution}
            setResolution={setResolution}
            resolutionList={getResolutionComponent(
              selectedDeliveredReasonCode.action,
              refundData,
              selectedOrderItems,
              selectedShipmentCard,
              isPaidNExpediteRefundEnable
            )}
            labels={labels}
            resolutionModuleXContent={resolutionModuleXContent}
            cancelResolutionModuleXContent={cancelResolutionModuleXContent}
            selectedReasonCode={selectedDeliveredReasonCode}
            transitHelpModuleXContent={transitHelpModuleXContent}
            selectedShipmentCard={selectedShipmentCard}
            refundData={refundData}
            selectOrderAction={selectOrderAction}
            estimatedDeliveryDate={estimatedDeliveryDate}
            setWrongItemData={setWrongItemData}
            wrongItemData={wrongItemData}
            isOrderStatusTransit={isOrderStatusTransit}
            orderLabels={orderLabels}
            orderDetailsData={orderDetailsData}
            trackAnalyticsClick={trackAnalyticsClick}
            orderDelivereyDate={orderDelivereyDate}
            clickAnalyticsData={clickAnalyticsData}
            isPaidNExpediteRefundEnable={isPaidNExpediteRefundEnable}
            clearSelectedResolution={clearSelectedResolution}
            isLoggedIn={isLoggedIn}
            selectedOrderItems={selectedOrderItems}
            isLiveChatEnabled={isLiveChatEnabled}
            isInternationalShipping={isInternationalShipping}
            stepId="step-5"
          />
        </Col>
      </Row>
    )
  );
};

const getTellUsMoreHeading = (labels) => {
  return (
    <div className="header-title" id="step-3">
      <BodyCopy
        component="p"
        fontWeight="bold"
        fontFamily="secondary"
        fontSize="fs16"
        className="tell-us-more"
      >
        {getLabelValue(labels, 'lbl_tell_us_more_about_your_shipment', '')}
      </BodyCopy>
    </div>
  );
};

const filterDeliveryReasons = (reasons) => {
  const inDeliveryReasonList = reasons?.filter(({ action }) =>
    FILTERED_REASON_CODES_ON_SHIPPMENT_STATUS.DELIVERY_ACTIONS.includes(action)
  );
  const inRushOrExpressReasonList = reasons?.filter(({ action }) =>
    FILTERED_REASON_CODES_ON_SHIPPMENT_STATUS.RUSH_EXPRESS_ACTIONS.includes(action)
  );
  return [...inDeliveryReasonList, ...inRushOrExpressReasonList];
};

export const renderDelivered = ({
  reasons,
  setSelectedDeliveredReasonCode,
  selectedDeliveredReasonCode,
  clearSelectedResolution,
  labels,
  isLoggedIn,
  orderDetailsData,
  refundData,
  trackAnalyticsClick,
  shipmentData,
  getRefundStatus,
  preReasonFlag,
  orderItems,
  cshReturnItemOlderDays,
  cshForMonthsToDisplay,
  selectedResolution,
  setResolution,
  selectedOrderItems,
  selectedShipmentCard,
  isPaidNExpediteRefundEnable,
  resolutionModuleXContent,
  cancelResolutionModuleXContent,
  transitHelpModuleXContent,
  selectOrderAction,
  estimatedDeliveryDate,
  setWrongItemData,
  wrongItemData,
  isOrderStatusTransit,
  orderLabels,
  orderDelivereyDate,
  clickAnalyticsData,
  isLiveChatEnabled,
  isInternationalShipping,
  selectedWrongItemResolution,
  others,
}) => {
  const { helpstep1 } = others;
  const filteredDeliveryReasons = filterDeliveryReasons(reasons);

  return (
    <>
      <Row fullBleed id="reason-code-header">
        <Col colSize={{ small: 6, medium: 8, large: 12 }}>
          <div className="header-container">
            {getTellUsMoreHeading(labels)}
            {selectedDeliveredReasonCode && (
              <Button
                className="blue-link"
                nohover
                type="button"
                link
                underline
                color="blue"
                onClick={() => {
                  setSelectedDeliveredReasonCode(null);
                  clearSelectedResolution();
                }}
              >
                {getLabelValue(orderLabels, 'lbl_edit_option', 'orders')}
              </Button>
            )}
          </div>
        </Col>
      </Row>
      {getReasonCodeComponent({
        reasons: filteredDeliveryReasons,
        setSelectedDeliveredReasonCode,
        selectedDeliveredReasonCode,
        clearSelectedResolution,
        labels,
        isLoggedIn,
        orderDetailsData,
        refundData,
        trackAnalyticsClick,
        shipmentData,
        getRefundStatus,
        preReasonFlag,
        orderItems,
        cshReturnItemOlderDays,
        cshForMonthsToDisplay,
        helpstep1,
      })}
      {(selectedDeliveredReasonCode?.action === REASON_CODES.PACKAGE_DELIVERED_BUT_NOT_RECEIVED ||
        selectedDeliveredReasonCode?.action ===
          REASON_CODES.PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME ||
        selectedDeliveredReasonCode?.action === REASON_CODES.SOMETHING_ELSE) &&
        renderResolution({
          selectedDeliveredReasonCode,
          refundData,
          labels,
          orderDetailsData,
          trackAnalyticsClick,
          clearSelectedResolution,
          isLoggedIn,
          selectedResolution,
          setResolution,
          selectedOrderItems,
          selectedShipmentCard,
          isPaidNExpediteRefundEnable,
          resolutionModuleXContent,
          cancelResolutionModuleXContent,
          transitHelpModuleXContent,
          selectOrderAction,
          estimatedDeliveryDate,
          setWrongItemData,
          wrongItemData,
          isOrderStatusTransit,
          orderLabels,
          orderDelivereyDate,
          clickAnalyticsData,
          isLiveChatEnabled,
          isInternationalShipping,
          selectedWrongItemResolution,
        })}
    </>
  );
};
