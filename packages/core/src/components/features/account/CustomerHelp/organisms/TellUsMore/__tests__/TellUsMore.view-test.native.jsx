// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import TellUsMore from '../views/TellUsMore.view.native';

describe('TellUsMore Template', () => {
  const props = {
    orderDetailsData: {
      orderNumber: '123123',
      summary: {
        currencySymbol: '$',
        grandTotal: '7.29',
      },
      orderDate: '123',
    },
    orderLabels: {},
    labels: {},
    reasonCodes: [{ label: test }],
    navigation: {
      getParam: () => {
        return '123';
      },
    },
    orderItems: [{ productInfo: { imagePath: 'path' } }],
  };
  it('should render TellUsMore Template', () => {
    const component = shallow(<TellUsMore {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should render Boss/Bopis Order', () => {
    const updatedProps = {
      ...props,
      orderDetailsData: { ...props.orderDetailsData, isBopisOrder: true },
    };
    const component = shallow(<TellUsMore {...updatedProps} />);
    expect(component).toMatchSnapshot();
  });
  it('should render TellUsMore Template with Selected ReasonCode', () => {
    const selectedReasonCode = {
      label: '1001',
    };
    const updatedProps = { ...props, selectedReasonCode };
    const component = shallow(<TellUsMore {...updatedProps} />);
    expect(component).toMatchSnapshot();
  });

  it('should render TellUsMore Template with ReasonCode LOST_ORDER', () => {
    const selectedReasonCode = {
      label: '1001',
      action: 'LOST_ORDER',
    };
    const updatedProps = { ...props, selectedReasonCode };
    const component = shallow(<TellUsMore {...updatedProps} />);
    expect(component).toMatchSnapshot();
  });

  it('should render TellUsMore Template with ReasonCode MISSING_ORDER', () => {
    const selectedReasonCode = {
      label: '1001',
      action: 'MISSING_ORDER',
    };
    const updatedProps = { ...props, selectedReasonCode };
    const component = shallow(<TellUsMore {...updatedProps} />);
    expect(component).toMatchSnapshot();
  });

  it('should render TellUsMore Template with ReasonCode SHIPMENT_DELAYED', () => {
    const selectedReasonCode = {
      label: '1001',
      action: 'SHIPMENT_DELAYED',
    };
    const updatedProps = { ...props, selectedReasonCode };
    const component = shallow(<TellUsMore {...updatedProps} />);
    expect(component).toMatchSnapshot();
  });
});
