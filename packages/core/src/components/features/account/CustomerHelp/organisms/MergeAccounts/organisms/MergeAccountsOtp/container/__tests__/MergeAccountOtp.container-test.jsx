import React from 'react';
import { shallow } from 'enzyme';
import MergeAccountOtpContainer from '../MergeAccountOtp.container';

describe('MergeAccount Otp Container Template', () => {
  const props = {
    labels: {},
    className: '',
    handleOtpSubmission: () => {},
    otpDetail: {},
    resendMessageThreshold: 3000,
    showRecaptcha: false,
    victimEmailAddress: '',
    otpUniqueKey: '',
    setShowOtp: () => {},
    getOtpOnEmail: () => {},
  };
  it('should render correctly', () => {
    const component = shallow(<MergeAccountOtpContainer {...props} />);
    expect(component).toMatchSnapshot();
  });
});
