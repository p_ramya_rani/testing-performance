import { call, takeLatest, put } from 'redux-saga/effects';
import {
  mergeAccountSuccess,
  setAccountStatus,
  setIsFromOTP,
  setMergeAccountButtonLoader,
  setNumberOfRetries,
} from '../../../container/MergeAccounts.actions';
import {
  getOtpDetails as getOtpDetailsAbstractor,
  checkOtpDetails as validateOtpDetailsAbstractor,
} from '../../../../../../../../../services/abstractors/customerHelp/customerHelp';
import MERGE_ACCOUNT_CONSTANTS from '../../../MergeAccounts.constants';

import {
  setOtpDetail,
  getOtpSuccess,
  getOtpFailure,
  setOtpUniqueKey,
} from './MergeAccountOtp.action';
import {
  MERGE_ACCOUNT_OTP_CONSTANTS,
  MERGE_ACCOUNT_OTP_ERROR_CONSTANTS,
} from '../MergeAccountsOtp.constants';
import { isMobileApp } from '../../../../../../../../../utils';

function resetRecaptcha() {
  try {
    if (!isMobileApp()) {
      // eslint-disable-next-line no-unused-expressions
      window?.grecaptcha?.reset();
    }
    return null;
  } catch (error) {
    return error;
  }
}
const isShowError = (e) => {
  return (
    e?.errorCode === MERGE_ACCOUNT_OTP_ERROR_CONSTANTS.INVALID_OTP ||
    e?.errorCode === MERGE_ACCOUNT_OTP_ERROR_CONSTANTS.OTP_EXPIRED ||
    e?.errorCode === MERGE_ACCOUNT_OTP_ERROR_CONSTANTS.OTP_TIME_EXPIRED
  );
};

export function* checkOtpDetails(otpToCheck) {
  const { payload } = otpToCheck;
  const otpCheck = {
    uniqueKey: payload.uniqueKey,
    otp: payload.otp,
  };
  try {
    yield put(setMergeAccountButtonLoader(true));
    yield put(setIsFromOTP(true));
    yield put(setOtpDetail(null));
    const otpDetail = yield call(validateOtpDetailsAbstractor, otpCheck);
    if (otpDetail?.data) {
      yield put(setOtpDetail(otpDetail));
      if (otpDetail.status === 'SUCCESS') {
        yield put(setMergeAccountButtonLoader(false));
        return yield put(mergeAccountSuccess(otpDetail.data?.mergeAccountResponse));
      }
    }
    return null;
  } catch (e) {
    if (isShowError(e)) {
      const retryCount = parseInt(
        e?.errorResponse?.errors[0]?.errorParameters?.numberOfRetries,
        10
      );
      if (retryCount) yield put(setNumberOfRetries(retryCount));
      return yield put(setOtpDetail(e?.errorResponse?.errors[0]));
    }

    if (
      e?.errorCode === MERGE_ACCOUNT_OTP_ERROR_CONSTANTS.RETRY_LIMIT_EXCEEDS ||
      e?.errorCode === MERGE_ACCOUNT_OTP_ERROR_CONSTANTS.ACCOUNT_HOLD
    ) {
      return yield put(setAccountStatus(MERGE_ACCOUNT_CONSTANTS.ACCOUNT_STATUS[1]));
    }
    return yield put(mergeAccountSuccess(e));
  } finally {
    yield put(setMergeAccountButtonLoader(false));
    resetRecaptcha();
  }
}

export function* getOtpDetails(victimEmail) {
  try {
    const response = yield call(getOtpDetailsAbstractor, victimEmail);
    if (response) {
      yield put(getOtpSuccess(response?.body));
      yield put(setOtpUniqueKey(response?.body?.data));
    } else {
      return yield put(getOtpFailure(response));
    }
    return null;
  } catch (e) {
    if (e?.errorCode === 'ACCOUNT_HOLD') {
      yield put(setAccountStatus(MERGE_ACCOUNT_CONSTANTS.ACCOUNT_STATUS[1]));
    }
    return yield put(getOtpFailure(e));
  }
}

export function* MergeAccountOtpSaga() {
  yield takeLatest(MERGE_ACCOUNT_OTP_CONSTANTS.MERGE_ACCOUNT_GET_OTP, getOtpDetails);
  yield takeLatest(MERGE_ACCOUNT_OTP_CONSTANTS.MERGE_ACCOUNT_VALIDATE_OTP, checkOtpDetails);
}

export default MergeAccountOtpSaga;
