// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { BodyCopy, Image } from '@tcp/core/src/components/common/atoms';
import Anchor from '@tcp/core/src/components/common/atoms/Anchor/views/Anchor.native';
import { UrlHandler } from '@tcp/core/src/utils/utils.app';
import { getRCStatus } from '@tcp/core/src/components/features/account/OrderDetails/organism/OrderDetailsWrapper/views/OrderDetailsWrapper.utils';
import {
  getLabelValue,
  hasTrackingNum,
  getTrimmedTrackingNum,
  mobileOrderDateFormatted,
} from '@tcp/core/src/utils/utils';
import { ORDER_ITEM_STATUS } from '../../../../../CustomerHelp.constants';
import {
  OrderHeaderContainer,
  ItemHeader,
  AlignItemHeader,
  OrderStatusLabelText,
  OrderStatusLabel,
} from '../styles/OrderItemHeader.style.native';
import { Container } from '../../../../../styles/CustomerHelp.style.native';
import CircleCheckGreen from '../../../../../../../../../../../mobileapp/src/assets/images/circle-check-green.png';
import InTransit from '../../../../../../../../../../../mobileapp/src/assets/images/fast-shipping.png';
import inProgress from '../../../../../../../../../../../mobileapp/src/assets/images/union.png';
import ReturnedImage from '../../../../../../../../../../../mobileapp/src/assets/images/return_initiated.png';

/**
 * This function component use for return the Order Package Status
 * can be passed in the component.
 * @param otherProps - otherProps object used pass params to other component
 */

const getShipmentIconAndColor = (status) => {
  const { IN_TRANSIT, SHIPMENT_API_DELIVERED, IN_PROGRESS, RETURN_INITIATED } = ORDER_ITEM_STATUS;

  switch (status) {
    case IN_TRANSIT:
      return { icon: InTransit, color: 'black' };

    case SHIPMENT_API_DELIVERED:
      return { icon: CircleCheckGreen, color: 'g600' };

    case IN_PROGRESS:
      return { icon: inProgress, color: 'black' };

    case RETURN_INITIATED:
      return { icon: ReturnedImage, color: 'black' };

    default:
      return { icon: '', color: '' };
  }
};

const isShipmentItems = (orderStatus) =>
  !(
    orderStatus === ORDER_ITEM_STATUS.IN_PROGRESS ||
    orderStatus === ORDER_ITEM_STATUS.RETURN_INITIATED
  );

const getEstimateDate = (estimatedDate) =>
  estimatedDate ? mobileOrderDateFormatted(estimatedDate) : '';

const OrderItemHeader = (props) => {
  const {
    orderStatus,
    orderDisplayStatus,
    trackingNumber,
    trackingUrl,
    orderLabels,
    shipmentNumber,
    ItemsCount,
    shippedDate,
    shipmentStatus,
    actualDeliveryDate,
    carrierDeliveryDate,
    estimatedDeliveryDate,
  } = props;
  const { icon, color } = getShipmentIconAndColor(orderStatus);
  const trimTrackingNumber = getTrimmedTrackingNum(trackingNumber);
  const actualDeliveryDateValue = getEstimateDate(actualDeliveryDate);
  const carrierDeliveryDateValue = getEstimateDate(carrierDeliveryDate);
  const estimatedDeliveryDateValue = getEstimateDate(estimatedDeliveryDate);
  const shipDateDateValue = getEstimateDate(shippedDate);
  const { label, displayDate } =
    getRCStatus(
      shipmentStatus,
      estimatedDeliveryDateValue,
      actualDeliveryDateValue,
      shipDateDateValue,
      carrierDeliveryDateValue,
      orderLabels
    ) || {};

  return (
    <Container>
      <OrderHeaderContainer>
        <ItemHeader>
          {icon && <Image alt="" source={icon} width="20px" height="20px" />}
          <OrderStatusLabel>
            <OrderStatusLabelText color={color}>{orderDisplayStatus}</OrderStatusLabelText>
          </OrderStatusLabel>
          {isShipmentItems(orderStatus) && (
            <BodyCopy
              fontSize="fs14"
              fontFamily="secondary"
              text={`Shipment ${shipmentNumber} of ${ItemsCount}`}
            />
          )}
        </ItemHeader>

        {label && displayDate ? (
          <AlignItemHeader>
            <BodyCopy
              text={`${label} `}
              fontWeight="extrabold"
              fontSize="fs14"
              color="green.500"
              fontFamily="secondary"
            />
            <BodyCopy
              text={displayDate}
              fontWeight="extrabold"
              fontSize="fs14"
              color="green.500"
              fontFamily="secondary"
            />
          </AlignItemHeader>
        ) : null}

        {hasTrackingNum(trimTrackingNumber) ? (
          <AlignItemHeader>
            <BodyCopy
              text={getLabelValue(orderLabels, 'lbl_orders_tracking-cta')}
              fontSize="fs14"
              fontFamily="secondary"
            />
            <Anchor
              to={trackingUrl}
              asPath={trackingUrl}
              onPress={() => UrlHandler(trackingUrl)}
              underlineBlue
              anchorVariation="secondary"
              fontSize="fs14"
              target="_blank"
              text={trimTrackingNumber}
            />
          </AlignItemHeader>
        ) : null}
      </OrderHeaderContainer>
    </Container>
  );
};
OrderItemHeader.propTypes = {
  trackingNumber: PropTypes.string,
  trackingUrl: PropTypes.string,
  orderLabels: PropTypes.shape({
    lbl_orderDetails_shipping: PropTypes.string,
  }),
  orderStatus: PropTypes.string,
  orderDisplayStatus: PropTypes.string,
  shipmentNumber: PropTypes.string,
  ItemsCount: PropTypes.number,
  shippedDate: PropTypes.string,
  theme: PropTypes.shape({}).isRequired,
};

OrderItemHeader.defaultProps = {
  trackingNumber: '',
  trackingUrl: '',
  orderLabels: {
    lbl_orderDetails_shipping: '',
  },
  orderStatus: '',
  orderDisplayStatus: '',
  shipmentNumber: '',
  ItemsCount: 0,
  shippedDate: '',
};

export default OrderItemHeader;
export { OrderItemHeader as OrderItemHeaderVanilla };
