// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { BodyCopy, Row, Col } from '@tcp/core/src/components/common/atoms';
import OrderItem from '../../../../TellUsMore/organisms/OrderItem';
import { REASON_CODES, CUSTOMER_HELP_PAGE_TEMPLATE } from '../../../../../CustomerHelp.constants';
/**
 * This function component use for return the Review item list based on group
 * can be passed in the component.
 * @param otherProps - otherProps object used for showing Review Item list
 */

const getSuccessScenario = (isSelectedOrderItem, orderStatus, action) => {
  const { CANCEL_MODIFY } = REASON_CODES;
  if (action === CANCEL_MODIFY || orderStatus === 'CANCELED ITEM(S)' || isSelectedOrderItem) {
    return true;
  }
  return false;
};

const ReviewItemsList = ({ className, ...otherProps }) => {
  const { items, orderLabels, currencySymbol, selectedOrderItems, orderStatus, action, router } =
    otherProps;

  const isConfirmationPage = router?.query?.section === CUSTOMER_HELP_PAGE_TEMPLATE.CONFIRMATION;

  const renderSelectedOrderItem = (itm, index) => {
    const { lineNumber } = itm;

    const isSelectedOrderItem = selectedOrderItems[lineNumber] > 0;

    const item = isSelectedOrderItem
      ? {
          ...itm,
          ...{ itemInfo: { ...itm.itemInfo, quantity: selectedOrderItems[lineNumber] } },
        }
      : itm;

    return (
      <>
        {getSuccessScenario(isSelectedOrderItem, orderStatus, action) && (
          <Col
            ignoreGutter={(index + 1) % 2 === 0 ? { large: true, medium: true } : {}}
            colSize={{ large: 6, medium: 8, small: 6 }}
            className={`${index === items.length - 1 && index !== 0 ? 'bottom-margin' : ''}`}
          >
            <OrderItem
              key={index.toString()}
              {...{ item }}
              currencySymbol={currencySymbol}
              orderLabels={orderLabels && orderLabels.orders}
              noBorder
              noDisabledState={isConfirmationPage}
            />
          </Col>
        )}
      </>
    );
  };

  return (
    <BodyCopy component="div" className={className}>
      <Row fullBleed>{items.map((item, index) => renderSelectedOrderItem(item, index))}</Row>
    </BodyCopy>
  );
};
ReviewItemsList.propTypes = {
  className: PropTypes.string,
  labels: PropTypes.shape({}),
  orderStatus: PropTypes.string,
};

ReviewItemsList.defaultProps = {
  className: '',
  labels: {},
  orderStatus: '',
};

export default ReviewItemsList;
