// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { reduxForm, Field } from 'redux-form';
import PropTypes from 'prop-types';
import Notification from '@tcp/core/src/components/common/molecules/Notification';
import { isClient, getViewportInfo } from '@tcp/core/src/utils';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import ShortUnderLine from '@tcp/core/src/components/common/atoms/ShortUnderLine';
import styles from '../styles/EmailConfirmationForm.styles';
import { Row, Col, Button, TextBox, BodyCopy } from '../../../../../../../../common/atoms';
import createValidateMethod from '../../../../../../../../../utils/formValidation/createValidateMethod';
import getStandardConfig from '../../../../../../../../../utils/formValidation/validatorStandardConfig';
import withStyles from '../../../../../../../../common/hoc/withStyles';
import { convertHtml } from '../../../../../../../CnC/LoyaltyBanner/util/utility';

const EmailConfirmationForm = React.memo(
  ({
    className,
    handleSubmit,
    fullPage,
    labels,
    emailConfirmationError,
    pristine,
    emailConfirmationLoader,
    emailConfirmationSuccess,
    reset,
    resetEmailConfirmation,
    setShowOtp,
    mergeAccountReset,
  }) => {
    const focusFirstField = () => {
      if (isClient()) {
        const { isMobile = false } = getViewportInfo() || {};
        return fullPage && !isMobile;
      }
      return false;
    };
    const successCheckEnable = !!emailConfirmationSuccess;
    const shouldNextButtonDisabled = () =>
      pristine || emailConfirmationLoader || successCheckEnable;
    const resetEmailConfirmationForm = () => {
      reset();
      resetEmailConfirmation();
      setShowOtp(true);
      mergeAccountReset();
    };
    return (
      <BodyCopy component="div" className={`${className} container`}>
        <Row fullBleed>
          <Col colSize={{ small: 6, medium: 7, large: 12 }}>
            <BodyCopy component="div" className="header-title">
              <BodyCopy component="div">
                <BodyCopy component="p" fontWeight="bold" fontFamily="secondary" fontSize="fs16">
                  {getLabelValue(labels, 'lbl_merge_user__account_information')}
                </BodyCopy>
                <ShortUnderLine />
                <BodyCopy className="title-underline elem-mt-XXXS elem-mb-XXXS" />
              </BodyCopy>
              {successCheckEnable && (
                <BodyCopy component="div">
                  <Button
                    className="edit-option"
                    nohover
                    type="button"
                    link
                    underline
                    onClick={resetEmailConfirmationForm}
                  >
                    {getLabelValue(labels, 'lbl_merge_account_change_email')}
                  </Button>
                </BodyCopy>
              )}
            </BodyCopy>
          </Col>
        </Row>
        <Row fullBleed>
          <Col colSize={{ small: 6, medium: 7, large: 12 }}>
            <BodyCopy component="div" className="top-section elem-mb-REG">
              {emailConfirmationError && (
                <Col colSize={{ small: 6, medium: 4, large: 4 }}>
                  <BodyCopy component="div" className="email-confirmation-error">
                    <Notification
                      status="error"
                      colSize={{ large: 12, medium: 8, small: 6 }}
                      message={emailConfirmationError}
                    />
                  </BodyCopy>
                </Col>
              )}
            </BodyCopy>
            <BodyCopy component="div" className="middle-section">
              <BodyCopy
                component="p"
                fontSize={['fs14', 'fs10', 'fs14']}
                fontWeight="semibold"
                fontFamily="secondary"
                className="elem-mb-LRG description"
              >
                {convertHtml(getLabelValue(labels, 'lbl_merge_text_account_information'))}
              </BodyCopy>
              <form
                onSubmit={handleSubmit}
                name="EmailConfirmationForm"
                noValidate
                className={className}
              >
                <Field
                  id="emailAddress"
                  placeholder="Email Address"
                  name="emailAddress"
                  component={TextBox}
                  dataLocator="email-confirmation-emailfield"
                  errorDataLocator="email-confirmation-emailerror"
                  showSuccessCheck={successCheckEnable}
                  enableSuccessCheck={successCheckEnable}
                  className="input-field"
                  autoFocus={focusFirstField}
                  disabled={successCheckEnable}
                />
                {!successCheckEnable && (
                  <Button
                    fill="BLUE"
                    type="submit"
                    buttonVariation="variable-width"
                    className="elem-mt-MED_1 button-width"
                    disabled={shouldNextButtonDisabled()}
                    loading={emailConfirmationLoader}
                  >
                    {getLabelValue(labels, 'lbl_merge_button_next_account_information')}
                  </Button>
                )}
              </form>
            </BodyCopy>
          </Col>
        </Row>
      </BodyCopy>
    );
  }
);

EmailConfirmationForm.propTypes = {
  className: PropTypes.string,
  emailConfirmationError: PropTypes.string,
  handleSubmit: PropTypes.func.isRequired,
  labels: PropTypes.shape({}).isRequired,
  fullPage: PropTypes.bool,
  pristine: PropTypes.bool,
  emailConfirmationLoader: PropTypes.bool,
  emailConfirmationSuccess: PropTypes.shape({}),
  reset: PropTypes.func,
  resetEmailConfirmation: PropTypes.func.isRequired,
  setShowOtp: PropTypes.func.isRequired,
  mergeAccountReset: PropTypes.func.isRequired,
};

EmailConfirmationForm.defaultProps = {
  className: '',
  emailConfirmationError: '',
  fullPage: false,
  pristine: false,
  emailConfirmationLoader: false,
  emailConfirmationSuccess: {},
  reset: () => {},
};

const validateMethod = createValidateMethod(
  getStandardConfig([{ emailAddress: 'emailAddressNoAsync' }])
);

export default reduxForm({
  form: 'EmailConfirmationForm',
  enableReinitialize: true,
  ...validateMethod,
})(withStyles(EmailConfirmationForm, styles));

export { EmailConfirmationForm as EmailConfirmationFormVanilla };
