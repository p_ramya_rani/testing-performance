// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import { Button, Anchor } from '@tcp/core/src/components/common/atoms';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import { BodyCopyWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';
import { REASON_CODES } from '../../../../../CustomerHelp.constants';
import OrderCard from '../../OrderCard';
import FullBleedView from '../styles/OrderCards.style.native';
import { SideBySideView } from '../../../../../styles/CustomerHelp.style.native';

const getHeadingTitle = (action, selectedShipmentCard, labels) => {
  const { PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME } = REASON_CODES;
  if (action === PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME) {
    return (
      <>
        <BodyCopyWithSpacing
          fontWeight="bold"
          fontFamily="secondary"
          fontSize="fs16"
          text={getLabelValue(labels, 'lbl_your_shipments', '')}
        />
        <BodyCopyWithSpacing
          fontFamily="secondary"
          fontSize="fs14"
          text={` (${getLabelValue(labels, 'lbl_no_selection_needed', '')})`}
        />
      </>
    );
  }
  return (
    <BodyCopyWithSpacing
      fontWeight="bold"
      fontFamily="secondary"
      fontSize="fs16"
      spacingStyles="margin-left-XS"
      text={
        selectedShipmentCard
          ? getLabelValue(labels, 'lbl_selected_shipment', '')
          : getLabelValue(labels, 'lbl_select_the_shipment', '')
      }
    />
  );
};

const OrderCards = ({
  labels,
  setSelectedShipmentCard,
  clearSelectedResolution,
  selectedShipmentCard,
  orderDetailsData,
  orderLabels,
  isDelivered,
  handleShipmentCardClick,
  setIsDelivered,
  action,
  setOrderStatusInTransit,
  setToggleCards,
  toggleCards,
  shipmentData,
  filteredItems,
  setFilteredList,
  ref,
}) => {
  const { PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME } = REASON_CODES;
  return (
    <>
      <FullBleedView>
        <View colSize={{ small: 6, medium: 6, large: 12 }} ref={ref}>
          <SideBySideView>
            <BodyCopyWithSpacing
              spacingStyles="margin-left-XS"
              text={getHeadingTitle(action, selectedShipmentCard, labels)}
            />
            <Anchor
              text={selectedShipmentCard ? getLabelValue(labels, 'lbl_change_shipment', '') : ''}
              fontSizeVariation="large"
              anchorVariation="secondary"
              fontSize="fs14"
              fontFamily="primary"
              onPress={() => {
                setSelectedShipmentCard(null);
                clearSelectedResolution();
                setOrderStatusInTransit(null);
              }}
              underlineBlue
              spacingStyles="margin-left-XS"
            />
          </SideBySideView>
        </View>
      </FullBleedView>
      <OrderCard
        labels={labels}
        orderDetailsData={orderDetailsData}
        orderLabels={orderLabels}
        isDelivered={isDelivered}
        onCardClick={handleShipmentCardClick}
        selectedShipmentCard={selectedShipmentCard}
        action={action}
        setToggleCards={setToggleCards}
        toggleCards={toggleCards}
        shipmentData={shipmentData}
        setIsDelivered={setIsDelivered}
        filteredItems={filteredItems}
        setFilteredList={setFilteredList}
      />

      {!selectedShipmentCard && action === PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME && toggleCards && (
        <FullBleedView centered>
          <Button
            nohover
            type="button"
            link
            color="blue"
            onPress={() => {
              setToggleCards(false);
            }}
          >
            ^
          </Button>
          <Button
            nohover
            type="button"
            link
            underline
            color="blue"
            onPress={() => {
              setToggleCards(false);
            }}
          >
            {getLabelValue(labels, 'lbl_collapse', '')}
          </Button>
        </FullBleedView>
      )}
    </>
  );
};
OrderCards.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  setSelectedShipmentCard: PropTypes.func.isRequired,
  clearSelectedResolution: PropTypes.func.isRequired,
  selectedShipmentCard: PropTypes.string,
  orderDetailsData: PropTypes.shape({}).isRequired,
  orderLabels: PropTypes.shape({}).isRequired,
  shipmentData: PropTypes.shape({}),
  isDelivered: PropTypes.bool,
  handleShipmentCardClick: PropTypes.func.isRequired,
  setIsDelivered: PropTypes.func.isRequired,
  action: PropTypes.string,
  setOrderStatusInTransit: PropTypes.func,
  setToggleCards: PropTypes.func,
  toggleCards: PropTypes.bool,
  setFilteredList: PropTypes.func,
  filteredItems: PropTypes.shape([]),
  ref: PropTypes.shape({}),
};

OrderCards.defaultProps = {
  selectedShipmentCard: '',
  shipmentData: {},
  isDelivered: false,
  action: '',
  setOrderStatusInTransit: () => {},
  setToggleCards: () => {},
  toggleCards: false,
  setFilteredList: () => {},
  filteredItems: [],
  ref: {},
};

export default OrderCards;
