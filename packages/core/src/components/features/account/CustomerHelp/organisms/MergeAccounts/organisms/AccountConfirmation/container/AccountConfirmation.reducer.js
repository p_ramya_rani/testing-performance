// 9fbef606107a605d69c0edbcd8029e5d
import { fromJS } from 'immutable';
import ACCOUNT_CONFIRMATION_CONSTANTS from '../AccountConfirmation.constants';

const initialState = {
  error: null,
  accountConfirmationSuccess: null,
};

const accountConfirmationReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ACCOUNT_CONFIRMATION_CONSTANTS.ACCOUNT_CONFIRMATION_REQUEST: {
      return state.set('error', null).set('accountConfirmationSuccess', null);
    }
    case ACCOUNT_CONFIRMATION_CONSTANTS.ACCOUNT_CONFIRMATION_SUCCESS: {
      return state.set('error', null).set('accountConfirmationSuccess', payload);
    }
    case ACCOUNT_CONFIRMATION_CONSTANTS.ACCOUNT_CONFIRMATION_FAILED: {
      return state.set('error', payload).set('accountConfirmationSuccess', null);
    }
    case ACCOUNT_CONFIRMATION_CONSTANTS.RESET_ACCOUNT_CONFIRMATION: {
      return state.set('accountConfirmationSuccess', null).set('error', null);
    }
    default:
      if (state instanceof Object) {
        return fromJS(state);
      }
      return state;
  }
};

export default accountConfirmationReducer;
