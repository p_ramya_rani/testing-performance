// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { ORDER_ITEM_STATUS } from '../../../../../CustomerHelp.constants';
import { OrderItemsVanilla } from '../views/OrderItem.view.native';

describe('Order Items Native component', () => {
  const props = {
    item: {
      productInfo: {
        color: {
          imagePath: '3016287/3016287_7R_swatch.jpg',
          name: 'REDWOOD',
        },
        fit: null,
        giftOption: 'N',
        imagePath:
          'https://test1.theplace.com/image/upload/ecom/assets/products/tcp/3016287/3016287_7R.jpg',
        lineNumber: '1518191089',
        name: 'Boys Active Fleece Jogger Pants',
        pdpUrl:
          'http://uatlive1.childrensplace.com/us/p/Boys-Active-Fleece-Jogger-Pants-3016287-7R',
        size: 'XS (4)',
        thumbnailImgPath: '3016287/3016287_7R.jpg',
        upc: '00194936100757',
        variantNo: '3016287007',
      },
      itemInfo: {
        linePrice: 2.15,
        itemBrand: 'TCP',
        quantity: 1,
        quantityCanceled: false,
        listPrice: 2.25,
        offerPrice: 2.25,
      },
      trackingInfo: [{ status: ORDER_ITEM_STATUS.SHIPPED }],
    },
    currencySymbol: '$',
    isShowWriteReview: 'true',
    ordersLabels: {},
    selectedReasonCode: {
      action: 'WRONG_ITEMS',
      external: 0,
      id: '/7',
      label: 'I received the wrong item(s)',
      target: '_modal',
      title: '',
    },

    disabled: false,
    itemDisabled: false,
    appeasementApplied: true,
    noDisabledState: false,
  };
  it('should renders correctly', () => {
    const component = shallow(<OrderItemsVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders selected State', () => {
    const updatedProps = { ...props, selected: true };
    const component = shallow(<OrderItemsVanilla {...updatedProps} />);
    expect(component).toMatchSnapshot();
  });

  it('should render with returned status', () => {
    const { item } = props;
    const trackingInfo = [{ status: ORDER_ITEM_STATUS.RETURNED }];
    const updatedProps = {
      ...props,
      item: { ...item, trackingInfo, orderItemStatus: ORDER_ITEM_STATUS.RETURNED },
    };
    const component = shallow(<OrderItemsVanilla {...updatedProps} />);
    expect(component).toMatchSnapshot();
  });

  it('should render with Canceled status', () => {
    const { item } = props;
    const trackingInfo = [{ status: ORDER_ITEM_STATUS.CANCELLED }];
    const updatedProps = {
      ...props,
      item: { ...item, trackingInfo, orderItemStatus: ORDER_ITEM_STATUS.CANCELLED },
    };
    const component = shallow(<OrderItemsVanilla {...updatedProps} />);
    expect(component).toMatchSnapshot();
  });

  it('should render with item refunded', () => {
    const { item } = props;
    const trackingInfo = [{ status: ORDER_ITEM_STATUS.REFUNDED }];
    const updatedProps = {
      ...props,
      item: { ...item, trackingInfo, orderItemStatus: ORDER_ITEM_STATUS.REFUNDED },
    };
    const component = shallow(<OrderItemsVanilla {...updatedProps} />);
    expect(component).toMatchSnapshot();
  });

  it('should render with In-Transit status', () => {
    const { item } = props;
    const trackingInfo = [{ status: ORDER_ITEM_STATUS.RETURNED }];
    const updatedProps = {
      ...props,
      item: { ...item, trackingInfo, orderItemStatus: ORDER_ITEM_STATUS.IN_TRANSIT },
      orderStatus: ORDER_ITEM_STATUS.IN_TRANSIT,
    };
    const component = shallow(<OrderItemsVanilla {...updatedProps} />);
    expect(component).toMatchSnapshot();
  });
  it('should render with In-Progress status', () => {
    const { item } = props;
    const trackingInfo = [{ status: ORDER_ITEM_STATUS.RETURNED }];
    const updatedProps = {
      ...props,
      item: { ...item, trackingInfo, orderItemStatus: ORDER_ITEM_STATUS.IN_PROGRESS },
      orderStatus: ORDER_ITEM_STATUS.IN_PROGRESS,
    };
    const component = shallow(<OrderItemsVanilla {...updatedProps} />);
    expect(component).toMatchSnapshot();
  });

  it('should render with Return Initiated status', () => {
    const { item } = props;
    const trackingInfo = [{ status: ORDER_ITEM_STATUS.RETURN_INITIATED }];
    const updatedProps = {
      ...props,
      item: {
        ...item,
        trackingInfo,
        orderItemStatus: ORDER_ITEM_STATUS.RETURN_INITIATED,
        selectedReasonCode: {
          action: 'RETURN_ITEMS',
          external: 0,
          id: '/7',
          label: 'I received the wrong item(s)',
          target: '_modal',
          title: '',
        },
        appeasementApplied: true,
      },
    };
    const component = shallow(<OrderItemsVanilla {...updatedProps} />);
    expect(component).toMatchSnapshot();
  });

  it('should render with Return Completed status', () => {
    const { item } = props;
    const trackingInfo = [{ status: ORDER_ITEM_STATUS.NonEligibleForReturn }];
    const updatedProps = {
      ...props,
      item: { ...item, trackingInfo, orderItemStatus: ORDER_ITEM_STATUS.NonEligibleForReturn },
    };
    const component = shallow(<OrderItemsVanilla {...updatedProps} />);
    expect(component).toMatchSnapshot();
  });

  it('should render with Delivered status', () => {
    const newProps = {
      item: {
        productInfo: {
          appeasementApplied: false,
          name: 'test product',
          imagePath: 'test',
          color: 'red',
          fit: 'test',
          size: 'test',
          upc: 'test',
          pdpUrl: 'test',
        },
        itemInfo: {
          linePrice: 2.15,
          itemBrand: 'TCP',
          quantity: 1,
          quantityCanceled: false,
          listPrice: 2.25,
          offerPrice: 2.25,
        },
        orderItemStatus: ORDER_ITEM_STATUS.RETURNED,
        trackingInfo: [
          {
            actualDeliveryDate: '2021-10-19 12:19:00',
            appeasement: 0,
            carrier: 'UPS',
            carrierDeliveryDate: null,
            isCSHReturn: 'false',
            quantity: 1,
            refundAmount: 0,
            refundDate: null,
            returnInitiatedDate: null,
            shipDate: '11/10/21 00:00',
            shipVia: 'UGNR',
            status: 'Order Returned',
            trackingNbr: '7NW64107Y600306642_3',
            trackingUrl:
              'https://childrensplace.narvar.com/childrensplace/tracking/ups?tracking_numbers=7NW641',
          },
        ],
        currencySymbol: '$',
        isShowWriteReview: 'true',
        ordersLabels: {},
        selectedReasonCode: {},
      },
    };
    const component = shallow(<OrderItemsVanilla {...newProps} />);
    expect(component).toMatchSnapshot();
  });
});
