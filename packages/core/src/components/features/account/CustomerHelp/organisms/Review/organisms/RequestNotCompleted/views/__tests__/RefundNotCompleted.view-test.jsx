// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import RequestNotCompleted from '../RequestNotCompleted.view';

describe('Refund Under Review component ', () => {
  it('should render correctly', () => {
    const props = {
      labels: {},
      className: '',
    };
    const component = shallow(<RequestNotCompleted {...props} />);
    expect(component).toMatchSnapshot();
  });
});
