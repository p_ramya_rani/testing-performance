// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { Row, BodyCopy, Col, Button } from '@tcp/core/src/components/common/atoms';
import { isMobileWeb, navigateToOrderDetailPage, smoothScrolling } from '@tcp/core/src/utils';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import { REASON_CODES, ORDER_ITEM_STATUS } from '../../../../../CustomerHelp.constants';
import OrderCard from '../../OrderCard';
import internalEndpoints from '../../../../../../common/internalEndpoints';

const getHeadingTitle = (action, selectedShipmentCard, labels) => {
  const { PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME } = REASON_CODES;
  if (action === PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME) {
    return (
      <>
        <BodyCopy component="p" fontWeight="bold" fontFamily="secondary" fontSize="fs16">
          {getLabelValue(labels, 'lbl_your_shipments', '')}
        </BodyCopy>
        <BodyCopy component="span" fontFamily="secondary" fontSize="fs14" className="elem-ml-XS">
          {`(${getLabelValue(labels, 'lbl_no_selection_needed', '')})`}
        </BodyCopy>
      </>
    );
  }
  return (
    <BodyCopy component="p" fontWeight="bold" fontFamily="secondary" fontSize="fs16">
      {selectedShipmentCard
        ? getLabelValue(labels, 'lbl_selected_shipment', '')
        : getLabelValue(labels, 'lbl_select_the_shipment', '')}
    </BodyCopy>
  );
};

const getButtonText = (action, selectedShipmentCard, labels) => {
  if (selectedShipmentCard && action === REASON_CODES.WHEN_WILL_MY_ORDER_ARRIVE) {
    return getLabelValue(labels, 'lbl_check_another_shipment', '');
  }
  return selectedShipmentCard ? getLabelValue(labels, 'lbl_change_shipment', '') : '';
};

const getDescriptionBasedOnTile = (selectedTile, label, displayDate, labels) => {
  const { IN_TRANSIT, SHIPMENT_API_DELIVERED, IN_PROGRESS, ORDER_BEING_PROCESSED, CANCELED } =
    ORDER_ITEM_STATUS;
  if (selectedTile === IN_TRANSIT) {
    if (label && displayDate) {
      return getLabelValue(labels, 'lbl_arrive_transit_cmplt_text');
    }
    return getLabelValue(labels, 'lbl_arrive_transit_incmplt_text');
  }
  if (selectedTile === SHIPMENT_API_DELIVERED) {
    if (label && displayDate) {
      return getLabelValue(labels, 'lbl_arrive_delivered_cmplt_text');
    }
    return getLabelValue(labels, 'lbl_arrive_delivered_incmplt_text');
  }
  if (selectedTile === ORDER_BEING_PROCESSED || selectedTile === IN_PROGRESS) {
    if (label && displayDate) {
      return getLabelValue(labels, 'lbl_arrive_inprocess_cmplt_text');
    }
    return getLabelValue(labels, 'lbl_arrive_inprocess_incmplt_text');
  }
  if (selectedTile === CANCELED) {
    return getLabelValue(labels, 'lbl_arrive_canceled_text');
  }
  return null;
};

const renderInTransitDesc = (label, displayDate, labels, orderLabels) => {
  let text = getLabelValue(labels, 'lbl_arrive_transit_incmplt_text');
  let labelToRender = label;
  console.log('LABEL', label);
  if (label === getLabelValue(orderLabels, 'lbl_edd_arrivedby')) {
    text = getLabelValue(labels, 'lbl_currently_in_transit_estimated');
    labelToRender = getLabelValue(orderLabels, 'lbl_edd_arrive_by');
  } else if (label === getLabelValue(orderLabels, 'lbl_orderDetails_shipment_on')) {
    text = getLabelValue(labels, 'lbl_currently_in_transit_shipped');
  }
  return (
    <>
      {text}
      &nbsp;
      <BodyCopy
        component="span"
        className="rcdate-text"
        fontWeight="semibold"
        fontFamily="secondary"
        fontSize="fs14"
      >
        {`${labelToRender} ${displayDate}.`}
      </BodyCopy>
    </>
  );
};
const renderShipmentStatusAndText = (
  selectedTile,
  itemLength,
  label,
  displayDate,
  labels,
  orderLabels
) => {
  const text = getDescriptionBasedOnTile(selectedTile, label, displayDate, labels);
  const { IN_TRANSIT, CANCELED } = ORDER_ITEM_STATUS;
  return (
    <BodyCopy
      component="p"
      fontFamily="secondary"
      fontSize="fs14"
      className="msg-container-description"
    >
      {selectedTile === CANCELED
        ? getLabelValue(labels, 'lbl_arrive_shipment_start_text_canceled')
        : getLabelValue(labels, 'lbl_arrive_shipment_start_text')}
      &nbsp;
      {itemLength}
      &nbsp;
      {selectedTile !== IN_TRANSIT && (
        <>
          {text}
          &nbsp;
        </>
      )}
      {selectedTile === IN_TRANSIT && (!label || !displayDate) && (
        <>
          {text}
          &nbsp;
        </>
      )}
      {label && displayDate && selectedTile !== IN_TRANSIT && (
        <>
          <BodyCopy
            component="span"
            className="rcdate-text"
            fontWeight="semibold"
            fontFamily="secondary"
            fontSize="fs14"
          >
            {`${label} ${displayDate}.`}
          </BodyCopy>
        </>
      )}
      {label &&
        displayDate &&
        selectedTile === IN_TRANSIT &&
        renderInTransitDesc(label, displayDate, labels, orderLabels?.orders)}
    </BodyCopy>
  );
};

const OrderCards = ({
  labels,
  setSelectedShipmentCard,
  clearSelectedResolution,
  selectedShipmentCard,
  orderDetailsData,
  orderLabels,
  isDelivered,
  handleShipmentCardClick,
  setIsDelivered,
  action,
  setOrderStatusInTransit,
  setToggleCards,
  toggleCards,
  shipmentData,
  filteredItems,
  setFilteredList,
  handleChangeShipment,
  clearOrderArriveSelectedReasonCode,
  selectedTile,
  itemLength,
  RCDate,
  isLoggedIn,
  setSelectedDeliveredReasonCode,
}) => {
  const { orderNumber, encryptedEmailAddress } = orderDetailsData || {};
  const { orderPage } = internalEndpoints;
  const { PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME } = REASON_CODES;
  return (
    orderDetailsData && (
      <>
        {(action !== REASON_CODES.WHEN_WILL_MY_ORDER_ARRIVE || selectedTile) && (
          <Row fullBleed className="elem-mb-MED">
            <Col colSize={{ small: 6, medium: 7, large: 12 }}>
              <div className="header-container" id="myorderArrive-step5">
                <div className="header-title">
                  {getHeadingTitle(action, selectedShipmentCard, labels)}
                </div>

                <Button
                  className="blue-link"
                  nohover
                  type="button"
                  link
                  underline
                  color="blue"
                  onClick={() => {
                    setSelectedShipmentCard(null);
                    clearSelectedResolution();
                    setOrderStatusInTransit(false);
                    clearOrderArriveSelectedReasonCode();
                    setSelectedDeliveredReasonCode(null);
                    if (handleChangeShipment) handleChangeShipment();
                    smoothScrolling('step-4');
                  }}
                >
                  {getButtonText(action, selectedShipmentCard, labels)}
                </Button>
              </div>
              {selectedTile && (
                <BodyCopy
                  component="p"
                  fontWeight="bold"
                  fontFamily="secondary"
                  fontSize="fs14"
                  className="msg-container elem-mb-MED elem-mt-MED"
                >
                  {renderShipmentStatusAndText(
                    selectedTile,
                    itemLength,
                    RCDate?.label,
                    RCDate?.displayDate,
                    labels,
                    orderLabels
                  )}
                  <div className="msg-container-title2">
                    <BodyCopy component="span" fontFamily="secondary" fontSize="fs14">
                      {getLabelValue(labels, 'lbl_arrive_shipment_detail_text')}
                      &nbsp;
                    </BodyCopy>
                    <BodyCopy
                      component="span"
                      fontSize="fs14"
                      fontFamily="secondary"
                      fontWeight="normal"
                      className="blue-link order-link"
                      onClick={() =>
                        navigateToOrderDetailPage(
                          isLoggedIn,
                          orderPage,
                          orderNumber,
                          encryptedEmailAddress
                        )
                      }
                    >
                      {getLabelValue(labels, 'lbl_returns_order_details')}
                    </BodyCopy>
                  </div>
                </BodyCopy>
              )}
            </Col>
          </Row>
        )}
        <OrderCard
          labels={labels}
          orderDetailsData={orderDetailsData}
          orderLabels={orderLabels}
          isDelivered={isDelivered}
          onCardClick={handleShipmentCardClick}
          selectedShipmentCard={selectedShipmentCard}
          action={action}
          setToggleCards={setToggleCards}
          toggleCards={toggleCards}
          shipmentData={shipmentData}
          setIsDelivered={setIsDelivered}
          filteredItems={filteredItems}
          setFilteredList={setFilteredList}
        />

        {!selectedShipmentCard &&
          action === PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME &&
          toggleCards &&
          isMobileWeb() && (
            <Row fullBleed className="elem-mt-LRG" centered>
              <Button
                nohover
                type="button"
                className="blue-link elem-mr-XS"
                link
                color="blue"
                onClick={() => {
                  setToggleCards(false);
                }}
              >
                ^
              </Button>
              <Button
                nohover
                className="blue-link"
                type="button"
                link
                underline
                color="blue"
                onClick={() => {
                  setToggleCards(false);
                }}
              >
                {getLabelValue(labels, 'lbl_collapse', '')}
              </Button>
            </Row>
          )}
      </>
    )
  );
};
OrderCards.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  setSelectedShipmentCard: PropTypes.func.isRequired,
  clearSelectedResolution: PropTypes.func.isRequired,
  selectedShipmentCard: PropTypes.string,
  orderDetailsData: PropTypes.shape({}).isRequired,
  orderLabels: PropTypes.shape({}).isRequired,
  shipmentData: PropTypes.shape({}),
  isDelivered: PropTypes.bool,
  handleShipmentCardClick: PropTypes.func.isRequired,
  setIsDelivered: PropTypes.func.isRequired,
  action: PropTypes.string,
  setOrderStatusInTransit: PropTypes.func,
  setToggleCards: PropTypes.func,
  toggleCards: PropTypes.bool,
  setFilteredList: PropTypes.func,
  filteredItems: PropTypes.shape([]),
  handleChangeShipment: PropTypes.func,
  clearOrderArriveSelectedReasonCode: PropTypes.func,
  isLoggedIn: PropTypes.bool,
  selectedTile: PropTypes.shape({}),
  itemLength: PropTypes.number,
  RCDate: PropTypes.shape({}),
  setSelectedDeliveredReasonCode: PropTypes.func,
};

OrderCards.defaultProps = {
  selectedShipmentCard: '',
  shipmentData: {},
  isDelivered: false,
  action: '',
  setOrderStatusInTransit: () => {},
  setToggleCards: () => {},
  toggleCards: false,
  setFilteredList: () => {},
  filteredItems: [],
  handleChangeShipment: () => {},
  clearOrderArriveSelectedReasonCode: () => {},
  isLoggedIn: false,
  selectedTile: null,
  itemLength: 0,
  RCDate: {},
  setSelectedDeliveredReasonCode: () => {},
};

export default OrderCards;
