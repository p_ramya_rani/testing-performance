// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import { BodyCopy, Image } from '@tcp/core/src/components/common/atoms';
import {
  ViewWithSpacing,
  BodyCopyWithSpacing,
} from '@tcp/core/src/components/common/atoms/styledWrapper';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import { formatAmount } from '../../../../../util/utility';
import { Container, RowContainer } from '../../../../Confirmation/styles/Confirmation.style.native';

const RequestNotCompleted = ({
  imageName,
  title,
  helpMessage,
  caseId,
  labels,
  refundUnderReview,
  currencySymbol,
  refundTotal,
  errorLabel,
}) => {
  return (
    <ViewWithSpacing spacingStyles="margin-top-MED">
      <Container>
        <Image source={imageName} height="49px" width="56px" />

        <BodyCopyWithSpacing
          spacingStyles="margin-top-SM"
          text={title}
          fontSize="fs18"
          fontWeight="extrabold"
          fontFamily="secondary"
          color={refundUnderReview ? 'yellow.700' : 'red.800'}
        />

        <BodyCopyWithSpacing
          spacingStyles="margin-top-SM margin-bottom-MED margin-left-SM margin-right-SM"
          text={helpMessage}
          fontSize="fs14"
          fontWeight="extrabold"
          textAlign="center"
          fontFamily="secondary"
        />

        {!!caseId && (
          <BodyCopy
            text={`${getLabelValue(labels, 'lbl_review_under_case')}${caseId || ''}`}
            fontSize="fs14"
            fontFamily="secondary"
          />
        )}

        {errorLabel ===
          'GC_IN_ORDER' /** Refund items should be visible only when GC_IN_ORDER for refund under review screen */ && (
          <RowContainer spacingStyles="margin-top-MED margin-bottom-MED padding-left-XXXL padding-right-XXXL">
            <BodyCopy
              text={getLabelValue(labels, 'lbl_refund_total')}
              fontSize="fs14"
              fontFamily="secondary"
            />

            <BodyCopy
              text={formatAmount(refundTotal, currencySymbol)}
              fontSize="fs18"
              fontWeight="extrabold"
              fontFamily="secondary"
            />
          </RowContainer>
        )}
      </Container>
    </ViewWithSpacing>
  );
};
RequestNotCompleted.propTypes = {
  imageName: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  helpMessage: PropTypes.string.isRequired,
  caseId: PropTypes.string.isRequired,
  labels: PropTypes.shape({}),
  refundUnderReview: PropTypes.bool,
  currencySymbol: PropTypes.string,
  refundTotal: PropTypes.string,
  errorLabel: PropTypes.string,
};

RequestNotCompleted.defaultProps = {
  labels: {},
  refundUnderReview: false,
  currencySymbol: '',
  refundTotal: '',
  errorLabel: '',
};

export default RequestNotCompleted;
