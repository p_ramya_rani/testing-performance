/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { ScrollView, BackHandler, Dimensions } from 'react-native';
import { Button, BodyCopy } from '@tcp/core/src/components/common/atoms';
import { ViewWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import { getCalculatedRefund } from '../../../util/utility';
import ReviewBasicDetails from '../../Review/organisms/ReviewBasicDetails';
import CustomerServiceHelp from '../../TellUsMore/organisms/CustomerServiceHelp';
import RequestNotCompleted from '../../Review/organisms/RequestNotCompleted';
import ReviewItemsList from '../../Review/organisms/ReviewItemList';
import RefundSummaryDetails from '../../Review/organisms/RefundSummary';
import PaymentDetails from '../../Review/organisms/PaymentDetails';
import {
  REFUND_UNDER_REVIEW_LABELS_MAPPING,
  REASON_CODES,
  REFUND_UNDER_REVIEW,
  GC_IN_ORDER,
} from '../../../CustomerHelp.constants';
import { GrayTopBorder, BlueUnderLine } from '../../../styles/CustomerHelp.style.native';
import { BottomContainer } from '../styles/Confirmation.style.native';

const triangleWarning = require('../../../../../../../../../mobileapp/src/assets/images/triangle-warning.png');
const triangleAlert = require('../../../../../../../../../mobileapp/src/assets/images/triangle-alert.png');
const successCheckIcon = require('../../../../../../../../../mobileapp/src/assets/images/circle-green-check.png');

const windowWidth = Dimensions.get('window').width;

function handleSwipe(onSwipeRight, rangeOffset = 4) {
  let firstTouch = 0;
  function onTouchStart(e) {
    firstTouch = e.nativeEvent.pageX;
  }
  function onTouchEnd(e) {
    const positionX = e.nativeEvent.pageX;
    const range = windowWidth / rangeOffset;
    if (positionX - firstTouch > range && onSwipeRight) {
      onSwipeRight();
    }
  }
  return { onTouchStart, onTouchEnd };
}

const getImageIcon = (errorLabel) =>
  errorLabel === GC_IN_ORDER ? triangleWarning : successCheckIcon;

const getOrderStatusCancelItems = (action, labels) => {
  return action === REASON_CODES.CANCEL_MODIFY
    ? getLabelValue(labels, 'lbl_canceled_items')
    : getLabelValue(labels, 'lbl_refunded_items');
};

const getReviewItemListComponent = (
  orderItems,
  selectedOrderItems,
  action,
  currencySymbol,
  labels,
  orderLabels,
  navigation
) => {
  if (action !== REASON_CODES.PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME) {
    return (
      <ViewWithSpacing spacingStyles="margin-right-MED margin-left-MED margin-top-SM">
        <BodyCopy
          fontSize="fs14"
          fontWeight="extrabold"
          fontFamily="secondary"
          color="black"
          text={getOrderStatusCancelItems(action, labels)}
        />
        <BlueUnderLine />
        {Array.isArray(orderItems) &&
          orderItems.map((orderGroup, index) => (
            <ReviewItemsList
              key={index.toString()}
              orderLabels={orderLabels}
              items={orderGroup.items}
              currencySymbol={currencySymbol}
              orderStatus={getOrderStatusCancelItems(action, labels)}
              isEcol
              labels={labels}
              action={action}
              selectedOrderItems={selectedOrderItems}
              navigation={navigation}
            />
          ))}
      </ViewWithSpacing>
    );
  }
  return null;
};

const renderNonRefundCompleteScreen = (inputParams) => {
  const {
    itemLevelReturns,
    labels,
    caseId,
    errorLabel,
    refundTotal,
    currencySymbol,
    shipmentDelayedStatusData,
    action,
  } = inputParams;
  const { CANCEL_MODIFY } = REASON_CODES;
  if (
    (itemLevelReturns &&
      itemLevelReturns.errorCode === REFUND_UNDER_REVIEW &&
      action !== CANCEL_MODIFY) ||
    (shipmentDelayedStatusData &&
      shipmentDelayedStatusData.errorCode === REFUND_UNDER_REVIEW &&
      action !== CANCEL_MODIFY)
  ) {
    const helpMessage = errorLabel
      ? REFUND_UNDER_REVIEW_LABELS_MAPPING[errorLabel]
      : 'lbl_help_refund_under_review_message';
    return (
      <RequestNotCompleted
        imageName={triangleWarning}
        title={getLabelValue(labels, 'lbl_help_refund_under_review_title')}
        helpMessage={getLabelValue(labels, helpMessage)}
        caseId={caseId}
        labels={labels}
        refundUnderReview
        errorLabel={errorLabel}
        refundTotal={refundTotal}
        currencySymbol={currencySymbol}
      />
    );
  }

  return (
    <RequestNotCompleted
      imageName={triangleAlert}
      title={
        action === REASON_CODES.CANCEL_MODIFY
          ? getLabelValue(labels, 'lbl_unable_cancel')
          : getLabelValue(labels, 'lbl_help_refund_unsuccessful_title')
      }
      helpMessage={
        action === REASON_CODES.CANCEL_MODIFY
          ? getLabelValue(labels, 'lbl_cancel_help')
          : getLabelValue(labels, 'lbl_help_refund_unsuccessful_message')
      }
      caseId={caseId}
      labels={labels}
    />
  );
};

const getSuccessFullHeadingText = (action, labels, errorLabel) => {
  let heading = `${getLabelValue(labels, 'lbl_review_sucess')}`;
  if (action === REASON_CODES.CANCEL_MODIFY) {
    heading = `${getLabelValue(labels, 'lbl_order_cancelled')}`;
  } else if (errorLabel === GC_IN_ORDER) {
    heading = getLabelValue(labels, 'lbl_help_refund_under_review_title');
  }
  return heading;
};

const getSuccessConfirmationText = (action, labels, emailToDisplay, errorLabel) => {
  let msg = `${getLabelValue(labels, 'lbl_cancel_confirm_one')} ${emailToDisplay}`;
  if (action === REASON_CODES.CANCEL_MODIFY) {
    msg = `${getLabelValue(labels, 'lbl_confirmation_mail_sent_to')} ${emailToDisplay}`;
  } else if (errorLabel === GC_IN_ORDER) {
    msg = getLabelValue(
      labels,
      errorLabel
        ? REFUND_UNDER_REVIEW_LABELS_MAPPING[errorLabel]
        : 'lbl_help_refund_under_review_message'
    );
  }
  return msg;
};

const getCaseIDErrorLabelAndAmount = ({
  itemLevelReturns,
  action,
  cancelStatus = '',
  shipmentDelayedStatusData = {},
}) => {
  const { caseId: caseIdRefund, errorCode, status = '' } = itemLevelReturns;
  const {
    caseId: caseIdShippingRefund,
    errorCode: errorCodeShipping,
    status: shippingStatus = '',
    errorLabel: errorLabelShipping,
  } = (shipmentDelayedStatusData && shipmentDelayedStatusData.errorResponse) ||
  shipmentDelayedStatusData;
  const { PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME } = REASON_CODES;
  let errorLabelCode = '';
  if (errorCode === REFUND_UNDER_REVIEW || errorCodeShipping === REFUND_UNDER_REVIEW) {
    if (action === PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME) {
      errorLabelCode = errorLabelShipping;
    } else {
      const { errorLabel } = itemLevelReturns;
      errorLabelCode = errorLabel;
    }
  }

  let caseId = '';
  if (action === PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME) {
    caseId = caseIdShippingRefund;
  } else {
    caseId = caseIdRefund;
  }

  return {
    caseId,
    errorLabel: errorLabelCode,
    isRefundSuccessful: status.toLowerCase() === 'success',
    isCancelSuccessful: cancelStatus.toLowerCase() === 'success',
    isShipmentDelayedSuccessful: shippingStatus.toLowerCase() === 'success',
  };
};

const getRefundDataInfo = ({
  isPaidAndShippingCase,
  shipmentDelayedStatusData,
  itemLevelReturns,
}) => {
  if (isPaidAndShippingCase) {
    if (
      shipmentDelayedStatusData &&
      shipmentDelayedStatusData.errorResponse &&
      shipmentDelayedStatusData.errorResponse.errorLabel &&
      shipmentDelayedStatusData.errorResponse.errorLabel === GC_IN_ORDER
    ) {
      return shipmentDelayedStatusData.errorResponse;
    }
    return shipmentDelayedStatusData;
  }
  return itemLevelReturns;
};

const getOrderPaymentDetails = (action, labels) => {
  return action === REASON_CODES.CANCEL_MODIFY
    ? `${getLabelValue(labels, 'lbl_auth_hold_policies')}${getLabelValue(
        labels,
        'lbl_auth_hold_policies1'
      )}`
    : getLabelValue(labels, 'lbl_payment_details_msg');
};

const renderPaymentView = (orderDetailsData, orderLabels, labels, action) => {
  return (
    Object.keys(orderDetailsData).length > 0 && (
      <PaymentDetails
        labels={labels}
        orderDetailsData={orderDetailsData}
        ordersLabels={orderLabels && orderLabels.orders}
        orderStatus={getOrderPaymentDetails(action, labels)}
        showCardDetails
      />
    )
  );
};

const getOrderStatus = (action, labels) => {
  return action === REASON_CODES.CANCEL_MODIFY
    ? getLabelValue(labels, 'lbl_cancelation_summary')
    : getLabelValue(labels, 'lbl_refund_summary');
};

const getSuccessScenario = (
  isCancelSuccessful,
  isRefundSuccessful,
  isShipmentDelayedSuccessful,
  errorLabel
) => {
  return !!(
    isCancelSuccessful ||
    isRefundSuccessful ||
    isShipmentDelayedSuccessful ||
    errorLabel === GC_IN_ORDER
  );
};

const getEmailToDisplay = (isLoggedIn, loggedInUserEmail, orderDetailsData) =>
  isLoggedIn ? loggedInUserEmail : (orderDetailsData && orderDetailsData.emailAddress) || '';

const shouldRenderConfirmation = (orderDetailsData) =>
  orderDetailsData && Object.keys(orderDetailsData).length;

const triggerAnalytics = (isRefundSuccessful, refundTotal, trackAnalyticsPageView) => {
  if (isRefundSuccessful) {
    trackAnalyticsPageView(
      { metric112: refundTotal, customEvents: ['event112'] },
      { currentScreen: 'orderRefund_successfull_page' }
    );
  }
};

const handleBackButtonClick = ({ isLoggedIn, navigation, selectOrderAction }) => {
  if (isLoggedIn) {
    selectOrderAction({ navigation });
  } else {
    navigation.navigate('Home');
  }
};

/**
 * This function component use for return the ReviewDetailsView
 * can be passed in the component.
 * @param orderDetailsData - orderDetailsData object used for showing Order Details
 */

const Confirmation = ({
  orderDetailsData,
  orderLabels,
  labels,
  selectedOrderItems,
  selectOrderAction,
  itemLevelReturns,
  loggedInUserEmail,
  isLoggedIn,
  cancelStatusData,
  selectedReasonCode,
  shipmentDelayedStatusData,
  navigation,
  trackAnalyticsPageView,
  showLoader,
  hideLoader,
  getOrderSummary,
  resetCustomerHelpReducer,
  isMobileLiveChatEnabled,
  trackAnalyticsClick,
}) => {
  const { summary = {}, orderItems } = orderDetailsData || {};
  const { status } = cancelStatusData || {};
  const { currencySymbol } = summary;
  const { shippingTotal } = getOrderSummary || {};
  const { action } = selectedReasonCode || {};
  const isPaidAndShippingCase = action === REASON_CODES.PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME;

  const onSwipeRight = () => {
    handleBackButtonClick({ isLoggedIn, navigation, selectOrderAction });
  };

  const { onTouchStart, onTouchEnd } = handleSwipe(onSwipeRight, 15);

  const {
    caseId,
    errorLabel,
    isRefundSuccessful,
    isCancelSuccessful,
    isShipmentDelayedSuccessful,
  } = getCaseIDErrorLabelAndAmount({
    itemLevelReturns,
    cancelStatus: status,
    action,
    shipmentDelayedStatusData,
  });

  const refundData = getRefundDataInfo({
    isPaidAndShippingCase,
    shipmentDelayedStatusData,
    itemLevelReturns,
  });

  const { totalBeforeTax, tax, itemsCount, refundTotal } = getCalculatedRefund(
    selectedOrderItems,
    orderItems,
    refundData,
    action,
    getOrderSummary
  );

  const emailToDisplay = getEmailToDisplay(isLoggedIn, loggedInUserEmail, orderDetailsData);
  const isCancelOrderPage = action === REASON_CODES.CANCEL_MODIFY;

  useEffect(() => {
    return resetCustomerHelpReducer();
  }, []);

  useEffect(() => {
    if (orderItems) {
      hideLoader();
    } else {
      showLoader();
    }
  }, [orderItems]);

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', () => {
      handleBackButtonClick({ isLoggedIn, navigation, selectOrderAction });
      return true;
    });
    triggerAnalytics(isRefundSuccessful, refundTotal, trackAnalyticsPageView);

    return () => BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
  }, []);

  return shouldRenderConfirmation(orderDetailsData) ? (
    <ScrollView onTouchStart={onTouchStart} onTouchEnd={onTouchEnd}>
      <GrayTopBorder spacingStyles="margin-top-XS" />
      <ViewWithSpacing>
        {getSuccessScenario(
          isCancelSuccessful,
          isRefundSuccessful,
          isShipmentDelayedSuccessful,
          errorLabel
        ) ? (
          <>
            <ViewWithSpacing spacingStyles="margin-right-MED margin-left-MED">
              <ReviewBasicDetails
                orderDetailsData={orderDetailsData}
                email={emailToDisplay}
                refundTotal={refundTotal}
                labels={labels}
                currencySymbol={currencySymbol}
                caseId={caseId}
                action={action}
                headingText={getSuccessFullHeadingText(action, labels, errorLabel)}
                confirmationText={getSuccessConfirmationText(
                  action,
                  labels,
                  emailToDisplay,
                  errorLabel
                )}
                imageIcon={getImageIcon(errorLabel)}
                errorLabel={errorLabel}
              />
            </ViewWithSpacing>

            <GrayTopBorder spacingStyles="margin-top-XS" />

            <ViewWithSpacing spacingStyles="margin-right-MED margin-left-MED">
              <RefundSummaryDetails
                orderDetailsData={orderDetailsData}
                orderLabels={orderLabels && orderLabels.orders}
                labels={labels}
                totalBeforeTax={totalBeforeTax}
                tax={tax}
                refundTotal={refundTotal}
                orderStatus={getOrderStatus(action, labels)}
                itemsCount={itemsCount}
                currencySymbol={currencySymbol}
                isCancelOrderPage={isCancelOrderPage}
                isPaidAndShippingCase={isPaidAndShippingCase}
                shippingTotal={shippingTotal}
              />
            </ViewWithSpacing>
            <GrayTopBorder spacingStyles="margin-top-XS" />
            <ViewWithSpacing spacingStyles="margin-right-MED margin-left-MED">
              {renderPaymentView(orderDetailsData, orderLabels, labels, action)}
            </ViewWithSpacing>
            <GrayTopBorder spacingStyles="margin-top-XS" />
            {getReviewItemListComponent(
              orderItems,
              selectedOrderItems,
              action,
              currencySymbol,
              labels,
              orderLabels,
              navigation
            )}
          </>
        ) : (
          renderNonRefundCompleteScreen({
            itemLevelReturns,
            labels,
            caseId,
            errorLabel,
            refundTotal,
            currencySymbol,
            status,
            action,
            shipmentDelayedStatusData,
          })
        )}
        <BottomContainer spacingStyles="padding-bottom-XXXL">
          {!isCancelSuccessful && !isRefundSuccessful && (
            <CustomerServiceHelp
              labels={labels}
              navigation={navigation}
              helpMessage={getLabelValue(labels, 'lbl_resend_with_free_shipping_help')}
              selectedReasonCode={selectedReasonCode}
              orderDetailsData={orderDetailsData}
              selectedOrderItems={selectedOrderItems}
              isLiveChatEnabled={isMobileLiveChatEnabled}
              isUserLoggedIn={isLoggedIn}
              trackAnalyticsClick={trackAnalyticsClick}
            />
          )}
          <ViewWithSpacing spacingStyles="padding-top-LRG padding-right-XXL padding-left-XXL">
            <Button
              fontSize="fs14"
              fontWeight="extrabold"
              buttonVariation="variable-width"
              fill="WHITE"
              onPress={() => selectOrderAction({ navigation })}
              text={getLabelValue(orderLabels && orderLabels.orders, 'lbl_help_another_order')}
            />
          </ViewWithSpacing>
        </BottomContainer>
      </ViewWithSpacing>
    </ScrollView>
  ) : null;
};

Confirmation.propTypes = {
  orderDetailsData: PropTypes.shape({}),
  orderLabels: PropTypes.shape({}).isRequired,
  labels: PropTypes.shape({}),
  selectedOrderItems: PropTypes.shape({}),
  itemLevelReturns: PropTypes.shape({}),
  loggedInUserEmail: PropTypes.string,
  isLoggedIn: PropTypes.bool,
  cancelStatusData: PropTypes.shape({}),
  selectedReasonCode: PropTypes.shape({}),
  navigation: PropTypes.shape({}).isRequired,
  trackAnalyticsPageView: PropTypes.func,
  shipmentDelayedStatusData: PropTypes.shape({}),
  selectOrderAction: PropTypes.func,
  showLoader: PropTypes.func,
  hideLoader: PropTypes.func,
  getOrderSummary: PropTypes.shape({}),
  resetCustomerHelpReducer: PropTypes.func,
  trackAnalyticsClick: PropTypes.func.isRequired,
  isMobileLiveChatEnabled: PropTypes.bool,
};

Confirmation.defaultProps = {
  orderDetailsData: {},
  labels: {},
  selectedOrderItems: {},
  selectOrderAction: () => {},
  itemLevelReturns: {},
  loggedInUserEmail: '',
  isLoggedIn: false,
  isMobileLiveChatEnabled: false,
  cancelStatusData: {},
  selectedReasonCode: {},
  trackAnalyticsPageView: () => {},
  shipmentDelayedStatusData: {},
  showLoader: () => {},
  hideLoader: () => {},
  getOrderSummary: {},
  resetCustomerHelpReducer: () => {},
};

const checkOrderNumber = (prevProps, nextProps) => {
  const { prevOrderNumber } = prevProps.orderDetailsData || {};
  const { nextOrderNumber } = nextProps.orderDetailsData || {};

  if (prevOrderNumber === nextOrderNumber) {
    return true;
  }

  return false;
};

export default React.memo(Confirmation, checkOrderNumber);
