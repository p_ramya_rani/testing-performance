// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  .reason-code-rectangle {
    max-width: 330px;
    height: 59px;
    border-radius: 8px;
    display: flex;
    align-items: center;
    justify-content: flex-start;
    border: solid 1px ${props => props.theme.colorPalette.gray[600]};
    cursor: pointer;
    .textColor {
      color: ${props => props.theme.colors.PRIMARY.DARK};
    }
    @media ${props => props.theme.mediaQuery.smallMax} {
      max-width: 100%;
    }
  }

  .selected-reason {
    margin-top: ${props => props.theme.spacing.ELEM_SPACING.MED};
  }
`;

export default styles;
