// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { EmailConfirmationFormVanilla } from '../EmailConfirmationForm.view';

describe('MergeAccount EmailConfirmation Form component', () => {
  const props = {
    labels: {},
    handleSubmit: jest.fn(),
    className: 'testclass',
    emailConfirmationError: '',
    resetEmailConfirmation: jest.fn(),
  };

  it('Should renders correctly', () => {
    const component = shallow(<EmailConfirmationFormVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('Should renders correctly with error', () => {
    props.emailConfirmationError = 'test error';
    const component = shallow(<EmailConfirmationFormVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('Should fire onSubmit callback when form is submitted', () => {
    const wrapper = shallow(<EmailConfirmationFormVanilla {...props} />);
    const form = wrapper.find('form');
    form.find('#emailAddress').simulate('change', { target: { value: 'test@test.com' } });
    form.simulate('submit');
    expect(props.handleSubmit).toHaveBeenCalled();
  });

  it('Should display input field error on the submit', () => {
    const wrapper = shallow(<EmailConfirmationFormVanilla {...props} />);
    const form = wrapper.find('form');
    form.find('#emailAddress').simulate('change', { target: { value: '' } });
    form.simulate('submit');
    setTimeout(() => {
      expect(wrapper.find('[role="alert"]')).toHaveLength(1);
    }, 500);
  });
});
