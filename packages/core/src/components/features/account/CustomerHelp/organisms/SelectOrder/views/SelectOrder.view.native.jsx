// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import createThemeColorPalette from '@tcp/core/styles/themes/createThemeColorPalette';
import RecentOrders from '../organisms/RecentOrders';
import SeeMoreLink from '../styles/SelectOrder.style.native';
import { Anchor } from '../../../../../../common/atoms';

const colorPallete = createThemeColorPalette();

const SelectOrder = ({
  ordersListItems,
  orderLabels,
  labels,
  cshForMonthsToDisplay,
  clearSelectedOrderItems,
  clearSelectedReasonCode,
  clearSelectedResolution,
  navigation,
  isFetching,
  setWrongItemData,
  isLoadMoreActive,
  setCurrentPage,
  currentPageNumber,
  setFilteredList,
}) => {
  const [seeMore, setSeeMore] = useState(false);
  return (
    <>
      <RecentOrders
        labels={labels}
        orderLabels={orderLabels}
        ordersListItems={ordersListItems}
        cshForMonthsToDisplay={cshForMonthsToDisplay}
        clearSelectedOrderItems={clearSelectedOrderItems}
        clearSelectedReasonCode={clearSelectedReasonCode}
        clearSelectedResolution={clearSelectedResolution}
        navigation={navigation}
        isFetching={isFetching}
        setWrongItemData={setWrongItemData}
        setFilteredList={setFilteredList}
        seeMore={seeMore}
        currentPageNumber={currentPageNumber}
      />
      {isLoadMoreActive && !isFetching && (
        <SeeMoreLink>
          <Anchor
            onPress={(e) => {
              setSeeMore(true);
              setCurrentPage(currentPageNumber + 1);
              e.preventDefault();
            }}
            colorName={colorPallete.blue[800]}
            text={getLabelValue(orderLabels, 'lbl_see_more', 'orders')}
          />
        </SeeMoreLink>
      )}
    </>
  );
};

SelectOrder.propTypes = {
  ordersListItems: PropTypes.shape([]).isRequired,
  orderLabels: PropTypes.shape({}).isRequired,
  labels: PropTypes.shape({}).isRequired,
  cshForMonthsToDisplay: PropTypes.number,
  navigation: PropTypes.shape({}).isRequired,
  isFetching: PropTypes.bool,
  clearSelectedReasonCode: PropTypes.func,
  clearSelectedOrderItems: PropTypes.func,
  clearSelectedResolution: PropTypes.func,
  setWrongItemData: PropTypes.func,
  isLoadMoreActive: PropTypes.bool,
  currentPageNumber: PropTypes.number,
  setCurrentPage: PropTypes.func,
  setFilteredList: PropTypes.func,
};

SelectOrder.defaultProps = {
  cshForMonthsToDisplay: 3,
  isFetching: false,
  clearSelectedOrderItems: () => {},
  clearSelectedReasonCode: () => {},
  clearSelectedResolution: () => {},
  setWrongItemData: () => {},
  isLoadMoreActive: false,
  setCurrentPage: () => {},
  currentPageNumber: 0,
  setFilteredList: () => {},
};

export default SelectOrder;
