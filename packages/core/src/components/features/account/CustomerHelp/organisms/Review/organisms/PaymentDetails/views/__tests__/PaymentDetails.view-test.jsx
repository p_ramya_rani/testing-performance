// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { PaymentDetailsVanilla } from '../PaymentDetails.view';

describe('Payment Details component', () => {
  it('should renders correctly', () => {
    const props = {
      labels: {},
      className: '',
    };
    const component = shallow(<PaymentDetailsVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
