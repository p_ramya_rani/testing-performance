import React from 'react';
import { shallow } from 'enzyme';
import { MergeAccountsVanilla } from '../MergeAccounts.view';

describe('MergeAccounts view', () => {
  it('should render correctly', () => {
    const props = {
      isLoggedIn: true,
    };
    const tree = shallow(<MergeAccountsVanilla {...props} />);
    expect(tree).toMatchSnapshot();
  });
  it('show otp', () => {
    const component = shallow(<MergeAccountsVanilla showOtp />);
    expect(component).toMatchSnapshot();
  });
  it('should match for logged-in users', () => {
    const component = shallow(<MergeAccountsVanilla isLoggedIn />);
    expect(component).toMatchSnapshot();
  });
});
