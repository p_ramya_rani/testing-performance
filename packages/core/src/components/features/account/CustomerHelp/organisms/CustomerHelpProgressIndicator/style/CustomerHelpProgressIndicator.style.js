// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

export const ProgressBarContainer = styled.View`
  margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.SM};
`;

export const StyledDisableLabels = styled.Text`
  font-family: ${props => props.theme.typography.fonts.secondary};
  color: ${props => props.theme.colors.TEXT.DARK};
  position: absolute;
  top: ${props => props.theme.spacing.ELEM_SPACING.XXL};
  left: -${props => props.theme.spacing.ELEM_SPACING.SM};
  width: 100px;
`;
