// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { AccountAndCouponsVanilla } from '../views/AccountAndCoupons.view';

describe('AccountAndCoupons', () => {
  const props = {
    labels: {},
    orderLabels: {},
    reasonCodesAccountsAndCoupons: [
      {
        id: '/1',
        label: "I'm having issues with my coupon/promo code",
        external: 0,
        title: '',
        action: 'COUPON_ISSUE',
        target: '_modal',
        __typename: 'Button',
      },
      {
        id: '/2',
        label: "I'd like to merge multiple accounts",
        external: 0,
        title: '',
        action: 'MERGE_MYPLACE_ACCOUNTS',
        target: '_modal',
        __typename: 'Button',
      },
      {
        id: '/3',
        label: 'I need help with something else',
        external: 0,
        title: '',
        action: 'SOMETHING_ELSE',
        target: '_modal',
        __typename: 'Button',
      },
    ],
  };

  it('should render correctly', () => {
    const component = shallow(<AccountAndCouponsVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('should render after selecting reason code ', () => {
    const component = shallow(<AccountAndCouponsVanilla selectedReasonCode {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('should render coupon self help ', () => {
    const component = shallow(<AccountAndCouponsVanilla isCouponSelfHelpEnabled {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('should render merge account ', () => {
    const component = shallow(<AccountAndCouponsVanilla isMergeMyPlaceAccount {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('should render US only reason code ', () => {
    const component = shallow(<AccountAndCouponsVanilla isUsOnly {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('should call setSelectedAccountsReasonCode function', () => {
    const extendedProps = {
      selectedAccountsReasonCode: 'sample',
      setSelectedAccountsReasonCode: jest.fn(),
    };
    const updatedProps = { ...props, ...extendedProps };
    const component = shallow(<AccountAndCouponsVanilla {...updatedProps} />);
    component.find("[className='edit-option']").simulate('click');
    expect(extendedProps.setSelectedAccountsReasonCode).toBeCalled();
  });
});
