// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { Row, Col, BodyCopy, Image, Anchor } from '@tcp/core/src/components/common/atoms';
import PropTypes from 'prop-types';
import {
  getIconPath,
  getAPIConfig,
  configureInternalNavigationFromCMSUrl,
} from '@tcp/core/src/utils';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import LiveChat from '../../../../../molecules/LiveChat';
import styles from '../styles/CustomerServiceHelp.style';

const fallbackImg = getIconPath('essential-style-fallback');

const renderReturnPolicyLink = (labels) => {
  return (
    <>
      &nbsp;
      <Anchor
        to="/help-center/returns-exchanges#returnsandexchanges"
        target="_blank"
        anchorVariation="primary"
        fontFamily="secondary"
        fontWeight="normal"
        fontSizeVariation="large"
        className="return-policy"
        dataLocator="navigationToReturnPolicy"
      >
        {getLabelValue(labels, 'lbl_returns_return_policy')}
      </Anchor>
      .
    </>
  );
};

const CustomerServiceHelp = ({
  labels,
  helpMessage,
  isNonOnlineOrOlderOrder,
  isSomethingElse,
  selectedReasonCode,
  orderDetailsData,
  selectedOrderItems,
  isLiveChatEnabled,
  isInternationalShipping,
  trackAnalyticsClick,
  className,
  couponData,
  selectedReturnMethod,
  isInitiateReturnFailure,
  showReturnPolicy,
  justifyCenter,
}) => {
  const emailIcon = getIconPath('email-help');
  const callIcon = getIconPath('call-help');
  const infoIcon = getIconPath('info-icon');
  let callUsLabel = getLabelValue(labels, 'lbl_help_call_us_number');

  const { country } = getAPIConfig();
  if (country === 'CA') {
    callUsLabel = getLabelValue(labels, 'lbl_csh_help_call_other_number');
  }
  if (isInternationalShipping) {
    callUsLabel = getLabelValue(labels, 'lbl_csh_help_call_international_number');
  }
  const href = getLabelValue(labels, 'lbl_email_us_href');

  return (
    <div className={`${className}`}>
      {helpMessage && (
        <Row fullBleed className="elem-mt-MED with-info-message" id="step-7">
          <Col
            colSize={{ small: 12 }}
            className={`${isNonOnlineOrOlderOrder ? 'disable-message-with-icon' : ''}`}
          >
            {isNonOnlineOrOlderOrder && (
              <Image
                width="26px"
                height="26px"
                src={infoIcon || fallbackImg}
                alt=""
                className="elem-pr-XS"
              />
            )}
            <BodyCopy className="elem-mb-MED" component="p" fontFamily="secondary" fontSize="fs16">
              {helpMessage}
              {showReturnPolicy && renderReturnPolicyLink(labels)}
            </BodyCopy>
          </Col>
        </Row>
      )}

      <Row
        fullBleed
        className={`${!isSomethingElse ? 'elem-mt-LRG' : ''} ${
          justifyCenter ? 'justify-center' : ''
        }`}
      >
        <Col
          colSize={{ small: 6, medium: 4, large: 4 }}
          className="reason-code-rectangle align-left elem-mb-LRG contact-method"
        >
          <Image
            width="18px"
            height="18px"
            src={callIcon || fallbackImg}
            alt=""
            className="elem-pl-MED"
          />
          <BodyCopy
            component="p"
            fontWeight="semibold"
            fontSize="fs14"
            fontFamily="secondary"
            color="text.dark"
            className="elem-pl-XS elem-pr-XS"
          >
            {getLabelValue(labels, 'lbl_help_call_us')}
            <BodyCopy
              component="a"
              href={`tel:+${callUsLabel}`}
              fontWeight="semibold"
              fontSize="fs14"
              fontFamily="secondary"
              color="text.dark"
              className="elem-pr-XS blue-link"
            >
              {callUsLabel}
            </BodyCopy>
          </BodyCopy>
        </Col>
        <Col
          colSize={{ small: 6, medium: 4, large: 4 }}
          className="reason-code-rectangle align-left elem-mb-LRG email-help contact-method"
        >
          <div className="help-message-with-icon">
            <Image
              width="18px"
              height="18px"
              src={emailIcon || fallbackImg}
              alt=""
              className="elem-pl-MED"
            />
            <Anchor
              component="a"
              to={configureInternalNavigationFromCMSUrl(href)}
              asPath={href}
              target="_blank"
              fontWeight="semibold"
              fontSize="fs14"
              fontFamily="secondary"
              color="text.dark"
              className="elem-pl-XS elem-pr-XS no-decoration"
            >
              {getLabelValue(labels, 'lbl_help_email_us')}
            </Anchor>
          </div>
        </Col>
        {isLiveChatEnabled && (
          <Col
            colSize={{ small: 6, medium: 4, large: 4 }}
            className="reason-code-rectangle align-left elem-mb-LRG email-help contact-method"
          >
            <LiveChat
              selectedReasonCode={selectedReasonCode}
              orderDetailsData={orderDetailsData}
              selectedOrderItems={selectedOrderItems}
              labels={labels}
              trackAnalyticsClick={trackAnalyticsClick}
              couponData={couponData}
              selectedReturnMethod={selectedReturnMethod}
              errorLabel={isInitiateReturnFailure}
            />
          </Col>
        )}
      </Row>
    </div>
  );
};

CustomerServiceHelp.propTypes = {
  className: PropTypes.string,
  labels: PropTypes.shape({}).isRequired,
  helpMessage: PropTypes.string.isRequired,
  isNonOnlineOrOlderOrder: PropTypes.bool,
  isSomethingElse: PropTypes.bool,
  selectedReasonCode: PropTypes.shape({}),
  orderDetailsData: PropTypes.shape({}),
  selectedOrderItems: PropTypes.shape({}),
  isLiveChatEnabled: PropTypes.bool,
  trackAnalyticsClick: PropTypes.func.isRequired,
  couponData: PropTypes.shape({}),
  selectedReturnMethod: PropTypes.string,
  isInitiateReturnFailure: PropTypes.string,
  justifyCenter: PropTypes.bool,
};

CustomerServiceHelp.defaultProps = {
  className: '',
  isNonOnlineOrOlderOrder: false,
  isSomethingElse: false,
  selectedReasonCode: {},
  orderDetailsData: {},
  selectedOrderItems: {},
  isLiveChatEnabled: false,
  couponData: {},
  selectedReturnMethod: '',
  isInitiateReturnFailure: '',
  justifyCenter: false,
};

export default withStyles(CustomerServiceHelp, styles);
export { CustomerServiceHelp as CustomerServiceHelpVanilla };
