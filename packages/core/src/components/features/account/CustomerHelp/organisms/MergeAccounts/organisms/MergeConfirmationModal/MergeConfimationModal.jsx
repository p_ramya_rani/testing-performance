import React from 'react';
import PropTypes from 'prop-types';
import Modal from '@tcp/core/src/components/common/molecules/Modal/view/Modal.jsx';
import colors from '@tcp/core/styles/themes/TCP/colors';
import { BodyCopy, Button, Image } from '../../../../../../../common/atoms';
import { getIconPath, getLabelValue } from '../../../../../../../../utils';
import styles, { modalStyles } from './styles/MergeConfirmationModal.styles';
import withStyles from '../../../../../../../common/hoc/withStyles';
import { convertHtml } from '../../../../../../CnC/LoyaltyBanner/util/utility';
import fonts from '../../../../../../../../../styles/themes/TCP/fonts';

const getCopyWithEmail = (victimEmail, labels) => {
  const textToBold = getLabelValue(labels, 'lbl_merge_modal_warning_text_txt_to_bold');
  let labelTxt = getLabelValue(labels, 'lbl_merge_modal_warning_text');
  labelTxt = labelTxt.replace(
    textToBold,
    `<span style="font-weight:${fonts.fontWeight.extrabold};color:${colors.TEXT.RED};">${textToBold}</span>`
  );
  labelTxt += `<span style="color:${colors.TEXT.BLUE};">${victimEmail}</span>${getLabelValue(
    labels,
    'lbl_merge_modal_warning_text_part_two'
  )}`;
  return labelTxt;
};

function MergeConfimationModal({
  className,
  isModalOpen,
  closeModal,
  labels,
  setIsMergeConfirmed,
  trackAnalyticsPageView,
}) {
  const closeAndConfirm = (isConfirmed) => {
    setIsMergeConfirmed(isConfirmed);
    closeModal();
  };
  return (
    <Modal
      fixedWidth
      isOpen={isModalOpen}
      onRequestClose={() => {
        closeAndConfirm(false);
      }}
      overlayClassName="TCPModal__Overlay"
      className={`${className} TCPModal__Content`}
      maxWidth="700px"
      inheritedStyles={modalStyles}
      shouldCloseOnOverlayClick={false}
    >
      <div className="modal-container">
        <BodyCopy className="elem-mb-MED">
          <Image
            alt="triangle-warning"
            src={getIconPath('triangle-warning')}
            height="49px"
            width="56px"
          />
        </BodyCopy>
        <BodyCopy
          fontSize={['fs18', 'fs14', 'fs24']}
          fontWeight={['bold', 'extrabold', 'extrabold']}
          fontFamily="secondary"
          className={`elem-mb-XXL modal-heading `}
        >
          {getLabelValue(labels, 'lbl_merge_modal_heading')}
        </BodyCopy>

        <BodyCopy
          fontSize={['fs14', 'fs14', 'fs16']}
          fontWeight={['semibold', 'semibold', 'semibold']}
          fontFamily="secondary"
          className="elem-mb-XXL modal-text-middle"
        >
          {convertHtml(getCopyWithEmail(isModalOpen, labels))}
        </BodyCopy>

        <BodyCopy component="div" textAlign="center" className="cta-container">
          <Button
            fill="BLUE"
            type="button"
            buttonVariation="fixed-width"
            fullWidth
            onClick={() => {
              closeAndConfirm(true);

              trackAnalyticsPageView(
                { metric159: '1', customEvents: ['event159'] },
                { currentScreen: 'mergeAccount_page' }
              );
            }}
            className="elem-mr-LRG cta-btn"
          >
            {getLabelValue(labels, 'lbl_merge_cta_merge')}
          </Button>
          <Button
            fill="WHITE"
            type="button"
            buttonVariation="fixed-width"
            fullWidth
            onClick={() => {
              closeAndConfirm(false);
              trackAnalyticsPageView(
                { metric160: '1', customEvents: ['event160'] },
                { currentScreen: 'mergeAccount_page' }
              );
            }}
            className="cta-btn"
          >
            {getLabelValue(labels, 'lbl_merge_cta_cancel')}
          </Button>
        </BodyCopy>
      </div>
    </Modal>
  );
}

MergeConfimationModal.propTypes = {
  className: PropTypes.string.isRequired,
  isModalOpen: PropTypes.bool.isRequired,
  closeModal: PropTypes.func.isRequired,
  trackAnalyticsPageView: PropTypes.func.isRequired,
  setIsMergeConfirmed: PropTypes.func,
  labels: PropTypes.shape({}).isRequired,
};

MergeConfimationModal.defaultProps = {
  setIsMergeConfirmed: () => {},
};

export default withStyles(MergeConfimationModal, styles);
export { MergeConfimationModal as MergeConfimationModalVanilla };
