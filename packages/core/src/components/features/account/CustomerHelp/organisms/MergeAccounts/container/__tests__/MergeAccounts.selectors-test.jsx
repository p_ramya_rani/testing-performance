// 9fbef606107a605d69c0edbcd8029e5d
import { fromJS } from 'immutable';
import constants from '../../MergeAccounts.constants';
import {
  getMergeAccountSuccess,
  getMergeAccountFailure,
  getAccountStatus,
  getNumberOfRetries,
  getMergeStatus,
  getShowRecaptcha,
  getFirstTImeOtp,
  getShowConfirmationModal,
  getIsMergeConfirmed,
} from '../MergeAccounts.selectors';

describe('Merge account account confirmation selector', () => {
  const mergeAccountsState = fromJS({
    error: null,
    mergeAccountSuccess: {},
    mergeAccountButtonLoader: false,
    numberOfRetries: 3,
    accountStatus: constants.ACCOUNT_STATUS[0],
    firstTimeOtp: true,
    showConfirmationModal: true,
    isMergeConfirmed: true,
  });

  const state = {
    /* reducer-key:state */
    MergeAccount: mergeAccountsState,
    Labels: {
      global: {
        helpCenter: {
          lbl_merge_recaptcha_threshold: '3',
        },
      },
    },
  };

  it('getMergeAccountFailure should return error state', () => {
    expect(getMergeAccountFailure(state)).toEqual(mergeAccountsState.get('error'));
  });

  it('getMergeAccountSuccess should return success state', () => {
    expect(getMergeAccountSuccess(state)).toEqual(mergeAccountsState.get('mergeAccountSuccess'));
  });

  it('getAccountStatus should return acountStatus state', () => {
    expect(getAccountStatus(state)).toEqual(mergeAccountsState.get('accountStatus'));
  });

  it('getNumberOfRetries should return numberOfRetries state', () => {
    expect(getNumberOfRetries(state)).toEqual(mergeAccountsState.get('numberOfRetries'));
  });

  it('getMergeStatus should return mergeStatus state', () => {
    expect(getMergeStatus(state)).toEqual(constants.MERGE_STATUS.MERGE_FAILED);
  });

  it('getShowRecaptcha should return showRecaptcha state', () => {
    expect(getShowRecaptcha(state)).toEqual(true);
  });

  it('getFirstTImeOtp  should return firstTimeOtp state', () => {
    expect(getFirstTImeOtp(state)).toEqual(true);
  });

  it('getShowConfirmationModal   should return showConfirmationModal state', () => {
    expect(getShowConfirmationModal(state)).toEqual(true);
  });

  it('getIsMergeConfirmed   should return isMergeConfirmed state', () => {
    expect(getIsMergeConfirmed(state)).toEqual(true);
  });
});
