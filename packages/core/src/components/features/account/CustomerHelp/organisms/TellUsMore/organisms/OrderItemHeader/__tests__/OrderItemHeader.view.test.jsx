// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { OrderItemHeaderVanilla } from '../views/OrderItemHeader.view';

describe('OrderItemHeader Component', () => {
  const props = {
    shippedDate: 'May, 2019',
    trackingNumber: '1234567',
    trackingUrl: '/test',
    shipmentStatus: 'delivered',
    actualDeliveryDate: '2021-10-12',
  };
  it('should render OrderItemHeader Component', () => {
    const component = shallow(<OrderItemHeaderVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
