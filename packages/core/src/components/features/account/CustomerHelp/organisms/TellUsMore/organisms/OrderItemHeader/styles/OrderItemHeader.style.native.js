// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';

export const OrderHeaderContainer = styled.View`
  margin: ${(props) => props.theme.spacing.APP_LAYOUT_SPACING.XXS};
`;

export const ItemHeader = styled.View`
  flex-direction: row;
  flex-wrap: wrap;
`;
export const AlignItemHeader = styled.View`
  margin-left: 30px;
  flex-direction: row;
  flex-wrap: wrap;
`;

export const OrderStatusLabel = styled.View`
  margin-left: ${(props) => props.theme.spacing.APP_LAYOUT_SPACING.XXS};
  margin-right: ${(props) => props.theme.spacing.APP_LAYOUT_SPACING.XXS};
`;

export const OrderStatusLabelText = styled.Text`
  font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy4}px;
  font-family: ${(props) => props.theme.typography.fonts.secondary};
  font-weight: ${(props) => props.theme.typography.fontWeights.extrabold};
  color: ${(props) => (props.color === 'g600' ? props.theme.colorPalette.green[600] : props.color)};
`;
