// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { CheckCouponContainer } from '../container/CheckCoupon.container';

describe('Customer Help container', () => {
  it('should render Customer Help Container', () => {
    const wrapper = shallow(<CheckCouponContainer />);
    expect(wrapper.is(React.Fragment)).toBeTruthy();
  });
});
