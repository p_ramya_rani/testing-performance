// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import { getOrdersListState } from '@tcp/core/src/components/features/account/Orders/container/Orders.selectors';
import CustomerHelpProgress from '../views';
import { getCustomerHelpLabels } from '../../../container/CustomerHelp.selectors';
import { moveToStage } from '../../../util/utility';
import { getAvailableStages } from '../../../CustomerHelp.constants';

export class CustomerHelpProgressIndicator extends React.PureComponent {
  constructor() {
    super();
    this.availableStages = getAvailableStages();
  }

  render() {
    const {
      moveToCustomerHelpStage,
      labels,
      ordersListItems,
      activeStage,
      navigation,
    } = this.props;

    return (
      <CustomerHelpProgress
        activeStage={activeStage}
        availableStages={this.availableStages}
        moveToCustomerHelpStage={moveToCustomerHelpStage}
        labels={labels}
        ordersListItems={ordersListItems}
        navigation={navigation}
      />
    );
  }
}

const mapStateToProps = state => {
  return {
    labels: getCustomerHelpLabels(state),
    ...state,
    ordersListItems: getOrdersListState(state),
  };
};

export const mapDispatchToProps = () => {
  return {
    moveToCustomerHelpStage: (stage, app, navigation) => moveToStage(stage, app, navigation),
  };
};

CustomerHelpProgressIndicator.propTypes = {
  router: PropTypes.shape({}).isRequired,
  moveToCustomerHelpStage: PropTypes.func.isRequired,
  labels: PropTypes.shape({}).isRequired,
  ordersListItems: PropTypes.shape([]).isRequired,
  activeStage: PropTypes.string.isRequired,
  navigation: PropTypes.shape({}).isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomerHelpProgressIndicator);
