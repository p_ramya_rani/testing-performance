// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  .container-skeleton {
    width: 250px;
    position: relative;
    display: flex;
    padding-bottom: ${props => props.theme.spacing.ELEM_SPACING.XS};
  }
`;

export default styles;
