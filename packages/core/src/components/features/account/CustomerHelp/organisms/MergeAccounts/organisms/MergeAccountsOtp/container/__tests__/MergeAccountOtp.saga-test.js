// 9fbef606107a605d69c0edbcd8029e5d
import { call, takeLatest } from 'redux-saga/effects';
import { MERGE_ACCOUNT_OTP_CONSTANTS } from '../../MergeAccountsOtp.constants';
import { getOtpDetails, MergeAccountOtpSaga, checkOtpDetails } from '../MergeAccountOtp.saga';
import {
  getOtpDetails as getOtpDetailsAbstractor,
  checkOtpDetails as validateOtpDetailsAbstractor,
} from '../../../../../../../../../../services/abstractors/customerHelp/customerHelp';

window.grecaptcha = {};
window.grecaptcha.reset = jest.fn();

describe('MergeAccount OTP saga', () => {
  it('should return correct takeLatest effect', () => {
    const generator = MergeAccountOtpSaga();
    const takeLatestDescriptor = generator.next().value;
    expect(takeLatestDescriptor).toEqual(
      takeLatest(MERGE_ACCOUNT_OTP_CONSTANTS.MERGE_ACCOUNT_GET_OTP, getOtpDetails)
    );
    expect(generator.next().value).toEqual(
      takeLatest(MERGE_ACCOUNT_OTP_CONSTANTS.MERGE_ACCOUNT_VALIDATE_OTP, checkOtpDetails)
    );
  });
});

describe('getOtpDetails saga', () => {
  const payloadData = { payload: {} };
  const getOtpDetailsGen = getOtpDetails(payloadData);
  it('should dispatch', () => {
    const response = {};
    expect(getOtpDetailsGen.next(response).value).toEqual(
      call(getOtpDetailsAbstractor, {
        payload: {},
      })
    );
  });
});

describe('check otp details', () => {
  const payloadData = {
    payload: {
      uniqueKey: '123456',
      otp: '1234567',
    },
  };
  const response = {
    otp: '1234567',
    uniqueKey: '123456',
  };
  it('should dispatch', () => {
    const checkOtpDetailsGen = checkOtpDetails(payloadData);
    checkOtpDetailsGen.next();
    checkOtpDetailsGen.next();
    checkOtpDetailsGen.next();
    expect(checkOtpDetailsGen.next(response).value).toEqual(
      call(validateOtpDetailsAbstractor, response)
    );
  });
});
