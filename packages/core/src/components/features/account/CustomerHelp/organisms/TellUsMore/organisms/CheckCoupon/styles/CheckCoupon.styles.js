// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  &.container {
    @media ${(props) => props.theme.mediaQuery.large} {
      padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
    }
  }

  .container-check-coupon {
    position: relative;
  }

  .title-underline {
    border-bottom: 2px solid ${(props) => props.theme.colorPalette.blue[500]};
    width: 40px;
    margin-bottom: 2px;
  }
  .heading {
    @media ${(props) => props.theme.mediaQuery.medium} {
      flex: 50%;
    }
  }
  .description {
    @media ${(props) => props.theme.mediaQuery.small} {
      margin-bottom: 0px;
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    }
  }

  .check-coupon-button {
    display: flex;
    margin-left: auto;
    margin-right: auto;
    @media ${(props) => props.theme.mediaQuery.small} {
      margin-bottom: ${(props) =>
        props.couponError.length > 0 || props.coupon?.isExpired
          ? props.theme.spacing.ELEM_SPACING.REG
          : props.theme.spacing.ELEM_SPACING.LRG};
    }
    @media ${(props) => props.theme.mediaQuery.medium} {
      margin-bottom: ${(props) =>
        props.coupon?.error || props.couponError.length > 0 || props.coupon?.isExpired
          ? props.theme.spacing.ELEM_SPACING.REG
          : props.theme.spacing.ELEM_SPACING.LRG};
      margin-left: 0px;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-bottom: ${(props) =>
        props.coupon?.error || props.couponError.length > 0 || props.coupon?.isExpired
          ? props.theme.spacing.ELEM_SPACING.SM
          : props.theme.spacing.ELEM_SPACING.XL};
      margin-left: 0px;
    }
  }

  .top-section {
    display: flex;
    justify-content: space-between;
    @media ${(props) => props.theme.mediaQuery.medium} {
      margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS_6};
    }
  }
  .try-another-coupon {
    display: block;
    text-decoration: underline;
    color: ${(props) => props.theme.colors.PRIMARY.BLUE};
    align-self: start;
    padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXXS};
  }
  .middle-section {
    @media ${(props) => props.theme.mediaQuery.medium} {
      width: 50%;
    }
  }
  .input-field {
    @media ${(props) => props.theme.mediaQuery.medium} {
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
      width: 98%;
    }
  }

  .overlay {
    display: flex;
    flex-direction: column;
    position: absolute;
    width: 100%;
    height: 100%;
    top: -${(props) => props.theme.spacing.ELEM_SPACING.XS};
    left: -${(props) => props.theme.spacing.ELEM_SPACING.MED};
    padding: 0px ${(props) => props.theme.spacing.ELEM_SPACING.MED}
      ${(props) => props.theme.spacing.ELEM_SPACING.XL}
      ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    right: 0;
    bottom: 0;
    align-items: center;
    color: ${(props) => props.theme.colorPalette.white};
    background-color: ${(props) => props.theme.colors.REWARDS_OVERLAY};
    z-index: 5;
    justify-content: center;
    @media ${(props) => props.theme.mediaQuery.medium} {
      width: 54%;
      right: 0;
      padding: 0px 0px ${(props) => props.theme.spacing.ELEM_SPACING.XL} 0px;
    }
  }
  .overlay__content {
    flex: none;
    width: 100%;
    text-align: center;
  }

  .top-content {
    display: flex;
    flex-direction: column;
    align-items: center;
  }

  .bag_icon {
    width: ${(props) => props.theme.spacing.CREDIT_CARD_ICON_WIDTH};
  }

  .overlay-close-btn {
    position: absolute;
    width: ${(props) => props.theme.spacing.ELEM_SPACING.MED_1};
    top: ${(props) => props.theme.spacing.ELEM_SPACING.REG};
    right: ${(props) => props.theme.spacing.ELEM_SPACING.REG};
    z-index: 5;
    cursor: pointer;

    @media ${(props) => props.theme.mediaQuery.medium} {
      top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
      right: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    }
  }
`;
export default styles;
