/* eslint-disable max-lines */
/* eslint-disable sonarjs/cognitive-complexity */
/* eslint-disable complexity */
// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import format from 'date-fns/format';
import isValid from 'date-fns/isValid';
import parseISO from 'date-fns/parseISO';
import toLower from 'lodash/toLower';
import {
  getLabelValue,
  getFormatedOrderDate,
  formatYmd,
  convertToISOString,
  getIconPath,
} from '@tcp/core/src/utils/utils';
import { smoothScrolling } from '@tcp/core/src/utils';
import { BodyCopy, Button, Row, Col, Image } from '@tcp/core/src/components/common/atoms';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import {
  REASON_CODES,
  missingOrWrongItemsResolution,
  cancelNModifyResolution,
  delayedResolution,
  reasonWrongItemsResolution,
  shipmentResolution,
  ORDER_ITEM_STATUS,
  connectWithSpecialistResolution,
  SHIPMENT_METHODS,
  CUSTOMER_HELP_PAGE_TEMPLATE,
  CUSTOMER_HELP_GA,
  CUSTOMER_HELP_ROUTES,
} from '@tcp/core/src/components/features/account/CustomerHelp/CustomerHelp.constants';
import { isOldItem } from '@tcp/core/src/components/features/account/CustomerHelp/util/utility';
import { decode } from '@tcp/core/src/utils/base64.utils';

import ReactTooltip from '@tcp/core/src/components/common/atoms/ReactToolTip';
import styles from '../styles/TellUsMore.style';
import { getOrderTileItems } from '../../../util/utility';
import OrderTile from '../../../molecules/OrderTile';
import ReasonCode from '../organisms/ReasonCode';
import Resolution from '../organisms/Resolution';
import OrderItems from '../organisms/OrderItems';
import ReasonCodeSkeleton from '../skeleton';
import OrderCards from '../organisms/OrderCards';
import CustomerServiceHelp from '../organisms/CustomerServiceHelp';
import TellUsMoreSkeleton from '../skeleton/TellUsMoreSkeleton.view';
import InitiateReturn from '../organisms/ReturnItem/organisms/InitiateReturn';
import { convertHtml } from '../../../../../CnC/LoyaltyBanner/util/utility';
import MyOrderArrive from '../organisms/MyOrderArrive';
import { routerReplace } from '../../../../../../../utils/utils.web';

const getRefundstatus = (refundData) => {
  return (
    refundData && refundData.refundEligible && refundData.refundEligible.toLowerCase() === 'true'
  );
};

const getOrderTileData = ({ ordersListItems, orderID, cshForMonthsToDisplay }) => {
  const itemGroup =
    ordersListItems &&
    orderID &&
    ordersListItems.orders &&
    ordersListItems.orders.filter((order) => order.orderNumber === orderID);
  return getOrderTileItems({
    item: itemGroup && itemGroup[0],
    cshForMonthsToDisplay,
    noStatusDisplay: true,
  });
};

export const getResolutionComponent = (
  resolution,
  refundData,
  selectedOrderItems,
  selectedShipmentCard,
  isPaidNExpediteRefundEnable
) => {
  const refundStatus = getRefundstatus(refundData);
  switch (resolution) {
    case REASON_CODES.CANCEL_MODIFY:
      return cancelNModifyResolution;
    case REASON_CODES.MISSING_ORDER: {
      if (Object.keys(selectedOrderItems).length > 0) {
        return missingOrWrongItemsResolution;
      }
      return null;
    }
    case REASON_CODES.TRACKING_NUMBER_SHOW_IN_TRANSIT_FOR_LONG_TIME: {
      if (selectedShipmentCard && refundStatus) {
        return shipmentResolution;
      }
      return null;
    }
    case REASON_CODES.PACKAGE_DELIVERED_BUT_NOT_RECEIVED: {
      if (selectedShipmentCard) {
        return shipmentResolution;
      }
      return null;
    }
    case REASON_CODES.PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME:
      return isPaidNExpediteRefundEnable ? delayedResolution : connectWithSpecialistResolution;
    case REASON_CODES.RECEIVED_WRONG_ITEM: {
      if (Object.keys(selectedOrderItems).length > 0) {
        return reasonWrongItemsResolution;
      }
      return null;
    }
    case REASON_CODES.COUPON_ISSUE:
      return null;
    case REASON_CODES.INITIATE_RETURN:
      return null;
    case REASON_CODES.WHEN_WILL_MY_ORDER_ARRIVE:
      return null;
    default:
      return missingOrWrongItemsResolution;
  }
};

const getOrderCards = (params) => {
  const {
    selectedReasonCode,
    labels,
    setSelectedShipmentCard,
    clearSelectedResolution,
    selectedShipmentCard,
    orderDetailsData,
    orderLabels,
    isDelivered,
    handleShipmentCardClick,
    setIsDelivered,
    setOrderStatusInTransit,
    setToggleCards,
    toggleCards,
    shipmentData,
    filteredItems,
    setFilteredList,
  } = params;
  const { action } = selectedReasonCode || {};
  const {
    TRACKING_NUMBER_SHOW_IN_TRANSIT_FOR_LONG_TIME,
    PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME,
    PACKAGE_DELIVERED_BUT_NOT_RECEIVED,
  } = REASON_CODES;
  const { STANDARD, GROUND } = SHIPMENT_METHODS;
  const { shippingMethod } = orderDetailsData || {};
  const shippingForm = shippingMethod && shippingMethod.toLowerCase();
  const expressDelivery = shippingForm !== STANDARD && shippingForm !== GROUND;
  if (
    action === TRACKING_NUMBER_SHOW_IN_TRANSIT_FOR_LONG_TIME ||
    (action === PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME && expressDelivery) ||
    action === PACKAGE_DELIVERED_BUT_NOT_RECEIVED
  ) {
    return (
      <div id="step-4">
        <OrderCards
          labels={labels}
          setSelectedShipmentCard={setSelectedShipmentCard}
          clearSelectedResolution={clearSelectedResolution}
          selectedShipmentCard={selectedShipmentCard}
          orderDetailsData={orderDetailsData}
          orderLabels={orderLabels}
          isDelivered={isDelivered}
          handleShipmentCardClick={handleShipmentCardClick}
          setIsDelivered={setIsDelivered}
          action={action}
          setOrderStatusInTransit={setOrderStatusInTransit}
          setToggleCards={setToggleCards}
          toggleCards={toggleCards}
          shipmentData={shipmentData}
          filteredItems={filteredItems}
          setFilteredList={setFilteredList}
        />
      </div>
    );
  }
  return null;
};

const getContentReferedLabels = (
  labels,
  getReasonsCodeContent,
  fetchResolutionModuleX,
  fetchCancelContent
) => {
  if (typeof labels === 'object' && labels.referred) {
    getReasonsCodeContent(labels.referred);
    const referredResolutionContentId = labels.referred[1] && labels.referred[1].contentId;
    if (referredResolutionContentId) {
      fetchResolutionModuleX(referredResolutionContentId);
    }
    const contentId = labels.referred[9] && labels.referred[9].contentId;
    if (contentId) {
      fetchCancelContent(contentId);
    }
  }
};

export const getTotalItemsCount = (items) => {
  let totalItemsCount = 0;
  if (Array.isArray(items)) {
    items.forEach((item) => {
      const { itemInfo = {} } = item;
      totalItemsCount += itemInfo.quantity || 0;
    });
  }
  return totalItemsCount;
};

const getSubHeaderLabel = (selectedReasonCode, orderLabels, labels) => {
  const { INITIATE_RETURN, MISSING_ORDER } = REASON_CODES;
  switch (selectedReasonCode.action) {
    case INITIATE_RETURN:
      return getLabelValue(labels, 'lbl_csh_select_the_initiatereturn', '');
    case MISSING_ORDER:
      return getLabelValue(orderLabels, 'lbl_csh_select_the_missingItems', 'orders');
    default:
      return getLabelValue(orderLabels, 'lbl_csh_select_the_incorrectItems', 'orders');
  }
};

const TellUsMore = ({
  reasonCodes,
  selectedResolution,
  selectedWrongItemResolution,
  getReasonsCodeContent,
  setSelectedReasonCode,
  selectedReasonCode,
  clearSelectedReasonCode,
  clearSelectedResolution,
  labels,
  orderLabels,
  orderDetailsData,
  router,
  getOrderDetailsAction,
  selectOrderAction,
  isLoggedIn,
  orderItems,
  setSelectedOrderItems,
  setResolution,
  selectedOrderItems,
  shipmentData,
  clearSelectedOrderItems,
  fetchResolutionModuleX,
  fetchCancelContent,
  resolutionModuleXContent,
  cancelResolutionModuleXContent,
  getRefundStatus,
  fetchTransitHelpModuleX,
  transitHelpModuleXContent,
  refundData,
  resolutionContentId,
  setWrongItemData,
  wrongItemData,
  cshForMonthsToDisplay,
  setOrderStatusInTransit,
  isOrderStatusTransit,
  setToggleCards,
  toggleCards,
  isPaidNExpediteRefundEnable,
  trackAnalyticsPageView,
  trackAnalyticsClick,
  ordersListItems,
  selectedShipmentCard,
  setSelectedShipmentCard,
  clickAnalyticsData,
  resetCustomerHelpReducer,
  filteredItems,
  setFilteredList,
  isFetchingOrderItems,
  orderLookUpAPISwitch,
  isLiveChatEnabled,
  isInternationalShipping,
  preReasonFlag,
  walletLabels,
  isCouponSelfHelpEnabled,
  setLoader,
  setActiveSection,
  setSelectedTopReasonCode,
  topLevelReasonCodes,
  returnByMailContentId,
  returnInStoreContentId,
  returnPolicyContentId,
  fetchReturnByMailModuleX,
  fetchReturnInStoreModuleX,
  fetchReturnPolicyModuleX,
  clearReturnMethod,
  selectedReturnMethod,
  setReturnMethod,
  setReturnItemDisplay,
  isReturnItemDisplay,
  cshReturnItemOlderDays,
  returnPolicyModuleXContent,
  className,
  isNewWhenWillMyOrderArrive,
  setOrderArriveSelectedReasonCode,
  clearOrderArriveSelectedReasonCode,
  selectedOrderArriveReasonCode,
  setSelectedOrderArriveDeliveredReasonCode,
  selectedOrderArriveDeliveredReasonCode,
}) => {
  const [isDelivered, setIsDelivered] = useState(false);
  const [orderDelivereyDate, setOrderDeliveredDate] = useState(false);
  const [isAppeasementAppliedOnOrder, setIsAppeasementAppliedOnOrder] = useState(false);
  const [helpstep1, setHelpstep1] = useState('');
  const { estimatedDeliveryDate } = orderDetailsData || {};
  const { orderId: orderID, emailId, reasonCode } = (router && router.query) || {};

  const totalRecordsAdd =
    ordersListItems && ordersListItems.orders && ordersListItems.orders.length;

  const emailFromPath = emailId && decode(emailId);
  const reasonCodeFromPath = reasonCode && reasonCode.toUpperCase();

  const onContinueClick = () => {
    setReturnItemDisplay(true);

    setTimeout(() => {
      smoothScrolling('initiate-return-step2');
    }, 100);
  };
  useEffect(() => {
    setLoader(false);
    setActiveSection(undefined);
  }, []);

  useEffect(() => {
    if (
      topLevelReasonCodes.length > 0 &&
      router?.query?.section === CUSTOMER_HELP_PAGE_TEMPLATE.TELL_US_MORE
    ) {
      const reasonCodeToSet =
        topLevelReasonCodes &&
        topLevelReasonCodes.filter((rcode) => {
          return rcode.action === 'EXISTING_ORDER';
        });
      setSelectedTopReasonCode(reasonCodeToSet[0]);
      if (reasonCodes && reasonCodes.length > 0 && router.query?.reasonCode) {
        const reasonCodeFromArray = reasonCodes.find((rc) => rc.action === router.query.reasonCode);
        if (reasonCodeFromArray) setSelectedReasonCode(reasonCodeFromArray);
      }
    }
  }, [topLevelReasonCodes, router?.query?.reasonCode]);

  useEffect(() => {
    const { orderNumber, orderItems: orderList, encryptedEmailAddress } = orderDetailsData || {};
    const emailAddress = encryptedEmailAddress || emailFromPath;
    if (orderID && orderID === orderNumber) {
      resetCustomerHelpReducer();
      clearSelectedResolution();
      clearSelectedOrderItems();
      setOrderStatusInTransit(false);
      setToggleCards(false);
      setSelectedShipmentCard(null);
    }
    const defaultPayload = { orderId: orderID, fromCSH: true, orderLookUpAPISwitch };
    let payload;
    if (
      (orderID && orderID !== orderNumber) ||
      !orderList ||
      (Array.isArray(orderList) && !orderList.length)
    ) {
      if (isLoggedIn) {
        payload = defaultPayload;
        getOrderDetailsAction(payload);
      } else if (!isLoggedIn && orderID && emailAddress) {
        payload = { ...defaultPayload, emailAddress };
        getOrderDetailsAction(payload);
      }
    }
  }, [orderID, isLoggedIn]);

  useEffect(() => {
    let appeasementAppliedOnOrder = true;
    if (orderItems) {
      orderItems.every((orderItem) => {
        const {
          productInfo: { appeasementApplied },
        } = orderItem;
        if (!appeasementApplied) {
          appeasementAppliedOnOrder = false;
          return false;
        }
        return true;
      });
    }
    setIsAppeasementAppliedOnOrder(appeasementAppliedOnOrder);
  }, [orderItems]);

  useEffect(() => {
    getContentReferedLabels(
      labels,
      getReasonsCodeContent,
      fetchResolutionModuleX,
      fetchCancelContent
    );
  }, [reasonCodes]);

  useEffect(() => {
    const { orderNumber } = orderDetailsData || {};
    if (orderNumber) {
      trackAnalyticsPageView({ orderId_cd127: orderNumber });
    }
  }, [orderDetailsData]);

  useEffect(() => {
    if (resolutionContentId) {
      fetchTransitHelpModuleX(resolutionContentId.contentId);
    }
    if (returnByMailContentId) {
      fetchReturnByMailModuleX(returnByMailContentId.contentId);
    }
    if (returnInStoreContentId) {
      fetchReturnInStoreModuleX(returnInStoreContentId.contentId);
    }
    if (returnPolicyContentId) {
      fetchReturnPolicyModuleX(returnPolicyContentId.contentId);
    }
  }, [resolutionContentId]);

  const getOrderItem = () => {
    const {
      orderType,
      orderNumber,
      orderDate,
      encryptedEmailAddress,
      status,
      orderStatus,
      shippingMethod,
      summary,
      isBopisOrder,
      isBossOrder,
      purchasedItems,
      canceledItems,
    } = orderDetailsData || {};
    const { imagePath, name } =
      (orderItems && orderItems.length > 0 && orderItems[0].productInfo) || {};
    const ItemsCount = getTotalItemsCount(orderItems);
    const isoParsedDate = parseISO(orderDate);
    const orderDateParsed = isValid(isoParsedDate) && format(isoParsedDate, 'MM/dd/yyyy');
    const isOlderItem = isOldItem(new Date(convertToISOString(orderDate)), cshForMonthsToDisplay);
    const isOlderInitiateReturn = isOldItem(
      new Date(convertToISOString(orderDate)),
      cshReturnItemOlderDays
    );
    const { currencySymbol, grandTotal } = summary || {};
    const parsedDate = new Date(orderDateParsed);
    return {
      orderNumber,
      orderDate: getFormatedOrderDate(parsedDate),
      currencySymbol,
      grandTotal,
      productName: name,
      imagePath,
      orderType,
      encryptedEmailAddress,
      ItemsCount,
      orderStatus,
      status,
      shippingMethod,
      isOlderItem,
      isBopisOrder,
      isBossOrder,
      purchasedItems,
      shipmentData,
      canceledItems,
      isOlderInitiateReturn,
    };
  };

  const renderResolution = () => {
    const { RECEIVED_WRONG_ITEM, MISSING_ORDER } = REASON_CODES;
    return (
      <>
        {selectedReasonCode && (
          <Resolution
            selectedResolution={
              selectedReasonCode.action === RECEIVED_WRONG_ITEM
                ? selectedWrongItemResolution
                : selectedResolution
            }
            selectedWrongItemResolution={selectedWrongItemResolution}
            setResolution={setResolution}
            resolutionList={getResolutionComponent(
              selectedReasonCode.action,
              refundData,
              selectedOrderItems,
              selectedShipmentCard,
              isPaidNExpediteRefundEnable
            )}
            labels={labels}
            resolutionModuleXContent={resolutionModuleXContent}
            cancelResolutionModuleXContent={cancelResolutionModuleXContent}
            selectedReasonCode={selectedReasonCode}
            transitHelpModuleXContent={transitHelpModuleXContent}
            selectedShipmentCard={selectedShipmentCard}
            refundData={refundData}
            selectOrderAction={selectOrderAction}
            estimatedDeliveryDate={estimatedDeliveryDate}
            setWrongItemData={setWrongItemData}
            wrongItemData={wrongItemData}
            hideButton={selectedReasonCode.action === RECEIVED_WRONG_ITEM}
            isOrderStatusTransit={isOrderStatusTransit}
            orderLabels={orderLabels}
            orderDetailsData={orderDetailsData}
            trackAnalyticsClick={trackAnalyticsClick}
            orderDelivereyDate={orderDelivereyDate}
            clickAnalyticsData={clickAnalyticsData}
            isPaidNExpediteRefundEnable={isPaidNExpediteRefundEnable}
            clearSelectedResolution={clearSelectedResolution}
            isLoggedIn={isLoggedIn}
            selectedOrderItems={selectedOrderItems}
            isLiveChatEnabled={isLiveChatEnabled}
            isInternationalShipping={isInternationalShipping}
            walletLabels={walletLabels}
            selectedReturnMethod={selectedReturnMethod}
            stepId="step-5"
          />
        )}
        {wrongItemData &&
          Object.keys(wrongItemData).length > 0 &&
          wrongItemData.someThingElse &&
          selectedReasonCode &&
          selectedReasonCode.action === RECEIVED_WRONG_ITEM && (
            <Resolution
              selectedResolution={selectedResolution}
              setResolution={setResolution}
              resolutionList={getResolutionComponent(
                MISSING_ORDER,
                refundData,
                selectedOrderItems,
                selectedShipmentCard
              )}
              labels={labels}
              resolutionModuleXContent={resolutionModuleXContent}
              cancelResolutionModuleXContent={cancelResolutionModuleXContent}
              selectedReasonCode={selectedReasonCode}
              setWrongItemData={setWrongItemData}
              wrongItemData={wrongItemData}
              trackAnalyticsClick={trackAnalyticsClick}
              orderDelivereyDate={orderDelivereyDate}
              orderDetailsData={orderDetailsData}
              clickAnalyticsData={clickAnalyticsData}
              wrongItemResolution
              clearSelectedResolution={clearSelectedResolution}
              selectedOrderItems={selectedOrderItems}
              isLiveChatEnabled={isLiveChatEnabled}
              isInternationalShipping={isInternationalShipping}
              selectedReturnMethod={selectedReturnMethod}
              stepId="step-6"
            />
          )}
      </>
    );
  };

  useEffect(() => {
    trackAnalyticsClick({ orderHelpLocation: CUSTOMER_HELP_GA.BANNER_CTA_TRACK.CD126 });
  }, []);
  useEffect(() => {
    if (orderDetailsData && reasonCodes && reasonCodes.length > 0 && !selectedReasonCode) {
      setTimeout(() => {
        smoothScrolling('step-3');
      }, 100);
    }
  }, [orderDetailsData, reasonCodes]);

  useEffect(() => {
    if (selectedReasonCode) {
      const step = selectedReasonCode?.action === REASON_CODES.CANCEL_MODIFY ? 'step-5' : 'step-4';
      setTimeout(() => {
        smoothScrolling(step);
      }, 100);
    }
  }, [selectedReasonCode]);

  useEffect(() => {
    if (selectedOrderItems && Object.keys(selectedOrderItems).length !== 0) {
      if (
        selectedReasonCode?.action !== REASON_CODES.INITIATE_RETURN ||
        selectedReasonCode?.action !== REASON_CODES.WHEN_WILL_MY_ORDER_ARRIVE
      ) {
        setTimeout(() => {
          smoothScrolling('step-5');
        }, 100);
      }
      if (selectedReasonCode?.action === REASON_CODES.INITIATE_RETURN) {
        setTimeout(() => {
          smoothScrolling('return-continue-button', 500);
        }, 150);
      }
      if (selectedReasonCode?.action === REASON_CODES.WHEN_WILL_MY_ORDER_ARRIVE) {
        setTimeout(() => {
          smoothScrolling('myorderarrive');
        }, 100);
      }
    }
  }, [selectedOrderItems, selectedReasonCode]);

  useEffect(() => {
    if (selectedReasonCode && selectedReasonCode?.action === REASON_CODES.INITIATE_RETURN) {
      clearSelectedResolution();
    }
  }, [selectedReasonCode]);

  useEffect(() => {
    if (
      selectedReasonCode &&
      selectedReasonCode?.action === REASON_CODES.WHEN_WILL_MY_ORDER_ARRIVE
    ) {
      clearSelectedResolution();
      clearOrderArriveSelectedReasonCode();
    }
  }, [selectedReasonCode]);

  const getReasonCodeComponent = (reasons) => {
    const { isOlderInitiateReturn } = getOrderItem();
    if (reasons?.length > 0) {
      return (
        <Row fullBleed>
          <Col colSize={{ small: 6, medium: 8, large: 12 }}>
            <ReasonCode
              reasonList={reasons}
              setSelectedReasonCode={setSelectedReasonCode}
              selectedReasonCode={selectedReasonCode}
              clearSelectedReasonCode={() => clearSelectedReasonCode()}
              clearSelectedResolution={() => clearSelectedResolution()}
              labels={labels}
              isLoggedIn={isLoggedIn}
              orderDetailsData={orderDetailsData}
              refundData={refundData}
              estimatedDeliveryDate={estimatedDeliveryDate}
              setOrderStatusInTransit={setOrderStatusInTransit}
              trackAnalyticsClick={trackAnalyticsClick}
              shipmentData={shipmentData}
              getRefundStatus={getRefundStatus}
              scrollClassName="reason-codes-scroll"
              reasonCodeFromURL={reasonCodeFromPath}
              preReasonFlag={preReasonFlag}
              isCouponSelfHelpEnabled={isCouponSelfHelpEnabled}
              isOlderItem={isOlderInitiateReturn}
              isNewWhenWillMyOrderArrive={isNewWhenWillMyOrderArrive}
              updateHelpstep1={(payload) => setHelpstep1(payload)}
              helpstep1={helpstep1}
            />
          </Col>
        </Row>
      );
    }
    return (
      <Row fullBleed>
        <ReasonCodeSkeleton />
      </Row>
    );
  };

  const isNonOnlineOrOlderOrder = () => {
    const { isBossOrder, isBopisOrder, isOlderItem } = getOrderItem();
    return isBossOrder || isBopisOrder || isOlderItem;
  };

  const getHelpMessageLabel = () => {
    const { isBossOrder, isBopisOrder, isOlderItem } = getOrderItem();
    if (isBossOrder || isBopisOrder) {
      return (
        <>
          {getLabelValue(labels, 'lbl_pickup_disabled_initial', 'orders')}
          &nbsp;
          {getLabelValue(labels, 'lbl_contact_us_directly', 'orders')}
          &nbsp;
          {getLabelValue(labels, 'lbl_pickup_disabled_ending', 'orders')}
        </>
      );
    }
    if (isOlderItem) {
      return (
        <>
          {getLabelValue(labels, 'lbl_older_disabled_initial', 'orders')}
          &nbsp;
          {getLabelValue(labels, 'lbl_review_our_full', 'orders')}
        </>
      );
    }
    return '';
  };

  const getItems = (orderGroupSelected) => {
    let result;
    orderGroupSelected.items.forEach((el) => {
      result = { ...result, [el.lineNumber]: el.itemInfo.quantity };
    });
    return result;
  };

  const handleShipmentCardClick = (cardIdx, orderGroupSelected) => {
    const { orderNumber, orderDate, shippingMethod } = orderDetailsData;
    const { trackingDetails } = shipmentData || [];
    const selectedItemGroup =
      trackingDetails &&
      trackingDetails.filter((item) => item.trackingId === orderGroupSelected.trackingNumber);
    if (orderNumber && orderDate && shippingMethod) {
      const deliveredDate =
        selectedItemGroup && selectedItemGroup[0] && selectedItemGroup[0].deliveredDate;
      const orderId = orderNumber;
      const orderPlacedDate = formatYmd(orderDate);
      const orderDeliveredDate = deliveredDate ? formatYmd(deliveredDate) : '';
      getRefundStatus({ orderId, orderPlacedDate, shippingMethod, orderDeliveredDate });
      setOrderDeliveredDate(orderDeliveredDate);
    }
    setSelectedShipmentCard(cardIdx);
    const selectedItems = orderGroupSelected && getItems(orderGroupSelected);
    if (orderGroupSelected) {
      setSelectedOrderItems(selectedItems);
    }
  };

  const showOrderDisabled = () => {
    const { orderStatus, grandTotal } = getOrderItem();
    const orderTotalNonZero = !!grandTotal;
    const { CANCELLED, CANCELED } = ORDER_ITEM_STATUS;
    const isOrderCancelled =
      orderStatus === toLower(CANCELLED) || orderStatus === toLower(CANCELED);
    return (
      isNonOnlineOrOlderOrder() ||
      isAppeasementAppliedOnOrder ||
      isOrderCancelled ||
      !orderTotalNonZero
    );
  };

  const getTellUsMoreHeading = ({ classes = '', isShowOrderDisabled }) => {
    return (
      <div className={`header-title${classes}`} id="step-3">
        <BodyCopy
          component="p"
          fontWeight="bold"
          fontFamily="secondary"
          fontSize="fs16"
          className="tell-us-more"
        >
          {getLabelValue(labels, 'lbl_tell_us_more_about_your_order', '')}
        </BodyCopy>
        {!selectedReasonCode && isShowOrderDisabled && (
          <BodyCopy
            component="p"
            fontFamily="secondary"
            fontSize="fs14"
            className="elem-pl-XS elem-pr-XS select-option"
          >
            {getLabelValue(labels, 'lbl_select_an_option', '')}
          </BodyCopy>
        )}
      </div>
    );
  };

  return (
    <div className={className}>
      {orderDetailsData && reasonCodes && !isFetchingOrderItems ? (
        <>
          <>
            <Row fullBleed>
              <Col colSize={{ small: 6, medium: 7, large: 12 }}>
                <div className="header-container top-header-container">
                  <BodyCopy component="p" fontWeight="bold" fontFamily="secondary" fontSize="fs16">
                    {getLabelValue(labels, 'lbl_selected_order')}
                  </BodyCopy>
                  {!ordersListItems || totalRecordsAdd > 1 || !isLoggedIn ? (
                    <Button
                      className="blue-link"
                      nohover
                      type="button"
                      link
                      underline
                      color="blue"
                      onClick={selectOrderAction}
                    >
                      {getLabelValue(labels, 'lbl_check_another_order')}
                    </Button>
                  ) : null}
                </div>
              </Col>
            </Row>
            <Row fullBleed>
              <Col colSize={{ small: 6, medium: 4, large: 4 }} id="order-tile">
                <OrderTile
                  labels={labels}
                  orderLabels={orderLabels}
                  orderDetailsData={getOrderItem()}
                  selected
                  isNonOnlineOrOlderOrder={isNonOnlineOrOlderOrder()}
                  isAppeasementAppliedOnOrder={isAppeasementAppliedOnOrder}
                />
              </Col>
            </Row>
          </>
          <div className={`${showOrderDisabled() ? 'section-background disabled-area' : ''}`}>
            <Row fullBleed id="reason-code-header">
              <Col colSize={{ small: 6, medium: 7, large: 12 }}>
                <div
                  className={`header-container ${
                    showOrderDisabled() ? 'header-container-nononline elem-mb-XS' : ''
                  }`}
                >
                  {getTellUsMoreHeading({ isShowOrderDisabled: !showOrderDisabled() })}
                  {selectedReasonCode && (
                    <Button
                      className="blue-link"
                      nohover
                      type="button"
                      link
                      underline
                      color="blue"
                      onClick={() => {
                        clearSelectedReasonCode();
                        clearSelectedResolution();
                        clearSelectedOrderItems();
                        setWrongItemData({});
                        setOrderStatusInTransit(false);
                        setToggleCards(false);
                        setSelectedShipmentCard(null);
                        setIsDelivered(false);
                        setFilteredList(null);
                        setReturnItemDisplay(false);
                        clearReturnMethod();
                        setHelpstep1('');
                        if (router?.query?.reasonCode) {
                          routerReplace(
                            `${CUSTOMER_HELP_ROUTES.TELL_US_MORE.to}&orderId=${orderDetailsData?.orderNumber}`,
                            `${CUSTOMER_HELP_ROUTES.TELL_US_MORE.asPath}/${orderDetailsData?.orderNumber}`
                          );
                        }
                      }}
                    >
                      {getLabelValue(orderLabels, 'lbl_edit_option', 'orders')}
                    </Button>
                  )}
                </div>
              </Col>
            </Row>
            {!showOrderDisabled() && getReasonCodeComponent(reasonCodes)}
            {showOrderDisabled() && (
              <div className="disabled-content">
                <CustomerServiceHelp
                  labels={labels}
                  selectedOrderItems={selectedOrderItems}
                  helpMessage={getHelpMessageLabel()}
                  isNonOnlineOrOlderOrder
                  showReturnPolicy
                />
                <div
                  className={`review-button ${
                    showOrderDisabled() ? 'review-button-tell-us-more' : ''
                  }`}
                >
                  {totalRecordsAdd > 1 || !isLoggedIn ? (
                    <div className="review-submit-align">
                      <Button
                        className="footer-button "
                        fontSize="fs14"
                        fontWeight="extrabold"
                        buttonVariation="variable-width"
                        fill="WHITE"
                        onClick={selectOrderAction}
                      >
                        {getLabelValue(orderLabels && orderLabels.orders, 'lbl_help_another_order')}
                      </Button>
                    </div>
                  ) : null}
                </div>
              </div>
            )}
          </div>

          {selectedReasonCode &&
            (selectedReasonCode.action === REASON_CODES.MISSING_ORDER ||
              selectedReasonCode.action === REASON_CODES.RECEIVED_WRONG_ITEM ||
              selectedReasonCode.action === REASON_CODES.INITIATE_RETURN ||
              selectedReasonCode.action === REASON_CODES.WHEN_WILL_MY_ORDER_ARRIVE) && (
              <>
                {!isReturnItemDisplay ? (
                  <>
                    <Row fullBleed className="elem-mt-LRG elem-pt-XS" id="step-4">
                      {selectedReasonCode?.action === REASON_CODES.INITIATE_RETURN && (
                        <Col colSize={{ small: 12, medium: 12, large: 12 }}>
                          <div className="order-Items-header-container">
                            <BodyCopy
                              component="p"
                              fontWeight="bold"
                              fontFamily="secondary"
                              fontSize="fs22"
                              className="steps"
                            >
                              {getLabelValue(labels, 'lbl_initiate_return_step_1', '')}
                            </BodyCopy>
                          </div>
                        </Col>
                      )}
                      {selectedReasonCode?.action === REASON_CODES.WHEN_WILL_MY_ORDER_ARRIVE && (
                        <MyOrderArrive
                          orderDetailsData={orderDetailsData}
                          labels={labels}
                          orderLabels={orderLabels}
                          action={selectedReasonCode?.action}
                          setSelectedShipmentCard={setSelectedShipmentCard}
                          handleShipmentCardClick={handleShipmentCardClick}
                          selectedShipmentCard={selectedShipmentCard}
                          filteredItems={filteredItems}
                          setFilteredList={setFilteredList}
                          clearSelectedResolution={clearSelectedResolution}
                          isLoggedIn={isLoggedIn}
                          reasons={reasonCodes}
                          refundData={refundData}
                          trackAnalyticsClick={trackAnalyticsClick}
                          shipmentData={shipmentData}
                          getRefundStatus={getRefundStatus}
                          preReasonFlag={preReasonFlag}
                          cshReturnItemOlderDays={cshReturnItemOlderDays}
                          cshForMonthsToDisplay={cshForMonthsToDisplay}
                          selectedResolution={selectedResolution}
                          setResolution={setResolution}
                          selectedOrderItems={selectedOrderItems}
                          isPaidNExpediteRefundEnable={isPaidNExpediteRefundEnable}
                          resolutionModuleXContent={resolutionModuleXContent}
                          cancelResolutionModuleXContent={cancelResolutionModuleXContent}
                          transitHelpModuleXContent={transitHelpModuleXContent}
                          selectOrderAction={selectOrderAction}
                          setWrongItemData={setWrongItemData}
                          wrongItemData={wrongItemData}
                          isOrderStatusTransit={isOrderStatusTransit}
                          orderDelivereyDate={orderDelivereyDate}
                          clickAnalyticsData={clickAnalyticsData}
                          isLiveChatEnabled={isLiveChatEnabled}
                          isInternationalShipping={isInternationalShipping}
                          clearSelectedOrderItems={clearSelectedOrderItems}
                          reasonCodes={reasonCodes}
                          setOrderStatusInTransit={setOrderStatusInTransit}
                          isCouponSelfHelpEnabled={isCouponSelfHelpEnabled}
                          walletLabels={walletLabels}
                          setOrderArriveSelectedReasonCode={setOrderArriveSelectedReasonCode}
                          clearOrderArriveSelectedReasonCode={clearOrderArriveSelectedReasonCode}
                          selectedOrderArriveReasonCode={selectedOrderArriveReasonCode}
                          setSelectedOrderArriveDeliveredReasonCode={
                            setSelectedOrderArriveDeliveredReasonCode
                          }
                          selectedOrderArriveDeliveredReasonCode={
                            selectedOrderArriveDeliveredReasonCode
                          }
                          selectedWrongItemResolution={selectedWrongItemResolution}
                          updateHelpstep1={(payload) => setHelpstep1(payload)}
                          helpstep1={helpstep1}
                        />
                      )}
                      {selectedReasonCode?.action !== REASON_CODES.WHEN_WILL_MY_ORDER_ARRIVE ? (
                        <Col
                          className="select-option-text"
                          colSize={
                            selectedReasonCode?.action === REASON_CODES.INITIATE_RETURN
                              ? { small: 6, medium: 6, large: 5 }
                              : { small: 6, medium: 6, large: 3 }
                          }
                        >
                          <div className="order-Items-header-container">
                            <BodyCopy
                              component="p"
                              fontWeight="bold"
                              fontFamily="secondary"
                              fontSize={['fs14', 'fs16', 'fs16']}
                            >
                              {getSubHeaderLabel(selectedReasonCode, orderLabels, labels)}
                            </BodyCopy>
                            {selectedReasonCode.action === REASON_CODES.INITIATE_RETURN && (
                              <>
                                &nbsp;
                                {Object.keys(resolutionModuleXContent).length > 0 && (
                                  <ReactTooltip
                                    fontFamily="secondary"
                                    message={convertHtml(returnPolicyModuleXContent.richText)}
                                    minWidth="220px"
                                    aligned="right"
                                  >
                                    <Image
                                      width="20px"
                                      height="20px"
                                      src={getIconPath('info-icon-blue')}
                                      alt="info"
                                    />
                                  </ReactTooltip>
                                )}
                              </>
                            )}
                          </div>
                        </Col>
                      ) : null}
                      <Col colSize={{ small: 6, medium: 6, large: 6 }}>
                        <BodyCopy
                          className="elem-mt-XS elem-pt-XXXS order-Items-header-container multiple-items-text"
                          component="span"
                          fontFamily="secondary"
                          fontSize="fs14"
                        >
                          {selectedReasonCode.action === REASON_CODES.RECEIVED_WRONG_ITEM &&
                            getLabelValue(orderLabels, 'lbl_select_only_one_item', 'orders')}
                        </BodyCopy>
                      </Col>
                    </Row>
                    {selectedReasonCode?.action !== REASON_CODES.WHEN_WILL_MY_ORDER_ARRIVE ? (
                      <Row fullBleed>
                        <Col
                          ignoreGutter={{ large: true }}
                          colSize={{ small: 6, medium: 8, large: 12 }}
                        >
                          <OrderItems
                            labels={labels}
                            orderLabels={orderLabels}
                            orderDetailsData={orderDetailsData}
                            setSelectedOrderItems={setSelectedOrderItems}
                            selectedOrderItems={selectedOrderItems}
                            shipmentData={shipmentData}
                            clearSelectedOrderItems={clearSelectedOrderItems}
                            selectedReasonCode={selectedReasonCode}
                            clearSelectedResolution={clearSelectedResolution}
                          />
                        </Col>
                      </Row>
                    ) : null}
                    {selectedReasonCode?.action === REASON_CODES.INITIATE_RETURN && (
                      <Row fullBleed id="return-continue-button">
                        <Button
                          fill="BLUE"
                          type="submit"
                          buttonVariation="variable-width"
                          className="cta-btn initiate-return-continue-btn"
                          disabled={Object.keys(selectedOrderItems).length <= 0}
                          loading={false}
                          onClick={onContinueClick}
                        >
                          {getLabelValue(labels, 'lbl_help_refund_action_btn')}
                        </Button>
                      </Row>
                    )}
                  </>
                ) : (
                  <InitiateReturn
                    labels={labels}
                    orderLabels={orderLabels}
                    orderDetailsData={orderDetailsData}
                    setReturnMethod={setReturnMethod}
                    selectedReturnMethod={selectedReturnMethod}
                    clearReturnMethod={clearReturnMethod}
                    setReturnItemDisplay={setReturnItemDisplay}
                    selectedOrderItems={selectedOrderItems}
                    selectedReasonCode={selectedReasonCode}
                  />
                )}
              </>
            )}
        </>
      ) : (
        ordersListItems &&
        Object.keys(ordersListItems).length && (
          <>
            <Row fullBleed className="header-container top-header-container">
              <BodyCopy component="p" fontWeight="bold" fontFamily="secondary" fontSize="fs16">
                {getLabelValue(labels, 'lbl_selected_order')}
              </BodyCopy>
              {totalRecordsAdd > 1 || !isLoggedIn ? (
                <Button
                  className="blue-link"
                  nohover
                  type="button"
                  link
                  underline
                  color="blue"
                  onClick={selectOrderAction}
                >
                  {getLabelValue(labels, 'lbl_check_another_order')}
                </Button>
              ) : null}
            </Row>
            <Row fullBleed className="order-tile-container">
              <Col colSize={{ small: 6, medium: 4, large: 4 }}>
                <OrderTile
                  labels={labels}
                  orderLabels={orderLabels}
                  orderDetailsData={getOrderTileData({
                    ordersListItems,
                    orderID,
                    cshForMonthsToDisplay,
                  })}
                  selected
                />
              </Col>
            </Row>
            <div>
              <Row fullBleed id="reason-code-header">
                <Col colSize={{ small: 6, medium: 7, large: 12 }}>
                  {getTellUsMoreHeading({
                    classes: ' order-no-found-header',
                    isShowOrderDisabled: true,
                  })}
                </Col>
              </Row>
            </div>

            {isFetchingOrderItems && <TellUsMoreSkeleton />}
            {!isFetchingOrderItems && getReasonCodeComponent(reasonCodes)}
          </>
        )
      )}
      {!ordersListItems && isFetchingOrderItems && (
        <>
          <div className="cls-height-fix" />
          <TellUsMoreSkeleton />
        </>
      )}
      {getOrderCards({
        selectedReasonCode,
        labels,
        setSelectedShipmentCard,
        clearSelectedResolution,
        selectedShipmentCard,
        orderDetailsData,
        orderLabels,
        isDelivered,
        handleShipmentCardClick,
        setIsDelivered,
        setOrderStatusInTransit,
        setToggleCards,
        toggleCards,
        shipmentData,
        getRefundStatus,
        setFilteredList,
        filteredItems,
      })}
      {selectedReasonCode?.action !== REASON_CODES.WHEN_WILL_MY_ORDER_ARRIVE && renderResolution()}
    </div>
  );
};

TellUsMore.propTypes = {
  reasonCodes: PropTypes.shape([]).isRequired,
  getReasonsCodeContent: PropTypes.func.isRequired,
  labels: PropTypes.shape({}).isRequired,
  walletLabels: PropTypes.shape({}).isRequired,
  setSelectedReasonCode: PropTypes.func.isRequired,
  setOrderArriveSelectedReasonCode: PropTypes.func.isRequired,
  selectedReasonCode: PropTypes.shape({}).isRequired,
  selectedOrderArriveReasonCode: PropTypes.shape({}).isRequired,
  clearSelectedReasonCode: PropTypes.func.isRequired,
  clearOrderArriveSelectedReasonCode: PropTypes.func.isRequired,
  clearSelectedResolution: PropTypes.func.isRequired,
  orderLabels: PropTypes.shape({}).isRequired,
  orderDetailsData: PropTypes.shape({}).isRequired,
  getOrderDetailsAction: PropTypes.func.isRequired,
  router: PropTypes.shape({}).isRequired,
  selectOrderAction: PropTypes.func.isRequired,
  isLoggedIn: PropTypes.bool.isRequired,
  orderItems: PropTypes.shape([]).isRequired,
  setSelectedOrderItems: PropTypes.func.isRequired,
  selectedOrderItems: PropTypes.shape({}),
  shipmentData: PropTypes.shape({}),
  selectedResolution: PropTypes.string,
  setResolution: PropTypes.func.isRequired,
  clearSelectedOrderItems: PropTypes.func,
  fetchResolutionModuleX: PropTypes.func,
  resolutionModuleXContent: PropTypes.func,
  cancelResolutionModuleXContent: PropTypes.func,
  getRefundStatus: PropTypes.func,
  refundData: PropTypes.shape({}),
  fetchTransitHelpModuleX: PropTypes.shape({}),
  transitHelpModuleXContent: PropTypes.shape({}),
  resolutionContentId: PropTypes.shape({}),
  setWrongItemData: PropTypes.func,
  wrongItemData: PropTypes.shape({}),
  selectedWrongItemResolution: PropTypes.shape({}),
  cshForMonthsToDisplay: PropTypes.number,
  cshReturnItemOlderDays: PropTypes.number,
  setOrderStatusInTransit: PropTypes.func,
  isOrderStatusTransit: PropTypes.bool,
  setToggleCards: PropTypes.func,
  toggleCards: PropTypes.bool,
  isPaidNExpediteRefundEnable: PropTypes.bool,
  trackAnalyticsPageView: PropTypes.func,
  trackAnalyticsClick: PropTypes.func,
  selectedShipmentCard: PropTypes.string,
  setSelectedShipmentCard: PropTypes.func,
  clickAnalyticsData: PropTypes.shape({}),
  resetCustomerHelpReducer: PropTypes.func,
  setFilteredList: PropTypes.func,
  filteredItems: PropTypes.shape([]),
  ordersListItems: PropTypes.shape([]).isRequired,
  isFetchingOrderItems: PropTypes.bool,
  orderLookUpAPISwitch: PropTypes.bool,
  isLiveChatEnabled: PropTypes.bool,
  preReasonFlag: PropTypes.bool,
  isCouponSelfHelpEnabled: PropTypes.bool.isRequired,
  setLoader: PropTypes.bool,
  setActiveSection: PropTypes.func,
  clearReturnMethod: PropTypes.func,
  setReturnMethod: PropTypes.func,
  setReturnItemDisplay: PropTypes.func,
  selectedReturnMethod: PropTypes.string,
  isReturnItemDisplay: PropTypes.bool.isRequired,
  setSelectedOrderArriveDeliveredReasonCode: PropTypes.func,
  selectedOrderArriveDeliveredReasonCode: PropTypes.shape({}),
  helpstep1: PropTypes.string,
  updateHelpstep1: PropTypes.func,
};
TellUsMore.defaultProps = {
  selectedOrderItems: {},
  shipmentData: {},
  selectedResolution: '',
  clearSelectedOrderItems: () => {},
  fetchResolutionModuleX: () => {},
  resolutionModuleXContent: () => {},
  cancelResolutionModuleXContent: () => {},
  getRefundStatus: () => {},
  refundData: {},
  fetchTransitHelpModuleX: {},
  transitHelpModuleXContent: {},
  resolutionContentId: {},
  setWrongItemData: () => {},
  wrongItemData: PropTypes.shape({}),
  selectedWrongItemResolution: PropTypes.shape({}),
  cshForMonthsToDisplay: 3,
  cshReturnItemOlderDays: 3,
  setOrderStatusInTransit: () => {},
  isOrderStatusTransit: false,
  setToggleCards: () => {},
  toggleCards: false,
  isPaidNExpediteRefundEnable: false,
  trackAnalyticsPageView: () => {},
  trackAnalyticsClick: () => {},
  selectedShipmentCard: '',
  setSelectedShipmentCard: () => {},
  clickAnalyticsData: {},
  resetCustomerHelpReducer: () => {},
  setFilteredList: () => {},
  filteredItems: [],
  isFetchingOrderItems: true,
  orderLookUpAPISwitch: false,
  isLiveChatEnabled: false,
  preReasonFlag: true,
  setLoader: false,
  setActiveSection: () => {},
  clearReturnMethod: () => {},
  setReturnMethod: () => {},
  selectedReturnMethod: '',
  setReturnItemDisplay: () => {},
  setSelectedOrderArriveDeliveredReasonCode: () => {},
  selectedOrderArriveDeliveredReasonCode: null,
  helpstep1: '',
  updateHelpstep1: () => {},
};

export default withStyles(TellUsMore, styles);
