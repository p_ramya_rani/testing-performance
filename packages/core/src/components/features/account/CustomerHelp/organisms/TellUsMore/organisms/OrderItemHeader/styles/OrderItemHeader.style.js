// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  .order-header-container {
    display: flex;
    flex-direction: column;
  }
  .item-header {
    display: flex;
    align-items: center;
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  }

  .item-header-area {
    display: flex;
    align-items: center;
    margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.LRG_1};
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
  }

  .tracking-number-area {
    display: flex;
    align-items: center;
    margin-left: 26px;
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
  }

  .wrap {
    @media ${(props) => props.theme.mediaQuery.large} {
      width: 50%;
      word-break: break-all;
    }
  }

  .item-header-content {
    display: flex;
    align-items: center;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      font-size: 12px;
    }
    color: ${(props) => props.theme.colorPalette.green[600]};
  }
`;

export default styles;
