import { fromJS } from 'immutable';
import { getOtpDetail, getOtpUniqueKey } from '../MergeAccountOtp.selector';

describe('Merge Account Otp selector', () => {
  const mergeAccountOtpState = fromJS({
    otpDetail: {},
    getOtpSuccess: {},
    error: null,
    uniqueKey: null,
  });

  const state = {
    MergeAccountOtp: mergeAccountOtpState,
  };
  it('Merge Account Otp detail', () => {
    expect(getOtpDetail(state)).toEqual(mergeAccountOtpState.get('otpDetail'));
  });

  it('Merge Account Otp detail', () => {
    expect(getOtpUniqueKey(state)).toEqual(mergeAccountOtpState.get('uniqueKey'));
  });
});
