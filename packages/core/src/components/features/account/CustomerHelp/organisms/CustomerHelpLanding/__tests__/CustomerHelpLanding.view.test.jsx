// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import CustomerHelpLanding from '../views/CustomerHelpLanding.view';

describe('CustomerHelpLanding', () => {
  const props = {
    labels: {},
    topLevelReasonCodes: [
      {
        id: '/1',
        label: 'An Existing Order',
        external: 0,
        title: '',
        action: 'EXISTING_ORDER',
        target: '_modal',
        __typename: 'Button',
      },
      {
        id: '/2',
        label: 'My Account & Coupons',
        external: 0,
        title: '',
        action: 'ACCOUNT_AND_COUPONS',
        target: '_modal',
        __typename: 'Button',
      },
      {
        id: '/3',
        label: 'I need help with something else',
        external: 0,
        title: '',
        action: 'SOMETHING_ELSE',
        target: '_modal',
        __typename: 'Button',
      },
    ],
  };
  it('should render correctly', () => {
    const component = shallow(<CustomerHelpLanding {...props} />);
    expect(component).toMatchSnapshot();
  });
});
