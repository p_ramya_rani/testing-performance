// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  .elem-mb-XL {
    @media ${(props) => props.theme.mediaQuery.large} {
      margin: 0;
    }
  }
  .jJKDJu {
    @media ${(props) => props.theme.mediaQuery.small} {
      margin: 0;
    }
  }
  .account-info-main {
    .content {
      width: 100%;
    }
    min-height: 179px;
    .heading {
      margin: 0;
    }
    .dfhKWn {
      min-height: 150px;
      height: 150px;
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      padding-left: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
      padding-right: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
    }
  }
`;

export default styles;
