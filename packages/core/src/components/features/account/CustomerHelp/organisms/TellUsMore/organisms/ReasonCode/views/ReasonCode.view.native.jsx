// 9fbef606107a605d69c0edbcd8029e5d
import React, { useEffect } from 'react';
import { TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { BodyCopy } from '@tcp/core/src/components/common/atoms';
import AccessibilityRoles from '@tcp/core/src/constants/AccessibilityRoles.constant';
import { getMaxDeliveryDate } from '@tcp/core/src/components/features/account/CustomerHelp/util/utility';
import {
  REASON_CODES,
  ORDER_ITEM_STATUS,
} from '@tcp/core/src/components/features/account/CustomerHelp/CustomerHelp.constants';
import { getLabelValue, formatYmd } from '@tcp/core/src/utils/utils';
import { Container, ItemContainer, SelectedItemContainer } from '../styles/ReasonCode.style.native';

const getPaidNExpediteOrderTransitStatus = (
  selectedReasonCode,
  orderItems,
  setOrderStatusInTransit
) => {
  const { SHIPMENT_API_DELIVERED } = ORDER_ITEM_STATUS;
  const { PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME } = REASON_CODES;
  if (selectedReasonCode.action === PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME) {
    const orderItemsGroup =
      orderItems && orderItems.every((order) => order.orderStatus === SHIPMENT_API_DELIVERED);

    if (!orderItemsGroup) {
      setOrderStatusInTransit(true);
    }
  }
};

const navigateToOrderDetailPage = (orderNumber, orderLabels, navigation) => {
  const router = {
    query: {
      orderId: orderNumber,
    },
  };
  navigation.navigate('OrderDetailPage', {
    title: `${getLabelValue(orderLabels, 'lbl_orderDetail_heading', 'orders')} #${orderNumber}`,
    router,
  });
};

const getShipmentData = (
  reason,
  orderNumber,
  orderDate,
  shipmentData,
  shippingMethod,
  setSelectedReasonCode,
  getRefundStatus
) => {
  if (orderNumber && orderDate && shippingMethod) {
    const { trackingDetails } = shipmentData || {};
    const deliveryDate = getMaxDeliveryDate({ trackingDetails });
    const orderId = orderNumber;
    const orderPlacedDate = formatYmd(orderDate);
    const orderDeliveredDate = deliveryDate;
    getRefundStatus({ orderId, orderPlacedDate, shippingMethod, orderDeliveredDate });
  }
  setSelectedReasonCode(reason);
};

const ReasonCode = ({
  reasonList,
  selectedReasonCode,
  setSelectedReasonCode,
  orderDetailsData,
  getRefundStatus,
  setOrderStatusInTransit,
  navigation,
  orderLabels,
  trackAnalyticsClick,
  shipmentData,
}) => {
  const { orderDate, orderItems, shippingMethod, orderNumber } = orderDetailsData || {};
  useEffect(() => {
    if (selectedReasonCode) {
      // Scroll To Reason Code
      getPaidNExpediteOrderTransitStatus(selectedReasonCode, orderItems, setOrderStatusInTransit);
    }
  }, [selectedReasonCode]);

  useEffect(() => {
    const isInitiateReturn = (navigation && navigation.getParam('isInitiateReturn')) || '';
    if (isInitiateReturn) {
      setSelectedReasonCode({
        action: REASON_CODES.INITIATE_RETURN,
        label: "I'd like to return some items",
      });
    }
  }, []);

  const onReasonCodeClick = (reason) => {
    const { PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME, WHEN_WILL_MY_ORDER_ARRIVE } = REASON_CODES;
    const reasonCodeLbl = reason ? reason.label : '';
    trackAnalyticsClick(
      {
        orderHelp_path_cd128: reasonCodeLbl,
        orderHelpSelectedReason: reasonCodeLbl,
      },
      { name: 'orderHelpPath_click', module: 'account' }
    );
    switch (reason.action) {
      case WHEN_WILL_MY_ORDER_ARRIVE:
        navigateToOrderDetailPage(orderNumber, orderLabels, navigation);
        break;
      case PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME:
        getShipmentData(
          reason,
          orderNumber,
          orderDate,
          shipmentData,
          shippingMethod,
          setSelectedReasonCode,
          getRefundStatus
        );
        break;
      default:
        setSelectedReasonCode(reason);
    }
  };

  return (
    <Container>
      {!selectedReasonCode ? (
        reasonList.length > 0 &&
        reasonList.map((reason) => (
          <TouchableOpacity
            accessibilityRole={AccessibilityRoles.Button}
            onPress={() => onReasonCodeClick(reason)}
          >
            <ItemContainer>
              <BodyCopy
                fontWeight="semibold"
                fontSize="fs14"
                fontFamily="secondary"
                color="text.primary"
                text={reason.label}
              />
            </ItemContainer>
          </TouchableOpacity>
        ))
      ) : (
        <SelectedItemContainer>
          <BodyCopy
            fontWeight="semibold"
            fontSize="fs14"
            fontFamily="secondary"
            color="text.primary"
            text={selectedReasonCode.label}
          />
        </SelectedItemContainer>
      )}
    </Container>
  );
};

ReasonCode.propTypes = {
  orderDetailsData: PropTypes.shape({}).isRequired,
  reasonList: PropTypes.shape([]),
  selectedReasonCode: PropTypes.shape({}).isRequired,
  setSelectedReasonCode: PropTypes.func.isRequired,
  labels: PropTypes.shape({}).isRequired,
  orderLabels: PropTypes.shape({}).isRequired,
  navigation: PropTypes.shape({}).isRequired,
  getRefundStatus: PropTypes.func.isRequired,
  setOrderStatusInTransit: PropTypes.func,
  trackAnalyticsClick: PropTypes.func,
  shipmentData: PropTypes.shape({}).isRequired,
};

ReasonCode.defaultProps = {
  reasonList: [],
  setOrderStatusInTransit: () => {},
  trackAnalyticsClick: () => {},
};

export default ReasonCode;
