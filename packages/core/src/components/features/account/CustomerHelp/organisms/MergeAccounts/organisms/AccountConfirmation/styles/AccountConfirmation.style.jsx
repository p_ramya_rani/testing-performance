// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG_2};

  .TextBox__input {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: 99%;
    }
  }

  .merge-account-button {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      display: flex;
      margin-right: auto;
      margin-left: auto;
    }
  }

  .account-confirmation-error {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.REG};
  }

  .notification-container {
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  }
`;

export default styles;
