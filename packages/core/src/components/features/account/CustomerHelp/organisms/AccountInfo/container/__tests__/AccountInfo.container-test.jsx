// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import AccountInfoContainer from '../AccountInfo.container';

describe('AccountInfoContainer', () => {
  const props = {
    labels: {},
    profileInfo: {},
  };
  it('should render correctly', () => {
    const component = shallow(<AccountInfoContainer {...props} />);
    expect(component).toMatchSnapshot();
  });
});
