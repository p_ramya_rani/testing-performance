// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { ReasonCodeSkeletonVanilla } from '../ReasonCodeSkeleton.view';

describe('ReasonCodeSkeleton component', () => {
  it('should renders correctly', () => {
    const props = {
      className: 'sample-class',
    };
    const component = shallow(<ReasonCodeSkeletonVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
