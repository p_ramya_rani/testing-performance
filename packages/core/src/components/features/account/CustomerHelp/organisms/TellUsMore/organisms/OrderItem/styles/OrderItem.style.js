// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  position: relative;
  .container {
    align-items: center;
    display: flex;
    border-radius: 12px;
    border: ${(props) =>
      props.noBorder ? 'none' : `1px solid ${props.theme.colorPalette.gray[600]}`};
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    padding: ${(props) => props.theme.spacing.ELEM_SPACING.SM} 0px;
  }
  .item-status-tag {
    position: absolute;
    background-color: ${(props) => props.theme.colorPalette.black};
    border-radius: 14px;
    height: 22px;
    max-width: 106px;
    align-items: center;
    border: 1px solid ${(props) => props.theme.colorPalette.black};
    display: flex;
    right: -6px;
    top: -10px;
    padding: 0px 8px;
  }
  .imageContainer {
    width: 100px;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    padding-right: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
  }

  .detailContainer {
    display: flex;
    flex-direction: column;
    width: 100%;
  }
  .message_container {
    width: 70px;
    text-align: center;
    position: absolute;
    right: 23px;
    bottom: 36px;
    @media ${(props) => props.theme.mediaQuery.large} {
      right: 16px;
      bottom: 42px;
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      right: 9px;
      bottom: 42px;
    }
  }
  .product-item-container {
    width: 65%;
  }
  .qty-selector {
    text-align: center;
    position: absolute;
    right: 12px;
    top: 40px;
    height: 50px;
    margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
  }
  .pdp-qty {
    font-size: ${(props) => props.theme.fonts.fontSize.anchor.medium}px;
    color: ${(props) => props.theme.colors.PRIMARY.BLUE};
  }
  .itemContainer {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    display: flex;
  }

  .totalItems {
    background-color: ${(props) => props.theme.colorPalette.blue.C900};
    border-radius: 22px;
    padding: ${(props) => props.theme.spacing.ELEM_SPACING.XXS}
      ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    position: absolute;
  }

  .order-item-detail {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      font-size: ${(props) => props.theme.typography.fontSizes.fs12};
    }
  }
  .selected-item {
    .selected-tick-mark {
      display: inline-block;
    }
  }
  .tick-mark-wrap {
    display: none;
  }
  .tick-mark {
    position: absolute;
    right: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  }
`;

export default styles;
