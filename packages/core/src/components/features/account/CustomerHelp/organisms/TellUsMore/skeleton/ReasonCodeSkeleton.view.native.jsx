// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import PropTypes from 'prop-types';
import LoaderSkelton from '@tcp/core/src/components/common/molecules/LoaderSkelton/views/SkeletonLine.view.native';
import SkeletonContainer from '../styles/ReasonCodeSkeleton.style.native';

const ReasonCodeSkeleton = () => {
  return (
    <>
      <SkeletonContainer>
        <LoaderSkelton width="100%" height="58px" borderRadius="8px" />
      </SkeletonContainer>
      <SkeletonContainer>
        <LoaderSkelton width="100%" height="58px" borderRadius="8px" />
      </SkeletonContainer>
      <SkeletonContainer>
        <LoaderSkelton width="100%" height="58px" borderRadius="8px" />
      </SkeletonContainer>
    </>
  );
};

ReasonCodeSkeleton.propTypes = {
  orderDetailsData: PropTypes.shape({}).isRequired,
  selectedReasonCode: PropTypes.shape({}).isRequired,
  labels: PropTypes.shape({}).isRequired,
};

ReasonCodeSkeleton.defaultProps = {};

export default ReasonCodeSkeleton;
