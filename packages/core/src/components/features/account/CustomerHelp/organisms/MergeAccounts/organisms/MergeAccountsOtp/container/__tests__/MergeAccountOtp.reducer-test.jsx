import { fromJS, Map } from 'immutable';
import mergeAccountOtpReducer from '../MergeAccountOtp.reducer';
import { MERGE_ACCOUNT_OTP_CONSTANTS } from '../../MergeAccountsOtp.constants';

describe('MergeAccountOTP Reducer', () => {
  const initialState = fromJS({
    otpDetail: null,
    getOtpSuccess: null,
    error: null,
    uniqueKey: null,
  });
  it('should return null as a default state', () => {
    expect(mergeAccountOtpReducer(undefined, {}).get('uniqueKey')).toBeNull();
  });

  it('should reset  otp data', () => {
    const payload = {};
    expect(
      mergeAccountOtpReducer(initialState, {
        type: MERGE_ACCOUNT_OTP_CONSTANTS.MERGE_ACCOUNT_CLEAR_OTP_DATA,
        payload,
      })
    ).toEqual(Map(initialState));
  });

  it('should give unique key', () => {
    const payload = '1630590130074241055';
    expect(
      mergeAccountOtpReducer(initialState, {
        type: MERGE_ACCOUNT_OTP_CONSTANTS.MERGE_ACCOUNT_SET_UNIQUE_KEY,
        payload,
      })
    ).toEqual(
      Map({
        otpDetail: null,
        uniqueKey: payload,
        getOtpSuccess: null,
        error: null,
      })
    );
  });

  it('should set otp success', () => {
    const payload = fromJS({
      status: 'SUCCESS',
      timestamp: '2021-09-15T17:04:03.292228',
      data: '1631725443271543005',
    });
    expect(
      mergeAccountOtpReducer(initialState, {
        type: MERGE_ACCOUNT_OTP_CONSTANTS.MERGE_ACCOUNT_GET_OTP_SUCCESS,
        payload,
      })
    ).toEqual(
      Map({
        otpDetail: null,
        uniqueKey: null,
        getOtpSuccess: payload,
        error: null,
      })
    );
  });

  it('should set otp detail', () => {
    const payload = fromJS({
      data: {
        message: 'OTP validation is successful',
        success: true,
      },
      status: 'SUCCESS',
      timestamp: '2021-09-15T17:28:13.992615',
    });
    expect(
      mergeAccountOtpReducer(initialState, {
        type: MERGE_ACCOUNT_OTP_CONSTANTS.MERGE_ACCOUNT_OTP_DETAIL,
        payload,
      })
    ).toEqual(
      Map({
        otpDetail: payload,
        uniqueKey: null,
        getOtpSuccess: null,
        error: null,
      })
    );
  });
});
