// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  .initiate-return-continue-btn {
    margin: 0 auto;
    display: table;
  }
`;

export default styles;
