/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import differenceInHours from 'date-fns/differenceInHours';
import { BodyCopy, Image, Button, Anchor } from '@tcp/core/src/components/common/atoms';
import { getLabelValue } from '@tcp/core/src/utils';
import { getRefundedShipmentStatus } from '@tcp/core/src/components/features/account/CustomerHelp/util/utility';
import ModuleX from '@tcp/core/src/components/common/molecules/ModuleX';
import CustomerServiceHelp from '../../CustomerServiceHelp';
import CustomerSomethingElse from '../../CustomerSomethingElse';
import {
  BodyCopyWithSpacing,
  ViewWithSpacing,
} from '../../../../../../../../common/atoms/styledWrapper';
import {
  ModuleXView,
  ModifyOrderArea,
  ModuleXContainer,
  ResolutionItem,
  ResolutionItemButton,
  HeadingContainer,
  CustomButton,
  ButtonContainer,
  ResolutionContentArea,
  IconContainerShipping,
  ReviewButton,
  ResolutionModuleXView,
} from '../styles/Resolution.style.native';

import FullBleedView from '../../OrderCards/styles/OrderCards.style.native';
import {
  missingOrWrongItemsResolutionConst,
  cancelNModifyResolutionConst,
  delayedResolutionConst,
  wrongItemsResolutionConst,
  CUSTOMER_HELP_PAGE_TEMPLATE,
  REASON_CODES,
  shipmentResolutionConst,
  SHIPMENT_METHODS,
} from '../../../../../CustomerHelp.constants';

import { TellusMoreContext } from '../../../views/TellUsMore.view.native';

const infoIcon = require('../../../../../../../../../../../mobileapp/src/assets/images/info-icon.png');

const { refundItems, resendItemWithFreeShipping, connectWithCustomerSpecialist } =
  missingOrWrongItemsResolutionConst;

const { refundShippingFee } = delayedResolutionConst;

const { refundShipment, resendShipmentWithFreeShipping } = shipmentResolutionConst;

const { cancelOrder, modifyOrder } = cancelNModifyResolutionConst;
const { somethingElse, receiveDifferentItem, receiveWrongColorItem, receiveWrongSizeItem } =
  wrongItemsResolutionConst;

const isWrongItemResolution = (resolution) => {
  return (
    resolution === wrongItemsResolutionConst.somethingElse ||
    resolution === wrongItemsResolutionConst.receiveDifferentItem ||
    resolution === wrongItemsResolutionConst.receiveWrongColorItem ||
    resolution === wrongItemsResolutionConst.receiveWrongSizeItem
  );
};
const handleWrongItemUseCase = (labels, others, smoothScrolling) => {
  const { selectedResolution, setWrongItemData, wrongItemData } = others;

  switch (selectedResolution) {
    case somethingElse:
    case receiveDifferentItem:
    case receiveWrongColorItem:
    case receiveWrongSizeItem:
      return (
        <CustomerSomethingElse
          smoothScrolling={smoothScrolling}
          setWrongItemData={setWrongItemData}
          wrongItemData={wrongItemData}
          labels={labels}
          isSomethingElse={selectedResolution === somethingElse}
        />
      );

    default:
      return null;
  }
};

const refundEligibleReasons = [
  REASON_CODES.TRACKING_NUMBER_SHOW_IN_TRANSIT_FOR_LONG_TIME,
  REASON_CODES.PACKAGE_DELIVERED_BUT_NOT_RECEIVED,
  REASON_CODES.PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME,
];

const getContinueButton = (selectedResolution) => {
  switch (selectedResolution) {
    case refundItems:
    case cancelOrder:
    case refundShippingFee:
    case refundShipment:
      return selectedResolution;
    default:
      return '';
  }
};

const getShippingMethodVerified = ({ action, shippingForm }) => {
  const { PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME } = REASON_CODES;
  const { STANDARD, GROUND } = SHIPMENT_METHODS;
  return (
    action === PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME &&
    shippingForm === STANDARD &&
    shippingForm === GROUND
  );
};

const paidShippingRefundNonEligility = ({
  action,
  shippingForm,
  isPaidNExpediteRefundEnableApp,
  isDeliveredShipmentAppeased,
  isOrderStatusTransit,
}) => {
  const { PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME } = REASON_CODES;
  const { STANDARD } = SHIPMENT_METHODS;
  const nonStandardShipment =
    action === PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME && shippingForm !== STANDARD;
  return (
    (nonStandardShipment && isPaidNExpediteRefundEnableApp === false) ||
    (nonStandardShipment && isDeliveredShipmentAppeased) ||
    (action === PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME &&
      isOrderStatusTransit !== null &&
      isOrderStatusTransit)
  );
};

const getConditionForNonRefund = ({
  action,
  refundEligibility,
  isOrderStatusTransit,
  hoursDifference,
  shippingForm,
  isPaidNExpediteRefundEnableApp,
  isDeliveredShipmentAppeased,
}) => {
  const { PACKAGE_DELIVERED_BUT_NOT_RECEIVED } = REASON_CODES;
  return (
    (action !== PACKAGE_DELIVERED_BUT_NOT_RECEIVED &&
      refundEligibleReasons.includes(action) &&
      refundEligibility &&
      refundEligibility.toLowerCase() === 'false' &&
      !isOrderStatusTransit) ||
    (action === PACKAGE_DELIVERED_BUT_NOT_RECEIVED && hoursDifference <= 48) ||
    paidShippingRefundNonEligility({
      action,
      shippingForm,
      isPaidNExpediteRefundEnableApp,
      isDeliveredShipmentAppeased,
      isOrderStatusTransit,
    }) ||
    getShippingMethodVerified({ action, shippingForm })
  );
};

const checkNonRefundEligibility = ({
  refundData,
  action,
  isOrderStatusTransit,
  orderDelivereyDate,
  shippingMethod,
  isPaidNExpediteRefundEnableApp,
  isDeliveredShipmentAppeased,
}) => {
  const shippingForm = shippingMethod && shippingMethod.toLowerCase();
  const refundEligibility = refundData && refundData.refundEligible;
  let hoursDifference;
  const daysSla = orderDelivereyDate ? new Date(orderDelivereyDate) : null;
  if (daysSla) {
    hoursDifference = differenceInHours(new Date(), daysSla);
  }
  if (
    getConditionForNonRefund({
      action,
      refundEligibility,
      isOrderStatusTransit,
      hoursDifference,
      shippingForm,
      isPaidNExpediteRefundEnableApp,
      isDeliveredShipmentAppeased,
    })
  ) {
    return true;
  }
  return false;
};

export const SelectedResolutionAction = ({ labels, others, smoothScrolling }) => {
  SelectedResolutionAction.propTypes = {
    others: PropTypes.shape({}).isRequired,
    labels: PropTypes.shape({}).isRequired,
  };
  const {
    selectedResolution,
    resolutionModuleXContent,
    selectedReasonCode,
    orderDetailsData,
    hideButton,
    selectedOrderItems,
    isLiveChatEnabled,
    navigation,
    isLoggedIn,
    trackAnalyticsClick,
  } = others;
  const continueButton = getContinueButton(selectedResolution);
  const tellUsMoreContext = useContext(TellusMoreContext);

  switch (selectedResolution) {
    case continueButton:
      return (
        !hideButton && (
          <ButtonContainer spacingStyles="margin-top-XXL">
            <CustomButton
              onPress={() => navigation.navigate(CUSTOMER_HELP_PAGE_TEMPLATE.REVIEW)}
              fill="BLUE"
              type="button"
              fontSize="fs16"
              text={getLabelValue(labels, 'lbl_help_refund_action_btn')}
            />
          </ButtonContainer>
        )
      );
    case resendItemWithFreeShipping:
    case resendShipmentWithFreeShipping:
      return (
        <ViewWithSpacing spacingStyles="margin-top-MED">
          <CustomerServiceHelp
            navigation={navigation}
            labels={labels}
            helpMessage={getLabelValue(labels, 'lbl_resend_with_free_shipping_help')}
            selectedReasonCode={selectedReasonCode}
            orderDetailsData={orderDetailsData}
            selectedOrderItems={selectedOrderItems}
            isLiveChatEnabled={isLiveChatEnabled}
            isUserLoggedIn={isLoggedIn}
            trackAnalyticsClick={trackAnalyticsClick}
          />
        </ViewWithSpacing>
      );
    case modifyOrder:
      return (
        <ModuleXView>
          <ModifyOrderArea ref={tellUsMoreContext.step7}>
            <Image width="20px" height="20px" source={infoIcon} alt="info" />
            {Object.keys(resolutionModuleXContent).length > 0 ? (
              <ModuleXContainer spacingStyles="margin-left-SM">
                <ModuleX richTextList={[{ text: resolutionModuleXContent.richText }]} />
              </ModuleXContainer>
            ) : (
              <ModuleXContainer />
            )}
          </ModifyOrderArea>
        </ModuleXView>
      );

    case connectWithCustomerSpecialist:
      return (
        <ViewWithSpacing spacingStyles="margin-top-MED">
          <CustomerServiceHelp
            navigation={navigation}
            labels={labels}
            helpMessage={getLabelValue(labels, 'lbl_connect_with_customer_service_specialist_help')}
            selectedReasonCode={selectedReasonCode}
            orderDetailsData={orderDetailsData}
            selectedOrderItems={selectedOrderItems}
            isLiveChatEnabled={isLiveChatEnabled}
            isUserLoggedIn={isLoggedIn}
            trackAnalyticsClick={trackAnalyticsClick}
          />
        </ViewWithSpacing>
      );
    default:
      return handleWrongItemUseCase(labels, others, smoothScrolling);
  }
};

const SelectedOptionsHelpMessage = ({ selectedOption, labels }) => {
  SelectedOptionsHelpMessage.propTypes = {
    selectedOption: PropTypes.shape({}).isRequired,
    labels: PropTypes.shape({}).isRequired,
  };
  let label;
  if (selectedOption) {
    if (selectedOption === refundItems) {
      label = getLabelValue(labels, 'lbl_refund_items_help');
    } else if (selectedOption === cancelOrder) {
      label = getLabelValue(labels, 'lbl_cancel_item_help');
    } else if (selectedOption === refundShippingFee) {
      label = getLabelValue(labels, 'lbl_refund_shipping_fee_msg');
    } else if (selectedOption === refundShipment) {
      label = getLabelValue(labels, 'lbl_refund_shipment_help');
    }
  }

  return (
    !!label && (
      <BodyCopyWithSpacing
        spacingStyles="margin-top-MED margin-left-SM margin-right-SM margin-bottom-XXL"
        fontWeight="semibold"
        fontSize="fs14"
        fontFamily="secondary"
        color="black"
        text={label}
      />
    )
  );
};

const getDeliveredHeading = ({ labels, nonRefundable, selectedShipmentCard }) => {
  if (!nonRefundable && selectedShipmentCard) {
    return getLabelValue(labels, 'lbl_how_to_solve', '');
  }
  return null;
};

const enableHelpTextHeadingPaidNExpedite = ({
  nonRefundable,
  isOrderStatusTransit,
  shippingMethod,
}) => {
  const { STANDARD, GROUND } = SHIPMENT_METHODS;
  const shippingForm = shippingMethod && shippingMethod.toLowerCase();
  const noExpressDelivery = shippingForm === STANDARD || shippingForm === GROUND;
  if (nonRefundable || isOrderStatusTransit || noExpressDelivery) {
    return false;
  }
  return true;
};

const getWrongItemHeading = ({ isWrongItems, labels }) => {
  if (isWrongItems) {
    return getLabelValue(labels, 'lbl_wrong_item_help', '');
  }
  return getLabelValue(labels, 'lbl_how_to_solve', '');
};

const ResolutionHeading = ({
  action,
  labels,
  selectedShipmentCard,
  resolutionList,
  isSomethingElse,
  nonRefundable,
  isOrderStatusTransit,
  shippingMethod,
  isWrongItems,
  stepId,
}) => {
  ResolutionHeading.propTypes = {
    selectedOption: PropTypes.shape({}).isRequired,
    labels: PropTypes.shape({}).isRequired,
    selectedShipmentCard: PropTypes.string.isRequired,
    resolutionList: PropTypes.shape({}).isRequired,
    isSomethingElse: PropTypes.bool.isRequired,
    action: PropTypes.string.isRequired,
    nonRefundable: PropTypes.string.isRequired,
    isOrderStatusTransit: PropTypes.string.isRequired,
    shippingMethod: PropTypes.string.isRequired,
    isWrongItems: PropTypes.string.isRequired,
  };

  const {
    CANCEL_MODIFY,
    PACKAGE_DELIVERED_BUT_NOT_RECEIVED,
    TRACKING_NUMBER_SHOW_IN_TRANSIT_FOR_LONG_TIME,
    PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME,
  } = REASON_CODES;
  let label = '';
  const tellUsMoreContext = useContext(TellusMoreContext);

  switch (action) {
    case CANCEL_MODIFY:
      label = getLabelValue(labels, 'lbl_select_an_option_title', '');
      break;
    case TRACKING_NUMBER_SHOW_IN_TRANSIT_FOR_LONG_TIME: {
      label = selectedShipmentCard ? getLabelValue(labels, 'lbl_how_to_solve', '') : null;
      break;
    }
    case PACKAGE_DELIVERED_BUT_NOT_RECEIVED: {
      label = getDeliveredHeading({ labels, nonRefundable, selectedShipmentCard });
      break;
    }
    case PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME:
      label = enableHelpTextHeadingPaidNExpedite({
        nonRefundable,
        isOrderStatusTransit,
        shippingMethod,
      })
        ? getLabelValue(labels, 'lbl_how_to_solve', '')
        : '';
      break;

    default:
      label = getWrongItemHeading({ isWrongItems, labels });
      break;
  }

  if (isSomethingElse) {
    label = getLabelValue(labels, 'lbl_connect_directly_specialist', '');
  }

  const id = isSomethingElse ? tellUsMoreContext.step4 : stepId;

  return resolutionList ? (
    <HeadingContainer ref={id} isSomethingElse={isSomethingElse}>
      <BodyCopy fontFamily="secondary" fontSize="fs14" text={label} />
      {isSomethingElse ? (
        <BodyCopy
          fontFamily="secondary"
          fontSize="fs14"
          text={getLabelValue(labels, 'lbl_select_an_option', '')}
        />
      ) : null}
    </HeadingContainer>
  ) : null;
};

const renderSelectedResolution = (labels, others, smoothScrolling) => {
  const {
    selectedWrongItemResolution,
    selectedResolution,
    selectedReasonCode,
    resolutionList,
    orderDetailsData,
    selectedOrderItems,
    isLiveChatEnabled,
    navigation,
    isLoggedIn,
    trackAnalyticsClick,
  } = others;

  const { action } = selectedReasonCode || {};
  const isSomethingElse = action === REASON_CODES.SOMETHING_ELSE;
  const tellUsMoreContext = useContext(TellusMoreContext);

  return (
    <View ref={tellUsMoreContext.step7}>
      {selectedResolution || (selectedWrongItemResolution && !isSomethingElse) ? (
        <>
          <SelectedOptionsHelpMessage selectedOption={selectedResolution} labels={labels} />
          <SelectedResolutionAction
            labels={labels}
            others={others}
            smoothScrolling={smoothScrolling}
          />
        </>
      ) : (
        <>
          {isSomethingElse && resolutionList && (
            <CustomerServiceHelp
              navigation={navigation}
              labels={labels}
              isSomethingElse
              selectedReasonCode={selectedReasonCode}
              orderDetailsData={orderDetailsData}
              selectedOrderItems={selectedOrderItems}
              isLiveChatEnabled={isLiveChatEnabled}
              isUserLoggedIn={isLoggedIn}
              trackAnalyticsClick={trackAnalyticsClick}
            />
          )}
        </>
      )}
    </View>
  );
};

const renderAllResolution = (
  resolutionList,
  isSomethingElse,
  selectedResolution,
  selectedWrongItemResolution,
  handleSelection,
  isOrderStatusTransit,
  ...others
) => {
  const [labels, nonRefundable] = others;
  if (resolutionList && !isSomethingElse && !nonRefundable) {
    return (
      <View>
        {Object.keys(resolutionList).map((resolution) => (
          <ViewWithSpacing spacingStyles="margin-left-SM margin-right-SM margin-top-MED">
            <ResolutionItemButton
              onPress={() => handleSelection(resolution)}
              id={resolution}
              accessibilityRole="button"
              isSelected={
                selectedResolution === resolution || selectedWrongItemResolution === resolution
              }
            >
              <ResolutionItem text={labels && labels[resolutionList[resolution]]} />
            </ResolutionItemButton>
          </ViewWithSpacing>
        ))}
      </View>
    );
  }
  return null;
};

const getPaidShippingHelpText = (
  labels,
  action,
  refundData,
  shippingMethod,
  isOrderStatusTransit
) => {
  const { STANDARD, GROUND } = SHIPMENT_METHODS;
  const shippingForm = shippingMethod && shippingMethod.toLowerCase();
  const noExpressDelivery = shippingForm === STANDARD || shippingForm === GROUND;
  if (noExpressDelivery) {
    return getLabelValue(labels, 'lbl_no_express_selected', '');
  }
  return isOrderStatusTransit ? getLabelValue(labels, 'lbl_order_must_be_delivered', '') : '';
};

const getNoExpressDeliveryVerified = ({
  noExpressDelivery,
  isDeliveredShipmentAppeased,
  action,
}) => {
  const { PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME } = REASON_CODES;
  return (
    (noExpressDelivery && action === PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME) ||
    (isDeliveredShipmentAppeased && action === PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME)
  );
};

const getPaidShippingUnsuccessFull = ({
  isOrderStatusTransit,
  selectedReasonCode,
  nonRefundable,
  action,
  shippingForm,
  isDeliveredShipmentAppeased,
}) => {
  const { STANDARD, GROUND } = SHIPMENT_METHODS;
  const { PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME } = REASON_CODES;
  const noExpressDelivery = shippingForm === STANDARD || shippingForm === GROUND;
  if (
    (isOrderStatusTransit &&
      selectedReasonCode &&
      action === PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME) ||
    (nonRefundable && action === PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME) ||
    getNoExpressDeliveryVerified({ noExpressDelivery, isDeliveredShipmentAppeased, action })
  ) {
    return true;
  }
  return false;
};

const renderPaidShippingHelp = (
  labels,
  orderLabels,
  isOrderStatusTransit,
  selectOrderAction,
  selectedReasonCode,
  refundData,
  ...others
) => {
  const [shippingMethod, nonRefundable, navigation, isDeliveredShipmentAppeased] = others;
  const { STANDARD, GROUND } = SHIPMENT_METHODS;

  const { action } = selectedReasonCode || {};
  const shippingForm = shippingMethod && shippingMethod.toLowerCase();
  const infoIconCondition = shippingForm === STANDARD || isOrderStatusTransit;
  const noExpressDelivery = shippingForm === STANDARD || shippingForm === GROUND;
  const unSuccessFull = getPaidShippingUnsuccessFull({
    isOrderStatusTransit,
    selectedReasonCode,
    nonRefundable,
    action,
    shippingForm,
    isDeliveredShipmentAppeased,
  });
  const tellUsMoreContext = useContext(TellusMoreContext);
  if (unSuccessFull) {
    return (
      <ResolutionContentArea>
        <HeadingContainer ref={tellUsMoreContext.step5} isSomethingElse={true}>
          <BodyCopyWithSpacing
            fontFamily="secondary"
            fontSize="fs16"
            fontWeight="bold"
            spacingStyles="margin-left-MED"
            text={getLabelValue(labels, 'lbl_how_to_solve', '')}
          />
        </HeadingContainer>
        <View>
          <FullBleedView>
            {infoIconCondition && (
              <IconContainerShipping width="24px" height="24px" source={infoIcon} alt="info" />
            )}
            <BodyCopyWithSpacing
              fontFamily="secondary"
              fontSize="fs16"
              spacingStyles="margin-top-XS margin-left-XXXL margin-right-SM"
              text={getPaidShippingHelpText(
                labels,
                action,
                refundData,
                shippingMethod,
                isOrderStatusTransit
              )}
            />
          </FullBleedView>
          {(noExpressDelivery || !isOrderStatusTransit) && (
            <CustomerServiceHelp labels={labels} navigation={navigation} />
          )}
          <FullBleedView centered>
            <ReviewButton>
              <ViewWithSpacing>
                <ButtonContainer spacingStyles="margin-top-XXXS">
                  <Button
                    fontSize="fs14"
                    fontWeight="extrabold"
                    buttonVariation="fixed-width"
                    fill="WHITE"
                    onPress={() => selectOrderAction({ navigation })}
                    text={getLabelValue(orderLabels.orders, 'lbl_help_another_order')}
                  />
                </ButtonContainer>
              </ViewWithSpacing>
            </ReviewButton>
          </FullBleedView>
        </View>
      </ResolutionContentArea>
    );
  }
  return null;
};

const getResolutionUpperContent = (selectedOption, labels, expectedDeliveryDate) => {
  switch (selectedOption) {
    case REASON_CODES.CANCEL_MODIFY:
      return (
        <BodyCopy
          fontWeight="bold"
          fontFamily="secondary"
          fontSize="fs16"
          text={getLabelValue(labels, 'lbl_select_an_option_title', '')}
        />
      );
    case REASON_CODES.TRACKING_NUMBER_SHOW_IN_TRANSIT_FOR_LONG_TIME: {
      return (
        <>
          <BodyCopy
            fontWeight="bold"
            fontFamily="secondary"
            fontSize="fs16"
            text={getLabelValue(labels, 'lbl_resolution_transit_long_time', '')}
          />
          {expectedDeliveryDate && (
            <BodyCopy
              fontWeight="bold"
              fontFamily="secondary"
              fontSize="fs16"
              text={expectedDeliveryDate}
              spacingStyles="elem-ml-XXS"
            />
          )}
        </>
      );
    }
    case REASON_CODES.PACKAGE_DELIVERED_BUT_NOT_RECEIVED: {
      return (
        <BodyCopy
          fontWeight="bold"
          fontFamily="secondary"
          fontSize="fs16"
          text={getLabelValue(labels, 'lbl_resolution_package_delivered_not_received', '')}
        />
      );
    }
    default:
      return (
        <BodyCopy
          fontWeight="bold"
          fontFamily="secondary"
          fontSize="fs16"
          text={getLabelValue(labels, 'lbl_how_to_solve', '')}
        />
      );
  }
};

const renderResolutionContent = ({
  selectedShipmentCard,
  nonRefundable,
  labels,
  transitHelpModuleXContent,
  selectedReasonCode,
  expectedDeliveryDate,
  selectOrderAction,
  orderLabels,
  navigation,
}) => {
  renderResolutionContent.propTypes = {
    selectedShipmentCard: PropTypes.string,
    nonRefundable: PropTypes.bool.isRequired,
    labels: PropTypes.shape({}).isRequired,
    transitHelpModuleXContent: PropTypes.shape({}),
    selectedReasonCode: PropTypes.shape({}),
    expectedDeliveryDate: PropTypes.string.isRequired,
    selectOrderAction: PropTypes.func,
    orderLabels: PropTypes.shape({}).isRequired,
    navigation: PropTypes.shape({}).isRequired,
  };

  renderResolutionContent.defaultProps = {
    selectedReasonCode: {},
    transitHelpModuleXContent: {},
    selectedShipmentCard: '',
    selectOrderAction: () => {},
  };

  if (selectedShipmentCard && nonRefundable) {
    const tellUsMoreContext = useContext(TellusMoreContext);
    return (
      <ResolutionModuleXView>
        <ResolutionContentArea ref={tellUsMoreContext.step7}>
          <BodyCopyWithSpacing
            fontFamily="secondary"
            fontSize="fs16"
            fontWeight="bold"
            spacingStyles="margin-left-SM"
            text={getLabelValue(labels, 'lbl_how_to_solve', '')}
          />

          <FullBleedView>
            <ViewWithSpacing>
              <IconContainerShipping width="24px" height="24px" source={infoIcon} alt="info" />
            </ViewWithSpacing>

            <ViewWithSpacing spacingStyles="margin-top-XS margin-left-XXXL margin-right-SM">
              {getResolutionUpperContent(selectedReasonCode.action, labels, expectedDeliveryDate)}
            </ViewWithSpacing>

            <ViewWithSpacing spacingStyles="margin-top-MED margin-left-XXXL margin-right-SM">
              {Object.keys(transitHelpModuleXContent).length > 0 && (
                <ModuleX richTextList={[{ text: transitHelpModuleXContent.richText }]} />
              )}
            </ViewWithSpacing>

            <ViewWithSpacing spacingStyles="margin-left-XXXL">
              <BodyCopyWithSpacing
                spacingStyles="margin-left-XXL"
                text={getLabelValue(labels, 'lbl_further_assistance', '')}
              />

              <Anchor
                text={getLabelValue(labels, 'lbl_contact_customer_service', '')}
                onPress={() => {}}
                underlineBlue
              />
            </ViewWithSpacing>
          </FullBleedView>

          <FullBleedView centered>
            <ReviewButton>
              <ViewWithSpacing>
                <ButtonContainer spacingStyles="margin-top-XS">
                  <Button
                    buttonVariation="variable-width"
                    fill="WHITE"
                    onPress={() => selectOrderAction({ navigation })}
                    text={getLabelValue(orderLabels.orders, 'lbl_help_another_order')}
                  />
                </ButtonContainer>
              </ViewWithSpacing>
            </ReviewButton>
          </FullBleedView>
        </ResolutionContentArea>
      </ResolutionModuleXView>
    );
  }

  return null;
};

const Resolution = ({ labels, className, ...others }) => {
  const {
    resolutionList,
    setResolution,
    selectedResolution,
    selectedReasonCode,
    transitHelpModuleXContent,
    selectedShipmentCard,
    refundData,
    selectOrderAction,
    selectedWrongItemResolution,
    isOrderStatusTransit,
    orderLabels,
    orderDetailsData,
    setWrongItemData,
    orderDelivereyDate,
    isPaidNExpediteRefundEnableApp,
    navigation,
    clickAnalyticsData = {},
    wrongItemResolution,
    trackAnalyticsClick,
    smoothScrolling,
    stepId,
  } = others;
  let step1 = '';
  let clickPath = '';
  const { action } = selectedReasonCode || {};
  const { shippingMethod, orderItems = [] } = orderDetailsData || {};

  const { expectedDeliveryDate } = refundData || {};

  const isDeliveredShipmentAppeased = getRefundedShipmentStatus({ orderItems });
  const handleSelection = (resolutionText) => {
    const selectedResolutionName = resolutionText;
    if (!wrongItemResolution) {
      step1 = resolutionText;
      clickPath = `${clickAnalyticsData?.orderHelpSelectedReason}|${resolutionText}`;
    } else {
      step1 = clickAnalyticsData?.orderHelpSelectedResolution;
      clickPath = `${clickAnalyticsData?.orderHelpSelectedReason}|${step1}|${resolutionText}`;
    }

    trackAnalyticsClick(
      {
        orderHelpSelectedReason: clickAnalyticsData?.orderHelpSelectedReason,
        orderHelpSelectedResolution: step1,
        orderHelp_path_cd128: clickPath,
      },
      { name: 'orderHelpPath_click', module: 'account' }
    );

    if (
      action === REASON_CODES.RECEIVED_WRONG_ITEM &&
      isWrongItemResolution(selectedResolutionName)
    ) {
      setWrongItemData({});
      setResolution({ selectedWrongItemResolution: selectedResolutionName });
    } else {
      setResolution({ selectedResolution: selectedResolutionName });
    }
  };
  const isSomethingElse = action === REASON_CODES.SOMETHING_ELSE;
  const isWrongItems = action === REASON_CODES.RECEIVED_WRONG_ITEM;

  const nonRefundable = checkNonRefundEligibility({
    refundData,
    action,
    isOrderStatusTransit,
    orderDelivereyDate,
    shippingMethod,
    isPaidNExpediteRefundEnableApp,
    isDeliveredShipmentAppeased,
  });
  return (
    <View>
      {!nonRefundable && (
        <ViewWithSpacing>
          <ResolutionHeading
            action={action}
            labels={labels}
            selectedShipmentCard={selectedShipmentCard}
            resolutionList={resolutionList}
            isSomethingElse={isSomethingElse}
            nonRefundable={nonRefundable}
            isOrderStatusTransit={isOrderStatusTransit}
            shippingMethod={shippingMethod}
            isWrongItems={isWrongItems}
            stepId={stepId}
          />
        </ViewWithSpacing>
      )}

      {renderResolutionContent({
        selectedShipmentCard,
        nonRefundable,
        labels,
        transitHelpModuleXContent,
        selectedReasonCode,
        expectedDeliveryDate,
        selectOrderAction,
        orderLabels,
        navigation,
      })}

      {renderAllResolution(
        resolutionList,
        isSomethingElse,
        selectedResolution,
        selectedWrongItemResolution,
        handleSelection,
        isOrderStatusTransit,
        labels,
        nonRefundable
      )}

      {renderPaidShippingHelp(
        labels,
        orderLabels,
        isOrderStatusTransit,
        selectOrderAction,
        selectedReasonCode,
        refundData,
        shippingMethod,
        nonRefundable,
        navigation,
        isDeliveredShipmentAppeased
      )}

      {renderSelectedResolution(labels, others, smoothScrolling)}
    </View>
  );
};

Resolution.propTypes = {
  orderDetailsData: PropTypes.shape({}).isRequired,
  selectedReasonCode: PropTypes.shape({}),
  labels: PropTypes.shape({}).isRequired,
  className: PropTypes.string,
  resolutionList: PropTypes.shape({}),
  setResolution: PropTypes.func.isRequired,
  selectedResolution: PropTypes.string,
  resolutionModuleXContent: PropTypes.shape({}),
  transitHelpModuleXContent: PropTypes.shape({}),
  selectedShipmentCard: PropTypes.string,
  refundData: PropTypes.shape({}),
  selectOrderAction: PropTypes.func,
  isLiveChatEnabled: PropTypes.bool.isRequired,
  isLoggedIn: PropTypes.bool.isRequired,
  trackAnalyticsClick: PropTypes.func.isRequired,
};

Resolution.defaultProps = {
  className: '',
  resolutionList: {},
  selectedResolution: '',
  resolutionModuleXContent: {},
  selectedReasonCode: {},
  transitHelpModuleXContent: {},
  selectedShipmentCard: '',
  refundData: {},
  selectOrderAction: () => {},
};
export default React.memo(Resolution);
export { Resolution as ResolutionVanilla };
