import React from 'react';
import PropTypes from 'prop-types';
import { BodyCopy, Image, Anchor } from '../../../../../../../../../../../../common/atoms';
import withStyles from '../../../../../../../../../../../../common/hoc/withStyles';
import styles from '../styles/ReturnMethods.style';
import { getIconPath } from '../../../../../../../../../../../../../utils';

const ReturnMethods = ({ className, item, setReturnMethod }) => {
  const onMethodClicked = (itemSelected) => {
    setReturnMethod(itemSelected?.key);
  };

  return (
    <BodyCopy
      component="div"
      className={`${className} container elem-mr-MED_1`}
      onClick={() => onMethodClicked(item)}
    >
      <BodyCopy component="div" className="internal-container">
        <BodyCopy component="div" className="elem-mb-SM img-container">
          <Image
            alt="close"
            className="icon"
            src={getIconPath(item.iconPath)}
            height="26px"
            width="30px"
          />
        </BodyCopy>
        <BodyCopy component="div" className="inline-item">
          <BodyCopy
            component="p"
            fontWeight="bold"
            fontFamily="secondary"
            fontSize="fs14"
            className="elem-mb-XS_6"
          >
            {item.returnText}
            <br />
            {item.returnTextCost}
          </BodyCopy>

          <BodyCopy
            component="p"
            fontWeight="semibold"
            fontFamily="secondary"
            fontSize="fs12"
            className="elem-mr-SM elem-mb-SM"
          >
            {item.returnDesc}
          </BodyCopy>
          <BodyCopy component="div" className="short-desc-container">
            {item?.shortDesc?.shortDescIcon && (
              <Image
                alt="close"
                className="elem-mr-XXS"
                src={getIconPath(item?.shortDesc?.shortDescIcon)}
                height="18px"
                width="16px"
              />
            )}
            <Anchor
              to="/store-locator"
              target="_blank"
              anchorVariation="primary"
              fontFamily="secondary"
              fontWeight="bold"
              fontSizeVariation="medium"
              className="shortdescText"
              dataLocator="navigationToStoreLocator"
            >
              {item?.shortDesc?.shortDescText}
            </Anchor>
          </BodyCopy>
        </BodyCopy>
      </BodyCopy>
    </BodyCopy>
  );
};

ReturnMethods.propTypes = {
  className: PropTypes.string,
  item: PropTypes.shape({
    iconPath: PropTypes.string,
    returnText: PropTypes.string,
    returnDesc: PropTypes.string,
    shortDesc: PropTypes.shape({
      shortDescIcon: PropTypes.string,
      shortDescText: PropTypes.string,
    }),
  }).isRequired,
  setReturnMethod: PropTypes.func.isRequired,
};

ReturnMethods.defaultProps = {
  className: '',
};

export default withStyles(ReturnMethods, styles);
export { ReturnMethods as ReturnMethodsVanilla };
