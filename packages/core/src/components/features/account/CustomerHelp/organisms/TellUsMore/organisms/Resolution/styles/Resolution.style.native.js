// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';
import { BodyCopy, Button, Image } from '@tcp/core/src/components/common/atoms';
import { ViewWithSpacing } from '../../../../../../../../common/atoms/styledWrapper';

export const ModifyOrderArea = styled.View`
  flex-direction: row;
  padding: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  background-color: ${(props) => props.theme.colors.PRIMARY.PALEGRAY};
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
`;

export const ModuleXContainer = styled(ViewWithSpacing)`
  width: 100%;
`;

export const ModuleXView = styled.View`
  flex: 1;
  min-height: 300px;
`;

export const ResolutionModuleXView = styled.View`
  flex: 1;
  min-height: 450px;
`;

export const ResolutionUpperContentContainer = styled.Text`
  font-size: ${(props) => props.theme.typography.fontSizes.fs16};
  font-family: ${(props) => props.theme.typography.fonts.secondary};
  font-weight: ${(props) => props.theme.fonts.fontWeight.bold};
`;

export const ResolutionItemButton = styled.TouchableOpacity`
  justify-content: center;
  display: flex;
  border-radius: 8px;
  border: ${(props) =>
    props.isSelected
      ? `solid 2px ${props.theme.colorPalette.blue[800]}`
      : `solid 1px ${props.theme.colorPalette.gray[600]}`};
  height: 59px;
`;

export const ResolutionItem = styled(BodyCopy)`
  color: ${(props) => props.theme.colors.TEXT.DARKGRAY};
  font-size: ${(props) => props.theme.typography.fontSizes.fs14};
  text-align: left;
  font-family: ${(props) => props.theme.typography.fonts.secondary};
  font-weight: ${(props) => props.theme.fonts.fontWeight.semiBold};
  padding-left: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
`;

export const HeadingContainer = styled.View`
  background-color: ${(props) =>
    props.isSomethingElse ? props.theme.colors.PRIMARY.PALEGRAY : props.theme.colors.WHITE};
  padding: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
`;

export const CustomButton = styled(Button)`
  width: 210px;
  text-align: center;
  font-weight: ${(props) => props.theme.fonts.fontWeight.bold};
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XXXL};
`;

export const ButtonContainer = styled(ViewWithSpacing)`
  align-items: center;
`;

export const ResolutionContentArea = styled.View`
  background-color: ${(props) => props.theme.colors.PRIMARY.PALEGRAY};
  width: 100%;
  padding: ${(props) => props.theme.spacing.ELEM_SPACING.XS} 0
    ${(props) => props.theme.spacing.ELEM_SPACING.XL} 0;
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
`;

export const IconContainerShipping = styled(Image)`
  position: absolute;
  left: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
`;

export const ReviewButton = styled.View`
  display: flex;
  justify-content: center;
  padding: 15px;
`;

export const StyledTextInput = styled.TextInput`
  height: 120px;
  width: 100%;
  border: 1px solid ${(props) => props.theme.colors.FOOTER.DIVIDER};
  border-radius: 8px;
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  padding: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
`;

export const CharacterLeft = styled(BodyCopy)`
  position: absolute;
  right: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  top: -20px;
`;

export const CharacterLeftContainer = styled.View`
  position: relative;
`;

export const ErrorIcon = styled(Image)`
  height: 14px;
  width: 16px;
`;

export const AdditionalDetailText = styled(ViewWithSpacing)`
  flex-direction: row;
  flex-wrap: wrap;
`;
