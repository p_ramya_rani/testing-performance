// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import OrderItems from '../views';

describe('OrderItems Component', () => {
  const props = {
    orderDetailsData: {
      purchasedItems: [
        {
          trackingNumber: '34234234',
          trackingUrl: '/trackingUrl',
          items: [{ name: 'name' }],
        },
      ],
      canceledItems: [],
    },
  };
  it('should render OrderItems', () => {
    const component = shallow(<OrderItems {...props} />);
    expect(component).toMatchSnapshot();
  });
});
