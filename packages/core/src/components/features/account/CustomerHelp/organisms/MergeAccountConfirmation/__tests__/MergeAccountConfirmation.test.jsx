import React from 'react';
import { shallow } from 'enzyme';
import { MergeAccountConfirmationVanilla } from '../views/MergeAccountConfirmation.view';

describe('MergeAccount Success template', () => {
  const props = {
    labels: {},
    className: '',
  };

  it('should render correctly', () => {
    const component = shallow(<MergeAccountConfirmationVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
