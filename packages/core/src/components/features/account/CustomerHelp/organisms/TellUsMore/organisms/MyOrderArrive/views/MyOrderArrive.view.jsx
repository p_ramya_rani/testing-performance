/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import {
  REASON_CODES,
  shipmentResolution,
  missingOrWrongItemsResolution,
  ORDER_ITEM_STATUS,
  CUSTOMER_HELP_ROUTES,
  ORDER_STATUS,
} from '@tcp/core/src/components/features/account/CustomerHelp/CustomerHelp.constants';
import { smoothScrolling } from '@tcp/core/src/utils';
import { Col, BodyCopy, Row, Button } from '../../../../../../../../common/atoms';
import withStyles from '../../../../../../../../common/hoc/withStyles';
import styles from '../styles/MyOrderArrive.style';
import { orderDateFormatted, getShipmentCount } from '../../../../../../../../../utils/utils';
import OrderCards from '../../OrderCards';
import { renderDelivered } from './MyOrderArrive.utils';
import {
  getLabelValue,
  navigateToOrderDetailPage,
  routerPush,
} from '../../../../../../../../../utils';
import internalEndpoints from '../../../../../../common/internalEndpoints';
import ReasonCode from '../../ReasonCode';
import Resolution from '../../Resolution';
import ReasonCodeSkeleton from '../../../skeleton';
import Separator from '../../../../../../../../common/atoms/Separator';
import CustomerServiceHelp from '../../CustomerServiceHelp';
import { getRCStatus } from '../../../../../../OrderDetails/organism/OrderDetailsWrapper/views/OrderDetailsWrapper.utils';

const getClickedTile = (filteredItems) => {
  return filteredItems[0]?.orderStatus;
};

const getItemsLength = (items) => {
  if (items) {
    let total = 0;
    items.map((item) => {
      total += item?.quantity ? item?.quantity : 1;
      return null;
    });
    return total;
  }
  return null;
};

const renderInProcess = (className, labels, isLiveChatEnabled, others) => {
  const { trackAnalyticsClick } = others;
  return (
    <BodyCopy component="div" className={`${className} parent`}>
      <BodyCopy component="div">
        <BodyCopy
          component="p"
          fontFamily="secondary"
          fontSize="fs14"
          className="processed-order-desc elem-mt-XXS elem-mb-LRG_1"
        >
          {getLabelValue(labels, 'lbl_arrive_inprocess_text')}
        </BodyCopy>
      </BodyCopy>
      <Separator className="separator-first" />
      <BodyCopy
        component="p"
        fontFamily="secondary"
        fontSize="fs16"
        fontWeight="bold"
        className="elem-mt-LRG_1"
      >
        {getLabelValue(labels, 'lbl_merge_customer_service_text')}
      </BodyCopy>
      <CustomerServiceHelp
        labels={labels}
        isLiveChatEnabled={isLiveChatEnabled}
        trackAnalyticsClick={trackAnalyticsClick}
      />
      <Separator className="separator-second" />
    </BodyCopy>
  );
};

const getReasonCodeHeading = ({ classes = '', labels }) => {
  return (
    <div className={`header-title${classes}`} id="step-3">
      <BodyCopy
        component="p"
        fontWeight="bold"
        fontFamily="secondary"
        fontSize="fs16"
        className="tell-us-more"
      >
        {getLabelValue(labels, 'lbl_tell_us_more_about_your_shipment', '')}
      </BodyCopy>
    </div>
  );
};

const getReasonCodeComponent = (
  orderLabels,
  labels,
  isLiveChatEnabled,
  others,
  inTransit = false
) => {
  const {
    reasonCodes,
    orderDetailsData,
    isLoggedIn,
    getRefundStatus,
    setOrderStatusInTransit,
    trackAnalyticsClick,
    shipmentData,
    isCouponSelfHelpEnabled,
    clearSelectedResolution,
    setOrderArriveSelectedReasonCode,
    clearOrderArriveSelectedReasonCode,
    selectedOrderArriveReasonCode,
    action,
    updateHelpstep1,
    helpstep1,
  } = others;

  if (reasonCodes?.length > 0) {
    return (
      <>
        <Row fullBleed>
          <Col colSize={{ small: 6, medium: 7, large: 12 }}>
            <div className="header-container">
              {getReasonCodeHeading({ labels })}
              {selectedOrderArriveReasonCode && (
                <Button
                  className="blue-link"
                  nohover
                  type="button"
                  link
                  underline
                  color="blue"
                  onClick={() => {
                    clearSelectedResolution();
                    clearOrderArriveSelectedReasonCode();
                  }}
                >
                  {getLabelValue(orderLabels, 'lbl_edit_option', 'orders')}
                </Button>
              )}
            </div>
          </Col>
        </Row>
        <Row fullBleed>
          <Col colSize={{ small: 6, medium: 8, large: 12 }}>
            <ReasonCode
              reasonList={reasonCodes}
              orderDetailsData={orderDetailsData}
              setSelectedReasonCode={setOrderArriveSelectedReasonCode}
              selectedReasonCode={selectedOrderArriveReasonCode}
              labels={labels}
              isLoggedIn={isLoggedIn}
              getRefundStatus={getRefundStatus}
              setOrderStatusInTransit={setOrderStatusInTransit}
              trackAnalyticsClick={trackAnalyticsClick}
              shipmentData={shipmentData}
              scrollClassName="reason-codes-scroll"
              isCouponSelfHelpEnabled={isCouponSelfHelpEnabled}
              orderArriveInTransitReasonCode={inTransit}
              action={action}
              updateHelpstep1={updateHelpstep1}
              helpstep1={helpstep1}
            />
          </Col>
        </Row>
      </>
    );
  }
  return (
    <Row fullBleed>
      <ReasonCodeSkeleton />
    </Row>
  );
};

const getRefundstatus = (refundData) => {
  return (
    refundData && refundData.refundEligible && refundData.refundEligible.toLowerCase() === 'true'
  );
};

const getResolutionComponent = (resolution, selectedShipmentCard, refundData) => {
  const refundStatus = getRefundstatus(refundData);
  switch (resolution) {
    case REASON_CODES.TRACKING_NUMBER_SHOW_IN_TRANSIT_FOR_LONG_TIME: {
      if (selectedShipmentCard && refundStatus) {
        return shipmentResolution;
      }
      return null;
    }
    default:
      return missingOrWrongItemsResolution;
  }
};

const renderResolution = (orderLabels, labels, isLiveChatEnabled, others) => {
  const {
    selectedOrderArriveReasonCode,
    selectedResolution,
    orderDetailsData,
    walletLabels,
    refundData,
    selectedShipmentCard,
    trackAnalyticsClick,
    setResolution,
    resolutionModuleXContent,
    cancelResolutionModuleXContent,
    transitHelpModuleXContent,
    selectOrderAction,
    estimatedDeliveryDate,
    setWrongItemData,
    wrongItemData,
    isOrderStatusTransit,
    orderDelivereyDate,
    clickAnalyticsData,
    isPaidNExpediteRefundEnable,
    clearSelectedResolution,
    isLoggedIn,
    selectedOrderItems,
    isInternationalShipping,
  } = others;
  return (
    <>
      <Row fullBleed className="elem-mt-MED">
        <Col colSize={{ small: 6, medium: 7, large: 12 }}>
          {selectedOrderArriveReasonCode && (
            <Resolution
              selectedResolution={selectedResolution}
              setResolution={setResolution}
              labels={labels}
              orderDetailsData={orderDetailsData}
              selectedReasonCode={selectedOrderArriveReasonCode}
              walletLabels={walletLabels}
              resolutionList={getResolutionComponent(
                selectedOrderArriveReasonCode.action,
                selectedShipmentCard,
                refundData
              )}
              trackAnalyticsClick={trackAnalyticsClick}
              isLiveChatEnabled={isLiveChatEnabled}
              selectedShipmentCard={selectedShipmentCard}
              orderLabels={orderLabels}
              resolutionModuleXContent={resolutionModuleXContent}
              cancelResolutionModuleXContent={cancelResolutionModuleXContent}
              transitHelpModuleXContent={transitHelpModuleXContent}
              refundData={refundData}
              selectOrderAction={selectOrderAction}
              estimatedDeliveryDate={estimatedDeliveryDate}
              setWrongItemData={setWrongItemData}
              wrongItemData={wrongItemData}
              isOrderStatusTransit={isOrderStatusTransit}
              orderDelivereyDate={orderDelivereyDate}
              clickAnalyticsData={clickAnalyticsData}
              isPaidNExpediteRefundEnable={isPaidNExpediteRefundEnable}
              clearSelectedResolution={clearSelectedResolution}
              isLoggedIn={isLoggedIn}
              selectedOrderItems={selectedOrderItems}
              isInternationalShipping={isInternationalShipping}
            />
          )}
        </Col>
      </Row>
    </>
  );
};

const renderInTransit = (orderLabels, labels, isLiveChatEnabled, others) => {
  return (
    <>
      <Row fullBleed>
        {getReasonCodeComponent(orderLabels, labels, isLiveChatEnabled, others, true)}
      </Row>
      <Row id="step-6" fullBleed>
        {renderResolution(orderLabels, labels, isLiveChatEnabled, others)}
      </Row>
    </>
  );
};

const renderOnTileClicked = (
  selectedTile,
  orderLabels,
  labels,
  className,
  isLiveChatEnabled,
  others,
  deliveredProps
) => {
  const { IN_TRANSIT, SHIPMENT_API_DELIVERED, IN_PROGRESS, ORDER_BEING_PROCESSED, TRANSIT } =
    ORDER_ITEM_STATUS;
  const {
    reasons,
    setSelectedDeliveredReasonCode,
    selectedDeliveredReasonCode,
    clearSelectedResolution,
    isLoggedIn,
    orderDetailsData,
    refundData,
    trackAnalyticsClick,
    shipmentData,
    getRefundStatus,
    preReasonFlag,
    orderItems,
    cshReturnItemOlderDays,
    cshForMonthsToDisplay,
    selectedResolution,
    setResolution,
    selectedOrderItems,
    selectedShipmentCard,
    isPaidNExpediteRefundEnable,
    resolutionModuleXContent,
    cancelResolutionModuleXContent,
    transitHelpModuleXContent,
    selectOrderAction,
    estimatedDeliveryDate,
    setWrongItemData,
    wrongItemData,
    isOrderStatusTransit,
    orderDelivereyDate,
    clickAnalyticsData,
    isInternationalShipping,
    selectedWrongItemResolution,
  } = deliveredProps;
  switch (selectedTile) {
    case IN_PROGRESS:
    case ORDER_BEING_PROCESSED:
      return renderInProcess(className, labels, isLiveChatEnabled, others);
    case IN_TRANSIT:
    case TRANSIT:
      return renderInTransit(orderLabels, labels, isLiveChatEnabled, others);
    case SHIPMENT_API_DELIVERED:
      return renderDelivered({
        reasons,
        setSelectedDeliveredReasonCode,
        selectedDeliveredReasonCode,
        clearSelectedResolution,
        labels,
        isLoggedIn,
        orderDetailsData,
        refundData,
        trackAnalyticsClick,
        shipmentData,
        getRefundStatus,
        preReasonFlag,
        orderItems,
        cshReturnItemOlderDays,
        cshForMonthsToDisplay,
        selectedResolution,
        setResolution,
        selectedOrderItems,
        selectedShipmentCard,
        isPaidNExpediteRefundEnable,
        resolutionModuleXContent,
        cancelResolutionModuleXContent,
        transitHelpModuleXContent,
        selectOrderAction,
        estimatedDeliveryDate,
        setWrongItemData,
        wrongItemData,
        isOrderStatusTransit,
        orderLabels,
        orderDelivereyDate,
        clickAnalyticsData,
        isLiveChatEnabled,
        isInternationalShipping,
        selectedWrongItemResolution,
        others,
      });
    default:
      return null;
  }
};

const getEstimateDate = (estimatedDate) => (estimatedDate ? orderDateFormatted(estimatedDate) : '');

const shipmentDesc = (str, orderNumber, shipmentCount) => {
  const string = str.split('**orderNumber**');
  const str1 = `${string[0]}${orderNumber}${string[1]}`;

  const str2 = str1.split('**totalShipmentCount**');
  return `${str2[0]}${shipmentCount}${str2[1]}`;
};

const groupingArray = (orderItems) => {
  const processed = [];
  const transit = [];
  const delivered = [];
  const others = [];
  orderItems.map((item) => {
    if (item.orderStatus === 'In-Progress') {
      processed.push(item);
    } else if (item.orderStatus === 'In-transit') {
      transit.push(item);
    } else if (item.orderStatus === 'Delivered') {
      delivered.push(item);
    } else {
      others.push(item);
    }
    return null;
  });
  return [...processed, ...transit, ...delivered, ...others];
};

const shouldRedirectReason = (reason) => {
  if (!reason) return false;
  const { action } = reason;
  return (
    action === REASON_CODES.RECEIVED_WRONG_ITEM ||
    action === REASON_CODES.MISSING_ORDER ||
    action === REASON_CODES.INITIATE_RETURN
  );
};

const renderOrderStatusText = (orderStatus, labels) => {
  const {
    ORDER_BEING_PROCESSED,
    SHIPPED,
    PARTIALLY_SHIPPED,
    PARTIALLY_DELIVERED,
    DELIVERED,
    ORDER_PARTIALLY_SHIPPED,
  } = ORDER_STATUS;
  switch (orderStatus) {
    case ORDER_BEING_PROCESSED:
      return getLabelValue(labels, 'lbl_orderStatus_processing');
    case PARTIALLY_SHIPPED:
    case ORDER_PARTIALLY_SHIPPED:
      return getLabelValue(labels, 'lbl_orderStatus_partiallyShipped');
    case SHIPPED:
      return getLabelValue(labels, 'lbl_orderStatus_fullyShipped');
    case PARTIALLY_DELIVERED:
      return getLabelValue(labels, 'lbl_orderStatus_partiallyDelivered');
    case DELIVERED:
      return getLabelValue(labels, 'lbl_orderStatus_fullyDelivered');
    default:
      return 'null';
  }
};

const renderDescText = (labels, orderNumber, shipmentCount, orderStatus) => {
  const {
    ORDER_BEING_PROCESSED,
    SHIPPED,
    PARTIALLY_SHIPPED,
    PARTIALLY_DELIVERED,
    DELIVERED,
    ORDER_PARTIALLY_SHIPPED,
  } = ORDER_STATUS;
  if (orderStatus === ORDER_BEING_PROCESSED) {
    const str = getLabelValue(labels, 'lbl_orderStatus_processing_text');
    return `${str}${orderNumber}.`;
  }
  if (orderStatus === PARTIALLY_SHIPPED || orderStatus === ORDER_PARTIALLY_SHIPPED) {
    const str = shipmentDesc(
      getLabelValue(labels, 'lbl_arrive_shipment_header_text'),
      orderNumber,
      shipmentCount
    );
    const str2 = getLabelValue(labels, 'lbl_orderStatus_shipment_desc');
    return `${str} ${str2}`;
  }
  if (orderStatus === SHIPPED) {
    const str = getLabelValue(labels, 'lbl_orderStatus_fullyShipped_text');
    return `${str}${orderNumber}.`;
  }
  if (orderStatus === PARTIALLY_DELIVERED) {
    const str = shipmentDesc(
      getLabelValue(labels, 'lbl_arrive_shipment_header_text_partially_delivered'),
      orderNumber,
      shipmentCount
    );
    const str2 = getLabelValue(labels, 'lbl_orderStatus_partiallyDelivered_text2');
    const str3 = getLabelValue(labels, 'lbl_orderStatus_shipment_desc');
    return `${str}${str2} ${str3}`;
  }
  if (orderStatus === DELIVERED) {
    const str = getLabelValue(labels, 'lbl_orderStatus_fullyDelivered_text');
    return `${str}${orderNumber}`;
  }
  return null;
};

const MyOrderArrive = ({
  className,
  orderDetailsData,
  labels,
  orderLabels,
  action,
  setSelectedShipmentCard,
  handleShipmentCardClick,
  selectedShipmentCard,
  filteredItems,
  setFilteredList,
  clearSelectedResolution,
  isLoggedIn,
  reasons,
  refundData,
  trackAnalyticsClick,
  shipmentData,
  getRefundStatus,
  preReasonFlag,
  cshReturnItemOlderDays,
  cshForMonthsToDisplay,
  selectedResolution,
  setResolution,
  selectedOrderItems,
  isPaidNExpediteRefundEnable,
  resolutionModuleXContent,
  cancelResolutionModuleXContent,
  transitHelpModuleXContent,
  selectOrderAction,
  setWrongItemData,
  wrongItemData,
  isOrderStatusTransit,
  orderDelivereyDate,
  clickAnalyticsData,
  isLiveChatEnabled,
  isInternationalShipping,
  clearSelectedOrderItems,
  reasonCodes,
  setOrderStatusInTransit,
  isCouponSelfHelpEnabled,
  walletLabels,
  setOrderArriveSelectedReasonCode,
  clearOrderArriveSelectedReasonCode,
  selectedOrderArriveReasonCode,
  setSelectedOrderArriveDeliveredReasonCode: setSelectedDeliveredReasonCode,
  selectedOrderArriveDeliveredReasonCode: selectedDeliveredReasonCode,
  selectedWrongItemResolution,
  helpstep1,
}) => {
  const { orderItems, orderNumber, encryptedEmailAddress, estimatedDeliveryDate, orderStatus } =
    orderDetailsData || {};
  const { orderPage } = internalEndpoints;

  const shipmentCount = getShipmentCount(orderItems);
  const [selectedTile, setSelectedTile] = useState(null);
  const [itemLength, setItemLength] = useState(0);
  const deliveredProps = {
    reasons,
    clearSelectedResolution,
    labels,
    isLoggedIn,
    orderDetailsData,
    refundData,
    trackAnalyticsClick,
    shipmentData,
    getRefundStatus,
    preReasonFlag,
    orderItems,
    cshReturnItemOlderDays,
    cshForMonthsToDisplay,
    selectedDeliveredReasonCode,
    setSelectedDeliveredReasonCode,
    selectedResolution,
    setResolution,
    selectedOrderItems,
    selectedShipmentCard,
    isPaidNExpediteRefundEnable,
    resolutionModuleXContent,
    cancelResolutionModuleXContent,
    transitHelpModuleXContent,
    selectOrderAction,
    estimatedDeliveryDate,
    setWrongItemData,
    wrongItemData,
    isOrderStatusTransit,
    orderLabels,
    orderDelivereyDate,
    clickAnalyticsData,
    isLiveChatEnabled,
    isInternationalShipping,
    selectedWrongItemResolution,
  };

  useEffect(() => {
    if (selectedDeliveredReasonCode && shouldRedirectReason(selectedDeliveredReasonCode)) {
      setSelectedShipmentCard(null);
      clearSelectedResolution();
      clearSelectedOrderItems();
      routerPush(
        `${CUSTOMER_HELP_ROUTES.TELL_US_MORE.to}&orderId=${orderNumber}&email=${encryptedEmailAddress}&reasonCode=${selectedDeliveredReasonCode.action}`,
        `${CUSTOMER_HELP_ROUTES.TELL_US_MORE.asPath}/${orderNumber}/${encryptedEmailAddress}/${selectedDeliveredReasonCode.action}`
      );
    }
  }, [selectedDeliveredReasonCode]);

  useEffect(() => {
    return () => {
      setSelectedDeliveredReasonCode(null);
    };
  }, []);
  const [RCDate, setRCDate] = useState({});
  const setRCStatus = () => {
    const shipmentStatus = filteredItems[0]?.packageStatus;

    const actualDeliveryDateValue = getEstimateDate(filteredItems[0]?.actualDeliveryDate);
    const carrierDeliveryDateValue = getEstimateDate(filteredItems[0]?.carrierDeliveryDate);
    const estimatedDeliveryDateValue = getEstimateDate(estimatedDeliveryDate);
    const shipDateDateValue = getEstimateDate(filteredItems[0]?.shipDate);

    const { label, displayDate } =
      getRCStatus(
        shipmentStatus,
        estimatedDeliveryDateValue,
        actualDeliveryDateValue,
        shipDateDateValue,
        carrierDeliveryDateValue,
        orderLabels?.orders
      ) || {};

    setRCDate({ label, displayDate });
  };

  useEffect(() => {
    setSelectedShipmentCard(null);
  }, []);

  useEffect(() => {
    if (orderDetailsData) {
      const newList = groupingArray(orderItems);
      setFilteredList(newList);
    }
  }, [orderDetailsData]);

  useEffect(() => {
    if (selectedShipmentCard && filteredItems?.length === 1) {
      const orderStatusSelected = getClickedTile(filteredItems);
      setSelectedTile(orderStatusSelected);
      setItemLength(getItemsLength(filteredItems[0].items));
      setRCStatus();
    } else {
      setSelectedTile(null);
    }
  }, [filteredItems]);

  useEffect(() => {
    if (selectedShipmentCard && reasonCodes && reasonCodes.length > 0) {
      setTimeout(() => {
        smoothScrolling('myorderArrive-step5');
      }, 100);
    }
  }, [selectedShipmentCard, reasonCodes]);

  useEffect(() => {
    if (selectedShipmentCard && selectedOrderArriveReasonCode) {
      setTimeout(() => {
        smoothScrolling('step-6');
      }, 100);
    }
  }, [selectedShipmentCard, selectedOrderArriveReasonCode]);

  useEffect(() => {
    if (!selectedShipmentCard) {
      const newList = groupingArray(orderItems);
      setFilteredList(newList);
    }
  }, [selectedShipmentCard]);

  useEffect(() => {
    if (selectedShipmentCard && selectedDeliveredReasonCode) {
      setTimeout(() => {
        smoothScrolling('step-5');
      }, 100);
    }
  }, [selectedShipmentCard, selectedDeliveredReasonCode]);

  return (
    <>
      <Row fullBleed>
        <Col colSize={{ small: 12, medium: 12, large: 12 }} className={`${className}`}>
          {!selectedTile && (
            <div className="order-Items-header-container">
              <BodyCopy
                component="p"
                fontWeight="bold"
                fontFamily="secondary"
                fontSize="fs16"
                className="elem-mb-MED"
              >
                {getLabelValue(labels, 'lbl_select_the_shipment')}
              </BodyCopy>
            </div>
          )}

          {!selectedTile && (
            <BodyCopy
              component="p"
              fontWeight="bold"
              fontFamily="secondary"
              fontSize="fs14"
              className="msg-container elem-mb-MED"
            >
              <>
                <BodyCopy
                  component="p"
                  fontWeight="bold"
                  className="msg-container-title elem-mb-XS"
                  fontSize="fs14"
                  fontFamily="secondary"
                >
                  {renderOrderStatusText(orderStatus, labels)}
                </BodyCopy>
                <BodyCopy
                  component="p"
                  fontFamily="secondary"
                  fontSize="fs14"
                  className="msg-container-description"
                >
                  {renderDescText(labels, orderNumber, shipmentCount, orderStatus)}
                </BodyCopy>
              </>

              <div className="msg-container-title2">
                <BodyCopy component="span" fontFamily="secondary" fontSize="fs14">
                  {getLabelValue(labels, 'lbl_arrive_checkout_text')}
                  &nbsp;
                </BodyCopy>
                <BodyCopy
                  component="span"
                  fontSize="fs14"
                  fontFamily="secondary"
                  fontWeight="normal"
                  className="blue-link order-link"
                  onClick={() =>
                    navigateToOrderDetailPage(
                      isLoggedIn,
                      orderPage,
                      orderNumber,
                      encryptedEmailAddress
                    )
                  }
                >
                  {getLabelValue(labels, 'lbl_returns_order_details')}
                </BodyCopy>
              </div>
            </BodyCopy>
          )}
          {!selectedTile && (
            <BodyCopy
              component="p"
              fontFamily="secondary"
              fontSize="fs14"
              fontWeight="semibold"
              className="msg-container-description2 elem-mb-LRG"
            >
              {getLabelValue(labels, 'lbl_arrive_desc_text')}
            </BodyCopy>
          )}

          <OrderCards
            orderDetailsData={orderDetailsData}
            labels={labels}
            orderLabels={orderLabels}
            action={action}
            setSelectedShipmentCard={setSelectedShipmentCard}
            handleShipmentCardClick={handleShipmentCardClick}
            selectedShipmentCard={selectedShipmentCard}
            filteredItems={filteredItems}
            setFilteredList={setFilteredList}
            clearSelectedResolution={clearSelectedResolution}
            clearOrderArriveSelectedReasonCode={clearOrderArriveSelectedReasonCode}
            setSelectedDeliveredReasonCode={setSelectedDeliveredReasonCode}
            itemLength={itemLength}
            selectedTile={selectedTile}
            RCDate={RCDate}
            isLoggedIn={isLoggedIn}
            handleChangeShipment={() => setSelectedDeliveredReasonCode(null)}
          />
        </Col>
      </Row>
      {selectedTile &&
        renderOnTileClicked(
          selectedTile,
          orderLabels,
          labels,
          className,
          isLiveChatEnabled,
          {
            reasonCodes,
            orderDetailsData,
            labels,
            isLoggedIn,
            getRefundStatus,
            setOrderStatusInTransit,
            trackAnalyticsClick,
            shipmentData,
            isCouponSelfHelpEnabled,
            selectedResolution,
            walletLabels,
            isLiveChatEnabled,
            selectedShipmentCard,
            refundData,
            setResolution,
            clearSelectedResolution,
            setOrderArriveSelectedReasonCode,
            clearOrderArriveSelectedReasonCode,
            selectedOrderArriveReasonCode,
            action,
            resolutionModuleXContent,
            cancelResolutionModuleXContent,
            transitHelpModuleXContent,
            selectOrderAction,
            estimatedDeliveryDate,
            setWrongItemData,
            wrongItemData,
            isOrderStatusTransit,
            orderDelivereyDate,
            clickAnalyticsData,
            isPaidNExpediteRefundEnable,
            selectedOrderItems,
            isInternationalShipping,
            helpstep1,
          },
          deliveredProps
        )}
    </>
  );
};

MyOrderArrive.propTypes = {
  className: PropTypes.string,
  action: PropTypes.string,
  labels: PropTypes.shape({}).isRequired,
  orderLabels: PropTypes.shape({
    orders: PropTypes.shape({}),
  }).isRequired,
  orderDetailsData: PropTypes.shape({
    orderItems: PropTypes.shape([]),
  }),
  reasonCodes: PropTypes.shape([]).isRequired,
  setSelectedShipmentCard: PropTypes.func,
  selectedShipmentCard: PropTypes.string,
  clearSelectedResolution: PropTypes.func.isRequired,
  isLoggedIn: PropTypes.bool.isRequired,
  getRefundStatus: PropTypes.func,
  setOrderStatusInTransit: PropTypes.func,
  trackAnalyticsClick: PropTypes.func,
  shipmentData: PropTypes.shape({}),
  isCouponSelfHelpEnabled: PropTypes.bool.isRequired,
  selectedResolution: PropTypes.string,
  walletLabels: PropTypes.shape({}).isRequired,
  refundData: PropTypes.shape({}),
  setResolution: PropTypes.func.isRequired,
  setOrderArriveSelectedReasonCode: PropTypes.func.isRequired,
  clearOrderArriveSelectedReasonCode: PropTypes.func.isRequired,
  selectedOrderArriveReasonCode: PropTypes.shape({}).isRequired,
  isLiveChatEnabled: PropTypes.bool.isRequired,
  handleShipmentCardClick: PropTypes.func.isRequired,
  setFilteredList: PropTypes.func,
  filteredItems: PropTypes.shape([]),
  setSelectedOrderArriveDeliveredReasonCode: PropTypes.func.isRequired,
  selectedOrderArriveDeliveredReasonCode: PropTypes.shape({}).isRequired,
  helpstep1: PropTypes.string,
  updateHelpstep1: PropTypes.func,
};

MyOrderArrive.defaultProps = {
  className: '',
  action: '',
  orderDetailsData: {},
  setSelectedShipmentCard: () => {},
  selectedShipmentCard: '',
  getRefundStatus: () => {},
  setOrderStatusInTransit: () => {},
  shipmentData: {},
  trackAnalyticsClick: () => {},
  selectedResolution: '',
  refundData: {},
  setFilteredList: () => {},
  filteredItems: [],
  helpstep1: '',
  updateHelpstep1: () => {},
};

export default withStyles(MyOrderArrive, styles);
export { MyOrderArrive as MyOrderArriveVanilla };
