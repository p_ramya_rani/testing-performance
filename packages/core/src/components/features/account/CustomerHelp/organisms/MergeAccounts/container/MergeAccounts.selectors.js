// 9fbef606107a605d69c0edbcd8029e5d
import { createSelector } from 'reselect';
import { getEmailConfirmationSuccess } from '../../EmailConfirmation/container/EmailConfirmation.selector';
import { getProfileInfoTileData } from '../../../../User/container/User.selectors';
import { getLabelValue } from '../../../../../../../utils';
import constants from '../MergeAccounts.constants';

export const getLabels = (state) => state?.Labels?.global;

export const getVictimEmailAddress = (state) => {
  const emailConfirmation = getEmailConfirmationSuccess(state);
  return emailConfirmation?.data?.email;
};

export const getSurvivorEmailAddress = (state) => {
  const profileInfo = getProfileInfoTileData(state);
  return profileInfo?.emailAddress;
};

export const getMergeAccountButtonLoader = (state) => {
  return state.MergeAccount && state.MergeAccount.get('mergeAccountButtonLoader');
};

export const getNumberOfRetries = (state) => {
  return state.MergeAccount && state.MergeAccount.get('numberOfRetries');
};

export const getAccountStatus = (state) => {
  return state.MergeAccount && state.MergeAccount.get('accountStatus');
};

export const getMergeError = (state) => {
  return state.MergeAccount && state.MergeAccount.get('error');
};

export const getMergeFromOTPFlow = (state) => {
  return state.MergeAccount && state.MergeAccount.get('isFromOTPFlow');
};

export const getMergeAccountFailure = (state) => {
  return state.MergeAccount && state.MergeAccount.get('error');
};

export const getMergeAccountSuccess = (state) => {
  return state.MergeAccount && state.MergeAccount.get('mergeAccountSuccess');
};

export const getFirstTImeOtp = (state) => {
  return state.MergeAccount && state.MergeAccount.get('firstTimeOtp');
};

export const getMergeStatus = createSelector([getMergeAccountSuccess], (mergeAccountSuccess) => {
  if (mergeAccountSuccess) {
    return mergeAccountSuccess.mergeStatus
      ? constants.MERGE_STATUS.MERGE_SUCCESS
      : constants.MERGE_STATUS.MERGE_FAILED;
  }
  return null;
});

export const getShowConfirmationModal = (state) => {
  return state.MergeAccount && state.MergeAccount.get('showConfirmationModal');
};

export const getIsMergeConfirmed = (state) => {
  return state.MergeAccount && state.MergeAccount.get('isMergeConfirmed');
};

export const getShowRecaptcha = createSelector(
  [getNumberOfRetries, getLabels],
  (numberOfRetries, labels) => {
    const recaptchaThreshold =
      parseInt(getLabelValue(labels.helpCenter, 'lbl_merge_recaptcha_threshold'), 10) ||
      constants.recaptchaThresholdDefault;
    return numberOfRetries >= recaptchaThreshold;
  }
);
