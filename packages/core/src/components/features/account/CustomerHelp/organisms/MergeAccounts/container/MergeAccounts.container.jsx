// 9fbef606107a605d69c0edbcd8029e5d
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { setClickAnalyticsData, trackPageView, trackClick } from '@tcp/core/src/analytics/actions';
import MergeAccountsView from '../views';
import { getUserLoggedInState } from '../../../../User/container/User.selectors';
import {
  getShowConfirmationModal,
  getSurvivorEmailAddress,
  getVictimEmailAddress,
  getIsMergeConfirmed,
} from './MergeAccounts.selectors';
import { resetEmailConfirmationRequest } from '../../EmailConfirmation/container/EmailConfirmation.action';
import {
  mergeAccountReset as mergeAccountResetAction,
  setShowConfirmationModal as setShowConfirmationModalAction,
  setIsMergeConfirmed as setIsMergeConfirmedAction,
} from './MergeAccounts.actions';

const MergeAccounts = (props) => {
  const { mergeAccountReset, resetEmailConfirmation } = props;

  useEffect(() => {
    mergeAccountReset();
    resetEmailConfirmation();
  }, []);
  return <MergeAccountsView {...props} />;
};

const mapStateToProps = (state) => {
  return {
    isLoggedIn: getUserLoggedInState(state),
    survivorEmailAddress: getSurvivorEmailAddress(state),
    victimEmailAddress: getVictimEmailAddress(state),
    showConfirmationModal: getShowConfirmationModal(state),
    isMergeConfirmed: getIsMergeConfirmed(state),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    resetEmailConfirmation: () => dispatch(resetEmailConfirmationRequest()),
    mergeAccountReset: () => dispatch(mergeAccountResetAction()),
    setShowConfirmationModal: (payload) => dispatch(setShowConfirmationModalAction(payload)),
    setIsMergeConfirmed: (payload) => dispatch(setIsMergeConfirmedAction(payload)),
    trackAnalyticsPageView: (eventData, payload) => {
      const loadtimer = setTimeout(() => {
        dispatch(setClickAnalyticsData(eventData));
        // Delayed setting of data when redirection on click event
        clearTimeout(loadtimer);
      }, 300);
      const timer = setTimeout(() => {
        dispatch(trackPageView(payload));
        clearTimeout(timer);
      }, 800);
    },
    trackAnalyticsClick: (eventData, payload) => {
      dispatch(setClickAnalyticsData(eventData));
      const timer = setTimeout(() => {
        dispatch(trackClick(payload));
        clearTimeout(timer);
      }, 800);
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MergeAccounts);
