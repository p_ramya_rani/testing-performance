/* eslint-disable max-lines */
/* eslint-disable complexity */
/* eslint-disable sonarjs/cognitive-complexity */
// 9fbef606107a605d69c0edbcd8029e5d
import React, { useEffect, useState, useRef } from 'react';
import PropTypes from 'prop-types';
import differenceInCalendarDays from 'date-fns/differenceInCalendarDays';
import toLower from 'lodash/toLower';
import { View, FlatList, Dimensions } from 'react-native';
import {
  BodyCopyWithSpacing,
  ViewWithSpacing,
} from '@tcp/core/src/components/common/atoms/styledWrapper';
import { Image, Anchor } from '@tcp/core/src/components/common/atoms';
import {
  getLabelValue,
  convertToISOString,
  getMobileFormatedOrderDate,
  formatYmd,
} from '@tcp/core/src/utils/utils';
import {
  CUSTOMER_HELP_PAGE_TEMPLATE,
  REASON_CODES,
  missingOrWrongItemsResolution,
  cancelNModifyResolution,
  delayedResolution,
  reasonWrongItemsResolution,
  SHIPMENT_METHODS,
  shipmentResolution,
  connectWithSpecialistResolution,
  ORDER_ITEM_STATUS,
} from '@tcp/core/src/components/features/account/CustomerHelp/CustomerHelp.constants';

import { getOrderTileItems } from '../../../util/utility';
import OrderTile from '../../../molecules/OrderTile';
import ReasonCode from '../organisms/ReasonCode';
import {
  RowContainer,
  CustomButton,
  TellUsMoreSkeletonView,
} from '../../../styles/CustomerHelp.style.native';
import OrderItems from '../organisms/OrderItems';
import Resolution from '../organisms/Resolution/views';
import ReasonCodeSkeleton from '../skeleton/ReasonCodeSkeleton.view.native';
import TellUsMoreSkeleton from '../skeleton/TellUsMoreSkeleton.view.native';
import OrderCards from '../organisms/OrderCards/views';
import CustomerServiceHelp from '../organisms/CustomerServiceHelp';
import { SafeArea } from '../../../../../../common/atoms/styledWrapper/styledWrapper.native';

const { height } = Dimensions.get('window');
const guidelineBaseHeight = 680;
const verticalScale = (size) => (height / guidelineBaseHeight) * size;

const infoIcon = require('../../../../../../../../../mobileapp/src/assets/images/info-icon.png');

const wrapStyle = { flexWrap: 'wrap', flex: 1 };

let isFirstOrder = true;

const getRefundstatus = (refundData) => {
  return (
    refundData && refundData.refundEligible && refundData.refundEligible.toLowerCase() === 'true'
  );
};

const getOrderTileData = ({ ordersListItems, orderId, cshForMonthsToDisplay }) => {
  const itemGroup =
    ordersListItems &&
    orderId &&
    ordersListItems.orders &&
    ordersListItems.orders.filter((order) => order.orderNumber === orderId);
  return getOrderTileItems({
    item: itemGroup && itemGroup[0],
    cshForMonthsToDisplay,
    noStatusDisplay: true,
  });
};

const getResolutionComponent = (
  resolution,
  refundData,
  selectedOrderItems,
  selectedShipmentCard,
  isPaidNExpediteRefundEnableApp
) => {
  const refundStatus = getRefundstatus(refundData);
  switch (resolution) {
    case REASON_CODES.CANCEL_MODIFY:
      return cancelNModifyResolution;
    case REASON_CODES.MISSING_ORDER: {
      if (Object.keys(selectedOrderItems).length > 0) {
        return missingOrWrongItemsResolution;
      }
      return null;
    }
    case REASON_CODES.TRACKING_NUMBER_SHOW_IN_TRANSIT_FOR_LONG_TIME: {
      if (selectedShipmentCard && refundStatus) {
        return shipmentResolution;
      }
      return null;
    }
    case REASON_CODES.PACKAGE_DELIVERED_BUT_NOT_RECEIVED: {
      if (selectedShipmentCard) {
        return shipmentResolution;
      }
      return null;
    }
    case REASON_CODES.PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME:
      return isPaidNExpediteRefundEnableApp ? delayedResolution : connectWithSpecialistResolution;
    case REASON_CODES.RECEIVED_WRONG_ITEM: {
      if (Object.keys(selectedOrderItems).length > 0) {
        return reasonWrongItemsResolution;
      }
      return null;
    }
    default:
      return missingOrWrongItemsResolution;
  }
};

const renderBossBopisAndOlderOrderView = (props) => {
  const { labels, selectOrderAction, navigation, orderLabels, isLoggedIn, info, totalRecordsAdd } =
    props;

  return (
    <ViewWithSpacing spacingStyles="margin-top-MED">
      <BodyCopyWithSpacing
        fontFamily="secondary"
        fontSize="fs16"
        fontWeight="bold"
        text={getLabelValue(labels, 'lbl_how_we_can_help', '')}
      />

      <RowContainer spacingStyles="margin-top-MED margin-bottom-MED">
        <Image width="26px" height="26px" source={infoIcon} alt="info" />
        <BodyCopyWithSpacing
          fontFamily="secondary"
          fontSize="fs14"
          spacingStyles="margin-left-XS"
          text={info}
          style={{ ...wrapStyle }}
        />
      </RowContainer>
      <CustomerServiceHelp labels={labels} navigation={navigation} />
      {totalRecordsAdd > 1 || !isLoggedIn ? (
        <ViewWithSpacing spacingStyles="margin-top-LRG margin-left-XXL margin-right-XXL">
          <CustomButton
            fontSize="fs14"
            type="button"
            fontWeight="bold"
            onPress={() => selectOrderAction({ navigation })}
            text={getLabelValue(orderLabels.orders, 'lbl_help_another_order')}
          />
        </ViewWithSpacing>
      ) : null}
    </ViewWithSpacing>
  );
};

const getOrderCards = (params) => {
  const {
    selectedReasonCode,
    labels,
    setSelectedShipmentCard,
    clearSelectedResolution,
    selectedShipmentCard,
    orderDetailsData,
    orderLabels,
    isDelivered,
    handleShipmentCardClick,
    setIsDelivered,
    setOrderStatusInTransit,
    setToggleCards,
    toggleCards,
    shipmentData,
    filteredItems,
    setFilteredList,
    ref,
  } = params;
  const { action } = selectedReasonCode || {};
  const {
    TRACKING_NUMBER_SHOW_IN_TRANSIT_FOR_LONG_TIME,
    PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME,
    PACKAGE_DELIVERED_BUT_NOT_RECEIVED,
  } = REASON_CODES;
  const { STANDARD, GROUND } = SHIPMENT_METHODS;
  const { shippingMethod } = orderDetailsData || {};
  const shippingForm = shippingMethod && shippingMethod.toLowerCase();
  const expressDelivery = shippingForm !== STANDARD && shippingForm !== GROUND;
  if (
    action === TRACKING_NUMBER_SHOW_IN_TRANSIT_FOR_LONG_TIME ||
    (action === PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME && expressDelivery) ||
    action === PACKAGE_DELIVERED_BUT_NOT_RECEIVED
  ) {
    return (
      <View ref={ref}>
        <OrderCards
          labels={labels}
          setSelectedShipmentCard={setSelectedShipmentCard}
          clearSelectedResolution={clearSelectedResolution}
          selectedShipmentCard={selectedShipmentCard}
          orderDetailsData={orderDetailsData}
          orderLabels={orderLabels}
          isDelivered={isDelivered}
          handleShipmentCardClick={handleShipmentCardClick}
          setIsDelivered={setIsDelivered}
          action={action}
          setOrderStatusInTransit={setOrderStatusInTransit}
          setToggleCards={setToggleCards}
          toggleCards={toggleCards}
          shipmentData={shipmentData}
          filteredItems={filteredItems}
          setFilteredList={setFilteredList}
        />
      </View>
    );
  }
  return null;
};

const getMissingOrderOrWrongItemLabel = (selectedReasonCode, orderLabels) => {
  return selectedReasonCode.action === REASON_CODES.MISSING_ORDER
    ? getLabelValue(orderLabels, 'lbl_csh_select_the_missingItems', 'orders')
    : getLabelValue(orderLabels, 'lbl_csh_select_the_incorrectItems', 'orders');
};

const getTotalItemsCount = (items) => {
  let totalItemsCount = 0;
  if (Array.isArray(items)) {
    items.forEach((item) => {
      const { itemInfo = {} } = item;
      totalItemsCount += itemInfo.quantity || 0;
    });
  }
  return totalItemsCount;
};

export const TellusMoreContext = React.createContext();

const usePrevious = (value) => {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  }, [value]);
  return ref.current;
};

const TellUsMore = (props) => {
  const {
    reasonCodes,
    selectedResolution,
    selectedWrongItemResolution,
    getReasonsCodeContent,
    setSelectedReasonCode,
    selectedReasonCode,
    clearSelectedReasonCode,
    clearSelectedResolution,
    labels,
    orderLabels,
    orderDetailsData,
    getOrderDetailsAction,
    selectOrderAction,
    isLoggedIn,
    setSelectedOrderItems,
    setResolution,
    selectedOrderItems,
    shipmentData,
    clearSelectedOrderItems,
    fetchResolutionModuleX,
    resolutionModuleXContent,
    getRefundStatus,
    transitHelpModuleXContent,
    refundData,
    resolutionContentId,
    setWrongItemData,
    wrongItemData,
    setOrderStatusInTransit,
    isOrderStatusTransit,
    setToggleCards,
    toggleCards,
    isPaidNExpediteRefundEnableApp,
    selectedShipmentCard,
    setSelectedShipmentCard,
    fetchTransitHelpModuleX,
    navigation,
    orderItems,
    cshForMonthsToDisplay,
    filteredItems,
    setFilteredList,
    trackAnalyticsClick,
    trackAnalyticsPageView,
    ordersListItems,
    isFetchingOrderItems,
    clickAnalyticsData,
    orderLookUpAPISwitch,
    isMobileLiveChatEnabled,
  } = props;

  const [isDelivered, setIsDelivered] = useState(false);
  const [orderDelivereyDate, setOrderDeliveredDate] = useState(false);
  const [isAppeasementAppliedOnOrder, setIsAppeasementAppliedOnOrder] = useState(false);
  const {
    estimatedDeliveryDate,
    orderNumber,
    encryptedEmailAddress: emailAddress,
  } = orderDetailsData || {};

  const totalRecordsAdd =
    ordersListItems && ordersListItems.orders && ordersListItems.orders.length;
  const { RECEIVED_WRONG_ITEM, MISSING_ORDER } = REASON_CODES;
  const { action: selectedReasonCodeAction } = selectedReasonCode || {};
  const step1 = useRef();
  const step2 = useRef();
  const step3 = useRef();
  const step4 = useRef();
  const step5 = useRef();
  const step6 = useRef();
  const step7 = useRef();
  const scrollRef = useRef();
  const orderId = (navigation && navigation.getParam('orderId')) || '';
  let prevOrderNum;

  const prevSelectedResolution = usePrevious(selectedResolution);
  const prevWrongSelectedResolution = usePrevious(selectedResolution);

  const scrollToEndView = (stepId) => {
    stepId.current.measure(() => {
      scrollRef.current.scrollToEnd({ animated: true, duration: 100 });
    });
  };

  const smoothScrolling = (steps) => {
    if (
      steps === 'step-4' &&
      step4 &&
      step4.current &&
      scrollRef &&
      scrollRef.current &&
      scrollRef.current.scrollToOffset
    ) {
      step4.current.measure(() => {
        scrollRef.current.scrollToOffset({ animated: true, offset: verticalScale(280) });
      });
    }

    if (
      steps === 'step-3' &&
      step3 &&
      step3.current &&
      scrollRef &&
      scrollRef.current &&
      scrollRef.current.scrollToOffset
    ) {
      step3.current.measure(() => {
        scrollRef.current.scrollToOffset({ animated: true, offset: verticalScale(200) });
      });
    }

    if (steps === 'step-5' && step5 && step5.current && scrollRef && scrollRef.current) {
      scrollToEndView(step5);
    }

    if (steps === 'step-7' && step7 && step7.current && scrollRef && scrollRef.current) {
      scrollToEndView(step7);
    }
    if (steps === 'step-6' && step6 && step6.current && scrollRef && scrollRef.current) {
      scrollToEndView(step6);
    }
  };

  useEffect(() => {
    if (orderDetailsData && reasonCodes && reasonCodes.length > 0) {
      setTimeout(() => {
        smoothScrolling('step-3');
      }, 100);
    }
  }, [orderDetailsData, reasonCodes]);

  useEffect(() => {
    if (selectedResolution && prevSelectedResolution !== selectedResolution) {
      const timeout = selectedResolution === 'modifyOrder' ? 1000 : 100;
      setTimeout(() => {
        smoothScrolling('step-7');
      }, timeout);
    }
  }, [selectedResolution]);

  useEffect(() => {
    if (selectedShipmentCard) {
      setTimeout(() => {
        smoothScrolling('step-7');
      }, 100);
    }
  }, [selectedShipmentCard]);

  useEffect(() => {
    if (
      selectedWrongItemResolution &&
      prevWrongSelectedResolution !== selectedWrongItemResolution
    ) {
      setTimeout(() => {
        smoothScrolling('step-7');
      }, 300);
    }
  }, [selectedWrongItemResolution]);

  useEffect(() => {
    if (selectedReasonCode) {
      const { CANCEL_MODIFY, COUPON_ISSUE, RETURN_ITEMS } = REASON_CODES;

      const isStep5 =
        selectedReasonCode?.action === CANCEL_MODIFY ||
        selectedReasonCode?.action === COUPON_ISSUE ||
        selectedReasonCode?.action === RETURN_ITEMS;
      const step = isStep5 ? 'step-5' : 'step-4';

      setTimeout(() => {
        smoothScrolling(step);
      }, 500);
    }
  }, [selectedReasonCode]);

  useEffect(() => {
    if (selectedOrderItems) {
      if (isFirstOrder) {
        setTimeout(() => {
          scrollRef.current.scrollToEnd({ animated: true, duration: 100 });
        }, 500);
      } else {
        setTimeout(() => {
          smoothScrolling('step-5');
        }, 500);
      }
      isFirstOrder = false;
    }
  }, [selectedOrderItems]);

  useEffect(() => {
    const { orderItems: orderList = [] } = orderDetailsData || {};
    const defaultPayload = { orderId, fromCSH: true, orderLookUpAPISwitch };
    let payload;
    if ((orderId && orderId !== orderNumber) || !orderList || !orderList.length) {
      payload = defaultPayload;
      if (!isLoggedIn && orderId && orderId === orderNumber) {
        payload = { ...defaultPayload, emailAddress };
      }
      getOrderDetailsAction(payload);
    }
  }, [orderId]);

  useEffect(() => {
    if (typeof labels === 'object' && labels.referred) {
      getReasonsCodeContent(labels.referred);
      const referredResolutionContentId = labels.referred[1] && labels.referred[1].contentId;
      if (referredResolutionContentId) {
        fetchResolutionModuleX(referredResolutionContentId);
      }
    }
  }, [reasonCodes]);

  useEffect(() => {
    if (orderNumber !== undefined) {
      if (orderNumber !== prevOrderNum) {
        trackAnalyticsPageView(
          { ...clickAnalyticsData, orderId_cd127: orderNumber },
          { currentScreen: 'orderHelp_page' }
        );
      }
      prevOrderNum = orderNumber;
      if (orderId && orderId === orderNumber) {
        clearSelectedReasonCode();
        clearSelectedResolution();
        clearSelectedOrderItems();
        setWrongItemData({});
        setOrderStatusInTransit(null);
        setToggleCards(false);
        setSelectedShipmentCard(null);
        setIsDelivered(false);
        setFilteredList(null);
      }
    }
  }, [orderNumber]);

  useEffect(() => {
    let appeasementAppliedOnOrder = true;
    if (orderItems) {
      orderItems.every((orderItem) => {
        const {
          productInfo: { appeasementApplied },
        } = orderItem;
        if (!appeasementApplied) {
          appeasementAppliedOnOrder = false;
          return false;
        }
        return true;
      });
    }
    setIsAppeasementAppliedOnOrder(appeasementAppliedOnOrder);
  }, [orderItems]);

  useEffect(() => {
    if (resolutionContentId) {
      fetchTransitHelpModuleX(resolutionContentId.contentId);
    }
  }, [resolutionContentId]);

  const getOrderItem = () => {
    const {
      orderType,
      orderDate,
      status,
      encryptedEmailAddress,
      isEcomOrder,
      isBossOrder,
      isBopisOrder,
      shippingMethod,
      canceledItems,
      orderItems: orderItemsData,
    } = orderDetailsData || {};

    const { summary } = orderDetailsData || {};
    const { currencySymbol, grandTotal } = summary || {};
    const { imagePath, name } =
      (orderItems && orderItems.length > 0 && orderItems[0].productInfo) || {};
    const imagePathValue = imagePath.includes('https') ? imagePath : `https://${imagePath}`;
    const ItemsCount = getTotalItemsCount(orderItems);
    const orderDateParsed = new Date(convertToISOString(orderDate));

    let isOlderItem = false;
    const dateDifference = differenceInCalendarDays(
      new Date(),
      new Date(convertToISOString(orderDate))
    );
    if (dateDifference > cshForMonthsToDisplay) {
      isOlderItem = true;
    }
    return {
      orderNumber,
      orderDate: getMobileFormatedOrderDate(orderDateParsed),
      currencySymbol,
      grandTotal,
      productName: name,
      imagePath: imagePathValue || '',
      orderType,
      encryptedEmailAddress,
      ItemsCount,
      isEcomOrder,
      isOlderItem,
      status,
      isNonOnline: isBossOrder || isBopisOrder,
      shippingMethod,
      canceledItems,
      orderItems: orderItemsData,
    };
  };

  const isNonOnlineOrOlderOrder = () => {
    const { isBossOrder, isBopisOrder, isOlderItem } = orderDetailsData || {};
    return isBossOrder || isBopisOrder || isOlderItem;
  };

  const showOrderDisabled = () => {
    const { orderStatus } = orderDetailsData || {};
    const { CANCELLED, CANCELED } = ORDER_ITEM_STATUS;
    const isOrderCancelled =
      orderStatus === toLower(CANCELLED) || orderStatus === toLower(CANCELED);
    return isNonOnlineOrOlderOrder() || isAppeasementAppliedOnOrder || isOrderCancelled;
  };

  const orderData = orderDetailsData && getOrderItem();
  const { isNonOnline, isOlderItem: isOldOrder } = orderData || {};
  const isDisabled = showOrderDisabled();

  const getReasonCodeComponent = (reasons) => {
    if (reasons?.length > 0) {
      const filteredReasonList = reasons.filter(
        (item) =>
          item.action !== REASON_CODES.RETURN_ITEMS && item.action !== REASON_CODES.COUPON_ISSUE
      );
      return (
        <ReasonCode
          ref={step3}
          reasonList={filteredReasonList}
          setSelectedReasonCode={setSelectedReasonCode}
          selectedReasonCode={selectedReasonCode}
          clearSelectedReasonCode={() => clearSelectedReasonCode()}
          clearSelectedResolution={() => clearSelectedResolution()}
          orderLabels={orderLabels}
          isLoggedIn={isLoggedIn}
          orderDetailsData={orderDetailsData}
          navigation={navigation}
          getRefundStatus={getRefundStatus}
          setOrderStatusInTransit={setOrderStatusInTransit}
          trackAnalyticsClick={trackAnalyticsClick}
          shipmentData={shipmentData}
        />
      );
    }
    return <ReasonCodeSkeleton />;
  };

  const getLabelMessage = () => {
    let message = '';
    if (isNonOnline) {
      message = `${getLabelValue(labels, 'lbl_pickup_disabled_initial')}  ${getLabelValue(
        labels,
        'lbl_contact_us_directly'
      )} ${getLabelValue(labels, 'lbl_pickup_disabled_ending')}`;
    }
    if (isOldOrder) {
      message = `${getLabelValue(labels, 'lbl_older_disabled_initial')}  ${getLabelValue(
        labels,
        'lbl_contact_us_directly'
      )} ${getLabelValue(labels, 'lbl_older_disabled_ending')} `;
    }
    return message;
  };

  const getItems = (orderGroupSelected) => {
    let result;
    orderGroupSelected.items.forEach((el) => {
      result = { ...result, [el.lineNumber]: el.itemInfo.quantity };
    });
    return result;
  };

  const handleShipmentCardClick = (cardIdx, orderGroupSelected) => {
    const { orderDate, shippingMethod } = orderDetailsData;
    const { trackingDetails } = shipmentData || [];
    const selectedItemGroup =
      trackingDetails &&
      trackingDetails.filter((item) => item.trackingId === orderGroupSelected.trackingNumber);
    if (orderNumber && orderDate && shippingMethod) {
      const deliveredDate =
        selectedItemGroup && selectedItemGroup[0] && selectedItemGroup[0].deliveredDate;
      const orderID = orderNumber;
      const orderPlacedDate = formatYmd(orderDate);
      const orderDeliveredDate = deliveredDate ? formatYmd(deliveredDate) : '';
      getRefundStatus({ orderID, orderPlacedDate, shippingMethod, orderDeliveredDate });
      setOrderDeliveredDate(orderDeliveredDate);
    }
    setSelectedShipmentCard(cardIdx);
    const selectedItems = orderGroupSelected && getItems(orderGroupSelected);
    if (orderGroupSelected) {
      setSelectedOrderItems(selectedItems);
    }
  };

  const RenderResolution = () => {
    return (
      <View>
        {selectedReasonCode && (
          <Resolution
            selectedResolution={
              selectedReasonCode.action === RECEIVED_WRONG_ITEM
                ? selectedWrongItemResolution
                : selectedResolution
            }
            selectedWrongItemResolution={selectedWrongItemResolution}
            setResolution={setResolution}
            resolutionList={getResolutionComponent(
              selectedReasonCode.action,
              refundData,
              selectedOrderItems,
              selectedShipmentCard,
              isPaidNExpediteRefundEnableApp
            )}
            labels={labels}
            resolutionModuleXContent={resolutionModuleXContent}
            selectedReasonCode={selectedReasonCode}
            transitHelpModuleXContent={transitHelpModuleXContent}
            selectedShipmentCard={selectedShipmentCard}
            refundData={refundData}
            selectOrderAction={selectOrderAction}
            estimatedDeliveryDate={estimatedDeliveryDate}
            setWrongItemData={setWrongItemData}
            wrongItemData={wrongItemData}
            hideButton={selectedReasonCode.action === RECEIVED_WRONG_ITEM}
            isOrderStatusTransit={isOrderStatusTransit}
            orderLabels={orderLabels}
            orderDetailsData={orderDetailsData}
            orderDelivereyDate={orderDelivereyDate}
            isPaidNExpediteRefundEnableApp={isPaidNExpediteRefundEnableApp}
            navigation={navigation}
            trackAnalyticsClick={trackAnalyticsClick}
            smoothScrolling={smoothScrolling}
            scrollRef={scrollRef}
            clickAnalyticsData={clickAnalyticsData}
            isLiveChatEnabled={isMobileLiveChatEnabled}
            isLoggedIn={isLoggedIn}
            stepId={step5}
          />
        )}
        {wrongItemData &&
          Object.keys(wrongItemData).length > 0 &&
          wrongItemData.someThingElse &&
          selectedReasonCode &&
          selectedReasonCode.action === RECEIVED_WRONG_ITEM && (
            <Resolution
              selectedResolution={selectedResolution}
              setResolution={setResolution}
              resolutionList={getResolutionComponent(
                MISSING_ORDER,
                refundData,
                selectedOrderItems,
                selectedShipmentCard
              )}
              labels={labels}
              resolutionModuleXContent={resolutionModuleXContent}
              selectedReasonCode={selectedReasonCode}
              setWrongItemData={setWrongItemData}
              wrongItemData={wrongItemData}
              orderDelivereyDate={orderDelivereyDate}
              wrongItemResolution
              navigation={navigation}
              orderLabels={orderLabels}
              trackAnalyticsClick={trackAnalyticsClick}
              smoothScrolling={smoothScrolling}
              scrollRef={scrollRef}
              clickAnalyticsData={clickAnalyticsData}
              isLiveChatEnabled={isMobileLiveChatEnabled}
              isLoggedIn={isLoggedIn}
              stepId={step6}
            />
          )}
      </View>
    );
  };

  const info = getLabelMessage();
  const DATA = [{}];

  const renderItem = () => (
    <TellusMoreContext.Provider
      value={{
        step1,
        step2,
        step3,
        step4,
        step5,
        step6,
        step7,
      }}
    >
      <View>
        {orderDetailsData && reasonCodes && !isFetchingOrderItems ? (
          <>
            <RowContainer>
              <BodyCopyWithSpacing
                fontFamily="secondary"
                fontSize="fs16"
                fontWeight="bold"
                spacingStyles="margin-bottom-XS margin-top-XS"
                text={getLabelValue(labels, 'lbl_selected_order')}
              />
              {totalRecordsAdd > 1 || !isLoggedIn ? (
                <Anchor
                  text={getLabelValue(labels, 'lbl_check_another_order')}
                  fontSizeVariation="large"
                  anchorVariation="secondary"
                  fontSize="fs14"
                  fontFamily="primary"
                  onPress={() => selectOrderAction({ navigation })}
                  underlineBlue
                />
              ) : null}
            </RowContainer>
            {orderDetailsData && (
              <OrderTile
                labels={labels}
                orderLabels={orderLabels}
                orderDetailsData={orderData}
                selected={!isDisabled}
                isNonOnlineOrOlderOrder={isDisabled}
                origin={CUSTOMER_HELP_PAGE_TEMPLATE.TELL_US_MORE}
                isAppeasementAppliedOnOrder={isAppeasementAppliedOnOrder}
              />
            )}
            <>
              {!isDisabled ? (
                <ViewWithSpacing spacingStyles="padding-bottom-XXXL">
                  <RowContainer>
                    <View ref={step3}>
                      <BodyCopyWithSpacing
                        fontFamily="secondary"
                        fontSize="fs16"
                        fontWeight="bold"
                        spacingStyles="margin-left-XS margin-bottom-XS margin-top-XS"
                        text={getLabelValue(labels, 'lbl_reasoncode_header', '')}
                      />
                    </View>

                    {selectedReasonCode && (
                      <Anchor
                        text={getLabelValue(orderLabels, 'lbl_edit_option', 'orders')}
                        fontSizeVariation="large"
                        anchorVariation="secondary"
                        fontSize="fs14"
                        fontFamily="primary"
                        onPress={() => {
                          smoothScrolling('step-3');
                          clearSelectedReasonCode();
                          clearSelectedResolution();
                          clearSelectedOrderItems();
                          setWrongItemData({});
                          setOrderStatusInTransit(null);
                          setToggleCards(false);
                          setSelectedShipmentCard(null);
                          setIsDelivered(false);
                          setFilteredList(null);
                        }}
                        underlineBlue
                      />
                    )}
                  </RowContainer>
                  {getReasonCodeComponent(reasonCodes)}

                  {selectedReasonCode &&
                    (selectedReasonCodeAction === MISSING_ORDER ||
                      selectedReasonCodeAction === RECEIVED_WRONG_ITEM) && (
                      <View ref={step4}>
                        <BodyCopyWithSpacing
                          spacingStyles="margin-left-XS margin-bottom-XXS"
                          text={getMissingOrderOrWrongItemLabel(selectedReasonCode, orderLabels)}
                          fontWeight="bold"
                          fontFamily="secondary"
                          fontSize="fs16"
                        />
                        <BodyCopyWithSpacing
                          spacingStyles="margin-left-XS"
                          component="span"
                          fontFamily="secondary"
                          fontSize="fs12"
                          text={
                            selectedReasonCodeAction === RECEIVED_WRONG_ITEM
                              ? getLabelValue(orderLabels, 'lbl_select_only_one_item', 'orders')
                              : getLabelValue(
                                  orderLabels,
                                  'lbl_csh_multiple_items_cant_selected',
                                  'orders'
                                )
                          }
                        />
                        <OrderItems
                          labels={labels}
                          orderLabels={orderLabels}
                          orderDetailsData={orderDetailsData}
                          setSelectedOrderItems={setSelectedOrderItems}
                          selectedOrderItems={selectedOrderItems}
                          shipmentData={shipmentData}
                          clearSelectedOrderItems={clearSelectedOrderItems}
                          selectedReasonCode={selectedReasonCode}
                          clearSelectedResolution={clearSelectedResolution}
                        />
                      </View>
                    )}

                  {getOrderCards({
                    selectedReasonCode,
                    labels,
                    setSelectedShipmentCard,
                    clearSelectedResolution,
                    selectedShipmentCard,
                    orderDetailsData,
                    orderLabels,
                    isDelivered,
                    handleShipmentCardClick,
                    setIsDelivered,
                    setOrderStatusInTransit,
                    setToggleCards,
                    toggleCards,
                    shipmentData,
                    getRefundStatus,
                    setFilteredList,
                    filteredItems,
                    ref: step4,
                  })}

                  <RenderResolution />
                </ViewWithSpacing>
              ) : (
                renderBossBopisAndOlderOrderView({ ...props, info, totalRecordsAdd })
              )}
            </>
          </>
        ) : (
          <>
            {ordersListItems && Object.keys(ordersListItems).length && (
              <>
                <RowContainer>
                  <BodyCopyWithSpacing
                    fontWeight="bold"
                    fontFamily="secondary"
                    fontSize="fs16"
                    text={getLabelValue(labels, 'lbl_selected_order')}
                  />
                </RowContainer>

                <OrderTile
                  labels={labels}
                  orderLabels={orderLabels}
                  orderDetailsData={getOrderTileData({
                    ordersListItems,
                    orderId,
                    cshForMonthsToDisplay,
                  })}
                  selected
                />

                <TellUsMoreSkeleton />
              </>
            )}

            {!ordersListItems && isFetchingOrderItems && (
              <>
                <TellUsMoreSkeletonView />
                <TellUsMoreSkeleton />
              </>
            )}

            {getOrderCards({
              selectedReasonCode,
              labels,
              setSelectedShipmentCard,
              clearSelectedResolution,
              selectedShipmentCard,
              orderDetailsData,
              orderLabels,
              isDelivered,
              handleShipmentCardClick,
              setIsDelivered,
              setOrderStatusInTransit,
              setToggleCards,
              toggleCards,
              shipmentData,
              getRefundStatus,
              setFilteredList,
              filteredItems,
              ref: step4,
            })}

            <RenderResolution />
          </>
        )}
      </View>
    </TellusMoreContext.Provider>
  );

  const keyExtractorId = (item) => item.id;
  return (
    <SafeArea>
      <FlatList ref={scrollRef} data={DATA} renderItem={renderItem} keyExtractor={keyExtractorId} />
    </SafeArea>
  );
};

TellUsMore.propTypes = {
  reasonCodes: PropTypes.shape([]).isRequired,
  getReasonsCodeContent: PropTypes.func.isRequired,
  labels: PropTypes.shape({}).isRequired,
  setSelectedReasonCode: PropTypes.func.isRequired,
  selectedReasonCode: PropTypes.shape({}).isRequired,
  clearSelectedReasonCode: PropTypes.func.isRequired,
  clearSelectedResolution: PropTypes.func.isRequired,
  orderLabels: PropTypes.shape({}).isRequired,
  orderDetailsData: PropTypes.shape({}).isRequired,
  ordersListItems: PropTypes.shape({}).isRequired,
  getOrderDetailsAction: PropTypes.func.isRequired,
  router: PropTypes.shape({}).isRequired,
  selectOrderAction: PropTypes.func.isRequired,
  isLoggedIn: PropTypes.bool.isRequired,
  setSelectedOrderItems: PropTypes.func.isRequired,
  selectedOrderItems: PropTypes.shape({}),
  shipmentData: PropTypes.shape({}),
  selectedResolution: PropTypes.string,
  setResolution: PropTypes.func.isRequired,
  clearSelectedOrderItems: PropTypes.func,
  fetchResolutionModuleX: PropTypes.func,
  resolutionModuleXContent: PropTypes.func,
  getRefundStatus: PropTypes.func,
  refundData: PropTypes.shape({}),
  fetchTransitHelpModuleX: PropTypes.shape({}),
  transitHelpModuleXContent: PropTypes.shape({}),
  resolutionContentId: PropTypes.shape({}),
  setWrongItemData: PropTypes.func,
  wrongItemData: PropTypes.shape({}),
  selectedWrongItemResolution: PropTypes.shape({}),
  setOrderStatusInTransit: PropTypes.func,
  isOrderStatusTransit: PropTypes.bool,
  setToggleCards: PropTypes.func,
  toggleCards: PropTypes.bool,
  isPaidNExpediteRefundEnableApp: PropTypes.bool,
  selectedShipmentCard: PropTypes.string,
  setSelectedShipmentCard: PropTypes.func,
  navigation: PropTypes.shape({}),
  orderItems: PropTypes.shape([]).isRequired,
  cshForMonthsToDisplay: PropTypes.number,
  setFilteredList: PropTypes.func,
  filteredItems: PropTypes.shape([]),
  trackAnalyticsClick: PropTypes.func,
  trackAnalyticsPageView: PropTypes.func,
  isFetchingOrderItems: PropTypes.bool,
  clickAnalyticsData: PropTypes.shape({}),
  orderLookUpAPISwitch: PropTypes.bool,
  isMobileLiveChatEnabled: PropTypes.bool,
};
TellUsMore.defaultProps = {
  selectedOrderItems: {},
  shipmentData: {},
  selectedResolution: '',
  clearSelectedOrderItems: () => {},
  fetchResolutionModuleX: () => {},
  resolutionModuleXContent: () => {},
  getRefundStatus: () => {},
  refundData: {},
  fetchTransitHelpModuleX: {},
  transitHelpModuleXContent: {},
  resolutionContentId: {},
  setWrongItemData: () => {},
  wrongItemData: PropTypes.shape({}),
  selectedWrongItemResolution: PropTypes.shape({}),
  setOrderStatusInTransit: () => {},
  isOrderStatusTransit: false,
  setToggleCards: () => {},
  toggleCards: false,
  isPaidNExpediteRefundEnableApp: false,
  selectedShipmentCard: '',
  setSelectedShipmentCard: () => {},
  navigation: {},
  cshForMonthsToDisplay: 3,
  setFilteredList: () => {},
  filteredItems: [],
  trackAnalyticsClick: () => {},
  trackAnalyticsPageView: () => {},
  isFetchingOrderItems: true,
  clickAnalyticsData: {},
  orderLookUpAPISwitch: false,
  isMobileLiveChatEnabled: false,
};

renderBossBopisAndOlderOrderView.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  selectOrderAction: PropTypes.func.isRequired,
  navigation: PropTypes.shape({}).isRequired,
  orderLabels: PropTypes.shape({}).isRequired,
  info: PropTypes.string.isRequired,
  isLoggedIn: PropTypes.bool.isRequired,
  totalRecordsAdd: PropTypes.number.isRequired,
};

export default React.memo(TellUsMore);
