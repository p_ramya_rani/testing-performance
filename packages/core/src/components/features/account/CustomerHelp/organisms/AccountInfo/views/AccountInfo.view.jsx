// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import { Row, BodyCopy, Col } from '../../../../../../common/atoms';
import AccountOverviewTile from '../../../../../../common/molecules/AccountOverviewTile';
import withStyles from '../../../../../../common/hoc/withStyles';
import styles from '../styles/AccountInfo.style';
import AccountInfoSkelton from '../skelton/AccountInfoSkelton.view';

const AccountInfo = ({ labels, profileInfo, className, isFetching }) => {
  const { firstName, lastName, emailAddress, rewardsAccountNumber } = profileInfo;
  return (
    <Row fullBleed className={`${className} elem-pt-LRG account-info-wrap`}>
      <Col
        colSize={{
          small: 5,
          medium: 5,
          large: 5,
        }}
        className="overviewCol elem-mb-XL"
      >
        {isFetching ? (
          <div className="account-info-wrap-main">
            <AccountInfoSkelton />
          </div>
        ) : (
          <>
            <AccountOverviewTile
              title={`${getLabelValue(labels, 'lbl_merge_account_information')} `}
              dataLocatorPrefix="accountInfo"
              className="account-info-main"
            >
              <BodyCopy component="div">
                <Row fullBleed>
                  <Col
                    colSize={{
                      small: 5,
                      large: 10,
                      medium: 6,
                    }}
                  >
                    <BodyCopy
                      component="div"
                      data-locator="username"
                      fontSize="fs14"
                      fontWeight="extrabold"
                      fontFamily="secondary"
                    >
                      {firstName}
                      {` `}
                      {lastName}
                    </BodyCopy>
                    <BodyCopy
                      component="div"
                      data-locator="memberLbl"
                      fontSize="fs14"
                      fontFamily="secondary"
                    >
                      {`${getLabelValue(labels, 'lbl_merge_member_account_information')} `}
                      <BodyCopy
                        component="span"
                        data-locator="memberId"
                        fontSize="fs14"
                        fontFamily="secondary"
                      >
                        {rewardsAccountNumber}
                      </BodyCopy>
                    </BodyCopy>
                    <BodyCopy component="div" className="elem-mt-LRG">
                      <BodyCopy
                        component="div"
                        data-locator="emailLbl"
                        fontSize="fs14"
                        fontFamily="secondary"
                      >
                        {getLabelValue(labels, 'lbl_merge_email_address_account_information')}
                      </BodyCopy>
                      <BodyCopy
                        component="span"
                        data-locator="emailValue"
                        fontSize="fs14"
                        fontFamily="secondary"
                      >
                        {emailAddress}
                      </BodyCopy>
                    </BodyCopy>
                  </Col>
                </Row>
              </BodyCopy>
            </AccountOverviewTile>
          </>
        )}
      </Col>
    </Row>
  );
};

AccountInfo.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  profileInfo: PropTypes.shape({
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    emailAddress: PropTypes.string.isRequired,
    rewardsAccountNumber: PropTypes.string.isRequired,
  }).isRequired,
  className: PropTypes.string,
  isFetching: PropTypes.bool,
};

AccountInfo.defaultProps = {
  className: '',
  isFetching: false,
};

export default withStyles(AccountInfo, styles);

export { AccountInfo as AccountInfoVanilla };
