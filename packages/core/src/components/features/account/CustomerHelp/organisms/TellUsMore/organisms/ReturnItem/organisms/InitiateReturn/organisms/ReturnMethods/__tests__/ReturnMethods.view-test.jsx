import React from 'react';
import { shallow } from 'enzyme';
import { ReturnMethodsVanilla } from '../views/ReturnMethods.view';

describe('Return Method View', () => {
  it('should render correctly', () => {
    const props = {
      className: '',
      item: {},
      setReturnMethod: () => {},
    };
    const component = shallow(<ReturnMethodsVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should render correctly with item', () => {
    const props = {
      className: '',
      item: {
        id: 1,
        key: `lbl_returns_instore_returnMethod_key`,
        iconPath: `fast-shipping`,
        returnText: `lbl_returns_instore_returnMethod_text`,
        returnDesc: `lbl_returns_instore_returnMethod_desc`,
        shortDesc: {
          shortDescIcon: `location-icon`,
          shortDescText: `lbl_returns_instore_returnMethod_shortDescText`,
        },
      },
      setReturnMethod: () => {},
    };
    const component = shallow(<ReturnMethodsVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
