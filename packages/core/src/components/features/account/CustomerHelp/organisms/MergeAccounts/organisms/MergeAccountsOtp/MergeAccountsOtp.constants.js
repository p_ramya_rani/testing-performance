export const MERGE_ACCOUNT_OTP_CONSTANTS = {
  BTN_LOADER: 'BTN_LOADER',
  MERGE_ACCOUNT_VALIDATE_OTP: 'VALIDATE_OTP',
  MERGE_ACCOUNT_OTP_DETAIL: 'OTP_DETAIL',
  MERGE_ACCOUNT_GET_OTP: 'GET_OTP',
  MERGE_ACCOUNT_GET_OTP_SUCCESS: 'GET_OTP_SUCCESS',
  MERGE_ACCOUNT_GET_OTP_FAILURE: 'GET_OTP_FAILURE',
  MERGE_ACCOUNT_SET_UNIQUE_KEY: 'SET_UNIQUE_KEY',
  MERGE_ACCOUNT_CLEAR_OTP_DATA: 'CLEAR_OTP_DATA',
};

export const MERGE_ACCOUNT_ANALYTICS = {
  MERGE_ACCOUNT_TITLE: 'My Accounts & Coupons|I want to merge My PlACE account',
  MERGE_SUCCESSFUL: 'Account Merge Successful',
  MERGE_FAILED: 'Account Merge Failed',
  MERGE_MANY_ATTEMPTS: 'Too mani incorrect attempts',
  OTP_FLOW: 'OTP flow',
  ALTERNATE_FLOW: 'Alternate Flow',
};

export const MERGE_ACCOUNT_OTP_ERROR_CONSTANTS = {
  INVALID_OTP: 'INVALID_OTP',
  OTP_EXPIRED: 'OTP_EXPIRED',
  OTP_TIME_EXPIRED: 'OTP_TIME_EXPIRED',
  RETRY_LIMIT_EXCEEDS: 'RETRY_LIMIT_EXCEEDS',
  ACCOUNT_HOLD: 'ACCOUNT_HOLD',
};
