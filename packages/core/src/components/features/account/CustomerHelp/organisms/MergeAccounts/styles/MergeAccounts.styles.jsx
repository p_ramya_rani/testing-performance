// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  .merge-accounts-container {
    position: relative;
  }

  .bottom-section {
    display: flex;
  }
  .login {
    width: 166px;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-left: auto;
      margin-right: auto;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    }
  }
  .description {
    @media ${(props) => props.theme.mediaQuery.medium} {
      width: 70%;
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    }
  }
  .create-new-account {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      display: flex;
      margin-left: auto;
      margin-right: auto;
      justify-content: center;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED_1};
    }
  }
  .content {
    @media ${(props) => props.theme.mediaQuery.large} {
      width: 50%;
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      width: 60%;
    }
  }
  .create-one {
    cursor: pointer;
    text-decoration: underline;
  }
  .login-to-continue {
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXXL};
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
    }
  }
`;
export default styles;
