// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

export const modalStyles = css`
  && .TCPModal__InnerContent {
    text-align: center;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      padding: ${(props) => props.theme.spacing.ELEM_SPACING.MED_1};
      height: auto;
      width: 96%;
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      height: auto;
      max-width: ${(props) => props.theme.spacing.MODAL_WIDTH.MEDIUM};
    }
  }
`;

const styles = css`
  .modal-container {
    display: flex;
    flex-direction: column;
    align-items: center;
    margin-top: ${(props) => props.theme.spacing.LAYOUT_SPACING.LRG};
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.XXL};
      margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.XXL};
    }
    @media ${(props) => props.theme.mediaQuery.largeOnly} {
      margin-left: ${(props) => props.theme.spacing.LAYOUT_SPACING.XL};
      margin-right: ${(props) => props.theme.spacing.LAYOUT_SPACING.XL};
    }
  }

  .modal-heading {
    color: #f79e1b;
  }

  .modal-text-middle {
    color: ${(props) => props.theme.colors.TEXT.DARKERGRAY};
    text-align: center;
    @media ${(props) => props.theme.mediaQuery.largeOnly} {
      margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XXXL};
    }
  }

  .cta-container {
    display: flex;
    margin-bottom: ${(props) => props.theme.spacing.LAYOUT_SPACING.LRG};
  }

  .cta-btn {
    width: 264px;
    height: ${(props) => props.theme.spacing.ELEM_SPACING.XXXL};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      max-width: 155px;
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      max-width: ${(props) => props.theme.spacing.LAYOUT_SPACING.XXL};
    }
  }
`;

export default styles;
