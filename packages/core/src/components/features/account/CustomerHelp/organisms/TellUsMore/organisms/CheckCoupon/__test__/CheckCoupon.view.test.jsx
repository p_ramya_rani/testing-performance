// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { CheckCouponVanilla } from '../views/CheckCoupon.view';

describe('CheckCoupon template', () => {
  const props = {
    className: 'CheckCoupon',
    labels: {},
    orderLabels: {},
  };
  it('should render correctly', () => {
    const component = shallow(<CheckCouponVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('should render overlay', () => {
    const component = shallow(<CheckCouponVanilla show {...props} />);
    expect(component).toMatchSnapshot();
  });
});
