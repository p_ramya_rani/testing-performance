import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import BagPageSelectors from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.selectors';
import BAG_PAGE_ACTIONS from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.actions';

import CouponSelfHelp from '../views/CouponSelfHelp.view';
import {
  getCouponsLabels,
  getNeedHelpContent,
  getPlaceCashContent,
} from '../../../../../../../CnC/common/organism/CouponAndPromos/container/Coupon.selectors';

const getUpdatedLabels = (needHelpContentLabels, needHelpRichText, placeCashContent) => {
  return {
    ...needHelpContentLabels,
    NEED_HELP_RICH_TEXT: needHelpRichText,
    PLACE_CASH_CONTENT: placeCashContent,
  };
};
function CouponSelfHelpContainer({
  labels,
  orderLabels,
  isLoggedIn,
  needHelpContentLabels,
  placeCashContent,
  needHelpRichText,
  placeCashContentId,
  fetchNeedHelpContent,
  needHelpContentId,
}) {
  useEffect(() => {
    fetchNeedHelpContent([placeCashContentId]);
    fetchNeedHelpContent([needHelpContentId]);
  }, []);

  const updateLabels = getUpdatedLabels(needHelpContentLabels, needHelpRichText, placeCashContent);
  return (
    <CouponSelfHelp
      orderLabels={orderLabels}
      isLoggedIn={isLoggedIn}
      labels={labels}
      needHelpContentLabels={updateLabels}
    />
  );
}
CouponSelfHelpContainer.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  fetchNeedHelpContent: PropTypes.func.isRequired,
  placeCashContentId: PropTypes.shape({}).isRequired,
  needHelpContentId: PropTypes.shape({}).isRequired,
  isLoggedIn: PropTypes.bool.isRequired,
  orderLabels: PropTypes.shape({}).isRequired,
  needHelpContentLabels: PropTypes.shape({}).isRequired,
  needHelpRichText: PropTypes.string.isRequired,
  placeCashContent: PropTypes.string.isRequired,
};

export const mapDispatchToProps = (dispatch) => {
  return {
    fetchNeedHelpContent: (contentIds) => {
      dispatch(BAG_PAGE_ACTIONS.fetchModuleX(contentIds));
    },
  };
};
export const mapStateToProps = (state) => {
  return {
    needHelpContentLabels: getCouponsLabels(state),
    needHelpRichText: getNeedHelpContent(state),
    placeCashContent: getPlaceCashContent(state),
    needHelpContentId: BagPageSelectors.getNeedHelpContent(state),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(CouponSelfHelpContainer);
