// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { Linking, Text } from 'react-native';
import { BodyCopy, Image } from '@tcp/core/src/components/common/atoms';
import PropTypes from 'prop-types';
import { getAPIConfig } from '@tcp/core/src/utils';
import { getLabelValue, getBrand } from '@tcp/core/src/utils/utils';
import { BodyCopyWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';
import { ReasonCodeRectangle } from '../../../../../styles/CustomerHelp.style.native';
import { BRAND_ID } from '../../../../../CustomerHelp.constants';
import {
  RowContainer,
  CustomerServiceHelpContainer,
  BlueAnchor,
} from '../styles/CustomerServiceHelp.style.native';
import { ViewWithSpacing } from '../../../../../../../../common/atoms/styledWrapper';
import LiveChat from '../../../../../molecules/LiveChat/views/LiveChat.view';

const phoneIcon = require('../../../../../../../../../../../mobileapp/src/assets/images/Phone.png');
const emailIcon = require('../../../../../../../../../../../mobileapp/src/assets/images/Email.png');
const chatIcon = require('../../../../../../../../../../../mobileapp/src/assets/images/Chat.png');

const CustomerServiceHelp = ({
  labels,
  helpMessage,
  selectedReasonCode,
  orderDetailsData,
  selectedOrderItems,
  navigation,
  isLiveChatEnabled,
  isUserLoggedIn,
  trackAnalyticsClick,
}) => {
  let callUsLabel = getLabelValue(labels, 'lbl_help_call_us_number');

  const [isOpenChat, setOpenChatForm] = React.useState(false);

  const { country } = getAPIConfig();
  if (country === 'US') {
    callUsLabel = getLabelValue(labels, 'lbl_help_call_us_number');
  }

  const brandId = getBrand();
  const hereLink =
    brandId === BRAND_ID.TCP
      ? getLabelValue(labels, 'lbl_contact_us_tcp_prod_href')
      : getLabelValue(labels, 'lbl_contact_us_gym_prod_href');

  const updateChatForm = () => {
    setOpenChatForm(!isOpenChat);
    if (isOpenChat) {
      trackAnalyticsClick(
        {
          liveChat_name_p145: '1',
          customEvents: ['event145'],
        },
        { name: 'livechat_open', module: 'account', pageDate: '' }
      );
    }
  };

  return (
    <CustomerServiceHelpContainer>
      {!!helpMessage && (
        <BodyCopyWithSpacing
          spacingStyles="margin-top-SM margin-left-XS margin-right-XS margin-bottom-SM"
          text={helpMessage}
          fontFamily="secondary"
          fontSize="fs16"
        />
      )}
      <ReasonCodeRectangle>
        <RowContainer>
          <ViewWithSpacing spacingStyles="margin-right-MED margin-left-XS">
            <Image source={phoneIcon} alt="" width="19px" height="19px" />
          </ViewWithSpacing>
          <Text>{getLabelValue(labels, 'lbl_help_call_us')}</Text>

          <BlueAnchor
            onPress={() => Linking.openURL(`tel:+${callUsLabel}`)}
            anchorVariation="secondary"
            underlineBlue
            fontSize="fs14"
            text={callUsLabel}
            color="red"
          />
        </RowContainer>
      </ReasonCodeRectangle>
      <ReasonCodeRectangle onPress={() => Linking.openURL(hereLink)}>
        <RowContainer onPress={() => Linking.openURL(hereLink)}>
          <ViewWithSpacing spacingStyles="margin-right-MED margin-left-XS">
            <Image source={emailIcon} width="19px" height="19px" />
          </ViewWithSpacing>
          <BodyCopy
            fontWeight="semibold"
            fontSize="fs14"
            fontFamily="secondary"
            color="text.darkgray"
            text={getLabelValue(labels, 'lbl_help_email_us')}
          />
        </RowContainer>
      </ReasonCodeRectangle>
      {isLiveChatEnabled && (
        <>
          <ReasonCodeRectangle onPress={updateChatForm}>
            <RowContainer onPress={updateChatForm}>
              <ViewWithSpacing spacingStyles="margin-right-MED margin-left-XS">
                <Image source={chatIcon} alt="" width="19px" height="19px" />
              </ViewWithSpacing>
              <BodyCopy
                fontWeight="semibold"
                fontSize="fs14"
                fontFamily="secondary"
                color="text.darkgray"
                text={getLabelValue(labels, 'lbl_help_live_chat')}
              />
            </RowContainer>
          </ReasonCodeRectangle>
          <LiveChat
            isOpenChat={isOpenChat}
            closeChatWindow={updateChatForm}
            isUserLoggedIn={isUserLoggedIn}
            navigation={navigation}
            selectedReasonCode={selectedReasonCode}
            orderDetailsData={orderDetailsData}
            selectedOrderItems={selectedOrderItems}
          />
        </>
      )}
    </CustomerServiceHelpContainer>
  );
};

CustomerServiceHelp.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  helpMessage: PropTypes.string.isRequired,
  selectedReasonCode: PropTypes.shape({}),
  orderDetailsData: PropTypes.shape({}),
  selectedOrderItems: PropTypes.shape({}),
  isLiveChatEnabled: PropTypes.bool,
  isUserLoggedIn: PropTypes.bool,
  navigation: PropTypes.func.isRequired,
  trackAnalyticsClick: PropTypes.func.isRequired,
};

CustomerServiceHelp.defaultProps = {
  selectedReasonCode: {},
  orderDetailsData: {},
  selectedOrderItems: {},
  isLiveChatEnabled: false,
  isUserLoggedIn: false,
};
export default React.memo(CustomerServiceHelp);
