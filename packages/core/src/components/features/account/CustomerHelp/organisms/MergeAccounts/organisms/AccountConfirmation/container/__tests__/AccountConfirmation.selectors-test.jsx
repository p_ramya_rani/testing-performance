// 9fbef606107a605d69c0edbcd8029e5d
import { fromJS } from 'immutable';
import {
  getAccountConfirmationFailure,
  getAccountConfirmationSuccess,
} from '../AccountConfirmation.selectors';

describe('Merge account account confirmation selector', () => {
  const accountConfirmationState = fromJS({
    error: null,
    accountConfirmationSuccess: null,
  });

  const state = {
    /* reducer-key:state */
    AccountConfirmation: accountConfirmationState,
  };

  it('Merge account confirmation should return error state', () => {
    expect(getAccountConfirmationFailure(state)).toEqual(accountConfirmationState.get('error'));
  });

  it('Merge account  confirmation should return success state', () => {
    expect(getAccountConfirmationSuccess(state)).toEqual(
      accountConfirmationState.get('accountConfirmationSuccess')
    );
  });
});
