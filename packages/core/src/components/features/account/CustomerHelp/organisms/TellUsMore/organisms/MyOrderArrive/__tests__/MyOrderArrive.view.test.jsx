import React from 'react';
import { shallow } from 'enzyme';
import { MyOrderArriveVanilla } from '../views/MyOrderArrive.view';

describe('CheckCoupon template', () => {
  const props = {
    className: '',
    labels: {},
    orderLabels: {},
    action: {},
    isLiveChatEnabled: true,
  };
  it('should render correctly', () => {
    const component = shallow(<MyOrderArriveVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
