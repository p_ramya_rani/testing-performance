// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  .disable-message-with-icon {
    display: flex;
    @media ${(props) => props.theme.mediaQuery.large} {
      position: absolute;
      left: ${(props) => props.theme.spacing.LAYOUT_SPACING.LRGS};
    }
  }
  .with-info-message {
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XXL};
    }
  }
  .review-button-tell-us-more {
    display: flex;
    justify-content: center;
  }
  .reason-code-rectangle {
    min-height: 59px;
    border-radius: 8px;
    display: flex;
    align-items: center;
    justify-content: flex-start;
    border: solid 1px ${(props) => props.theme.colorPalette.gray[600]};
    cursor: pointer;
    box-sizing: border-box;
    background: ${(props) => props.theme.colors.WHITE};
    .textColor {
      color: ${(props) => props.theme.colors.PRIMARY.DARK};
    }
    padding: 10px;
  }
  .contact-method {
    background-color: ${(props) => props.theme.colors.WHITE};
  }
  .blue-link {
    color: ${(props) => props.theme.colors.PRIMARY.BLUE};
  }
  .email-help {
    display: flex;
    flex-wrap: wrap;
  }
  .help-message-with-icon {
    display: flex;
  }
  .no-decoration {
    text-decoration: none;
  }
  .return-policy {
    text-decoration: underline;
  }

  .justify-center {
    justify-content: center;
  }
`;
export default styles;
