// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { AccountInfoVanilla } from '../AccountInfo.view';

describe('AccountInfo component', () => {
  it('should render correctly', () => {
    const props = {
      labels: {},
      profileInfo: {
        firstName: 'First',
        lastName: 'Last',
        emailAddress: 'first.last@tcp.com',
        rewardsAccountNumber: 'B100000012222',
      },
    };
    const component = shallow(<AccountInfoVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
