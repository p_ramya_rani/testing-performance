// 9fbef606107a605d69c0edbcd8029e5d

import ACCOUNT_CONFIRMATION_CONSTANTS from '../AccountConfirmation.constants';

export const validateMember = (payload) => ({
  type: ACCOUNT_CONFIRMATION_CONSTANTS.VALIDATE_MEMBER,
  payload,
});

export const accountConfirmationFailure = (payload) => ({
  type: ACCOUNT_CONFIRMATION_CONSTANTS.ACCOUNT_CONFIRMATION_FAILED,
  payload,
});

export const accountConfirmationRequest = (payload) => ({
  type: ACCOUNT_CONFIRMATION_CONSTANTS.ACCOUNT_CONFIRMATION_REQUEST,
  payload,
});

export const accountConfirmationSuccess = (payload) => ({
  type: ACCOUNT_CONFIRMATION_CONSTANTS.ACCOUNT_CONFIRMATION_SUCCESS,
  payload,
});

export const resetAccountConfirmationRequest = () => ({
  type: ACCOUNT_CONFIRMATION_CONSTANTS.RESET_ACCOUNT_CONFIRMATION,
});
