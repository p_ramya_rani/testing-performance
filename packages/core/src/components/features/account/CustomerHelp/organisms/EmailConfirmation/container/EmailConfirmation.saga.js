// 9fbef606107a605d69c0edbcd8029e5d
import { call, takeLatest, put, take, select } from 'redux-saga/effects';
import EMAIL_CONFIRMATION_CONSTANTS from '../EmailConfirmation.constants';
import mergeAccountEmailAddressConfirmation from '../../../../../../../services/abstractors/account/EmailAddressConfirmation';
import {
  emailConfirmationFailure,
  emailConfirmationSuccess,
  setEmailConfirmationLoader,
} from './EmailConfirmation.action';
import { getOtpDetails } from '../../../../../../../services/abstractors/customerHelp/customerHelp';
import {
  getOtpSuccess,
  setOtpUniqueKey,
} from '../../MergeAccounts/organisms/MergeAccountsOtp/container/MergeAccountOtp.action';
import {
  setFirstTimeOtp,
  setShowConfirmationModal,
} from '../../MergeAccounts/container/MergeAccounts.actions';
import { getIsMergeConfirmed } from '../../MergeAccounts/container/MergeAccounts.selectors';
import MERGE_ACCOUNT_CONSTANTS from '../../MergeAccounts/MergeAccounts.constants';

export function* emailConfirmation({ payload }) {
  const { emailAddress } = payload;
  try {
    const response = yield call(mergeAccountEmailAddressConfirmation, emailAddress);

    if (response && response.body) {
      yield put(setEmailConfirmationLoader(false));
      yield put(setShowConfirmationModal(emailAddress));
      yield take(MERGE_ACCOUNT_CONSTANTS.SET_MERGE_CONFIRMED);
      const isMergeConfirmed = yield select(getIsMergeConfirmed);
      if (isMergeConfirmed) {
        yield put(setEmailConfirmationLoader(true));
        const responseOtp = yield call(getOtpDetails, { payload: emailAddress });
        if (responseOtp) {
          yield put(getOtpSuccess(responseOtp?.body));
          yield put(setOtpUniqueKey(responseOtp?.body?.data));
          yield put(setFirstTimeOtp(true));
          yield put(emailConfirmationSuccess(response.body));
        } else {
          yield put(emailConfirmationFailure(responseOtp));
        }
      }
    } else return yield put(emailConfirmationFailure(response));
    return null;
  } catch (err) {
    if (err?.errorCode === 'ACCOUNT_HOLD') {
      yield put(emailConfirmationFailure(err));
    }
    return yield put(emailConfirmationFailure(err));
  } finally {
    yield put(setEmailConfirmationLoader(false));
  }
}

export function* emailConfirmationSaga() {
  yield takeLatest(EMAIL_CONFIRMATION_CONSTANTS.EMAIL_CONFIRMATION_REQUEST, emailConfirmation);
}

export default emailConfirmationSaga;
