// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { ConfirmationVanilla } from '../views/Confirmation.view';

const props = {
  isLoggedIn: true,
  orderDetailsData: {
    orderNumber: '344069007',
    shippingMethod: 'Express',
    estimatedDeliveryDate: '',
    emailAddress: 'SANT.HOTA99@GMAIL.COM',
    orderDate: '2021-09-29 08:11:30.914',
    pickUpExpirationDate: null,
    pickedUpDate: '2021-09-30 02:10:00',
    shippedDate: '2021-09-30 02:10:00',
    orderStatus: 'order shipped',
    encryptedEmailAddress: 'heAL2XJwMyghv0AMtl9iXCOY487kfb3p',
    status: 'lbl_orders_statusOrderShipped',
    trackingNumber: null,
    trackingUrl: null,
    isBopisOrder: false,
    isBossOrder: false,
    orderType: 'ECOM',
    bossMaxDate: null,
    bossMinDate: null,
    orderLabels: {
      orders: {},
    },
    orderItems: [
      {
        orderStatus: 'Delivered',
        trackingNumber: '7NW64107Y600304266_1',
        trackingUrl:
          'https://childrensplace.narvar.com/childrensplace/tracking/ups?tracking_numbers=7NW64107Y600304266_1&service=UG&ozip=35968&dzip=46350&locale=en_US&order_number=344069001&order_date=2021-09-29T07:56:00.000-04:00&ship_date=2021-09-30T06:41:30.000-04:00',
        shippedDate: '9/20/21 00:00',
        items: [
          {
            productInfo: {
              appeasementApplied: false,
              fit: null,
              pdpUrl:
                'http://uatlive1.childrensplace.com/us/p/Boys-Active-Fleece-Jogger-Pants-3016287-7R',
              name: 'Boys Active Fleece Jogger Pants',
              imagePath:
                'https://test1.theplace.com/image/upload/ecom/assets/products/tcp/3016287/3016287_7R.jpg',
              thumbnailImgPath: '3016287/3016287_7R.jpg',
              upc: '00194936100764',
              lineNumber: '1517870001',
              variantNo: '3016287008',
              size: 'S (5/6)',
              color: { name: 'REDWOOD', imagePath: '3016287/3016287_7R_swatch.jpg' },
            },
            itemInfo: {
              listPrice: 19.95,
              offerPrice: 19.95,
              linePrice: 79.8,
              quantity: 4,
              quantityCanceled: 0,
              quantityShipped: 4,
              quantityReturned: 0,
              quantityOOS: 0,
              itemBrand: 'TCP',
              taxUnitPrice: 1.4,
            },
            trackingInfo: [
              {
                carrier: 'UPS',
                quantity: 4,
                shipDate: '9/20/21 00:00',
                carrierDeliveryDate: null,
                shipVia: 'UGNR',
                status: 'Delivered',
                trackingUrl:
                  'https://childrensplace.narvar.com/childrensplace/tracking/ups?tracking_numbers=7NW64107Y600304266_1&service=UG&ozip=35968&dzip=46350&locale=en_US&order_number=344069001&order_date=2021-09-29T07:56:00.000-04:00&ship_date=2021-09-30T06:41:30.000-04:00',
                trackingNbr: '7NW64107Y600304266_1',
                refundAmount: 0,
                refundDate: null,
                appeasement: 0,
                actualDeliveryDate: '2021-10-01 16:19:00',
                returnInitiatedDate: null,
                isCSHReturn: null,
              },
            ],
            isReturnedItem: false,
            itemStatus: 'Delivered',
            quantity: 4,
            lineNumber: '1517870001_7NW64107Y600304266_1_4',
            orderItemStatus: 'Delivered',
          },
        ],
      },
    ],
  },
  selectedReasonCode: {
    id: '/10',
    label: "I'd like to return some item",
    external: 0,
    title: '',
    action: 'RETURN_ITEMS',
    target: '_modal',
    __typename: 'Button',
  },
  selectedOrderItems: {
    '1517870010_7NW64107Y600304405_4': 1,
  },
  cshForMonthsToDisplay: 90,
  loggedInUserEmail: 'sant.hota99@gmail.com',
  itemLevelReturns: {},
  shipmentData: {
    status: 'success',
    orderId: '344069007',
    trackingDetails: '[{…}]',
  },
  selectedResolution: null,
  selectedWrongItemResolution: null,
  isFetching: false,
  resolutionModuleXContent: {
    name: 'moduleX',
    richText:
      '<style>\r\n    .heading-modify,\r\n    .list-heading,\r\n    .footer-note,\r\n    .thank-you,\r\n    li {\r\n        font-family: Nunito;\r\n        font-size: 14px;\r\n        line-height: normal;\r\n    }\r\n\r\n    .heading-modify {\r\n        font-weight: 700;\r\n    }\r\n\r\n    .heading {\r\n        margin-bottom: 14px;\r\n    }\r\n\r\n    ul {\r\n        margin-top: 0px;\r\n    }\r\n\r\n    .list-heading {\r\n        margin-top:16px;\r\n        margin-bottom: 0px;\r\n    }\r\n\r\n    .footer-note {\r\n        margin: 16px 0px;\r\n    }\r\n\r\n    .lis...',
  },
  returnInStoreModuleXContent: {
    name: 'moduleX',
    richText:
      "<style> \r\n  li{font-family: Nunito; font-size: 14px; \r\n    font-weight:600;line-height: normal;}\r\n  .list-container{color: #1a1a1a;background-color:#f3f3f3;padding-top:10px;padding-bottom:10px;padding-left:1.7em;padding-right:5px;margin:12px 0 2px 0;border-radius:4px;}\r\n  .list-container li{ margin: 7px 0px;}</style>  \r\n  <ol class='list-container'>\r\n  <li style='list-style-type:decimal;'>Locate your nearest store</li>\r\n  <li style='list-style-type:decimal;'>Bring your receipt (printed or on mob...",
  },
  returnByMailModuleXContent: {
    name: 'moduleX',
    richText:
      "<style> \r\n  li{font-family: Nunito; font-size: 14px; \r\n    font-weight:600;line-height: normal;}\r\n  .list-container{color: #1a1a1a;background-color:#f3f3f3;padding-top:10px;padding-bottom:10px;padding-left:1.7em;padding-right:5px;margin:12px 0 2px 0;border-radius:4px;}\r\n  .list-container li{ margin: 7px 0px;}</style>  \r\n  <ol class='list-container'>\r\n  <li style='list-style-type:decimal;'>Fill out the packing slip or print your receipt</li>\r\n  <li style='list-style-type:decimal;'>Pack it with th...",
  },
  wrongItemData: {},
  transitHelpModuleXContent: {},
  refundData: [],
  cancelStatusData: {},
  shipmentDelayedStatusData: {},
  returnByMailContentId: {
    name: 'return_by_mail_instructions',
    contentId: '997775bf-a84e-411f-a3d2-ccf232fed4b9',
    __typename: 'ReferredContent',
  },
  returnInStoreContentId: {
    name: 'return_in_store_instructions',
    contentId: '18b0abfa-a3ae-4e9a-aa70-e9c580e45684',
    __typename: 'ReferredContent',
  },
  isLoadMoreActive: true,
  currentPageNumber: 3,
  isOrderStatusTransit: null,
  toggleCards: false,
  isPaidNExpediteRefundEnable: true,
  isPaidNExpediteRefundEnableApp: true,
  selectedShipmentCard: null,
  clickAnalyticsData: {
    orderHelp_path_cd128: "I'd like to return some items",
    orderHelpSelectedReason: "I'd like to return some items",
  },
  filteredItems: null,
  isFetchingOrderItems: false,
  getOrderSummary: {},
  orderLookUpAPISwitch: true,
  isLiveChatEnabled: true,
  isMobileLiveChatEnabled: true,
  preReasonFlag: false,
  isCouponSelfHelpEnabled: true,
  isMergeMyPlaceAccount: false,
  topLevelReasonCodes: [
    '{__typename: "Button", action: "EXISTING_ORDER", ex…}',
    '{__typename: "Button", action: "ACCOUNT_AND_COUPONS…}',
    '{__typename: "Button", action: "SOMETHING_ELSE", ex…}',
  ],
  resetPasswordError: null,
  accountStatus: 'UNLOCKED',
  mergeStatus: null,
  mergeError: null,
  victimEmailAddress: 'SANT.HOTA99@GMAIL.COM',
  survivorEmailAddress: '',
  selectedReturnMethod: 'BY_MAIL',
  isReturnItemDisplay: true,
  orderReturnDays: 0,
  userAccountId: 'B10000089983244',
  isInitiateReturnSuccessful: true,
  cshReturnItemOlderDays: 5,
  isTellUsMorePage: false,
};

describe('Confirmation Template', () => {
  it('should render Confirmation Template', () => {
    const component = shallow(<ConfirmationVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
