// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { BodyCopy, Image } from '@tcp/core/src/components/common/atoms';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import { CUSTOMER_HELP_PAGE_TEMPLATE } from '@tcp/core/src/components/features/account/CustomerHelp/CustomerHelp.constants';
import differenceInCalendarDays from 'date-fns/differenceInCalendarDays';
import { View, FlatList } from 'react-native';
import Anchor from '@tcp/core/src/components/common/atoms/Anchor';
import {
  BodyCopyWithSpacing,
  ViewWithSpacing,
} from '@tcp/core/src/components/common/atoms/styledWrapper';
import OrderTile from '../../../../../molecules/OrderTile';
import { NoOrderContainer, LabelContainer } from '../styles/RecentOrder.style.native';
import OrderTileSkeleton from '../../../skeleton';

const noOrderImage = require('../../../../../../../../../../../mobileapp/src/assets/images/no-order.png');

const scrollTo = (ordersListItems, currentPageNumber, seeMore, flatListRef) => {
  let section = 0;
  if (
    ordersListItems &&
    ordersListItems.orders &&
    ordersListItems.orders.length > 1 &&
    seeMore &&
    currentPageNumber > 0
  ) {
    const pageNumber = ordersListItems.orders.length - CUSTOMER_HELP_PAGE_TEMPLATE.ORDER_SIZE;
    section = seeMore && currentPageNumber > 0 ? pageNumber : 0;
    if (section && flatListRef && flatListRef.current) {
      setTimeout(() => {
        flatListRef.current.scrollToIndex({
          animated: true,
          index: section,
        });
      }, 100);
    }
  }
};

const RecentOrders = ({
  ordersListItems,
  orderLabels,
  labels,
  cshForMonthsToDisplay,
  clearSelectedReasonCode,
  clearSelectedResolution,
  clearSelectedOrderItems,
  navigation,
  setWrongItemData,
  setOrderStatusInTransit,
  setSelectedShipmentCard,
  setToggleCards,
  setFilteredList,
  seeMore,
  currentPageNumber,
}) => {
  const flatListRef = React.useRef();
  const navigateToTellUsMore = (item) => {
    navigation.navigate(CUSTOMER_HELP_PAGE_TEMPLATE.TELL_US_MORE, {
      orderId: item.orderNumber,
      fromPage: CUSTOMER_HELP_PAGE_TEMPLATE.SELECT_ORDER,
    });
    if (flatListRef && flatListRef.current) {
      flatListRef.current.scrollToIndex({
        animated: true,
        index: 0,
      });
    }
  };

  const getOrderItem = (item) => {
    const {
      isEcomOrder,
      orderNumber,
      orderDate,
      currencySymbol,
      orderTotal,
      status,
      imgPath,
      itemCount,
      productName,
      orderStatus,
    } = item;
    let isOlderItem = false;
    const dateDifference = differenceInCalendarDays(new Date(), new Date(orderDate));
    if (dateDifference > cshForMonthsToDisplay) {
      isOlderItem = true;
    }

    return {
      orderNumber,
      orderDate,
      currencySymbol,
      grandTotal: orderTotal,
      isEcomOrder,
      isOlderItem,
      onRecentOrderPage: true,
      status,
      productName,
      imagePath: imgPath.includes('https') ? imgPath : `https://${imgPath}`,
      ItemsCount: itemCount,
      orderStatus,
    };
  };

  const handleOrderItems = (item) => {
    const { isEcomOrder, isOlderItem } = getOrderItem(item);
    if (isEcomOrder && !isOlderItem) {
      setWrongItemData({});
      clearSelectedReasonCode();
      clearSelectedResolution();
      clearSelectedOrderItems();
      setOrderStatusInTransit(false);
      setToggleCards(false);
      setSelectedShipmentCard('');
      setFilteredList(null);
      navigateToTellUsMore(item);
    }
  };

  const customerServiceHref = `/us${getLabelValue(labels, 'lbl_contact_us_href', 'orders')}`;

  const getAnchor = () => {
    return (
      <Anchor
        text={getLabelValue(labels, 'lbl_contact_us')}
        anchorVariation="secondary"
        fontSize="fs14"
        fontFamily="primary"
        underlineBlue
        url={customerServiceHref}
      />
    );
  };

  const getHeading = () => {
    return (
      <ViewWithSpacing spacingStyles="margin-left-XS margin-top-XS">
        <BodyCopy
          fontFamily="secondary"
          fontSize="fs16"
          fontWeight="bold"
          text={getLabelValue(labels, 'lbl_your_order')}
          color="text.darkgray"
        />
      </ViewWithSpacing>
    );
  };

  const getItemLayout = (data, index) => ({ length: 170, offset: 170 * index, index });

  const renderItem = ({ item }) => (
    <View>
      <OrderTile
        orderLabels={orderLabels}
        orderDetailsData={getOrderItem(item)}
        labels={labels}
        item={item}
        onPressHandler={handleOrderItems}
      />
    </View>
  );

  const contentSizeChange = () =>
    scrollTo(ordersListItems, currentPageNumber, seeMore, flatListRef);

  return ordersListItems && ordersListItems.orders && ordersListItems.orders.length > 1 ? (
    <>
      {ordersListItems && ordersListItems.orders ? (
        <>
          <FlatList
            ref={flatListRef}
            onContentSizeChange={contentSizeChange}
            onEndReachedThreshold={0.7}
            initialScrollIndex={0}
            data={ordersListItems.orders}
            renderItem={renderItem}
            keyExtractor={(item) => item}
            ListHeaderComponent={getHeading}
            disableVirtualization={true}
            getItemLayout={getItemLayout}
          />
        </>
      ) : (
        <OrderTileSkeleton />
      )}
    </>
  ) : (
    <>
      <NoOrderContainer>
        <Image source={noOrderImage} height="53" width="56" alt="empty-box" />

        <LabelContainer>
          <BodyCopyWithSpacing
            fontFamily="secondary"
            fontSize="fs14"
            fontWeight="semibold"
            text={getLabelValue(labels, 'lbl_no_orders_initial', 'orders')}
            color="text.darkgray"
          />
          {getAnchor()}

          <ViewWithSpacing spacingStyles="margin-left-XXS">
            <BodyCopy
              fontFamily="secondary"
              fontSize="fs14"
              fontWeight="semibold"
              text={getLabelValue(labels, 'lbl_no_orders_ending', 'orders')}
              color="text.darkgray"
            />
          </ViewWithSpacing>
        </LabelContainer>
      </NoOrderContainer>
    </>
  );
};

RecentOrders.propTypes = {
  ordersListItems: PropTypes.shape([]).isRequired,
  orderLabels: PropTypes.shape({}).isRequired,
  labels: PropTypes.shape({}).isRequired,
  cshForMonthsToDisplay: PropTypes.number.isRequired,
  navigation: PropTypes.shape({}).isRequired,
  clearSelectedOrderItems: PropTypes.func,
  clearSelectedReasonCode: PropTypes.func,
  clearSelectedResolution: PropTypes.func,
  setWrongItemData: PropTypes.func,
  setOrderStatusInTransit: PropTypes.func,
  setToggleCards: PropTypes.func,
  setSelectedShipmentCard: PropTypes.func,
  setFilteredList: PropTypes.func,
};

RecentOrders.defaultProps = {
  clearSelectedReasonCode: () => {},
  clearSelectedOrderItems: () => {},
  clearSelectedResolution: () => {},
  setWrongItemData: () => {},
  setOrderStatusInTransit: () => {},
  setToggleCards: () => {},
  setSelectedShipmentCard: () => {},
  setFilteredList: () => {},
};

export default RecentOrders;
