// 9fbef606107a605d69c0edbcd8029e5d
import { call, put, takeLatest } from 'redux-saga/effects';
import { emailConfirmationSaga, emailConfirmation } from '../EmailConfirmation.saga';
import EMAIL_CONFIRMATION_CONSTANTS from '../../EmailConfirmation.constants';
import { emailConfirmationFailure, emailConfirmationSuccess } from '../EmailConfirmation.action';
import mergeAccountEmailAddressConfirmation from '../../../../../../../../services/abstractors/account/EmailAddressConfirmation';
import MERGE_ACCOUNT_CONSTANTS from '../../../MergeAccounts/MergeAccounts.constants';

describe('MergeAccount emailaddress confirmaqtion saga', () => {
  let gen;
  const payload = {};

  beforeEach(() => {
    gen = emailConfirmation({ payload });
  });

  it('should return correct takeLatest effect', () => {
    gen = emailConfirmationSaga();
    expect(gen.next().value).toEqual(
      takeLatest(EMAIL_CONFIRMATION_CONSTANTS.EMAIL_CONFIRMATION_REQUEST, emailConfirmation)
    );
    expect(gen.next().done).toBeTruthy();
  });

  it('should test emailconfirmation success', () => {
    const res = {
      body: {
        status: 'SUCCESS',
        timestamp: '2021-09-06T08:56:34.528516',
        data: {
          status: 'Active',
          email: 'test@test.com',
        },
      },
    };

    const responseOtp = {
      body: {
        data: '1633553777990529569',
        status: 'SUCCESS',
        timestamp: '2021-10-06T20:56:18.000562',
      },
    };
    expect(gen.next(res).value).toEqual(
      call(mergeAccountEmailAddressConfirmation, payload.emailAddress)
    );
    gen.next(res);
    gen.next();
    gen.next(MERGE_ACCOUNT_CONSTANTS.SET_MERGE_CONFIRMED);
    gen.next(true);
    gen.next(true);
    gen.next(responseOtp);
    gen.next(responseOtp.body);
    gen.next(responseOtp.body.data);
    gen.next();
    expect(gen.next(res).value).toEqual(put(emailConfirmationSuccess(res.body)));
    gen.next();
    expect(gen.next().done).toBeTruthy();
  });

  it('should fail emailconfirmation', () => {
    const err = {
      status: 'ERROR',
      timestamp: '2021-09-07T08:38:37.264499',
      errors: [
        {
          errorCode: 'Internal Server Error',
          errorMessage: 'Error occured while fetching the members details from Brierley',
          errorParameters: null,
          errorType: null,
        },
      ],
      exceptionType: 'RUNTIME_EXCEPTION',
    };
    expect(gen.next().value).toEqual(
      call(mergeAccountEmailAddressConfirmation, payload.emailAddress)
    );
    expect(gen.next(err).value).toEqual(put(emailConfirmationFailure(err)));
    gen.next();
    expect(gen.next().done).toBeTruthy();
  });
});
