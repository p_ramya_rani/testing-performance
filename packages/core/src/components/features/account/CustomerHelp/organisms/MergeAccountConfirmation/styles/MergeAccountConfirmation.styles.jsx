import { css } from 'styled-components';

const styles = css`
  &.info-container {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
  }

  .failure-text {
    color: ${(props) => props.theme.colorPalette.red[500]};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      font-size: ${(props) => props.theme.typography.fontSizes.fs16};
    }
  }

  .success-text {
    color: ${(props) => props.theme.colorPalette.green[500]};
    @media ${(props) => props.theme.mediaQuery.large} {
      margin: auto auto ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    }
  }

  .img-container {
    margin-bottom: 0px;
  }

  &.info {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  &.customer-service-help {
    margin: 0 ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  }

  &.help-btn {
    display: flex;
    justify-content: center;
    align-items: center;

    background-color: ${(props) => props.theme.colors.PRIMARY.PALEGRAY};
    padding: ${(props) => props.theme.spacing.ELEM_SPACING.MED_1} 0;

    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      padding: ${(props) => props.theme.spacing.ELEM_SPACING.MED_1}
        ${(props) => props.theme.spacing.ELEM_SPACING.XL}
        ${(props) => props.theme.spacing.ELEM_SPACING.XXXL};
      ${(props) => props.theme.spacing.ELEM_SPACING.XL}
    }
  }

  .merge-success-detail {
    width: 50%;
    text-align: center;
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  }

  .merge-success-text {
    width: 50%;
    text-align: center;
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    font-weight: 600;
    @media ${(props) => props.theme.mediaQuery.large} {
      font-size: ${(props) => props.theme.spacing.ELEM_SPACING.REG};
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      font-size: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    }

    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: 78%;
      font-size: ${(props) => props.theme.spacing.ELEM_SPACING.REG};
    }
  }

  .footer-button {
    padding-left: ${(props) => props.theme.spacing.ELEM_SPACING.MED_1};
    padding-right: ${(props) => props.theme.spacing.ELEM_SPACING.MED_1};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      font-size: ${(props) => props.theme.typography.fontSizes.fs11};
    }
  }
`;
export default styles;
