// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import { Image, BodyCopy, SelectBox } from '@tcp/core/src/components/common/atoms';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import MiniBagSelect from '@tcp/web/src/components/features/CnC/MiniBag/molecules/MiniBagSelectBox/MiniBagSelectBox';
import { getIconPath } from '@tcp/core/src/utils';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import styles from '../styles/OrderItem.style';
import selectBoxStyle from '../styles/SelectBox.style';
import { ORDER_ITEM_STATUS, REASON_CODES } from '../../../../../CustomerHelp.constants';
import { formatAmount } from '../../../../../util/utility';

const renderImageContainer = (itemBrand, name, imagePath, isGiftCardItem) => {
  return (
    <div className="imageContainer">
      <Image
        height="65px"
        width="60px"
        src={imagePath}
        alt={name}
        data-locator="order_item_image"
      />
      {!isGiftCardItem && (
        <Image
          width="60px"
          height="30px"
          alt={itemBrand}
          src={getIconPath(`header__brand-tab-${itemBrand.toLowerCase()}`)}
          data-locator="order_item_brand_logo"
        />
      )}
    </div>
  );
};
const getDefaultStatus = (orderStatus, orderLabels) => {
  if (orderStatus === ORDER_ITEM_STATUS.IN_TRANSIT) {
    return {
      orderItemMessage: getLabelValue(orderLabels, 'lbl_csh_in_transit_message'),
      itemDisabled: true,
    };
  }
  if (orderStatus === ORDER_ITEM_STATUS.IN_PROGRESS) {
    return {
      orderItemMessage: getLabelValue(orderLabels, 'lbl_csh_in_progress_message'),
      itemDisabled: true,
    };
  }
  return {};
};

const getOrderItemStatus = (itemStatus, orderStatus, orderLabels) => {
  const returnIcon = 'item-returned';
  switch (itemStatus) {
    case ORDER_ITEM_STATUS.RETURNED:
      return {
        orderItemMessage: getLabelValue(orderLabels, 'lbl_csh_returnItem_message'),
        orderItemStatus: getLabelValue(orderLabels, 'lbl_csh_returned'),
        statusIcon: getIconPath(returnIcon),
        itemDisabled: true,
      };
    case ORDER_ITEM_STATUS.RETURN_COMPLETED:
    case ORDER_ITEM_STATUS.ITEM_REFUNDED:
    case ORDER_ITEM_STATUS.REFUNDED:
      return {
        orderItemMessage: getLabelValue(orderLabels, 'lbl_csh_refundedItem_message'),
        orderItemStatus: getLabelValue(orderLabels, 'lbl_csh_refunded'),
        statusIcon: getIconPath('item-refunded'),
        itemDisabled: true,
      };
    case ORDER_ITEM_STATUS.CANCELLED:
    case ORDER_ITEM_STATUS.CANCELED:
      return {
        orderItemMessage: getLabelValue(orderLabels, 'lbl_csh_cancelItem_message'),
        orderItemStatus: getLabelValue(orderLabels, 'lbl_csh_canceled'),
        statusIcon: getIconPath('item-canceled'),
        itemDisabled: true,
      };
    case ORDER_ITEM_STATUS.RETURN_INITIATED:
      return {
        orderItemMessage: getLabelValue(orderLabels, 'lbl_csh_returninitiateItem_message'),
        orderItemStatus: getLabelValue(orderLabels, 'lbl_csh_returning'),
        statusIcon: getIconPath(returnIcon),
        itemDisabled: true,
      };
    case ORDER_ITEM_STATUS.NonEligibleForReturn:
      return {
        orderItemMessage: getLabelValue(orderLabels, 'lbl_csh_not_eligible_return_message'),
        itemDisabled: true,
      };
    case ORDER_ITEM_STATUS.GIFT_ITEM:
      return {
        orderItemMessage: getLabelValue(orderLabels, 'lbl_gift_card_not_eligble_return'),
        orderItemStatus: getLabelValue(orderLabels, 'lbl_gift_card_non_refundable'),
        statusIcon: getIconPath('gift-card-icon-white'),
        itemDisabled: true,
      };
    default:
      return getDefaultStatus(orderStatus, orderLabels);
  }
};

const isSelected = (selected, selectedOrderItems, lineNumber) => {
  return selected || selectedOrderItems[lineNumber] > 0;
};

const isDisabled = (
  disabled,
  itemDisabled,
  appeasementApplied,
  noDisabledState,
  orderStatus,
  selectedReasonCode
) => {
  const revisedAppeasementApplied =
    orderStatus === ORDER_ITEM_STATUS.SHIPMENT_API_DELIVERED &&
    selectedReasonCode?.action === REASON_CODES.INITIATE_RETURN &&
    appeasementApplied
      ? false
      : appeasementApplied;
  return !noDisabledState ? disabled || itemDisabled || revisedAppeasementApplied : false;
};

const setClass = (isItemDisabled, isItemSelected) => {
  return (isItemDisabled && 'disabled-item') || (isItemSelected && 'selected-item') || '';
};

const setTickMark = (selectedReasonCode) => {
  return selectedReasonCode?.action === REASON_CODES.INITIATE_RETURN && 'selected-tick-mark';
};

const getQuantityList = (selectedReasonCode, quantity) => {
  const qty = selectedReasonCode.action === REASON_CODES.RECEIVED_WRONG_ITEM ? 1 : quantity;
  const quantityArray = new Array(qty).fill(1);
  return quantityArray.map((val, index) => ({
    displayName: index + 1,
    id: index + 1,
  }));
};

const shouldClearSelectedOrderItems = (items, clearSelectedOrderItems) => {
  const selectedItems = Object.keys(items).filter((item) => !!items[item]);
  if (!selectedItems.length) {
    clearSelectedOrderItems();
  }
};

const getDefaultValue = (selectedOrderItems, lineNumber) => {
  if (!isEmpty(selectedOrderItems)) {
    return selectedOrderItems[lineNumber];
  }
  return 1;
};

const getSelectedItems = (itemQuantity, selectedReasonCode, lineNumber, selectedOrderItems) => {
  return selectedReasonCode && selectedReasonCode.action === REASON_CODES.RECEIVED_WRONG_ITEM
    ? { [lineNumber]: itemQuantity }
    : { ...selectedOrderItems, [lineNumber]: itemQuantity };
};

const getDisplayQuantity = (isCanceledList, quantityCanceled, quantity) => {
  return isCanceledList ? quantityCanceled : quantity;
};

const showOrderItemMessage = ({ orderItemMessage, noDisabledState }) => {
  return orderItemMessage && !noDisabledState;
};

const getRevisedItemStatus = (selectedReasonCode, isReturnedItem, itemStatus, isGiftCardItem) => {
  if (isGiftCardItem) {
    return ORDER_ITEM_STATUS.GIFT_ITEM;
  }
  return selectedReasonCode?.action === REASON_CODES.INITIATE_RETURN &&
    isReturnedItem &&
    itemStatus === ORDER_ITEM_STATUS.SHIPMENT_API_DELIVERED
    ? ORDER_ITEM_STATUS.NonEligibleForReturn
    : itemStatus;
};

const setDropDownQuantityInTile = (qty, quantity, setDropDownQuantity) => {
  return setDropDownQuantity ? { value: quantity } : { value: qty };
};

const getGiftCardItem = (giftItem) => giftItem?.toLowerCase() === 'y';

const getItemSizeorValueLabel = (isGiftCardItem, orderLabels) =>
  isGiftCardItem
    ? getLabelValue(orderLabels, 'lbl_orderDetails_value')
    : getLabelValue(orderLabels, 'lbl_orderDetails_size');

/**
 * This function component use for return the OrderItems
 * can be passed in the component.
 * @param otherProps - otherProps object used pass params to other component
 */

const OrderItem = ({ className, ...otherProps }) => {
  /**
   * @function return  Used to render the JSX of the component
   * @param    {[Void]} function does not accept anything.
   * @return   {[Object]} JSX of the component
   */

  const {
    item: {
      productInfo: { appeasementApplied, name, imagePath, color, fit, size, variantNo, giftItem },
      itemInfo: { itemBrand, quantity, quantityCanceled, offerPrice },
      orderItemStatus: itemStatus,
      isReturnedItem,
      lineNumber,
    },
    currencySymbol,
    isCanceledList,
    orderLabels,
    selected,
    disabled,
    setSelectedOrderItems,
    selectedOrderItems = {},
    orderStatus,
    clearSelectedOrderItems,
    selectedReasonCode,
    clearSelectedResolution,
    noDisabledState,
    isDropDownDisabled,
    setDropDownQuantity,
  } = otherProps;

  const isGiftCardItem = getGiftCardItem(giftItem);

  const revisedItemStatus = getRevisedItemStatus(
    selectedReasonCode,
    isReturnedItem,
    itemStatus,
    isGiftCardItem
  );

  const { orderItemStatus, statusIcon, orderItemMessage, itemDisabled } = getOrderItemStatus(
    revisedItemStatus,
    orderStatus,
    orderLabels
  );

  const isItemSelected = isSelected(selected, selectedOrderItems, lineNumber);
  const isItemDisabled = isDisabled(
    disabled,
    itemDisabled,
    appeasementApplied,
    noDisabledState,
    orderStatus,
    selectedReasonCode
  );

  const onClickOrderItem = (itmQty = 1) => {
    if (!isItemDisabled && setSelectedOrderItems) {
      const itemQuantity = selectedOrderItems[lineNumber] ? 0 : itmQty;
      const items = getSelectedItems(
        itemQuantity,
        selectedReasonCode,
        lineNumber,
        selectedOrderItems
      );
      setSelectedOrderItems(items);
      shouldClearSelectedOrderItems(items, clearSelectedOrderItems);
      if (itemQuantity === 0) {
        clearSelectedResolution();
      }
    }
  };

  const [qty, setQuantity] = useState(getDefaultValue(selectedOrderItems, lineNumber));
  const quantityChange = (e) => {
    setQuantity(e.target.value);
    if (!isItemDisabled) {
      const itemQuantity = parseInt(e.target.value, 10);
      const items = { ...selectedOrderItems, [lineNumber]: itemQuantity };
      setSelectedOrderItems(items);
    }
  };

  const displayQuantity = getDisplayQuantity(isCanceledList, quantityCanceled, quantity);
  const paidPrice = offerPrice * displayQuantity;

  return (
    <div className={className}>
      <BodyCopy
        onClick={() => onClickOrderItem()}
        component="div"
        className={`container ${setClass(isItemDisabled, isItemSelected)}`}
      >
        {renderImageContainer(itemBrand, name, imagePath, isGiftCardItem)}
        <div className="detailContainer">
          <BodyCopy
            className="product-item-container"
            fontSize="fs14"
            fontWeight="extrabold"
            fontFamily="secondary"
            color="black"
          >
            {name}
          </BodyCopy>
          <span className={`${setTickMark(selectedReasonCode)} tick-mark-wrap`}>
            <Image
              width="22px"
              height="22px"
              src={getIconPath('tick-mark-black')}
              className="tick-mark"
              alt="info"
            />
          </span>

          <BodyCopy
            className="elem-mt-XXXS order-item-detail"
            fontSize="fs14"
            fontFamily="secondary"
            color="black"
          >
            {getLabelValue(orderLabels, 'lbl_orderDetails_itemnumber')}
            {variantNo}
          </BodyCopy>
          {!isGiftCardItem && (
            <BodyCopy
              className="elem-mt-XXXS order-item-detail"
              fontSize="fs14"
              fontFamily="secondary"
              color="black"
            >
              {getLabelValue(orderLabels, 'lbl_orderDetails_color')}
              {color.name}
            </BodyCopy>
          )}
          <>
            {fit && (
              <BodyCopy
                className="order-item-detail"
                component="span"
                fontSize="fs14"
                fontFamily="secondary"
                color="black"
              >
                {getLabelValue(orderLabels, 'lbl_orderDetails_fit')}
                {fit}
              </BodyCopy>
            )}
            {size && (
              <BodyCopy
                component="span"
                className="elem-mt-XXXS order-item-detail"
                fontSize="fs14"
                fontFamily="secondary"
                color="black"
              >
                {getItemSizeorValueLabel(isGiftCardItem, orderLabels)}
                {size}
              </BodyCopy>
            )}
          </>

          <BodyCopy
            className="elem-mt-XXXS order-item-detail"
            fontSize="fs14"
            fontFamily="secondary"
            color="black"
          >
            {getLabelValue(orderLabels, 'lbl_orderDetails_quantity')}
            {displayQuantity}
          </BodyCopy>
          <BodyCopy
            className="elem-mt-XXXS order-item-detail"
            fontSize="fs14"
            fontFamily="secondary"
            color="black"
          >
            {getLabelValue(orderLabels, 'lbl_orderDetails_youPaid')}
            {formatAmount(paidPrice, currencySymbol)}
          </BodyCopy>
          {showOrderItemMessage({ orderItemMessage, noDisabledState }) && (
            <div className="message_container">
              <BodyCopy fontSize="fs12" textAlign="center" color="black" fontFamily="secondary">
                {orderItemMessage}
              </BodyCopy>
            </div>
          )}
        </div>
      </BodyCopy>
      {showOrderItemMessage({ orderItemMessage, noDisabledState }) && (
        <div className="message_container">
          <BodyCopy fontSize="fs12" textAlign="center" color="black" fontFamily="secondary">
            {orderItemMessage}
          </BodyCopy>
        </div>
      )}

      {isItemSelected && displayQuantity > 1 && (
        <div className="qty-selector">
          <BodyCopy
            fontSize="fs12"
            fontFamily="secondary"
            fontWeight="extrabold"
            className="pdp-qty"
          >
            {getLabelValue(orderLabels, 'lbl_orderDetails_quantity')}
          </BodyCopy>
          <SelectBox
            width={40}
            id="quantity"
            name="Quantity"
            component={MiniBagSelect}
            options={getQuantityList(selectedReasonCode, quantity)}
            onChange={quantityChange}
            input={setDropDownQuantityInTile(qty, quantity, setDropDownQuantity)}
            meta
            inheritedStyles={selectBoxStyle}
            disabled={isDropDownDisabled}
          />
        </div>
      )}

      {orderItemStatus && !noDisabledState && (
        <div className="item-status-tag">
          <Image width="16px" height="16px" alt="" src={statusIcon} />
          <BodyCopy className="elem-ml-XXS" fontSize="fs12" color="white" fontFamily="secondary">
            {orderItemStatus}
          </BodyCopy>
        </div>
      )}
    </div>
  );
};
OrderItem.propTypes = {
  className: PropTypes.string,
  currencySymbol: PropTypes.string.isRequired,
  orderGroup: PropTypes.shape({}).isRequired,
  selectedOrderItems: PropTypes.shape({}),
  clearSelectedOrderItems: PropTypes.func,
  noDisabledState: PropTypes.bool,
  isDropDownDisabled: PropTypes.bool,
  setDropDownQuantity: PropTypes.bool,
};

OrderItem.defaultProps = {
  className: '',
  selectedOrderItems: {},
  clearSelectedOrderItems: () => {},
  noDisabledState: false,
  isDropDownDisabled: false,
  setDropDownQuantity: false,
};

export default withStyles(OrderItem, styles);
export { OrderItem as OrderItemsVanilla };
