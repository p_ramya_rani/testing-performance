// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

export const Container = styled.View`
  margin: ${props => props.theme.spacing.ELEM_SPACING.XS}
    ${props => props.theme.spacing.ELEM_SPACING.XS};
`;

export const ItemContainer = styled.View`
  border-radius: 8px;
  border: solid 1px ${props => props.theme.colorPalette.gray[600]};
  padding: ${props => props.theme.spacing.ELEM_SPACING.MED};
  margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.MED};
`;

export const SelectedItemContainer = styled.View`
  border-radius: 8px;
  border: solid 2px ${props => props.theme.colorPalette.blue[800]};
  padding: ${props => props.theme.spacing.ELEM_SPACING.MED};
  margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.MED};
`;
