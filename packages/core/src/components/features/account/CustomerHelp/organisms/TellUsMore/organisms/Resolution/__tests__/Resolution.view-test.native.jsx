// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { ResolutionVanilla } from '../views/Resolution.view.native';
import { TellusMoreContext } from '../../../views/TellUsMore.view.native';

describe('Missing/Wrong order resolution', () => {
  const props = {
    labels: {
      lbl_how_to_solve: 'How can we resolve this?',
    },
    selectedResolution: 'refundItems',
  };

  const continueButtonRef = {
    current: {
      measure: jest.fn(),
    },
  };
  const step1 = continueButtonRef;
  const step2 = continueButtonRef;
  const step3 = continueButtonRef;
  const step4 = continueButtonRef;
  const step5 = continueButtonRef;
  const step6 = continueButtonRef;
  const step7 = continueButtonRef;
  const scrollWrongItemsResolutionRef = continueButtonRef;

  it('should render Resolution option', () => {
    const component = shallow(
      <TellusMoreContext.Provider
        value={{
          step1,
          step2,
          step3,
          step4,
          step5,
          step6,
          step7,
          continueButtonRef,
          scrollWrongItemsResolutionRef,
        }}
      >
        <ResolutionVanilla {...props} />
      </TellusMoreContext.Provider>
    );
    expect(component).toMatchSnapshot();
  });
});
