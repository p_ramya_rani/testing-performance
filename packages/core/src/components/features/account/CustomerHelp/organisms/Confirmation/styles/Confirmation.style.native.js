// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';
import { ViewWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';

export const Container = styled(ViewWithSpacing)`
  align-items: center;
`;

export const RowContainer = styled(ViewWithSpacing)`
  flex-direction: row;
  align-items: center;
  justify-content: space-around;
  width: 100%;
  flex: 1;
`;

export const BottomContainer = styled(ViewWithSpacing)`
  background-color: ${props => props.theme.colorPalette.gray[300]};
  flex: 1;
`;
