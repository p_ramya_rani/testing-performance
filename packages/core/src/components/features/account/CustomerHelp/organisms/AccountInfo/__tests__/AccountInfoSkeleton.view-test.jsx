// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { AccountInfoTileSkeltonVanilla } from '../skelton/AccountInfoSkelton.view';

describe('AccountOverviewTileSkelton component', () => {
  it('should renders correctly', () => {
    const props = {
      className: 'sample-class',
    };
    const component = shallow(<AccountInfoTileSkeltonVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
