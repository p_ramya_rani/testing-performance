// 9fbef606107a605d69c0edbcd8029e5d
import React, { useEffect, useRef } from 'react';
import { Field, reduxForm, change as reduxFormChange, untouch } from 'redux-form';
import PropTypes from 'prop-types';
import { isClient } from '@tcp/core/src/utils';
import Notification from '@tcp/core/src/components/common/molecules/Notification';
import { Row, Col, TextBox, Button, BodyCopy } from '../../../../../../../../common/atoms';
import createValidateMethod from '../../../../../../../../../utils/formValidation/createValidateMethod';
import { removeSpaces } from '../../../../../../../../../utils/formValidation/phoneNumber';
import { getLabelValue } from '../../../../../../../../../utils';
import constants from '../AccountConfirmation.constants';
import withStyles from '../../../../../../../../common/hoc/withStyles';
import styles from '../styles/AccountConfirmation.style';
import getStandardConfig from '../../../../../../../../../utils/formValidation/validatorStandardConfig';
import Recaptcha from '../../../../../../../../common/molecules/recaptcha/recaptcha';

const UseFocus = () => {
  const htmlElRef = useRef(null);
  const setFocus = () => {
    // eslint-disable-next-line no-unused-expressions
    htmlElRef.current?.focus();
  };

  return [htmlElRef, setFocus];
};

export const focusFirstField = (setFirstNameFocus) => {
  if (isClient()) {
    setFirstNameFocus();
  }
};

function AccountConfirmationForm({
  labels,
  handleSubmit,
  className,
  pristine,
  invalid,
  change,
  setShowOtp,
  mergeAccountButtonLoader,
  setMergeAccountButtonLoader,
  accountConfirmationError,
  setAccountConfirmationFailure,
  showRecaptcha,
  trackAnalyticsClick,
}) {
  const [firstNameRef, setFirstNameFocus] = UseFocus();

  useEffect(() => {
    setAccountConfirmationFailure(null);
    focusFirstField(setFirstNameFocus);
    return () => {
      if (mergeAccountButtonLoader) setMergeAccountButtonLoader(false);
    };
  }, []);

  const shouldMergeButtonDisabled = () => pristine || mergeAccountButtonLoader || invalid;

  const OtpFlow = (toggleScreen) => {
    toggleScreen(true);
  };

  const onSubmitForm = (e) => {
    e.stopPropagation();
    const clickPath = 'My Accounts & Coupons|I want to merge My PlACE account|Alternate Flow';
    trackAnalyticsClick({
      orderHelp_path_cd128: clickPath,
    });
    return handleSubmit(e);
  };

  return (
    <div className={className}>
      <Row fullBleed>
        <Col colSize={{ small: 6, medium: 7, large: 12 }} className="elem-mb-XS">
          <div className="header-container top-header-container">
            <div className="header-title">
              <BodyCopy component="p" fontWeight="bold" fontFamily="secondary" fontSize="fs16">
                {getLabelValue(labels, 'lbl_merge_confirm_heading')}
              </BodyCopy>
            </div>

            <Button
              className="blue-link"
              nohover
              type="button"
              link
              underline
              color="blue"
              onClick={() => {
                OtpFlow(setShowOtp);
              }}
            >
              {getLabelValue(labels, 'lbl_merge_back_to_otp')}
            </Button>
          </div>
          {accountConfirmationError && (
            <Col colSize={{ small: 6, medium: 4, large: 4 }}>
              <BodyCopy component="div" className="account-confirmation-error">
                <Notification
                  status="error"
                  colSize={{ large: 12, medium: 8, small: 6 }}
                  message={accountConfirmationError}
                />
              </BodyCopy>
            </Col>
          )}
        </Col>
      </Row>

      <BodyCopy
        component="p"
        fontWeight="semibold"
        fontFamily="secondary"
        fontSize="fs14"
        className="elem-mb-XXS"
      >
        {getLabelValue(labels, 'lbl_merge_confirm_account_desc')}
      </BodyCopy>

      <form
        name={constants.ACCOUNT_CONFIRMATION_FORM}
        className="form-test-class"
        onSubmit={onSubmitForm}
        noValidate
      >
        <Row fullBleed className="row-form-wrapper elem-mb-XXL">
          <Col colSize={{ small: 6, medium: 4, large: 6 }}>
            <Field
              placeholder={getLabelValue(labels, 'lbl_merge_firstName')}
              name="firstName"
              id="firstName"
              component={TextBox}
              dataLocator="first-name-acc-merge"
              enableSuccessCheck={false}
              inputRef={firstNameRef}
            />
          </Col>
          <Col colSize={{ small: 6, medium: 4, large: 6 }}>
            <Field
              placeholder={getLabelValue(labels, 'lbl_merge_lastName')}
              name="lastName"
              id="lastName"
              component={TextBox}
              dataLocator="last name-acc-merge"
              enableSuccessCheck={false}
            />
          </Col>
          <Col colSize={{ small: 6, medium: 4, large: 6 }}>
            <Field
              placeholder={getLabelValue(labels, 'lbl_merge_zipCode')}
              maxLength={15}
              name="noCountryZip"
              id="noCountryZip"
              component={TextBox}
              dataLocator="zip-code-acc-merge"
              enableSuccessCheck={false}
              normalize={removeSpaces}
            />
          </Col>
        </Row>

        <BodyCopy component="div" className="elem-mb-XXL">
          {showRecaptcha && (
            <>
              <Recaptcha
                verifyCallback={(token) => {
                  change('recaptchaToken', token);
                }}
                expiredCallback={() => {
                  change('recaptchaToken', '');
                }}
              />
              <Field
                id="recaptchaToken"
                component={TextBox}
                type="hidden"
                name="recaptchaToken"
                data-locator="merge-account-confirmation-recaptchca"
              />
            </>
          )}
        </BodyCopy>

        <Button
          buttonVariation="variable-width"
          fill="BLUE"
          type="submit"
          data-locator="merge-account-btn"
          loading={mergeAccountButtonLoader}
          disabled={shouldMergeButtonDisabled()}
          className="merge-account-button"
        >
          {getLabelValue(labels, 'lbl_merge_account')}
        </Button>
      </form>
    </div>
  );
}

AccountConfirmationForm.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  handleSubmit: PropTypes.func.isRequired,
  className: PropTypes.string,
  pristine: PropTypes.bool.isRequired,
  invalid: PropTypes.bool.isRequired,
  change: PropTypes.func.isRequired,
  setShowOtp: PropTypes.func.isRequired,
  mergeAccountButtonLoader: PropTypes.bool.isRequired,
  setMergeAccountButtonLoader: PropTypes.func.isRequired,
  accountConfirmationError: PropTypes.shape({}).isRequired,
  setAccountConfirmationFailure: PropTypes.func.isRequired,
  showRecaptcha: PropTypes.bool.isRequired,
  trackAnalyticsClick: PropTypes.func.isRequired,
};

AccountConfirmationForm.defaultProps = {
  className: '',
};

const validateMethod = createValidateMethod(
  getStandardConfig([
    'firstName',
    'lastName',
    { noCountryZip: 'noCountryZipMerge' },
    'recaptchaToken',
  ])
);
const afterSubmit = (result, dispatch) => {
  dispatch(reduxFormChange(constants.ACCOUNT_CONFIRMATION_FORM, 'recaptchaToken', ''));
  dispatch(untouch(constants.ACCOUNT_CONFIRMATION_FORM, 'recaptchaToken'));
};

export default reduxForm({
  form: constants.ACCOUNT_CONFIRMATION_FORM,

  enableReinitialize: true,
  onSubmitSuccess: afterSubmit,
  ...validateMethod,
})(withStyles(AccountConfirmationForm, styles));

export { AccountConfirmationForm as AccountConfirmationFormVanilla };
