import React from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { getResentMessageHideThresold } from '../../../../../../ResetPassword/container/ResetPassword.selectors';
import MergeAccountsOtp from '../views';
import { checkOtpdetails, clearOtpData, getOtpdetails } from './MergeAccountOtp.action';
import { getOtpDetail, getOtpUniqueKey, getOtpError } from './MergeAccountOtp.selector';
import {
  getFirstTImeOtp,
  getMergeAccountButtonLoader,
  getShowRecaptcha,
} from '../../../container/MergeAccounts.selectors';
import { getGlobalLabels } from '../../../../../../Account/container/Account.selectors';
import { setFirstTimeOtp as setFirstTimeOtpAction } from '../../../container/MergeAccounts.actions';

const MergeAccountContainer = ({
  labels,
  isLiveChatEnabled,
  isInternationalShipping,
  resetPasswordError,
  handleOtpSubmission,
  mergeAccountButtonLoader,
  otpDetail,
  resendMessageThreshold,
  setShowOtp,
  getOtpOnEmail,
  otpUniqueKey,
  victimEmailAddress,
  globalLabels,
  clearOtpDataFields,
  showRecaptcha,
  firstTimeOtp,
  setFirstTimeOtp,
  ...otherprops
}) => {
  return (
    <MergeAccountsOtp
      labels={labels}
      isLiveChatEnabled={isLiveChatEnabled}
      isInternationalShipping={isInternationalShipping}
      resetPasswordError={resetPasswordError}
      handleOtpSubmission={handleOtpSubmission}
      mergeAccountButtonLoader={mergeAccountButtonLoader}
      otpDetail={otpDetail}
      resendMessageThreshold={resendMessageThreshold}
      setShowOtp={setShowOtp}
      getOtpOnEmail={getOtpOnEmail}
      otpUniqueKey={otpUniqueKey}
      victimEmailAddress={victimEmailAddress}
      globalLabels={globalLabels}
      clearOtpDataFields={clearOtpDataFields}
      showRecaptcha={showRecaptcha}
      firstTimeOtp={firstTimeOtp}
      setFirstTimeOtp={setFirstTimeOtp}
      {...otherprops}
    />
  );
};

const mapStateToProps = (state) => {
  return {
    mergeAccountButtonLoader: getMergeAccountButtonLoader(state),
    otpDetail: getOtpDetail(state),
    resendMessageThreshold: getResentMessageHideThresold(state),
    otpUniqueKey: getOtpUniqueKey(state),
    globalLabels: getGlobalLabels(state),
    otpError: getOtpError(state),
    showRecaptcha: getShowRecaptcha(state),
    firstTimeOtp: getFirstTImeOtp(state),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleOtpSubmission: (payload) => {
      dispatch(checkOtpdetails(payload));
    },

    getOtpOnEmail: (payload) => {
      dispatch(getOtpdetails(payload));
    },

    clearOtpDataFields: () => {
      dispatch(clearOtpData());
    },
    setFirstTimeOtp: (payload) => {
      dispatch(setFirstTimeOtpAction(payload));
    },
  };
};

MergeAccountContainer.propTypes = {
  className: PropTypes.string,
  labels: PropTypes.shape({}).isRequired,
  globalLabels: PropTypes.shape({}).isRequired,
  handleOtpSubmission: PropTypes.func,
  mergeAccountButtonLoader: PropTypes.bool.isRequired,
  otpDetail: PropTypes.shape({}),
  isLiveChatEnabled: PropTypes.bool.isRequired,
  resendMessageThreshold: PropTypes.number,
  victimEmailAddress: PropTypes.string,
  otpUniqueKey: PropTypes.string,
  setShowOtp: PropTypes.func,
  getOtpOnEmail: PropTypes.func,
  clearOtpDataFields: PropTypes.func,
  showRecaptcha: PropTypes.bool.isRequired,
  firstTimeOtp: PropTypes.bool.isRequired,
};

MergeAccountContainer.defaultProps = {
  className: '',
  handleOtpSubmission: () => {},
  otpDetail: {},
  resendMessageThreshold: 3000,
  victimEmailAddress: '',
  otpUniqueKey: '',
  setShowOtp: () => {},
  getOtpOnEmail: () => {},
  clearOtpDataFields: () => {},
};

export default connect(mapStateToProps, mapDispatchToProps)(MergeAccountContainer);
