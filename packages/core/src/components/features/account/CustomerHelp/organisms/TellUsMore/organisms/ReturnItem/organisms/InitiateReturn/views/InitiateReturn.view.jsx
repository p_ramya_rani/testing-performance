import React from 'react';
import PropTypes from 'prop-types';
import { BodyCopy, Button, Row, Col, Anchor } from '../../../../../../../../../../common/atoms';
import withStyles from '../../../../../../../../../../common/hoc/withStyles';
import styles from '../styles/InitiateReturn.style';
import ReturnMethods from '../organisms/ReturnMethods';
import { moveToStage } from '../../../../../../../util/utility';
import { getLabelValue, smoothScrolling } from '../../../../../../../../../../../utils';
import CUSTOMER_HELP_CONSTANTS, {
  CUSTOMER_HELP_PAGE_TEMPLATE,
} from '../../../../../../../CustomerHelp.constants';
import OrderItem from '../../../../OrderItem';
import { RETURN_ITEM_ICONS } from '../../../ReturnItem.constant';

const { RETURN_METHOD } = CUSTOMER_HELP_CONSTANTS;
const onNextBtnClick = () => {
  moveToStage(CUSTOMER_HELP_PAGE_TEMPLATE.REVIEW);
};
const onEditBtnClick = (setReturnItemDisplay) => {
  setReturnItemDisplay(false);
  setTimeout(() => {
    smoothScrolling('step-4');
  }, 100);
};
const renderHeadingContainer = (labels) => {
  return (
    <BodyCopy
      component="div"
      className="return-item-heading elem-mt-MED_1"
      id="initiate-return-step2"
    >
      <BodyCopy
        component="p"
        fontWeight="bold"
        fontFamily="secondary"
        fontSize="fs18"
        className="heading-line"
      >
        {getLabelValue(labels, 'lbl_returns_step_two_heading')}
      </BodyCopy>
    </BodyCopy>
  );
};

const renderSubHeadingContainer = (labels) => {
  return (
    <BodyCopy
      component="p"
      fontWeight="bold"
      fontFamily="secondary"
      fontSize="fs16"
      className="elem-mb-XS"
    >
      {getLabelValue(labels, 'lbl_returns_step_two_subheading')}
    </BodyCopy>
  );
};

const renderDescription = (labels) => {
  return (
    <BodyCopy component="div" className="return-item-desc">
      <span>
        <BodyCopy
          component="p"
          fontWeight="normal"
          fontFamily="secondary"
          fontSize="fs14"
          className=""
        >
          {getLabelValue(labels, 'lbl_returns_step_two_description_one')}
          &nbsp;
          <Anchor
            to="/help-center/returns-exchanges#returnsandexchanges"
            target="_blank"
            anchorVariation="primary"
            fontFamily="secondary"
            fontWeight="normal"
            fontSizeVariation="large"
            className="return-policy"
            dataLocator="navigationToReturnPolicy"
          >
            {getLabelValue(labels, 'lbl_returns_return_policy')}
          </Anchor>
          .
        </BodyCopy>
      </span>
      <br />
      <BodyCopy
        component="p"
        fontWeight="normal"
        fontFamily="secondary"
        fontSize="fs14"
        className=""
      >
        {getLabelValue(labels, 'lbl_returns_step_two_description_two')}
      </BodyCopy>
    </BodyCopy>
  );
};

const renderReturnMethods = (returnMethods, setReturnMethod, selectedReturnMethod) => {
  let selected;
  return (
    <div className="return-method-container elem-mb-XL">
      {returnMethods &&
        returnMethods.map((item) => {
          if (item.key === selectedReturnMethod) {
            selected = true;
          } else {
            selected = false;
          }
          return (
            <ReturnMethods
              key={item.id}
              item={item}
              setReturnMethod={setReturnMethod}
              selected={selected}
              onClick={smoothScrolling('return-next-button', 200)}
            />
          );
        })}
    </div>
  );
};

const renderButton = (labels, selectedReturnMethod) => {
  return (
    <BodyCopy component="div" className="btn-container" id="return-next-button">
      <Button
        fill="BLUE"
        type="submit"
        buttonVariation="variable-width"
        className="next-button"
        disabled={!selectedReturnMethod}
        onClick={() => onNextBtnClick()}
      >
        {getLabelValue(labels, 'lbl_returns_button_next')}
      </Button>
    </BodyCopy>
  );
};

const renderSelectedHeading = (labels, setReturnItemDisplay) => {
  return (
    <>
      <BodyCopy
        component="p"
        fontWeight="bold"
        fontFamily="secondary"
        fontSize="fs16"
        className="steps"
      >
        {getLabelValue(labels, 'lbl_initiate_return_step_1')}
      </BodyCopy>
      <BodyCopy component="div" className="top-heading-container">
        <BodyCopy component="p" fontWeight="bold" fontFamily="secondary" fontSize="fs16">
          {getLabelValue(labels, 'lbl_returns_selected_text')}
        </BodyCopy>
        <Button
          className="edit-option"
          nohover
          type="button"
          link
          underline
          color="blue"
          onClick={() => {
            onEditBtnClick(setReturnItemDisplay);
          }}
        >
          {getLabelValue(labels, 'lbl_returns_edit_items_btn')}
        </Button>
      </BodyCopy>
    </>
  );
};

const renderSelectedOrderItem = (
  itm,
  index,
  orderLabels,
  currencySymbol,
  selectedOrderItems,
  selectedReasonCode
) => {
  const { lineNumber, itemStatus } = itm;

  const isSelectedOrderItem = selectedOrderItems[lineNumber] > 0;

  const item = isSelectedOrderItem
    ? {
        ...itm,
        ...{ itemInfo: { ...itm.itemInfo, quantity: selectedOrderItems[lineNumber] } },
      }
    : itm;
  return (
    <>
      {isSelectedOrderItem && itemStatus !== 'Refunded' && (
        <Col
          ignoreGutter={(index + 1) % 3 === 0 ? { large: true } : {}}
          colSize={{ large: 4, medium: 4, small: 6 }}
          className="elem-mt-LRG order-items-container"
        >
          <OrderItem
            key={index.toString()}
            {...{ item }}
            selected
            orderLabels={orderLabels && orderLabels.orders}
            currencySymbol={currencySymbol}
            selectedReasonCode={selectedReasonCode}
            isDropDownDisabled={true}
            setDropDownQuantity={true}
          />
        </Col>
      )}
    </>
  );
};

const renderSelectedItemList = (
  items,
  orderLabels,
  currencySymbol,
  selectedOrderItems,
  selectedReasonCode
) => {
  return (
    <BodyCopy component="div">
      <Row fullBleed>
        {items &&
          items.map((item, index) =>
            renderSelectedOrderItem(
              item,
              index,
              orderLabels,
              currencySymbol,
              selectedOrderItems,
              selectedReasonCode
            )
          )}
      </Row>
    </BodyCopy>
  );
};

const InitiateReturn = ({
  className,
  setReturnMethod,
  selectedReturnMethod,
  labels,
  orderLabels,
  orderDetailsData,
  setReturnItemDisplay,
  selectedOrderItems,
  selectedReasonCode,
}) => {
  const { orderItems } = orderDetailsData || [];
  const { summary } = orderDetailsData || {};
  const { currencySymbol } = summary || {};
  const returnMethods = [
    {
      id: 1,
      key: `${RETURN_METHOD.IN_STORE}`,
      iconPath: `${RETURN_ITEM_ICONS.STORE_ICON}`,
      returnText: `${getLabelValue(labels, 'lbl_returns_instore_returnMethod_text')}`,
      returnTextCost: `${getLabelValue(labels, 'lbl_free_of_charge')}`,
      returnDesc: `${getLabelValue(labels, 'lbl_returns_instore_returnMethod_desc')}`,
      shortDesc: {
        shortDescIcon: `${RETURN_ITEM_ICONS.LOCATION_ICON}`,
        shortDescText: `${getLabelValue(labels, 'lbl_returns_instore_returnMethod_shortDescText')}`,
      },
    },
    {
      id: 2,
      key: `${RETURN_METHOD.BY_MAIL}`,
      iconPath: `${RETURN_ITEM_ICONS.SHIPPING_ICON}`,
      returnText: `${getLabelValue(labels, 'lbl_returns_bymail_returnMethod_text')}`,
      returnTextCost: `${getLabelValue(labels, 'lbl_shipping_fees_apply')}`,
      returnDesc: `${getLabelValue(labels, 'lbl_returns_bymail_returnMethod_desc')}`,
    },
  ];

  return (
    <Col colSize={{ small: 6, medium: 7, large: 12 }}>
      <BodyCopy component="div" className={`${className} return-item-container elem-mt-XL`}>
        {renderSelectedHeading(labels, setReturnItemDisplay)}
        {orderItems &&
          orderItems.map(({ items }) => {
            return renderSelectedItemList(
              items,
              orderLabels,
              currencySymbol,
              selectedOrderItems,
              selectedReasonCode
            );
          })}
        {renderHeadingContainer(labels)}
        {renderSubHeadingContainer(labels)}
        {renderDescription(labels)}
        {renderReturnMethods(returnMethods, setReturnMethod, selectedReturnMethod)}
        {renderButton(labels, selectedReturnMethod)}
      </BodyCopy>
    </Col>
  );
};

InitiateReturn.propTypes = {
  className: PropTypes.string,
  setReturnMethod: PropTypes.func.isRequired,
  selectedReturnMethod: PropTypes.string,
  labels: PropTypes.shape({}).isRequired,
  orderLabels: PropTypes.shape({}).isRequired,
  selectedOrderItems: PropTypes.shape({}),
  selectedReasonCode: PropTypes.shape({}),
  clearReturnMethod: PropTypes.func,
  setReturnItemDisplay: PropTypes.func,
  orderDetailsData: PropTypes.shape({
    orderItems: PropTypes.shape({}),
    summary: PropTypes.shape({
      currencySymbol: PropTypes.string,
    }),
  }),
};

InitiateReturn.defaultProps = {
  className: '',
  selectedReturnMethod: '',
  setReturnItemDisplay: () => {},
  orderDetailsData: {},
  selectedOrderItems: {},
  selectedReasonCode: {},
};

export default withStyles(InitiateReturn, styles);
export { InitiateReturn as InitiateReturnVanilla };
