// 9fbef606107a605d69c0edbcd8029e5d
import { fromJS } from 'immutable';
import EMAIL_CONFIRMATION_CONSTANTS from '../EmailConfirmation.constants';

const initialState = {
  isFetching: false,
  error: null,
  emailConfirmationSuccess: null,
};

const emailConfirmationReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case EMAIL_CONFIRMATION_CONSTANTS.EMAIL_CONFIRMATION_REQUEST: {
      return state.set('isFetching', true).set('error', null).set('emailConfirmationSuccess', null);
    }
    case EMAIL_CONFIRMATION_CONSTANTS.EMAIL_CONFIRMATION_SUCCESS: {
      return state
        .set('isFetching', false)
        .set('error', null)
        .set('emailConfirmationSuccess', payload);
    }
    case EMAIL_CONFIRMATION_CONSTANTS.EMAIL_CONFIRMATION_FAILED: {
      return state
        .set('isFetching', false)
        .set('error', payload)
        .set('emailConfirmationSuccess', null);
    }
    case EMAIL_CONFIRMATION_CONSTANTS.RESET_EMAIL_CONFIRMATION: {
      return state
        .set('emailConfirmationSuccess', null)
        .set('error', null)
        .set('isFetching', false);
    }
    case EMAIL_CONFIRMATION_CONSTANTS.SET_EMAIL_CONFIRMATION_LOADER: {
      return state.set('isFetching', payload);
    }
    default:
      if (state instanceof Object) {
        return fromJS(state);
      }
      return state;
  }
};

export default emailConfirmationReducer;
