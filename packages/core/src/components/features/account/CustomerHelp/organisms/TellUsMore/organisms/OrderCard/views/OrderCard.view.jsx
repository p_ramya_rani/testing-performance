// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { Row, Image, BodyCopy, Col, Button } from '@tcp/core/src/components/common/atoms';
import { isMobileWeb, getViewportInfo } from '@tcp/core/src/utils';
import { getLabelValue, getIconPath, getShipmentCount } from '@tcp/core/src/utils/utils';
import { getAppeasedShipmentStatus, getWarningMsg } from '../../../../../util/utility';
import OrderItemHeader from '../../OrderItemHeader';
import { ORDER_ITEM_STATUS, REASON_CODES } from '../../../../../CustomerHelp.constants';
import styles from '../styles/OrderCard.style';

const fallbackImg = getIconPath('essential-style-fallback');

const getOrderDisplayStatus = (orderStatus, orderLabels) => {
  const {
    IN_TRANSIT,
    SHIPMENT_API_DELIVERED,
    IN_PROGRESS,
    RETURN_INITIATED,
    ORDER_BEING_PROCESSED,
  } = ORDER_ITEM_STATUS;

  switch (orderStatus) {
    case IN_TRANSIT:
      return getLabelValue(orderLabels, 'lbl_orders_statusItemsInTransit', 'orders');

    case SHIPMENT_API_DELIVERED:
      return getLabelValue(orderLabels, 'lbl_orders_statusItemsDelivered', 'orders');

    case ORDER_BEING_PROCESSED:
    case IN_PROGRESS:
      return getLabelValue(orderLabels, 'lbl_orders_statusItemsInProgress', 'orders');
    case RETURN_INITIATED:
      return getLabelValue(orderLabels, 'lbl_orders_returns_initiated', 'orders');
    case ORDER_ITEM_STATUS.CANCELED:
    case ORDER_ITEM_STATUS.CANCELLED:
      return getLabelValue(orderLabels, 'lbl_orders_statusOrderCancelled', 'orders');
    default:
      return '';
  }
};

const getCardStyle = ({
  action,
  orderGroup,
  selectedShipmentCard,
  filterValue,
  itemIndex,
  toggleCards,
  isAppeased,
}) => {
  const { isMobile, isDesktop, isTablet } = getViewportInfo();
  const { PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME, WHEN_WILL_MY_ORDER_ARRIVE } = REASON_CODES;
  const { RETURN_INITIATED } = ORDER_ITEM_STATUS;
  if (action === WHEN_WILL_MY_ORDER_ARRIVE && orderGroup.orderStatus !== RETURN_INITIATED) {
    if (!selectedShipmentCard) {
      return 'elem-mb-MED order-card-container';
    }
    return 'elem-mb-MED order-card-container selected-item';
  }
  if (orderGroup.orderStatus === filterValue && !isAppeased) {
    if (selectedShipmentCard === itemIndex) {
      return 'elem-mb-MED order-card-container selected-item';
    }
    return `${
      action === PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME &&
      !toggleCards &&
      isMobile &&
      !isDesktop &&
      !isTablet
        ? 'order-card-container'
        : 'elem-mb-MED order-card-container'
    }`;
  }
  return 'elem-mb-MED order-card-container delivered-hazy';
};

const getItemIndex = (orderGroupItems) => {
  if (Array.isArray(orderGroupItems) && orderGroupItems[0] && orderGroupItems[0].productInfo) {
    return orderGroupItems[0].lineNumber;
  }
  return null;
};

const cardClick = ({
  onCardClick,
  itemIndex,
  orderGroup,
  action,
  filterValue,
  getFilteredList,
  setToggleCards,
  isAppeased,
}) => {
  const { PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME, WHEN_WILL_MY_ORDER_ARRIVE } = REASON_CODES;
  const { RETURN_INITIATED } = ORDER_ITEM_STATUS;

  if (action === PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME && isMobileWeb() && !isAppeased) {
    setToggleCards(true);
  } else if (
    (orderGroup.orderStatus === filterValue &&
      action !== PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME &&
      !isAppeased) ||
    (action === WHEN_WILL_MY_ORDER_ARRIVE && orderGroup.orderStatus !== RETURN_INITIATED)
  ) {
    onCardClick(itemIndex, orderGroup);
    getFilteredList(itemIndex, action);
  }
  return null;
};

const getFilterValue = (selectedAction) => {
  const {
    TRACKING_NUMBER_SHOW_IN_TRANSIT_FOR_LONG_TIME,
    PACKAGE_DELIVERED_BUT_NOT_RECEIVED,
    PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME,
  } = REASON_CODES;
  const { IN_TRANSIT, SHIPMENT_API_DELIVERED } = ORDER_ITEM_STATUS;
  let filterKey;
  if (selectedAction === TRACKING_NUMBER_SHOW_IN_TRANSIT_FOR_LONG_TIME) {
    filterKey = IN_TRANSIT;
  } else if (
    selectedAction === PACKAGE_DELIVERED_BUT_NOT_RECEIVED ||
    selectedAction === PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME
  ) {
    filterKey = SHIPMENT_API_DELIVERED;
  }
  return filterKey;
};

const getToggleButton = (
  selectedShipmentCard,
  action,
  setIsDelivered,
  labels,
  isDelivered,
  ...others
) => {
  const [orderItems, orderGroupExists, nonOrderGroupExists] = others;
  const { PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME } = REASON_CODES;
  if (
    !selectedShipmentCard &&
    action !== PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME &&
    orderItems &&
    orderItems.length > 1 &&
    orderGroupExists &&
    nonOrderGroupExists
  ) {
    return (
      <Row fullBleed className="elem-mt-LRG" centered>
        <Button
          nohover
          className="blue-link"
          type="button"
          link
          underline
          color="blue"
          onClick={() => {
            setIsDelivered((prev) => !prev);
          }}
        >
          {isDelivered
            ? getLabelValue(labels, 'lbl_hide_other_shipments', '')
            : getLabelValue(labels, 'lbl_show_all_shipments', '')}
        </Button>
      </Row>
    );
  }
  return null;
};

const getFullListCondition = ({
  isDelivered,
  selectedShipmentCard,
  selectedAction,
  toggleCards,
  paidShippingOverMobile,
  filterKey,
  orderItems,
}) => {
  const { PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME, WHEN_WILL_MY_ORDER_ARRIVE } = REASON_CODES;
  if (selectedAction === WHEN_WILL_MY_ORDER_ARRIVE && selectedShipmentCard) return false;
  const orderGroupExists =
    orderItems && orderItems.some((order) => order.orderStatus === filterKey);
  const paidShippingNonMobile =
    selectedAction === PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME && !isMobileWeb();

  if (
    (isDelivered && !selectedShipmentCard && selectedAction) ||
    paidShippingNonMobile ||
    (paidShippingOverMobile && toggleCards) ||
    !orderGroupExists
  ) {
    return true;
  }
  return false;
};

const groupMyOrderArriveShipment = (filteredItems) => {
  return filteredItems?.filter((item) => {
    return (
      item.orderStatus !== ORDER_ITEM_STATUS.IN_PROGRESS &&
      item.orderStatus !== ORDER_ITEM_STATUS.ORDER_BEING_PROCESSED &&
      item.orderStatus !== ORDER_ITEM_STATUS.RETURN_INITIATED &&
      item.orderStatus !== ORDER_ITEM_STATUS.RETURNED &&
      item.orderStatus !== ORDER_ITEM_STATUS.RETURN_COMPLETED
    );
  });
};

const OrderCard = ({
  className,
  orderDetailsData,
  orderLabels,
  labels,
  isDelivered,
  onCardClick,
  selectedShipmentCard,
  action,
  setToggleCards,
  toggleCards,
  setIsDelivered,
  filteredItems,
  setFilteredList,
}) => {
  const myArriveGroupedList = filteredItems && groupMyOrderArriveShipment(filteredItems);
  const { orderItems, estimatedDeliveryDate } = orderDetailsData || {};
  const shipmentCount = getShipmentCount(orderItems);
  const [filterValue, setFilter] = useState(null);
  const { PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME } = REASON_CODES;
  const filterKey = getFilterValue(action);
  const getFilteredList = (cardIdx, selectedAction) => {
    let filteredList;
    const paidShippingOverMobile =
      selectedAction === PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME && isMobileWeb();
    setFilter(filterKey);
    if (
      getFullListCondition({
        isDelivered,
        selectedShipmentCard,
        selectedAction,
        toggleCards,
        paidShippingOverMobile,
        filterKey,
        orderItems,
      })
    ) {
      filteredList = orderItems;
    } else if (paidShippingOverMobile && !toggleCards) {
      const orderItemsGroup = orderItems && orderItems[0];
      if (orderItemsGroup) {
        filteredList = [orderItemsGroup];
      }
    } else if (selectedShipmentCard) {
      filteredList =
        orderItems &&
        orderItems.filter(
          (orderGroup) =>
            orderGroup.items &&
            orderGroup.items.length > 0 &&
            orderGroup.items[0].lineNumber === cardIdx
        );
    } else {
      filteredList =
        orderItems && orderItems.filter((orderGroup) => orderGroup.orderStatus === filterKey);
    }
    setFilteredList(filteredList);
  };

  const orderGroupExists =
    orderItems && orderItems.some((order) => order.orderStatus === filterKey);
  const nonOrderGroupExists =
    orderItems && orderItems.some((order) => order.orderStatus !== filterKey);

  useEffect(() => {
    if (orderItems) {
      getFilteredList(selectedShipmentCard, action);
    }
  }, [isDelivered, selectedShipmentCard, toggleCards, orderItems]);

  return (
    <div className={`${className} `}>
      <>
        <Row fullBleed>
          {filteredItems &&
            filteredItems.length > 0 &&
            filteredItems.map((orderGroup, index) => {
              const orderGroupItems = orderGroup.items ? orderGroup.items : null;
              const itemIndex = getItemIndex(orderGroupItems);
              const isAppeased = getAppeasedShipmentStatus({ orderGroup });
              let isInMyArriveList = false;
              let myArriveIdx;
              myArriveGroupedList.forEach((itm, itmIndex) => {
                if (itm.trackingNumber === orderGroup.trackingNumber) {
                  myArriveIdx = itmIndex;
                  isInMyArriveList = true;
                }
                return null;
              });
              return (
                <>
                  <Col
                    colSize={{ small: 6, medium: 6, large: 10 }}
                    key={index.toString()}
                    className={getCardStyle({
                      action,
                      orderGroup,
                      selectedShipmentCard,
                      filterValue,
                      itemIndex,
                      toggleCards,
                      isAppeased,
                    })}
                    onClick={() => {
                      cardClick({
                        onCardClick,
                        itemIndex,
                        orderGroup,
                        action,
                        filterValue,
                        getFilteredList,
                        setToggleCards,
                        isAppeased,
                      });
                    }}
                  >
                    <Row fullBleed>
                      <OrderItemHeader
                        textFonSize="fs12"
                        shipmentNumber={isInMyArriveList ? myArriveIdx + 1 : index + 1}
                        ItemsCount={shipmentCount}
                        status={orderGroup.orderStatus}
                        trackingNumber={orderGroup.trackingNumber}
                        trackingUrl={orderGroup.trackingUrl}
                        orderLabels={orderLabels && orderLabels.orders}
                        orderStatus={orderGroup.orderStatus}
                        orderDisplayStatus={getOrderDisplayStatus(
                          orderGroup.orderStatus,
                          orderLabels
                        )}
                        mediumText
                        shippedDate={orderGroup.shipDate}
                        shipmentStatus={orderGroup.packageStatus}
                        estimatedDeliveryDate={estimatedDeliveryDate}
                        actualDeliveryDate={orderGroup.actualDeliveryDate}
                        carrierDeliveryDate={orderGroup.carrierDeliveryDate}
                        action={action}
                      />
                    </Row>
                    <Row fullBleed className="image-container">
                      {orderGroupItems.slice(0, 4).map((item, idx) => (
                        <div key={idx.toString()} className="image-wrapper">
                          <Image
                            width="56.9px"
                            height="62.2px"
                            src={item.productInfo.imagePath || fallbackImg}
                            alt={item.productInfo.name}
                            className={
                              orderGroupItems.length > 4 && idx === 3
                                ? 'items-image last-image'
                                : 'items-image'
                            }
                          />
                          {item.itemInfo.quantity > 1 && (
                            <BodyCopy
                              fontSize="fs10"
                              fontFamily="secondary"
                              component="p"
                              className="item-count"
                            >
                              {item.itemInfo.quantity}
                            </BodyCopy>
                          )}
                          {orderGroupItems.length > 4 && idx === 3 && (
                            <BodyCopy
                              fontSize="fs12"
                              fontFamily="secondary"
                              component="p"
                              fontWeight="extrabold"
                              className="total-items"
                            >
                              {`+${orderGroupItems.length - 3}`}
                            </BodyCopy>
                          )}
                        </div>
                      ))}
                    </Row>
                    <Row fullBleed className="warn-msg-container">
                      <BodyCopy
                        fontSize="fs14"
                        fontFamily="secondary"
                        component="p"
                        fontWeight="extrabold"
                        className="warn-msg"
                        textAlign="center"
                      >
                        {getWarningMsg({
                          orderGroup,
                          filterValue,
                          action,
                          orderGroupExists,
                          isAppeased,
                          labels,
                        })}
                      </BodyCopy>
                    </Row>
                  </Col>
                </>
              );
            })}
        </Row>
        {action === PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME && isMobileWeb() && !toggleCards && (
          <div className="stack-container">
            <div className="stack-one" />
            <div className="stack-two" />
          </div>
        )}
        {getToggleButton(
          selectedShipmentCard,
          action,
          setIsDelivered,
          labels,
          isDelivered,
          orderItems,
          orderGroupExists,
          nonOrderGroupExists
        )}
      </>
    </div>
  );
};

OrderCard.propTypes = {
  orderDetailsData: PropTypes.shape({}).isRequired,
  selectedReasonCode: PropTypes.shape({}),
  labels: PropTypes.shape({}),
  orderLabels: PropTypes.shape({}),
  setSelectedOrderItems: PropTypes.shape({}),
  selectedOrderItems: PropTypes.shape({}),
  shipmentData: PropTypes.shape({}),
  className: PropTypes.string,
  isDelivered: PropTypes.bool,
  onCardClick: PropTypes.func,
  selectedShipmentCard: PropTypes.string,
  action: PropTypes.string,
  setToggleCards: PropTypes.func,
  toggleCards: PropTypes.bool,
  setIsDelivered: PropTypes.func,
  filteredItems: PropTypes.shape([]),
  setFilteredList: PropTypes.func,
};

OrderCard.defaultProps = {
  orderLabels: {},
  setSelectedOrderItems: {},
  selectedOrderItems: {},
  selectedReasonCode: {},
  labels: {},
  shipmentData: {},
  className: '',
  isDelivered: false,
  onCardClick: () => {},
  selectedShipmentCard: '',
  action: '',
  setToggleCards: () => {},
  toggleCards: false,
  setIsDelivered: () => {},
  filteredItems: [],
  setFilteredList: () => {},
};

export default withStyles(OrderCard, styles);
export { OrderCard as OrderCardVanilla };
