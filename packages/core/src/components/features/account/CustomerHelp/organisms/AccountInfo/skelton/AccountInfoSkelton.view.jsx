// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import LoaderSkelton from '@tcp/core/src/components/common/molecules/LoaderSkelton';
import styles from '../styles/AccountInfoSkeleton.style';

const AccountInfoTileSkelton = ({ className }) => {
  return (
    <div className={className}>
      <div className="headingWrapper">
        <LoaderSkelton width="75%" height="30px" />
      </div>
      <div className="contentWrapper">
        <LoaderSkelton width="100%" height="140px" />
      </div>
    </div>
  );
};

AccountInfoTileSkelton.propTypes = {
  className: PropTypes.string.isRequired,
};
export default withStyles(AccountInfoTileSkelton, styles);

export { AccountInfoTileSkelton as AccountInfoTileSkeltonVanilla };
