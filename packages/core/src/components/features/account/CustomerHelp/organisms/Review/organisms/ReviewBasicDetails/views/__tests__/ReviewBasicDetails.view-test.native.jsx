// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import ReviewBasicDetails from '../ReviewBasicDetails.view';

describe('Review Basic Details component ', () => {
  it('should render correctly', () => {
    const props = {
      ordersLabels: {},
      className: '',
    };
    const component = shallow(<ReviewBasicDetails {...props} />);
    expect(component).toMatchSnapshot();
  });
});
