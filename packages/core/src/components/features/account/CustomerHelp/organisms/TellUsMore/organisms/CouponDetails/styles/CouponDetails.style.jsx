// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  .couponCard__container {
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    @media ${(props) => props.theme.mediaQuery.medium} {
      margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
    }
    width: 100%;
    position: relative;
    border: solid 1px ${(props) => props.theme.colors.BORDER.NORMAL};
    border-style: dashed;
    border-radius: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    background-color: ${(props) =>
      props.coupon?.status === 'applied'
        ? props.theme.colors.PRIMARY.COLOR1
        : props.theme.colors.WHITE};
  }

  .couponCard__notValid__container {
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
  }
  .couponCard__header {
    position: absolute;
    top: -1px;
    left: -1px;
    right: -1px;
    border-top-left-radius: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    border-top-right-radius: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    background: ${(props) => props.theme.colorPalette.orange[800]};
  }

  .couponCard__container_error {
    border: solid 1px ${(props) => props.theme.colors.TEXT.RED};
    padding: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
  }

  .couponCard__header_text {
    padding-left: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXXS};
    color: ${(props) => props.theme.colors.WHITE};
    font-weight: ${(props) => props.theme.fonts.fontWeight.bold};
    line-height: ${(props) => props.theme.spacing.ELEM_SPACING.REG};
  }
  .couponCard__text_style {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
    color: ${(props) => props.theme.colors.BLACK};
    line-height: ${(props) => props.theme.spacing.ELEM_SPACING.MED_1};
  }

  .couponDetails__text_useby {
    font-weight: ${(props) => props.theme.fonts.fontWeight.semiBold};
    line-height: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    color: ${(props) => props.theme.colors.TEXT.DARK};
  }

  .couponDetailsLink {
    color: ${(props) => props.theme.colors.TEXT.DARK};
    font-weight: ${(props) => props.theme.fonts.fontWeight.bold};
  }

  .couponCard__body {
    padding: ${(props) => props.theme.spacing.ELEM_SPACING.MED_1}
      ${(props) => props.theme.spacing.ELEM_SPACING.MED}
      ${(props) => props.theme.spacing.ELEM_SPACING.XS}
      ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  }
  .couponCard__row {
    display: flex;
  }

  .couponCard__col {
    flex: auto;
    &:nth-child(2) {
      text-align: end;
      margin: auto;
    }
  }

  .coupon__button {
    width: 92px;
    height: ${(props) => props.theme.spacing.ELEM_SPACING.XXXL};
    font-size: ${(props) => props.theme.typography.fontSizes.fs14};
    padding: 0;
  }

  .coupon__button_black {
    background-color: ${(props) => props.theme.colors.BLACK};
    ${(props) => (props.isFetching ? 'opacity:1;' : '')};
  }

  .coupon__button_gray {
    background-color: ${(props) => props.theme.colors.PRIMARY.COLOR1};
  }

  .couponCard__container_error_text {
    color: ${(props) => props.theme.colors.TEXT.RED};
  }

  .couponTitle {
    word-break: ${(props) => (props.isCarouselView ? 'break-word' : 'normal')};
  }

  .expiry-content {
    padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
  }
  .couponCard__apply_error {
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    padding-top: 0px;
    padding-bottom: 0px;

    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    }
  }
  .expired-content-txt {
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      font-size: ${(props) => props.theme.fonts.fontSize.anchor.medium}px;
    }
  }
`;

export default styles;
