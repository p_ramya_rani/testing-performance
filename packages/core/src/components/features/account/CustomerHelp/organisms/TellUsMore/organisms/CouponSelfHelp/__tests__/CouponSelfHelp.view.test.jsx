// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { CouponSelfHelpVanilla } from '../views/CouponSelfHelp.view';

describe('CouponSelfHelp Template', () => {
  const props = {
    className: 'couponSelfHelp',
    labels: {},
    needHelpContentLabels: {},
  };
  it('should not  render MyOffersCoupons or MyRewards when isLoggedIn is false', () => {
    const component = shallow(<CouponSelfHelpVanilla {...props} isLoggedIn={false} />);
    expect(component).toMatchSnapshot();
  });

  it('should render MyOffersCoupons or MyRewards when isLoggedIn is true', () => {
    const component = shallow(<CouponSelfHelpVanilla {...props} isLoggedIn />);
    expect(component).toMatchSnapshot();
  });
  it('should render need help content', () => {
    const component = shallow(<CouponSelfHelpVanilla {...props} needHelpContentLabels />);
    expect(component).toMatchSnapshot();
  });
});
