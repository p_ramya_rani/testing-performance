// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

const SeeMoreLink = styled.TouchableOpacity`
  display: flex;
  padding-bottom: ${props => props.theme.spacing.ELEM_SPACING.XL};
  padding-top: ${props => props.theme.spacing.ELEM_SPACING.LRG};
  font-family: ${props => props.theme.typography.fonts.secondary};
  font-size: ${props => props.theme.typography.fontSizes.fs16};
`;

export default SeeMoreLink;
