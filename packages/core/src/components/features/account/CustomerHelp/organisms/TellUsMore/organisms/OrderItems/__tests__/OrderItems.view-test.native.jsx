// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { ORDER_ITEM_STATUS } from '../../../../../CustomerHelp.constants';
import OrderItems from '../views/OrderItems.view.native';

describe('OrderItems Component', () => {
  const props = {
    orderDetailsData: {
      summary: {
        adjustmentCouponTotal: 0,
        appeasementTotal: 0,
        canceledItems: 0,
        couponsTotal: 0,
        currencySymbol: '$',
        grandTotal: 85.39,
        purchasedItems: 4,
        returnedInitiatedItems: 1,
        returnedItems: 1,
        returnedTotal: 0,
        shippedItems: 3,
        shippingTotal: 0,
        subTotal: 79.8,
        totalItems: 4,
        totalTax: 5.59,
      },
      orderItems: [
        {
          items: [
            {
              isReturnedItem: false,
              itemStatus: ORDER_ITEM_STATUS.RETURNED,
              lineNumber: '1518191089_7NW64107Y600306642_3_1',
              orderItemStatus: ORDER_ITEM_STATUS.RETURNED,
              quantity: 1,
              itemInfo: {
                itemBrand: 'TCP',
                linePrice: 59.849999999999994,
                listPrice: 19.95,
                offerPrice: 19.95,
                quantity: 1,
                quantityCanceled: 0,
                quantityOOS: 0,
                quantityReturned: 1,
                quantityShipped: 3,
                taxUnitPrice: 1.4,
              },
              productInfo: {
                appeasementApplied: false,
                color: { name: 'REDWOOD', imagePath: '' },
                fit: null,
                giftOption: 'N',
                imagePath: '',
                lineNumber: '1518191089',
                name: 'Boys',
                pdpUrl:
                  'http://uatlive1.childrensplace.com/us/p/Boys-Active-Fleece-Jogger-Pants-3016287',
                size: 'XS (4)',
                thumbnailImgPath: '3016287/3016287.jpg',
                upc: '00194936100757',
                variantNo: '3016287007',
              },
              trackingInfo: [
                {
                  actualDeliveryDate: '2021-10-29 12:19:00',
                  appeasement: 0,
                  carrier: 'UPS',
                  carrierDeliveryDate: null,
                  isCSHReturn: 'false',
                  quantity: 1,
                  refundAmount: 0,
                  refundDate: null,
                  returnInitiatedDate: null,
                  shipDate: '11/10/11 00:00',
                  shipVia: 'UGNR',
                  status: ORDER_ITEM_STATUS.RETURNED,
                  trackingNbr: '7NW64107Y600306642_3',
                  trackingUrl:
                    'https://childrensplace.narvar.com/childrensplace/tracking/ups?tracking_numbers=70',
                },
              ],
            },
          ],
          orderStatus: 'Delivered',
          shippedDate: '11/10/23 00:00',
          trackingNumber: '7NW64107Y600306642_3',
          trackingUrl:
            'https://childrensplace.narvar.com/childrensplace/tracking/ups?tracking_numbers=7NW64107Y600306642_3&service=UG&ozip=35968&dzip=46350&locale=en_US&order_number=344134958&order_date=2021-10-19T02:07:00.000-04:00&ship_date=2021-11-19T02:38:42.000-04:00',
        },
      ],
      appliedGiftCards: [],
      bossMaxDate: null,
      bossMinDate: null,
      canceledItems: [],
      checkout: {
        shippingAddress: {
          addressLine1: '111 I St',
          addressLine2: 'Oooo',
          city: 'La Porte',
          country: 'United States',
          firstName: 'Tester',
          lastName: 'Demo',
          state: 'IN',
          zipCode: '46350',
        },
        pickUpStore: null,
        billing: {
          billingAddress: {
            addressLine1: '111 I St',
            addressLine2: 'Oooo',
            city: 'La Porte',
            country: 'United States',
            firstName: 'Tester',
            lastName: 'Demo',
            state: 'IN',
            zipCode: '46350',
          },
          card: {
            afterpay: false,
            cardType: 'Visa',
            chargedAmount: '$85.39',
            endingNumbers: '************1111',
            id: null,
          },
        },
      },
      emailAddress: 'SANT.HOTA99@GMAIL.COM',
      encryptedEmailAddress: 'heAL2XJwMyghv0AMtl9iXCOY487kfb3p',
      enteredBy: undefined,
      estimatedDeliveryDate: '',
      isBopisOrder: false,
      isBossOrder: false,
      isCancellable: 'n',
      orderDate: '2021-10-19 02:07:27.141',
      orderNumber: '344134958',
      orderStatus: 'order shipped',
      orderType: 'ECOM',
      outOfStockItems: [],
      pickUpExpirationDate: null,
      pickedUpDate: '2021-10-19 02:38:00',
      purchasedItems: [
        {
          items: [
            {
              isShippedItem: false,
              itemInfo: {
                listPrice: 19.95,
                offerPrice: 19.95,
                linePrice: 59.849999999999994,
                quantity: 4,
                quantityCanceled: 0,
              },
              productInfo: {
                appeasementApplied: false,
                fit: null,
                pdpUrl:
                  'http://uatlive1.childrensplace.com/us/p/Boys-Active-Fleece-Jogger-Pants-3016287-72R',
                name: 'Boys Active Fleece ',
                imagePath:
                  'https://test1.theplace.com/image/upload/ecom/assets/products/tcp/3016287/3016287_74R.jpg',
              },
              trackingInfo: [],
              returnedInitiatedItems: [],
              returnedItems: [],
              shippedDate: '2021-10-19 02:38:00',
              shippingMethod: 'Standard',
              status: 'lbl_orders_statusOrderShipped',
              summary: {
                currencySymbol: '$',
                totalItems: 4,
                subTotal: 79.8,
                purchasedItems: 4,
                shippedItems: 3,
              },
              trackingNumber: null,
              trackingUrl: null,
            },
          ],
        },
      ],
    },
  };

  it('should render Delivered OrderItems', () => {
    const component = shallow(<OrderItems {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should render IN_TRANSIT OrderItems', () => {
    const { orderDetailsData } = props;
    const orderItems = [
      {
        items: [
          {
            isReturnedItem: false,
            itemStatus: ORDER_ITEM_STATUS.RETURNED,
            lineNumber: '1518191089_7NW64107Y600306642_3_1',
            orderItemStatus: ORDER_ITEM_STATUS.RETURNED,
            quantity: 1,
            itemInfo: {
              itemBrand: 'TCP',
              linePrice: 59.849999999999994,
              listPrice: 19.95,
              offerPrice: 19.95,
              quantity: 1,
              quantityCanceled: 0,
              quantityOOS: 0,
              quantityReturned: 1,
              quantityShipped: 3,
              taxUnitPrice: 1.4,
            },
            productInfo: {
              appeasementApplied: false,
              color: { name: 'REDWOOD', imagePath: '3016287/3016287_7R_swatch.jpg' },
              fit: null,
              giftOption: 'N',
              imagePath:
                'https://test1.theplace.com/image/upload/ecom/assets/products/tcp/3016287/3016287_7R.jpg',
              lineNumber: '1518191089',
              name: 'Boys Active Fleece Jogger Pants',
              pdpUrl:
                'http://uatlive1.childrensplace.com/us/p/Boys-Active-Fleece-Jogger-Pants-3016287-7R',
              size: 'XS (4)',
              thumbnailImgPath: '3016287/3016287_7R.jpg',
              upc: '00194936100757',
              variantNo: '3016287007',
            },
            trackingInfo: [
              {
                actualDeliveryDate: '2021-10-19 12:19:00',
                appeasement: 0,
                carrier: 'UPS',
                carrierDeliveryDate: null,
                isCSHReturn: 'false',
                quantity: 1,
                refundAmount: 0,
                refundDate: null,
                returnInitiatedDate: null,
                shipDate: '12/10/21 00:00',
                shipVia: 'UGNR',
                status: ORDER_ITEM_STATUS.RETURNED,
                trackingNbr: '7NW64107Y600306642_3',
                trackingUrl:
                  'https://childrensplace.narvar.com/childrensplace/tracking/ups?tracking_numbers=7',
              },
            ],
          },
        ],
        orderStatus: ORDER_ITEM_STATUS.IN_TRANSIT,
        shippedDate: '11/11/21 00:00',
        trackingNumber: '7NW64107Y600306642_3',
        trackingUrl:
          'https://childrensplace.narvar.com/childrensplace/tracking/ups?tracking_numbers=7NW64107Y600306642_3&service=UG&ozip=35968&dzip=46350&locale=en_US&order_number=344234958&order_date=2021-10-19T02:07:00.000-04:00&ship_date=2021-10-19T02:38:42.000-04:00',
      },
    ];
    const updatedProps = {
      ...props,
      orderDetailsData: { ...orderDetailsData, orderItems },
    };
    const component = shallow(<OrderItems {...updatedProps} />);
    expect(component).toMatchSnapshot();
  });

  it('should render default OrderItems', () => {
    const { orderDetailsData } = props;
    const orderItems = [
      {
        items: [],
        shippedDate: '11/08/21 00:00',
        trackingNumber: '7NW64107Y600306642_3',
        trackingUrl:
          'https://childrensplace.narvar.com/childrensplace/tracking/ups?tracking_numbers=7NW64107Y600306642_3&service=UG&ozip=35968&dzip=46350&locale=en_US&order_number=344134958&order_date=2021-10-19T02:07:00.000-04:00&ship_date=2021-10-19T02:38:42.000-04:00',
      },
    ];
    const updatedProps = {
      ...props,
      orderDetailsData: { ...orderDetailsData, orderItems },
    };
    const component = shallow(<OrderItems {...updatedProps} />);
    expect(component).toMatchSnapshot();
  });

  it('should render IN_PROGRESS OrderItems', () => {
    const { orderDetailsData } = props;
    const orderItems = [
      {
        items: [
          {
            isReturnedItem: false,
            itemStatus: ORDER_ITEM_STATUS.RETURNED,
            lineNumber: '1518191089_7NW64107Y600306642_3_1',
            orderItemStatus: 'Order Returned',
            quantity: 1,
            itemInfo: {
              itemBrand: 'TCP',
              linePrice: 59.849999999999994,
              listPrice: 19.95,
              offerPrice: 19.95,
              quantity: 1,
              quantityCanceled: 0,
              quantityOOS: 0,
              quantityReturned: 1,
              quantityShipped: 3,
              taxUnitPrice: 1.4,
            },
            productInfo: {
              appeasementApplied: false,
              color: { name: 'REDWOOD', imagePath: '3016287/3016287_7R_swatch.jpg' },
              fit: null,
              giftOption: 'N',
              imagePath:
                'https://test1.theplace.com/image/upload/ecom/assets/products/tcp/3016287/3016287_7R.jpg',
              lineNumber: '1518191089',
              name: 'Boys Active Fleece Jogger Pants',
              pdpUrl:
                'http://uatlive1.childrensplace.com/us/p/Boys-Active-Fleece-Jogger-Pants-3016287-7R',
              size: 'XS (4)',
              thumbnailImgPath: '3016287/3016287_7R.jpg',
              upc: '00194936100757',
              variantNo: '3016287007',
            },
            trackingInfo: [
              {
                actualDeliveryDate: '2021-10-19 12:19:00',
                appeasement: 0,
                carrier: 'UPS',
                carrierDeliveryDate: null,
                isCSHReturn: 'false',
                quantity: 1,
                refundAmount: 0,
                refundDate: null,
                returnInitiatedDate: null,
                shipDate: '11/10/21 00:00',
                shipVia: 'UGNR',
                status: 'Order Returned',
                trackingNbr: '7NW64107Y600306642_3',
                trackingUrl:
                  'https://childrensplace.narvar.com/childrensplace/tracking/ups?tracking_numbers=7',
              },
            ],
          },
        ],
        orderStatus: ORDER_ITEM_STATUS.IN_PROGRESS,
        shippedDate: '11/10/21 00:00',
        trackingNumber: '7NW64107Y600306642_3',
        trackingUrl:
          'https://childrensplace.narvar.com/childrensplace/tracking/ups?tracking_numbers=7NW64107Y600306642_3&service=UG&ozip=35968&dzip=46350&locale=en_US&order_number=344134958&order_date=2021-10-19T02:07:00.000-04:00&ship_date=2021-10-19T02:38:42.000-04:00',
      },
    ];
    const updatedProps = {
      ...props,
      orderDetailsData: { ...orderDetailsData, orderItems },
    };
    const component = shallow(<OrderItems {...updatedProps} />);
    expect(component).toMatchSnapshot();
  });
});
