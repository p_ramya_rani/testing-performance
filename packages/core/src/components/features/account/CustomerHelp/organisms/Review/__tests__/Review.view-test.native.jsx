// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import Review from '../views';

describe('Review Template', () => {
  const props = {
    orderDetailsData: {
      orderNumber: '324234243',
    },
  };
  it('should render Review Template', () => {
    const component = shallow(<Review {...props} />);
    expect(component).toMatchSnapshot();
  });
});
