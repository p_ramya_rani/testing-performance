// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { OrderItemsVanilla } from '../views/OrderItem.view';

describe('Order Items component', () => {
  const props = {
    item: {
      productInfo: {
        name: 'test product',
        imagePath: 'test',
        color: 'red',
        fit: 'test',
        size: 'test',
        upc: 'test',
        pdpUrl: 'test',
      },
      itemInfo: {
        linePrice: 2.15,
        itemBrand: 'TCP',
        quantity: 1,
        quantityCanceled: false,
        listPrice: 2.25,
        offerPrice: 2.25,
      },
      trackingInfo: [{ status: 'Order Shipped' }],
    },
    currencySymbol: '$',
    isShowWriteReview: 'true',
    ordersLabels: {},
    selectedReasonCode: {},
  };
  it('should renders correctly', () => {
    const component = shallow(<OrderItemsVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should renders selected State', () => {
    const updatedProps = { ...props, selected: true };
    const component = shallow(<OrderItemsVanilla {...updatedProps} />);
    expect(component).toMatchSnapshot();
  });

  it('should render with returned status', () => {
    const { item } = props;
    const trackingInfo = [{ status: 'Order Returned' }];
    const updatedProps = { ...props, item: { ...item, trackingInfo } };
    const component = shallow(<OrderItemsVanilla {...updatedProps} />);
    expect(component).toMatchSnapshot();
  });

  it('should render with Canceled status', () => {
    const { item } = props;
    const trackingInfo = [{ status: 'Order Canceled' }];
    const updatedProps = { ...props, item: { ...item, trackingInfo } };
    const component = shallow(<OrderItemsVanilla {...updatedProps} />);
    expect(component).toMatchSnapshot();
  });

  it('should render with item refunded', () => {
    const { item } = props;
    const trackingInfo = [{ status: 'item refunded' }];
    const updatedProps = { ...props, item: { ...item, trackingInfo } };
    const component = shallow(<OrderItemsVanilla {...updatedProps} />);
    expect(component).toMatchSnapshot();
  });

  it('should render with In-Transit status', () => {
    const { item } = props;
    const trackingInfo = [{ status: 'Order Returned' }];
    const updatedProps = { ...props, item: { ...item, trackingInfo }, orderStatus: 'In Transit' };
    const component = shallow(<OrderItemsVanilla {...updatedProps} />);
    expect(component).toMatchSnapshot();
  });

  it('should render with Gift item status', () => {
    const { item } = props;
    const trackingInfo = [{ status: 'Gift Item' }];
    const updatedProps = { ...props, item: { ...item, trackingInfo } };
    const component = shallow(<OrderItemsVanilla {...updatedProps} />);
    expect(component).toMatchSnapshot();
  });
});
