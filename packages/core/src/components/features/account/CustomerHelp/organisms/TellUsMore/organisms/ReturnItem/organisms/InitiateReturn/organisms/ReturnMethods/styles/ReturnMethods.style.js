import { css } from 'styled-components';

const styles = css`
  &.container {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED_1};
    max-width: 330px;
    position: relative;
    display: flex;
    border-radius: ${(props) => props.theme.spacing.ELEM_SPACING.XS_6};
    border: 2px solid ${(props) => props.theme.colorPalette.gray[600]};
    padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    border-color: ${(props) =>
      props.selected ? `${props.theme.colorPalette.blue[800]} !important` : null};
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      width: 330px;
    }
  }

  .internal-container {
    display: flex;
    flex-direction: row;
    padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  }

  .inline-item {
    display: flex;
    flex-direction: column;
  }
  .icon {
    max-width: none;
    padding: 0 ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  }

  .short-desc-container {
    display: flex;
    flex-direction: row;
  }
  .shortdescText {
    align-self: flex-end;

    &:hover {
      color: ${(props) => props.theme.colors.BRAND.PRIMARY};
    }
  }

  .selected-item {
    border: 2px solid ${(props) => props.theme.colorPalette.blue[800]} !important;
  }
  .selected-none {
    border: none;
  }
`;

export default styles;
