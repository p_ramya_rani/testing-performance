import React from 'react';
import { shallow } from 'enzyme';
import { EmailConfirmationVanilla } from '../EmailConfirmation.view';

describe('EmailConfirmation view', () => {
  it('should render correctly', () => {
    const props = {
      onSubmit: jest.fn(),
      labels: {},
      resetEmailConfirmation: jest.fn(),
      formErrorMessage: {},
    };
    const tree = shallow(<EmailConfirmationVanilla {...props} />);
    expect(tree).toMatchSnapshot();
  });
});
