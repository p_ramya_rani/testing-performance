// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'next/router';
import { PropTypes } from 'prop-types';
import { getOrdersListState } from '@tcp/core/src/components/features/account/Orders/container/Orders.selectors';
import CustomerHelpProgress from '../views';
import utils from '../../../../../../../utils';
import { getCustomerHelpLabels } from '../../../container/CustomerHelp.selectors';
import { moveToStage } from '../../../util/utility';
import { getAvailableStages, CUSTOMER_HELP_PAGE_TEMPLATE } from '../../../CustomerHelp.constants';

const CUSTOMERHELP_STAGE_PROP_TYPE = PropTypes.oneOf(
  Object.keys(CUSTOMER_HELP_PAGE_TEMPLATE).map((key) => CUSTOMER_HELP_PAGE_TEMPLATE[key])
);
export class CustomerHelpProgressIndicator extends React.PureComponent {
  constructor() {
    super();
    this.availableStages = getAvailableStages();
  }

  render() {
    const { router, moveToCustomerHelpStage, labels, ordersListItems, activeSection } = this.props;

    const activeStageUrl = utils.getObjectValue(router, undefined, 'query', 'section');

    return (
      <CustomerHelpProgress
        activeStage={activeStageUrl || activeSection}
        availableStages={this.availableStages}
        moveToCustomerHelpStage={moveToCustomerHelpStage}
        labels={labels}
        ordersListItems={ordersListItems}
      />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    labels: getCustomerHelpLabels(state),
    ...state,
    ordersListItems: getOrdersListState(state),
  };
};

export const mapDispatchToProps = () => {
  return {
    moveToCustomerHelpStage: (stage) => moveToStage(stage),
  };
};

CustomerHelpProgressIndicator.propTypes = {
  router: PropTypes.shape({}).isRequired,
  moveToCustomerHelpStage: PropTypes.func.isRequired,
  labels: PropTypes.shape({}).isRequired,
  ordersListItems: PropTypes.shape([]).isRequired,
  activeSection: CUSTOMERHELP_STAGE_PROP_TYPE,
};

CustomerHelpProgressIndicator.defaultProps = {
  activeSection: undefined,
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(CustomerHelpProgressIndicator)
);
