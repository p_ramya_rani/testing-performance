// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState } from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import Select from '@tcp/core/src/components/common/atoms/Select';
import { Image, BodyCopy } from '@tcp/core/src/components/common/atoms';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import MiniBagSelect from '@tcp/web/src/components/features/CnC/MiniBag/molecules/MiniBagSelectBox/MiniBagSelectBox';
import { BodyCopyWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import { getBrandIcon } from '@tcp/core/src/utils';
import {
  QuantityHolder,
  OrderItemStyles,
  ItemStatusTag,
  QuantityLabel,
  StatusLabel,
  SubText,
} from '../styles/OrderItem.style.native';
import selectBoxStyle from '../styles/SelectBox.style';
import { ORDER_ITEM_STATUS, REASON_CODES } from '../../../../../CustomerHelp.constants';
import {
  SideBySideView,
  ContainerWithLeftMargin,
  SelectableSideBySide,
  OrderItemMessageView,
  WrapContainer,
} from '../../../../../styles/CustomerHelp.style.native';
import { formatAmount } from '../../../../../util/utility';

const ReturnedImage = require('../../../../../../../../../../../mobileapp/src/assets/images/item-returned.png');
const ItemRefunded = require('../../../../../../../../../../../mobileapp/src/assets/images/item-refunded.png');

const CancelledImage = require('../../../../../../../../../../../mobileapp/src/assets/images/cancelled-order.png');

const renderImageContainer = (itemBrand, name, imagePath) => {
  return (
    <View>
      <Image
        height="65px"
        width="60px"
        source={{ uri: imagePath.includes('https') ? imagePath : `https://${imagePath}` }}
        alt={name}
        data-locator="order_item_image"
      />
      <Image
        width="60px"
        height="30px"
        alt={itemBrand}
        source={getBrandIcon({ itemBrand })}
        data-locator="order_item_brand_logo"
      />
    </View>
  );
};
const getDefaultStatus = (orderStatus, orderLabels) => {
  if (orderStatus === ORDER_ITEM_STATUS.IN_TRANSIT) {
    return {
      orderItemMessage: getLabelValue(orderLabels, 'lbl_csh_in_transit_message'),
      itemDisabled: true,
    };
  }
  if (orderStatus === ORDER_ITEM_STATUS.IN_PROGRESS) {
    return {
      orderItemMessage: getLabelValue(orderLabels, 'lbl_csh_in_progress_message'),
      itemDisabled: true,
    };
  }
  return {};
};

const isApplyOrderItemStatus = ({ orderItemStatus, noBorder, noDisabledState }) => {
  return orderItemStatus && !noBorder && !noDisabledState;
};

const getOrderItemStatus = (itemStatus, orderStatus, orderLabels) => {
  switch (itemStatus) {
    case ORDER_ITEM_STATUS.RETURNED:
      return {
        orderItemMessage: getLabelValue(orderLabels, 'lbl_csh_returnItem_message'),
        orderItemStatus: getLabelValue(orderLabels, 'lbl_csh_returned'),
        statusIcon: ReturnedImage,
        itemDisabled: true,
      };
    case ORDER_ITEM_STATUS.ITEM_REFUNDED:
    case ORDER_ITEM_STATUS.REFUNDED:
      return {
        orderItemMessage: getLabelValue(orderLabels, 'lbl_csh_refundedItem_message'),
        orderItemStatus: getLabelValue(orderLabels, 'lbl_csh_refunded'),
        statusIcon: ItemRefunded,
        itemDisabled: true,
      };
    case ORDER_ITEM_STATUS.CANCELLED:
    case ORDER_ITEM_STATUS.CANCELED:
      return {
        orderItemMessage: getLabelValue(orderLabels, 'lbl_csh_cancelItem_message'),
        orderItemStatus: getLabelValue(orderLabels, 'lbl_csh_canceled'),
        statusIcon: CancelledImage,
        itemDisabled: true,
      };
    case ORDER_ITEM_STATUS.RETURN_INITIATED:
      return {
        orderItemMessage: getLabelValue(orderLabels, 'lbl_csh_returninitiateItem_message'),
        orderItemStatus: getLabelValue(orderLabels, 'lbl_csh_returning'),
        statusIcon: ReturnedImage,
        itemDisabled: true,
      };
    case ORDER_ITEM_STATUS.NonEligibleForReturn:
      return {
        orderItemMessage: getLabelValue(orderLabels, 'lbl_csh_not_eligible_return_message'),
        itemDisabled: true,
      };
    default:
      return getDefaultStatus(orderStatus, orderLabels);
  }
};

const isSelected = (selected, selectedOrderItems, lineNumber) => {
  return selected || selectedOrderItems[lineNumber] > 0;
};

const isDisabled = (
  disabled,
  itemDisabled,
  appeasementApplied,
  noDisabledState,
  orderStatus,
  selectedReasonCode
) => {
  const revisedAppeasementApplied =
    orderStatus === ORDER_ITEM_STATUS.SHIPMENT_API_DELIVERED &&
    selectedReasonCode?.action === REASON_CODES.INITIATE_RETURN &&
    appeasementApplied
      ? false
      : appeasementApplied;
  return !noDisabledState ? disabled || itemDisabled || revisedAppeasementApplied : false;
};

const getQuantityList = (selectedReasonCode, quantity) => {
  const qty = selectedReasonCode.action === REASON_CODES.RECEIVED_WRONG_ITEM ? 1 : quantity;
  const quantityArray = new Array(qty).fill(1);
  return quantityArray.map((val, index) => ({
    displayName: index + 1,
    id: index + 1,
  }));
};

const shouldClearSelectedOrderItems = (
  items,
  clearSelectedOrderItems,
  itemQuantity,
  clearSelectedResolution
) => {
  const selectedItems = Object.keys(items).filter((item) => !!items[item]);
  if (!selectedItems.length) {
    clearSelectedOrderItems();
  }

  if (itemQuantity === 0) {
    clearSelectedResolution();
  }
};

const getDefaultValue = (selectedOrderItems, lineNumber) => {
  if (!isEmpty(selectedOrderItems)) {
    return selectedOrderItems[lineNumber];
  }
  return 1;
};
const getSelectedItems = (itemQuantity, selectedReasonCode, lineNumber, selectedOrderItems) => {
  return selectedReasonCode && selectedReasonCode.action === REASON_CODES.RECEIVED_WRONG_ITEM
    ? { [lineNumber]: itemQuantity }
    : { ...selectedOrderItems, [lineNumber]: itemQuantity };
};
const getDisplayQuantity = (isCanceledList, quantityCanceled, quantity) => {
  return isCanceledList ? quantityCanceled : quantity;
};

const getOrderItemMessage = ({ orderItemMessage, noDisabledState }) => {
  return orderItemMessage && !noDisabledState;
};

/**
 * This function component use for return the OrderItems
 * can be passed in the component.
 * @param props - otherProps object used pass params to other component
 */

const OrderItem = (props) => {
  /**
   * @function return  Used to render the JSX of the component
   * @param    {[Void]} function does not accept anything.
   * @return   {[Object]} JSX of the component
   */

  const {
    item: {
      productInfo: { appeasementApplied, name, imagePath, color, fit, size, variantNo },
      itemInfo: { itemBrand, quantity, quantityCanceled, offerPrice },
      orderItemStatus: itemStatus,
      lineNumber,
    },
    currencySymbol,
    isCanceledList,
    orderLabels,
    selected,
    disabled,
    setSelectedOrderItems,
    selectedOrderItems = {},
    orderStatus,
    clearSelectedOrderItems,
    noBorder,
    selectedReasonCode,
    clearSelectedResolution,
    noDisabledState,
  } = props;

  const { orderItemStatus, statusIcon, orderItemMessage, itemDisabled } = getOrderItemStatus(
    itemStatus,
    orderStatus,
    orderLabels
  );

  const isItemSelected = isSelected(selected, selectedOrderItems, lineNumber);
  const isItemDisabled = isDisabled(
    disabled,
    itemDisabled,
    appeasementApplied,
    noDisabledState,
    orderStatus,
    selectedReasonCode
  );

  const onClickOrderItem = (itmQty = 1) => {
    if (!isItemDisabled && setSelectedOrderItems) {
      const itemQuantity = selectedOrderItems[lineNumber] ? 0 : itmQty;
      const items = getSelectedItems(
        itemQuantity,
        selectedReasonCode,
        lineNumber,
        selectedOrderItems
      );
      setSelectedOrderItems(items);
      shouldClearSelectedOrderItems(
        items,
        clearSelectedOrderItems,
        itemQuantity,
        clearSelectedResolution
      );
    }
  };

  const [qty, setQuantity] = useState(getDefaultValue(selectedOrderItems, lineNumber));
  const quantityChange = (value) => {
    setQuantity(value);
    if (!isItemDisabled) {
      const itemQuantity = parseInt(value, 10);
      const items = { ...selectedOrderItems, [lineNumber]: itemQuantity };
      setSelectedOrderItems(items);
    }
  };

  const displayQuantity = getDisplayQuantity(isCanceledList, quantityCanceled, quantity);
  const paidPrice = offerPrice * displayQuantity;

  const pickerStyles = { backgroundColor: 'white' };
  const wrapStyle = { flexWrap: 'wrap', flex: 1 };

  return (
    <View>
      <SelectableSideBySide
        noBorder={noBorder}
        isItemSelected={isItemSelected}
        isItemDisabled={isItemDisabled}
        onPress={() => onClickOrderItem()}
      >
        <SideBySideView>
          {renderImageContainer(itemBrand, name, imagePath)}
          <ContainerWithLeftMargin>
            <WrapContainer width="220">
              <BodyCopyWithSpacing
                style={{ ...wrapStyle }}
                fontSize="fs14"
                fontWeight="extrabold"
                fontFamily="secondary"
                text={name}
              />
            </WrapContainer>
            <SubText
              fontSize="fs12"
              fontFamily="secondary"
              text={`${getLabelValue(orderLabels, 'lbl_orderDetails_itemnumber')}${variantNo}`}
            />
            <BodyCopy
              fontSize="fs12"
              fontFamily="secondary"
              text={`${getLabelValue(orderLabels, 'lbl_orderDetails_color')}${color.name}`}
            />

            <>
              {fit ? (
                <BodyCopy
                  fontSize="fs12"
                  fontFamily="secondary"
                  text={`${getLabelValue(orderLabels, 'lbl_orderDetails_fit')}${fit}`}
                />
              ) : null}
              {size ? (
                <BodyCopy
                  component="span"
                  fontSize="fs12"
                  fontFamily="secondary"
                  text={`${getLabelValue(orderLabels, 'lbl_orderDetails_size')}${size}`}
                />
              ) : null}
            </>
            <BodyCopy
              component="span"
              fontSize="fs12"
              fontFamily="secondary"
              text={`${getLabelValue(orderLabels, 'lbl_orderDetails_quantity')}${
                isCanceledList ? quantityCanceled : quantity
              }`}
            />
            <BodyCopy
              fontSize="fs12"
              fontFamily="secondary"
              text={`${getLabelValue(orderLabels, 'lbl_orderDetails_youPaid')}${formatAmount(
                paidPrice,
                currencySymbol
              )}`}
            />
          </ContainerWithLeftMargin>
        </SideBySideView>

        {getOrderItemMessage({ orderItemMessage, noDisabledState }) ? (
          <OrderItemMessageView>
            <BodyCopy
              fontSize="fs12"
              textAlign="center"
              color="black"
              fontFamily="secondary"
              text={orderItemMessage}
            />
          </OrderItemMessageView>
        ) : null}

        {isItemSelected ? (
          <QuantityHolder>
            <QuantityLabel>{getLabelValue(orderLabels, 'lbl_orderDetails_quantity')}</QuantityLabel>

            <Select
              width={40}
              id="quantity"
              name="Quantity"
              component={MiniBagSelect}
              options={getQuantityList(selectedReasonCode, quantity)}
              input={{ value: qty, onChange: quantityChange }}
              pickerStyle={pickerStyles}
              meta
              inheritedStyles={selectBoxStyle}
            />
          </QuantityHolder>
        ) : null}
      </SelectableSideBySide>
      {isApplyOrderItemStatus({ orderItemStatus, noBorder, noDisabledState }) ? (
        <ItemStatusTag>
          <Image width="14px" height="14px" alt="" source={statusIcon} />
          <StatusLabel
            fontSize="fs12"
            color="white"
            fontFamily="secondary"
            text={orderItemStatus}
          />
        </ItemStatusTag>
      ) : null}
    </View>
  );
};
OrderItem.propTypes = {
  currencySymbol: PropTypes.string.isRequired,
  orderGroup: PropTypes.shape({}),
  clearSelectedOrderItems: PropTypes.func,
  item: PropTypes.shape({}),
  isCanceledList: PropTypes.bool.isRequired,
  orderLabels: PropTypes.shape({}),
  selected: PropTypes.bool.isRequired,
  disabled: PropTypes.bool.isRequired,
  setSelectedOrderItems: PropTypes.func.isRequired,
  selectedOrderItems: PropTypes.shape({}),
  orderStatus: PropTypes.string.isRequired,
  noBorder: PropTypes.bool,
  selectedReasonCode: PropTypes.shape({}),
  clearSelectedResolution: PropTypes.func,
  noDisabledState: PropTypes.bool,
};

OrderItem.defaultProps = {
  item: {},
  orderLabels: {},
  selectedOrderItems: {},
  orderGroup: {},
  clearSelectedOrderItems: () => {},
  noBorder: false,
  selectedReasonCode: {},
  clearSelectedResolution: () => {},
  noDisabledState: false,
};

export default withStyles(OrderItem, OrderItemStyles);
export { OrderItem as OrderItemsVanilla };
