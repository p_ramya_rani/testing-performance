// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from '@tcp/core/src/components/common/atoms';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import LoaderSkelton from '@tcp/core/src/components/common/molecules/LoaderSkelton';
import styles from '../styles/OrderTileSkeleton.style';

const OrderTileSkeleton = ({ className }) => {
  return (
    <Row fullBleed className={className}>
      <Col colSize={{ small: 6, medium: 4, large: 3 }} className="elem-mt-LRG">
        <div className="container-skeleton" role="presentation">
          <LoaderSkelton width="100%" height="150px" className="elem-mb-MED" />
        </div>
      </Col>
      <Col colSize={{ small: 6, medium: 4, large: 3 }} className="elem-mt-LRG">
        <div className="container-skeleton" role="presentation">
          <LoaderSkelton width="100%" height="150px" className="elem-mb-MED" />
        </div>
      </Col>
      <Col colSize={{ small: 6, medium: 4, large: 3 }} className="elem-mt-LRG">
        <div className="container-skeleton" role="presentation">
          <LoaderSkelton width="100%" height="150px" className="elem-mb-MED" />
        </div>
      </Col>
    </Row>
  );
};

OrderTileSkeleton.propTypes = {
  className: PropTypes.string,
};
OrderTileSkeleton.defaultProps = {
  className: '',
};

export default withStyles(OrderTileSkeleton, styles);
export { OrderTileSkeleton as OrderTileVanilla };
