// 9fbef606107a605d69c0edbcd8029e5d
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { KEY_CODES } from '@tcp/core/src/constants/keyboard.constants';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import { Row, Col, BodyCopy, Button } from '@tcp/core/src/components/common/atoms';
import { isUsOnly, routerPush } from '@tcp/core/src/utils';
import Separator from '@tcp/core/src/components/common/atoms/Separator';
import styles from '../styles/AccountAndCoupons.styles';
import CouponSelfHelp from '../../TellUsMore/organisms/CouponSelfHelp';
import CustomerServiceHelp from '../../TellUsMore/organisms/CustomerServiceHelp';
import ReasonCodeSkeleton from '../../TellUsMore/skeleton';
import {
  REASON_CODES,
  CUSTOMER_HELP_ROUTES,
  CUSTOMER_HELP_DIRECT_LINK_TEMPLATE,
} from '../../../CustomerHelp.constants';
import MergeAccounts from '../../MergeAccounts';
import { smoothScrolling } from '../../../../../../../utils';
import { isAccountAndCouponURL } from '../../../util/utility';

const getFilteredReasonList = (reasonList, isCouponSelfHelpEnabled, isMergeMyPlaceAccount) => {
  return reasonList.filter((a) => {
    let keepReasonCode = true;
    if (keepReasonCode && !isCouponSelfHelpEnabled)
      keepReasonCode = a.action !== REASON_CODES.COUPON_ISSUE;
    if (keepReasonCode && !isMergeMyPlaceAccount)
      keepReasonCode = a.action !== REASON_CODES.MERGE_MYPLACE_ACCOUNTS;
    if (keepReasonCode && !isUsOnly())
      keepReasonCode = a.action !== REASON_CODES.MERGE_MYPLACE_ACCOUNTS;
    return keepReasonCode;
  });
};

// This function is used for DEEP LINKING of URLs.
const setReasonCodeFromURL = (
  reasonCodesList,
  reasonFromURL,
  handlerMap,
  setSelectedReasonCode,
  trackAnalyticsClick,
  clickAnalyticsData
) => {
  const reasonCodeToSet = reasonCodesList.filter((reasonCode) => {
    return reasonCode.action === reasonFromURL;
  });
  if (reasonCodeToSet.length > 0) {
    handlerMap(setSelectedReasonCode, reasonCodeToSet[0], trackAnalyticsClick, clickAnalyticsData);
  }
};

const renderCustomerHelp = (
  selectedReasonCode,
  labels,
  isLiveChatEnabled,
  isInternationalShipping,
  trackAnalyticsClick,
  isLoggedIn,
  loggedInUserEmail
) => {
  const orderDetailsData = {
    emailAddress: isLoggedIn ? loggedInUserEmail : '',
  };
  return (
    selectedReasonCode &&
    selectedReasonCode.action === REASON_CODES.SOMETHING_ELSE && (
      <>
        <Separator right="-15" />
        <CustomerServiceHelp
          labels={labels}
          isLiveChatEnabled={isLiveChatEnabled}
          isInternationalShipping={isInternationalShipping}
          trackAnalyticsClick={trackAnalyticsClick}
          selectedReasonCode={selectedReasonCode}
          orderDetailsData={orderDetailsData}
        />
      </>
    )
  );
};

const renderMergeAccount = (isLoggedIn, selectedReasonCode, labels) => {
  return (
    selectedReasonCode &&
    selectedReasonCode.action === REASON_CODES.MERGE_MYPLACE_ACCOUNTS && (
      <MergeAccounts labels={labels} />
    )
  );
};

const renderCouponSelfHelp = (
  selectedReasonCode,
  labels,
  orderLabels,
  isLoggedIn,
  walletLabels
) => {
  return (
    selectedReasonCode &&
    selectedReasonCode.action === REASON_CODES.COUPON_ISSUE && (
      <CouponSelfHelp
        labels={{ ...labels, ...orderLabels, ...walletLabels }}
        isLoggedIn={isLoggedIn}
      />
    )
  );
};

const handlerMap = (setSelectedReasonCode, reasonCode, trackAnalyticsClick, clickAnalyticsData) => {
  const reasonCodeLbl = reasonCode ? reasonCode.label : '';
  const clickPath = `${clickAnalyticsData?.orderHelpSelectedReason}|${reasonCodeLbl}`;
  trackAnalyticsClick({
    orderHelp_path_cd128: clickPath,
    orderHelpSelectedReason: reasonCodeLbl,
  });
  switch (reasonCode.action) {
    case REASON_CODES.COUPON_ISSUE:
      setSelectedReasonCode(reasonCode);
      break;
    case REASON_CODES.SOMETHING_ELSE:
      setSelectedReasonCode(reasonCode);
      break;
    case REASON_CODES.MERGE_MYPLACE_ACCOUNTS:
      setSelectedReasonCode(reasonCode);
      break;
    default:
      break;
  }
};

const renderReasonCode = (
  selectedReasonCode,
  reasonCodesAccountsAndCoupons,
  setSelectedReasonCode,
  trackAnalyticsClick,
  filteredReasonList,
  clickAnalyticsData
) => {
  return selectedReasonCode ? (
    <Col colSize={{ small: 6, medium: 4, large: 4 }} className="elem-mt-LRG">
      <div className="reason-code-rectangle selected-item selected-reason elem-mb-LRG">
        <BodyCopy
          component="p"
          textAlign="left"
          fontWeight="semibold"
          fontSize="fs14"
          fontFamily="secondary"
          color="text.darkgray"
          className="elem-pl-MED elem-pr-MED"
        >
          {selectedReasonCode.label}
        </BodyCopy>
      </div>
    </Col>
  ) : (
    !selectedReasonCode &&
      reasonCodesAccountsAndCoupons &&
      filteredReasonList.length > 0 &&
      filteredReasonList.map((reasonCode, index) => (
        <Col
          colSize={{ small: 6, medium: 4, large: 4 }}
          className="elem-mt-LRG"
          ignoreGutter={(index + 1) % 3 === 0 ? { large: true } : {}}
        >
          <BodyCopy
            component="div"
            role="button"
            className="reason-code-rectangle"
            onKeyDown={(e) => {
              if (e.keyCode === KEY_CODES.KEY_ENTER)
                handlerMap(
                  setSelectedReasonCode,
                  reasonCode,
                  trackAnalyticsClick,
                  clickAnalyticsData
                );
            }}
            onClick={() =>
              handlerMap(setSelectedReasonCode, reasonCode, trackAnalyticsClick, clickAnalyticsData)
            }
            tabIndex={0}
          >
            <BodyCopy
              component="p"
              textAlign="left"
              fontWeight="semibold"
              fontSize="fs14"
              fontFamily="secondary"
              color="text.darkgray"
              className="elem-pl-MED elem-pr-MED"
            >
              {reasonCode.label}
            </BodyCopy>
          </BodyCopy>
        </Col>
      ))
  );
};

const editOption = (selectedReasonCode, setSelectedReasonCode) => {
  if (selectedReasonCode?.action === CUSTOMER_HELP_DIRECT_LINK_TEMPLATE.MERGE_MYPLACE_ACCOUNTS) {
    routerPush(
      CUSTOMER_HELP_ROUTES.ACCOUNT_COUPONS.to,
      CUSTOMER_HELP_ROUTES.ACCOUNT_COUPONS.asPath
    );
  }
  setSelectedReasonCode(null);
};

const AccountAndCoupons = ({
  className,
  reasonCodesAccountsAndCoupons,
  labels,
  orderLabels,
  walletLabels,
  isLoggedIn,
  isLiveChatEnabled,
  isInternationalShipping,
  isCouponSelfHelpEnabled,
  router,
  setSelectedTopReasonCode,
  topLevelReasonCodes,
  trackAnalyticsClick,
  loggedInUserEmail,
  isMergeMyPlaceAccount,
  clickAnalyticsData,
  selectedAccountsReasonCode: selectedReasonCode,
  setSelectedAccountsReasonCode: setSelectedReasonCode,
}) => {
  const filteredReasonList = getFilteredReasonList(
    reasonCodesAccountsAndCoupons,
    isCouponSelfHelpEnabled,
    isMergeMyPlaceAccount
  );
  useEffect(() => {
    setTimeout(() => {
      if (reasonCodesAccountsAndCoupons && reasonCodesAccountsAndCoupons.length > 0) {
        smoothScrolling('step-1');
      }
    }, 100);
  }, [reasonCodesAccountsAndCoupons]);

  useEffect(() => {
    setTimeout(() => {
      if (selectedReasonCode) {
        smoothScrolling('step-2');
      }
    }, 100);
  }, [selectedReasonCode]);
  useEffect(() => {
    const reasonCodeFromURL = router.query?.reasonCode?.toUpperCase();
    if (reasonCodeFromURL) {
      setReasonCodeFromURL(
        filteredReasonList,
        reasonCodeFromURL,
        handlerMap,
        setSelectedReasonCode,
        trackAnalyticsClick,
        clickAnalyticsData
      );
    }
  }, [reasonCodesAccountsAndCoupons, router.query?.reasonCode]);

  useEffect(() => {
    const reasonCodeToSet =
      topLevelReasonCodes &&
      topLevelReasonCodes?.filter((rcode) => rcode.action === REASON_CODES.ACCOUNT_AND_COUPONS);
    if (isAccountAndCouponURL(router)) {
      setSelectedTopReasonCode(reasonCodeToSet[0]);
    }
  }, [topLevelReasonCodes]);

  useEffect(() => {
    return () => setSelectedReasonCode(null);
  }, []);

  return !reasonCodesAccountsAndCoupons || !reasonCodesAccountsAndCoupons.length > 0 ? (
    <ReasonCodeSkeleton />
  ) : (
    <div className={className}>
      <Row fullBleed>
        <Col colSize={{ small: 6, medium: 7, large: 12 }}>
          <BodyCopy component="div" className="header-title" id="step-1">
            <BodyCopy
              component="p"
              fontWeight="bold"
              fontFamily="secondary"
              fontSize="fs16"
              className="choose-the-action"
            >
              {getLabelValue(orderLabels, 'lbl_csh_select_an_action', 'orders')}
            </BodyCopy>
            {selectedReasonCode && (
              <Button
                className="edit-option"
                onClick={() => editOption(selectedReasonCode, setSelectedReasonCode)}
                nohover
                type="button"
                link
                underline
              >
                {getLabelValue(orderLabels, 'lbl_edit_option', 'orders')}
              </Button>
            )}
          </BodyCopy>
        </Col>
      </Row>
      <Row fullBleed>
        {renderReasonCode(
          selectedReasonCode,
          reasonCodesAccountsAndCoupons,
          setSelectedReasonCode,
          trackAnalyticsClick,
          filteredReasonList,
          clickAnalyticsData
        )}
      </Row>
      <div id="step-2">
        {renderCouponSelfHelp(selectedReasonCode, labels, orderLabels, isLoggedIn, walletLabels)}
        {renderCustomerHelp(
          selectedReasonCode,
          labels,
          isLiveChatEnabled,
          isInternationalShipping,
          trackAnalyticsClick,
          isLoggedIn,
          loggedInUserEmail
        )}
        {renderMergeAccount(isLoggedIn, selectedReasonCode, labels)}
      </div>
    </div>
  );
};

AccountAndCoupons.propTypes = {
  props: PropTypes.shape({}).isRequired,
  labels: PropTypes.shape({}).isRequired,
  clickAnalyticsData: PropTypes.shape({}).isRequired,
  orderLabels: PropTypes.shape({}).isRequired,
  className: PropTypes.string,
  reasonCodesAccountsAndCoupons: PropTypes.shape([]),
  walletLabels: PropTypes.shape({}).isRequired,
  isLoggedIn: PropTypes.bool,
  isLiveChatEnabled: PropTypes.bool.isRequired,
  router: PropTypes.shape({}),
  isCouponSelfHelpEnabled: PropTypes.bool.isRequired,
  referred: PropTypes.shape([]).isRequired,
  trackAnalyticsClick: PropTypes.func.isRequired,
  loggedInUserEmail: PropTypes.string,
};
AccountAndCoupons.defaultProps = {
  className: '',
  reasonCodesAccountsAndCoupons: [],
  isLoggedIn: false,
  router: {},
  loggedInUserEmail: '',
};
export default withStyles(AccountAndCoupons, styles);
export { AccountAndCoupons as AccountAndCouponsVanilla };
