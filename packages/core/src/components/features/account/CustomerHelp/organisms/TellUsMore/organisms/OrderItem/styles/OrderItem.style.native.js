// 9fbef606107a605d69c0edbcd8029e5d
import styled, { css } from 'styled-components/native';
import { BodyCopy } from '@tcp/core/src/components/common/atoms';

const QuantityHolder = styled.View`
  text-align: center;
  position: absolute;
  right: 12px;
  top: 40px;
  height: 50px;
  margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.XXXS};
`;

const ItemStatusTag = styled.View`
  position: absolute;
  flex-direction: row;
  border-radius: 14px;
  justify-content: space-around;
  height: 24px;
  width: 87px;
  max-width: 87px;
  align-items: center;
  border: 1px solid ${(props) => props.theme.colorPalette.black};
  display: flex;
  right: 6px;
  padding: 0px ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  background-color: ${(props) => props.theme.colorPalette.black};
`;

const QuantityLabel = styled.Text`
  font-size: ${(props) => props.theme.fonts.fontSize.anchor.medium}px;
  color: ${(props) => props.theme.colors.PRIMARY.BLUE};
  font-family: ${(props) => props.theme.typography.fonts.secondary};
  font-weight: ${(props) => props.theme.typography.fontWeights.extrabold};
`;

const StatusLabel = styled(BodyCopy)`
  margin-left: 5px;
  margin-top: 1px;
`;
const SubText = styled(BodyCopy)`
  margin-top: 5px;
`;

const OrderItemStyles = css`
  .pdp-qty {
    font-size: ${(props) => props.theme.fonts.fontSize.anchor.medium}px;
    color: ${(props) => props.theme.colors.PRIMARY.BLUE};
  }
  .item-style {
    line-height: 1.5;
  }
  .image-text {
    margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
  }
`;

export { QuantityHolder, OrderItemStyles, ItemStatusTag, QuantityLabel, StatusLabel, SubText };
