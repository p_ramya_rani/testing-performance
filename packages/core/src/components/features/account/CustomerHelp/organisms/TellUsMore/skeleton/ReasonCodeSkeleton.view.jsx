// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { Row, Col } from '@tcp/core/src/components/common/atoms';
import LoaderSkelton from '@tcp/core/src/components/common/molecules/LoaderSkelton';
import styles from '../styles/ReasonCodeSkeleton.style';

const ReasonCodeSkeleton = ({ className }) => {
  return (
    <Row fullBleed className={className}>
      <Col colSize={{ small: 6, medium: 4, large: 4 }} className="elem-mt-LRG">
        <div className="reason-code-skeleton-rectangle">
          <LoaderSkelton width="100%" height="58px" className="elem-mb-MED" />
        </div>
      </Col>
      <Col colSize={{ small: 6, medium: 4, large: 4 }} className="elem-mt-LRG">
        <div className="reason-code-skeleton-rectangle">
          <LoaderSkelton width="100%" height="58px" className="elem-mb-MED" />
        </div>
      </Col>
      <Col colSize={{ small: 6, medium: 4, large: 4 }} className="elem-mt-LRG">
        <div className="reason-code-skeleton-rectangle">
          <LoaderSkelton width="100%" height="58px" className="elem-mb-MED" />
        </div>
      </Col>
    </Row>
  );
};

ReasonCodeSkeleton.propTypes = {
  orderDetailsData: PropTypes.shape({}).isRequired,
  selectedReasonCode: PropTypes.shape({}).isRequired,
  labels: PropTypes.shape({}).isRequired,
  className: PropTypes.string,
};

ReasonCodeSkeleton.defaultProps = {
  className: '',
};

export default withStyles(ReasonCodeSkeleton, styles);
export { ReasonCodeSkeleton as ReasonCodeSkeletonVanilla };
