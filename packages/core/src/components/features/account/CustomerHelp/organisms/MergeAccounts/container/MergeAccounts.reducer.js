// 9fbef606107a605d69c0edbcd8029e5d
import { fromJS } from 'immutable';
import constants from '../MergeAccounts.constants';

const initialState = fromJS({
  mergeAccountButtonLoader: false,
  mergeAccountSuccess: null,
  error: null,
  numberOfRetries: 0,
  accountStatus: constants.ACCOUNT_STATUS[0],
  firstTimeOtp: false,
  showConfirmationModal: false,
  isMergeConfirmed: false,
  isFromOTPFlow: false,
});

const mergeAccountReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case constants.BTN_LOADER:
      return state.set('mergeAccountButtonLoader', payload);
    case constants.IS_FROM_OTP:
      return state.set('isFromOTPFlow', payload);
    case constants.SET_NUMBER_OF_RETRIES:
      return state.set('numberOfRetries', payload);
    case constants.SET_ACCOUNT_STATUS:
      return state.set('accountStatus', payload);
    case constants.MERGE_ACCOUNT_SUCCESS: {
      return state
        .set('error', null)
        .set('mergeAccountSuccess', payload)
        .set('isMergeConfirmed', false);
    }
    case constants.MERGE_ACCOUNT_FAILED: {
      return state
        .set('error', payload)
        .set('mergeAccountSuccess', null)
        .set('isMergeConfirmed', false);
    }
    case constants.SET_FIRST_TIME_OTP: {
      return state.set('firstTimeOtp', payload);
    }
    case constants.MERGE_ACCOUNT_RESET: {
      return state
        .set('mergeAccountSuccess', null)
        .set('error', null)
        .set('mergeAccountButtonLoader', false)
        .set('numberOfRetries', 0)
        .set('accountStatus', constants.ACCOUNT_STATUS[0])
        .set('firstTimeOtp', false)
        .set('showConfirmationModal', false)
        .set('isMergeConfirmed', false);
    }
    case constants.SET_SHOW_CONFIRMATION_MODAL: {
      return state.set('showConfirmationModal', payload);
    }
    case constants.SET_MERGE_CONFIRMED: {
      return state.set('isMergeConfirmed', payload);
    }
    default:
      if (state instanceof Object) {
        return fromJS(state);
      }
      return state;
  }
};

export default mergeAccountReducer;
