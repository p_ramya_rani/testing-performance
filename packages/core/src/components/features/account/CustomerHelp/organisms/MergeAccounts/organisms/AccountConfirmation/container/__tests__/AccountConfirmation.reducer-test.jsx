// 9fbef606107a605d69c0edbcd8029e5d
import { fromJS, Map } from 'immutable';
import accountConfirmationReducer from '../AccountConfirmation.reducer';
import ACCOUNT_CONFIRMATION_CONSTANTS from '../../AccountConfirmation.constants';

describe('Account confirmation Reducer', () => {
  const initialState = fromJS({
    error: null,
    accountConfirmationSuccess: null,
  });

  it('should return empty merge account account confirmation as default state', () => {
    expect(accountConfirmationReducer(undefined, {}).get('accountConfirmationSuccess')).toBeNull();
    expect(accountConfirmationReducer(undefined, {}).get('error')).toBeNull();
  });

  it('should be called on merge account confirmation success', () => {
    const payload = {
      status: 'SUCCESS',
      timestamp: '2021-09-06T08:56:34.528516',
      data: {
        status: 'Active',
        email: 'test@test.com',
      },
    };
    expect(
      accountConfirmationReducer(initialState, {
        type: ACCOUNT_CONFIRMATION_CONSTANTS.ACCOUNT_CONFIRMATION_SUCCESS,
        payload,
      })
    ).toEqual(
      Map({
        error: null,
        accountConfirmationSuccess: payload,
      })
    );
  });

  it('should be called on merge account confirmation failed', () => {
    const err = fromJS({
      status: 'ERROR',
      timestamp: '2021-09-07T08:38:37.264499',
      errors: [
        {
          errorCode: 'Internal Server Error',
          errorMessage: 'Error occured while fetching the members details from Brierley',
          errorParameters: null,
          errorType: null,
        },
      ],
      exceptionType: 'RUNTIME_EXCEPTION',
    });
    expect(
      accountConfirmationReducer(initialState, {
        type: ACCOUNT_CONFIRMATION_CONSTANTS.ACCOUNT_CONFIRMATION_FAILED,
        payload: err,
      })
    ).toEqual(
      Map({
        error: err,
        accountConfirmationSuccess: null,
      })
    );
  });

  it('should be called on merge account confirmation state reset', () => {
    const payload = {};
    expect(
      accountConfirmationReducer(initialState, {
        type: ACCOUNT_CONFIRMATION_CONSTANTS.RESET_ACCOUNT_CONFIRMATION,
        payload,
      })
    ).toEqual(Map(initialState));
  });
});
