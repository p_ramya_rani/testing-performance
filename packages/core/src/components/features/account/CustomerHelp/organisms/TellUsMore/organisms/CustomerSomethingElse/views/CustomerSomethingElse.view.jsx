// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState, useEffect } from 'react';
import { Row, Col, BodyCopy, TextArea } from '@tcp/core/src/components/common/atoms';
import PropTypes from 'prop-types';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import Button from '@tcp/core/src/components/common/atoms/Button';
import { smoothScrolling } from '@tcp/core/src/utils';
import styles from '../../Resolution/styles/Resolution.style';

const refToInputElement = React.createRef();
const inputElementKey = '0';

const onContinueButton = ({ setWrongItemData, isSomethingElse, text, setIsError }) => {
  if (isSomethingElse) {
    if (text !== '') {
      setWrongItemData({ someThingElse: 'true', comments: text });
      setIsError(false);
    } else {
      setIsError(true);
    }
  } else {
    setWrongItemData({ someThingElse: 'false', comments: text });
    setIsError(false);
  }
  setTimeout(() => {
    smoothScrolling('step-6');
  }, 100);
};

const CustomerSomethingElse = ({
  labels,
  setWrongItemData,
  isSomethingElse,
  wrongItemData,
  className,
}) => {
  const [isError, setIsError] = useState(false);
  const [text, setText] = useState('');
  const topPlaceHolder = getLabelValue(labels, 'lbl_placeholder_value_textArea');
  const maxCharacter = 200;
  const [placeHolder, setPlaceHolder] = useState({
    top: topPlaceHolder,
    bottom: maxCharacter.toString(),
  });

  const onChangeHandler = (e) => {
    const { value } = e.target;
    setText(value);
    setIsError(false);
    if (value !== '') {
      const charLeft = (maxCharacter - Number(text.length)).toString();
      setPlaceHolder({ top: '', bottom: charLeft });
    } else {
      setPlaceHolder({
        top: topPlaceHolder,
        bottom: maxCharacter.toString(),
      });
    }
  };

  useEffect(() => {
    if (wrongItemData && Object.keys(wrongItemData).length && wrongItemData.comments) {
      const charLeft = (maxCharacter - Number(wrongItemData.comments.length)).toString();
      setText(wrongItemData.comments);
      setPlaceHolder({ top: '', bottom: charLeft });
    }
    if (wrongItemData && !Object.keys(wrongItemData).length) {
      setText('');
      setPlaceHolder({
        top: topPlaceHolder,
        bottom: maxCharacter.toString(),
      });
    }
  }, [wrongItemData]);

  return (
    <div className={`${className} `}>
      <Row fullBleed>
        <>
          <Row fullBleed id="somethingElseContainer">
            <Col colSize={{ small: 6, medium: 4, large: 4 }}>
              <BodyCopy component="p" fontWeight="bold" fontFamily="secondary" fontSize="fs16">
                {getLabelValue(labels, 'lbl_additional_details')}
              </BodyCopy>
            </Col>
          </Row>

          <Row fullBleed className="text-box-container">
            <Col colSize={{ small: 6, medium: 4, large: 4 }} className="full-width">
              <TextArea
                input={{ value: text }}
                inputRef={refToInputElement}
                key={inputElementKey}
                maxLength={maxCharacter}
                enableSuccessCheck={false}
                showExplicitError={isError ? getLabelValue(labels, 'lbl_section_completed') : false}
                placeholder={placeHolder && placeHolder.top}
                roundTextBox
                onChange={(val) => onChangeHandler(val)}
                className="details-text"
              />
              <BodyCopy
                component="p"
                fontFamily="secondary"
                fontSize="fs12"
                className="character-count"
              >
                {`${placeHolder && placeHolder.bottom} ${getLabelValue(
                  labels,
                  'lbl_characters_left'
                )}`}
              </BodyCopy>
            </Col>
          </Row>
        </>
        {wrongItemData && Object.keys(wrongItemData).length === 0 && (
          <Row fullBleed className="elem-mt-LRG align-center edit-form-css">
            <Col colSize={{ small: 6, medium: 4, large: 4 }}>
              <Button
                buttonVariation="fixed-width"
                className="cta-btn"
                fill="BLUE"
                type="submit"
                onClick={() =>
                  onContinueButton({ setWrongItemData, isSomethingElse, text, setIsError })
                }
              >
                {getLabelValue(labels, 'lbl_help_refund_action_btn')}
              </Button>
            </Col>
          </Row>
        )}
      </Row>
    </div>
  );
};

CustomerSomethingElse.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  setWrongItemData: PropTypes.func.isRequired,
  wrongItemData: PropTypes.shape({}).isRequired,
  isSomethingElse: PropTypes.bool,
  className: PropTypes.string,
};

CustomerSomethingElse.defaultProps = {
  isSomethingElse: false,
  className: '',
};

export default withStyles(CustomerSomethingElse, styles);
export { CustomerSomethingElse as CustomerSomethingElseVanilla };
