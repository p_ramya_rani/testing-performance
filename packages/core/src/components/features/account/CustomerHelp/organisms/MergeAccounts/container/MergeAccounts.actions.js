// 9fbef606107a605d69c0edbcd8029e5d
import constants from '../MergeAccounts.constants';

export const setMergeAccountButtonLoader = (payload) => {
  return {
    type: constants.BTN_LOADER,
    payload,
  };
};

export const setIsFromOTP = (payload) => {
  return {
    type: constants.IS_FROM_OTP,
    payload,
  };
};

export const mergeAccountFailure = (payload) => ({
  type: constants.MERGE_ACCOUNT_FAILED,
  payload,
});

export const mergeAccountSuccess = (payload) => ({
  type: constants.MERGE_ACCOUNT_SUCCESS,
  payload,
});

export const setNumberOfRetries = (payload) => ({
  type: constants.SET_NUMBER_OF_RETRIES,
  payload,
});

export const setAccountStatus = (payload) => ({
  type: constants.SET_ACCOUNT_STATUS,
  payload,
});

export const setFirstTimeOtp = (payload) => ({
  type: constants.SET_FIRST_TIME_OTP,
  payload,
});

export const mergeAccountReset = () => ({
  type: constants.MERGE_ACCOUNT_RESET,
});

export const setShowConfirmationModal = (payload) => ({
  type: constants.SET_SHOW_CONFIRMATION_MODAL,
  payload,
});

export const setIsMergeConfirmed = (payload) => ({
  type: constants.SET_MERGE_CONFIRMED,
  payload,
});
