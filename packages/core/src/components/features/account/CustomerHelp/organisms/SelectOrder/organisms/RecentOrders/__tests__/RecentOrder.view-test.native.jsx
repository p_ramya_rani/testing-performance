// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import RecentOrder from '../views/RecentOrder.view';

describe('RecentOrder', () => {
  const props = {
    ordersListItems: {
      orders: [
        {
          orderDate: 'Jan 20, 2021',
          orderNumber: '342614244',
          orderStatus: 'lbl_orders_statusNa',
          status: 'N/A',
          currencySymbol: '$',
          isEcomOrder: true,
          imgPath: '',
        },
        {
          orderDate: 'Jan 20, 2021',
          orderNumber: '342614244',
          orderStatus: 'lbl_orders_statusNa',
          status: 'N/A',
          currencySymbol: '$',
          imgPath: '',
        },
      ],
    },
    orderLabels: {},
    labels: {},
    imgPath: '',
  };

  it('should render RecentOrder without OrderListItems', () => {
    const component = shallow(<RecentOrder imgPath="" />);
    expect(component).toMatchSnapshot();
  });

  it('should render RecentOrder with OrderListItems', () => {
    const component = shallow(<RecentOrder {...props} />);
    expect(component).toMatchSnapshot();
  });
});
