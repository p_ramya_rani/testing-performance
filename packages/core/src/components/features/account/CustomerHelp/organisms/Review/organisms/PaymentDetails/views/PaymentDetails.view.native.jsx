// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { Image } from '@tcp/core/src/components/common/atoms';
import { getIconCard } from '@tcp/core/src/utils/index.native';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import {
  ViewWithSpacing,
  BodyCopyWithSpacing,
} from '@tcp/core/src/components/common/atoms/styledWrapper';
import get from 'lodash/get';
import cardIconMapping from '../../../../../../OrderDetails/organism/OrderBillingDetails/OrderBillingDetails.constants';

import {
  RowContainer,
  Row,
  BlueUnderLine,
  Line,
  PaymentDetailsLogoWrap,
} from '../../../../../styles/CustomerHelp.style.native';

const addCreditCard = require('../../../../../../../../../../../mobileapp/src/assets/images/add-credit-card.png');

const getCardTypeImgUrl = (cardType) => {
  return getIconCard(cardIconMapping[cardType]);
};

const getPaymentType = (card) => {
  if (card && card.afterpay) {
    return 'AFTERPAY';
  }
  return (card && card.cardType && card.cardType.toUpperCase()) || '';
};

const renderGiftCardPayment = (showCardDetails, appliedGiftCards, labels) => {
  const showGCDetails = appliedGiftCards && appliedGiftCards.length > 0;
  let gcCardType = '';
  if (showCardDetails && showGCDetails && appliedGiftCards[0] && appliedGiftCards[0].cardType) {
    gcCardType = appliedGiftCards[0] && appliedGiftCards[0].cardType;
  }
  return (
    showCardDetails &&
    showGCDetails && (
      <Row spacingStyles="margin-top-MED">
        <Image
          width="44px"
          height="27"
          source={getCardTypeImgUrl(gcCardType.toUpperCase())}
          alt={gcCardType}
        />
        <BodyCopyWithSpacing
          spacingStyles="margin-left-SM"
          fontSize="fs14"
          fontFamily="secondary"
          text={getLabelValue(labels, 'lbl_help_gift_card')}
        />
      </Row>
    )
  );
};

const PaymentDetails = ({
  labels,
  ordersLabels,
  orderDetailsData,
  orderStatus,
  showCardDetails,
}) => {
  const { appliedGiftCards } = orderDetailsData;
  const card = get(orderDetailsData, 'checkout.billing.card', {});
  const firstName = get(orderDetailsData, 'checkout.billing.billingAddress.firstName', '');
  const lastName = get(orderDetailsData, 'checkout.billing.billingAddress.lastName', '');
  const billingIcon = (cardTypeUpperCase) => {
    return cardTypeUpperCase !== 'APPLEPAY' && card && card.endingNumbers
      ? `${getLabelValue(ordersLabels, 'lbl_orders_ending')} ${card.endingNumbers.slice(-4)}`
      : '';
  };

  const getFirstNameLastNameView = (cardType) => {
    if (cardType === 'VENMO' || cardType === 'APPLEPAY' || cardType === 'PAYPAL') {
      return null;
    }

    return (
      <BodyCopyWithSpacing
        spacingStyles="margin-top-XXS"
        text={`${firstName} ${lastName}`}
        component="p"
        fontSize="fs14"
        fontFamily="secondary"
      />
    );
  };

  const cardTypeUpperCase = getPaymentType(card);
  const wrapStyle = { flexWrap: 'wrap', flex: 1 };

  return (
    <ViewWithSpacing spacingStyles="margin-top-MED">
      <Row>
        <Image alt="close" source={addCreditCard} height="22px" width="25px" />
        <BodyCopyWithSpacing
          fontSize="fs16"
          fontWeight="extrabold"
          fontFamily="secondary"
          color="black"
          text={getLabelValue(labels, 'lbl_payment_details')}
          spacingStyles="margin-left-XS"
        />
      </Row>
      <BlueUnderLine />
      <>
        <>
          {showCardDetails && card && card.endingNumbers && (
            <RowContainer>
              <Row>
                <PaymentDetailsLogoWrap afterPay={card.afterpay}>
                  <Image
                    width="40px"
                    height="27"
                    source={getIconCard(cardIconMapping[cardTypeUpperCase])}
                    alt={card.cardType}
                  />
                </PaymentDetailsLogoWrap>
                {!card.afterpay && (
                  <BodyCopyWithSpacing
                    spacingStyles="margin-left-SM margin-top-XXS"
                    fontSize="fs14"
                    fontFamily="secondary"
                    text={
                      cardTypeUpperCase !== 'VENMO'
                        ? billingIcon(cardTypeUpperCase)
                        : card.endingNumbers
                    }
                  />
                )}
              </Row>

              {(!!firstName || !!lastName) &&
                !card.afterpay &&
                getFirstNameLastNameView(cardTypeUpperCase)}
            </RowContainer>
          )}
          {renderGiftCardPayment(showCardDetails, appliedGiftCards, labels)}
          <Line spacingStyles="margin-top-SM margin-bottom-SM" />
          <RowContainer>
            <BodyCopyWithSpacing
              style={{ ...wrapStyle }}
              text={orderStatus}
              fontSize="fs14"
              fontWeight="bold"
              fontFamily="secondary"
            />
          </RowContainer>
        </>
      </>
    </ViewWithSpacing>
  );
};
PaymentDetails.propTypes = {
  orderDetailsData: PropTypes.shape({}),
  ordersLabels: PropTypes.shape({}),
  labels: PropTypes.shape({}),
  orderStatus: PropTypes.string,
  showCardDetails: PropTypes.bool.isRequired,
};

PaymentDetails.defaultProps = {
  orderDetailsData: {},
  ordersLabels: {},
  labels: {},
  orderStatus: '',
};

export default PaymentDetails;
