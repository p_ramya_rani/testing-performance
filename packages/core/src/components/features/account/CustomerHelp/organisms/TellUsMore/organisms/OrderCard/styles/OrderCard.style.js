// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  .order-card-container {
    border: solid 1px ${props => props.theme.colorPalette.gray[600]};
    padding: ${props => props.theme.spacing.ELEM_SPACING.MED};
    border-radius: 12px;
    width: 300px;
    @media ${props => props.theme.mediaQuery.smallMax} {
      width: 100%;
    }
    .track-number {
      font-size: 12px;
    }
  }
  .image-container {
    position: relative;
  }

  .image-wrapper {
    position: relative;
    .items-image {
      padding: ${props => props.theme.spacing.ELEM_SPACING.XS};
    }
    .last-image {
      opacity: 0.15;
    }
  }

  .item-count {
    right: 10px;
    position: absolute;
    top: 60px;
    width: 18px;
    height: 18px;
    border-radius: 50%;
    background-color: ${props => props.theme.colorPalette.blue.C900};
    color: ${props => props.theme.colorPalette.white};
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .total-items {
    right: 35px;
    position: absolute;
    text-align: right;
    top: 35px;
    width: 100%;
  }
  .warn-msg-container {
    justify-content: center;
    .warn-msg {
      color: ${props => props.theme.colorPalette.red[800]};
    }
  }
  .delivered-hazy {
    opacity: 0.5;
  }
  .stack-container {
    @media ${props => props.theme.mediaQuery.smallMax} {
      display: flex;
      flex-direction: column;
      align-items: center;
      position: relative;
      .stack-one,
      .stack-two {
        border-bottom: solid 1px ${props => props.theme.colorPalette.gray[600]};
        width: 95%;
        position: absolute;
        top: -15px;
        height: 24px;
        border-radius: 12px;
      }
      .stack-two {
        width: 90%;
        top: -5px;
      }
    }
  }
`;

export default styles;
