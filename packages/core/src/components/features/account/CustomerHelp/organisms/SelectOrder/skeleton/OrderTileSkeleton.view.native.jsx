// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import LoaderSkelton from '@tcp/core/src/components/common/molecules/LoaderSkelton/views/SkeletonLine.view.native';
import ContainerSkeleton from '../styles/OrderTileSkeleton.style.native';

const OrderTileSkeleton = () => {
  return (
    <>
      <ContainerSkeleton>
        <LoaderSkelton width="100%" height="150px" borderRadius="12px" />
      </ContainerSkeleton>
      <ContainerSkeleton>
        <LoaderSkelton width="100%" height="150px" borderRadius="12px" />
      </ContainerSkeleton>
      <ContainerSkeleton>
        <LoaderSkelton width="100%" height="150px" borderRadius="12px" />
      </ContainerSkeleton>
    </>
  );
};

export default OrderTileSkeleton;
export { OrderTileSkeleton as OrderTileVanilla };
