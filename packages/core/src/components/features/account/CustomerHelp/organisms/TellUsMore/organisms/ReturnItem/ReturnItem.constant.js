export const RETURN_ITEM_ICONS = {
  STORE_ICON: `fav-store-icon`,
  SHIPPING_ICON: `fast-shipping`,
  LOCATION_ICON: `location-icon`,
};

export const RETURN_ITEM_METHOD_KEY = {
  BY_MAIL: `BY_MAIL`,
  IN_STORE: `IN_STORE`,
};
