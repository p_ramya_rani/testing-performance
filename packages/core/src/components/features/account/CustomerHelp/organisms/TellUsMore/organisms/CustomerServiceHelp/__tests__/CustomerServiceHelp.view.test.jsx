// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { CustomerServiceHelpVanilla } from '../views/CustomerServiceHelp.view';

describe('Customer Service Help View', () => {
  const props = {
    labels: {
      lbl_how_to_solve: 'How can we resolve this?',
    },
    isNonOnlineOrOlderOrder: true,
    isSomethingElse: true,
    isLiveChatEnabled: true,
    isInternationalShipping: true,
    isInitiateReturnFailure: true,
    helpMessage: 'Help-Message',
  };

  it('should render CustomerServiceHelp', () => {
    const component = shallow(<CustomerServiceHelpVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('should render return policy link ', () => {
    const component = shallow(<CustomerServiceHelpVanilla showReturnPolicy {...props} />);
    expect(component).toMatchSnapshot();
  });
});
