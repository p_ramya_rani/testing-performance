// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { MergeConfimationModalVanilla } from '../MergeConfimationModal';

const props = {
  className: '',
  isModalOpen: true,
  closeModal: () => {},
  setIsMergeConfirmed: () => {},
  labels: {},
};
describe('Review Template', () => {
  it('should render Review Template', () => {
    const component = shallow(<MergeConfimationModalVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
