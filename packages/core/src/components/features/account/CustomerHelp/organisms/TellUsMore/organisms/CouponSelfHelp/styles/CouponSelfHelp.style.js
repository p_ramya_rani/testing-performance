// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  &.coupon-self-help-container {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXXS};
  }

  .slick-dots {
    position: relative;
    bottom: 0;
  }

  .couponCard_slick {
    margin: ${(props) => props.theme.spacing.ELEM_SPACING.MED}
      ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
  }
  .couponTable {
    width: 100% !important;
  }
`;

export default styles;
