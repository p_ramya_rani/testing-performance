// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  .parent-header {
    display: flex;
  }
  .header-title {
    display: flex;
    justify-content: space-between;
  }
  .choose-the-action {
    display: flex;
  }
`;
export default styles;
