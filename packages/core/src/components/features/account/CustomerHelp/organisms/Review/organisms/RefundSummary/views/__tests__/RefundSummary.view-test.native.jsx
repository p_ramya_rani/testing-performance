// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import RefundSummary from '../RefundSummary.view';

describe('Order Summary Details component', () => {
  it('should renders correctly with refunded data', () => {
    const props = {
      orderLabels: {},
      orderDetailsData: {
        summary: {
          currencySymbol: '$',
          grandTotal: 12.5,
          shippingTotal: 23,
          totalTax: 1.37,
          canceledItems: 1,
          purchasedItems: 1,
          subTotal: 23,
        },
      },
      labels: {},
    };
    const component = shallow(<RefundSummary {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('if orderDetails Data is not available', () => {
    const props = {
      orderLabels: {},
      orderDetailsData: {},
      labels: {},
    };
    const component = shallow(<RefundSummary {...props} />);
    expect(component).toMatchSnapshot();
  });
});
