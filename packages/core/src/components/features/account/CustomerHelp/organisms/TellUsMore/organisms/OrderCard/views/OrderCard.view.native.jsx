// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { Anchor } from '@tcp/core/src/components/common/atoms';
import { BodyCopyWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';
import { getLabelValue, getShipmentCount } from '@tcp/core/src/utils/utils';
import { getAppeasedShipmentStatus, getWarningMsg } from '../../../../../util/utility';
import OrderItemHeader from '../../OrderItemHeader';
import { ORDER_ITEM_STATUS, REASON_CODES } from '../../../../../CustomerHelp.constants';
import {
  OrderCardContainer,
  ImageContainer,
  ImageWrapper,
  ItemCount,
  TotalItems,
  StackedContainer,
  FirstStack,
  SecondStack,
  ItemsImage,
  FullBleedView,
  ItemCountWrapper,
} from '../styles/OrderCard.style.native';

const getOrderDisplayStatus = (orderStatus, orderLabels) => {
  const { IN_TRANSIT, SHIPMENT_API_DELIVERED, IN_PROGRESS } = ORDER_ITEM_STATUS;

  switch (orderStatus) {
    case IN_TRANSIT:
      return getLabelValue(orderLabels, 'lbl_orders_statusItemsInTransit', 'orders');

    case SHIPMENT_API_DELIVERED:
      return getLabelValue(orderLabels, 'lbl_orders_statusItemsDelivered', 'orders');

    case IN_PROGRESS:
      return getLabelValue(orderLabels, 'lbl_orders_statusItemsInProgress', 'orders');

    default:
      return '';
  }
};

const getCardStyle = ({ orderGroup, selectedShipmentCard, filterValue, itemIndex, isAppeased }) => {
  if (orderGroup.orderStatus === filterValue && !isAppeased) {
    if (selectedShipmentCard === itemIndex) {
      return { selected: true, hazy: false };
    }
    return { selected: false, hazy: false };
  }
  return { selected: false, hazy: true };
};

const getItemIndex = (orderGroupItems) => {
  if (Array.isArray(orderGroupItems) && orderGroupItems[0]) {
    return orderGroupItems[0].lineNumber;
  }
  return null;
};

const cardClick = ({
  onCardClick,
  itemIndex,
  orderGroup,
  action,
  filterValue,
  getFilteredList,
  setToggleCards,
  isAppeased,
}) => {
  const { PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME } = REASON_CODES;
  if (action === PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME && !isAppeased) {
    setToggleCards(true);
  } else if (
    orderGroup.orderStatus === filterValue &&
    action !== PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME &&
    !isAppeased
  ) {
    onCardClick(itemIndex, orderGroup);
    getFilteredList(itemIndex, action);
  }
  return null;
};

const getFilterValue = (selectedAction) => {
  const {
    TRACKING_NUMBER_SHOW_IN_TRANSIT_FOR_LONG_TIME,
    PACKAGE_DELIVERED_BUT_NOT_RECEIVED,
    PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME,
  } = REASON_CODES;
  const { IN_TRANSIT, SHIPMENT_API_DELIVERED } = ORDER_ITEM_STATUS;
  let filterKey;
  if (selectedAction === TRACKING_NUMBER_SHOW_IN_TRANSIT_FOR_LONG_TIME) {
    filterKey = IN_TRANSIT;
  } else if (
    selectedAction === PACKAGE_DELIVERED_BUT_NOT_RECEIVED ||
    selectedAction === PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME
  ) {
    filterKey = SHIPMENT_API_DELIVERED;
  }
  return filterKey;
};

const getToggleButton = (
  selectedShipmentCard,
  action,
  setIsDelivered,
  labels,
  isDelivered,
  ...others
) => {
  const [orderItems, orderGroupExists, nonOrderGroupExists] = others;
  const { PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME } = REASON_CODES;
  if (
    !selectedShipmentCard &&
    action !== PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME &&
    orderItems &&
    orderItems.length > 1 &&
    orderGroupExists &&
    nonOrderGroupExists
  ) {
    return (
      <FullBleedView centered>
        <Anchor
          text={
            isDelivered
              ? getLabelValue(labels, 'lbl_hide_other_shipments', '')
              : getLabelValue(labels, 'lbl_show_all_shipments', '')
          }
          fontSizeVariation="large"
          anchorVariation="secondary"
          fontSize="fs14"
          fontFamily="primary"
          onPress={() => {
            setIsDelivered((prev) => !prev);
          }}
          underlineBlue
        />
      </FullBleedView>
    );
  }
  return null;
};

const getFullListCondition = ({
  isDelivered,
  selectedShipmentCard,
  selectedAction,
  toggleCards,
  paidShippingOverMobile,
  filterKey,
  orderItems,
}) => {
  const { PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME } = REASON_CODES;
  const orderGroupExists =
    orderItems && orderItems.some((order) => order.orderStatus === filterKey);
  const paidShippingNonMobile = selectedAction === PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME;

  if (
    (isDelivered && !selectedShipmentCard && selectedAction) ||
    paidShippingNonMobile ||
    (paidShippingOverMobile && toggleCards) ||
    !orderGroupExists
  ) {
    return true;
  }
  return false;
};

const OrderCard = ({
  orderDetailsData,
  orderLabels,
  labels,
  isDelivered,
  onCardClick,
  selectedShipmentCard,
  action,
  setToggleCards,
  toggleCards,
  setIsDelivered,
  filteredItems,
  setFilteredList,
}) => {
  const { orderItems } = orderDetailsData || {};
  const shipmentCount = getShipmentCount(orderItems);

  const [filterValue, setFilter] = useState(null);
  const { PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME } = REASON_CODES;
  const filterKey = getFilterValue(action);
  const getFilteredList = (cardIdx, selectedAction) => {
    let filteredList;
    const paidShippingOverMobile = selectedAction === PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME;
    setFilter(filterKey);
    if (
      getFullListCondition({
        isDelivered,
        selectedShipmentCard,
        selectedAction,
        toggleCards,
        paidShippingOverMobile,
        filterKey,
        orderItems,
      })
    ) {
      filteredList = orderItems;
    } else if (paidShippingOverMobile && !toggleCards) {
      const orderItemsGroup = orderItems && orderItems[0];
      if (orderItemsGroup) {
        filteredList = [orderItemsGroup];
      }
    } else if (selectedShipmentCard) {
      filteredList =
        orderItems &&
        orderItems.filter(
          (orderGroup) =>
            orderGroup.items &&
            orderGroup.items.length > 0 &&
            orderGroup.items[0].lineNumber === cardIdx
        );
    } else {
      filteredList =
        orderItems && orderItems.filter((orderGroup) => orderGroup.orderStatus === filterKey);
    }
    setFilteredList(filteredList);
  };

  const orderGroupExists =
    orderItems && orderItems.some((order) => order.orderStatus === filterKey);
  const nonOrderGroupExists =
    orderItems && orderItems.some((order) => order.orderStatus !== filterKey);

  useEffect(() => {
    if (orderItems) {
      getFilteredList(selectedShipmentCard, action);
    }
  }, [isDelivered, selectedShipmentCard, toggleCards, orderItems]);

  return (
    <>
      <FullBleedView>
        {filteredItems &&
          filteredItems.length > 0 &&
          filteredItems.map((orderGroup, index) => {
            const orderGroupItems = orderGroup.items ? orderGroup.items : null;
            const itemIndex = getItemIndex(orderGroupItems);
            const isAppeased = getAppeasedShipmentStatus({ orderGroup });
            const { selected, hazy } = getCardStyle({
              action,
              orderGroup,
              selectedShipmentCard,
              filterValue,
              itemIndex,
              toggleCards,
              isAppeased,
            });
            return (
              <OrderCardContainer
                selected={selected}
                hazy={hazy}
                key={itemIndex.toString()}
                onPress={() => {
                  cardClick({
                    onCardClick,
                    itemIndex,
                    orderGroup,
                    action,
                    filterValue,
                    getFilteredList,
                    setToggleCards,
                    isAppeased,
                  });
                }}
              >
                <FullBleedView>
                  <OrderItemHeader
                    textFonSize="fs12"
                    shipmentNumber={index + 1}
                    ItemsCount={shipmentCount}
                    status={orderGroup.orderStatus}
                    trackingNumber={orderGroup.trackingNumber}
                    trackingUrl={orderGroup.trackingUrl}
                    shippedDate={orderGroup.shippedDate}
                    orderLabels={orderLabels && orderLabels.orders}
                    orderStatus={orderGroup.orderStatus}
                    orderDisplayStatus={getOrderDisplayStatus(orderGroup.orderStatus, orderLabels)}
                    mediumText
                  />
                </FullBleedView>
                <ImageContainer>
                  {orderGroupItems.slice(0, 4).map((item, idx) => (
                    <ImageWrapper key={idx.toString()}>
                      <ItemsImage
                        width="56.9px"
                        height="62.2px"
                        url={
                          item.productInfo.imagePath.includes('https')
                            ? item.productInfo.imagePath
                            : `https://${item.productInfo.imagePath}`
                        }
                        alt={item.productInfo.name}
                        lastImage={orderGroupItems.length > 4 && idx === 3}
                      />
                      {item.itemInfo.quantity > 1 && (
                        <ItemCountWrapper>
                          <ItemCount
                            textAlign="center"
                            fontSize="fs12"
                            text={item.itemInfo.quantity}
                          />
                        </ItemCountWrapper>
                      )}
                      {orderGroupItems.length > 4 && idx === 3 && (
                        <TotalItems fontSize="fs12" fontFamily="secondary" fontWeight="extrabold">
                          {`+${orderGroupItems.length - 3}`}
                        </TotalItems>
                      )}
                    </ImageWrapper>
                  ))}
                </ImageContainer>
                <View>
                  <BodyCopyWithSpacing
                    fontSize="fs14"
                    fontFamily="secondary"
                    fontWeight="extrabold"
                    color="red.800"
                    textAlign="center"
                    text={getWarningMsg({
                      orderGroup,
                      filterValue,
                      action,
                      orderGroupExists,
                      isAppeased,
                      labels,
                    })}
                  />
                </View>
              </OrderCardContainer>
            );
          })}
      </FullBleedView>
      {action === PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME && !toggleCards && (
        <StackedContainer>
          <FirstStack />
          <SecondStack />
        </StackedContainer>
      )}
      {getToggleButton(
        selectedShipmentCard,
        action,
        setIsDelivered,
        labels,
        isDelivered,
        orderItems,
        orderGroupExists,
        nonOrderGroupExists
      )}
    </>
  );
};

OrderCard.propTypes = {
  orderDetailsData: PropTypes.shape({}).isRequired,
  selectedReasonCode: PropTypes.shape({}),
  labels: PropTypes.shape({}),
  orderLabels: PropTypes.shape({}),
  setSelectedOrderItems: PropTypes.shape({}),
  selectedOrderItems: PropTypes.shape({}),
  shipmentData: PropTypes.shape({}),
  isDelivered: PropTypes.bool,
  onCardClick: PropTypes.func,
  selectedShipmentCard: PropTypes.string,
  action: PropTypes.string,
  setToggleCards: PropTypes.func,
  toggleCards: PropTypes.bool,
  setIsDelivered: PropTypes.func,
  filteredItems: PropTypes.shape([]),
  setFilteredList: PropTypes.func,
};

OrderCard.defaultProps = {
  orderLabels: {},
  setSelectedOrderItems: {},
  selectedOrderItems: {},
  selectedReasonCode: {},
  labels: {},
  shipmentData: {},
  isDelivered: false,
  onCardClick: () => {},
  selectedShipmentCard: '',
  action: '',
  setToggleCards: () => {},
  toggleCards: false,
  setIsDelivered: () => {},
  filteredItems: [],
  setFilteredList: () => {},
};

export default OrderCard;
