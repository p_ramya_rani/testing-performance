// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { CustomerHelpProgressIndicator } from '../container/CustomerHelpProgressIndicator.container';
import CustomerHelpProgress from '../views';

describe('Customer Help container', () => {
  it('should render Customer Help component', () => {
    const component = shallow(<CustomerHelpProgressIndicator />);
    expect(component.is(CustomerHelpProgress)).toBeTruthy();
  });
});
