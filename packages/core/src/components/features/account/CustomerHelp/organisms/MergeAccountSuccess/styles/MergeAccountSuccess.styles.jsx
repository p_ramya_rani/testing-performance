import { css } from 'styled-components';

const styles = css`
  &.info-container {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
  }

  .failure-text {
    color: ${(props) => props.theme.colorPalette.red[500]};
  }

  .success-text {
    color: ${(props) => props.theme.colorPalette.green[500]};
    @media ${(props) => props.theme.mediaQuery.large} {
      margin: auto auto ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    }
  }

  .img-container {
    margin-bottom: 0px;
  }

  &.info {
    display: flex;
    justify-content: center;
    align-items: center;
  }
  .merge-success-detail {
    width: 50%;
    text-align: center;
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
  }

  .merge-success-text {
    width: 50%;
    text-align: center;
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  }
`;
export default styles;
