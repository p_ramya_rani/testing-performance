// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { BodyCopy, Row, Col, Image } from '@tcp/core/src/components/common/atoms';
import { getIconPath } from '@tcp/core/src/utils';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import CustomerServiceHelp from '../../../../TellUsMore/organisms/CustomerServiceHelp';
import { formatAmount } from '../../../../../util/utility';

const RequestNotCompleted = ({
  className,
  imageName,
  title,
  helpMessage,
  caseId,
  labels,
  refundUnderReview,
  currencySymbol,
  refundTotal,
  selectedReasonCode,
  orderDetailsData,
  errorLabel,
  selectedOrderItems,
  isLiveChatEnabled,
  isInternationalShipping,
  trackAnalyticsClick,
  selectedReturnMethod,
  isInitiateReturnFailure,
}) => {
  return (
    <>
      <div className="row-background">
        <BodyCopy component="div" className={className}>
          <Row fullBleed>
            <Col colSize={{ large: 12, medium: 8, small: 6 }}>
              <BodyCopy className="elem-mb-SM refund-successful">
                <Image alt={imageName} src={getIconPath(imageName)} height="49px" width="56px" />
              </BodyCopy>
            </Col>
          </Row>

          <Row fullBleed>
            <Col colSize={{ large: 12, medium: 8, small: 6 }}>
              <BodyCopy
                fontSize="fs18"
                fontWeight="extrabold"
                fontFamily="secondary"
                className={`elem-mb-SM refund-successful ${
                  refundUnderReview ? 'refund-review-text' : 'refund-failed-text'
                }`}
              >
                {title}
              </BodyCopy>
            </Col>
          </Row>

          <Row fullBleed>
            <Col colSize={{ large: 12, medium: 8, small: 6 }}>
              <BodyCopy
                component="div"
                fontSize="fs14"
                fontWeight="extrabold"
                textAlign="center"
                className="elem-mb-SM refund-successful"
                fontFamily="secondary"
              >
                {helpMessage}
              </BodyCopy>
            </Col>
          </Row>

          {caseId && (
            <Row fullBleed>
              <Col colSize={{ small: 12 }}>
                <BodyCopy
                  component="div"
                  fontSize="fs14"
                  fontWeight="extra"
                  className="elem-mb-SM refund-successful"
                  fontFamily="secondary"
                >
                  {`${getLabelValue(labels, 'lbl_review_under_case')}${caseId || ''}`}
                </BodyCopy>
              </Col>
            </Row>
          )}

          {errorLabel ===
            'GC_IN_ORDER' /** Refund items should be visible only when GC_IN_ORDER for refund under review screen */ && (
            <BodyCopy component="div" className="refund-successful">
              <BodyCopy
                className="elem-mr-XXXL"
                fontSize="fs14"
                fontWeight="extra"
                fontFamily="secondary"
              >
                {getLabelValue(labels, 'lbl_refund_total')}
              </BodyCopy>

              <BodyCopy
                className="elem-ml-XXXL"
                fontSize="fs18"
                fontWeight="extrabold"
                fontFamily="secondary"
              >
                {formatAmount(refundTotal, currencySymbol)}
              </BodyCopy>
            </BodyCopy>
          )}
        </BodyCopy>
      </div>
      <div className="row-background row-margin">
        <Row fullBleed>
          <Col colSize={{ small: 12 }}>
            <BodyCopy
              component="p"
              fontWeight="semibold"
              fontSize="fs16"
              fontFamily="secondary"
              color="black"
            >
              {getLabelValue(labels, 'lbl_merge_customer_service_text')}
            </BodyCopy>
          </Col>
        </Row>
        <Row fullBleed>
          <Col className="contact-us-options" colSize={{ small: 4, medium: 8, large: 12 }}>
            <CustomerServiceHelp
              labels={labels}
              selectedReasonCode={selectedReasonCode}
              orderDetailsData={orderDetailsData}
              selectedOrderItems={selectedOrderItems}
              isLiveChatEnabled={isLiveChatEnabled}
              isInternationalShipping={isInternationalShipping}
              trackAnalyticsClick={trackAnalyticsClick}
              selectedReturnMethod={selectedReturnMethod}
              isInitiateReturnFailure={isInitiateReturnFailure}
            />
          </Col>
        </Row>
      </div>
    </>
  );
};
RequestNotCompleted.propTypes = {
  className: PropTypes.string,
  imageName: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  helpMessage: PropTypes.string.isRequired,
  caseId: PropTypes.string.isRequired,
  labels: PropTypes.shape({}),
  selectedReasonCode: PropTypes.shape({}),
  orderDetailsData: PropTypes.shape({}),
  refundUnderReview: PropTypes.bool,
  currencySymbol: PropTypes.string,
  refundTotal: PropTypes.string,
  errorLabel: PropTypes.string,
  selectedOrderItems: PropTypes.shape({}),
  isLiveChatEnabled: PropTypes.bool,
  trackAnalyticsClick: PropTypes.func,
  selectedReturnMethod: PropTypes.string,
  isInitiateReturnFailure: PropTypes.string,
};

RequestNotCompleted.defaultProps = {
  className: '',
  labels: {},
  orderDetailsData: {},
  selectedReasonCode: {},
  refundUnderReview: false,
  currencySymbol: '',
  refundTotal: '',
  errorLabel: '',
  selectedOrderItems: {},
  isLiveChatEnabled: false,
  trackAnalyticsClick: () => {},
  selectedReturnMethod: '',
  isInitiateReturnFailure: '',
};

export default RequestNotCompleted;
