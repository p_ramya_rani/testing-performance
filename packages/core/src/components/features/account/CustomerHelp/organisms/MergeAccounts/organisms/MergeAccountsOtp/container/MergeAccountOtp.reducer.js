import { fromJS } from 'immutable';
import { MERGE_ACCOUNT_OTP_CONSTANTS } from '../MergeAccountsOtp.constants';

const initialState = fromJS({
  otpDetail: null,
  getOtpSuccess: null,
  error: null,
  uniqueKey: null,
});

const mergeAccountOtpReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case MERGE_ACCOUNT_OTP_CONSTANTS.MERGE_ACCOUNT_OTP_DETAIL:
      return state.set('otpDetail', payload);
    case MERGE_ACCOUNT_OTP_CONSTANTS.MERGE_ACCOUNT_GET_OTP_SUCCESS: {
      return state.set('getOtpSuccess', payload);
    }
    case MERGE_ACCOUNT_OTP_CONSTANTS.MERGE_ACCOUNT_GET_OTP_FAILURE: {
      return state.set('error', payload);
    }
    case MERGE_ACCOUNT_OTP_CONSTANTS.MERGE_ACCOUNT_SET_UNIQUE_KEY: {
      return state.set('uniqueKey', payload).set('error', null);
    }
    case MERGE_ACCOUNT_OTP_CONSTANTS.MERGE_ACCOUNT_CLEAR_OTP_DATA: {
      return state
        .set('uniqueKey', null)
        .set('error', null)
        .set('otpDetail', null)
        .set('getOtpSuccess', null);
    }
    default:
      if (state instanceof Object) {
        return fromJS(state);
      }
      return state;
  }
};

export default mergeAccountOtpReducer;
