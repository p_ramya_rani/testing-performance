// 9fbef606107a605d69c0edbcd8029e5d
import { MERGE_ACCOUNT_EMAIL_ADDRESS_CONFIRMATION_REDUCER_KEY } from '@tcp/core/src/constants/reducer.constants';
import { createSelector } from 'reselect';
import EMAIL_CONFIRMATION_CONSTANTS from '../EmailConfirmation.constants';

export const getLabels = (state) => state?.Labels?.global;

export const emailConfirmationRequestLoader = (state) => {
  return state[MERGE_ACCOUNT_EMAIL_ADDRESS_CONFIRMATION_REDUCER_KEY].get('isFetching');
};

export const getEmailConfirmationRequestLoader = createSelector(
  emailConfirmationRequestLoader,
  (isFetching) => isFetching
);

export const getEmailConfirmationFailure = (state) => {
  return state[MERGE_ACCOUNT_EMAIL_ADDRESS_CONFIRMATION_REDUCER_KEY].get('error');
};

export const getEmailConfirmationSuccess = (state) => {
  return state[MERGE_ACCOUNT_EMAIL_ADDRESS_CONFIRMATION_REDUCER_KEY].get(
    'emailConfirmationSuccess'
  );
};

export const getMergeAccountEmailConfirmationSuccess = createSelector(
  getEmailConfirmationSuccess,
  (getEmailConfirmationSuccessData) => getEmailConfirmationSuccessData
);

const errorMessage = (error, labels) => {
  if (error?.errorMessage === EMAIL_CONFIRMATION_CONSTANTS.EMAIL_ADDRESS_SAME) {
    return labels?.helpCenter?.lbl_merge_account_same_email_error_message
      ? labels.helpCenter.lbl_merge_account_same_email_error_message
      : labels.errorMessages.lbl_errorMessages_DEFAULT;
  }
  if (error?.errorCode === EMAIL_CONFIRMATION_CONSTANTS.ACCOUNT_HOLD) {
    return labels?.helpCenter?.lbl_merge_account_account_locked_error_message
      ? labels.helpCenter.lbl_merge_account_account_locked_error_message
      : labels.errorMessages.lbl_errorMessages_DEFAULT;
  }
  return error?.errorMessage
    ? labels?.helpCenter['lbl_merge_account_emailverification_error-message']
    : labels?.errorMessages.lbl_errorMessages_DEFAULT;
};

export const getMergeAccountEmailConfirmationError = createSelector(
  [getEmailConfirmationFailure, getLabels],
  (error, labels) => (error ? errorMessage(error, labels) : null)
);
