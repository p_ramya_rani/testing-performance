import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { reduxForm, Field, change as reduxFormChange, untouch } from 'redux-form';
import { BodyCopy, Button, TextBox } from '../../../../../../../../common/atoms';
import withStyles from '../../../../../../../../common/hoc/withStyles';
import styles from '../styles/MergeAccountOtp.style';
import ResetPasswordPinSection from '../../../../../../ResetPassword/molecules/ResetPasswordPinSection';
import { getLabelValue } from '../../../../../../../../../utils';
import Recaptcha from '../../../../../../../../common/molecules/recaptcha/recaptcha';
import {
  MERGE_ACCOUNT_ANALYTICS,
  MERGE_ACCOUNT_OTP_ERROR_CONSTANTS,
} from '../MergeAccountsOtp.constants';
import createValidateMethod from '../../../../../../../../../utils/formValidation/createValidateMethod';
import getStandardConfig from '../../../../../../../../../utils/formValidation/validatorStandardConfig';

const renderCaptcha = (change) => {
  return (
    <BodyCopy component="div">
      <Recaptcha
        verifyCallback={(token) => change('recaptchaToken', token)}
        expiredCallback={() => change('recaptchaToken', '')}
      />
      <Field
        id="recaptchaToken"
        component={TextBox}
        type="hidden"
        name="recaptchaToken"
        data-locator="merge-account-confirmation-recaptchca"
      />
    </BodyCopy>
  );
};

const isBtnDisabled = (otpLength, mergeAccountButtonLoader, invalid) => {
  if (otpLength !== 6 || mergeAccountButtonLoader || invalid) {
    return true;
  }
  return false;
};

const onBtnClicked = (handleOtpSubmission, otpDetails, trackAnalyticsClick) => {
  const { MERGE_ACCOUNT_TITLE, OTP_FLOW } = MERGE_ACCOUNT_ANALYTICS;
  trackAnalyticsClick({
    orderHelp_path_cd128: `${MERGE_ACCOUNT_TITLE}|${OTP_FLOW}`,
  });
  handleOtpSubmission(otpDetails);
};

const showNewCodeSentMessage = (newCodeSent, labels) => {
  return (
    <>
      {newCodeSent && (
        <BodyCopy fontSize="fs12" fontWeight="semibold" color="green.500" className="elem-mb-XL">
          {getLabelValue(labels, 'lbl_merge_new_code_sent')}
        </BodyCopy>
      )}
    </>
  );
};

const showInvalidOTPMessage = (labels) => {
  return (
    <BodyCopy component="p" fontSize="fs12" fontWeight="semibold" className="error-text elem-mb-XL">
      {getLabelValue(labels, 'lbl_merge__TCP_RESET_PASSWORD_CODE_INVALID')}
    </BodyCopy>
  );
};

const MergeAccountOtp = ({
  className,
  labels,
  handleOtpSubmission,
  mergeAccountButtonLoader,
  otpDetail,
  resendMessageThreshold,
  change,
  setShowOtp,
  getOtpOnEmail,
  survivorEmailAddress,
  victimEmailAddress,
  otpUniqueKey,
  globalLabels,
  clearOtpDataFields,
  firstTimeOtp,
  showRecaptcha,
  invalid,
  setFirstTimeOtp,
  trackAnalyticsClick,
}) => {
  const [passwordValidationCodeValid, setPasswordValidationCodeValid] = useState('na');

  const [, setPasswordValidation] = useState('');

  const [newCodeSent, setNewCodeSent] = useState(false);
  const [otpLength, setOtpLength] = useState(0);

  const [otpValue, setOtpValue] = useState('');

  const otpDetails = {
    uniqueKey: otpUniqueKey,
    otp: otpValue,
    survivorEmailAddress,
    victimEmailAddress,
  };

  const getOtpError = () => {
    let otpError = {};
    if (otpDetail?.errorCode) {
      otpError = {
        size: 1,
      };
    }
    return otpError;
  };

  useEffect(() => {
    if (!firstTimeOtp) getOtpOnEmail(victimEmailAddress);
    if (firstTimeOtp) setFirstTimeOtp(false);
    return () => {
      clearOtpDataFields();
    };
  }, []);

  useEffect(() => {
    if (otpDetail?.errorCode) {
      getOtpError();
    }
  }, [otpDetail]);

  const setPasswordValidationCodeStatus = (value) => {
    setOtpLength(value.length);
    if (value && value.length === 6) {
      setOtpValue(value);
      setPasswordValidationCodeValid(true);
    }
  };

  const setpasswordValidationCode = (value) => {
    setPasswordValidation(value);
    setPasswordValidationCodeStatus(value);
  };

  const alternateFlow = (toggleOtp) => {
    toggleOtp(false);
  };

  const resendCode = () => {
    getOtpOnEmail(victimEmailAddress);
    setNewCodeSent(true);
    const timer = setTimeout(() => {
      setNewCodeSent(false);
      clearTimeout(timer);
    }, resendMessageThreshold);
  };

  return (
    <>
      <BodyCopy component="div" className={`${className} otp-container`}>
        <BodyCopy
          component="div"
          className="verify-otp-text"
          fontSize="fs16"
          fontFamily="secondary"
          fontWeight="bold"
        >
          {getLabelValue(labels, 'lbl_merge_one_time_passcode')}
        </BodyCopy>
        <BodyCopy
          component="div"
          className="email"
          fontWeight="medium"
          fontSize="fs14"
          fontFamily="secondary"
        >
          {getLabelValue(labels, 'lbl_merge_passcode_sent_text')}
        </BodyCopy>
        <ResetPasswordPinSection
          setpasswordValidationCode={setpasswordValidationCode}
          passwordValidationCodeValid={passwordValidationCodeValid}
          labels={globalLabels?.password}
          errorMessage={getOtpError()}
          className="reset-password-container"
        />
        {(otpDetail?.errorCode === MERGE_ACCOUNT_OTP_ERROR_CONSTANTS.INVALID_OTP ||
          otpDetail?.errorCode === MERGE_ACCOUNT_OTP_ERROR_CONSTANTS.OTP_EXPIRED) &&
          showInvalidOTPMessage(labels)}
        {showNewCodeSentMessage(newCodeSent, labels)}
        {showRecaptcha && renderCaptcha(change)}
        <Button
          fill="BLUE"
          type="submit"
          buttonVariation="variable-width"
          className="merge-account-button"
          disabled={isBtnDisabled(otpLength, mergeAccountButtonLoader, invalid)}
          onClick={() => onBtnClicked(handleOtpSubmission, otpDetails, trackAnalyticsClick)}
          loading={mergeAccountButtonLoader}
        >
          {getLabelValue(labels, 'lbl_merge_account')}
        </Button>
        <BodyCopy component="div" className="resend-container  mt">
          <BodyCopy
            component="p"
            className="code-not-recieved"
            fontSize="fs12"
            fontFamily="secondary"
            fontWeight="semibold"
          >
            {getLabelValue(labels, 'lbl_merge_code_not_received')}
          </BodyCopy>
          <Button
            className="edit-option"
            nohover
            type="button"
            link
            underline
            color="blue"
            onClick={resendCode}
          >
            {getLabelValue(labels, 'lbl_merge_resend_otp')}
          </Button>
        </BodyCopy>
        <BodyCopy component="div" className="resend-container">
          <BodyCopy
            component="p"
            className="code-not-recieved"
            fontSize="fs12"
            fontFamily="secondary"
            fontWeight="semibold"
          >
            {getLabelValue(labels, 'lbl_merge_email_not_access')}
          </BodyCopy>
          <Button
            className="edit-option"
            nohover
            type="button"
            link
            underline
            color="blue"
            onClick={() => alternateFlow(setShowOtp)}
          >
            {getLabelValue(labels, 'lbl_merge_verify_from_account')}
          </Button>
        </BodyCopy>
      </BodyCopy>
    </>
  );
};

MergeAccountOtp.propTypes = {
  className: PropTypes.string,
  labels: PropTypes.shape({}).isRequired,
  globalLabels: PropTypes.shape({}).isRequired,
  handleOtpSubmission: PropTypes.func,
  mergeAccountButtonLoader: PropTypes.bool.isRequired,
  otpDetail: PropTypes.shape({}),
  resendMessageThreshold: PropTypes.number,
  setShowOtp: PropTypes.func,
  change: PropTypes.func,
  getOtpOnEmail: PropTypes.func,
  clearOtpDataFields: PropTypes.func,
  survivorEmailAddress: PropTypes.string,
  victimEmailAddress: PropTypes.string,
  otpUniqueKey: PropTypes.string,
};

MergeAccountOtp.defaultProps = {
  className: '',
  survivorEmailAddress: '',
  victimEmailAddress: '',
  otpUniqueKey: '',
  handleOtpSubmission: () => {},
  otpDetail: {},
  resendMessageThreshold: 3000,
  setShowOtp: () => {},
  getOtpOnEmail: () => {},
  change: () => {},
  clearOtpDataFields: () => {},
};
const validateMethod = createValidateMethod(getStandardConfig(['recaptchaToken']));
const afterSubmit = (result, dispatch) => {
  dispatch(reduxFormChange('MergeAccountOtp', 'recaptchaToken', ''));
  dispatch(untouch('MergeAccountOtp', 'recaptchaToken'));
};

export default reduxForm({
  form: 'MergeAccountOtp',
  enableReinitialize: true,
  onSubmitSuccess: afterSubmit,
  onSubmitFail: afterSubmit,
  ...validateMethod,
})(withStyles(MergeAccountOtp, styles));
export { MergeAccountOtp as MergeAccountOtpVanilla };
