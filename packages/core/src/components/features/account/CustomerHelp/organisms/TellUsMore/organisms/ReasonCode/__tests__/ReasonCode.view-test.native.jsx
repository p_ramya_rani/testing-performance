// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import ReasonCode from '../views';

describe('ReasonCodes Component', () => {
  const props = {
    reasonList: [{ label: test }],
    labels: {},
  };
  it('should render ReasonCode Component', () => {
    const component = shallow(<ReasonCode />);
    expect(component).toMatchSnapshot();
  });

  it('should render ReasonCode Component with Selected ReasonCode', () => {
    const selectedReasonCode = {
      label: '1001',
    };
    const updatedProps = { ...props, selectedReasonCode };
    const component = shallow(<ReasonCode {...updatedProps} />);
    expect(component).toMatchSnapshot();
  });
});
