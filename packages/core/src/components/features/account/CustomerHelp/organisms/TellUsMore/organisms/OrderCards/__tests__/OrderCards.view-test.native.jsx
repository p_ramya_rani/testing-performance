// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import OrderCards from '../views/OrderCards.view.native';

describe('Order Items component', () => {
  const props = {
    labels: {},
    setSelectedShipmentCard: () => {},
    clearSelectedResolution: () => {},
    selectedShipmentCard: '',
    orderDetailsData: {},
    orderLabels: {},
    shipmentData: {},
    isDelivered: false,
    handleShipmentCardClick: () => {},
    setIsDelivered: () => {},
  };
  it('should renders correctly', () => {
    const component = shallow(<OrderCards {...props} />);
    expect(component).toMatchSnapshot();
  });
});
