// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  .see-more-link {
    color: ${props => props.theme.colorPalette.blue[800]};
    display: flex;
    justify-content: center;
    margin-top: ${props => props.theme.spacing.ELEM_SPACING.LRG};
  }
`;

export default styles;
