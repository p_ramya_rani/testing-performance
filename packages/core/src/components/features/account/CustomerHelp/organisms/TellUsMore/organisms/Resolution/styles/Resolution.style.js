// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  .modify-order-area {
    background-color: ${props => props.theme.colors.PRIMARY.PALEGRAY};
    width: 100vw;
    position: relative;
    left: calc(-50vw + 50%);
    @media ${props => props.theme.mediaQuery.large} {
      left: calc(-45vw + 50%);
    }
  }

  .resolution-content-area {
    background-color: ${props => props.theme.colors.PRIMARY.PALEGRAY};
    width: 100vw;
    position: relative;
    left: calc(-50vw + 50%);
    padding: ${props => props.theme.spacing.ELEM_SPACING.XS} 0
      ${props => props.theme.spacing.ELEM_SPACING.XL} 0;
    @media ${props => props.theme.mediaQuery.large} {
      left: calc(-45vw + 50%);
    }
  }

  .resolution-container {
    position: relative;
    margin-left: ${props => props.theme.spacing.ELEM_SPACING.XS};
    @media ${props => props.theme.mediaQuery.large} {
      left: calc(-41vw + 50%);
      margin-left: 0;
    }
  }

  .info-icon-container,
  .icon-container-shipping {
    position: absolute;
    left: 10px;
  }

  .alignCards {
    margin-left: 12px;
    @media ${props => props.theme.mediaQuery.large} {
      margin-left: 0px;
    }
  }

  .icon-container-shipping {
    @media ${props => props.theme.mediaQuery.large} {
      left: calc(-50vw + 50%);
    }
  }

  .info-icon-container {
    margin-top: ${props => props.theme.spacing.ELEM_SPACING.LRG};
  }

  .selected-help-message {
    margin-top: -10px;
  }

  .align-left {
    justify-content: left;
  }

  .module-x-container {
    width: 90%;
    margin-left: 40px;
    margin-right: ${props => props.theme.spacing.ELEM_SPACING.XS};
  }
  .module-container {
    width: 90%;
    margin-left: ${props => props.theme.spacing.ELEM_SPACING.XXL};
  }
  .align-center {
    justify-content: center;
  }

  .full-width {
    width: 100%;
  }
  .something-else-message {
    width: 100%;
    height: 146px;
    resize: none;
    font-size: ${props => props.theme.fonts.fontSize.textbox}px;
    margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.XL};
    overflow-y: hidden;
    box-sizing: border-box;
    border: 2px solid ${props => props.theme.colorPalette.gray[500]};
    border-radius: 4px;
  }
  .continue-btn {
    width: 200px;
  }
  .help-button {
    width: 250px;
    height: 51px;
    flex-grow: 0;
    border: solid 1px ${props => props.theme.colorPalette.gray[600]};
    font-weight: ${props => props.theme.typography.fontWeights.extrabold};
    color: ${props => props.theme.colorPalette.gray[800]};
  }
  .resolution {
    min-width: 293.5px;
  }

  .select-option {
    @media ${props => props.theme.mediaQuery.mediumMax} {
      margin-top: 0;
      padding: 0;
    }
  }

  .header-title {
    @media ${props => props.theme.mediaQuery.large} {
      display: flex;
      align-items: baseline;
    }
  }

  .help-text-container {
    align-items: center;
    @media ${props => props.theme.mediaQuery.smallMax} {
      .help-text {
        width: 90%;
        font-size: 14px;
      }
    }
  }

  .review-button {
    display: flex;
    justify-content: center;
    padding: 15px;
    @media ${props => props.theme.mediaQuery.large} {
      margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.XS};
    }
  }

  .customer-something-else {
    .TextArea__input {
      position: relative;
      border-radius: 8px;
      height: 90px;
      padding: 15px;
      width: 95%;
      resize: none;
      color: ${props => props.theme.colorPalette.gray[800]};
      @media ${props => props.theme.mediaQuery.large} {
        width: 100%;
      }
      @media ${props => props.theme.mediaQuery.smallMax} {
        width: 93%;
      }
    }
  }

  .text-box-container {
    position: relative;
    .character-count,
    .TextArea__label {
      position: absolute;
      color: ${props => props.theme.colorPalette.gray[800]};
    }
    .character-count {
      right: 30px;
      top: 100px;
      @media ${props => props.theme.mediaQuery.large} {
        right: -20px;
      }
    }
    .TextArea__label {
      font-size: 14px;
      left: 15px;
      top: 15px;
    }
  }

  .help-with-another-btn {
    @media ${props => props.theme.mediaQuery.large} {
      width: 75%;
    }
  }

  .footer-button {
    padding: ${props => props.theme.spacing.ELEM_SPACING.MED}
      ${props => props.theme.spacing.ELEM_SPACING.XS};
  }

  .module-container-transit {
    width: 90%;
    margin-left: ${props => props.theme.spacing.ELEM_SPACING.SM};
  }
`;

export default styles;
