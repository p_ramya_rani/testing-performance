// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import Confirmation from '../views';

describe('Confirmation Template', () => {
  const props = {
    orderDetailsData: {
      orderNumber: '123123',
    },
  };
  it('should render Confirmation Template', () => {
    const component = shallow(<Confirmation {...props} />);
    expect(component).toMatchSnapshot();
  });
});
