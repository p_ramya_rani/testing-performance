// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';
import { ViewWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';

const FullBleedView = styled(ViewWithSpacing)`
  width: 100%;
  margin-right: 0;
  margin-left: 0;
  ${props => (props.centered ? `justify-content: center;` : ``)}
`;

export default FullBleedView;
