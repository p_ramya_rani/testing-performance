// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from '@tcp/core/src/components/common/atoms';
import { getLabelValue, getShipmentCount } from '@tcp/core/src/utils/utils';
import OrderItem from '../../OrderItem';
import OrderItemHeader from '../../OrderItemHeader';
import { ORDER_ITEM_STATUS } from '../../../../../CustomerHelp.constants';

const getOrderDisplayStatus = (orderStatus, orderLabels) => {
  const {
    IN_TRANSIT,
    IN_PROGRESS,
    SHIPMENT_API_DELIVERED,
    RETURN_INITIATED,
    ORDER_BEING_PROCESSED,
    CANCELED,
  } = ORDER_ITEM_STATUS;
  switch (orderStatus) {
    case IN_TRANSIT:
      return getLabelValue(orderLabels, 'lbl_orders_statusItemsInTransit', 'orders');
    case SHIPMENT_API_DELIVERED:
      return getLabelValue(orderLabels, 'lbl_orders_statusItemsDelivered', 'orders');
    case IN_PROGRESS:
    case ORDER_BEING_PROCESSED:
      return getLabelValue(orderLabels, 'lbl_orders_statusItemsInProgress', 'orders');
    case RETURN_INITIATED:
      return getLabelValue(orderLabels, 'lbl_orders_returns_initiated', 'orders');
    case CANCELED:
      return getLabelValue(orderLabels, 'lbl_orders_statusOrderCancelled', 'orders');
    default:
      return '';
  }
};

const OrderItems = ({
  orderDetailsData,
  orderLabels,
  setSelectedOrderItems,
  selectedOrderItems,
  clearSelectedOrderItems,
  selectedReasonCode,
  clearSelectedResolution,
}) => {
  const { summary, orderItems, estimatedDeliveryDate } = orderDetailsData || {};
  const { currencySymbol } = summary || {};
  const shipmentCount = getShipmentCount(orderItems);
  const trackingInfo = (orderGroup) =>
    orderGroup && orderGroup.items[0] && orderGroup.items[0].trackingInfo[0];
  return (
    <>
      {orderItems &&
        orderItems.length > 0 &&
        orderItems.map((orderGroup, index) => (
          <>
            {orderGroup.items && orderGroup.items.length > 0 && (
              <>
                <Row key={index.toString()} fullBleed className="elem-mb-SM">
                  <Row fullBleed>
                    <OrderItemHeader
                      shipmentNumber={index + 1}
                      ItemsCount={shipmentCount}
                      status={orderGroup.orderStatus}
                      trackingNumber={orderGroup.trackingNumber}
                      trackingUrl={orderGroup.trackingUrl}
                      orderLabels={orderLabels && orderLabels.orders}
                      orderStatus={orderGroup.orderStatus}
                      shippedDate={trackingInfo(orderGroup)?.shipDate}
                      shipmentStatus={trackingInfo(orderGroup)?.status}
                      estimatedDeliveryDate={estimatedDeliveryDate}
                      actualDeliveryDate={trackingInfo(orderGroup)?.actualDeliveryDate}
                      carrierDeliveryDate={trackingInfo(orderGroup)?.carrierDeliveryDate}
                      orderDisplayStatus={getOrderDisplayStatus(
                        orderGroup.orderStatus,
                        orderLabels
                      )}
                      textFonSize="fs14"
                    />
                  </Row>
                  <Row key={index.toString()} fullBleed>
                    {orderGroup.items.map(
                      (item, itemIndex) =>
                        item && (
                          <Col
                            className="elem-mt-LRG order-items-container"
                            key={itemIndex.toString()}
                            colSize={{ large: 4, medium: 4, small: 6 }}
                            ignoreGutter={(itemIndex + 1) % 3 === 0 ? { large: true } : {}}
                          >
                            <OrderItem
                              setSelectedOrderItems={setSelectedOrderItems}
                              selectedOrderItems={selectedOrderItems}
                              key={itemIndex.toString()}
                              {...{ item }}
                              currencySymbol={currencySymbol}
                              orderLabels={orderLabels && orderLabels.orders}
                              orderStatus={orderGroup.orderStatus}
                              clearSelectedOrderItems={clearSelectedOrderItems}
                              selectedReasonCode={selectedReasonCode}
                              clearSelectedResolution={clearSelectedResolution}
                            />
                          </Col>
                        )
                    )}
                  </Row>
                </Row>
              </>
            )}
          </>
        ))}
    </>
  );
};

OrderItems.propTypes = {
  orderDetailsData: PropTypes.shape({}).isRequired,
  selectedReasonCode: PropTypes.shape({}),
  labels: PropTypes.shape({}),
  orderLabels: PropTypes.shape({}),
  setSelectedOrderItems: PropTypes.shape({}),
  selectedOrderItems: PropTypes.shape({}),
  shipmentData: PropTypes.shape({}),
  clearSelectedOrderItems: PropTypes.func,
  clearSelectedResolution: PropTypes.func,
};

OrderItems.defaultProps = {
  orderLabels: {},
  setSelectedOrderItems: {},
  selectedOrderItems: {},
  selectedReasonCode: {},
  labels: {},
  shipmentData: {},
  clearSelectedOrderItems: () => {},
  clearSelectedResolution: () => {},
};

export default OrderItems;
