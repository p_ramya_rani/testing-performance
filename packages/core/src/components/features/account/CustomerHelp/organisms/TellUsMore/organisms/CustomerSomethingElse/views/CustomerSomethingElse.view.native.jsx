// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState, useEffect } from 'react';
import { View } from 'react-native';
import { BodyCopy } from '@tcp/core/src/components/common/atoms';
import PropTypes from 'prop-types';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import Button from '@tcp/core/src/components/common/atoms/Button';
import {
  StyledTextInput,
  CharacterLeft,
  CharacterLeftContainer,
  ErrorIcon,
  AdditionalDetailText,
} from '../../Resolution/styles/Resolution.style.native';
import {
  ViewWithSpacing,
  StyledErrorWrapper,
} from '../../../../../../../../common/atoms/styledWrapper/styledWrapper.native';

const errorIcon = require('../../../../../../../../../../../mobileapp/src/assets/images/alert-triangle.png');

const refToInputElement = React.createRef();
const inputElementKey = '0';

const retainComment = ({ wrongItemData, maxCharacter, setText, setCharacterLeft }) => {
  if (wrongItemData && Object.keys(wrongItemData).length && wrongItemData.comments) {
    const charLeft = (maxCharacter - Number(wrongItemData.comments.length)).toString();
    setText(wrongItemData.comments);
    setCharacterLeft(charLeft);
  }
  if (wrongItemData && !Object.keys(wrongItemData).length) {
    setText('');
    setCharacterLeft(maxCharacter.toString());
  }
};

const renderCustomTextArea = (isError, labels, onChangeHandler, text, characterLeft) => {
  return (
    <ViewWithSpacing spacingStyles="margin-bottom-LRG">
      <StyledTextInput
        value={text}
        inputRef={refToInputElement}
        key={inputElementKey}
        maxLength={200}
        enableSuccessCheck={false}
        showExplicitError={isError ? getLabelValue(labels, 'lbl_section_completed') : false}
        placeholder={getLabelValue(labels, 'lbl_placeholder_value_textArea')}
        roundTextBox
        onChangeText={(val) => onChangeHandler(val)}
        isCVVField={false}
        type="text"
        dataLocator="something-message"
        multiline
        textAlignVertical="top"
      />
      <CharacterLeftContainer>
        <CharacterLeft
          fontFamily="secondary"
          fontSize="fs12"
          text={`${characterLeft} ${getLabelValue(labels, 'lbl_characters_left')}`}
        />
      </CharacterLeftContainer>
      {isError && (
        <StyledErrorWrapper>
          <ViewWithSpacing spacingStyles="margin-right-XS">
            <ErrorIcon source={errorIcon} />
          </ViewWithSpacing>
          <BodyCopy
            fontFamily="secondary"
            fontWeight="extrabold"
            fontSize="fs12"
            text={getLabelValue(labels, 'lbl_section_completed')}
            color="error"
          />
        </StyledErrorWrapper>
      )}
    </ViewWithSpacing>
  );
};

const CustomerSomethingElse = ({
  labels,
  setWrongItemData,
  wrongItemData,
  isSomethingElse,
  smoothScrolling,
}) => {
  const [isError, setIsError] = useState(false);
  const [text, setText] = useState('');
  const maxCharacter = 200;
  const [characterLeft, setCharacterLeft] = useState(maxCharacter);

  useEffect(() => {
    const charLeft = (maxCharacter - Number(text.length)).toString();
    setCharacterLeft(charLeft);
  }, [text]);

  useEffect(() => {
    retainComment({ wrongItemData, maxCharacter, setText, setCharacterLeft });
  }, [wrongItemData]);

  const onContinueButton = () => {
    if (isSomethingElse) {
      if (text !== '') {
        setWrongItemData({ isWrongItemDataSet: true, someThingElse: 'true', comments: text });
        setIsError(false);
      } else {
        setIsError(true);
      }
    } else {
      setWrongItemData({ isWrongItemDataSet: true, someThingElse: 'false', comments: text });
      setIsError(false);
    }

    setTimeout(() => {
      smoothScrolling('step-6');
    }, 100);
  };

  const onChangeHandler = (val) => {
    setText(val);
    setIsError(false);
  };

  return (
    <ViewWithSpacing spacingStyles="margin-right-SM margin-left-SM margin-top-LRG">
      <View>
        <AdditionalDetailText>
          <BodyCopy
            text={getLabelValue(labels, 'lbl_additional_details')}
            fontWeight="bold"
            fontFamily="secondary"
            fontSize="fs16"
          />
          <ViewWithSpacing spacingStyles="margin-top-XXS margin-left-XXS">
            <BodyCopy
              text={`${
                isSomethingElse
                  ? getLabelValue(labels, 'lbl_required_text')
                  : getLabelValue(labels, 'lbl_optional_text')
              }`}
              fontFamily="secondary"
              fontSize="fs12"
            />
          </ViewWithSpacing>
        </AdditionalDetailText>
        {renderCustomTextArea(isError, labels, onChangeHandler, text, characterLeft)}
      </View>
      {!(wrongItemData && wrongItemData.isWrongItemDataSet) && (
        <ViewWithSpacing spacingStyles="margin-right-XXXL margin-left-XXXL margin-bottom-XXL">
          <Button
            buttonVariation="fixed-width"
            fill="BLUE"
            type="submit"
            onPress={onContinueButton}
            text={getLabelValue(labels, 'lbl_help_refund_action_btn')}
          />
        </ViewWithSpacing>
      )}
    </ViewWithSpacing>
  );
};

CustomerSomethingElse.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  setWrongItemData: PropTypes.func.isRequired,
  wrongItemData: PropTypes.shape({}).isRequired,
  isSomethingElse: PropTypes.bool,
  smoothScrolling: PropTypes.func,
};

CustomerSomethingElse.defaultProps = {
  isSomethingElse: false,
  smoothScrolling: () => {},
};

export default CustomerSomethingElse;
