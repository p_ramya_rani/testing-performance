// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import Separator from '@tcp/core/src/components/common/atoms/Separator';
import { getViewportInfo, isClient } from '@tcp/core/src/utils';
import styles from '../styles/CouponSelfHelp.style';
import MyOffersCoupons from '../../../../../../common/organism/MyOffersCoupons';
import MyRewards from '../../../../../../common/organism/MyRewards';
import CheckCouponContainer from '../../CheckCoupon';
import { BodyCopy, RichText } from '../../../../../../../../common/atoms';

const CouponSelfHelp = ({ isLoggedIn, className, labels, orderLabels, needHelpContentLabels }) => {
  const { isMobile } = (isClient() && getViewportInfo()) || {};
  return (
    <div className={`${className} coupon-self-help-container`}>
      <Separator />
      <CheckCouponContainer labels={{ ...orderLabels, ...labels }} />
      <Separator />
      {isLoggedIn && (
        <>
          <BodyCopy component="div" className="elem-mt-XXL">
            {isMobile ? (
              <MyOffersCoupons couponSelfHelp />
            ) : (
              <MyRewards labels={labels} view="all" couponSelfHelp />
            )}
          </BodyCopy>
          <Separator />
        </>
      )}
      {needHelpContentLabels.NEED_HELP_RICH_TEXT && (
        <>
          <RichText richTextHtml={needHelpContentLabels.NEED_HELP_RICH_TEXT} />
          <Separator />
        </>
      )}
    </div>
  );
};

CouponSelfHelp.propTypes = {
  className: PropTypes.string.isRequired,
  isLoggedIn: PropTypes.bool.isRequired,
  labels: PropTypes.shape({}).isRequired,
  orderLabels: PropTypes.shape({}).isRequired,
};

CouponSelfHelp.defaultProps = {};

export default withStyles(CouponSelfHelp, styles);
export { CouponSelfHelp as CouponSelfHelpVanilla };
