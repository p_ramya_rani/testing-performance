// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getEnableFullPageAuth } from '@tcp/web/src/components/features/content/HomePage/HomePage.selectors';
import EmailConfirmationComponent from '../views';
import { getFormValidationErrorMessages } from '../../../../Account/container/Account.selectors';
import {
  getEmailConfirmationRequestLoader,
  getMergeAccountEmailConfirmationError,
  getMergeAccountEmailConfirmationSuccess,
} from './EmailConfirmation.selector';
import {
  emailConfirmationRequest,
  resetEmailConfirmationRequest,
  emailConfirmationFailure,
} from './EmailConfirmation.action';
import { mergeAccountReset as mergeAccountResetAction } from '../../MergeAccounts/container/MergeAccounts.actions';
import { getProfileInfoTileData } from '../../../../User/container/User.selectors';
import EMAIL_CONFIRMATION_CONSTANTS from '../EmailConfirmation.constants';

const EmailConfirmation = ({
  onSubmit,
  labels,
  formErrorMessage,
  fullPage,
  emailConfirmationLoader,
  emailConfirmationError,
  emailConfirmationSuccess,
  resetEmailConfirmation,
  setShowOtp,
  mergeAccountReset,
  profileInfo,
}) => {
  const checkEmailAddressAndOnSubmit = (values) => {
    onSubmit(values, profileInfo);
  };
  return (
    <EmailConfirmationComponent
      onSubmit={checkEmailAddressAndOnSubmit}
      labels={labels}
      formErrorMessage={formErrorMessage}
      fullPage={fullPage}
      emailConfirmationLoader={emailConfirmationLoader}
      emailConfirmationError={emailConfirmationError}
      emailConfirmationSuccess={emailConfirmationSuccess}
      resetEmailConfirmation={resetEmailConfirmation}
      setShowOtp={setShowOtp}
      mergeAccountReset={mergeAccountReset}
    />
  );
};

EmailConfirmation.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  onSubmit: PropTypes.func.isRequired,
  formErrorMessage: PropTypes.shape({}).isRequired,
  fullPage: PropTypes.bool,
  emailConfirmationLoader: PropTypes.bool,
  emailConfirmationError: PropTypes.string,
  emailConfirmationSuccess: PropTypes.shape({}),
  resetEmailConfirmation: PropTypes.func.isRequired,
  setShowOtp: PropTypes.func.isRequired,
  mergeAccountReset: PropTypes.func.isRequired,
  profileInfo: PropTypes.shape({}),
};

EmailConfirmation.defaultProps = {
  fullPage: false,
  emailConfirmationLoader: false,
  emailConfirmationError: '',
  emailConfirmationSuccess: {},
  profileInfo: {},
};

const mapStateToProps = (state) => {
  return {
    formErrorMessage: getFormValidationErrorMessages(state),
    fullPage: getEnableFullPageAuth(state),
    emailConfirmationLoader: getEmailConfirmationRequestLoader(state),
    emailConfirmationError: getMergeAccountEmailConfirmationError(state),
    emailConfirmationSuccess: getMergeAccountEmailConfirmationSuccess(state),
    profileInfo: getProfileInfoTileData(state),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onSubmit: (payload, profileInfo) => {
      const { emailAddress: profileEmailAddress } = profileInfo;
      const { emailAddress } = payload;
      if (profileEmailAddress.toLowerCase() === emailAddress.toLowerCase()) {
        const error = {
          errorMessage: EMAIL_CONFIRMATION_CONSTANTS.EMAIL_ADDRESS_SAME,
        };
        dispatch(emailConfirmationFailure(error));
      } else {
        dispatch(emailConfirmationRequest(payload));
      }
    },
    resetEmailConfirmation: () => {
      dispatch(resetEmailConfirmationRequest());
    },
    mergeAccountReset: () => dispatch(mergeAccountResetAction()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EmailConfirmation);
