import { css } from 'styled-components';

const styles = css`
  &.return-item-container {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    }
  }

  .return-item-heading {
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  }

  .heading-line {
    color: ${(props) => props.theme.colorPalette.blue[800]};
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS_6};

    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    }
  }
  .return-policy {
    text-decoration: underline;
  }

  .return-item-desc {
    margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  }

  .return-method-container {
    display: flex;

    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      flex-direction: column;
    }
  }
  .btn-container {
    display: flex;
    align-items: center;
    justify-content: center;
  }

  .top-heading-container {
    display: flex;
    justify-content: space-between;
    align-items: baseline;
  }
  .selected-item-container {
    max-width: 330px;
  }
`;

export default styles;
