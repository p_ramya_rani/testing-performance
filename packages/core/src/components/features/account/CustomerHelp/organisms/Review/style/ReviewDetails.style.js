// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

export default css`
  .elem-margin-right {
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-right: 0;
    }
  }
  .order-status-header {
    @media ${(props) => props.theme.mediaQuery.large} {
      padding-top: ${(props) => props.theme.spacing.LAYOUT_SPACING.XXS};
    }
  }
  .row-background {
    background-color: ${(props) => props.theme.colorPalette.white};
    border: 1px solid white;
    padding: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  }
  .review-table-background {
    background-color: ${(props) => props.theme.colorPalette.gray[300]};
    padding: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      padding: ${(props) => props.theme.spacing.ELEM_SPACING.MED} 0;
    }
  }
  .row-margin {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  }
  .review-basic-details {
    margin-top: 20px;
  }
  .bottom-margin {
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  }
  .review-button {
    display: flex;
    justify-content: center;
    padding: 15px;
    @media ${(props) => props.theme.mediaQuery.mediumMax} {
      display: flex;
      bottom: 0px;
      left: 0px;
      padding: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
      position: fixed;
      width: 100%;
      z-index: 98;
      background-color: ${(props) => props.theme.colors.WHITE};
      margin: 0px;
      box-shadow: ${(props) => props.theme.colorPalette.gray[600]} 0px 0px 4px 0px;
    }
  }
  .review-submit-align {
    @media ${(props) => props.theme.mediaQuery.large} {
      padding: 5px;
    }
  }

  .group-row {
    border-bottom: solid 1px ${(props) => props.theme.colorPalette.gray[500]};
    padding-bottom: ${(props) => props.theme.spacing.LAYOUT_SPACING.SM};
    margin-bottom: ${(props) => props.theme.spacing.LAYOUT_SPACING.XS};
    @media ${(props) => props.theme.mediaQuery.medium} {
      padding-bottom: ${(props) => props.theme.spacing.LAYOUT_SPACING.MED};
      margin-bottom: ${(props) => props.theme.spacing.LAYOUT_SPACING.XXS};
      padding-top: ${(props) => props.theme.spacing.LAYOUT_SPACING.XXS};
    }
  }
  .button-container {
    margin-top: ${(props) => props.theme.spacing.LAYOUT_SPACING.XS};
    text-align: left;
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-top: 0px;
      text-align: right;
    }
  }
  .button-track {
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      width: 40%;
    }
  }
  .title-underline {
    border-bottom: 2px solid ${(props) => props.theme.colorPalette.blue[500]};
    width: 40px;
    margin-bottom: 20px;
  }
  .underline-bottom-margin {
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  }
  .orderDetail-trackingNumber {
    display: block;
    margin-top: ${(props) => props.theme.spacing.APP_LAYOUT_SPACING.XXS};
    @media ${(props) => props.theme.mediaQuery.medium} {
      display: inline;
      margin-top: 0;
    }
  }
  .orderDetail-trackingNumber-pipe {
    display: none;
    @media ${(props) => props.theme.mediaQuery.medium} {
      display: inline;
    }
  }
  .order-return-details {
    display: block;
  }
  .order-Item {
    padding-top: ${(props) => props.theme.spacing.LAYOUT_SPACING.SM};
  }
  .margin-tablet {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-top: 0;
    }
  }
  .margin-mobile {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    @media ${(props) => props.theme.mediaQuery.medium} {
      margin-top: 0;
    }
  }

  .back-from-receipt {
    font-family: ${(props) => props.theme.fonts.secondaryFontFamily};
    color: rgb(46, 106, 145);
    font-size: 16px;
    margin-bottom: 24px;
    border: none;
    background: none;
    cursor: pointer;
    outline: none;
    min-height: auto;
    letter-spacing: normal;
    text-transform: none;
  }
  .back-from-receipt .left-arrow {
    border: solid ${(props) => props.theme.colors.ANCHOR.SECONDARY};
    border-width: 0 2px 2px 0;
    display: inline-block;
    padding: 5px;
    transform: rotate(135deg);
  }

  .success-text {
    color: ${(props) => props.theme.colorPalette.green[500]};
  }
  .refund-review-text {
    color: #f79e1b;
  }
  .refund-failed-text {
    color: ${(props) => props.theme.colorPalette.red[800]};
  }
  .refund-successful {
    display: flex;
    justify-content: center;
    align-items: center;
    @media ${(props) => props.theme.mediaQuery.large} {
      width: ${(props) => props.theme.spacing.MODAL_WIDTH.MEDIUM};
      margin: auto auto ${(props) => props.theme.spacing.ELEM_SPACING.XS};
    }
  }
  .button-redius-set {
    border-radius: 8px;
  }
  .card-details {
    display: flex;
    align-items: center;
    margin: ${(props) => props.theme.spacing.ELEM_SPACING.SM} 0;
  }
  .card-border {
    border: 1px solid ${(props) => props.theme.colorPalette.gray[500]};
    object-fit: contain;
    border-radius: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
    width: ${(props) => props.theme.spacing.ELEM_SPACING.XXXL};
  }

  .confirmation-message {
    display: block;
    width: 100%;
    .confirmation-labels {
      display: inline;
    }
  }

  .review-footer {
    width: 200px;
    height: 51px;
  }

  .back-btn {
    @media ${(props) => props.theme.mediaQuery.mediumMax} {
      display: none;
    }
  }

  .inner-summary {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    }
  }

  .items-and-payment {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      display: flex;
      flex-direction: column-reverse;
    }
  }

  .review-details {
    @media ${(props) => props.theme.mediaQuery.large} {
      padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    }
  }

  .item-details {
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-top: 0;
    }
  }

  .review-details-text {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      font-size: ${(props) => props.theme.typography.fontSizes.fs14};
    }
  }

  .payment-details-area {
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      margin-top: 0;
    }
  }

  .return-method-container {
    display: flex;
  }

  .return-method-heading-container {
    display: flex;
    justify-content: space-between;
  }

  .return-method-text-container {
    display: flex;
    flex-direction: column;
  }

  .footer-button {
    padding: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  }

  .find-store-cta {
    &:hover {
      color: ${(props) => props.theme.colors.BRAND.PRIMARY};
    }
  }

  .print-receipts-btn {
    display: block;
    color: ${(props) => props.theme.colors.PRIMARY.BLUE};
    align-self: start;
    font-family: ${(props) => props.theme.typography.fonts.secondary};
    font-weight: ${(props) => props.theme.typography.fontWeights.bold};
    padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXXS};
    @media ${(props) => props.theme.mediaQuery.mediumMax} {
      font-size: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    }
  }

  .order-details-link {
    text-decoration: underline;
    color: ${(props) => props.theme.colors.PRIMARY.BLUE};
  }
`;
