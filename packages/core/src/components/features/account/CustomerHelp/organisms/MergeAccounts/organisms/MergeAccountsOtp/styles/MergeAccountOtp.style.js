import { css } from 'styled-components';

const styles = css`
  &.otp-container {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    }
  }

  .email {
    width: 70%;
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XS_6};
    margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.REG};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: 100%;
    }
  }

  .otp-box {
    @media ${(props) => props.theme.mediaQuery.small} {
      margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
    }
  }
  .merge-account-button {
    display: flex;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-left: auto;
      margin-right: auto;
    }
    margin-top: ${(props) =>
      props?.otpDetail?.isInvalidOtp
        ? props.theme.spacing.ELEM_SPACING.MED_1
        : props.theme.spacing.ELEM_SPACING.XL};
  }

  .resend-container {
    display: flex;

    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      justify-content: center;
    }
  }
  .mt {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
    padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  }
  .code-not-recieved {
    @media ${(props) => props.theme.mediaQuery.small} {
      margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
    }
  }

  .enter-code-label {
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.REG};
  }
  @media ${(props) => props.theme.mediaQuery.smallOnly} {
    .enter-code-label {
      display: none;
    }
  }

  .reset-password-container {
    width: auto;
  }

  .error-text {
    color: ${(props) => props.theme.colors.NOTIFICATION.ERROR};
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
    width: 320px;
  }
`;

export default styles;
