import React from 'react';
import PropTypes from 'prop-types';
import LoaderSkelton from '@tcp/core/src/components/common/molecules/LoaderSkelton/views/SkeletonLine.view.native';
import SkeletonContainer from '../styles/ReasonCodeSkeleton.style.native';

const TellUsMoreSkeleton = () => {
  return (
    <>
      <SkeletonContainer>
        <LoaderSkelton width="100%" height="58px" borderRadius="8px" />
      </SkeletonContainer>
      <SkeletonContainer>
        <LoaderSkelton width="100%" height="58px" borderRadius="8px" />
      </SkeletonContainer>
      <SkeletonContainer>
        <LoaderSkelton width="100%" height="58px" borderRadius="8px" />
      </SkeletonContainer>
      <SkeletonContainer>
        <LoaderSkelton width="100%" height="58px" borderRadius="8px" />
      </SkeletonContainer>
      <SkeletonContainer>
        <LoaderSkelton width="100%" height="58px" borderRadius="8px" />
      </SkeletonContainer>
      <SkeletonContainer>
        <LoaderSkelton width="100%" height="58px" borderRadius="8px" />
      </SkeletonContainer>
      <SkeletonContainer>
        <LoaderSkelton width="100%" height="58px" borderRadius="8px" />
      </SkeletonContainer>
      <SkeletonContainer>
        <LoaderSkelton width="100%" height="58px" borderRadius="8px" />
      </SkeletonContainer>
      <SkeletonContainer>
        <LoaderSkelton width="100%" height="58px" borderRadius="8px" />
      </SkeletonContainer>
    </>
  );
};

TellUsMoreSkeleton.propTypes = {
  orderDetailsData: PropTypes.shape({}).isRequired,
  selectedReasonCode: PropTypes.shape({}).isRequired,
  labels: PropTypes.shape({}).isRequired,
};

TellUsMoreSkeleton.defaultProps = {};

export default TellUsMoreSkeleton;
