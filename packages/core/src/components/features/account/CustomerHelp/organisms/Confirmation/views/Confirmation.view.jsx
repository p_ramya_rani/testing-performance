/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Button, BodyCopy } from '@tcp/core/src/components/common/atoms';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { routerPush, smoothScrolling } from '@tcp/core/src/utils';
import { getLabelValue, getSiteId } from '@tcp/core/src/utils/utils';
import { getCalculatedRefund, routeToPage } from '../../../util/utility';
import ReviewBasicDetails from '../../Review/organisms/ReviewBasicDetails';
import RequestNotCompleted from '../../Review/organisms/RequestNotCompleted';
import styles from '../../Review/style/ReviewDetails.style';
import ReviewItemsList from '../../Review/organisms/ReviewItemList';
import RefundSummaryDetails from '../../Review/organisms/RefundSummary';
import PaymentDetails from '../../Review/organisms/PaymentDetails';
import CUSTOMER_HELP_CONSTANTS, {
  REFUND_UNDER_REVIEW_LABELS_MAPPING,
  REASON_CODES,
  REFUND_UNDER_REVIEW,
  GC_IN_ORDER,
  CUSTOMER_HELP_ROUTES,
} from '../../../CustomerHelp.constants';
import {
  getReturnItemsDescription,
  renderInstructionsModuleX,
  renderPaymentDetailView,
  renderReturnItemsHeading,
  renderReturnMethod,
} from '../../Review/views/Review.view';
import SpinnerOverlay from '../../../../../../common/atoms/SpinnerOverlay';
import { routerReplace } from '../../../../../../../utils/utils.web';

const getErrorScreenTitle = (action, labels) => {
  switch (action) {
    case REASON_CODES.CANCEL_MODIFY:
      return getLabelValue(labels, 'lbl_unable_cancel');
    case REASON_CODES.INITIATE_RETURN:
      return getLabelValue(labels, 'lbl_returns_error_title');
    default:
      return getLabelValue(labels, 'lbl_help_refund_unsuccessful_title');
  }
};

const getErrorScreenHelpMessage = (action, labels) => {
  switch (action) {
    case REASON_CODES.CANCEL_MODIFY:
      return getLabelValue(labels, 'lbl_cancel_help');
    case REASON_CODES.INITIATE_RETURN:
      return getLabelValue(labels, 'lbl_returns_error_message');
    default:
      return getLabelValue(labels, 'lbl_help_refund_unsuccessful_message');
  }
};

const navigateToOrderHelp = (action, resetInitiateReturnStatus, setReturnItemDisplay) => {
  resetInitiateReturnStatus();
  setReturnItemDisplay(false);
  if (action === REASON_CODES.INITIATE_RETURN)
    return routerReplace(
      CUSTOMER_HELP_ROUTES.LANDING_PAGE.to,
      CUSTOMER_HELP_ROUTES.LANDING_PAGE.asPath
    );
  return routeToPage(CUSTOMER_HELP_ROUTES.LANDING_PAGE);
};

const renderNonRefundCompleteScreen = (inputParams) => {
  const {
    itemLevelReturns,
    labels,
    caseId,
    errorLabel,
    refundTotal,
    currencySymbol,
    action,
    shipmentDelayedStatusData,
    selectedReasonCode,
    orderDetailsData,
    selectedOrderItems,
    isLiveChatEnabled,
    trackAnalyticsClick,
    selectedReturnMethod,
    isInitiateReturnFailure,
  } = inputParams;
  if (
    (itemLevelReturns && itemLevelReturns.errorCode === REFUND_UNDER_REVIEW) ||
    (shipmentDelayedStatusData && shipmentDelayedStatusData.errorCode === REFUND_UNDER_REVIEW)
  ) {
    const helpMessage = errorLabel
      ? REFUND_UNDER_REVIEW_LABELS_MAPPING[errorLabel]
      : 'lbl_help_refund_under_review_message';
    return (
      <RequestNotCompleted
        imageName="triangle-warning"
        title={getLabelValue(labels, 'lbl_help_refund_under_review_title')}
        helpMessage={getLabelValue(labels, helpMessage)}
        caseId={caseId}
        labels={labels}
        refundUnderReview
        errorLabel={errorLabel}
        refundTotal={refundTotal}
        currencySymbol={currencySymbol}
        selectedReasonCode={selectedReasonCode}
        orderDetailsData={orderDetailsData}
        selectedOrderItems={selectedOrderItems}
        isLiveChatEnabled={isLiveChatEnabled}
        trackAnalyticsClick={trackAnalyticsClick}
        selectedReturnMethod={selectedReturnMethod}
        isInitiateReturnFailure={isInitiateReturnFailure}
      />
    );
  }

  const title = getErrorScreenTitle(action, labels);
  const helpMessage = getErrorScreenHelpMessage(action, labels);

  return (
    <RequestNotCompleted
      imageName="triangle-alert"
      title={title}
      helpMessage={helpMessage}
      caseId={caseId}
      labels={labels}
      selectedReasonCode={selectedReasonCode}
      orderDetailsData={orderDetailsData}
      selectedOrderItems={selectedOrderItems}
      isLiveChatEnabled={isLiveChatEnabled}
      trackAnalyticsClick={trackAnalyticsClick}
      selectedReturnMethod={selectedReturnMethod}
      isInitiateReturnFailure={isInitiateReturnFailure}
    />
  );
};

const getSuccessFullHeadingText = (action, labels, errorLabel) => {
  let heading = `${getLabelValue(labels, 'lbl_review_sucess')}`;
  if (action === REASON_CODES.CANCEL_MODIFY) {
    heading = `${getLabelValue(labels, 'lbl_order_cancelled')}`;
  } else if (errorLabel === GC_IN_ORDER) {
    heading = getLabelValue(labels, 'lbl_help_refund_under_review_title');
  }

  return heading;
};

const getSuccessConfirmationText = (action, labels, emailToDisplay, errorLabel) => {
  let msg = `${getLabelValue(labels, 'lbl_cancel_confirm_one')} ${emailToDisplay}`;
  if (action === REASON_CODES.CANCEL_MODIFY) {
    msg = `${getLabelValue(labels, 'lbl_confirmation_mail_sent_to')} ${emailToDisplay}`;
  } else if (action === REASON_CODES.INITIATE_RETURN) {
    msg = `${getLabelValue(labels, 'lbl_returns_mail_sent_to')} ${emailToDisplay}.`;
  } else if (errorLabel === GC_IN_ORDER) {
    msg = getLabelValue(
      labels,
      errorLabel
        ? REFUND_UNDER_REVIEW_LABELS_MAPPING[errorLabel]
        : 'lbl_help_refund_under_review_message'
    );
  }
  return msg;
};

const getCaseIDErrorLabelAndAmount = ({
  itemLevelReturns,
  action,
  cancelStatus = '',
  shipmentDelayedStatusData = {},
}) => {
  const { caseId: caseIdRefund, errorCode, status = '' } = itemLevelReturns;
  const {
    caseId: caseIdShippingRefund,
    errorCode: errorCodeShipping,
    status: shippingStatus = '',
    errorLabel: errorLabelShipping,
  } = (shipmentDelayedStatusData && shipmentDelayedStatusData.errorResponse) ||
  shipmentDelayedStatusData;
  const { PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME } = REASON_CODES;
  let errorLabelCode = '';
  if (errorCode === REFUND_UNDER_REVIEW || errorCodeShipping === REFUND_UNDER_REVIEW) {
    if (action === PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME) {
      errorLabelCode = errorLabelShipping;
    } else {
      const { errorLabel } = itemLevelReturns;
      errorLabelCode = errorLabel;
    }
  }

  let caseId = '';
  if (action === PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME) {
    caseId = caseIdShippingRefund;
  } else {
    caseId = caseIdRefund;
  }

  return {
    caseId,
    errorLabel: errorLabelCode,
    isRefundSuccessful: status.toLowerCase() === 'success',
    isCancelSuccessful: cancelStatus.toLowerCase() === 'success',
    isShipmentDelayedSuccessful: shippingStatus.toLowerCase() === 'success',
  };
};

const getOrderPaymentDetails = (action, labels, selectedReturnMethod) => {
  const reviewHref = `/${getSiteId()}${getLabelValue(
    labels,
    'lbl_authorization_hold_href',
    'orders'
  )}`;
  return action === REASON_CODES.CANCEL_MODIFY ? (
    <>
      <a className="blue-link" href={reviewHref} rel="noopener noreferrer" target="_blank">
        {getLabelValue(labels, 'lbl_auth_hold_policies')}
      </a>
      &nbsp;
      {getLabelValue(labels, 'lbl_auth_hold_policies1')}
    </>
  ) : (
    (selectedReturnMethod === CUSTOMER_HELP_CONSTANTS.RETURN_METHOD.IN_STORE &&
      getLabelValue(labels, 'lbl_payment_details_in_store_msg')) ||
      (selectedReturnMethod === CUSTOMER_HELP_CONSTANTS.RETURN_METHOD.BY_MAIL &&
        getLabelValue(labels, 'lbl_payment_details_msg'))
  );
};

const renderPaymentView = (orderDetailsData, orderLabels, labels, action, selectedReturnMethod) => {
  return (
    Object.keys(orderDetailsData).length > 0 && (
      <div className="row-background row-margin">
        <PaymentDetails
          labels={labels}
          orderDetailsData={orderDetailsData}
          ordersLabels={orderLabels && orderLabels.orders}
          orderStatus={getOrderPaymentDetails(action, labels, selectedReturnMethod)}
          showCardDetails
        />
      </div>
    )
  );
};

const getOrderStatus = (action, labels) => {
  return action === REASON_CODES.CANCEL_MODIFY
    ? getLabelValue(labels, 'lbl_cancelation_summary')
    : getLabelValue(labels, 'lbl_refund_summary');
};

const getOrderStatusCancelItems = (action, labels) => {
  switch (action) {
    case REASON_CODES.CANCEL_MODIFY:
      return getLabelValue(labels, 'lbl_canceled_items');
    case REASON_CODES.INITIATE_RETURN:
      return getLabelValue(labels, 'lbl_items_to_be_returned');
    default:
      return getLabelValue(labels, 'lbl_refunded_items');
  }
};

const getSuccessScenario = (
  isCancelSuccessful,
  isRefundSuccessful,
  isShipmentDelayedSuccessful,
  errorLabel,
  isInitiateReturnSuccessful
) => {
  if (
    isCancelSuccessful ||
    isRefundSuccessful ||
    isShipmentDelayedSuccessful ||
    isInitiateReturnSuccessful ||
    errorLabel === GC_IN_ORDER
  ) {
    return true;
  }
  return false;
};

const getEmailToDisplay = (isLoggedIn, loggedInUserEmail, orderDetailsData) =>
  isLoggedIn ? loggedInUserEmail : (orderDetailsData && orderDetailsData.emailAddress) || '';

const shouldRenderConfirmation = (orderDetailsData) =>
  orderDetailsData && Object.keys(orderDetailsData).length;

const triggerAnalytics = (
  isRefundSuccessful,
  isCancelSuccessful,
  refundTotal,
  trackAnalyticsPageView
) => {
  if (isRefundSuccessful || isCancelSuccessful) {
    trackAnalyticsPageView({ metric112: refundTotal, customEvents: ['event112'] });
  }
};

const getImageIcon = (errorLabel) =>
  errorLabel === GC_IN_ORDER ? 'triangle-warning' : 'circle-green-check';

const getReviewItemListComponent = ({
  orderItems,
  selectedOrderItems,
  router,
  action,
  currencySymbol,
  labels,
  orderLabels,
  returnedOrderItems,
}) => {
  if (action === REASON_CODES.INITIATE_RETURN && returnedOrderItems) {
    return (
      <div className="row-background row-margin">
        <BodyCopy
          fontSize="fs14"
          fontWeight="extrabold"
          fontFamily="secondary"
          className="elem-mb-SM"
          color="black"
        >
          {getOrderStatusCancelItems(action, labels)}
        </BodyCopy>
        <BodyCopy className="title-underline" />
        {Array.isArray(returnedOrderItems) &&
          returnedOrderItems.map((orderGroup, index) => (
            <ReviewItemsList
              key={index.toString()}
              orderLabels={orderLabels}
              items={orderGroup.items}
              currencySymbol={currencySymbol}
              orderStatus={getOrderStatusCancelItems(action, labels)}
              isEcol
              action={action}
              labels={labels}
              router={router}
              selectedOrderItems={selectedOrderItems}
            />
          ))}
      </div>
    );
  }
  if (action !== REASON_CODES.PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME) {
    return (
      <div className="row-background row-margin">
        <BodyCopy
          fontSize="fs14"
          fontWeight="extrabold"
          fontFamily="secondary"
          className="elem-mb-SM"
          color="black"
        >
          {getOrderStatusCancelItems(action, labels)}
        </BodyCopy>
        <BodyCopy className="title-underline" />
        {Array.isArray(orderItems) &&
          orderItems.map((orderGroup, index) => (
            <ReviewItemsList
              key={index.toString()}
              orderLabels={orderLabels}
              items={orderGroup.items}
              currencySymbol={currencySymbol}
              orderStatus={getOrderStatusCancelItems(action, labels)}
              isEcol
              action={action}
              labels={labels}
              selectedOrderItems={selectedOrderItems}
              router={router}
            />
          ))}
      </div>
    );
  }
  return null;
};

const handleBrowserBack = ({ isLoggedIn, selectOrderAction }) => {
  window.onpopstate = () => {
    if (isLoggedIn) {
      selectOrderAction();
    } else {
      routerPush('/', '/home');
    }
  };
};

const getRefundDataInfo = ({
  isPaidAndShippingCase,
  shipmentDelayedStatusData,
  itemLevelReturns,
}) => {
  if (isPaidAndShippingCase) {
    if (
      shipmentDelayedStatusData &&
      shipmentDelayedStatusData.errorResponse &&
      shipmentDelayedStatusData.errorResponse.errorLabel &&
      shipmentDelayedStatusData.errorResponse.errorLabel === GC_IN_ORDER
    ) {
      return shipmentDelayedStatusData.errorResponse;
    }
    return shipmentDelayedStatusData;
  }
  return itemLevelReturns;
};

const getReturnSuccessFullHeadingText = (labels, returnMethod) => {
  if (returnMethod === CUSTOMER_HELP_CONSTANTS.RETURN_METHOD.IN_STORE) {
    return getLabelValue(labels, 'lbl_returns_in_store_success_title');
  }
  if (returnMethod === CUSTOMER_HELP_CONSTANTS.RETURN_METHOD.BY_MAIL) {
    return getLabelValue(labels, 'lbl_returns_by_mail_success_title');
  }
  return null;
};

const renderReturnsSuccessScreen = (
  props,
  errorLabel,
  action,
  emailToDisplay,
  orderItems,
  currencySymbol
) => {
  const {
    labels,
    selectedReturnMethod,
    resolutionModuleXContent,
    returnInStoreModuleXContent,
    returnByMailModuleXContent,
    orderLabels,
    router,
    selectedOrderItems,
    orderDetailsData,
    isLoggedIn,
    orderReturnDays,
    userAccountId,
    returnedOrderItems,
  } = props;
  return (
    <Row fullBleed>
      <Col colSize={{ large: 8, medium: 4, small: 6 }}>
        <div className="row-background">
          <ReviewBasicDetails
            email={emailToDisplay}
            labels={labels}
            action={action}
            headingText={getReturnSuccessFullHeadingText(labels, selectedReturnMethod)}
            imageIcon={getImageIcon(errorLabel)}
            orderDetailsData={orderDetailsData}
            selectedReturnMethod={selectedReturnMethod}
            isLoggedIn={isLoggedIn}
          />
        </div>
        <BodyCopy component="div" className="row-background row-margin">
          {renderReturnItemsHeading(
            labels,
            selectedReturnMethod,
            orderDetailsData,
            orderLabels,
            isLoggedIn,
            orderReturnDays,
            userAccountId
          )}
          <BodyCopy className=" title-underline" />
          <BodyCopy
            component="div"
            fontSize="fs14"
            fontWeight="semibold"
            fontFamily="secondary"
            color="text.dark"
          >
            {getReturnItemsDescription(labels, selectedReturnMethod)}
          </BodyCopy>
          {renderInstructionsModuleX(
            true,
            labels,
            resolutionModuleXContent,
            selectedReturnMethod,
            returnInStoreModuleXContent,
            returnByMailModuleXContent
          )}
        </BodyCopy>
        {getReviewItemListComponent({
          orderItems,
          selectedOrderItems,
          router,
          action,
          currencySymbol,
          labels,
          orderLabels,
          returnedOrderItems,
        })}
      </Col>
      <Col colSize={{ large: 4, medium: 4, small: 6 }} className="margin-tablet margin-mobile">
        {renderReturnMethod(labels, selectedReturnMethod)}
        {renderPaymentDetailView(
          orderDetailsData,
          orderLabels,
          action,
          labels,
          selectedReturnMethod
        )}
      </Col>
    </Row>
  );
};

const trackClick = (isInitiateReturnSuccessful, trackAnalyticsClick, clickAnalyticsData) => {
  const clickPath = isInitiateReturnSuccessful
    ? `${clickAnalyticsData?.orderHelpSelectedReason} | Return Successfully Initiated`
    : `${clickAnalyticsData?.orderHelpSelectedReason} | Return Not Initiated`;

  if (!clickPath.includes(undefined)) {
    trackAnalyticsClick({
      orderHelp_path_cd128: clickPath,
    });
  }
};
const renderSpinner = (isFetching) => {
  return isFetching && <SpinnerOverlay />;
};

/**
 * This function component use for return the ReviewDetailsView
 * can be passed in the component.
 * @param orderDetailsData - orderDetailsData object used for showing Order Details
 */

// eslint-disable-next-line sonarjs/cognitive-complexity
const Confirmation = (props) => {
  const {
    orderDetailsData,
    className,
    orderLabels,
    labels,
    selectedOrderItems,
    selectOrderAction,
    itemLevelReturns,
    loggedInUserEmail,
    isLoggedIn,
    cancelStatusData,
    selectedReasonCode,
    shipmentDelayedStatusData,
    trackAnalyticsPageView,
    router,
    showLoader,
    hideLoader,
    getOrderSummary,
    isLiveChatEnabled,
    trackAnalyticsClick,
    isInitiateReturnSuccessful,
    resetInitiateReturnStatus,
    setReturnItemDisplay,
    clearReturnMethod,
    clickAnalyticsData,
    isFetching,
    selectedReturnMethod,
    isInitiateReturnFailure,
    clearReturnedOrderItems,
  } = props;
  const { summary = {}, orderItems } = orderDetailsData || {};
  const { status } = cancelStatusData || {};
  const { currencySymbol } = summary;
  const { shippingTotal } = getOrderSummary || {};
  const { action } = selectedReasonCode || {};
  const isPaidAndShippingCase = action === REASON_CODES.PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME;

  const {
    caseId,
    errorLabel,
    isRefundSuccessful,
    isCancelSuccessful,
    isShipmentDelayedSuccessful,
  } = getCaseIDErrorLabelAndAmount({
    itemLevelReturns,
    cancelStatus: status,
    action,
    shipmentDelayedStatusData,
  });
  const refundData = getRefundDataInfo({
    isPaidAndShippingCase,
    shipmentDelayedStatusData,
    itemLevelReturns,
  });
  const { totalBeforeTax, tax, itemsCount, refundTotal } = getCalculatedRefund(
    selectedOrderItems,
    orderItems,
    refundData,
    action,
    getOrderSummary
  );

  const emailToDisplay = getEmailToDisplay(isLoggedIn, loggedInUserEmail, orderDetailsData);

  useEffect(() => {
    handleBrowserBack({ isLoggedIn, selectOrderAction });
  }, []);

  useEffect(() => {
    if (isInitiateReturnFailure || isInitiateReturnSuccessful) {
      setTimeout(() => {
        smoothScrolling('customer-help-support');
      }, 100);
    }
  }, [isInitiateReturnFailure, isInitiateReturnSuccessful]);

  useEffect(() => {
    if (refundTotal && refundTotal > 0) {
      triggerAnalytics(isRefundSuccessful, isCancelSuccessful, refundTotal, trackAnalyticsPageView);
    }
  }, [refundTotal]);

  useEffect(() => {
    if (orderItems) {
      hideLoader();
    } else {
      showLoader();
    }
  }, [orderItems]);

  useEffect(() => {
    trackClick(isInitiateReturnSuccessful, trackAnalyticsClick, clickAnalyticsData);
  }, [isInitiateReturnSuccessful]);

  useEffect(() => {
    return () => {
      setReturnItemDisplay(false);
      clearReturnMethod();
      clearReturnedOrderItems();
    };
  }, []);

  const isCancelOrderPage = action === REASON_CODES.CANCEL_MODIFY;

  return (
    <>
      {renderSpinner(isFetching)}
      {!isFetching && shouldRenderConfirmation(orderDetailsData) && (
        <div className={className}>
          <>
            <div className="review-table-background">
              {getSuccessScenario(
                isCancelSuccessful,
                isRefundSuccessful,
                isShipmentDelayedSuccessful,
                errorLabel,
                isInitiateReturnSuccessful
              ) ? (
                <>
                  {action === REASON_CODES.INITIATE_RETURN &&
                    renderReturnsSuccessScreen(
                      props,
                      errorLabel,
                      action,
                      emailToDisplay,
                      orderItems,
                      currencySymbol
                    )}
                  {action !== REASON_CODES.INITIATE_RETURN && (
                    <Row fullBleed>
                      <Col colSize={{ large: 8, medium: 4, small: 6 }}>
                        <div className="row-background">
                          <ReviewBasicDetails
                            orderDetailsData={orderDetailsData}
                            email={emailToDisplay}
                            refundTotal={refundTotal}
                            labels={labels}
                            currencySymbol={currencySymbol}
                            caseId={caseId}
                            action={action}
                            headingText={getSuccessFullHeadingText(action, labels, errorLabel)}
                            confirmationText={getSuccessConfirmationText(
                              action,
                              labels,
                              emailToDisplay,
                              errorLabel
                            )}
                            imageIcon={getImageIcon(errorLabel)}
                            errorLabel={errorLabel}
                          />
                        </div>
                        {getReviewItemListComponent({
                          orderItems,
                          selectedOrderItems,
                          router,
                          action,
                          currencySymbol,
                          labels,
                          orderLabels,
                        })}
                      </Col>
                      <Col colSize={{ large: 4, medium: 4, small: 6 }}>
                        <div className="row-background">
                          <RefundSummaryDetails
                            orderDetailsData={orderDetailsData}
                            orderLabels={orderLabels && orderLabels.orders}
                            labels={labels}
                            totalBeforeTax={totalBeforeTax}
                            tax={tax}
                            refundTotal={refundTotal}
                            orderStatus={getOrderStatus(action, labels)}
                            itemsCount={itemsCount}
                            currencySymbol={currencySymbol}
                            isCancelOrderPage={isCancelOrderPage}
                            isPaidAndShippingCase={isPaidAndShippingCase}
                            shippingTotal={shippingTotal}
                          />
                        </div>
                        {renderPaymentView(
                          orderDetailsData,
                          orderLabels,
                          labels,
                          action,
                          selectedReturnMethod
                        )}
                      </Col>
                    </Row>
                  )}
                </>
              ) : (
                <div>
                  {renderNonRefundCompleteScreen({
                    itemLevelReturns,
                    labels,
                    caseId,
                    errorLabel,
                    refundTotal,
                    currencySymbol,
                    shipmentDelayedStatusData,
                    action,
                    selectedReasonCode,
                    orderDetailsData,
                    selectedOrderItems,
                    isLiveChatEnabled,
                    trackAnalyticsClick,
                    selectedReturnMethod,
                    isInitiateReturnFailure,
                  })}
                </div>
              )}
            </div>

            <div className={className}>
              <Row fullBleed>
                <Col colSize={{ large: 12, medium: 8, small: 6 }}>
                  <div className="review-button">
                    <div className="review-submit-align">
                      <Button
                        className="footer-button "
                        fontSize="fs14"
                        fontWeight="extrabold"
                        buttonVariation="variable-width"
                        fill="WHITE"
                        onClick={() =>
                          navigateToOrderHelp(
                            action,
                            resetInitiateReturnStatus,
                            setReturnItemDisplay
                          )
                        }
                      >
                        {getLabelValue(labels, 'lbl_merge_something_else_btn')}
                      </Button>
                    </div>
                  </div>
                </Col>
              </Row>
            </div>
          </>
        </div>
      )}
    </>
  );
};

Confirmation.propTypes = {
  className: PropTypes.string,
  orderDetailsData: PropTypes.shape({}),
  orderLabels: PropTypes.shape({}).isRequired,
  labels: PropTypes.shape({}),
  selectOrderAction: PropTypes.func,
  selectedOrderItems: PropTypes.shape({}),
  itemLevelReturns: PropTypes.shape({}),
  loggedInUserEmail: PropTypes.string,
  isLoggedIn: PropTypes.bool,
  cancelStatusData: PropTypes.shape({}),
  selectedReasonCode: PropTypes.shape({}),
  shipmentDelayedStatusData: PropTypes.shape({}),
  trackAnalyticsPageView: PropTypes.func,
  router: PropTypes.shape({}),
  showLoader: PropTypes.func,
  hideLoader: PropTypes.func,
  getOrderSummary: PropTypes.shape({}),
  isLiveChatEnabled: PropTypes.bool,
  selectedReturnMethod: PropTypes.string,
  isInitiateReturnFailure: PropTypes.string,
  clearReturnMethod: PropTypes.func,
};

Confirmation.defaultProps = {
  className: '',
  orderDetailsData: {},
  labels: {},
  selectedOrderItems: {},
  selectOrderAction: () => {},
  itemLevelReturns: {},
  loggedInUserEmail: '',
  isLoggedIn: false,
  cancelStatusData: {},
  selectedReasonCode: {},
  shipmentDelayedStatusData: {},
  trackAnalyticsPageView: () => {},
  router: {},
  showLoader: () => {},
  hideLoader: () => {},
  getOrderSummary: {},
  isLiveChatEnabled: false,
  selectedReturnMethod: '',
  isInitiateReturnFailure: '',
  clearReturnMethod: () => {},
};

export default withStyles(Confirmation, styles);
export { Confirmation as ConfirmationVanilla };
