// 9fbef606107a605d69c0edbcd8029e5d
import { fromJS, Map } from 'immutable';
import mergeAccountsReducer from '../MergeAccounts.reducer';
import MERGE_ACCOUNTS_CONSTANTS from '../../MergeAccounts.constants';

describe('Merge Accounts Reducer', () => {
  const initialState = fromJS({
    mergeAccountButtonLoader: false,
    mergeAccountSuccess: null,
    error: null,
    showConfirmationModal: false,
    isMergeConfirmed: false,
    firstTimeOtp: false,
  });

  it('should return empty merge account as default state', () => {
    expect(mergeAccountsReducer(undefined, {}).get('mergeAccountSuccess')).toBeNull();
    expect(mergeAccountsReducer(undefined, {}).get('error')).toBeNull();
  });

  it('should be called on merge account success', () => {
    const payload = {
      status: 'SUCCESS',
      timestamp: '2021-09-09T18:05:09.583788',
      data: {
        survivourStatus: 'null',
        victimStatus: null,
        survivourEmail: 'guptarajat846@gmail.com',
        victimEmail: null,
        mergeStatus: 'Merged',
      },
    };
    expect(
      mergeAccountsReducer(initialState, {
        type: MERGE_ACCOUNTS_CONSTANTS.MERGE_ACCOUNT_SUCCESS,
        payload,
      })
    ).toEqual(
      Map({
        mergeAccountButtonLoader: false,
        mergeAccountSuccess: payload,
        error: null,
        isMergeConfirmed: false,
        showConfirmationModal: false,
        firstTimeOtp: false,
      })
    );
  });

  it('should be called on merge account  failed', () => {
    const err = fromJS({
      status: 'ERROR',
      timestamp: '2021-09-07T08:38:37.264499',
      errors: [
        {
          errorCode: 'Internal Server Error',
          errorMessage: 'Error occured while fetching the members details from Brierley',
          errorParameters: null,
          errorType: null,
        },
      ],
      exceptionType: 'RUNTIME_EXCEPTION',
    });
    expect(
      mergeAccountsReducer(initialState, {
        type: MERGE_ACCOUNTS_CONSTANTS.MERGE_ACCOUNT_FAILED,
        payload: err,
      })
    ).toEqual(
      Map({
        mergeAccountButtonLoader: false,
        error: err,
        mergeAccountSuccess: null,
        isMergeConfirmed: false,
        showConfirmationModal: false,
        firstTimeOtp: false,
      })
    );
  });

  it('should be called on merge account show confirmation modal set', () => {
    const payload = true;
    expect(
      mergeAccountsReducer(initialState, {
        type: MERGE_ACCOUNTS_CONSTANTS.SET_SHOW_CONFIRMATION_MODAL,
        payload,
      })
    ).toEqual(
      Map({
        mergeAccountButtonLoader: false,
        mergeAccountSuccess: null,
        error: null,
        showConfirmationModal: true,
        isMergeConfirmed: false,
        firstTimeOtp: false,
      })
    );
  });

  it('should be called on merge account set merge confirmed', () => {
    const payload = true;
    expect(
      mergeAccountsReducer(initialState, {
        type: MERGE_ACCOUNTS_CONSTANTS.SET_MERGE_CONFIRMED,
        payload,
      })
    ).toEqual(
      Map({
        mergeAccountButtonLoader: false,
        mergeAccountSuccess: null,
        error: null,
        showConfirmationModal: false,
        isMergeConfirmed: true,
        firstTimeOtp: false,
      })
    );
  });

  it('should be called on merge account set first time otp', () => {
    const payload = true;
    expect(
      mergeAccountsReducer(initialState, {
        type: MERGE_ACCOUNTS_CONSTANTS.SET_FIRST_TIME_OTP,
        payload,
      })
    ).toEqual(
      Map({
        mergeAccountButtonLoader: false,
        mergeAccountSuccess: null,
        error: null,
        showConfirmationModal: false,
        isMergeConfirmed: false,
        firstTimeOtp: true,
      })
    );
  });
});
