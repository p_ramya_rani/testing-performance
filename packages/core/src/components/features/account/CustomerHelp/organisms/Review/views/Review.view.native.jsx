// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { ScrollView } from 'react-native';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import {
  ViewWithSpacing,
  BodyCopyWithSpacing,
} from '@tcp/core/src/components/common/atoms/styledWrapper';
import { Button } from '@tcp/core/src/components/common/atoms';
import { getCalculatedRefund } from '../../../util/utility';
import RefundSummaryDetails from '../organisms/RefundSummary';

import { GrayTopBorder, BlueUnderLine } from '../../../styles/CustomerHelp.style.native';
import { REASON_CODES } from '../../../CustomerHelp.constants';
import PaymentDetails from '../organisms/PaymentDetails';
import ReviewItemsList from '../organisms/ReviewItemList';

const getOrderStatusCancelItems = (action, labels) => {
  return action === REASON_CODES.CANCEL_MODIFY
    ? getLabelValue(labels, 'lbl_items_to_be_canceled')
    : getLabelValue(labels, 'lbl_items_to_be_refunded');
};

const getOrderPaymentDetails = (action, labels) => {
  return action === REASON_CODES.CANCEL_MODIFY
    ? `${getLabelValue(labels, 'lbl_auth_hold_policies')} ${getLabelValue(
        labels,
        'lbl_auth_hold_policies1'
      )}`
    : getLabelValue(labels, 'lbl_payment_details_msg');
};
const getOrderStatus = (action, labels) => {
  return action === REASON_CODES.CANCEL_MODIFY
    ? getLabelValue(labels, 'lbl_cancelation_summary')
    : getLabelValue(labels, 'lbl_refund_summary');
};

const renderRefundSummary = (
  orderDetailsData,
  orderLabels,
  labels,
  selectedOrderItems,
  orderItems,
  ...others
) => {
  const [action, summary, isPaidAndShippingCase] = others;
  if (orderDetailsData && Object.keys(orderDetailsData).length) {
    const { currencySymbol, shippingTotal } = summary || {};
    const { totalBeforeTax, tax, refundTotal, itemsCount } = getCalculatedRefund(
      selectedOrderItems,
      orderItems,
      '',
      action,
      summary
    );
    const isCancelOrderPage = action === REASON_CODES.CANCEL_MODIFY;
    return (
      <RefundSummaryDetails
        orderDetailsData={orderDetailsData}
        orderLabels={orderLabels && orderLabels.orders}
        labels={labels}
        totalBeforeTax={totalBeforeTax}
        tax={tax}
        refundTotal={refundTotal}
        itemsCount={itemsCount}
        currencySymbol={currencySymbol}
        orderStatus={getOrderStatus(action, labels)}
        isCancelOrderPage={isCancelOrderPage}
        shippingTotal={shippingTotal}
        isPaidAndShippingCase={isPaidAndShippingCase}
        isReviewPage
      />
    );
  }

  return null;
};

const initiateRefund = props => {
  const {
    submitRefund,
    orderDetailsData,
    selectedOrderItems,
    loggedInUserEmail,
    isLoggedIn,
    wrongItemData = {},
    action,
    getShipmentRefundStatus,
    navigation,
  } = props;
  let refundPayload;
  const { orderNumber, orderItems } = orderDetailsData;
  const items = [];
  orderItems.forEach(purchasedItem => {
    purchasedItem.items.forEach(item => {
      const { lineNumber: updatedLineNumber } = item;
      const {
        productInfo: { lineNumber },
      } = item;
      const isSelectedOrderItem = selectedOrderItems[updatedLineNumber] > 0;
      if (isSelectedOrderItem) {
        items.push({
          lineNumber,
          refundQty: selectedOrderItems[updatedLineNumber],
        });
      }
    });
  });
  refundPayload = {
    orderId: orderNumber,
    emailId: isLoggedIn ? loggedInUserEmail : orderDetailsData.emailAddress || '',
    reasonCode: action,
    navigation,
  };

  if (action === REASON_CODES.PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME) {
    getShipmentRefundStatus(refundPayload);
  } else {
    refundPayload = {
      ...refundPayload,
      items,
      ...wrongItemData,
    };
    submitRefund(refundPayload);
  }
};

const handleSubmit = ({
  action,
  submitRefund,
  orderDetailsData,
  selectedOrderItems,
  loggedInUserEmail,
  isLoggedIn,
  orderNumber,
  getCancelStatus,
  wrongItemData,
  getShipmentRefundStatus,
  navigation,
  summary,
  getSavedSummary,
}) => {
  if (action === REASON_CODES.CANCEL_MODIFY) {
    getSavedSummary(summary);
    getCancelStatus({ orderNumber, loggedInUserEmail, navigation });
  } else {
    initiateRefund({
      submitRefund,
      orderDetailsData,
      selectedOrderItems,
      loggedInUserEmail,
      isLoggedIn,
      wrongItemData,
      action,
      getShipmentRefundStatus,
      navigation,
    });
  }
};

const renderPaymentDetailView = (orderDetailsData, orderLabels, action, labels) => {
  return (
    orderDetailsData &&
    Object.keys(orderDetailsData).length > 0 && (
      <PaymentDetails
        labels={labels}
        orderDetailsData={orderDetailsData}
        ordersLabels={orderLabels && orderLabels.orders}
        orderStatus={getOrderPaymentDetails(action, labels)}
        showCardDetails={false}
      />
    )
  );
};

const getReviewMessage = (labels, action) => {
  if (action === 'CANCEL_MODIFY') {
    return (
      <BodyCopyWithSpacing
        spacingStyles="margin-top-MED"
        fontSize="fs14"
        fontFamily="secondary"
        color="black"
        text={getLabelValue(labels, 'lbl_review_details_msg1')}
      />
    );
  }
  if (action === REASON_CODES.RECEIVED_WRONG_ITEM) {
    return (
      <BodyCopyWithSpacing
        spacingStyles="margin-top-MED"
        fontSize="fs14"
        fontFamily="secondary"
        color="black"
        text={getLabelValue(labels, 'lbl_wrong_item_msg')}
      />
    );
  }

  return (
    <BodyCopyWithSpacing
      spacingStyles="margin-top-MED"
      fontSize="fs14"
      fontFamily="secondary"
      color="black"
      text={getLabelValue(labels, 'lbl_delivered_not_received_msg')}
    />
  );
};

const Review = ({
  orderDetailsData,
  className,
  orderLabels,
  labels,
  selectedOrderItems,
  selectedReasonCode,
  wrongItemData,
  ...otherPros
}) => {
  const { summary = {}, orderItems, orderNumber } = orderDetailsData || {};

  const { currencySymbol } = summary;
  const { action } = selectedReasonCode || {};
  const {
    submitRefund,
    loggedInUserEmail,
    isLoggedIn,
    getCancelStatus,
    getShipmentRefundStatus,
    getSavedSummary,
  } = otherPros;
  const { navigation } = otherPros;
  const isPaidAndShippingCase = action === REASON_CODES.PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME;

  return (
    orderDetailsData &&
    Object.keys(orderDetailsData).length && (
      <>
        <ScrollView>
          <GrayTopBorder />
          <ViewWithSpacing spacingStyles="margin-left-MED margin-right-MED margin-bottom-MED">
            <BodyCopyWithSpacing
              spacingStyles="margin-top-MED"
              fontSize="fs16"
              fontWeight="extrabold"
              fontFamily="secondary"
              color="black"
              text={getLabelValue(labels, 'lbl_review_details')}
            />
            <BlueUnderLine />

            {getReviewMessage(labels, action)}
            <BodyCopyWithSpacing
              spacingStyles="margin-top-MED"
              fontSize="fs14"
              fontFamily="secondary"
              color="black"
              text={getLabelValue(labels, 'lbl_review_details_msg2')}
            />
          </ViewWithSpacing>

          <GrayTopBorder />
          <ViewWithSpacing spacingStyles="margin-left-MED margin-right-MED margin-bottom-MED">
            {renderRefundSummary(
              orderDetailsData,
              orderLabels,
              labels,
              selectedOrderItems,
              orderItems,
              action,
              summary,
              isPaidAndShippingCase
            )}
          </ViewWithSpacing>

          <GrayTopBorder />
          <ViewWithSpacing spacingStyles="margin-left-MED margin-right-MED margin-bottom-MED">
            {renderPaymentDetailView(orderDetailsData, orderLabels, action, labels)}
          </ViewWithSpacing>

          <GrayTopBorder />
          {action !== REASON_CODES.PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME && (
            <ViewWithSpacing spacingStyles="margin-top-MED">
              <BodyCopyWithSpacing
                text={getOrderStatusCancelItems(action, labels)}
                fontSize="fs16"
                fontWeight="extrabold"
                fontFamily="secondary"
                color="black"
                spacingStyles="margin-left-MED margin-right-MED"
              />
              <ViewWithSpacing spacingStyles="margin-left-MED">
                <BlueUnderLine />
              </ViewWithSpacing>

              {Array.isArray(orderItems) &&
                orderItems.map((orderGroup, index) => (
                  <ReviewItemsList
                    key={index.toString()}
                    orderLabels={orderLabels}
                    items={orderGroup.items}
                    currencySymbol={currencySymbol}
                    isEcol
                    labels={labels}
                    action={action}
                    selectedOrderItems={selectedOrderItems}
                    orderStatus={getOrderStatusCancelItems(action, labels)}
                  />
                ))}
            </ViewWithSpacing>
          )}
        </ScrollView>
        <ViewWithSpacing spacingStyles="margin-top-MED margin-bottom-LRG margin-left-XXXL margin-right-XXXL">
          <Button
            fontSize="fs14"
            buttonVariation="variable-width"
            fill="BLUE"
            height="51px"
            text={getLabelValue(orderLabels && orderLabels.orders, 'lbl_review_submit')}
            onPress={() => {
              handleSubmit({
                action,
                submitRefund,
                orderDetailsData,
                selectedOrderItems,
                loggedInUserEmail: isLoggedIn
                  ? loggedInUserEmail
                  : orderDetailsData.emailAddress || '',
                isLoggedIn,
                orderNumber,
                getCancelStatus,
                wrongItemData,
                getShipmentRefundStatus,
                navigation,
                summary,
                getSavedSummary,
              });
            }}
          />
        </ViewWithSpacing>
      </>
    )
  );
};

Review.propTypes = {
  labels: PropTypes.shape({}).isRequired,
};
Review.defaultProps = {};

export default Review;
