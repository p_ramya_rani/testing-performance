// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { BodyCopy, Image, Row, Col } from '@tcp/core/src/components/common/atoms';
import { getIconPath } from '@tcp/core/src/utils';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import get from 'lodash/get';
import styles from '../../../../../../OrderDetails/organism/OrderBillingDetails/styles/OrderBillingDetails.style';
import cardIconMapping from '../../../../../../OrderDetails/organism/OrderBillingDetails/OrderBillingDetails.constants';

const renderGiftCardPayment = (showCardDetails, appliedGiftCards, labels) => {
  const showGCDetails = appliedGiftCards && appliedGiftCards.length > 0;
  const gcCardType =
    showCardDetails && showGCDetails && appliedGiftCards[0] && appliedGiftCards[0].cardType;
  const gcCardTypeUpperCase = gcCardType && gcCardType.toUpperCase();
  return (
    showCardDetails &&
    showGCDetails && (
      <Col
        className="card-details card-details-summary"
        colSize={{ large: 12, medium: 4, small: 3 }}
      >
        <BodyCopy component="div" className="card-info">
          <Image
            src={getIconPath(cardIconMapping[gcCardTypeUpperCase])}
            alt={gcCardType}
            className="elem-mr-XS card-border"
          />
          <BodyCopy fontSize="fs14" fontFamily="secondary">
            {getLabelValue(labels, 'lbl_help_gift_card')}
          </BodyCopy>
        </BodyCopy>
      </Col>
    )
  );
};

const getPaymentType = (card) => {
  if (card && card.afterpay) {
    return 'AFTERPAY';
  }
  return (card && card.cardType && card.cardType.toUpperCase()) || '';
};

const PaymentDetails = ({
  labels,
  orderDetailsData,
  ordersLabels,
  orderStatus,
  showCardDetails,
}) => {
  const { appliedGiftCards } = orderDetailsData;
  const card = get(orderDetailsData, 'checkout.billing.card', {});
  const firstName = get(orderDetailsData, 'checkout.billing.billingAddress.firstName', '');
  const lastName = get(orderDetailsData, 'checkout.billing.billingAddress.lastName', '');

  const billingIcon = (cardType) => {
    return cardType !== 'APPLEPAY' && card && card.endingNumbers
      ? `${getLabelValue(ordersLabels, 'lbl_orders_ending')} ${card.endingNumbers.slice(-4)}`
      : '';
  };

  const getFirstNameLastNameView = (cardType) => {
    if (cardType === 'VENMO' || cardType === 'APPLEPAY' || cardType === 'PAYPAL') {
      return null;
    }

    return (
      <BodyCopy component="p" fontSize="fs14" fontFamily="secondary">
        {`${firstName} ${lastName}`}
      </BodyCopy>
    );
  };

  const cardTypeUpperCase = getPaymentType(card);
  return (
    <BodyCopy component="div">
      <Row className="payment-details-header">
        <Image alt="close" src={getIconPath('card-payment-logo')} height="22px" width="25px" />
        <BodyCopy
          fontSize="fs16"
          fontWeight="extrabold"
          fontFamily="secondary"
          className="elem-mb-XS elem-pl-XXS"
          color="black"
        >
          {getLabelValue(labels, 'lbl_payment_details')}
        </BodyCopy>
      </Row>
      <BodyCopy className=" title-underline" />
      <Row fullBleed className="elem-mb-XS">
        {showCardDetails && card && card.endingNumbers && (
          <Col
            className="card-details card-details-summary"
            colSize={{ large: 12, medium: 4, small: 3 }}
          >
            <BodyCopy component="div" className="card-info">
              <Image
                src={getIconPath(cardIconMapping[cardTypeUpperCase])}
                alt={card.cardType}
                className="elem-mr-XS card-border"
              />
              {!card.afterpay && (
                <BodyCopy fontSize="fs14" fontFamily="secondary">
                  {cardTypeUpperCase !== 'VENMO'
                    ? billingIcon(cardTypeUpperCase)
                    : card.endingNumbers}
                </BodyCopy>
              )}
            </BodyCopy>

            {(firstName || lastName) &&
              !card.afterpay &&
              getFirstNameLastNameView(cardTypeUpperCase)}
          </Col>
        )}
        {renderGiftCardPayment(showCardDetails, appliedGiftCards, labels)}
        <BodyCopy
          className="payment-details-text"
          component="div"
          fontSize="fs14"
          fontWeight="semiBold"
          fontFamily="secondary"
        >
          {orderStatus}
        </BodyCopy>
      </Row>
    </BodyCopy>
  );
};
PaymentDetails.propTypes = {
  orderDetailsData: PropTypes.shape({}),
  ordersLabels: PropTypes.shape({}),
  labels: PropTypes.shape({}),
  orderStatus: PropTypes.string,
  showCardDetails: PropTypes.bool.isRequired,
};

PaymentDetails.defaultProps = {
  orderDetailsData: {},
  ordersLabels: {},
  labels: {},
  orderStatus: '',
};

export default withStyles(PaymentDetails, styles);
export { PaymentDetails as PaymentDetailsVanilla };
