// 9fbef606107a605d69c0edbcd8029e5d
import { MERGE_ACCOUNT_ACCOUNT_CONFIRMATION_REDUCER_KEY } from '@tcp/core/src/constants/reducer.constants';
import { createSelector } from 'reselect';
import { getLabelValue } from '../../../../../../../../../utils';
import ACCOUNT_CONFIRMATION_CONSTANTS from '../AccountConfirmation.constants';

export const getLabels = (state) => state?.Labels?.global;

export const getAccountConfirmationFailure = (state) => {
  return state[MERGE_ACCOUNT_ACCOUNT_CONFIRMATION_REDUCER_KEY].get('error');
};

export const getAccountConfirmationSuccess = (state) => {
  return state[MERGE_ACCOUNT_ACCOUNT_CONFIRMATION_REDUCER_KEY].get('accountConfirmationSuccess');
};

const errorMessage = (error, labels) =>
  error === ACCOUNT_CONFIRMATION_CONSTANTS.ACCOUNT_INVALID
    ? getLabelValue(labels.helpCenter, 'lbl_merge_account_accountverification_error-message')
    : labels.errorMessages.lbl_errorMessages_DEFAULT;

export const getMergeAccountConfirmationError = createSelector(
  [getAccountConfirmationFailure, getLabels],

  (error, labels) => (error ? errorMessage(error, labels) : null)
);
