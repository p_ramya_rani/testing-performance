// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import AccountInfoComponent from '../views';
import {
  getProfileInfoTileData,
  getUserInfoFetchingState,
} from '../../../../User/container/User.selectors';

const AccountInfo = ({ labels, profileInfo, isFetching }) => (
  <AccountInfoComponent profileInfo={profileInfo} labels={labels} isFetching={isFetching} />
);

AccountInfo.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  profileInfo: PropTypes.shape({}).isRequired,
  isFetching: PropTypes.bool,
};

AccountInfo.defaultProps = {
  isFetching: false,
};

export const mapStateToProps = (state) => ({
  profileInfo: getProfileInfoTileData(state),
  isFetching: getUserInfoFetchingState(state),
});

export { AccountInfo as ProfileInfoTileVanilla };

export default connect(mapStateToProps, null)(AccountInfo);
