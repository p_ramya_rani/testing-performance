// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { SelectOrderVanilla } from '../views/SelectOrder.view';

describe('Select Order Template', () => {
  it('should render Select Order Template', () => {
    const component = shallow(<SelectOrderVanilla />);
    expect(component).toMatchSnapshot();
  });
});
