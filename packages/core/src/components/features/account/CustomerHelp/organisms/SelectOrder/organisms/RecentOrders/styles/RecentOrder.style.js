// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  .no-order-content {
    margin: auto;
    text-align: center;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  }

  .empty-box {
    margin-top: 80px;
  }

  .no-order-message {
    margin: ${props => props.theme.spacing.ELEM_SPACING.LRG} auto 140px;
    opacity: 0.9;
    width: 290px;
  }

  .bigger-container {
    @media ${props => props.theme.mediaQuery.medium} {
      width: 330px;
    }
    @media ${props => props.theme.mediaQuery.xlarge} {
      :nth-child(3n) {
        margin-right: 0;
      }
    }
  }
`;

export default styles;
