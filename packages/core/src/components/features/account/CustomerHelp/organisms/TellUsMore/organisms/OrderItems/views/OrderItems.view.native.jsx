// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { getLabelValue, getShipmentCount } from '@tcp/core/src/utils/utils';
import OrderItem from '../../OrderItem/views/OrderItem.view.native';
import OrderItemHeader from '../../OrderItemHeader/views/OrderItemHeader.view.native';
import { ORDER_ITEM_STATUS } from '../../../../../CustomerHelp.constants';

const getOrderDisplayStatus = (orderStatus, orderLabels) => {
  const { IN_TRANSIT, IN_PROGRESS, SHIPMENT_API_DELIVERED, RETURN_INITIATED } = ORDER_ITEM_STATUS;
  switch (orderStatus) {
    case IN_TRANSIT:
      return getLabelValue(orderLabels, 'lbl_orders_statusItemsInTransit', 'orders');
    case SHIPMENT_API_DELIVERED:
      return getLabelValue(orderLabels, 'lbl_orders_statusItemsDelivered', 'orders');
    case IN_PROGRESS:
      return getLabelValue(orderLabels, 'lbl_orders_statusItemsInProgress', 'orders');
    case RETURN_INITIATED:
      return getLabelValue(orderLabels, 'lbl_orders_returns_initiated', 'orders');
    default:
      return '';
  }
};

const OrderItems = ({
  orderDetailsData,
  orderLabels,
  setSelectedOrderItems,
  selectedOrderItems,
  clearSelectedOrderItems,
  selectedReasonCode,
  clearSelectedResolution,
}) => {
  const { summary, orderItems, estimatedDeliveryDate } = orderDetailsData || {};
  const { currencySymbol } = summary || '$';
  const shipmentCount = getShipmentCount(orderItems);
  const trackingInfo = (orderGroup) =>
    orderGroup && orderGroup.items[0] && orderGroup.items[0].trackingInfo[0];

  return (
    <View>
      {orderItems &&
        orderItems.length > 0 &&
        orderItems.map((orderGroup, index) => (
          <>
            {orderGroup.items && orderGroup.items.length > 0 && (
              <>
                <OrderItemHeader
                  shipmentNumber={index + 1}
                  ItemsCount={shipmentCount}
                  status={orderGroup.orderStatus}
                  trackingNumber={orderGroup.trackingNumber}
                  trackingUrl={orderGroup.trackingUrl}
                  orderLabels={orderLabels && orderLabels.orders}
                  orderStatus={orderGroup.orderStatus}
                  shippedDate={trackingInfo(orderGroup)?.shipDate}
                  shipmentStatus={trackingInfo(orderGroup)?.status}
                  estimatedDeliveryDate={estimatedDeliveryDate}
                  actualDeliveryDate={trackingInfo(orderGroup)?.actualDeliveryDate}
                  carrierDeliveryDate={trackingInfo(orderGroup)?.carrierDeliveryDate}
                  orderDisplayStatus={getOrderDisplayStatus(orderGroup.orderStatus, orderLabels)}
                />

                {orderGroup.items.map(
                  (item, itemIndex) =>
                    item && (
                      <OrderItem
                        setSelectedOrderItems={setSelectedOrderItems}
                        selectedOrderItems={selectedOrderItems}
                        key={itemIndex.toString()}
                        {...{ item }}
                        currencySymbol={currencySymbol}
                        orderLabels={orderLabels && orderLabels.orders}
                        orderStatus={orderGroup.orderStatus}
                        clearSelectedOrderItems={clearSelectedOrderItems}
                        selectedReasonCode={selectedReasonCode}
                        clearSelectedResolution={clearSelectedResolution}
                      />
                    )
                )}
              </>
            )}
          </>
        ))}
    </View>
  );
};

OrderItems.propTypes = {
  orderDetailsData: PropTypes.shape({}).isRequired,
  selectedReasonCode: PropTypes.shape({}),
  labels: PropTypes.shape({}),
  orderLabels: PropTypes.shape({}),
  setSelectedOrderItems: PropTypes.shape({}),
  selectedOrderItems: PropTypes.shape({}),
  shipmentData: PropTypes.shape({}),
  clearSelectedOrderItems: PropTypes.func,
  clearSelectedResolution: PropTypes.func,
};

OrderItems.defaultProps = {
  orderLabels: {},
  setSelectedOrderItems: {},
  selectedOrderItems: {},
  selectedReasonCode: {},
  labels: {},
  shipmentData: {},
  clearSelectedOrderItems: () => {},
  clearSelectedResolution: () => {},
};

export default OrderItems;
