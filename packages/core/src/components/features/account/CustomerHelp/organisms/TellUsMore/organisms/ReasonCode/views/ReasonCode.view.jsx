// 9fbef606107a605d69c0edbcd8029e5d
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { Row, Col, BodyCopy } from '@tcp/core/src/components/common/atoms';
import uniqBy from 'lodash/uniqBy';
import { routerPush, formatYmd, smoothScrolling } from '@tcp/core/src/utils';
import {
  REASON_CODES,
  ORDER_ITEM_STATUS,
  SHIPPING_METHOD_STATUS,
  FILTERED_REASON_CODES_ON_SHIPPMENT_STATUS,
  FILTERED_REASON_CODES_ON_ORDER_ARRIVE,
} from '@tcp/core/src/components/features/account/CustomerHelp/CustomerHelp.constants';
import { getMaxDeliveryDate } from '@tcp/core/src/components/features/account/CustomerHelp/util/utility';
import internalEndpoints from '@tcp/core/src/components/features/account/common/internalEndpoints';
import styles from '../styles/ReasonCode.style';

const navigateToOrderDetailPage = (isLoggedIn, orderPage, orderNumber, encryptedEmailAddress) => {
  if (isLoggedIn) {
    routerPush(
      `${orderPage.link}&orderId=${orderNumber}&fromOrderHelp=true`,
      `${orderPage.path}/${orderNumber}`
    );
  } else {
    routerPush(
      `${internalEndpoints.trackOrder.link}&orderId=${orderNumber}&email=${encryptedEmailAddress}`,
      `${internalEndpoints.trackOrder.path}/${orderNumber}/${encryptedEmailAddress}`
    );
  }
};

const getPreSelectedReason = (reasonList, reasonCodeFromURL) =>
  reasonList && reasonList.find((reason) => reason.action === reasonCodeFromURL);

const onReasonCodeClick = ({
  reason,
  isLoggedIn,
  orderPage,
  orderNumber,
  encryptedEmailAddress,
  getShipmentData,
  setSelectedReasonCode,
  trackAnalyticsClick,
  isNewWhenWillMyOrderArrive,
  updateHelpstep1,
  helpstep1,
}) => {
  const { PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME, WHEN_WILL_MY_ORDER_ARRIVE } = REASON_CODES;

  const reasonCodeLbl = reason ? reason.label : '';
  let clickPath = reasonCodeLbl;
  if (reason.action === WHEN_WILL_MY_ORDER_ARRIVE && isNewWhenWillMyOrderArrive) {
    updateHelpstep1(reasonCodeLbl);
    clickPath = reasonCodeLbl;
  } else if (helpstep1) {
    clickPath = `${helpstep1}|${reasonCodeLbl}`;
  }

  trackAnalyticsClick({
    orderHelp_path_cd128: clickPath,
    orderHelpSelectedReason: clickPath,
  });

  switch (reason.action) {
    case WHEN_WILL_MY_ORDER_ARRIVE:
      if (isNewWhenWillMyOrderArrive) setSelectedReasonCode(reason);
      else navigateToOrderDetailPage(isLoggedIn, orderPage, orderNumber, encryptedEmailAddress);
      break;
    case PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME:
      getShipmentData(reason);
      break;
    default:
      setSelectedReasonCode(reason);
  }
};

const getPaidNExpediteOrderTransitStatus = (
  selectedReasonCode,
  orderItems,
  setOrderStatusInTransit
) => {
  const { SHIPMENT_API_DELIVERED } = ORDER_ITEM_STATUS;
  const { PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME } = REASON_CODES;
  if (selectedReasonCode.action === PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME) {
    const orderItemsGroup =
      orderItems && orderItems.every((order) => order.orderStatus === SHIPMENT_API_DELIVERED);
    if (!orderItemsGroup) {
      setOrderStatusInTransit(true);
    }
  }
};

const getStyle = (selectedReasonCode) => {
  return `reason-code-rectangle selected-reason ${selectedReasonCode ? 'selected-item' : ''}`;
};

const getInTransitDeliveryProgressReasonList = (
  inTransitDeliveryProgressStatus,
  inTransitDeliveryStatus
) => {
  const { IN_TRANSIT_ACTIONS, DELIVERY_ACTIONS } = FILTERED_REASON_CODES_ON_SHIPPMENT_STATUS;
  return inTransitDeliveryProgressStatus || inTransitDeliveryStatus
    ? [...IN_TRANSIT_ACTIONS, ...DELIVERY_ACTIONS]
    : [];
};

const getInProgressReasonList = (inProgressStatus) => {
  const { IN_PROGRESS_ACTIONS } = FILTERED_REASON_CODES_ON_SHIPPMENT_STATUS;
  return inProgressStatus ? [...IN_PROGRESS_ACTIONS] : [];
};

const getInTransitOrProgressReasonList = (inTransitProgressStatus, inTransitStatus) => {
  const { IN_TRANSIT_ACTIONS } = FILTERED_REASON_CODES_ON_SHIPPMENT_STATUS;
  return inTransitProgressStatus || inTransitStatus ? [...IN_TRANSIT_ACTIONS] : [];
};

const getInDeliveryOrProgressReasonList = (inDeliveryProgressStatus, inDeliveryStatus) => {
  const { DELIVERY_ACTIONS } = FILTERED_REASON_CODES_ON_SHIPPMENT_STATUS;
  return inDeliveryProgressStatus || inDeliveryStatus ? [...DELIVERY_ACTIONS] : [];
};

const getInRushOrExpressReasonList = (rush, express, inDeliveryStatus) => {
  const { RUSH_EXPRESS_ACTIONS } = FILTERED_REASON_CODES_ON_SHIPPMENT_STATUS;
  return (rush && inDeliveryStatus) || (express && inDeliveryStatus)
    ? [...RUSH_EXPRESS_ACTIONS]
    : [];
};

const getPartialDeliveredStatus = (
  inDeliveryProgressStatus,
  inTransitDeliveryStatus,
  inTransitDeliveryProgressStatus
) => {
  return inDeliveryProgressStatus || inTransitDeliveryStatus || inTransitDeliveryProgressStatus
    ? [REASON_CODES.WHEN_WILL_MY_ORDER_ARRIVE]
    : [];
};

const getNoOrderDetailsReasonList = (inProgress, inTransit, delivery, rush, express) => {
  const { ORDER_NOT_FOUND } = FILTERED_REASON_CODES_ON_SHIPPMENT_STATUS;
  return !inProgress && !inTransit && !delivery && !rush && !express ? [...ORDER_NOT_FOUND] : [];
};

const filterReasonCodeList = (orderStatuses) => {
  const { inProgress, inTransit, delivery, rush, express } = orderStatuses;

  const inDeliveryStatus = delivery && !inTransit && !inProgress;
  const inDeliveryProgressStatus = delivery && inProgress && !inTransit;
  const inTransitStatus = inTransit && !inProgress && !delivery;
  const inTransitProgressStatus = inTransit && inProgress && !delivery;
  const inProgressStatus = inProgress && !inTransit && !delivery;
  const inTransitDeliveryProgressStatus = inTransit && delivery && inProgress;
  const inTransitDeliveryStatus = inTransit && delivery && !inProgress;

  const inTransitDeliveryProgressReasonList = getInTransitDeliveryProgressReasonList(
    inTransitDeliveryProgressStatus,
    inTransitDeliveryStatus
  );
  const inProgressReasonList = getInProgressReasonList(inProgressStatus);
  const inTransitOrProgressReasonList = getInTransitOrProgressReasonList(
    inTransitProgressStatus,
    inTransitStatus
  );
  const partialDeliveredReasonList = getPartialDeliveredStatus(
    inDeliveryProgressStatus,
    inTransitDeliveryStatus,
    inTransitDeliveryProgressStatus
  );

  const inDeliveryOrProgressReasonList = getInDeliveryOrProgressReasonList(
    inDeliveryProgressStatus,
    inDeliveryStatus
  );
  const inRushOrExpressReasonList = getInRushOrExpressReasonList(rush, express, inDeliveryStatus);

  const orderNotFound = getNoOrderDetailsReasonList(inProgress, inTransit, delivery, rush, express);
  const reasoncodes = [
    ...inTransitDeliveryProgressReasonList,
    ...inProgressReasonList,
    ...inTransitOrProgressReasonList,
    ...inDeliveryOrProgressReasonList,
    ...inRushOrExpressReasonList,
    ...orderNotFound,
  ];

  if (
    partialDeliveredReasonList?.length > 0 &&
    !reasoncodes.includes(REASON_CODES.WHEN_WILL_MY_ORDER_ARRIVE)
  ) {
    return [...partialDeliveredReasonList, ...reasoncodes];
  }
  return reasoncodes;
};

const getFilteredReasonList = (
  reasonList,
  isCouponSelfHelpEnabled,
  orderItems = [],
  shippingMethod,
  isOlderItem,
  orderArriveInTransitReasonCode
) => {
  if (orderArriveInTransitReasonCode) {
    const { MY_ORDER_ARRIVE_TRANSIT_ACTIONS } = FILTERED_REASON_CODES_ON_ORDER_ARRIVE;
    return reasonList.filter((reason) => {
      return MY_ORDER_ARRIVE_TRANSIT_ACTIONS.includes(reason.action);
    });
  }
  const { SHIPMENT_API_DELIVERED, IN_TRANSIT, IN_PROGRESS, RETURN_INITIATED } = ORDER_ITEM_STATUS;
  const { RUSH, EXPRESS } = SHIPPING_METHOD_STATUS;

  const orderStatuses = {
    inProgress: false,
    inTransit: false,
    delivery: false,
    rush: false,
    express: false,
  };

  if (shippingMethod === RUSH) {
    orderStatuses.rush = true;
  } else if (shippingMethod === EXPRESS) {
    orderStatuses.express = true;
  }

  orderItems.forEach((orderItem) => {
    const { orderStatus } = orderItem;
    if (orderStatus === IN_PROGRESS) {
      orderStatuses.inProgress = true;
    } else if (orderStatus === IN_TRANSIT) {
      orderStatuses.inTransit = true;
    } else if (orderStatus === SHIPMENT_API_DELIVERED || orderStatus === RETURN_INITIATED) {
      orderStatuses.delivery = true;
    }
  });

  const reasonCodeActions = filterReasonCodeList(orderStatuses).filter((reasonCodeAction) => {
    if (reasonCodeAction === REASON_CODES.INITIATE_RETURN) {
      return !isOlderItem ? reasonCodeAction : null;
    }
    return reasonCodeAction;
  });

  return uniqBy(
    reasonList.filter((reason) => {
      return reasonCodeActions.includes(reason.action);
    }),
    'id'
  );
};

const triggerReasonEvent = (
  reason,
  isLoggedIn,
  orderPage,
  orderNumber,
  encryptedEmailAddress,
  setSelectedReasonCode,
  getShipmentData
) => {
  const { WHEN_WILL_MY_ORDER_ARRIVE, PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME } = REASON_CODES;
  switch (reason && reason.action) {
    case WHEN_WILL_MY_ORDER_ARRIVE:
      navigateToOrderDetailPage(isLoggedIn, orderPage, orderNumber, encryptedEmailAddress);
      break;
    case PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME:
      getShipmentData(reason);
      break;
    default:
      setSelectedReasonCode(reason);
  }
};

const getIgnoreGutter = (index) => ((index + 1) % 3 === 0 ? { large: true } : {});

const ReasonCode = ({
  reasonList,
  isCouponSelfHelpEnabled,
  selectedReasonCode,
  setSelectedReasonCode,
  className,
  isLoggedIn,
  orderDetailsData,
  getRefundStatus,
  setOrderStatusInTransit,
  trackAnalyticsClick,
  scrollClassName,
  shipmentData,
  reasonCodeFromURL,
  preReasonFlag,
  isOlderItem,
  isNewWhenWillMyOrderArrive,
  orderArriveInTransitReasonCode,
  action,
  updateHelpstep1,
  helpstep1,
}) => {
  const { orderDate, orderItems, shippingMethod, orderNumber, encryptedEmailAddress } =
    orderDetailsData || {};

  const { orderPage } = internalEndpoints;

  const getShipmentData = (reason) => {
    if (orderNumber && orderDate && shippingMethod) {
      const { trackingDetails } = shipmentData || {};
      const deliveryDate = getMaxDeliveryDate({ trackingDetails });
      const orderId = orderNumber;
      const orderPlacedDate = formatYmd(orderDate);
      const orderDeliveredDate = deliveryDate;

      getRefundStatus({ orderId, orderPlacedDate, shippingMethod, orderDeliveredDate });
    }
    setSelectedReasonCode(reason);
  };

  const filteredReasonList = getFilteredReasonList(
    reasonList,
    isCouponSelfHelpEnabled,
    orderItems,
    shippingMethod,
    isOlderItem,
    orderArriveInTransitReasonCode
  );

  useEffect(() => {
    if (selectedReasonCode && action !== REASON_CODES.WHEN_WILL_MY_ORDER_ARRIVE) {
      smoothScrolling('reason-code-header');
      getPaidNExpediteOrderTransitStatus(selectedReasonCode, orderItems, setOrderStatusInTransit);
    }

    if (reasonCodeFromURL && preReasonFlag) {
      const preSelectedReason = getPreSelectedReason(filteredReasonList, reasonCodeFromURL);
      triggerReasonEvent(
        preSelectedReason,
        isLoggedIn,
        orderPage,
        orderNumber,
        encryptedEmailAddress,
        setSelectedReasonCode,
        getShipmentData
      );
    }
  }, [selectedReasonCode]);

  return (
    <div className={className}>
      <Row fullBleed>
        {!selectedReasonCode ? (
          filteredReasonList.length > 0 &&
          filteredReasonList.map((reason, index) => (
            <Col
              colSize={{ small: 6, medium: 4, large: 4 }}
              className={`${scrollClassName} elem-mt-LRG`}
              onClick={() =>
                onReasonCodeClick({
                  reason,
                  isLoggedIn,
                  orderPage,
                  orderNumber,
                  encryptedEmailAddress,
                  getShipmentData,
                  setSelectedReasonCode,
                  trackAnalyticsClick,
                  isNewWhenWillMyOrderArrive,
                  updateHelpstep1,
                  helpstep1,
                })
              }
              ignoreGutter={getIgnoreGutter(index)}
            >
              <div className="reason-code-rectangle">
                <BodyCopy
                  component="p"
                  textAlign="left"
                  fontWeight="semibold"
                  fontSize="fs14"
                  fontFamily="secondary"
                  color="text.darkgray"
                  className="elem-pl-MED elem-pr-MED"
                >
                  {reason.label}
                </BodyCopy>
              </div>
            </Col>
          ))
        ) : (
          <Col colSize={{ small: 6, medium: 4, large: 4 }} id="selected-item">
            <div className={getStyle(selectedReasonCode)}>
              <BodyCopy
                component="p"
                textAlign="left"
                fontWeight="semibold"
                fontSize="fs14"
                fontFamily="secondary"
                color="text.darkgray"
                className="elem-pl-MED elem-pr-MED textColor"
              >
                {selectedReasonCode.label}
              </BodyCopy>
            </div>
          </Col>
        )}
      </Row>
    </div>
  );
};

ReasonCode.propTypes = {
  orderDetailsData: PropTypes.shape({}),
  reasonList: PropTypes.shape([]),
  selectedReasonCode: PropTypes.shape({}).isRequired,
  setSelectedReasonCode: PropTypes.func.isRequired,
  labels: PropTypes.shape({}).isRequired,
  className: PropTypes.string,
  isLoggedIn: PropTypes.bool.isRequired,
  getRefundStatus: PropTypes.func.isRequired,
  setOrderStatusInTransit: PropTypes.func,
  trackAnalyticsClick: PropTypes.func,
  shipmentData: PropTypes.shape({}).isRequired,
  scrollClassName: PropTypes.string.isRequired,
  reasonCodeFromURL: PropTypes.string,
  preReasonFlag: PropTypes.bool,
  isCouponSelfHelpEnabled: PropTypes.bool.isRequired,
  refundData: PropTypes.shape({}),
  isOlderItem: PropTypes.bool,
  orderArriveInTransitReasonCode: PropTypes.bool,
  helpstep1: PropTypes.string,
  updateHelpstep1: PropTypes.func,
};

ReasonCode.defaultProps = {
  reasonList: [],
  className: '',
  orderDetailsData: {
    orderNumber: '',
    encryptedEmailAddress: '',
  },
  setOrderStatusInTransit: () => {},
  trackAnalyticsClick: () => {},
  reasonCodeFromURL: '',
  preReasonFlag: true,
  refundData: {},
  isOlderItem: false,
  orderArriveInTransitReasonCode: false,
  helpstep1: '',
  updateHelpstep1: () => {},
};

export default withStyles(ReasonCode, styles);
export { ReasonCode as ReasonCodeVanilla };
