// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { BodyCopy, Anchor } from '@tcp/core/src/components/common/atoms';
import {
  getRCStatus,
  getLogoIconPath,
} from '@tcp/core/src/components/features/account/OrderDetails/organism/OrderDetailsWrapper/views/OrderDetailsWrapper.utils';
import {
  getLabelValue,
  getIconPath,
  hasTrackingNum,
  getTrimmedTrackingNum,
  orderDateFormatted,
} from '@tcp/core/src/utils/utils';
import styles from '../styles/OrderItemHeader.style';
import { ORDER_ITEM_STATUS } from '../../../../../CustomerHelp.constants';

const getShipmentIconAndColor = (status) => {
  const {
    IN_TRANSIT,
    SHIPMENT_API_DELIVERED,
    IN_PROGRESS,
    RETURN_INITIATED,
    ORDER_BEING_PROCESSED,
  } = ORDER_ITEM_STATUS;
  switch (status) {
    case IN_TRANSIT:
      return { icon: getIconPath('in-transit.svg'), color: 'black' };
    case SHIPMENT_API_DELIVERED:
      return { icon: getIconPath('circle-green-check'), color: 'green.600' };
    case ORDER_BEING_PROCESSED:
    case IN_PROGRESS:
      return { icon: getIconPath('in-progress'), color: 'black' };
    case ORDER_ITEM_STATUS.CANCELED:
    case ORDER_ITEM_STATUS.CANCELLED:
      return { icon: getIconPath('status-shipping-cancelled'), color: 'black' };
    case RETURN_INITIATED:
      return { icon: getLogoIconPath(RETURN_INITIATED), color: 'black' };
    default:
      return { icon: '', color: '' };
  }
};

const getEstimateDate = (estimatedDate) => (estimatedDate ? orderDateFormatted(estimatedDate) : '');
/**
 * This function component use for return the Order Package Status
 * can be passed in the component.
 * @param otherProps - otherProps object used pass params to other component
 */

const isShipmentItems = (orderStatus) =>
  !(
    orderStatus === ORDER_ITEM_STATUS.IN_PROGRESS ||
    orderStatus === ORDER_ITEM_STATUS.RETURN_INITIATED ||
    orderStatus === ORDER_ITEM_STATUS.CANCELED
  );

const OrderItemHeader = (props) => {
  const {
    orderStatus,
    orderDisplayStatus,
    trackingNumber,
    trackingUrl,
    orderLabels,
    shipmentNumber,
    ItemsCount,
    shippedDate,
    className,
    textFonSize,
    mediumText,
    shipmentStatus,
    actualDeliveryDate,
    carrierDeliveryDate,
    estimatedDeliveryDate,
  } = props;

  const { icon, color } = getShipmentIconAndColor(orderStatus);
  const orderTrackingNumber = getTrimmedTrackingNum(trackingNumber);

  const actualDeliveryDateValue = getEstimateDate(actualDeliveryDate);
  const carrierDeliveryDateValue = getEstimateDate(carrierDeliveryDate);
  const estimatedDeliveryDateValue = getEstimateDate(estimatedDeliveryDate);
  const shipDateDateValue = getEstimateDate(shippedDate);

  const { label, displayDate } =
    getRCStatus(
      shipmentStatus,
      estimatedDeliveryDateValue,
      actualDeliveryDateValue,
      shipDateDateValue,
      carrierDeliveryDateValue,
      orderLabels
    ) || {};

  return (
    <div className={className}>
      <div className="order-header-container">
        <div className="item-header">
          <img alt="" src={icon} width="20px" height="20px" />
          <BodyCopy
            className="elem-ml-XS"
            fontWeight="extrabold"
            fontSize="fs18"
            fontFamily="secondary"
            color={color}
          >
            {orderDisplayStatus}
          </BodyCopy>
          {isShipmentItems(orderStatus) && (
            <BodyCopy className="elem-ml-XS" fontSize={textFonSize} fontFamily="secondary">
              {`Shipment ${shipmentNumber} of ${ItemsCount}`}
            </BodyCopy>
          )}
        </div>

        {label && displayDate && (
          <div className="item-header-area">
            <BodyCopy
              className="item-header-content"
              fontSize={textFonSize}
              fontFamily="secondary"
              fontWeight="bold"
            >
              {`${label}`}
            </BodyCopy>
            <BodyCopy
              className="elem-pl-XXS item-header-content"
              fontWeight="bold"
              fontSize={textFonSize}
              fontFamily="secondary"
            >
              {displayDate}
            </BodyCopy>
          </div>
        )}
        {hasTrackingNum(orderTrackingNumber) && (
          <div className="tracking-number-area">
            <BodyCopy fontSize={textFonSize} fontFamily="secondary">
              {getLabelValue(orderLabels, 'lbl_orders_tracking-cta')}
            </BodyCopy>
            <Anchor
              className="blue-link elem-ml-XXS item-header-content"
              to={trackingUrl}
              asPath={trackingUrl}
              underline
              fontSizeVariation={mediumText ? 'medium' : 'large'}
              target="_blank"
            >
              {orderTrackingNumber}
            </Anchor>
          </div>
        )}
      </div>
    </div>
  );
};
OrderItemHeader.propTypes = {
  trackingNumber: PropTypes.string,
  trackingUrl: PropTypes.string,
  orderLabels: PropTypes.shape({
    lbl_orderDetails_shipping: PropTypes.string,
  }),
  orderStatus: PropTypes.string,
  orderDisplayStatus: PropTypes.string,
  shipmentNumber: PropTypes.string,
  ItemsCount: PropTypes.number,
  shippedDate: PropTypes.string,
  className: PropTypes.string,
  textFonSize: PropTypes.string,
  mediumText: PropTypes.string,
};

OrderItemHeader.defaultProps = {
  trackingNumber: '',
  trackingUrl: '',
  orderLabels: {
    lbl_orderDetails_shipping: '',
  },
  orderStatus: '',
  orderDisplayStatus: '',
  shipmentNumber: '',
  ItemsCount: 0,
  shippedDate: '',
  className: '',
  textFonSize: 'fs14',
  mediumText: 'large',
};

export default withStyles(OrderItemHeader, styles);
export { OrderItemHeader as OrderItemHeaderVanilla };
