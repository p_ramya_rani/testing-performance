// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  getCouponDetails,
  resetCouponDetails,
  setCouponDetailsError,
  applyCoupon,
  removeCoupon,
  toggleNeedHelpModalState,
} from '@tcp/core/src/components/features/CnC/common/organism/CouponAndPromos/container/Coupon.actions';
import {
  getCommonLabels,
  getAccessibilityLabels,
} from '@tcp/core/src/components/features/account/Account/container/Account.selectors';
import {
  getCouponDetails as getCoupon,
  getCouponDetailsError,
  getCouponDetailsLoader,
  getApplyCouponCSHLoader,
  getAllCoupons,
  getAppliedCouponListState,
  getNeedHelpModalState,
  getNeedHelpContent,
  getPlaceCashContent,
  getonHoldCouponListState,
} from '@tcp/core/src/components/features/CnC/common/organism/CouponAndPromos/container/Coupon.selectors';
import {
  getIsLiveChatEnabled,
  getIsLiveChatBackABisOff,
  getIsInternationalShipping,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import {
  getUserLoggedInState,
  getUserEmail,
} from '@tcp/core/src/components/features/account/User/container/User.selectors';
import { setClickAnalyticsData, trackClick } from '@tcp/core/src/analytics/actions';
import CheckCoupon from '../views';
import CouponHelpModal from '../../../../../../../CnC/common/organism/CouponAndPromos/views/CouponHelpModal.view';
import { crossTop } from '../../../../../../../CnC/common/organism/CouponAndPromos/styles/Coupon.style';

const getHelpModalLabels = (needHelpRichText, placeCashContent) => {
  return {
    NEED_HELP_RICH_TEXT: needHelpRichText,
    PLACE_CASH_CONTENT: placeCashContent,
  };
};

export function CheckCouponContainer(props) {
  const {
    labels,
    handleCheckCoupon,
    coupon,
    resetCoupon,
    couponError,
    setCouponError,
    couponDetailsLoader,
    handleApplyCoupon,
    handleRemoveCoupon,
    appliedCouponList,
    applyCouponLoader,
    commonLabels,
    isLiveChatEnabled,
    isInternationalShipping,
    isLoggedIn,
    loggedInUserEmail,
    trackAnalyticsClick,
    isNeedHelpModalOpen,
    toggleNeedHelpModal,
    needHelpRichText,
    placeCashContent,
    onHoldCouponList,
  } = props;

  const helpModalLabels = getHelpModalLabels(needHelpRichText, placeCashContent);
  return (
    <>
      <CheckCoupon
        labels={labels}
        onSubmit={handleCheckCoupon}
        onApply={handleApplyCoupon}
        onRemove={handleRemoveCoupon}
        coupon={coupon}
        resetCoupon={resetCoupon}
        couponError={couponError}
        setCouponError={setCouponError}
        couponDetailsLoader={couponDetailsLoader}
        appliedCouponList={appliedCouponList}
        onHoldCouponList={onHoldCouponList}
        applyCouponLoader={applyCouponLoader}
        commonLabels={commonLabels}
        isLiveChatEnabled={isLiveChatEnabled}
        isInternationalShipping={isInternationalShipping}
        isLoggedIn={isLoggedIn}
        loggedInUserEmail={loggedInUserEmail}
        trackAnalyticsClick={trackAnalyticsClick}
      />
      {!isLoggedIn && (
        <CouponHelpModal
          labels={helpModalLabels}
          openState={isNeedHelpModalOpen}
          onRequestClose={() => {
            toggleNeedHelpModal();
          }}
          inheritedStyles={crossTop}
        />
      )}
    </>
  );
}

CheckCouponContainer.propTypes = {
  isLiveChatEnabled: PropTypes.bool.isRequired,
  handleCheckCoupon: PropTypes.func.isRequired,
  handleApplyCoupon: PropTypes.func.isRequired,
  handleRemoveCoupon: PropTypes.func.isRequired,
  coupon: PropTypes.shape({}).isRequired,
  resetCoupon: PropTypes.func.isRequired,
  couponError: PropTypes.func.isRequired,
  setCouponError: PropTypes.func.isRequired,
  couponDetailsLoader: PropTypes.bool.isRequired,
  applyCouponLoader: PropTypes.bool.isRequired,
  labels: PropTypes.shape({}).isRequired,
  appliedCouponList: PropTypes.shape([]).isRequired,
  onHoldCouponList: PropTypes.shape([]).isRequired,
  commonLabels: PropTypes.shape({}).isRequired,
  isLoggedIn: PropTypes.bool.isRequired,
  loggedInUserEmail: PropTypes.string,
  trackAnalyticsClick: PropTypes.func.isRequired,
  toggleNeedHelpModal: PropTypes.func.isRequired,
  isNeedHelpModalOpen: PropTypes.bool.isRequired,
  needHelpRichText: PropTypes.string.isRequired,
  placeCashContent: PropTypes.string.isRequired,
};

CheckCouponContainer.defaultProps = {
  loggedInUserEmail: '',
};

const mapStateToProps = (state) => {
  return {
    coupon: getCoupon(state),
    isLiveChatEnabled: getIsLiveChatEnabled(state) && getIsLiveChatBackABisOff(state),
    isInternationalShipping: getIsInternationalShipping(state),
    couponError: getCouponDetailsError(state),
    couponDetailsLoader: getCouponDetailsLoader(state),
    applyCouponLoader: getApplyCouponCSHLoader(state),
    allCouponList: getAllCoupons(state),
    appliedCouponList: getAppliedCouponListState(state),
    onHoldCouponList: getonHoldCouponListState(state),
    commonLabels: {
      ...getCommonLabels(state),
      ...getAccessibilityLabels(state),
    },
    loggedInUserEmail: getUserEmail(state),
    isLoggedIn: getUserLoggedInState(state),
    isNeedHelpModalOpen: getNeedHelpModalState(state),
    needHelpRichText: getNeedHelpContent(state),
    placeCashContent: getPlaceCashContent(state),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleCheckCoupon: (formData) => {
      dispatch(getCouponDetails({ formData }));
    },
    resetCoupon: () => {
      dispatch(resetCouponDetails());
    },
    setCouponError: (error) => {
      dispatch(setCouponDetailsError(error));
    },
    handleApplyCoupon: (coupon) => {
      return new Promise((resolve, reject) => {
        dispatch(
          applyCoupon({
            formData: { couponCode: coupon.id },
            formPromise: { resolve, reject },
            coupon,
            fromCSH: true,
          })
        );
      });
    },
    handleRemoveCoupon: (coupon) => {
      return new Promise((resolve, reject) => {
        dispatch(
          removeCoupon({
            coupon,
            formPromise: { resolve, reject },
            fromCSH: true,
          })
        );
      });
    },
    trackAnalyticsClick: (eventData, payload) => {
      dispatch(setClickAnalyticsData(eventData));
      const timer = setTimeout(() => {
        dispatch(trackClick(payload));
        clearTimeout(timer);
      }, 800);
    },
    toggleNeedHelpModal: () => {
      dispatch(toggleNeedHelpModalState());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CheckCouponContainer);
