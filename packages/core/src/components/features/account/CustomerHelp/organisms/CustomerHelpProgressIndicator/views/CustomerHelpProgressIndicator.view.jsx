// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import StepIndicator from '@tcp/web/src/components/features/content/CheckoutProgressIndicator/views/StepIndicator.view';
import styles from '@tcp/web/src/components/features/content/CheckoutProgressIndicator/StepIndicator.style';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import { Col, Row } from '../../../../../../common/atoms';
import { CUSTOMER_HELP_PAGE_TEMPLATE } from '../../../CustomerHelp.constants';

const CUSTOMERHELP_STAGE_PROP_TYPE = PropTypes.oneOf(
  Object.keys(CUSTOMER_HELP_PAGE_TEMPLATE).map((key) => CUSTOMER_HELP_PAGE_TEMPLATE[key])
);

export class CustomerHelpProgressIndicator extends React.Component {
  static propTypes = {
    activeStage: CUSTOMERHELP_STAGE_PROP_TYPE.isRequired,
    moveToCustomerHelpStage: PropTypes.func.isRequired,
    availableStages: PropTypes.arrayOf(CUSTOMERHELP_STAGE_PROP_TYPE).isRequired,
    className: PropTypes.string.isRequired,
    labels: PropTypes.shape({}).isRequired,
    ordersListItems: PropTypes.shape([]),
  };

  static defaultProps = {
    ordersListItems: [],
  };

  getStageLabel = (stage) => {
    const { labels } = this.props;
    const customerHelpProgressBarLabels = {
      'select-order-label': getLabelValue(labels, 'lbl_start'),
      'tell-us-more-label': getLabelValue(labels, 'lbl_tell_us_more_add'),
      'review-label': getLabelValue(labels, 'lbl_submit_add'),
    };
    return customerHelpProgressBarLabels[`${stage}-label`];
  };

  render() {
    const { activeStage, availableStages, className, moveToCustomerHelpStage, ordersListItems } =
      this.props;

    let hasSeenActive = false;

    return (
      <div className={className}>
        <Row fullBleed>
          <Col colSize={{ small: 6, medium: 8, large: 12 }}>
            <ul>
              {availableStages.length >= 3 &&
                availableStages.map((stage) => {
                  if (stage?.indexOf(activeStage) > -1) {
                    hasSeenActive = true;
                    return (
                      <StepIndicator
                        availableStages={availableStages}
                        isActive
                        key={stage}
                        name={this.getStageLabel(stage)}
                        isOrderHelpLabel
                      />
                    );
                  }
                  if (hasSeenActive) {
                    return (
                      <StepIndicator
                        availableStages={availableStages}
                        key={stage}
                        name={this.getStageLabel(stage)}
                        isOrderHelpLabel
                      />
                    );
                  }
                  return (
                    <StepIndicator
                      availableStages={availableStages}
                      isComplete
                      stage={stage}
                      onClick={() => {
                        if (
                          (!ordersListItems ||
                            !ordersListItems.orders ||
                            ordersListItems.orders.length <= 1) &&
                          stage === 'select-order'
                        ) {
                          return false;
                        }
                        return moveToCustomerHelpStage(stage);
                      }}
                      name={this.getStageLabel(stage)}
                      clickable
                      isOrderHelpLabel
                    />
                  );
                })}
            </ul>
          </Col>
        </Row>
      </div>
    );
  }
}

export default withStyles(CustomerHelpProgressIndicator, styles);
export { CustomerHelpProgressIndicator as CustomerHelpProgressIndicatorVanilla };
