// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import EmailConfirmationContainer from '../EmailConfirmation.container';

describe('EmailConfirmationContainer', () => {
  const props = {
    onSubmit: jest.fn(),
    labels: {},
    formErrorMessage: {},
    resetEmailConfirmation: jest.fn(),
  };
  it('should render correctly', () => {
    const component = shallow(<EmailConfirmationContainer {...props} />);
    expect(component).toMatchSnapshot();
  });
});
