import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import colors from '@tcp/core/styles/themes/TCP/colors';
import { BodyCopy, Button, Image } from '../../../../../../common/atoms';
import { getIconPath, getLabelValue, smoothScrolling } from '../../../../../../../utils';
import Separator from '../../../../../../common/atoms/Separator';
import withStyles from '../../../../../../common/hoc/withStyles';
import styles from '../styles/MergeAccountConfirmation.styles';
import CustomerServiceHelp from '../../TellUsMore/organisms/CustomerServiceHelp';
import MERGE_ACCOUNT_CONSTANTS from '../../MergeAccounts/MergeAccounts.constants';
import { routerReplace } from '../../../../../../../utils/utils.web';
import { CUSTOMER_HELP_ROUTES } from '../../../CustomerHelp.constants';
import { MERGE_ACCOUNT_ANALYTICS } from '../../MergeAccounts/organisms/MergeAccountsOtp/MergeAccountsOtp.constants';
import { convertHtml } from '../../../../../CnC/LoyaltyBanner/util/utility';

const {
  MERGE_ACCOUNT_TITLE,
  MERGE_SUCCESSFUL,
  MERGE_FAILED,
  MERGE_MANY_ATTEMPTS,
  OTP_FLOW,
  ALTERNATE_FLOW,
} = MERGE_ACCOUNT_ANALYTICS;

const getImageIcon = (mergeError, retryError) => {
  return mergeError || retryError ? 'triangle-alert' : 'circle-green-check';
};

const getMergeAccountLockText = (str, victimEmailAddress) => {
  const string = str.split('&&');
  return `${string[0]}${victimEmailAddress}${string[1]}`;
};

const getMergeAccountSuccessText = (str, victimEmailAddress) => {
  const string = str.split('##');
  return `${string[0]}${victimEmailAddress}${string[1]}`;
};

const getMergeAccountConfirmationText = (str, survivorEmailAddress, victimEmailAddress) => {
  const string = str.split('##');
  const str1 = `${string[0]}<span style="color:${
    colors.TEXT.BLUE
  };">${victimEmailAddress.toLowerCase()}</span>${string[1]}`;

  const str2 = str1.split('&&');
  return `${str2[0]}<span style="color:${
    colors.TEXT.BLUE
  };">${survivorEmailAddress.toLowerCase()}</span>${str2[1]}`;
};

const getHeadingDesc = (
  mergeError,
  retryError,
  labels,
  survivorEmailAddress,
  victimEmailAddress
) => {
  if (retryError) {
    return getMergeAccountLockText(
      getLabelValue(labels, 'lbl_merge_account_lock_text'),
      victimEmailAddress
    );
  }
  if (mergeError) {
    return convertHtml(
      getMergeAccountConfirmationText(
        getLabelValue(labels, 'lbl_merge_account_merge_fail_text'),
        survivorEmailAddress,
        victimEmailAddress
      )
    );
  }
  return convertHtml(
    getMergeAccountConfirmationText(
      getLabelValue(labels, 'lbl_merge_account_merge_success_text'),
      survivorEmailAddress,
      victimEmailAddress
    )
  );
};

const navigateToAccountPage = (
  mergeAccountReset,
  resetEmailConfirmation,
  setSelectedAccountsReasonCode
) => {
  mergeAccountReset();
  resetEmailConfirmation();
  setSelectedAccountsReasonCode(null);
  routerReplace(
    CUSTOMER_HELP_ROUTES.ACCOUNT_COUPONS.to,
    CUSTOMER_HELP_ROUTES.ACCOUNT_COUPONS.asPath
  );
};

const getHeadingText = (mergeError, retryError, labels) => {
  if (retryError) {
    return getLabelValue(labels, 'lbl_merge_incorrect_attempts_heading');
  }
  if (mergeError) {
    return getLabelValue(labels, 'lbl_merge_fail_heading');
  }
  return getLabelValue(labels, 'lbl_merge_success_heading');
};

const renderMergeFailureText = (
  className,
  labels,
  isLiveChatEnabled,
  isInternationalShipping,
  mergeAccountReset,
  resetEmailConfirmation,
  setSelectedAccountsReasonCode
) => {
  return (
    <div className={`${className} customer-service-help`}>
      <BodyCopy component="p" fontSize="fs16" fontWeight="bold" fontFamily="secondary">
        {getLabelValue(labels, 'lbl_merge_customer_service_text')}
      </BodyCopy>
      <CustomerServiceHelp labels={labels} isInternationalShipping={isInternationalShipping} />
      <BodyCopy component="div" className={`${className} help-btn`}>
        <Button
          className="footer-button "
          fontSize="fs14"
          fontWeight="extrabold"
          buttonVariation="variable-width"
          fill="WHITE"
          onClick={() =>
            navigateToAccountPage(
              mergeAccountReset,
              resetEmailConfirmation,
              setSelectedAccountsReasonCode
            )
          }
        >
          {getLabelValue(labels, 'lbl_merge_something_else_btn')}
        </Button>
      </BodyCopy>
    </div>
  );
};

const handleBrowserBack = (mergeAccountReset) => {
  window.onpopstate = () => {
    mergeAccountReset();
    routerReplace('/customer-help', '/customer-help');
  };
};

const renderMergeSuccessText = (className, labels, victimEmailAddress) => {
  return (
    <>
      <BodyCopy component="div" className={`${className} info`}>
        <BodyCopy
          fontSize="fs11"
          fontWeight="bold"
          className="merge-success-detail"
          fontFamily="secondary"
        >
          {getMergeAccountSuccessText(
            getLabelValue(labels, 'lbl_merge_success_text'),
            victimEmailAddress
          )}
        </BodyCopy>
      </BodyCopy>
      <Separator right="-15" />
    </>
  );
};

const analyticsCall = (path, mergeFromOtpFlow, trackAnalyticsClick, clearMergeAccountFromOTP) => {
  const clickPath = mergeFromOtpFlow
    ? `${MERGE_ACCOUNT_TITLE}|${OTP_FLOW}`
    : `${MERGE_ACCOUNT_TITLE}|${ALTERNATE_FLOW}`;

  if (!clickPath.includes(undefined)) {
    trackAnalyticsClick({
      orderHelp_path_cd128: `${clickPath}|${path}`,
    });
    clearMergeAccountFromOTP(false);
  }
};

const MergeAccountConfirmation = ({
  className,
  labels,
  isLiveChatEnabled,
  mergeAccountReset,
  accountStatus,
  mergeApiError,
  mergeStatus,
  victimEmailAddress,
  survivorEmailAddress,
  resetEmailConfirmation,
  isInternationalShipping,
  setSelectedAccountsReasonCode,
  trackAnalyticsClick,
  mergeFromOtpFlow,
  clearMergeAccountFromOTP,
}) => {
  const [mergeError, setMergeError] = useState(false);
  const [retryError, setRetryError] = useState(false);

  useEffect(() => {
    smoothScrolling('customer-help-support');
    return () => {
      mergeAccountReset();
      resetEmailConfirmation();
    };
  }, []);
  useEffect(() => {
    const isSuccess =
      mergeApiError === null && mergeStatus === MERGE_ACCOUNT_CONSTANTS.MERGE_STATUS.MERGE_SUCCESS;
    if (accountStatus === MERGE_ACCOUNT_CONSTANTS.ACCOUNT_STATUS[1] && mergeApiError === null) {
      setRetryError(true);
      setMergeError(false);
      analyticsCall(
        `${MERGE_MANY_ATTEMPTS}`,
        mergeFromOtpFlow,
        trackAnalyticsClick,
        clearMergeAccountFromOTP
      );
    }
    if (mergeApiError || mergeStatus === MERGE_ACCOUNT_CONSTANTS.MERGE_STATUS.MERGE_FAILED) {
      setRetryError(false);
      setMergeError(true);
      analyticsCall(
        `${MERGE_FAILED}`,
        mergeFromOtpFlow,
        trackAnalyticsClick,
        clearMergeAccountFromOTP
      );
    }
    if (isSuccess) {
      analyticsCall(
        `${MERGE_SUCCESSFUL}`,
        mergeFromOtpFlow,
        trackAnalyticsClick,
        clearMergeAccountFromOTP
      );
    }
  }, [accountStatus, mergeApiError, mergeStatus]);

  useEffect(() => {
    handleBrowserBack(mergeAccountReset);
  }, []);

  return (
    <>
      <Separator right="-15" />
      <BodyCopy component="div" className={`${className} info-container`}>
        <BodyCopy className="elem-mb-SM img-container">
          <Image
            alt="close"
            src={getIconPath(getImageIcon(mergeError, retryError))}
            height="40px"
            width="40px"
          />
        </BodyCopy>
        <BodyCopy
          fontSize="fs22"
          fontWeight="extrabold"
          fontFamily="secondary"
          className={`elem-mb-SM refund-successful  ${
            mergeError || retryError ? 'failure-text' : 'success-text'
          }`}
        >
          {getHeadingText(mergeError, retryError, labels)}
        </BodyCopy>
        <BodyCopy
          fontSize="fs12"
          fontWeight="semibold"
          className="merge-success-text"
          fontFamily="secondary"
        >
          {getHeadingDesc(mergeError, retryError, labels, survivorEmailAddress, victimEmailAddress)}
        </BodyCopy>
      </BodyCopy>
      <Separator right="-15" />
      {mergeError || retryError
        ? renderMergeFailureText(
            className,
            labels,
            isLiveChatEnabled,
            isInternationalShipping,
            mergeAccountReset,
            resetEmailConfirmation,
            setSelectedAccountsReasonCode
          )
        : renderMergeSuccessText(className, labels, victimEmailAddress)}
    </>
  );
};

MergeAccountConfirmation.propTypes = {
  className: PropTypes.string,
  labels: PropTypes.shape({}),
  isLiveChatEnabled: PropTypes.bool.isRequired,
  mergeAccountReset: PropTypes.func.isRequired,
  resetEmailConfirmation: PropTypes.func.isRequired,
  accountStatus: PropTypes.string,
  mergeApiError: PropTypes.shape({}),
  mergeStatus: PropTypes.string,
  victimEmailAddress: PropTypes.string,
  survivorEmailAddress: PropTypes.string,
  trackAnalyticsClick: PropTypes.func.isRequired,
};

MergeAccountConfirmation.defaultProps = {
  className: '',
  labels: {},
  mergeStatus: '',
  mergeApiError: {},
  accountStatus: '',
  victimEmailAddress: '',
  survivorEmailAddress: '',
};

export default withStyles(MergeAccountConfirmation, styles);
export { MergeAccountConfirmation as MergeAccountConfirmationVanilla };
