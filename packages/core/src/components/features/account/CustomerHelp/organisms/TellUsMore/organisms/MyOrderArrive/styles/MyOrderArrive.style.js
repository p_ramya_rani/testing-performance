import { css } from 'styled-components';

const styles = css`
  .msg-container {
    background-color: ${(props) => props.theme.colors.PRIMARY.COLOR1};
    padding: ${(props) => props.theme.spacing.ELEM_SPACING.REG}
      ${(props) => props.theme.spacing.ELEM_SPACING.MED_1};
  }

  .msg-container-title {
    color: ${(props) => props.theme.colorPalette.green[600]};
  }

  .msg-container-title2 {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  }
  .order-link {
    cursor: pointer;
  }
  &.parent {
    width: 100%;
  }

  .separator-second {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      display: none;
    }
  }
  .separator-first {
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      display: none;
    }
  }

  .processed-order-desc {
    color: ${(props) => props.theme.colorPalette.gray[800]};
  }
  .rcdate-text {
    color: ${(props) => props.theme.colorPalette.green[600]};
  }
  .msg-container-description2 {
    color: ${(props) => props.theme.colorPalette.gray[800]};
  }
`;

export default styles;
