// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { setClickAnalyticsData, trackPageView, trackClick } from '@tcp/core/src/analytics/actions';
import { setMergeAccountButtonLoader } from '../../../container/MergeAccounts.actions';
import {
  getMergeAccountButtonLoader,
  getShowRecaptcha,
} from '../../../container/MergeAccounts.selectors';
import AccountConfirmationView from '../views/AccountConfirmation.view';
import { accountConfirmationFailure, validateMember } from './AccountConfirmation.actions';
import { getMergeAccountConfirmationError } from './AccountConfirmation.selectors';
import { getFormValidationErrorMessages } from '../../../../../../Account/container/Account.selectors';

const AccountConfirmation = ({
  labels,
  handleValidateMemberAndMerge,
  setShowOtp,
  mergeAccountButtonLoader,
  accountConfirmationError,
  setAccountConfirmationFailure,
  showRecaptcha,
  formErrorMessage,
  trackAnalyticsPageView,
  trackAnalyticsClick,
}) => (
  <AccountConfirmationView
    labels={labels}
    onSubmit={handleValidateMemberAndMerge}
    setShowOtp={setShowOtp}
    mergeAccountButtonLoader={mergeAccountButtonLoader}
    setMergeAccountButtonLoader={setMergeAccountButtonLoader}
    accountConfirmationError={accountConfirmationError}
    setAccountConfirmationFailure={setAccountConfirmationFailure}
    showRecaptcha={showRecaptcha}
    formErrorMessage={formErrorMessage}
    trackAnalyticsPageView={trackAnalyticsPageView}
    trackAnalyticsClick={trackAnalyticsClick}
  />
);

AccountConfirmation.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  handleValidateMemberAndMerge: PropTypes.func.isRequired,
  setShowOtp: PropTypes.func.isRequired,
  mergeAccountButtonLoader: PropTypes.bool.isRequired,
  accountConfirmationError: PropTypes.shape({}).isRequired,
  setAccountConfirmationFailure: PropTypes.shape({}).isRequired,
  showRecaptcha: PropTypes.bool.isRequired,
  formErrorMessage: PropTypes.shape({}).isRequired,
};

const mapStateToProps = (state) => {
  return {
    mergeAccountButtonLoader: getMergeAccountButtonLoader(state),
    accountConfirmationError: getMergeAccountConfirmationError(state),
    showRecaptcha: getShowRecaptcha(state),
    formErrorMessage: getFormValidationErrorMessages(state),
  };
};

const mapDispatchToProps = (dispatch, { survivorEmailAddress, victimEmailAddress }) => {
  return {
    handleValidateMemberAndMerge: (formData) => {
      dispatch(validateMember({ ...formData, survivorEmailAddress, victimEmailAddress }));
    },
    setMergeAccountButtonLoader: (loaderState) =>
      dispatch(setMergeAccountButtonLoader(loaderState)),
    setAccountConfirmationFailure: (payload) => dispatch(accountConfirmationFailure(payload)),
    trackAnalyticsPageView: (eventData, payload) => {
      const loadtimer = setTimeout(() => {
        dispatch(setClickAnalyticsData(eventData));
        // Delayed setting of data when redirection on click event
        clearTimeout(loadtimer);
      }, 300);
      const timer = setTimeout(() => {
        dispatch(trackPageView(payload));
        clearTimeout(timer);
      }, 800);
    },
    trackAnalyticsClick: (eventData, payload) => {
      dispatch(setClickAnalyticsData(eventData));
      const timer = setTimeout(() => {
        dispatch(trackClick(payload));
        clearTimeout(timer);
      }, 800);
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AccountConfirmation);
