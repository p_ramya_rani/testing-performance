// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

const ContainerSkeleton = styled.View`
  position: relative;
  display: flex;
  padding-bottom: ${props => props.theme.spacing.ELEM_SPACING.XS};
  margin: ${props => props.theme.spacing.ELEM_SPACING.XS}
    ${props => props.theme.spacing.ELEM_SPACING.XS};
`;

export default ContainerSkeleton;
