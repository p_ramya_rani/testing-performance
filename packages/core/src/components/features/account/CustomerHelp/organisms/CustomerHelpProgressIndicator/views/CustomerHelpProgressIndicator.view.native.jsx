// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { PropTypes } from 'prop-types';
import { View } from 'react-native';
import { getLocator } from '@tcp/core/src/utils';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import Anchor from '../../../../../../common/atoms/Anchor';

import {
  StepIndicatorContainer,
  ProgressStep,
  ProgressDot,
  ProgressBar,
  ProgressDotIcon,
  CheckoutProgressBar,
  ProgressDotActive,
  StyledAnchor,
  StyledAnchorCompleted,
} from '../../../../../CnC/Checkout/molecules/CheckoutProgressIndicator/styles/CheckoutProgressIndicator.style.native';
import {
  ProgressBarContainer,
  StyledDisableLabels,
} from '../style/CustomerHelpProgressIndicator.style';
import { CUSTOMER_HELP_PAGE_TEMPLATE } from '../../../CustomerHelp.constants';

const completedStage = require('../../../../../../../../../mobileapp/src/assets/images/circle-check-fill-black.png');

const CUSTOMERHELP_STAGE_PROP_TYPE = PropTypes.oneOf(
  Object.keys(CUSTOMER_HELP_PAGE_TEMPLATE).map(key => CUSTOMER_HELP_PAGE_TEMPLATE[key])
);

export class CustomerHelpProgressIndicator extends React.PureComponent {
  getStageLabel = stage => {
    const { labels } = this.props;
    const customerHelpProgressBarLabels = {
      'select-order-label': getLabelValue(labels, 'lbl_order_help'),
      'tell-us-more-label': getLabelValue(labels, 'lbl_tell_us_more_add'),
      'review-label': getLabelValue(labels, 'lbl_submit_add'),
    };
    return customerHelpProgressBarLabels[`${stage}-label`];
  };

  moveToStageAfterSingleOrderCheck = (
    ordersListItems,
    stage,
    moveToCustomerHelpStage,
    navigation
  ) => {
    if (
      (!ordersListItems || !ordersListItems.orders || ordersListItems.orders.length <= 1) &&
      stage === CUSTOMER_HELP_PAGE_TEMPLATE.SELECT_ORDER
    ) {
      return false;
    }
    return moveToCustomerHelpStage(stage, true, navigation);
  };

  render() {
    const {
      activeStage,
      availableStages,
      moveToCustomerHelpStage,
      ordersListItems,
      navigation,
    } = this.props;
    let hasSeenActive = false;
    return (
      <ProgressBarContainer>
        <CheckoutProgressBar>
          <StepIndicatorContainer>
            {availableStages?.map((stage, index) => {
              if (availableStages[index].toLowerCase() === activeStage.toLowerCase()) {
                hasSeenActive = true;
                return (
                  <>
                    {index === availableStages.length - 1 ? (
                      <View>
                        <ProgressDotActive />
                        <StyledDisableLabels
                          accessible
                          accessibilityRole="link"
                          accessibilityLabel={`Active stage ${this.getStageLabel(stage)}`}
                        >
                          {this.getStageLabel(stage)}
                        </StyledDisableLabels>
                      </View>
                    ) : (
                      <ProgressStep>
                        <ProgressDotActive />
                        <ProgressBar />
                        <StyledAnchor index={index}>
                          <Anchor
                            fontSizeVariation="large"
                            fontFamily="secondary"
                            anchorVariation="primary"
                            fontWeightVariation="extrabold"
                            onPress={() => {
                              this.moveToStageAfterSingleOrderCheck(
                                ordersListItems,
                                stage,
                                moveToCustomerHelpStage,
                                navigation
                              );
                            }}
                            dataLocator=""
                            text={this.getStageLabel(stage)}
                            accessible
                            accessibilityRole="link"
                            accessibilityLabel={`Active stage ${this.getStageLabel(stage)}`}
                          />
                        </StyledAnchor>
                      </ProgressStep>
                    )}
                  </>
                );
              }
              if (hasSeenActive) {
                return (
                  <>
                    {index === availableStages.length - 1 ? (
                      <View>
                        <ProgressDot />
                        <StyledDisableLabels
                          accessible
                          accessibilityRole="link"
                          accessibilityLabel={`Pending stage ${this.getStageLabel(stage)}`}
                        >
                          {this.getStageLabel(stage)}
                        </StyledDisableLabels>
                      </View>
                    ) : (
                      <ProgressStep>
                        <ProgressDot />
                        {index !== availableStages.length - 1 && <ProgressBar />}
                        <StyledDisableLabels
                          accessible
                          accessibilityRole="link"
                          accessibilityLabel={`Pending stage ${this.getStageLabel(stage)}`}
                        >
                          {this.getStageLabel(stage)}
                        </StyledDisableLabels>
                      </ProgressStep>
                    )}
                  </>
                );
              }
              return (
                <ProgressStep>
                  <Anchor
                    onPress={() =>
                      this.moveToStageAfterSingleOrderCheck(
                        ordersListItems,
                        stage,
                        moveToCustomerHelpStage,
                        navigation
                      )
                    }
                  >
                    <ProgressDotIcon
                      source={completedStage}
                      data-locator={getLocator('global_headerpanelbagicon')}
                    />
                  </Anchor>
                  <ProgressBar />
                  <StyledAnchorCompleted index={index}>
                    <Anchor
                      fontSizeVariation="large"
                      fontFamily="secondary"
                      anchorVariation="primary"
                      fontWeightVariation="extrabold"
                      onPress={() => moveToCustomerHelpStage(stage, true, navigation)}
                      dataLocator=""
                      text={this.getStageLabel(stage)}
                      accessible
                      accessibilityRole="link"
                      accessibilityLabel={`Completed stage ${this.getStageLabel(stage)}`}
                    />
                  </StyledAnchorCompleted>
                </ProgressStep>
              );
            })}
          </StepIndicatorContainer>
        </CheckoutProgressBar>
      </ProgressBarContainer>
    );
  }
}
CustomerHelpProgressIndicator.propTypes = {
  activeStage: CUSTOMERHELP_STAGE_PROP_TYPE.isRequired,
  moveToCustomerHelpStage: PropTypes.func.isRequired,
  availableStages: PropTypes.arrayOf(CUSTOMERHELP_STAGE_PROP_TYPE).isRequired,
  labels: PropTypes.shape({}).isRequired,
  ordersListItems: PropTypes.shape([]),
  navigation: PropTypes.shape({}).isRequired,
};
CustomerHelpProgressIndicator.defaultProps = {
  ordersListItems: [],
};

export default CustomerHelpProgressIndicator;
