// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

const NoOrderContainer = styled.View`
  display: flex;
  align-items: center;
  justify-content: center;
  margin: ${props => props.theme.spacing.ELEM_SPACING.MED};
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.XXXL};
`;

const LabelContainer = styled.View`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  align-items: center;
  justify-content: center;
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.LRG};
`;

export { LabelContainer, NoOrderContainer };
