/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Button, BodyCopy, Image, Anchor } from '@tcp/core/src/components/common/atoms';
import SpinnerOverlay from '@tcp/core/src/components/common/atoms/SpinnerOverlay';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { getIconPath, getLabelValue, getSiteId, isCanada } from '@tcp/core/src/utils/utils';
import { DeliveredKey } from '@tcp/core/src/services/abstractors/account/ordersList.js';
import CUSTOMER_HELP_CONSTANTS, {
  CUSTOMER_HELP_ROUTES,
  REASON_CODES,
} from '../../../CustomerHelp.constants';
import { routeToPage, getCalculatedRefund } from '../../../util/utility';
import styles from '../style/ReviewDetails.style';
import ReviewItemsList from '../organisms/ReviewItemList';
import RefundSummaryDetails from '../organisms/RefundSummary';
import PaymentDetails from '../organisms/PaymentDetails';
import { KEY_CODES } from '../../../../../../../constants/keyboard.constants';
import ModuleX from '../../../../../../common/molecules/ModuleX';
import { convertHtml } from '../../../../../CnC/LoyaltyBanner/util/utility';
import PrintComponent from '../../../../../../common/atoms/PrintComponent';
import { smoothScrolling } from '../../../../../../../utils';
import { OrderReturnReceiptContainer } from '../../../../Orders/molecules/OrderReturnReceipt/container/OrderReturnReceipt.container';

/**
 * This function component use for return the ReviewDetailsView
 * can be passed in the component.
 * @param orderDetailsData - orderDetailsData object used for showing Order Details
 */
const { RETURN_METHOD } = CUSTOMER_HELP_CONSTANTS;

const onSubmitRefund = (props, Payload) => {
  let refundPayload = Payload;
  const { submitRefund, action, selectedOrderArriveReasonCodeAction } = props;
  if (action === REASON_CODES.WHEN_WILL_MY_ORDER_ARRIVE && selectedOrderArriveReasonCodeAction) {
    refundPayload = {
      ...refundPayload,
      reasonCode: selectedOrderArriveReasonCodeAction,
    };
  }
  submitRefund(refundPayload);
};

const initiateAction = (props) => {
  const {
    orderDetailsData,
    selectedOrderItems,
    loggedInUserEmail,
    isLoggedIn,
    wrongItemData = {},
    action,
    getShipmentRefundStatus,
    returnMethod,
    initiateReturnAction,
    setReturnedOrderItems,
  } = props;
  let refundPayload;
  const { orderNumber, orderItems } = orderDetailsData;
  const items = [];
  const returnItems = [];
  const emailId = isLoggedIn ? loggedInUserEmail : orderDetailsData.emailAddress || '';
  orderItems.forEach((purchasedItem) => {
    purchasedItem.items.forEach((item) => {
      const { lineNumber: updatedLineNumber, trackingInfo } = item;
      const {
        productInfo: { lineNumber },
      } = item;
      const isSelectedOrderItem = selectedOrderItems[updatedLineNumber] > 0;
      if (isSelectedOrderItem) {
        if (action === REASON_CODES.INITIATE_RETURN) {
          returnItems.push({
            packageTrackingNumber:
              trackingInfo &&
              trackingInfo.find((trkng) => trkng.status === DeliveredKey)?.trackingNbr,
            orderLineNumber: lineNumber,
            returnQuantity: selectedOrderItems[updatedLineNumber],
          });
        } else
          items.push({
            lineNumber,
            refundQty: selectedOrderItems[updatedLineNumber],
          });
      }
    });
  });

  if (action === REASON_CODES.INITIATE_RETURN) {
    const returnPayload = {
      orderId: orderNumber,
      reasonCode: action,
      returnMethod,
      emailId,
      itemDetails: returnItems,
    };

    setReturnedOrderItems(orderItems);
    return initiateReturnAction(returnPayload);
  }
  refundPayload = {
    orderId: orderNumber,
    emailId,
    reasonCode: action,
  };

  if (action === REASON_CODES.PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME) {
    getShipmentRefundStatus(refundPayload);
  } else {
    refundPayload = {
      ...refundPayload,
      items,
      ...wrongItemData,
    };

    onSubmitRefund(props, refundPayload);
  }

  return null;
};

export const getReturnItemsHeading = (labels, returnMethod) => {
  if (returnMethod === RETURN_METHOD.IN_STORE) {
    return getLabelValue(labels, 'lbl_review_in_store_heading');
  }
  if (returnMethod === RETURN_METHOD.BY_MAIL) {
    return getLabelValue(labels, 'lbl_review_by_mail_heading');
  }
  return null;
};

export const getReturnItemsDescription = (labels, returnMethod) => {
  if (returnMethod === RETURN_METHOD.IN_STORE) {
    return getLabelValue(labels, 'lbl_review_in_store_desc');
  }
  if (returnMethod === RETURN_METHOD.BY_MAIL) {
    return getLabelValue(labels, 'lbl_review_by_mail_desc');
  }
  return null;
};

const tabIndex = 0;
const role = 'button';

const getInteractiveProps = (onClick, onKeyDown) => {
  return {
    ...(onClick && { onClick, onKeyDown, tabIndex, role }),
  };
};

const IconWithText = ({
  title,
  description,
  image,
  imgWidth,
  imgHeight,
  imgSpacing = '',
  textSpacing,
  titleFontSize,
  titleFontWeight,
  onClick,
  descAsHTML,
}) => {
  const onKeyDown = (e) => {
    return e.keyCode === KEY_CODES.KEY_ENTER && onClick(e);
  };
  const interactiveProps = getInteractiveProps(onClick, onKeyDown);

  return (
    <div className="return-method-container" {...interactiveProps}>
      <Image
        width={imgWidth || '26px'}
        height={imgHeight || imgWidth || '26px'}
        src={getIconPath(image)}
        alt="return-method-icon"
        className={imgSpacing}
      />
      <div className={`return-method-text-container ${textSpacing || 'elem-pl-MED'} `}>
        <BodyCopy
          component="p"
          textAlign="left"
          fontWeight={titleFontWeight || 'bold'}
          fontSize={titleFontSize || 'fs14'}
          fontFamily="secondary"
          color="text.dark"
          className="elem-mb-XXS"
        >
          {title}
        </BodyCopy>
        {description && (
          <BodyCopy
            component="p"
            textAlign="left"
            fontWeight="semibold"
            fontSize="fs12"
            fontFamily="secondary"
            color="text.dark"
            className="elem-pr-MED"
          >
            {descAsHTML ? convertHtml(description) : description}
          </BodyCopy>
        )}
      </div>
    </div>
  );
};

const renderInStoreReturnMethod = (labels) => {
  return (
    <>
      <IconWithText
        image="fav-store-icon"
        title={getLabelValue(labels, 'lbl_returns_in_store_title')}
        description={getLabelValue(labels, 'lbl_returns_in_store_desc')}
      />
      <div className="elem-ml-XXL elem-mt-REG hide-on-mobile">
        <Anchor target="_blank" to="/store-locator" className="find-store-cta">
          <IconWithText
            image="marker-icon"
            imgWidth="20px"
            textSpacing="elem-ml-XS_6"
            title={getLabelValue(labels, 'lbl_review_find_a_store')}
            titleFontWeight="semibold"
          />
        </Anchor>
      </div>
    </>
  );
};

const renderByMailReturnMethod = (labels) => {
  const mailingAddress = isCanada()
    ? getLabelValue(labels, 'lbl_returns_mailing_address_ca')
    : getLabelValue(labels, 'lbl_returns_mailing_address_us');
  return (
    <>
      <IconWithText
        image="fast-shipping"
        title={getLabelValue(labels, 'lbl_returns_by_mail_title')}
      />
      <div className="elem-ml-XXL elem-mt-XXXS elem-mb-SM">
        <IconWithText
          image="place-bucks"
          imgWidth="22px"
          title={getLabelValue(labels, 'lbl_returns_by_mail_fees_apply')}
          titleFontSize="fs12"
          textSpacing="elem-pl-XS"
        />
      </div>
      <IconWithText
        image="marker-icon"
        imgWidth="22px"
        imgSpacing="elem-pt-XXS elem-mr-XXXS elem-ml-XXXS"
        title={getLabelValue(labels, 'lbl_returns_mailing_address')}
        descAsHTML
        description={mailingAddress}
      />
    </>
  );
};

export const renderReturnMethod = (labels, returnMethod) => {
  return (
    <div className="row-background">
      <BodyCopy component="div">
        <div className="return-method-heading-container">
          <BodyCopy
            fontSize="fs16"
            fontWeight="extrabold"
            fontFamily="secondary"
            className="elem-mb-XS"
            color="black"
          >
            {getLabelValue(labels, 'lbl_review_return_method')}
          </BodyCopy>
          {returnMethod === RETURN_METHOD.IN_STORE && (
            <Anchor
              target="_blank"
              to="/store-locator"
              className="find-store-cta hide-on-tablet hide-on-desktop"
            >
              <IconWithText
                image="marker-icon"
                imgWidth="22px"
                textSpacing="elem-ml-XS_6"
                title={getLabelValue(labels, 'lbl_review_find_a_store')}
                titleFontWeight="semibold"
              />
            </Anchor>
          )}
        </div>
        <BodyCopy className=" title-underline" />
      </BodyCopy>
      {returnMethod === RETURN_METHOD.IN_STORE && renderInStoreReturnMethod(labels)}
      {returnMethod === RETURN_METHOD.BY_MAIL && renderByMailReturnMethod(labels)}
    </div>
  );
};

const getOrderStatus = (action, labels) => {
  return action === REASON_CODES.CANCEL_MODIFY
    ? getLabelValue(labels, 'lbl_cancelation_summary')
    : getLabelValue(labels, 'lbl_refund_summary');
};

const getOrderStatusCancelItems = (action, labels) => {
  switch (action) {
    case REASON_CODES.CANCEL_MODIFY:
      return getLabelValue(labels, 'lbl_items_to_be_canceled');
    case REASON_CODES.INITIATE_RETURN:
      return getLabelValue(labels, 'lbl_items_to_be_returned');
    default:
      return getLabelValue(labels, 'lbl_items_to_be_refunded');
  }
};

const getOrderPaymentDetails = (action, labels, returnMethod) => {
  const reviewHref = `/${getSiteId()}${getLabelValue(
    labels,
    'lbl_authorization_hold_href',
    'orders'
  )}`;
  return action === REASON_CODES.CANCEL_MODIFY ? (
    <>
      <a className="blue-link" href={reviewHref} rel="noopener noreferrer" target="_blank">
        {getLabelValue(labels, 'lbl_auth_hold_policies')}
      </a>
      &nbsp;
      {getLabelValue(labels, 'lbl_auth_hold_policies1')}
    </>
  ) : (
    (action === REASON_CODES.INITIATE_RETURN &&
      returnMethod === RETURN_METHOD.IN_STORE &&
      getLabelValue(labels, 'lbl_payment_details_in_store_msg')) ||
      getLabelValue(labels, 'lbl_payment_details_msg')
  );
};

const handleSubmit = ({
  action,
  submitRefund,
  orderDetailsData,
  selectedOrderItems,
  loggedInUserEmail,
  isLoggedIn,
  orderNumber,
  getCancelStatus,
  wrongItemData,
  getShipmentRefundStatus,
  summary,
  getSavedSummary,
  returnMethod,
  initiateReturnAction,
  setReturnedOrderItems,
  selectedOrderArriveReasonCodeAction,
}) => {
  if (action === REASON_CODES.CANCEL_MODIFY) {
    getSavedSummary(summary);
    getCancelStatus({ orderNumber, loggedInUserEmail });
  } else {
    initiateAction({
      submitRefund,
      orderDetailsData,
      selectedOrderItems,
      loggedInUserEmail,
      isLoggedIn,
      wrongItemData,
      action,
      getShipmentRefundStatus,
      returnMethod,
      initiateReturnAction,
      setReturnedOrderItems,
      selectedOrderArriveReasonCodeAction,
    });
  }
};

export const renderPaymentDetailView = (
  orderDetailsData,
  orderLabels,
  action,
  labels,
  returnMethod
) => {
  return (
    orderDetailsData &&
    Object.keys(orderDetailsData).length > 0 && (
      <div className="row-background row-margin">
        <PaymentDetails
          labels={labels}
          orderDetailsData={orderDetailsData}
          ordersLabels={orderLabels && orderLabels.orders}
          orderStatus={getOrderPaymentDetails(action, labels, returnMethod)}
          returnMethod={returnMethod}
          showCardDetails={false}
        />
      </div>
    )
  );
};

const renderRefundSummary = (
  orderDetailsData,
  orderLabels,
  labels,
  selectedOrderItems,
  orderItems,
  isReturnItemsCase,
  ...others
) => {
  if (isReturnItemsCase) return null;
  const [action, summary, isPaidAndShippingCase] = others;
  if (orderDetailsData && Object.keys(orderDetailsData).length) {
    const { currencySymbol, shippingTotal } = summary || {};
    const { totalBeforeTax, tax, refundTotal, itemsCount } = getCalculatedRefund(
      selectedOrderItems,
      orderItems,
      '',
      '',
      summary
    );

    const isCancelOrderPage = action === REASON_CODES.CANCEL_MODIFY;
    return (
      <div className="row-background">
        <RefundSummaryDetails
          orderDetailsData={orderDetailsData}
          orderLabels={orderLabels && orderLabels.orders}
          labels={labels}
          totalBeforeTax={totalBeforeTax}
          tax={tax}
          refundTotal={refundTotal}
          itemsCount={itemsCount}
          currencySymbol={currencySymbol}
          orderStatus={getOrderStatus(action, labels)}
          isCancelOrderPage={isCancelOrderPage}
          shippingTotal={shippingTotal}
          isPaidAndShippingCase={isPaidAndShippingCase}
          isReviewPage
        />
      </div>
    );
  }

  return null;
};

export const renderInstructionsModuleX = (
  isReturnItemsCase,
  labels,
  resolutionModuleXContent,
  returnMethod,
  returnInStoreModuleXContent,
  returnByMailModuleXContent
) => {
  return (
    isReturnItemsCase && (
      <div className="return-instructions-container">
        <BodyCopy component="div">
          {Object.keys(resolutionModuleXContent).length > 0 &&
            returnMethod === RETURN_METHOD.IN_STORE && (
              <ModuleX richTextList={[{ text: returnInStoreModuleXContent.richText }]} />
            )}
          {Object.keys(resolutionModuleXContent).length > 0 &&
            returnMethod === RETURN_METHOD.BY_MAIL && (
              <ModuleX richTextList={[{ text: returnByMailModuleXContent.richText }]} />
            )}
        </BodyCopy>
      </div>
    )
  );
};

const getCTALabel = (isReturnItemsCase, labels, orderLabels) => {
  return isReturnItemsCase
    ? getLabelValue(labels, 'lbl_returns_initiate_return')
    : getLabelValue(orderLabels && orderLabels.orders, 'lbl_review_submit');
};

const refundOrderDetails = (orderDetailsData) => {
  if (orderDetailsData && Object.keys(orderDetailsData).length) {
    return true;
  }
  return false;
};

const getEmail = (isLoggedIn, loggedInUserEmail, orderDetailsData) =>
  isLoggedIn ? loggedInUserEmail : orderDetailsData.emailAddress || '';

const getReviewMessage = (action, labels, isReturnItemsCase, returnMethod) => {
  if (action === 'CANCEL_MODIFY') {
    return (
      <BodyCopy
        component="div"
        fontSize="fs16"
        fontWeight="bold"
        fontFamily="secondary"
        color="black"
      >
        <>
          {getLabelValue(labels, 'lbl_auth_hold_policies2')}
          &nbsp;
          <a
            className="blue-link"
            rel="noopener noreferrer"
            target="_blank"
            href={`/${getSiteId()}${getLabelValue(labels, 'lbl_authorization_hold_href')}`}
          >
            {getLabelValue(labels, 'lbl_auth_hold_policies3', 'orders')}
          </a>
          &nbsp;
          {getLabelValue(labels, 'lbl_auth_hold_policies4')}
        </>
      </BodyCopy>
    );
  }
  if (action === REASON_CODES.RECEIVED_WRONG_ITEM) {
    return (
      <BodyCopy
        component="div"
        fontSize="fs16"
        fontWeight="bold"
        fontFamily="secondary"
        color="black"
      >
        {getLabelValue(labels, 'lbl_wrong_item_msg')}
      </BodyCopy>
    );
  }

  if (isReturnItemsCase) {
    return (
      <BodyCopy
        component="div"
        fontSize="fs14"
        fontWeight="semibold"
        fontFamily="secondary"
        color="text.dark"
      >
        {getReturnItemsDescription(labels, returnMethod)}
      </BodyCopy>
    );
  }

  return (
    <BodyCopy
      component="div"
      fontSize="fs16"
      fontWeight="bold"
      fontFamily="secondary"
      color="black"
    >
      {getLabelValue(labels, 'lbl_delivered_not_received_msg')}
    </BodyCopy>
  );
};

const renderTriggerButton = (labels) => {
  return (
    <Button className="print-receipts-btn" onClick={() => {}} type="button" link>
      {getLabelValue(labels, 'lbl_returns_print_receipts')}
    </Button>
  );
};

export const renderReturnItemsHeading = (
  labels,
  returnMethod,
  orderDetailsData,
  orderLabels,
  isLoggedIn,
  orderReturnDays,
  userAccountId
) => {
  return (
    <div className="return-method-heading-container" id="return-method-heading-container">
      <BodyCopy
        fontSize={['fs14', 'fs14', 'fs16']}
        fontWeight="extrabold"
        fontFamily="secondary"
        className="elem-mb-XS"
        color="black"
      >
        {getReturnItemsHeading(labels, returnMethod)}
      </BodyCopy>
      {orderDetailsData && (
        <PrintComponent
          ComponentToPrint={OrderReturnReceiptContainer}
          compProps={{
            orderDetailsData,
            ordersLabels: orderLabels?.orders,
            orderReturnDays,
            userAccountId,
            isLoggedIn,
          }}
          displayContentBeforePrint={false}
          renderTriggerButton={() => renderTriggerButton(labels)}
        />
      )}
    </div>
  );
};

const backButtonClicked = (setReturnItemDisplay, clearReturnMethod) => {
  setReturnItemDisplay(false);
  clearReturnMethod();
  routeToPage(CUSTOMER_HELP_ROUTES.TELL_US_MORE);
};

const Review = ({
  orderDetailsData,
  className,
  orderLabels,
  labels,
  selectedOrderItems,
  selectedReasonCode,
  wrongItemData,
  isFetching,
  returnInStoreModuleXContent,
  returnByMailModuleXContent,
  resolutionModuleXContent,
  initiateReturnAction,
  selectedReturnMethod: returnMethod,
  setReturnedOrderItems,
  setReturnItemDisplay,
  clearReturnMethod,
  selectedOrderArriveReasonCode,
  ...otherPros
}) => {
  const { summary = {}, orderItems, orderNumber } = orderDetailsData || {};
  const { currencySymbol } = summary;
  const { action } = selectedReasonCode || {};
  const { action: selectedOrderArriveReasonCodeAction } = selectedOrderArriveReasonCode || {};
  const {
    submitRefund,
    loggedInUserEmail,
    isLoggedIn,
    getCancelStatus,
    getShipmentRefundStatus,
    getSavedSummary,
  } = otherPros;

  useEffect(() => {
    smoothScrolling('return-method-heading-container');
  }, []);

  const isPaidAndShippingCase = action === REASON_CODES.PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME;
  const isReturnItemsCase = action === REASON_CODES.INITIATE_RETURN;

  return (
    <div className={className}>
      {!isFetching ? (
        <>
          {refundOrderDetails(orderDetailsData) && (
            <>
              <div className="review-table-background">
                <Row fullBleed>
                  <Col colSize={{ large: 8, medium: 4, small: 6 }}>
                    <div className="row-background review-details">
                      <BodyCopy component="div" className={className}>
                        {!isReturnItemsCase && (
                          <BodyCopy
                            fontSize="fs16"
                            fontWeight="extrabold"
                            fontFamily="secondary"
                            className="elem-mb-XS"
                            color="black"
                          >
                            {getLabelValue(labels, 'lbl_review_details')}
                          </BodyCopy>
                        )}
                        {isReturnItemsCase && renderReturnItemsHeading(labels, returnMethod)}
                        <BodyCopy className=" title-underline" />
                        {getReviewMessage(action, labels, isReturnItemsCase, returnMethod)}
                        {renderInstructionsModuleX(
                          isReturnItemsCase,
                          labels,
                          resolutionModuleXContent,
                          returnMethod,
                          returnInStoreModuleXContent,
                          returnByMailModuleXContent
                        )}
                        {!isReturnItemsCase && (
                          <BodyCopy
                            component="div"
                            className="review-basic-details review-details-text"
                            fontSize="fs16"
                            fontWeight="semiBold"
                            fontFamily="secondary"
                            color="black"
                          >
                            {getLabelValue(labels, 'lbl_review_details_msg2')}
                          </BodyCopy>
                        )}
                      </BodyCopy>
                    </div>
                    {action !== REASON_CODES.PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME && (
                      <div className="row-background row-margin">
                        <BodyCopy
                          fontSize="fs16"
                          fontWeight="extrabold"
                          fontFamily="secondary"
                          className="elem-mb-XS"
                          color="black"
                        >
                          {getOrderStatusCancelItems(action, labels)}
                        </BodyCopy>
                        <BodyCopy
                          className={`title-underline ${
                            isReturnItemsCase ? 'underline-bottom-margin' : ''
                          }`}
                        />
                        {isReturnItemsCase && (
                          <BodyCopy
                            fontSize="fs14"
                            fontWeight="semibold"
                            fontFamily="secondary"
                            className="elem-mb-XXS"
                          >
                            {`${getLabelValue(labels, 'lbl_review_order_no')}${orderNumber}`}
                          </BodyCopy>
                        )}
                        {Array.isArray(orderItems) &&
                          orderItems.map((orderGroup, index) => (
                            <ReviewItemsList
                              key={index.toString()}
                              orderLabels={orderLabels}
                              items={orderGroup.items}
                              currencySymbol={currencySymbol}
                              isEcol
                              labels={labels}
                              action={action}
                              selectedOrderItems={selectedOrderItems}
                              orderStatus={getOrderStatusCancelItems(action, labels)}
                            />
                          ))}
                      </div>
                    )}
                  </Col>
                  <Col colSize={{ large: 4, medium: 4, small: 6 }} className="inner-summary">
                    {renderRefundSummary(
                      orderDetailsData,
                      orderLabels,
                      labels,
                      selectedOrderItems,
                      orderItems,
                      isReturnItemsCase,
                      action,
                      summary,
                      isPaidAndShippingCase
                    )}
                    {isReturnItemsCase && renderReturnMethod(labels, returnMethod)}
                    {renderPaymentDetailView(
                      orderDetailsData,
                      orderLabels,
                      action,
                      labels,
                      returnMethod
                    )}
                  </Col>
                </Row>
              </div>

              <div className={className}>
                <Row fullBleed>
                  <Col colSize={{ large: 12, medium: 8, small: 6 }}>
                    <div className="review-button">
                      <div className="review-submit-align back-btn">
                        <Button
                          className="footer-button review-footer"
                          fontSize="fs14"
                          fontWeight="extrabold"
                          buttonVariation="variable-width"
                          fill="WHITE"
                          onClick={() => backButtonClicked(setReturnItemDisplay, clearReturnMethod)}
                        >
                          {getLabelValue(
                            orderLabels && orderLabels.orders,
                            'lbl_orderDetails_back'
                          )}
                        </Button>
                      </div>
                      <div className="review-submit-align">
                        <Button
                          className="footer-button review-footer"
                          fontSize="fs14"
                          fontWeight="extrabold"
                          buttonVariation="variable-width"
                          fill="BLUE"
                          onClick={() => {
                            handleSubmit({
                              action,
                              submitRefund,
                              orderDetailsData,
                              selectedOrderItems,
                              loggedInUserEmail: getEmail(
                                isLoggedIn,
                                loggedInUserEmail,
                                orderDetailsData
                              ),
                              isLoggedIn,
                              orderNumber,
                              getCancelStatus,
                              wrongItemData,
                              getShipmentRefundStatus,
                              summary,
                              getSavedSummary,
                              returnMethod,
                              initiateReturnAction,
                              setReturnedOrderItems,
                              selectedOrderArriveReasonCodeAction,
                            });
                          }}
                        >
                          {getCTALabel(isReturnItemsCase, labels, orderLabels)}
                        </Button>
                      </div>
                    </div>
                  </Col>
                </Row>
              </div>
            </>
          )}
        </>
      ) : (
        <SpinnerOverlay />
      )}
    </div>
  );
};

Review.propTypes = {
  className: PropTypes.string,
  orderDetailsData: PropTypes.shape({}),
  orderLabels: PropTypes.shape({}).isRequired,
  labels: PropTypes.shape({}),
  selectedOrderItems: PropTypes.shape({}),
  selectedReasonCode: PropTypes.shape({}),
  wrongItemData: PropTypes.shape({}),
  isFetching: PropTypes.bool,
  setReturnItemDisplay: PropTypes.func,
  clearReturnMethod: PropTypes.func,
  selectedOrderArriveReasonCode: PropTypes.shape({}),
};

Review.defaultProps = {
  className: '',
  orderDetailsData: {},
  labels: {},
  selectedOrderItems: {},
  selectedReasonCode: {},
  wrongItemData: {},
  isFetching: false,
  setReturnItemDisplay: () => {},
  clearReturnMethod: () => {},
  selectedOrderArriveReasonCode: {},
};

export default withStyles(Review, styles);
export { Review as ReviewVanilla };
