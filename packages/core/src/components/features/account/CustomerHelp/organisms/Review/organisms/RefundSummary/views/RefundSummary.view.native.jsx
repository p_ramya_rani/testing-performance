// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { BodyCopy } from '@tcp/core/src/components/common/atoms';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import { ViewWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';
import formatAmount from '../../../../../../OrderDetails/organism/OrderSummaryDetails/utils';

import { RowContainer, BlueUnderLine } from '../../../../../styles/CustomerHelp.style.native';

/**
 * This function component use for return the OrderShippingDetails
 * can be passed in the component.
 * @param orderLabels - ordersLabels object used for showing Orders Labels
 */

const RefundSummaryDetails = ({
  orderLabels,
  orderDetailsData,
  labels,
  totalBeforeTax,
  tax,
  refundTotal,
  itemsCount,
  orderStatus,
  isCancelOrderPage,
  shippingTotal,
  isReviewPage,
  isPaidAndShippingCase,
}) => {
  const { summary } = orderDetailsData || {};
  const { currencySymbol } = summary || {};
  /**
   * @function return  Used to render the JSX of the component
   * @param    {[Void]} function does not accept anything.
   * @return   {[Object]} JSX of the component
   */
  return (
    <ViewWithSpacing spacingStyles="margin-top-MED">
      <BodyCopy
        fontSize="fs16"
        fontWeight="extrabold"
        fontFamily="secondary"
        className="elem-mb-XS"
        color="black"
        text={orderStatus}
      />
      <BlueUnderLine />

      {!isPaidAndShippingCase ? (
        <>
          <RowContainer>
            <BodyCopy
              text={`${getLabelValue(orderLabels, 'lbl_orders_items')} (${itemsCount}):`}
              fontFamily="secondary"
              fontSize="fs14"
              color="black"
            />
            <BodyCopy
              text={formatAmount(totalBeforeTax, currencySymbol)}
              fontFamily="secondary"
              fontSize="fs14"
              textAlign="right"
              color="black"
            />
          </RowContainer>
          <RowContainer>
            <BodyCopy
              text={`${getLabelValue(labels, 'lbl_total_before_tax')}:`}
              fontFamily="secondary"
              fontSize="fs14"
              color="black"
            />
            <BodyCopy
              text={formatAmount(totalBeforeTax, currencySymbol)}
              fontFamily="secondary"
              fontSize="fs14"
              textAlign="right"
              color="black"
            />
          </RowContainer>
          {isCancelOrderPage && (
            <RowContainer>
              <BodyCopy
                text={`${getLabelValue(labels, 'lbl_shipping')}:`}
                fontFamily="secondary"
                fontSize="fs14"
                color="black"
              />
              <BodyCopy
                text={formatAmount(shippingTotal, currencySymbol)}
                fontFamily="secondary"
                fontSize="fs14"
                textAlign="right"
                color="black"
              />
            </RowContainer>
          )}
          <RowContainer>
            <BodyCopy
              text={`${getLabelValue(orderLabels, 'lbl_orders_tax')}:`}
              fontFamily="secondary"
              fontSize="fs14"
              color="black"
            />
            <BodyCopy
              text={formatAmount(tax, currencySymbol)}
              fontFamily="secondary"
              fontSize="fs14"
              textAlign="right"
              color="black"
            />
          </RowContainer>
        </>
      ) : (
        <>
          <RowContainer>
            <BodyCopy
              fontFamily="secondary"
              fontSize="fs14"
              color="black"
              text={`${getLabelValue(labels, 'lbl_shipping')}:`}
            />
            <BodyCopy
              fontFamily="secondary"
              fontSize="fs14"
              textAlign="right"
              color="black"
              text={
                isReviewPage
                  ? formatAmount(shippingTotal, currencySymbol)
                  : formatAmount(totalBeforeTax, currencySymbol)
              }
            />
          </RowContainer>
          {!isReviewPage && (
            <RowContainer>
              <BodyCopy
                fontFamily="secondary"
                fontSize="fs14"
                color="black"
                text={`${getLabelValue(orderLabels, 'lbl_orders_tax')}:`}
              />
              <BodyCopy
                fontFamily="secondary"
                fontSize="fs14"
                textAlign="right"
                color="black"
                text={formatAmount(tax, currencySymbol)}
              />
            </RowContainer>
          )}
        </>
      )}

      <RowContainer>
        <BodyCopy
          text={`${
            isCancelOrderPage
              ? getLabelValue(labels, 'lbl_order_total')
              : getLabelValue(labels, 'lbl_refund_total')
          }:`}
          fontSize="fs14"
          fontWeight="extrabold"
          fontFamily="secondary"
          color="black"
        />
        <BodyCopy
          text={
            isPaidAndShippingCase && isReviewPage
              ? formatAmount(shippingTotal, currencySymbol)
              : formatAmount(refundTotal, currencySymbol)
          }
          fontSize="fs18"
          fontWeight="extrabold"
          fontFamily="secondary"
          textAlign="right"
          color="black"
        />
      </RowContainer>
    </ViewWithSpacing>
  );
};
RefundSummaryDetails.propTypes = {
  orderLabels: PropTypes.shape({
    lbl_orderDetails_orderSummary: PropTypes.string,
  }),
  orderDetailsData: PropTypes.shape({}),
  labels: PropTypes.shape({}),
  selectedOrderItems: PropTypes.shape({}),
  totalBeforeTax: PropTypes.number,
  tax: PropTypes.number,
  refundTotal: PropTypes.number,
  itemsCount: PropTypes.number,
  orderStatus: PropTypes.string,
  isCancelOrderPage: PropTypes.bool,
  shippingTotal: PropTypes.number,
  isReviewPage: PropTypes.bool,
  isPaidAndShippingCase: PropTypes.bool,
};

RefundSummaryDetails.defaultProps = {
  orderLabels: {
    lbl_orderDetails_orderSummary: '',
  },
  orderDetailsData: {},
  labels: {},
  selectedOrderItems: {},
  totalBeforeTax: {},
  tax: {},
  refundTotal: {},
  itemsCount: {},
  orderStatus: '',
  isCancelOrderPage: false,
  shippingTotal: {},
  isReviewPage: false,
  isPaidAndShippingCase: false,
};

export default RefundSummaryDetails;
