// 9fbef606107a605d69c0edbcd8029e5d
import { call, takeLatest, put } from 'redux-saga/effects';
import { AccountConfirmationSaga, validateMember } from '../AccountConfirmation.saga';
import ACCOUNT_CONFIRMATION_CONSTANTS from '../../AccountConfirmation.constants';
import { mergeAccountValidateMember } from '../../../../../../../../../../services/abstractors/customerHelp/customerHelp';
import { accountConfirmationFailure } from '../AccountConfirmation.actions';
import { setAccountStatus, setNumberOfRetries } from '../../../../container/MergeAccounts.actions';

describe('MergeAccount Account confirmation saga', () => {
  let gen;
  const payload = {
    firstName: 'test',
    lastName: 'test',
    noCountryZip: '10001',
    survivorEmailAddress: 'test@test.com',
    victimEmailAddress: 'test@test2.com',
  };

  beforeEach(() => {
    gen = validateMember({ payload });
  });

  it('should return correct takeLatest effect', () => {
    gen = AccountConfirmationSaga();
    expect(gen.next().value).toEqual(
      takeLatest(ACCOUNT_CONFIRMATION_CONSTANTS.VALIDATE_MEMBER, validateMember)
    );
    expect(gen.next().done).toBeTruthy();
  });

  it('should test accountconfirmation failure', () => {
    const res = {
      status: 'SUCCESS',
      timestamp: '2021-09-06T08:56:34.528516',
      data: {
        status: 'INVALID',
        email: 'test@test.com',
        numberOfRetries: 2,
        accountStatus: 0,
      },
    };
    const { firstName, lastName, noCountryZip: zipCode, victimEmailAddress: emailId } = payload;
    gen.next();
    expect(gen.next(res).value).toEqual(
      call(mergeAccountValidateMember, {
        firstName,
        lastName,
        zipCode,
        emailId,
      })
    );
    expect(gen.next(res).value).toEqual(put(setNumberOfRetries(2)));
    expect(gen.next(res).value).toEqual(put(setAccountStatus('UNLOCKED')));
    expect(gen.next(res).value).toEqual(put(accountConfirmationFailure('INVALID')));
    gen.next();
    expect(gen.next().done).toBeTruthy();
  });
});
