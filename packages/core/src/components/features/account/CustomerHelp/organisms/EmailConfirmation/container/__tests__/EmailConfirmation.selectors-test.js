// 9fbef606107a605d69c0edbcd8029e5d
import { fromJS } from 'immutable';
import {
  emailConfirmationRequestLoader,
  getEmailConfirmationFailure,
  getEmailConfirmationSuccess,
} from '../EmailConfirmation.selector';

describe('Merge account email confirmation selector', () => {
  const emailConfirmationState = fromJS({
    isFetching: false,
    error: null,
    emailConfirmationSuccess: null,
  });

  const state = {
    /* reducer-key:state */
    MergeAccountEmailAddressConfirmationReducer: emailConfirmationState,
  };

  it('Merge account email confirmation should return error state', () => {
    expect(getEmailConfirmationFailure(state)).toEqual(emailConfirmationState.get('error'));
  });

  it('Merge account email confirmation should return success state', () => {
    expect(getEmailConfirmationSuccess(state)).toEqual(
      emailConfirmationState.get('emailConfirmationSuccess')
    );
  });

  it('Merge account email confirmation should return loading state', () => {
    expect(emailConfirmationRequestLoader(state)).toEqual(emailConfirmationState.get('isFetching'));
  });
});
