// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { OrderTileVanilla } from '../OrderTileSkeleton.view';

describe('OrderTileSkeleton component', () => {
  it('should renders correctly', () => {
    const props = {
      className: 'sample-class',
    };
    const component = shallow(<OrderTileVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
