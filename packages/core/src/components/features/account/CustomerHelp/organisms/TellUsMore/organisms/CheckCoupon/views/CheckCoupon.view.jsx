// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { reduxForm, Field } from 'redux-form';
import {
  couponFieldLength,
  getFormattedValue,
} from '@tcp/core/src/components/features/CnC/common/molecules/CouponForm/CouponForm.utils';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { getIconPath } from '@tcp/core/src/utils/';
import CouponIcon from '@tcp/core/src/components/features/account/common/molecule/CouponIcon';
import Separator from '@tcp/core/src/components/common/atoms/Separator';
import CustomerServiceHelp from '@tcp/core/src/components/features/account/CustomerHelp/organisms/TellUsMore/organisms/CustomerServiceHelp';
import { COUPON_STATUS } from '@tcp/core/src/services/abstractors/CnC/CartItemTile';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import styles from '../styles/CheckCoupon.styles';
import { BodyCopy, TextBox, Button, Row, Col, Image } from '../../../../../../../../common/atoms';
import CouponDetailModal from '../../../../../../../CnC/common/organism/CouponAndPromos/views/CouponDetailModal.view';
import CouponDetails from '../../CouponDetails';

class CheckCoupon extends React.PureComponent {
  constructor(props) {
    super(props);
    this.overlayTimeout = this.getOverlayTimeout();
    this.overlayRef = React.createRef();
    this.state = {
      openDetailModal: false,
      showOverlay: false,
      alreadyApplied: false,
      successCheck: false,
    };
  }

  componentDidMount() {
    const { resetCoupon, setCouponError } = this.props;
    resetCoupon();
    setCouponError('');
  }

  componentDidUpdate(prevProps) {
    const { coupon, applyCouponLoader, couponDetailsLoader, appliedCouponList } = this.props;
    const { alreadyApplied, successCheck } = this.state;
    if (prevProps.applyCouponLoader && !applyCouponLoader) {
      if (appliedCouponList.find((cpn) => cpn.id.includes(coupon?.id))) this.showHideOverlay();
      else if (alreadyApplied) this.setAlreadyApplied(false);
    }
    if (prevProps.couponDetailsLoader && !couponDetailsLoader) {
      if (coupon && !successCheck) {
        this.setState({ successCheck: true });
      } else if (!coupon && successCheck) {
        this.setState({ successCheck: false });
      }
      if (appliedCouponList.find((cpn) => cpn.id.includes(coupon?.id)))
        this.setAlreadyApplied(true);
      else if (alreadyApplied) this.setAlreadyApplied(false);
    }
  }

  onInputChange = (e, newVal) => {
    const { coupon } = this.props;
    const { successCheck } = this.state;
    if (coupon && newVal === coupon.id && successCheck === false) {
      this.setState({
        successCheck: true,
      });
    } else if (successCheck === true) {
      this.setState({
        successCheck: false,
      });
    }
  };

  getOverlayTimeout = () => {
    const { labels } = this.props;
    const timeout = parseInt(getLabelValue(labels, 'lbl_csh_overlay_timeout', 'orders'), 10);
    return timeout ? timeout * 1000 : 5000;
  };

  setAlreadyApplied = (isAlreadyApplied) => {
    this.setState({ alreadyApplied: isAlreadyApplied });
  };

  setOverlay = (show, callback) => {
    return callback
      ? this.setState({ showOverlay: show }, callback)
      : this.setState({ showOverlay: show });
  };

  showHideOverlay = () => {
    this.setOverlay(true, () => this.overlayRef?.current?.focus());
    const timer = setTimeout(() => {
      this.setOverlay(false);
      clearTimeout(timer);
    }, this.overlayTimeout);
  };

  couponDetailClick = () => {
    this.setState({
      openDetailModal: true,
    });
  };

  resetCouponForm = () => {
    const { reset, resetCoupon, setCouponError } = this.props;
    setCouponError('');
    resetCoupon();
    reset();
    this.setState({
      successCheck: false,
    });
  };

  handleSubmit = (e) => {
    e.stopPropagation();
    const { handleSubmit, resetCoupon, setCouponError } = this.props;
    setCouponError('');
    resetCoupon();
    return handleSubmit(e);
  };

  renderOverlay = (coupon) => {
    const { commonLabels: labels } = this.props;
    const { showOverlay } = this.state;
    const bagIcon = getIconPath('shopping-bag-overlay');
    const closeIcon = getIconPath('white-cross-icon');
    const show = showOverlay && coupon?.status === COUPON_STATUS.APPLIED;
    return (
      show && (
        <div
          tabIndex={-1}
          data-locator="coupon-overlay-csh"
          className="overlay"
          ref={this.overlayRef}
          onBlur={() => this.setOverlay(false)}
        >
          <BodyCopy component="div" textAlign="center" className="top-content">
            <CouponIcon coupon={coupon} labels={labels} className="elem-mb-LRG" />
            <BodyCopy
              fontSize="fs16"
              fontFamily="secondary"
              fontWeight="extrabold"
              title={coupon.title}
              className="elem-mb-MED_1"
              textAlign="center"
              data-locator="myrewards-details"
              color="white"
            >
              {coupon.title}
            </BodyCopy>
          </BodyCopy>
          <BodyCopy
            component="div"
            fontSize="fs16"
            fontWeight="extrabold"
            fontFamily="secondary"
            color="white"
            data-locator="coupon-overlay-icon"
            className="overlay__content"
          >
            <Image className="bag_icon" src={bagIcon} alt={labels.lbl_bag_icon} />
            <BodyCopy
              component="p"
              className="elem-mt-LRG"
              fontSize="fs16"
              fontWeight="extrabold"
              fontFamily="secondary"
              textAlign="center"
              color="white"
              data-locator="coupon-overlay-info-csh"
            >
              {getLabelValue(labels, 'lbl_common_applied_to_bag')}
            </BodyCopy>
          </BodyCopy>
          <BodyCopy
            onClick={() => this.setOverlay(false)}
            component="div"
            data-locator="overlay-close-icon"
            className="overlay-close-btn"
          >
            <Image src={closeIcon} alt="close-overlay" />
          </BodyCopy>
        </div>
      )
    );
  };

  renderTextBox = ({ input, ...otherParams }) => {
    const { successCheck } = this.state;
    const formattedValue = input.value ? getFormattedValue(input.value) : '';
    const formattedInput = { ...input, value: formattedValue };
    return <TextBox input={formattedInput} {...otherParams} showSuccessCheck={successCheck} />;
  };

  renderCustomerServiceHelp = (coupon, couponData) => {
    const {
      labels,
      isLiveChatEnabled,
      isInternationalShipping,
      isLoggedIn,
      loggedInUserEmail,
      trackAnalyticsClick,
    } = this.props;
    const selectedReasonCode = {
      action: 'COUPON_ISSUE',
    };
    const orderDetailsData = {
      emailAddress: isLoggedIn ? loggedInUserEmail : '',
    };
    return (
      coupon?.error && (
        <>
          <Separator />
          <Row fullBleed>
            <Col colSize={{ small: 12 }}>
              <BodyCopy
                component="p"
                fontWeight="semibold"
                fontSize="fs16"
                fontFamily="secondary"
                color="black"
                className="elem-pl-XS elem-pr-XS elem-mt-LRG"
              >
                {getLabelValue(labels, 'lbl_csh_coupon_customer_care', 'orders')}
              </BodyCopy>
            </Col>
          </Row>
          <CustomerServiceHelp
            labels={labels}
            isLiveChatEnabled={isLiveChatEnabled}
            isInternationalShipping={isInternationalShipping}
            couponData={couponData}
            selectedReasonCode={selectedReasonCode}
            orderDetailsData={orderDetailsData}
            trackAnalyticsClick={trackAnalyticsClick}
          />
        </>
      )
    );
  };

  render() {
    const {
      className,
      pristine,
      labels,
      coupon,
      couponDetailsLoader,
      applyCouponLoader,
      onApply,
      onRemove,
      appliedCouponList,
      couponError,
      onHoldCouponList,
    } = this.props;

    const isButtonDisabled = () => pristine || couponDetailsLoader;

    let couponData =
      (coupon && appliedCouponList.find((cpn) => cpn.id.includes(coupon.id))) || coupon;

    if (onHoldCouponList?.size > 0) {
      couponData = (coupon && onHoldCouponList.find((cpn) => cpn.id.includes(coupon.id))) || coupon;
    }
    const { openDetailModal, alreadyApplied, successCheck } = this.state;

    return (
      <>
        <BodyCopy component="div" className={`${className} container layout-pt-XS `}>
          <BodyCopy component="div" className="container-check-coupon">
            <Row fullBleed>
              <Col colSize={{ small: 6, medium: 7, large: 12 }}>
                <BodyCopy component="div" className="top-section elem-mb-REG">
                  <div>
                    <BodyCopy
                      component="p"
                      fontWeight="bold"
                      fontFamily="secondary"
                      fontSize="fs16"
                    >
                      {getLabelValue(labels, 'lbl_csh_check_coupon_heading', 'orders')}
                    </BodyCopy>
                    <BodyCopy className="title-underline elem-mt-XXXS elem-mb-XXXS" />
                  </div>
                  {((coupon && coupon.title) || couponError) && (
                    <Button
                      className="try-another-coupon"
                      onClick={this.resetCouponForm}
                      nohover
                      type="button"
                      link
                      underline
                    >
                      {getLabelValue(labels, 'lbl_csh_try_another_coupon', 'orders')}
                    </Button>
                  )}
                </BodyCopy>
              </Col>
            </Row>
            {this.renderOverlay(couponData)}
            <BodyCopy component="div" className="middle-section">
              <BodyCopy
                component="p"
                fontSize={['fs14', 'fs10', 'fs14']}
                fontWeight="semibold"
                fontFamily="secondary"
                className="elem-mb-LRG description"
              >
                {getLabelValue(labels, 'lbl_csh_check_coupon_description', 'orders')}
              </BodyCopy>
              <form name="checkCouponForm" onSubmit={this.handleSubmit}>
                <Field
                  meta={{
                    asyncValidating: !successCheck,
                  }}
                  id="couponCode"
                  component={this.renderTextBox}
                  type="text"
                  placeholder={getLabelValue(labels, 'lbl_csh_check_coupon_placeholder', 'orders')}
                  name="couponCode"
                  maxLength={couponFieldLength}
                  className="coupon_code_input input-field"
                  dataLocator="coupon_code"
                  autocomplete="off"
                  onChange={this.onInputChange}
                />
                <Button
                  fill="BLUE"
                  type="submit"
                  buttonVariation="variable-width"
                  loading={couponDetailsLoader}
                  className="elem-mt-MED_1 check-coupon-button"
                  disabled={isButtonDisabled()}
                  couponError={couponError}
                  coupon={coupon}
                >
                  {getLabelValue(labels, 'lbl_csh_validate', 'orders')}
                </Button>
              </form>
              {((coupon && coupon.title) || couponError) && (
                <CouponDetails
                  labels={labels}
                  isFetching={applyCouponLoader}
                  coupon={couponData}
                  couponError={couponError}
                  couponDetailClick={this.couponDetailClick}
                  onApply={onApply}
                  onRemove={onRemove}
                  ref={null}
                  couponsOnHold={false}
                  alreadyApplied={alreadyApplied}
                />
              )}
            </BodyCopy>
            {this.renderCustomerServiceHelp(coupon, couponData)}
            {coupon && (
              <CouponDetailModal
                labels={labels}
                openState={openDetailModal}
                coupon={couponData}
                onApplyCouponToBagFromList={onApply}
                onRequestClose={() => {
                  this.setState({
                    openDetailModal: false,
                  });
                }}
                additionalClassNameModal="coupon-modal-web"
              />
            )}
          </BodyCopy>
        </BodyCopy>
      </>
    );
  }
}
CheckCoupon.propTypes = {
  className: PropTypes.string,
  pristine: PropTypes.bool,
  labels: PropTypes.shape({}).isRequired,
  handleSubmit: PropTypes.func,
  resetCoupon: PropTypes.func,
  setCouponError: PropTypes.func,
  coupon: PropTypes.shape({}).isRequired,
  couponDetailsLoader: PropTypes.bool.isRequired,
  applyCouponLoader: PropTypes.bool.isRequired,
  couponError: PropTypes.string,
  reset: PropTypes.func,
  onApply: PropTypes.func.isRequired,
  onRemove: PropTypes.func.isRequired,
  commonLabels: PropTypes.shape({}).isRequired,
  appliedCouponList: PropTypes.shape([]).isRequired,
  onHoldCouponList: PropTypes.shape([]).isRequired,
  trackAnalyticsClick: PropTypes.func.isRequired,
  isLoggedIn: PropTypes.bool.isRequired,
  loggedInUserEmail: PropTypes.string,
};
CheckCoupon.defaultProps = {
  className: '',
  pristine: false,
  handleSubmit: () => {},
  resetCoupon: () => {},
  setCouponError: () => {},
  reset: () => {},
  couponError: '',
  loggedInUserEmail: '',
};

export default reduxForm({
  form: 'CheckCoupon',
})(withStyles(CheckCoupon, styles));

export { CheckCoupon as CheckCouponVanilla };
