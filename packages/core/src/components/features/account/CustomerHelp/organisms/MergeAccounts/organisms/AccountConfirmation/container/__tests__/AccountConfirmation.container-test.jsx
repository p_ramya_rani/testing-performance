// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import AccountConfirmationContainer from '../AccountConfirmation.container';

describe('AccountConfirmationContainer', () => {
  const props = {
    labels: {},
    setShowOtp: jest.fn(),
    victimEmailAddress: 'victim@test.com',
    survivorEmailAddress: 'survivor@test.com',
  };
  it('should render correctly', () => {
    const component = shallow(<AccountConfirmationContainer {...props} />);
    expect(component).toMatchSnapshot();
  });
});
