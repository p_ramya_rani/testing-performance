// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import { ViewWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';
import OrderItem from '../../../../TellUsMore/organisms/OrderItem';
import { REASON_CODES, CUSTOMER_HELP_PAGE_TEMPLATE } from '../../../../../CustomerHelp.constants';
/**
 * This function component use for return the Review item list based on group
 * can be passed in the component.
 * @param otherProps - otherProps object used for showing Review Item list
 */

const getSuccessScenario = (isSelectedOrderItem, orderStatus, action) => {
  const { CANCEL_MODIFY } = REASON_CODES;

  if (action === CANCEL_MODIFY || orderStatus === 'CANCELED ITEM(S)' || isSelectedOrderItem) {
    return true;
  }
  return false;
};

const ReviewItemsList = ({ className, ...otherProps }) => {
  const {
    items,
    orderLabels,
    currencySymbol,
    selectedOrderItems = {},
    orderStatus,
    action,
    navigation,
  } = otherProps;

  const route = get(navigation, 'state.routeName', false);
  const isConfirmationPage = route === CUSTOMER_HELP_PAGE_TEMPLATE.CONFIRMATION;

  const renderSelectedOrderItem = (itm, index) => {
    const { lineNumber } = itm;

    const isSelectedOrderItem = selectedOrderItems[lineNumber] > 0;

    const item = isSelectedOrderItem
      ? {
          ...itm,
          ...{ itemInfo: { ...itm.itemInfo, quantity: selectedOrderItems[lineNumber] } },
        }
      : itm;

    const isSuccessScenario = getSuccessScenario(isSelectedOrderItem, orderStatus, action);
    return (
      <ViewWithSpacing>
        {isSuccessScenario && (
          <OrderItem
            key={index.toString()}
            {...{ item }}
            currencySymbol={currencySymbol}
            orderLabels={orderLabels && orderLabels.orders}
            noBorder
            noDisabledState={isConfirmationPage}
          />
        )}
      </ViewWithSpacing>
    );
  };

  return (
    <ViewWithSpacing>
      {items.map((item, index) => renderSelectedOrderItem(item, index))}
    </ViewWithSpacing>
  );
};
ReviewItemsList.propTypes = {
  className: PropTypes.string,
  labels: PropTypes.shape({}),
  orderStatus: PropTypes.string,
  navigation: PropTypes.shape({}),
};

ReviewItemsList.defaultProps = {
  className: '',
  labels: {},
  orderStatus: '',
  navigation: {},
};

export default ReviewItemsList;
