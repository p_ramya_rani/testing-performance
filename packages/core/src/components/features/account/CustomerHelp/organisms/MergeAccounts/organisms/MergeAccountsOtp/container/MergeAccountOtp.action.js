import { MERGE_ACCOUNT_OTP_CONSTANTS } from '../MergeAccountsOtp.constants';

export const checkOtpdetails = (payload) => {
  return {
    type: MERGE_ACCOUNT_OTP_CONSTANTS.MERGE_ACCOUNT_VALIDATE_OTP,
    payload,
  };
};

export const setMergeAccountButtonLoader = (payload) => {
  return {
    type: MERGE_ACCOUNT_OTP_CONSTANTS.BTN_LOADER,
    payload,
  };
};

export const setOtpDetail = (payload) => {
  return {
    type: MERGE_ACCOUNT_OTP_CONSTANTS.MERGE_ACCOUNT_OTP_DETAIL,
    payload,
  };
};

export const getOtpdetails = (payload) => {
  return {
    type: MERGE_ACCOUNT_OTP_CONSTANTS.MERGE_ACCOUNT_GET_OTP,
    payload,
  };
};

export const getOtpSuccess = (payload) => ({
  type: MERGE_ACCOUNT_OTP_CONSTANTS.MERGE_ACCOUNT_GET_OTP_SUCCESS,
  payload,
});

export const getOtpFailure = (payload) => ({
  type: MERGE_ACCOUNT_OTP_CONSTANTS.MERGE_ACCOUNT_GET_OTP_FAILURE,
  payload,
});

export const setOtpUniqueKey = (payload) => ({
  type: MERGE_ACCOUNT_OTP_CONSTANTS.MERGE_ACCOUNT_SET_UNIQUE_KEY,
  payload,
});

export const clearOtpData = () => ({
  type: MERGE_ACCOUNT_OTP_CONSTANTS.MERGE_ACCOUNT_CLEAR_OTP_DATA,
});
