import React from 'react';
import { shallow } from 'enzyme';
import { AccountConfirmationFormVanilla } from '../AccountConfirmation.view';
import constants from '../../AccountConfirmation.constants';

describe('AccountConfirmation view', () => {
  const props = {
    labels: {},
    resetEmailConfirmation: jest.fn(),
    setShowOtp: jest.fn(),
    mergeAccountButtonLoader: false,
    setMergeAccountButtonLoader: jest.fn(),
    accountConfirmationError: 'Error',
    setAccountConfirmationFailure: jest.fn(),
    trackAnalyticsClick: jest.fn(),
    handleSubmit: jest.fn(),
    showRecaptcha: true,
    change: jest.fn(),
    pristine: false,
    invalid: false,
  };
  it('should render correct form', () => {
    const tree = shallow(<AccountConfirmationFormVanilla {...props} />);
    expect(tree.find('.form-test-class').prop('name')).toEqual(constants.ACCOUNT_CONFIRMATION_FORM);
  });

  it('simulate form submit ', () => {
    const tree = shallow(<AccountConfirmationFormVanilla {...props} />);
    tree.find('form').simulate('submit', { stopPropagation: jest.fn() });
    props.trackAnalyticsClick.mockImplementation();
    expect(props.handleSubmit).toHaveBeenCalled();
  });
});
