// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  &.container {
    position: relative;
    @media ${(props) => props.theme.mediaQuery.large} {
      padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
    }
  }
  .input-field {
    width: 50%;
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: 100%;
    }
  }
  .email-confirmation-error {
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.REG};
  }
  .button-width {
    width: 166px;
    height: ${(props) => props.theme.spacing.ELEM_SPACING.CREDIT_CARD_ICON_WIDTH};
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      margin-left: auto;
      margin-right: auto;
      display: table;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
    }
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      width: 184px;
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    }
  }
  .middle-section {
    @media ${(props) => props.theme.mediaQuery.mediumOnly} {
      width: 90%;
      .description {
        font-size: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
      }
    }
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      .description {
        font-size: ${(props) => props.theme.spacing.ELEM_SPACING.REG};
      }
    }
  }
`;

export default styles;
