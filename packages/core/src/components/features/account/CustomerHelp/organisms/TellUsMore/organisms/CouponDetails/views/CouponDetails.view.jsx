// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { getViewportInfo, isClient } from '@tcp/core/src/utils';
import { BodyCopy, Button } from '@tcp/core/src/components/common/atoms';
import ErrorMessage from '@tcp/core/src/components/features/CnC/common/molecules/ErrorMessage';
import { parseDate } from '../../../../../../../../../utils/parseDate';
import {
  COUPON_REDEMPTION_TYPE,
  constructDateFormat,
} from '../../../../../../../../../services/abstractors/CnC/CartItemTile';
import styles from '../styles/CouponDetails.style';
import COUPON_SELF_HELP_CONSTANT from '../../CouponSelfHelp/CouponSelfHelp.constants';
import { parseBoolean } from '../../../../../../../../../utils/utils.web';

class CouponDetails extends React.Component {
  RenderCardHeader = (isApplied, labels) => {
    const { alreadyApplied } = this.props;
    return (
      isApplied && (
        <div className="couponCard__header">
          <BodyCopy
            data-locator="coupondetails_header"
            className="couponCard__header_text"
            component="p"
            fontSize="fs10"
            fontFamily="primary"
          >
            {alreadyApplied
              ? getLabelValue(labels, 'lbl_csh_already_applied', 'orders')
              : getLabelValue(labels, 'lbl_csh_applied_to_bag', 'orders')}
          </BodyCopy>
        </div>
      )
    );
  };

  RenderExpiredContent = () => {
    const { labels, coupon } = this.props;
    return (
      <div className="couponCard__notValid__container">
        <ErrorMessage
          fontSize="fs14"
          fontWeight="bold"
          className="expiry-content"
          error={`${coupon?.title}
          ${getLabelValue(labels, 'lbl_csh_err_coupon_expired', 'orders')}
          ${coupon && constructDateFormat(parseDate(coupon?.expirationDate))}`}
          noBackground
        />
      </div>
    );
  };

  RenderUsedCouponError = () => {
    const { labels } = this.props;

    return (
      <div>
        <ErrorMessage
          fontSize="fs14"
          fontWeight="bold"
          error={getLabelValue(
            labels,
            'lbl_errorMessages_TCP_PROMOTION_COUPON_ALREADY_USED',
            'errorMessages'
          )}
          noBackground
        />
      </div>
    );
  };

  RenderCouponError = (couponError) => {
    const { labels } = this.props;
    return couponError === COUPON_SELF_HELP_CONSTANT.USED_ERR_CODE ? (
      this.RenderUsedCouponError()
    ) : (
      <ErrorMessage
        fontSize="fs14"
        fontWeight="bold"
        error={
          couponError === COUPON_SELF_HELP_CONSTANT.INACTIVE_ERR_CODE
            ? getLabelValue(labels, 'lbl_csh_err_coupon_inactive', 'orders')
            : getLabelValue(labels, 'lbl_csh_err_coupon_invalid', 'orders')
        }
        noBackground
      />
    );
  };

  RenderCouponNotValid = (couponError) => {
    return couponError === COUPON_SELF_HELP_CONSTANT.EXPIRED_ERR_CODE
      ? this.RenderExpiredContent()
      : this.RenderCouponError(couponError);
  };

  RenderApplyButton = () => {
    const { coupon, onApply, isFetching } = this.props;
    return (
      <Button
        onClick={() => {
          onApply(coupon);
        }}
        className="coupon__button"
        buttonVariation="variable-width"
        type="submit"
        fill="BLACK"
        loading={isFetching}
        data-locator={`coupon_${coupon.status}_apply_couponDetailsCta`}
        disabled={isFetching || coupon.error}
        aria-labelledby={coupon.id}
        borderRadius={16}
        loaderLeft
      >
        {coupon.labelStatus}
      </Button>
    );
  };

  RenderRemoveButton = () => {
    const { coupon, onRemove, isFetching } = this.props;
    return (
      <Button
        onClick={(e) => {
          e.preventDefault();
          onRemove(coupon);
        }}
        className="coupon__button coupon__button_gray"
        buttonVariation="variable-width"
        type="submit"
        data-locator={`coupon_${coupon.status}_remove_couponDetailsCta`}
        aria-label={`${coupon.labelStatus} ${coupon.id}`}
        disabled={isFetching}
        borderRadius={16}
      >
        {coupon.labelStatus}
      </Button>
    );
  };

  RenderValidityText = (coupon, labels) => {
    return (
      <BodyCopy
        className="couponCard__text_style couponDetails__text_useby"
        data-locator="couponDetailsValidity"
        component="p"
        fontSize="fs14"
        fontFamily="secondary"
        lineHeight="lh115"
      >
        {`${getLabelValue(labels, 'lbl_csh_validity_text', 'orders')} ${coupon.expirationDate}`}
      </BodyCopy>
    );
  };

  RenderButtons = (coupon) => {
    return (
      <div className="couponCard__col">
        {coupon.isOnHold !== 'true' &&
          coupon.status === 'available' &&
          coupon.isStarted &&
          this.RenderApplyButton()}
        {coupon.isOnHold !== 'true' && coupon.status === 'applied' && this.RenderRemoveButton()}
        {coupon.isOnHold === 'true' && this.RenderRemoveButton()}
      </div>
    );
  };

  handleDefaultLinkClick = (event) => {
    event.preventDefault();
    const { coupon, couponDetailClick } = this.props;
    couponDetailClick(coupon);
  };

  render() {
    const { labels, coupon, className, couponError } = this.props;
    const { isDesktop } = (isClient() && getViewportInfo()) || {};
    const fontSize = isDesktop ? 'fs14' : 'fs12';
    let couponApplyError = '';
    if (coupon && parseBoolean(coupon?.isOnHold)) {
      couponApplyError =
        coupon?.originalOfferType === 'PLCB'
          ? labels.onHoldCouponError
          : labels.onHoldAppleCouponError;
    } else {
      couponApplyError = coupon?.error;
    }
    return (
      <div className={`${className}`}>
        {couponError ? (
          this.RenderCouponNotValid(couponError, coupon)
        ) : (
          <>
            <ErrorMessage
              fontSize={fontSize}
              fontWeight="extrabold"
              error={couponApplyError}
              isEspot={
                coupon.offerType === COUPON_REDEMPTION_TYPE.PLACECASH && coupon.error.includes('<') // If it includes a left arrow,it is richtext
              }
              noBackground
              className="couponCard__apply_error"
            />
            <div className="couponCard__container">
              {this.RenderCardHeader(coupon.status === 'applied', labels)}
              <div className="couponCard__body">
                <div className="couponCard__row">
                  <div className="couponCard__col">
                    <>
                      <BodyCopy
                        component="p"
                        fontSize="fs14"
                        lineHeight="lh115"
                        fontWeight="bold"
                        fontFamily="secondary"
                        className="couponTitle"
                        data-locator={`coupon_${coupon.status}_couponNameLbl`}
                      >
                        {coupon.title}
                      </BodyCopy>
                      {this.RenderValidityText(coupon, labels)}
                    </>
                    <Button
                      dataLocator={`coupon_${coupon.status}_couponDetailsLink`}
                      fontSizeVariation="large"
                      underline
                      anchorVariation="primary"
                      nohover
                      type="button"
                      link
                      onClick={this.handleDefaultLinkClick}
                      className="couponDetailsLink"
                      aria-label={`${coupon.id} ${labels.DETAILS_BUTTON_TEXT}`}
                    >
                      {getLabelValue(labels, 'lbl_csh_details', 'orders')}
                    </Button>
                  </div>
                  {this.RenderButtons(coupon)}
                </div>
              </div>
            </div>
          </>
        )}
      </div>
    );
  }
}

CouponDetails.propTypes = {
  coupon: PropTypes.shape({}).isRequired,
  labels: PropTypes.shape({}).isRequired,
  onApply: PropTypes.func.isRequired,
  isFetching: PropTypes.bool.isRequired,
  onRemove: PropTypes.func.isRequired,
  couponDetailClick: PropTypes.func.isRequired,
  className: PropTypes.string.isRequired,
  alreadyApplied: PropTypes.string.isRequired,
  couponError: PropTypes.string.isRequired,
};

export default withStyles(CouponDetails, styles);
export { CouponDetails as CouponDetailsVanilla };
