// 9fbef606107a605d69c0edbcd8029e5d
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { getIconPath, smoothScrolling } from '@tcp/core/src/utils';
import Separator from '@tcp/core/src/components/common/atoms/Separator';
import ReasonCodeSkeleton from '../../TellUsMore/skeleton';
import CustomerServiceHelp from '../../TellUsMore/organisms/CustomerServiceHelp';
import ReasonCodeWithIcon from '../../../molecules/ReasonCodeWithIcon';
import { BodyCopy, Row, Col } from '../../../../../../common/atoms';
import { getLabelValue } from '../../../../../../../utils';
import { CUSTOMER_HELP_ROUTES, REASON_CODES } from '../../../CustomerHelp.constants';
import { routeToPage } from '../../../util/utility';

function CustomerHelpLanding({
  labels,
  selectedTopReasonCode,
  setSelectedTopReasonCode,
  topLevelReasonCodes,
  selectOrderAction,
  isLiveChatEnabled,
  isInternationalShipping,
  setLoader,
  setActiveSection,
  isLoggedIn,
  trackAnalyticsClick,
  loggedInUserEmail,
}) {
  useEffect(() => {
    if (selectedTopReasonCode?.action === REASON_CODES.SOMETHING_ELSE) {
      setTimeout(() => {
        smoothScrolling('customerServiceHelp');
      }, 0);
    }
  }, [selectedTopReasonCode]);

  const navigateToSelectOrder = (e, reason) => {
    e.preventDefault();
    if (isLoggedIn) {
      setLoader(true);
      setSelectedTopReasonCode(reason);
      setActiveSection('select-order');
    }
    selectOrderAction();
  };

  const handlerMap = {
    EXISTING_ORDER: navigateToSelectOrder,
    ACCOUNT_AND_COUPONS: (e, reason) => {
      setSelectedTopReasonCode(reason);
      routeToPage(CUSTOMER_HELP_ROUTES.ACCOUNT_COUPONS);
    },
    SOMETHING_ELSE: (e, reason) => {
      setSelectedTopReasonCode(reason);
    },
  };
  const fallbackImg = getIconPath('essential-style-fallback');
  const helpIcon = getIconPath('question-circle');
  const orderIcon = getIconPath('package');
  const accountIcon = getIconPath('user-icon-black');
  const iconMap = {
    EXISTING_ORDER: orderIcon,
    ACCOUNT_AND_COUPONS: accountIcon,
    SOMETHING_ELSE: helpIcon,
  };

  const renderCustomerHelp = () => {
    const selectedReasonCode = {
      action: REASON_CODES.SOMETHING_ELSE,
    };
    const orderDetailsData = {
      emailAddress: isLoggedIn ? loggedInUserEmail : '',
    };
    return (
      selectedTopReasonCode?.action === REASON_CODES.SOMETHING_ELSE && (
        <div id="customerServiceHelp">
          <Separator right="-15" />
          <CustomerServiceHelp
            labels={labels}
            isLiveChatEnabled={isLiveChatEnabled}
            isInternationalShipping={isInternationalShipping}
            trackAnalyticsClick={trackAnalyticsClick}
            selectedReasonCode={selectedReasonCode}
            orderDetailsData={orderDetailsData}
          />
        </div>
      )
    );
  };

  return !topLevelReasonCodes || !topLevelReasonCodes.length > 0 ? (
    <ReasonCodeSkeleton />
  ) : (
    <>
      {!selectedTopReasonCode && (
        <>
          <Row fullBleed>
            <Col colSize={{ small: 6, medium: 7, large: 12 }}>
              <div className="header-container top-header-container">
                <div className="header-title">
                  <BodyCopy component="p" fontWeight="bold" fontFamily="secondary" fontSize="fs16">
                    {getLabelValue(labels, 'lbl_how_we_can_help', '')}
                  </BodyCopy>
                  <BodyCopy
                    component="p"
                    fontFamily="secondary"
                    fontSize="fs14"
                    className="elem-pl-XS elem-pr-XS select-option"
                  >
                    {getLabelValue(labels, 'lbl_select_an_option', '')}
                  </BodyCopy>
                </div>
              </div>
            </Col>
          </Row>
          <Row fullBleed>
            <Col colSize={{ small: 6, medium: 8, large: 12 }}>
              <Row fullBleed>
                {topLevelReasonCodes.map((reason, index) => {
                  return (
                    <ReasonCodeWithIcon
                      onClick={(e) => {
                        trackAnalyticsClick({
                          orderHelp_path_cd128: reason?.label,
                          orderHelpSelectedReason: reason?.label,
                        });
                        handlerMap[reason.action](e, reason);
                      }}
                      index={index}
                      title={reason.label}
                      description={getLabelValue(
                        labels,
                        `lbl_rc_desc_${reason.action.toLowerCase()}`,
                        ''
                      )}
                      icon={iconMap[reason.action]}
                      fallbackImg={fallbackImg}
                      marginClasses="elem-mt-LRG"
                    />
                  );
                })}
              </Row>
            </Col>
          </Row>
        </>
      )}
      {renderCustomerHelp()}
    </>
  );
}

CustomerHelpLanding.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  selectedTopReasonCode: PropTypes.shape({}).isRequired,
  setSelectedTopReasonCode: PropTypes.func.isRequired,
  topLevelReasonCodes: PropTypes.shape([]).isRequired,
  selectOrderAction: PropTypes.func.isRequired,
  getReasonCodeContentByContentId: PropTypes.func.isRequired,
  isLiveChatEnabled: PropTypes.bool.isRequired,
  setLoader: PropTypes.func.isRequired,
  setActiveSection: PropTypes.func.isRequired,
  isLoggedIn: PropTypes.bool.isRequired,
  trackAnalyticsClick: PropTypes.func.isRequired,
  loggedInUserEmail: PropTypes.string,
};

CustomerHelpLanding.defaultProps = {
  loggedInUserEmail: '',
};
export default CustomerHelpLanding;
