// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import CustomerServiceHelp from '../views/CustomerServiceHelp.view.native';
import * as utils from '../../../../../../../../../utils/utils';
import { CustomerServiceHelpContainer } from '../styles/CustomerServiceHelp.style.native';

const utilsLocation = '@tcp/core/src/utils/utils';
jest.mock('@tcp/core/src/utils/utils');
jest.mock(utilsLocation);
const apiConfig = {
  country: 'US',
};
utils.getAPIConfig = jest.fn(() => {
  return apiConfig;
});

jest.mock('react-native/Libraries/Linking/Linking', () => ({
  openURL: jest.fn(() => Promise.resolve('mockResolve')),
}));

describe('Customer Service Help View', () => {
  const navigation = {
    navigate: () => {},
    getParam: () => {
      return {};
    },
  };
  const email = 'abc@gmail.com';
  const props = {
    labels: {
      lbl_how_to_solve: 'How can we resolve this?',
      lbl_help_call_us_number: 'Call Us',
      lbl_contact_us_tcp_prod_href: 'TCP Contact us',
      lbl_contact_us_gym_prod_href: 'GYM Contact us',
    },
    helpMessage: ' For a faster resolution,Contact us below.',
    selectedReasonCode: 'SOMETHING_ELSE',
    orderDetailsData: { orderNumber: 'R44343434343', emailAddress: email, purchasedItems: [] },
    selectedOrderItems: [123, 456],
    navigation,
    isLiveChatEnabled: false,
    isUserLoggedIn: true,
  };

  it('should render CustomerServiceHelp', () => {
    utils.getBrand = jest.fn().mockReturnValue('gym');
    const component = shallow(<CustomerServiceHelp {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('should render chat now button ', () => {
    utils.getBrand = jest.fn().mockReturnValue('tcp');
    const wrapper = shallow(<CustomerServiceHelp {...props} />);
    expect(wrapper.find(CustomerServiceHelpContainer).props().children).toHaveLength(4);
    expect(wrapper).toMatchSnapshot();
  });
  it('should not render chat now button ', () => {
    const data = { ...props, isLiveChatEnabled: true };
    const wrapper = shallow(<CustomerServiceHelp props={data} />);
    expect(wrapper).toMatchSnapshot();
  });
});
