export const getOtpDetail = (state) => {
  return state.MergeAccountOtp && state.MergeAccountOtp.get('otpDetail');
};

export const getOtpUniqueKey = (state) => {
  return state.MergeAccountOtp && state.MergeAccountOtp.get('uniqueKey');
};

export const getOtpError = (state) => {
  return state.MergeAccountOtp && state.MergeAccountOtp.get('error');
};
