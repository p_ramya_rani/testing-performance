// 9fbef606107a605d69c0edbcd8029e5d
import CUSTOMER_HELP_CONSTANTS, { RETURN_ITEM_CONSTANT } from '../CustomerHelp.constants';

export const getReferredModule = (referred) => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.GET_REFERRED_CONTENT,
    referred,
  };
};

export const getReasonCodes = (payload) => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.GET_REASON_CODES,
    payload,
  };
};

export const setReasonCodes = (payload) => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.SET_REASON_CODES,
    payload,
  };
};

export const setOrderArriveReasonCode = (payload) => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.SET_ORDER_ARRIVE_REASON_CODE,
    payload,
  };
};

export const setOrderArriveDeliveredReasonCode = (payload) => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.SET_ORDER_ARRIVE_DELIVERED_REASON_CODE,
    payload,
  };
};

export const setReferredModule = (content) => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.SET_REFERRED_CONTENT,
    payload: content,
  };
};

export const setSelectedItems = (items) => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.SET_SELECTED_ITEMS,
    payload: items,
  };
};

export const setResolution = (payload) => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.SET_RESOLUTION_CODE,
    payload,
  };
};

export const clearSelectedItems = () => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.CLEAR_SELECTED_ITEMS,
  };
};

export const selectOrder = (payload) => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.SELECT_ORDER,
    payload,
  };
};

export const setReasonCode = (payload) => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.SET_REASON_CODE,
    payload,
  };
};

export const clearSelectedReason = () => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.CLEAR_REASON_CODE,
  };
};

export const clearOrderArriveSelectedReason = () => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.CLEAR_ORDER_ARRIVE_REASON_CODE,
  };
};

export const clearSelectedResolution = () => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.CLEAR_SELECTED_RESOLUTION,
  };
};

export const getItemLevelRefund = (payload) => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.GET_ITEM_LEVEL_REFUND,
    payload,
  };
};

export const getShipmentStatus = (payload) => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.GET_SHIPMENT_STATUS,
    payload,
  };
};

export const setShipmentStatus = (payload) => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.SET_SHIPMENT_STATUS,
    payload,
  };
};

export const setItemLevelRefund = (payload) => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.SET_ITEM_LEVEL_REFUND,
    payload,
  };
};

export const setReturnedOrderItems = (payload) => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.SET_RETURNED_ORDER_ITEMS,
    payload,
  };
};

export const clearReturnedOrderItems = () => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.CLEAR_RETURNED_ORDER_ITEMS,
  };
};

export const setInitiateReturnStatus = (payload) => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.SET_INITIATE_RETURN_STATUS,
    payload,
  };
};

export const clearInitiateReturnStatus = () => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.CLEAR_INITIATE_RETURN_STATUS,
  };
};

export const initiateReturn = (payload) => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.INITIATE_RETURN,
    payload,
  };
};
export const setResolutionModuleX = (payload) => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.SET_RESOLUTION_MODULEX_CONTENT,
    payload,
  };
};

export const setCancelResolutionModuleX = (payload) => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.SET_CANCEL_RESOLUTION_MODULEX_CONTENT,
    payload,
  };
};

export const fetchModifiedResolutionModuleX = (payload) => {
  return {
    payload,
    type: CUSTOMER_HELP_CONSTANTS.FETCH_RESOLUTION_MODULEX_CONTENT,
  };
};

export const fetchCancelledResolutionModuleX = (payload) => {
  return {
    payload,
    type: CUSTOMER_HELP_CONSTANTS.FETCH_CANCEL_MODULEX_CONTENT,
  };
};

export const showLoader = () => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.SHOW_CSH_LOADER,
  };
};

export const hideLoader = () => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.HIDE_CSH_LOADER,
  };
};

export const setWrongItemState = (payload) => {
  return {
    payload,
    type: CUSTOMER_HELP_CONSTANTS.SET_WRONG_ITEM_STATE,
  };
};
export const setTransitResolutionModuleX = (payload) => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.SET_TRANSIT_RESOLUTION_MODULEX_CONTENT,
    payload,
  };
};

export const setReturnInStoreModuleX = (payload) => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.SET_RETURN_IN_STORE_MODULEX_CONTENT,
    payload,
  };
};

export const setReturnByMailModuleX = (payload) => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.SET_RETURN_BY_MAIL_MODULEX_CONTENT,
    payload,
  };
};
export const setReturnPolicyModuleX = (payload) => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.SET_RETURN_POLICY_MODULEX_CONTENT,
    payload,
  };
};

export const fetchTransitHelpModuleX = (payload) => {
  return {
    payload,
    type: CUSTOMER_HELP_CONSTANTS.FETCH_TRANSIT_RESOLUTION_MODULEX_CONTENT,
  };
};

export const fetchReturnInStoreModuleX = (payload) => {
  return {
    payload,
    type: CUSTOMER_HELP_CONSTANTS.FETCH_RETURN_IN_STORE_MODULEX_CONTENT,
  };
};

export const fetchReturnByMailModuleX = (payload) => {
  return {
    payload,
    type: CUSTOMER_HELP_CONSTANTS.FETCH_RETURN_BY_MAIL_MODULEX_CONTENT,
  };
};
export const fetchReturnPolicyModuleX = (payload) => {
  return {
    payload,
    type: CUSTOMER_HELP_CONSTANTS.FETCH_RETURN_POLICY_MODULEX_CONTENT,
  };
};

export const setRefundStatus = (payload) => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.SET_REFUND_STATUS,
    payload,
  };
};

export const getRefundStatus = (payload) => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.GET_REFUND_STATUS,
    payload,
  };
};

export const getCancelStatus = (payload) => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.GET_CANCEL_STATUS,
    payload,
  };
};

export const setCancelStatus = (payload) => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.SET_CANCEL_STATUS,
    payload,
  };
};

export const getShipmentDelayedRefundStatus = (payload) => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.GET_SHIPMENT_DELAYED_REFUND,
    payload,
  };
};

export const setShipmentDelayedRefundStatus = (payload) => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.SET_SHIPMENT_DELAYED_REFUND,
    payload,
  };
};

export const resetCustomerHelpReducer = () => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.RESET_CUSTOMER_HELP_REDUCER,
  };
};

export const setCurrentPage = (payload) => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.UPDATE_CURRENT_PAGE_NUMBER,
    payload,
  };
};
export const setOrderStatusInTransit = (payload) => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.ORDER_STATUS_IN_TRANSIT,
    payload,
  };
};

export const setToggleCards = (payload) => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.TOGGLE_CARDS,
    payload,
  };
};

export const setSelectedShipmentCard = (payload) => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.SET_SELECTED_SHIPMENT_CARD,
    payload,
  };
};

export const setFilteredList = (payload) => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.SET_FILTERED_LIST,
    payload,
  };
};

export const getSavedSummary = (payload) => {
  return {
    type: CUSTOMER_HELP_CONSTANTS.SET_SUMMARY,
    payload,
  };
};

export const setReturnMethod = (payload) => {
  return {
    type: RETURN_ITEM_CONSTANT.SET_RETURN_METHOD,
    payload,
  };
};

export const clearReturnMethod = () => {
  return {
    type: RETURN_ITEM_CONSTANT.CLEAR_RETURN_METHOD,
  };
};

export const toggleReturnItemDisplay = (payload) => {
  return {
    type: RETURN_ITEM_CONSTANT.IS_RETURN_ITEM_DISPLAY,
    payload,
  };
};
