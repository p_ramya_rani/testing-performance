/* eslint-disable sonarjs/max-switch-cases */
// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable complexity */
import { fromJS } from 'immutable';
import CUSTOMER_HELP_CONSTANTS, { RETURN_ITEM_CONSTANT } from '../CustomerHelp.constants';

const initialState = fromJS({
  selectedReasonCode: null,
  selectedWrongItemResolution: null,
  reasonCodes: [],
  topLevelReasonCodes: [],
  accountsCouponsReasonCodes: [],
  selectedOrderItems: {},
  itemLevelRefund: {},
  initiateReturnStatus: null,
  selectedResolution: null,
  resolutionModuleX: [],
  returnInStoreModuleX: [],
  returnByMailModuleX: [],
  returnPolicyModuleX: [],
  cancelResolutionModuleX: [],
  isFetching: false,
  transitHelpModuleX: [],
  refundStatusData: [],
  cancelStatusData: {},
  shipmentDelayedRefundData: {},
  currentPageNumber: 0,
  isOrderStatusInTransit: null,
  toggleCards: false,
  wrongItemData: {},
  selectedShipmentCard: null,
  filteredList: [],
  orderSummary: {},
  preReasonFlag: true,
  returnMethod: null,
  isReturnItemDisplay: false,
  returnedOrderItems: [],
  selectedOrderArriveDeliveredReasonCode: null,
});

const CustomerHelpReducerExtension = (state = initialState, action) => {
  switch (action.type) {
    case RETURN_ITEM_CONSTANT.SET_RETURN_METHOD:
      return { ...state, ...{ returnMethod: action.payload } };
    case RETURN_ITEM_CONSTANT.CLEAR_RETURN_METHOD:
      return { ...state, ...{ returnMethod: null } };
    case CUSTOMER_HELP_CONSTANTS.SET_RETURNED_ORDER_ITEMS:
      return { ...state, ...{ returnedOrderItems: action.payload } };
    case CUSTOMER_HELP_CONSTANTS.CLEAR_RETURNED_ORDER_ITEMS:
      return { ...state, ...{ returnedOrderItems: [] } };
    default:
      return state;
  }
};

const CustomerHelpReducer = (state = initialState, action) => {
  switch (action.type) {
    case CUSTOMER_HELP_CONSTANTS.SET_REFERRED_CONTENT:
      return { ...state, ...{ reasonCodes: action.payload } };
    case CUSTOMER_HELP_CONSTANTS.SET_REASON_CODES:
      return {
        ...state,
        ...{ [action.payload.reduxKey]: action.payload.reasonCodes },
      };
    case CUSTOMER_HELP_CONSTANTS.SET_REASON_CODE:
      return { ...state, ...{ selectedReasonCode: action.payload } };
    case CUSTOMER_HELP_CONSTANTS.SET_ORDER_ARRIVE_REASON_CODE:
      return { ...state, ...{ selectedOrderArriveReasonCode: action.payload } };
    case CUSTOMER_HELP_CONSTANTS.CLEAR_ORDER_ARRIVE_REASON_CODE:
      return { ...state, ...{ selectedOrderArriveReasonCode: null } };
    case CUSTOMER_HELP_CONSTANTS.SET_ORDER_ARRIVE_DELIVERED_REASON_CODE:
      return { ...state, ...{ selectedOrderArriveDeliveredReasonCode: action.payload } };
    case CUSTOMER_HELP_CONSTANTS.CLEAR_REASON_CODE:
      return { ...state, ...{ selectedReasonCode: null, preReasonFlag: false } };
    case CUSTOMER_HELP_CONSTANTS.CLEAR_SELECTED_RESOLUTION:
      return { ...state, ...{ selectedResolution: null, selectedWrongItemResolution: null } };
    case CUSTOMER_HELP_CONSTANTS.SET_SELECTED_ITEMS:
      return { ...state, ...{ selectedOrderItems: action.payload } };
    case CUSTOMER_HELP_CONSTANTS.SET_RESOLUTION_CODE:
      return { ...state, ...action.payload };
    case CUSTOMER_HELP_CONSTANTS.CLEAR_SELECTED_ITEMS:
      return { ...state, ...{ selectedOrderItems: {} } };
    case CUSTOMER_HELP_CONSTANTS.SET_RESOLUTION_MODULEX_CONTENT:
      return { ...state, ...{ resolutionModuleX: action.payload } };
    case CUSTOMER_HELP_CONSTANTS.SET_RETURN_IN_STORE_MODULEX_CONTENT:
      return { ...state, ...{ returnInStoreModuleX: action.payload } };
    case CUSTOMER_HELP_CONSTANTS.SET_RETURN_BY_MAIL_MODULEX_CONTENT:
      return { ...state, ...{ returnByMailModuleX: action.payload } };
    case CUSTOMER_HELP_CONSTANTS.SET_RETURN_POLICY_MODULEX_CONTENT:
      return { ...state, ...{ returnPolicyModuleX: action.payload } };
    case CUSTOMER_HELP_CONSTANTS.SET_CANCEL_RESOLUTION_MODULEX_CONTENT:
      return { ...state, ...{ cancelResolutionModuleX: action.payload } };
    case CUSTOMER_HELP_CONSTANTS.SET_ITEM_LEVEL_REFUND:
      return { ...state, ...{ itemLevelRefund: action.payload } };
    case CUSTOMER_HELP_CONSTANTS.SET_INITIATE_RETURN_STATUS:
      return { ...state, ...{ initiateReturnStatus: action.payload } };
    case CUSTOMER_HELP_CONSTANTS.CLEAR_INITIATE_RETURN_STATUS:
      return { ...state, ...{ initiateReturnStatus: null } };
    case CUSTOMER_HELP_CONSTANTS.SET_SHIPMENT_STATUS:
      return { ...state, ...{ ShipmentStatusData: action.payload } };
    case CUSTOMER_HELP_CONSTANTS.SHOW_CSH_LOADER:
      return { ...state, ...{ isFetching: true } };
    case CUSTOMER_HELP_CONSTANTS.HIDE_CSH_LOADER:
      return { ...state, ...{ isFetching: false } };
    case CUSTOMER_HELP_CONSTANTS.SET_WRONG_ITEM_STATE:
      return { ...state, ...{ wrongItemData: action.payload } };
    case CUSTOMER_HELP_CONSTANTS.SET_TRANSIT_RESOLUTION_MODULEX_CONTENT:
      return { ...state, ...{ transitHelpModuleX: action.payload } };
    case CUSTOMER_HELP_CONSTANTS.SET_REFUND_STATUS:
      return { ...state, ...{ refundStatusData: action.payload } };
    case CUSTOMER_HELP_CONSTANTS.SET_CANCEL_STATUS:
      return { ...state, ...{ cancelStatusData: action.payload } };
    case CUSTOMER_HELP_CONSTANTS.SET_SHIPMENT_DELAYED_REFUND:
      return { ...state, ...{ shipmentDelayedRefundData: action.payload } };
    case CUSTOMER_HELP_CONSTANTS.RESET_CUSTOMER_HELP_REDUCER:
      return { ...initialState, ...{ topLevelReasonCodes: state.topLevelReasonCodes } };
    case CUSTOMER_HELP_CONSTANTS.UPDATE_CURRENT_PAGE_NUMBER:
      return { ...state, ...{ currentPageNumber: action.payload } };
    case CUSTOMER_HELP_CONSTANTS.ORDER_STATUS_IN_TRANSIT:
      return { ...state, ...{ isOrderStatusInTransit: action.payload } };
    case CUSTOMER_HELP_CONSTANTS.SET_SELECTED_SHIPMENT_CARD:
      return { ...state, ...{ selectedShipmentCard: action.payload } };
    case CUSTOMER_HELP_CONSTANTS.SET_FILTERED_LIST:
      return { ...state, ...{ filteredList: action.payload } };
    case CUSTOMER_HELP_CONSTANTS.SET_SUMMARY:
      return { ...state, ...{ orderSummary: action.payload } };
    case RETURN_ITEM_CONSTANT.IS_RETURN_ITEM_DISPLAY:
      return { ...state, ...{ isReturnItemDisplay: action.payload } };
    default:
      return CustomerHelpReducerExtension(state, action);
  }
};

export default CustomerHelpReducer;
