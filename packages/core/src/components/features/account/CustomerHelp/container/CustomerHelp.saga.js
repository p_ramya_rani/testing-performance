// 9fbef606107a605d69c0edbcd8029e5d
import { call, takeLatest, put, select, takeEvery } from 'redux-saga/effects';
import {
  getDivisionTabs,
  getDivisionTabsCSH,
} from '@tcp/core/src/services/abstractors/common/divisionTabs/divisionTabs';
import { setTrackOrderModalMountedState } from '@tcp/core/src/components/features/account/TrackOrder/container/TrackOrder.actions';
import { getUserLoggedInState } from '@tcp/core/src/components/features/account/User/container/User.selectors';
import { getOrderDetails } from '@tcp/core/src/components/features/account/OrderDetails/container/OrderDetails.actions';
import setLoaderState from '@tcp/core/src/components/common/molecules/Loader/container/Loader.actions';
import { routerPush } from '@tcp/core/src/utils';
import {
  setOrdersList,
  showLoadMore,
  hideLoadMore,
} from '@tcp/core/src/components/features/account/Orders/container/Orders.actions';
import getItemLevelRefund, {
  getShipmentStatus,
  getRefundStatus,
  getCancelStatus,
  getShipmentDelayedRefundStatus,
  initiateReturn as initiateReturnAbstractor,
} from '@tcp/core/src/services/abstractors/customerHelp/customerHelp';
import { getOrderHistory } from '../../../../../services/abstractors/account/ordersList';
import CUSTOMER_HELP_CONSTANTS, {
  CUSTOMER_HELP_ROUTES,
  CUSTOMER_HELP_PAGE_TEMPLATE,
  INITIATE_RETURN_SUCCESS_STATUS,
} from '../CustomerHelp.constants';
import {
  setReferredModule,
  setItemLevelRefund,
  setShipmentStatus,
  setResolutionModuleX,
  setCancelResolutionModuleX,
  showLoader,
  hideLoader,
  setTransitResolutionModuleX,
  setRefundStatus,
  setCancelStatus,
  setShipmentDelayedRefundStatus,
  setCurrentPage,
  setReasonCodes,
  setReturnInStoreModuleX,
  setReturnByMailModuleX,
  setInitiateReturnStatus,
  setReturnPolicyModuleX,
} from './CustomerHelp.actions';
import { getModuleX } from '../../../../../services/abstractors/common/moduleX';
import { routeToPage } from '../util/utility';
import { isMobileApp } from '../../../../../utils';

export function* fetchModule({ referred }) {
  try {
    if (referred && referred.length > 0) {
      const { contentId } = referred[0];
      const result = yield call(getDivisionTabs, contentId);
      yield put(setReferredModule(result));
    }
  } catch (err) {
    yield null;
  }
}

export function* fetchReasonCodes({ payload }) {
  const { contentId, reduxKey } = payload;
  try {
    const result = yield call(getDivisionTabsCSH, contentId);
    yield put(setReasonCodes({ reduxKey, reasonCodes: result }));
  } catch (err) {
    yield null;
  }
}

function* showHideLoadMoreLogic(orders) {
  if (
    (orders && Number(orders.totalRecords)) >
    Number(CUSTOMER_HELP_PAGE_TEMPLATE.ORDER_SIZE) * (Number(0) + 1)
  ) {
    yield put(showLoadMore());
  } else {
    yield put(hideLoadMore());
  }
}

const handleRouting = ({ ordersList, navigation }) => {
  if (ordersList.length === 1) {
    const { orderNumber } = ordersList[0];
    if (!isMobileApp()) {
      routerPush(
        `${CUSTOMER_HELP_ROUTES.TELL_US_MORE.to}&orderId=${orderNumber}`,
        `${CUSTOMER_HELP_ROUTES.TELL_US_MORE.asPath}/${orderNumber}`
      );
    }
    const mobileRoute = CUSTOMER_HELP_PAGE_TEMPLATE.TELL_US_MORE;
    if (isMobileApp() && navigation) {
      navigation.navigate(mobileRoute, {
        orderId: orderNumber,
        fromPage: 'Help Center',
      });
    }
  } else {
    if (!isMobileApp()) {
      routeToPage(CUSTOMER_HELP_ROUTES.SELECT_ORDER);
    }
    const mobileRoute = CUSTOMER_HELP_PAGE_TEMPLATE.SELECT_ORDER;
    if (isMobileApp() && navigation) {
      navigation.navigate(mobileRoute);
    }
  }
};

export function* selectOrder({ payload }) {
  try {
    const { navigation = null } = payload || {};
    const isLoggedIn = yield select(getUserLoggedInState);
    if (!isLoggedIn) {
      if (isMobileApp() && navigation) {
        navigation.navigate('TrackOrder', {
          handleToggle: true,
          noHeader: true,
          trackOrderInitiator: CUSTOMER_HELP_CONSTANTS.CUSTOMER_HELP,
        });
      } else {
        yield put(
          setTrackOrderModalMountedState({
            state: true,
            trackOrderInitiator: CUSTOMER_HELP_CONSTANTS.CUSTOMER_HELP,
          })
        );
      }
    } else {
      yield put(showLoader());
      const orderHistoryPayload = {
        reqParams: {
          itemdetails: true,
          pageNumber: '0',
          pageSize: CUSTOMER_HELP_PAGE_TEMPLATE.ORDER_SIZE,
        },
      };
      const orders = yield call(getOrderHistory, '', '', orderHistoryPayload.reqParams);

      yield showHideLoadMoreLogic(orders);
      yield put(setCurrentPage(0));

      const { orders: ordersList } = orders;
      yield put(setOrdersList(orders));

      handleRouting({ ordersList, navigation });
    }
  } catch (e) {
    // routing to select order in case of api fail to show empty order list screen
    if (!isMobileApp()) {
      routeToPage(CUSTOMER_HELP_ROUTES.SELECT_ORDER);
    }
  } finally {
    yield put(hideLoader());
  }
}

export function* getItemsRefund({ payload }) {
  try {
    if (payload) {
      const { orderId } = payload;
      yield put(showLoader());
      yield put(setLoaderState(true));
      const isLoggedIn = yield select(getUserLoggedInState);
      const result = yield call(getItemLevelRefund, payload);
      yield put(setItemLevelRefund(result));
      const { navigation = null } = payload || {};
      if (isMobileApp() && navigation) {
        navigation.navigate(CUSTOMER_HELP_PAGE_TEMPLATE.CONFIRMATION);
      } else {
        routeToPage(CUSTOMER_HELP_ROUTES.CONFIRMATION);
      }
      if (result.status.toLowerCase() === 'success' && isLoggedIn) {
        yield put(getOrderDetails({ orderId, fromCSH: true }));
      }
    }
  } catch (err) {
    yield null;
  } finally {
    yield put(hideLoader());
    yield put(setLoaderState(false));
  }
}

export function* initiateReturn({ payload }) {
  try {
    if (payload) {
      const { orderId } = payload;
      yield put(showLoader());
      yield put(setLoaderState(true));
      const isLoggedIn = yield select(getUserLoggedInState);
      const result = yield call(initiateReturnAbstractor, payload, isLoggedIn);
      yield put(setInitiateReturnStatus(result));
      routeToPage(CUSTOMER_HELP_ROUTES.CONFIRMATION);
      if (result.status === INITIATE_RETURN_SUCCESS_STATUS && isLoggedIn) {
        yield put(getOrderDetails({ orderId, fromCSH: true }));
      }
    }
  } catch (err) {
    yield null;
  } finally {
    yield put(hideLoader());
    yield put(setLoaderState(false));
  }
}

export function* getShipmentDelayedRefundData({ payload }) {
  try {
    if (payload) {
      const { orderId } = payload;
      yield put(showLoader());
      yield put(setLoaderState(true));
      const isLoggedIn = yield select(getUserLoggedInState);
      const result = yield call(getShipmentDelayedRefundStatus, payload);
      yield put(setShipmentDelayedRefundStatus(result));
      const { navigation = null } = payload || {};
      if (isMobileApp() && navigation) {
        navigation.navigate(CUSTOMER_HELP_PAGE_TEMPLATE.CONFIRMATION);
      } else {
        routeToPage(CUSTOMER_HELP_ROUTES.CONFIRMATION);
      }
      if (result.status.toLowerCase() === 'success' && isLoggedIn) {
        yield put(getOrderDetails({ orderId, fromCSH: true }));
      }
    }
  } catch (err) {
    yield null;
  } finally {
    yield put(hideLoader());
    yield put(setLoaderState(false));
  }
}

export function* getShipmentStatusData({ payload }) {
  try {
    if (payload) {
      yield put(showLoader());
      const result = yield call(getShipmentStatus, payload);
      yield put(setShipmentStatus(result));
    }
  } catch (err) {
    yield null;
  } finally {
    yield put(hideLoader());
  }
}

export function* getRefundStatusData({ payload }) {
  try {
    if (payload) {
      yield put(showLoader());
      const result = yield call(getRefundStatus, payload);
      yield put(setRefundStatus(result));
    }
  } catch (err) {
    yield null;
  } finally {
    yield put(hideLoader());
  }
}

export function* getCancelStatusData({ payload }) {
  try {
    if (payload) {
      const { orderNumber } = payload;
      yield put(setLoaderState(true));
      yield put(showLoader());
      const isLoggedIn = yield select(getUserLoggedInState);
      const { navigation = null } = payload || {};
      const result = yield call(getCancelStatus, payload);
      yield put(setCancelStatus(result));
      if (isMobileApp() && navigation) {
        navigation.navigate(CUSTOMER_HELP_PAGE_TEMPLATE.CONFIRMATION);
      } else {
        routeToPage(CUSTOMER_HELP_ROUTES.CONFIRMATION);
      }
      if (result.status.toLowerCase() === 'success' && isLoggedIn) {
        yield put(getOrderDetails({ orderId: orderNumber, fromCSH: true }));
      }
    }
  } catch (err) {
    yield null;
  } finally {
    yield put(setLoaderState(false));
    yield put(hideLoader());
  }
}

export function* fetchResolutionModuleX({ payload }) {
  try {
    const result = yield call(getModuleX, payload);
    yield put(setResolutionModuleX(result));
  } catch (err) {
    yield null;
  }
}

export function* fetchCancelResolutionModuleX({ payload }) {
  try {
    const result = yield call(getModuleX, payload);
    yield put(setCancelResolutionModuleX(result));
  } catch (err) {
    yield null;
  }
}

export function* fetchTransitResolutionModuleX({ payload }) {
  try {
    const result = yield call(getModuleX, payload);
    yield put(setTransitResolutionModuleX(result));
  } catch (err) {
    yield null;
  }
}

export function* fetchReturnInStoreModuleX({ payload }) {
  try {
    const result = yield call(getModuleX, payload);
    yield put(setReturnInStoreModuleX(result));
  } catch (err) {
    yield null;
  }
}

export function* fetchReturnByMailModuleX({ payload }) {
  try {
    const result = yield call(getModuleX, payload);
    yield put(setReturnByMailModuleX(result));
  } catch (err) {
    yield null;
  }
}

export function* fetchReturnPolicyModuleX({ payload }) {
  try {
    const result = yield call(getModuleX, payload);
    yield put(setReturnPolicyModuleX(result));
  } catch (err) {
    yield null;
  }
}

export function* CustomerHelpSaga() {
  yield takeLatest(CUSTOMER_HELP_CONSTANTS.GET_REFERRED_CONTENT, fetchModule);
  yield takeLatest(CUSTOMER_HELP_CONSTANTS.INITIATE_RETURN, initiateReturn);
  yield takeEvery(CUSTOMER_HELP_CONSTANTS.GET_REASON_CODES, fetchReasonCodes);
  yield takeLatest(CUSTOMER_HELP_CONSTANTS.SELECT_ORDER, selectOrder);
  yield takeLatest(CUSTOMER_HELP_CONSTANTS.GET_ITEM_LEVEL_REFUND, getItemsRefund);
  yield takeLatest(CUSTOMER_HELP_CONSTANTS.GET_SHIPMENT_STATUS, getShipmentStatusData);
  yield takeLatest(
    CUSTOMER_HELP_CONSTANTS.FETCH_RESOLUTION_MODULEX_CONTENT,
    fetchResolutionModuleX
  );
  yield takeLatest(
    CUSTOMER_HELP_CONSTANTS.FETCH_RETURN_IN_STORE_MODULEX_CONTENT,
    fetchReturnInStoreModuleX
  );
  yield takeLatest(
    CUSTOMER_HELP_CONSTANTS.FETCH_RETURN_BY_MAIL_MODULEX_CONTENT,
    fetchReturnByMailModuleX
  );
  yield takeLatest(
    CUSTOMER_HELP_CONSTANTS.FETCH_RETURN_POLICY_MODULEX_CONTENT,
    fetchReturnPolicyModuleX
  );
  yield takeLatest(
    CUSTOMER_HELP_CONSTANTS.FETCH_CANCEL_MODULEX_CONTENT,
    fetchCancelResolutionModuleX
  );
  yield takeLatest(
    CUSTOMER_HELP_CONSTANTS.FETCH_TRANSIT_RESOLUTION_MODULEX_CONTENT,
    fetchTransitResolutionModuleX
  );
  yield takeLatest(CUSTOMER_HELP_CONSTANTS.GET_REFUND_STATUS, getRefundStatusData);
  yield takeLatest(CUSTOMER_HELP_CONSTANTS.GET_CANCEL_STATUS, getCancelStatusData);
  yield takeLatest(
    CUSTOMER_HELP_CONSTANTS.GET_SHIPMENT_DELAYED_REFUND,
    getShipmentDelayedRefundData
  );
}

export default CustomerHelpSaga;
