// 9fbef606107a605d69c0edbcd8029e5d
import { createSelector } from 'reselect';
import {
  CUSTOMER_HELP_REDUCER_KEY,
  RESET_PASSWORD_REDUCER_KEY,
} from '@tcp/core/src/constants/reducer.constants';
import CUSTOMER_HELP_CONSTANTS, {
  REASON_CODES,
  INITIATE_RETURN_SUCCESS_STATUS,
} from '../CustomerHelp.constants';

const getState = (state) => state[CUSTOMER_HELP_REDUCER_KEY];
const getResetPasswordState = (state) => state[RESET_PASSWORD_REDUCER_KEY];

export const getCustomerHelpLabels = (state) => {
  return state.Labels.global && state.Labels.global.helpCenter;
};

export const getReasonCodeList = createSelector(getState, (state) => state?.reasonCodes);

export const getTopLevelReasonCodeList = createSelector(
  getState,
  (state) => state?.topLevelReasonCodes
);

export const getReasonCodeListAccountsAndCoupons = createSelector(
  getState,
  (state) => state?.accountsCouponsReasonCodes
);

export const getSelectedReasonCode = createSelector(getState, (state) => state?.selectedReasonCode);

export const getOrderArriveSelectedReasonCode = createSelector(
  getState,
  (state) => state?.selectedOrderArriveReasonCode
);

export const getSelectedOrderArriveDeliveredReasonCode = createSelector(
  getState,
  (state) => state?.selectedOrderArriveDeliveredReasonCode
);

export const getPreReasonFlag = createSelector(getState, (state) => state?.preReasonFlag);

export const getSelectedOrderItems = createSelector(getState, (state) => state?.selectedOrderItems);

export const getItemLevelReturns = createSelector(getState, (state) => state?.itemLevelRefund);

export const getShipmentStatusData = createSelector(getState, (state) => state?.ShipmentStatusData);

export const getSelectedResolution = createSelector(getState, (state) => state?.selectedResolution);

export const getSelectedWrongItemResolution = createSelector(
  getState,
  (state) => state?.selectedWrongItemResolution
);

export const getResolutionModuleXContent = createSelector(
  getState,
  (state) => state?.resolutionModuleX
);
export const getCancelResolutionModuleXContent = createSelector(
  getState,
  (state) => state?.cancelResolutionModuleX
);

export const getReturnInStoreModuleXContent = createSelector(
  getState,
  (state) => state?.returnInStoreModuleX
);

export const getReturnPolicyModuleXContent = createSelector(
  getState,
  (state) => state?.returnPolicyModuleX
);

export const getReturnByMailModuleXContent = createSelector(
  getState,
  (state) => state?.returnByMailModuleX
);

export const getOrderListFetchingState = createSelector(getState, (state) => state?.isFetching);

export const getWrongItemState = createSelector(getState, (state) => state?.wrongItemData || null);
export const getTransitHelpModuleXContent = createSelector(
  getState,
  (state) => state?.transitHelpModuleX
);

export const getRefundStatusData = createSelector(getState, (state) => state?.refundStatusData);

export const getCancelStatusData = createSelector(getState, (state) => state?.cancelStatusData);

export const getShipmentDelayedStatusData = createSelector(
  getState,
  (state) => state?.shipmentDelayedRefundData
);

export const getOrderResolutionContentId = (state) => {
  const reasonType = getSelectedReasonCode(state);
  const selectedOrderArriveReasonCode =
    getSelectedOrderArriveDeliveredReasonCode(state) || getOrderArriveSelectedReasonCode(state);
  let resolutionName = '';
  if (reasonType) {
    switch (reasonType.action) {
      case REASON_CODES.TRACKING_NUMBER_SHOW_IN_TRANSIT_FOR_LONG_TIME:
        resolutionName = 'in_transit';
        break;
      case REASON_CODES.PACKAGE_DELIVERED_BUT_NOT_RECEIVED:
        resolutionName = 'not_received';
        break;
      default:
        resolutionName = '';
    }
  }

  if (!resolutionName && selectedOrderArriveReasonCode) {
    switch (selectedOrderArriveReasonCode.action) {
      case REASON_CODES.TRACKING_NUMBER_SHOW_IN_TRANSIT_FOR_LONG_TIME:
        resolutionName = 'in_transit';
        break;
      case REASON_CODES.PACKAGE_DELIVERED_BUT_NOT_RECEIVED:
        resolutionName = 'not_received';
        break;
      default:
        resolutionName = '';
    }
  }
  let contentID;
  if (
    state.Labels &&
    state.Labels.global &&
    state.Labels.global.helpCenter &&
    Array.isArray(state.Labels.global.helpCenter.referred)
  ) {
    contentID =
      state.Labels.global.helpCenter.referred.length &&
      state.Labels.global.helpCenter.referred.find(
        (label) => label.name === `resolution_txt_${resolutionName}`
      );
  }
  return contentID;
};

export const getReturnInstructionsContentId = (state, returnMethod) => {
  let contentID;
  if (
    state.Labels &&
    state.Labels.global &&
    state.Labels.global.helpCenter &&
    Array.isArray(state.Labels.global.helpCenter.referred)
  ) {
    contentID =
      state.Labels.global.helpCenter.referred.length &&
      state.Labels.global.helpCenter.referred.find(
        (label) => label.name === `return_${returnMethod}_instructions`
      );
  }
  return contentID;
};

export const getReturnInStoreInstructionsContentId = (state) => {
  return getReturnInstructionsContentId(
    state,
    CUSTOMER_HELP_CONSTANTS.RETURN_METHOD.IN_STORE.toLowerCase()
  );
};
export const getReturnPolicyContentId = (state) => {
  let contentID;
  if (
    state.Labels &&
    state.Labels.global &&
    state.Labels.global.helpCenter &&
    Array.isArray(state.Labels.global.helpCenter.referred)
  ) {
    contentID =
      state.Labels.global.helpCenter.referred.length &&
      state.Labels.global.helpCenter.referred.find((label) => label.name === `returnPolicy`);
  }
  return contentID;
};

export const getReturnByMailInstructionsContentId = (state) => {
  return getReturnInstructionsContentId(
    state,
    CUSTOMER_HELP_CONSTANTS.RETURN_METHOD.BY_MAIL.toLowerCase()
  );
};

export const getCurrentPageNumber = createSelector(getState, (state) => state?.currentPageNumber);

export const getIsOrderStatusInTransit = createSelector(
  getState,
  (state) => state?.isOrderStatusInTransit
);

export const getToggleCards = createSelector(getState, (state) => state?.toggleCards);

export const getSelectedShipmentCard = createSelector(
  getState,
  (state) => state?.selectedShipmentCard
);

export const getFilteredList = createSelector(getState, (state) => state?.filteredList);

export const getOrderSummary = createSelector(getState, (state) => state?.orderSummary);

export const getResetPasswordError = createSelector(getResetPasswordState, (state) => state?.error);

export const getReturnMethod = createSelector(getState, (state) => state?.returnMethod);

export const getReturnedOrderItems = createSelector(getState, (state) => state?.returnedOrderItems);

export const isReturnItemDisplay = createSelector(getState, (state) => state?.isReturnItemDisplay);

export const getInitiateReturnStatus = createSelector(
  getState,
  (state) => state?.initiateReturnStatus
);

export const isInitiateReturnSuccessful = createSelector(
  getInitiateReturnStatus,
  (returnStatus) => returnStatus?.status === INITIATE_RETURN_SUCCESS_STATUS
);

export const isInitiateReturnFailure = createSelector(getInitiateReturnStatus, (returnStatus) =>
  returnStatus?.status === 'FAIL' ? returnStatus?.errorCode : null
);
