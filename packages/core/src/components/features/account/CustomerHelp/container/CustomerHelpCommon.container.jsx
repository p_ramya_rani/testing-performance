// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import {
  getUserLoggedInState,
  getUserEmail,
  mprUserId,
} from '@tcp/core/src/components/features/account/User/container/User.selectors';
import {
  getOrderDetailsDataState,
  getAllItems,
  getOrderDetailsDataFetchingState,
  getOrderLookUpAPISwitch,
  getOrderReceiptReturnDays,
} from '@tcp/core/src/components/features/account/OrderDetails/container/OrderDetails.selectors';
import { getOrderDetails } from '@tcp/core/src/components/features/account/OrderDetails/container/OrderDetails.actions';
import {
  getLabels,
  getGlobalLabels,
} from '@tcp/core/src/components/features/account/Account/container/Account.selectors';
import {
  selectOrder,
  getItemLevelRefund,
  setCurrentPage,
  initiateReturn,
  setReturnMethod,
  clearReturnMethod,
  toggleReturnItemDisplay,
  clearInitiateReturnStatus,
  setOrderArriveDeliveredReasonCode,
} from '@tcp/core/src/components/features/account/CustomerHelp/container/CustomerHelp.actions';
import { getOrdersList } from '@tcp/core/src/components/features/account/Orders/container/Orders.actions';
import {
  getOrdersListState,
  getLoadMoreState,
} from '@tcp/core/src/components/features/account/Orders/container/Orders.selectors';
import {
  getCSHOlderMonth,
  getIsLiveChatEnabled,
  getIsPaidNExpediteRefundEnable,
  getIsPaidNExpediteRefundEnableApp,
  getIsCouponSelfHelpEnabled,
  getIsLiveChatBackABisOff,
  getIsMobileLiveChatEnabled,
  getIsMergeMyPlaceAccount,
  getIsInternationalShipping,
  getCSHReturnItemOlderDays,
  getIsNewWhenWillMyOrderArriveEnabled,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import { setClickAnalyticsData, trackPageView, trackClick } from '@tcp/core/src/analytics/actions';
import { getClickAnalyticsData } from '@tcp/core/src/analytics/Analytics.selectors';
import {
  getCustomerHelpLabels,
  getReasonCodeList,
  getReasonCodeListAccountsAndCoupons,
  getSelectedReasonCode,
  getSelectedOrderItems,
  getItemLevelReturns,
  getShipmentStatusData,
  getSelectedResolution,
  getSelectedWrongItemResolution,
  getResolutionModuleXContent,
  getCancelResolutionModuleXContent,
  getOrderListFetchingState,
  getWrongItemState,
  getTransitHelpModuleXContent,
  getRefundStatusData,
  getCancelStatusData,
  getShipmentDelayedStatusData,
  getOrderResolutionContentId,
  getCurrentPageNumber,
  getIsOrderStatusInTransit,
  getToggleCards,
  getSelectedShipmentCard,
  getFilteredList,
  getOrderSummary,
  getPreReasonFlag,
  getTopLevelReasonCodeList,
  getReturnByMailInstructionsContentId,
  getReturnInStoreInstructionsContentId,
  getReturnPolicyContentId,
  getReturnInStoreModuleXContent,
  getReturnPolicyModuleXContent,
  getReturnByMailModuleXContent,
  getReturnMethod,
  isReturnItemDisplay,
  isInitiateReturnSuccessful,
  isInitiateReturnFailure,
  getReturnedOrderItems,
  getOrderArriveSelectedReasonCode,
  getSelectedOrderArriveDeliveredReasonCode,
} from './CustomerHelp.selectors';
import {
  getReferredModule,
  setReasonCode,
  clearSelectedReason,
  setSelectedItems,
  setResolution,
  clearSelectedItems,
  clearSelectedResolution,
  fetchModifiedResolutionModuleX,
  fetchCancelledResolutionModuleX,
  setWrongItemState,
  fetchTransitHelpModuleX,
  getRefundStatus,
  getCancelStatus,
  getShipmentDelayedRefundStatus,
  setOrderStatusInTransit,
  setToggleCards,
  setSelectedShipmentCard,
  resetCustomerHelpReducer,
  setFilteredList,
  showLoader,
  hideLoader,
  getSavedSummary,
  getReasonCodes,
  fetchReturnInStoreModuleX,
  fetchReturnByMailModuleX,
  fetchReturnPolicyModuleX,
  setReturnedOrderItems as setReturnedOrderItemsAction,
  clearReturnedOrderItems as clearReturnedOrderItemsAction,
  setOrderArriveReasonCode,
  clearOrderArriveSelectedReason,
} from './CustomerHelp.actions';

import { resetEmailConfirmationRequest } from '../organisms/EmailConfirmation/container/EmailConfirmation.action';
import { getError } from '../../ResetPassword/container/ResetPassword.selectors';
import CustomerHelp from '../views';
import { getCouponsLabels } from '../../../CnC/common/organism/CouponAndPromos/container/Coupon.selectors';
import {
  getAccountStatus,
  getMergeStatus,
  getMergeError,
  getVictimEmailAddress,
  getSurvivorEmailAddress,
  getMergeFromOTPFlow,
} from '../organisms/MergeAccounts/container/MergeAccounts.selectors';
import {
  mergeAccountReset,
  setIsFromOTP,
} from '../organisms/MergeAccounts/container/MergeAccounts.actions';
/**
 * This Class component use for return the Customer Help data
 */

export class CustomerHelpContainer extends PureComponent {
  render() {
    return <CustomerHelp {...this.props} />;
  }
}

export const mapDispatchToProps = (dispatch) => {
  return {
    getReasonsCodeContent: (payload) => {
      dispatch(getReferredModule(payload));
    },
    getReasonCodeContentByContentId: (payload) => {
      dispatch(getReasonCodes(payload));
    },
    getOrderDetailsAction: (payload) => {
      dispatch(getOrderDetails(payload));
    },
    selectOrderAction: (payload) => {
      dispatch(selectOrder(payload));
    },
    setSelectedReasonCode: (payload) => {
      dispatch(setReasonCode(payload));
    },
    setOrderArriveSelectedReasonCode: (payload) => {
      dispatch(setOrderArriveReasonCode(payload));
    },
    setSelectedOrderArriveDeliveredReasonCode: (payload) => {
      dispatch(setOrderArriveDeliveredReasonCode(payload));
    },
    clearSelectedReasonCode: () => {
      dispatch(clearSelectedReason());
    },
    clearOrderArriveSelectedReasonCode: () => {
      dispatch(clearOrderArriveSelectedReason());
    },
    clearSelectedResolution: () => {
      dispatch(clearSelectedResolution());
    },
    fetchOrders: (payload) => {
      dispatch(getOrdersList(payload));
    },
    setSelectedOrderItems: (items) => {
      dispatch(setSelectedItems(items));
    },
    setResolution: (payload) => {
      dispatch(setResolution(payload));
    },
    submitRefund: (payload) => {
      dispatch(getItemLevelRefund(payload));
    },
    clearSelectedOrderItems: () => {
      dispatch(clearSelectedItems());
    },
    fetchResolutionModuleX: (payload) => {
      dispatch(fetchModifiedResolutionModuleX(payload));
    },
    fetchCancelContent: (payload) => {
      dispatch(fetchCancelledResolutionModuleX(payload));
    },
    setWrongItemData: (payload) => {
      dispatch(setWrongItemState(payload));
    },
    fetchTransitHelpModuleX: (items) => {
      dispatch(fetchTransitHelpModuleX(items));
    },
    fetchReturnByMailModuleX: (payload) => {
      dispatch(fetchReturnByMailModuleX(payload));
    },
    fetchReturnPolicyModuleX: (payload) => {
      dispatch(fetchReturnPolicyModuleX(payload));
    },
    fetchReturnInStoreModuleX: (payload) => {
      dispatch(fetchReturnInStoreModuleX(payload));
    },
    getRefundStatus: (items) => {
      dispatch(getRefundStatus(items));
    },
    getCancelStatus: (payload) => {
      dispatch(getCancelStatus(payload));
    },
    getShipmentRefundStatus: (payload) => {
      dispatch(getShipmentDelayedRefundStatus(payload));
    },
    setCurrentPage: (payload) => {
      dispatch(setCurrentPage(payload));
    },
    setOrderStatusInTransit: (payload) => {
      dispatch(setOrderStatusInTransit(payload));
    },
    setToggleCards: (payload) => {
      dispatch(setToggleCards(payload));
    },
    trackAnalyticsPageView: (eventData, payload) => {
      const loadtimer = setTimeout(() => {
        dispatch(setClickAnalyticsData(eventData));
        // Delayed setting of data when redirection on click event
        clearTimeout(loadtimer);
      }, 300);
      const timer = setTimeout(() => {
        dispatch(trackPageView(payload));
        clearTimeout(timer);
      }, 800);
    },
    trackAnalyticsClick: (eventData, payload) => {
      dispatch(setClickAnalyticsData(eventData));
      const timer = setTimeout(() => {
        dispatch(trackClick(payload));
        clearTimeout(timer);
      }, 800);
    },
    setSelectedShipmentCard: (payload) => {
      dispatch(setSelectedShipmentCard(payload));
    },
    resetCustomerHelpReducer: () => {
      dispatch(resetCustomerHelpReducer());
    },
    setFilteredList: (payload) => {
      dispatch(setFilteredList(payload));
    },
    showLoader: () => {
      dispatch(showLoader());
    },
    hideLoader: () => {
      dispatch(hideLoader());
    },
    getSavedSummary: (payload) => {
      dispatch(getSavedSummary(payload));
    },
    initiateReturnAction: (payload) => {
      dispatch(initiateReturn(payload));
    },
    mergeAccountReset: () => {
      dispatch(mergeAccountReset());
    },
    resetEmailConfirmation: () => dispatch(resetEmailConfirmationRequest()),

    setReturnMethod: (payload) => {
      dispatch(setReturnMethod(payload));
    },
    clearReturnMethod: () => {
      dispatch(clearReturnMethod());
    },
    setReturnItemDisplay: (payload) => {
      dispatch(toggleReturnItemDisplay(payload));
    },
    resetInitiateReturnStatus: () => {
      dispatch(clearInitiateReturnStatus());
    },
    setReturnedOrderItems: (payload) => {
      dispatch(setReturnedOrderItemsAction(payload));
    },
    clearReturnedOrderItems: (payload) => {
      dispatch(clearReturnedOrderItemsAction(payload));
    },
    clearMergeAccountFromOTP: (payload) => {
      dispatch(setIsFromOTP(payload));
    },
  };
};

export const mapStateToProps = (state) => {
  return {
    isLoggedIn: getUserLoggedInState(state),
    orderLabels: { ...getLabels(state), ...getCouponsLabels(state) },
    walletLabels: getGlobalLabels(state),
    labels: getCustomerHelpLabels(state),
    orderDetailsData: getOrderDetailsDataState(state),
    reasonCodes: getReasonCodeList(state),
    reasonCodesAccountsAndCoupons: getReasonCodeListAccountsAndCoupons(state),
    selectedReasonCode: getSelectedReasonCode(state),
    selectedOrderArriveReasonCode: getOrderArriveSelectedReasonCode(state),
    selectedOrderArriveDeliveredReasonCode: getSelectedOrderArriveDeliveredReasonCode(state),
    ordersListItems: getOrdersListState(state),
    orderItems: getAllItems(state),
    selectedOrderItems: getSelectedOrderItems(state),
    cshForMonthsToDisplay: getCSHOlderMonth(state),
    loggedInUserEmail: getUserEmail(state),
    itemLevelReturns: getItemLevelReturns(state),
    shipmentData: getShipmentStatusData(state),
    selectedResolution: getSelectedResolution(state),
    selectedWrongItemResolution: getSelectedWrongItemResolution(state),
    isFetching: getOrderListFetchingState(state),
    resolutionModuleXContent: getResolutionModuleXContent(state),
    returnInStoreModuleXContent: getReturnInStoreModuleXContent(state),
    returnPolicyModuleXContent: getReturnPolicyModuleXContent(state),
    returnByMailModuleXContent: getReturnByMailModuleXContent(state),
    cancelResolutionModuleXContent: getCancelResolutionModuleXContent(state),
    wrongItemData: getWrongItemState(state),
    transitHelpModuleXContent: getTransitHelpModuleXContent(state),
    refundData: getRefundStatusData(state),
    cancelStatusData: getCancelStatusData(state),
    shipmentDelayedStatusData: getShipmentDelayedStatusData(state),
    resolutionContentId: getOrderResolutionContentId(state),
    returnByMailContentId: getReturnByMailInstructionsContentId(state),
    returnInStoreContentId: getReturnInStoreInstructionsContentId(state),
    returnPolicyContentId: getReturnPolicyContentId(state),
    isLoadMoreActive: getLoadMoreState(state),
    currentPageNumber: getCurrentPageNumber(state),
    isOrderStatusTransit: getIsOrderStatusInTransit(state),
    toggleCards: getToggleCards(state),
    isPaidNExpediteRefundEnable: getIsPaidNExpediteRefundEnable(state),
    isPaidNExpediteRefundEnableApp: getIsPaidNExpediteRefundEnableApp(state),
    selectedShipmentCard: getSelectedShipmentCard(state),
    clickAnalyticsData: getClickAnalyticsData(state),
    filteredItems: getFilteredList(state),
    isFetchingOrderItems: getOrderDetailsDataFetchingState(state),
    getOrderSummary: getOrderSummary(state),
    orderLookUpAPISwitch: getOrderLookUpAPISwitch(state),
    isLiveChatEnabled: getIsLiveChatEnabled(state) && getIsLiveChatBackABisOff(state),
    isInternationalShipping: getIsInternationalShipping(state),
    isMobileLiveChatEnabled: getIsMobileLiveChatEnabled(state) && getIsLiveChatBackABisOff(state),
    preReasonFlag: getPreReasonFlag(state),
    isCouponSelfHelpEnabled: getIsCouponSelfHelpEnabled(state),
    isMergeMyPlaceAccount: getIsMergeMyPlaceAccount(state),
    topLevelReasonCodes: getTopLevelReasonCodeList(state),
    resetPasswordError: getError(state),
    accountStatus: getAccountStatus(state),
    mergeStatus: getMergeStatus(state),
    mergeError: getMergeError(state),
    mergeFromOtpFlow: getMergeFromOTPFlow(state),
    victimEmailAddress: getVictimEmailAddress(state),
    survivorEmailAddress: getSurvivorEmailAddress(state),
    selectedReturnMethod: getReturnMethod(state),
    isReturnItemDisplay: isReturnItemDisplay(state),
    orderReturnDays: getOrderReceiptReturnDays(state),
    userAccountId: mprUserId(state),
    isInitiateReturnSuccessful: isInitiateReturnSuccessful(state),
    isInitiateReturnFailure: isInitiateReturnFailure(state),
    cshReturnItemOlderDays: getCSHReturnItemOlderDays(state),
    isNewWhenWillMyOrderArrive: getIsNewWhenWillMyOrderArriveEnabled(state),
    returnedOrderItems: getReturnedOrderItems(state),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CustomerHelpContainer);
