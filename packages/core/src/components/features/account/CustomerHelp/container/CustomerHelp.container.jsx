// 9fbef606107a605d69c0edbcd8029e5d 
import { withRouter } from 'next/router';
import CustomerHelpCommonContainer from './CustomerHelpCommon.container';

const CustomerHelpWithRouter = withRouter(CustomerHelpCommonContainer);
export default CustomerHelpWithRouter;
