// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';
import { ViewWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';
import { Button } from '@tcp/core/src/components/common/atoms';

export const SideBySideView = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
`;

const getBorder = (props) => {
  if (props.noBorder) {
    return 'none';
  }
  if (props.isItemSelected) {
    return `2px solid ${props.theme.colorPalette.blue[800]}`;
  }
  return `1px solid ${props.theme.colorPalette.gray[600]}`;
};

export const SelectableSideBySide = styled.TouchableOpacity`
  flex-direction: row;
  justify-content: space-between;
  position: relative;
  border-radius: 12px;
  border: ${(props) => getBorder(props)};
  padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  margin: ${(props) => props.theme.spacing.ELEM_SPACING.SM}
    ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  ${(props) => (props.isItemDisabled ? 'opacity: 0.5 !important' : '')};
`;

export const ImageContainer = styled.View`
  width: 118px;
  height: 129px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const DetailContainer = styled.View`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 70px;
`;

export const ItemContainer = styled.View`
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  flex-direction: row;
`;

export const TotalItems = styled.View`
  background-color: ${(props) => props.theme.colorPalette.blue.C900};
  border-radius: 22px;
  padding: ${(props) => props.theme.spacing.ELEM_SPACING.XXS}
    ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  position: absolute;
`;

export const OrderTileContainer = styled.View`
  position: relative;
  display: flex;
  border-radius: 12px;
  ${(props) =>
    props.selected
      ? `border: 2px solid ${props.theme.colorPalette.blue[800]}`
      : ` border: 1px solid ${props.theme.colorPalette.gray[600]}`};
  ${(props) => (props.isDisabled ? 'opacity: 0.5 !important' : '')};
  padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  margin: ${(props) => props.theme.spacing.ELEM_SPACING.XS}
    ${(props) => props.theme.spacing.ELEM_SPACING.XS};
`;

export const TileIcons = styled.View`
  color: blue;
  position: absolute;
  top: 10px;
  right: 10px;
`;

export const LabelContainer = styled.View`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  align-items: center;
  justify-content: center;
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.LRG};
`;

export const ItemDisabled = styled.TouchableOpacity`
  opacity: 0.5 !important;
`;

export const ItemDisabledMessage = styled.Text`
  padding: 0px ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  color: ${(props) => props.theme.colors.TEXT.DARKGRAY};
  font-size: ${(props) => props.theme.typography.fontSizes.fs14};
  font-weight: ${(props) => props.theme.fonts.fontWeight.semiBold};
  font-family: ${(props) => props.theme.typography.fonts.secondary};
  margin: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
`;
export const Container = styled(ViewWithSpacing)`
  display: flex;
  flex-direction: column;
`;
export const RowContainer = styled(ViewWithSpacing)`
  flex-direction: row;
  justify-content: space-between;
  margin: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
`;
export const Row = styled(ViewWithSpacing)`
  flex-direction: row;
`;
export const BottomContainer = styled(ViewWithSpacing)`
  background-color: ${(props) => props.theme.colorPalette.gray[300]};
  padding: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  flex: 1;
`;
export const CustomButton = styled(Button)`
  background-color: white;
  color: ${(props) => props.theme.colorPalette.gray[800]};
  padding-top: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  padding-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
`;

export const ContainerWithLeftMargin = styled.View`
  margin-left: ${(props) => props.theme.spacing.APP_LAYOUT_SPACING.SM};
`;

export const ImageBrandStyle = styled.Image`
  bottom: 5px;
  width: 90px;
  height: 30px;
`;

export const OrderItemMessageView = styled.View`
  position: absolute;
  right: 0px;
  top: 40px;
  width: 76px;
  align-self: center;
  font-weight: bold;
  margin-right: 5px;
`;

export const ReasonCodeRectangle = styled.TouchableOpacity`
  width: 95%;
  min-height: 59px;
  border-radius: 8px;
  display: flex;
  align-items: flex-start;
  justify-content: center;
  border: solid 1px ${(props) => props.theme.colorPalette.gray[600]};
  background: ${(props) => props.theme.colors.WHITE};
  .textColor {
    color: ${(props) => props.theme.colors.PRIMARY.DARK};
  }
  padding: 10px;
  margin: 5px;
`;

export const SelectedItem = styled.View`
  border: 2px solid ${(props) => props.theme.colorPalette.blue[800]} !important;
`;

export const GrayTopBorder = styled(ViewWithSpacing)`
  background-color: ${(props) => props.theme.colorPalette.gray[300]};
  height: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
`;

export const BlueUnderLine = styled.View`
  background-color: ${(props) => props.theme.colorPalette.blue[500]};
  height: ${(props) => props.theme.spacing.ELEM_SPACING.XXXS};
  width: ${(props) => props.theme.spacing.ELEM_SPACING.XXL};
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
`;

export const Line = styled(ViewWithSpacing)`
  background-color: ${(props) => props.theme.colorPalette.gray[500]};
  height: 1px;
`;

export const WrapContainer = styled.View`
  display: flex;
  flex-direction: row;
  ${(props) => (props.width ? `width:${props.width}px` : 'width:280px')};
`;

export const ContactLink = styled.Text`
  text-decoration: underline;
  text-decoration-color: ${(props) => props.theme.colors.ANCHOR.SECONDARY};
  color: ${(props) => props.theme.colors.PRIMARY.BLUE};
`;

export const TellUsMoreSkeletonView = styled.View`
  min-width: 330px;
  height: ${(props) => props.theme.spacing.LAYOUT_SPACING.XXL};
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const PaymentDetailsLogoWrap = styled.View`
  ${(props) =>
    props.afterPay
      ? `background-color: ${props.theme.colors.AFTERPAY.MINT}; padding: 0 4px;margin-left:-${props.theme.spacing.ELEM_SPACING.XS};`
      : ''};
`;
