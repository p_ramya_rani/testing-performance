// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  .progressBarContainer {
    height: auto;
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XXXL};
    }
  }

  .order-no-found-header .tell-us-more,
  .order-no-found-header .select-option {
    display: inline-block;
  }

  .header-container {
    display: flex;
    justify-content: space-between;
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
    margin-bottom: 0px;
    .header-title,
    .select-option {
      display: flex;
      .select-option {
        align-items: center;
      }
    }
    .header-title {
      align-items: center;
    }
  }
  .order-Items-header-container {
    display: flex;
    margin: ${(props) => props.theme.spacing.ELEM_SPACING.MED} 0px 0px 0px;
  }

  .heading {
    display: flex;
    justify-content: center;
    @media ${(props) => props.theme.mediaQuery.large} {
      display: inline-block;
      margin-left: 0px;
    }
  }
  .selected-item {
    border: 2px solid ${(props) => props.theme.colorPalette.blue[800]} !important;
  }
  .disabled-item {
    opacity: 50% !important;
  }

  .set-align {
    display: flex;
    justify-content: center;
    margin-bottom: 15px;
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.REG};
      margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XXXL};
    }
  }
  .reason-code-rectangle {
    min-height: 59px;
    border-radius: 8px;
    display: flex;
    align-items: center;
    justify-content: flex-start;
    border: solid 1px ${(props) => props.theme.colorPalette.gray[600]};
    cursor: pointer;
    box-sizing: border-box;
    background: ${(props) => props.theme.colors.WHITE};
    .textColor {
      color: ${(props) => props.theme.colors.PRIMARY.DARK};
    }
    padding: 10px;
  }

  .full-width {
    width: 100%;
  }

  .help-message-with-icon {
    display: flex;
  }

  .disable-message-with-icon {
    display: flex;
    @media ${(props) => props.theme.mediaQuery.large} {
      position: absolute;
      left: ${(props) => props.theme.spacing.LAYOUT_SPACING.LRGS};
    }
  }

  .review-button-tell-us-more {
    display: flex;
    justify-content: center;
  }

  .section-background {
    background: ${(props) => props.theme.colorPalette.gray[300]};
    padding: ${(props) => props.theme.spacing.LAYOUT_SPACING.XXS};
    margin-top: ${(props) => props.theme.spacing.LAYOUT_SPACING.SM};
    @media ${(props) => props.theme.mediaQuery.mediumMax} {
      margin-left: -15px;
      margin-right: -15px;
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      width: 100vw;
      position: relative;
      left: calc(-45vw + 50%);
    }
  }

  .disabled-area {
    @media ${(props) => props.theme.mediaQuery.large} {
      width: 100vw;
      position: relative;
      left: calc(-45vw + 50%);
    }
  }

  .disabled-content {
    @media ${(props) => props.theme.mediaQuery.large} {
      width: 90%;
      margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXL};
    }
  }

  .with-info-message {
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XXL};
    }
  }

  .header-container-nononline {
    margin-top: 0;
    @media ${(props) => props.theme.mediaQuery.large} {
      position: absolute;
      left: ${(props) => props.theme.spacing.LAYOUT_SPACING.LRGS};
    }
  }

  .edit-option {
    display: block;
    text-decoration: underline;
    color: ${(props) => props.theme.colors.PRIMARY.BLUE};
  }

  .no-decoration {
    text-decoration: none;
  }

  .card-details-summary {
    width: 100%;
    justify-content: space-between;
  }

  .card-info {
    display: flex;
    align-items: center;
  }

  .payment-details-header {
    margin-left: 0;
  }

  .order-items-container {
    max-width: 330px;
  }

  .contact-method {
    background-color: ${(props) => props.theme.colors.WHITE};
  }

  .contact-us-options {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      width: 100%;
    }
  }

  .email-help {
    display: flex;
    flex-wrap: wrap;
  }

  .email-address {
    color: ${(props) => props.theme.colors.PRIMARY.BLUE};
  }

  .back-button-link {
    @media ${(props) => props.theme.mediaQuery.large} {
      display: none;
    }
  }

  .left-arrow {
    border-color: ${(props) => props.theme.colors.PRIMARY.DARK};
    margin-right: 5px;
  }

  .multiple-items-text {
    @media ${(props) => props.theme.mediaQuery.mediumMax} {
      margin-top: 5px;
      padding-top: 0;
    }
  }

  .select-option-text {
    margin-right: 0;
  }

  .progressBarArea {
    @media ${(props) => props.theme.mediaQuery.large} {
      margin-right: ${(props) => props.theme.spacing.LAYOUT_SPACING.XL};
      margin-left: 0;
    }
  }

  .cls-height-fix {
    min-width: 330px;
    height: ${(props) => props.theme.spacing.LAYOUT_SPACING.XXL};
    display: flex;
    align-items: center;
    justify-content: center;
  }

  .side-area {
    margin: auto 0;
    width: 100%;
  }

  .order-tile-container {
    margin-bottom: ${(props) => props.theme.spacing.LAYOUT_SPACING.XS};
  }

  .payment-details-text {
    @media ${(props) => props.theme.mediaQuery.smallOnly} {
      font-size: ${(props) => props.theme.typography.fontSizes.fs12};
    }
  }

  & .top-header-container {
    margin-top: 0;
  }

  .blue-link {
    color: ${(props) => props.theme.colors.PRIMARY.BLUE};
    text-decoration: underline;
  }
  .steps {
    color: ${(props) => props.theme.colorPalette.blue[800]};
  }

  .no-progress-margin {
    margin-bottom: ${(props) => props.theme.spacing.LAYOUT_SPACING.XS};
  }
`;

export default styles;
