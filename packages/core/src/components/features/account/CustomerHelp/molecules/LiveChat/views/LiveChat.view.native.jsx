// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { getAPIConfig } from '@tcp/core/src/utils';
import {
  LIVECHAT_SESSION_KEYS,
  REASON_CODES,
} from '@tcp/core/src/components/features/account/CustomerHelp/CustomerHelp.constants';
import { appendToken } from '@tcp/core/src/utils/utils';
import InAppWebView from 'mobileapp/src/components/features/content/InAppWebView/views';
import ModalNative from '../../../../../../common/molecules/Modal/view/Modal';
import ModalHeaderCurved from '../../../../../../common/molecules/Modal/view/Modal.header.curved';
import { readCookie } from '../../../../../../../utils/utils.app';

const overrideCSS = {
  imageWrapper: `
    left: 92%;
    top: 22px;
    left: 91%;
    z-index: 1px;
    `,
  styledTouchableOpacity: `padding: 0;`,
  styledCrossImage: `
    margin: 0;
    width: 20px;
    height: 20px;
    `,
  topBar: `margin: 6px auto 2px auto;border-width:0`,
};

const getOrderItems = (purchasedItems) => {
  const items =
    purchasedItems && purchasedItems.length
      ? purchasedItems[0]?.items.map((item) => item?.productInfo?.lineNumber)
      : null;
  if (items && items.length) {
    return items ? items.toString() : '';
  }
  return null;
};

const getSelectedItems = (selectedOrderItems) => {
  const selectedItems = selectedOrderItems ? Object.entries(selectedOrderItems) : null;
  if (selectedItems && selectedItems.length) {
    const selectedOrderItemsList = selectedItems.map(([key]) => key);
    return selectedOrderItemsList ? selectedOrderItemsList.toString() : '';
  }
  return null;
};

const LiveChat = ({
  closeChatWindow,
  isOpenChat,
  navigation,
  isUserLoggedIn,
  selectedReasonCode,
  customerScore,
  orderDetailsData,
  selectedOrderItems,
}) => {
  const { orderNumber, emailAddress, purchasedItems } = orderDetailsData || {};
  const {
    REASON_CODE,
    INITIATE_LIVE_CHAT,
    ORDER_NUMBER,
    EMAIL_ADDRESS,
    CUSTOMER_SCORE,
    SELECTED_ITEMS,
    ITEM_NUMBERS,
  } = LIVECHAT_SESSION_KEYS;
  const { action } = selectedReasonCode;

  const extraParams = `${REASON_CODE}=${action}&${INITIATE_LIVE_CHAT}=${true}&${ORDER_NUMBER}=${orderNumber}&${EMAIL_ADDRESS}=${emailAddress}`;

  const { chatBotVersion, chatBotEnv, brandId, chatBotPageUrl } = getAPIConfig();
  const [sessionInfo, setSessionInfo] = React.useState('');

  const getSessionInfo = async () => {
    let sessionCookieData = '';
    if (isUserLoggedIn) {
      sessionCookieData = await readCookie('tcpSessionInfo');
    }
    return sessionCookieData;
  };

  const setSessionData = async () => {
    const sessionData = await getSessionInfo();
    setSessionInfo(sessionData);
  };

  React.useEffect(() => {
    (async () => {
      await setSessionData();
    })();
  }, [isUserLoggedIn]);

  let chatBotWebViewUrl = `${chatBotPageUrl}?${
    appendToken() ? 'token=tcprwd&' : ''
  }loggedIn=${isUserLoggedIn}&version=${chatBotVersion}&user=${sessionInfo}&env=${chatBotEnv}&brand=${brandId}&${extraParams}`;

  if (action === REASON_CODES.SOMETHING_ELSE) {
    chatBotWebViewUrl = `${chatBotWebViewUrl}&${CUSTOMER_SCORE}=${customerScore}`;
  } else if (action === REASON_CODES.RECEIVED_WRONG_ITEM || action === REASON_CODES.MISSING_ORDER) {
    chatBotWebViewUrl = `${chatBotWebViewUrl}&${ITEM_NUMBERS}=${getOrderItems(
      purchasedItems
    )}&${SELECTED_ITEMS}=${getSelectedItems(selectedOrderItems)}`;
  }

  return (
    <>
      <ModalNative
        isOpen={isOpenChat}
        onRequestClose={closeChatWindow}
        headingFontFamily="secondary"
        roundedModal
        clickOutsideToClose
        fontSize="fs16"
        topMargin="100px"
        overrideCSS={{
          modalCurvedContent: `
            border-top-left-radius: 12;
            border-top-right-radius: 12;
            `,
        }}
      >
        <ModalHeaderCurved onRequestClose={closeChatWindow} overrideCSS={overrideCSS} />

        <InAppWebView pageUrl={chatBotWebViewUrl} navigation={navigation} />
      </ModalNative>
    </>
  );
};

LiveChat.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  selectedReasonCode: PropTypes.shape({ action: PropTypes.string }),
  orderDetailsData: PropTypes.shape({
    orderNumber: PropTypes.string,
    emailAddress: PropTypes.string,
    purchasedItems: PropTypes.string,
  }),
  customerScore: PropTypes.string,
  selectedOrderItems: PropTypes.shape({}),
  closeChatWindow: PropTypes.func,
  isOpenChat: PropTypes.bool,
  isUserLoggedIn: PropTypes.bool,
  navigation: PropTypes.func.isRequired,
};
LiveChat.defaultProps = {
  closeChatWindow: () => {},
  selectedReasonCode: { action: '' },
  orderDetailsData: { orderNumber: '', emailAddress: '', purchasedItems: '' },
  selectedOrderItems: {},
  customerScore: '',
  isOpenChat: false,
  isUserLoggedIn: false,
};

export default LiveChat;
