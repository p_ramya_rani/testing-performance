// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { ReasonCodeWithIconVanilla } from '../views/ReasonCodeWithIcon.view';

describe('ReasonCodeWithIcon Template', () => {
  const props = {
    index: 1,
    title: 'An Existing Order',
    description: 'An Existing Order',
  };
  it('should render ReasonCodeWithIcon Template', () => {
    const component = shallow(<ReasonCodeWithIconVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
