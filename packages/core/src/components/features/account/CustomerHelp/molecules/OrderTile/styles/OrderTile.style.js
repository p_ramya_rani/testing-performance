// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  .container {
    margin-top: 18px;
    min-width: 330px;
    position: relative;
    display: flex;
    border-radius: 12px;
    border: 1px solid ${props => props.theme.colorPalette.gray[600]};
    padding-bottom: ${props => props.theme.spacing.ELEM_SPACING.XS};
  }

  .imageContainer {
    width: 118px;
    height: 129px;
    display: flex;
    align-items: center;
    justify-content: center;
  }

  .detailContainer {
    display: flex;
    flex-direction: column;
  }

  .itemContainer {
    margin-top: ${props => props.theme.spacing.ELEM_SPACING.SM};
    display: flex;
  }

  .item-disabled {
    opacity: 30%;
  }

  .tile-icon {
    color: blue;
    position: absolute;
    top: 10px;
    right: 10px;
  }

  .item-disable-message {
    width: 250px;
    padding: 0px ${props => props.theme.spacing.ELEM_SPACING.XS};
    height: 58px;
    margin: 40px auto 31px;
  }
  .totalItems {
    background-color: ${props => props.theme.colorPalette.blue.C900};
    border-radius: 22px;
    padding: ${props => props.theme.spacing.ELEM_SPACING.XXS}
      ${props => props.theme.spacing.ELEM_SPACING.XS};
    position: absolute;
  }
`;

export default styles;
