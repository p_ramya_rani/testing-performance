// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { Image, Anchor } from '@tcp/core/src/components/common/atoms';
import { getIconPath } from '@tcp/core/src/utils';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { setSessionStorage, removeSessionStorage } from '@tcp/core/src/utils/utils.web';
import {
  REASON_CODES,
  LIVECHAT_SESSION_KEYS,
} from '@tcp/core/src/components/features/account/CustomerHelp/CustomerHelp.constants';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import styles from '../styles/LiveChat.style';

const liveChatIcon = getIconPath('help-chat_sm');

function clearSessionStorage() {
  const {
    REASON_CODE,
    ORDER_NUMBER,
    EMAIL_ADDRESS,
    CUSTOMER_SCORE,
    ITEM_NUMBERS,
    SELECTED_ITEMS,
    COUPON_NUMBER,
    ORDER_TOTAL,
    RETURN_METHOD,
    ERROR_CODE,
  } = LIVECHAT_SESSION_KEYS;
  removeSessionStorage(REASON_CODE);
  removeSessionStorage(ORDER_NUMBER);
  removeSessionStorage(EMAIL_ADDRESS);
  removeSessionStorage(CUSTOMER_SCORE);
  removeSessionStorage(ITEM_NUMBERS);
  removeSessionStorage(SELECTED_ITEMS);
  removeSessionStorage(COUPON_NUMBER);
  removeSessionStorage(ORDER_TOTAL);
  removeSessionStorage(RETURN_METHOD);
  removeSessionStorage(ERROR_CODE);
}

function setOrderItems(selectedItemNumber) {
  const { ITEM_NUMBERS } = LIVECHAT_SESSION_KEYS;
  if (selectedItemNumber && selectedItemNumber.length) {
    const itemNumbers = selectedItemNumber ? selectedItemNumber.toString() : '';
    setSessionStorage({ key: ITEM_NUMBERS, value: itemNumbers });
  }
}

function selectedOrderItemList(selectedOrderItemsList, orderItems) {
  const selectedItemNumber = orderItems
    .map((orderGroup) => {
      if (orderGroup?.items && orderGroup?.items?.length > 0) {
        return orderGroup.items
          .map((item) => {
            if (item && selectedOrderItemsList.includes(item?.lineNumber?.toString())) {
              return item?.productInfo?.variantNo ? item.productInfo.variantNo : null;
            }
            return null;
          })
          .filter((item) => {
            return item;
          });
      }
      return [];
    })
    .reduce((prev, next) => {
      return prev.concat(next);
    });
  setOrderItems(selectedItemNumber);
}

function setSelectedItems(selectedOrderItems, orderItems) {
  const { SELECTED_ITEMS } = LIVECHAT_SESSION_KEYS;
  const selectedItems = selectedOrderItems ? Object.entries(selectedOrderItems) : null;
  if (selectedItems && selectedItems.length) {
    const selectedOrderItemsList = selectedItems.map(([key]) => key);
    const itemNumbers = selectedOrderItemsList ? selectedOrderItemsList.toString() : '';
    setSessionStorage({ key: SELECTED_ITEMS, value: itemNumbers });
    if (orderItems && orderItems.length > 0) {
      selectedOrderItemList(selectedOrderItemsList, orderItems);
    }
  }
}

const LiveChat = ({
  className,
  selectedReasonCode,
  customerScore,
  orderDetailsData,
  selectedOrderItems,
  labels,
  trackAnalyticsClick,
  couponData,
  selectedReturnMethod,
  errorLabel,
}) => {
  const { orderNumber, emailAddress, orderItems, summary } = orderDetailsData;
  const { id } = couponData;
  const {
    REASON_CODE,
    INITIATE_LIVE_CHAT,
    ORDER_NUMBER,
    EMAIL_ADDRESS,
    CUSTOMER_SCORE,
    COUPON_NUMBER,
    ORDER_TOTAL,
    RETURN_METHOD,
    ERROR_CODE,
  } = LIVECHAT_SESSION_KEYS;
  const { action } = selectedReasonCode;
  const setStorage = (e) => {
    e.preventDefault();
    clearSessionStorage();

    trackAnalyticsClick({
      initiateLiveChat_cm145: '1',
      customEvents: ['event145'],
    });

    setSessionStorage({ key: REASON_CODE, value: action });
    setSessionStorage({ key: INITIATE_LIVE_CHAT, value: true });
    setSessionStorage({ key: ORDER_NUMBER, value: orderNumber });
    setSessionStorage({ key: EMAIL_ADDRESS, value: emailAddress });
    if (action === REASON_CODES.SOMETHING_ELSE) {
      setSessionStorage({ key: CUSTOMER_SCORE, value: customerScore }); /* can ignore it */
    } else if (
      action === REASON_CODES.RECEIVED_WRONG_ITEM ||
      action === REASON_CODES.MISSING_ORDER
    ) {
      setSelectedItems(selectedOrderItems, orderItems);
    } else if (action === REASON_CODES.COUPON_ISSUE) {
      setSessionStorage({ key: COUPON_NUMBER, value: id });
    } else if (action === REASON_CODES.INITIATE_RETURN) {
      const { grandTotal, currencySymbol } = summary;
      const orderTotal = `${currencySymbol}${grandTotal}`;
      setSelectedItems(selectedOrderItems, orderItems);
      setSessionStorage({ key: ORDER_TOTAL, value: orderTotal });
      setSessionStorage({ key: RETURN_METHOD, value: selectedReturnMethod });
      setSessionStorage({ key: ERROR_CODE, value: errorLabel });
    }
    window.launchBot();
  };

  return (
    <div className={className}>
      <Image width="18px" height="18px" src={liveChatIcon} alt="" className="elem-pl-MED" />
      <Anchor
        noLink
        fontWeight="semibold"
        fontSize="fs14"
        fontFamily="secondary"
        color="text.dark"
        className="elem-pl-XS elem-pr-XS no-decoration live-chat"
        onClick={(e) => setStorage(e)}
      >
        {getLabelValue(labels, 'lbl_help_live_chat')}
      </Anchor>
    </div>
  );
};

LiveChat.propTypes = {
  labels: PropTypes.shape({}).isRequired,
  className: PropTypes.string,
  selectedReasonCode: PropTypes.shape({}),
  orderDetailsData: PropTypes.shape({}),
  customerScore: PropTypes.string,
  selectedOrderItems: PropTypes.shape({}),
  trackAnalyticsClick: PropTypes.func.isRequired,
  couponData: PropTypes.shape({}),
  selectedReturnMethod: PropTypes.string,
  errorLabel: PropTypes.string,
};
LiveChat.defaultProps = {
  className: '',
  selectedReasonCode: {},
  orderDetailsData: {},
  selectedOrderItems: {},
  customerScore: '',
  couponData: {},
  selectedReturnMethod: '',
  errorLabel: '',
};

export default withStyles(LiveChat, styles);
export { LiveChat as LiveChatVanilla };
