// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { LiveChatVanilla } from '../views/LiveChat.view';

jest.mock('@tcp/core/src/utils/utils.web', () => ({
  setSessionStorage: jest.fn().mockImplementation(() => true),
  removeSessionStorage: jest.fn().mockImplementation(() => true),
}));

window.launchBot = jest.fn(() => {
  return {};
});

describe('Anchor component', () => {
  it('renders correctly', () => {
    const props = {
      className: 'live-chat',
      selectedReasonCode: 'MISSING_ITEM',
      orderNumber: 'R44343434343',
      itemNumbers: '2232323234342,23233452344',
      customerScore: '12',
    };
    const component = shallow(<LiveChatVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('renders correctly', () => {
    const props = {
      className: 'live-chat-empty-props',
    };
    const component = shallow(<LiveChatVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('renders correctly and triggers onclick event', () => {
    const props = {
      className: 'live-chat-click',
      selectedReasonCode: 'MISSING_ITEM',
      orderNumber: 'R44343434343',
      itemNumbers: '2232323234342,23233452344',
      customerScore: '12',
      trackAnalyticsClick: jest.fn(),
    };
    const component = shallow(<LiveChatVanilla {...props} />);
    component.find('.live-chat').simulate('click', { preventDefault: jest.fn() });
    expect(window.launchBot).toHaveBeenCalled();
  });
});
