// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { ContactUsModalVanilla } from '../views/InfoContactus.view';

describe('Order tile', () => {
  const props = {
    labels: {},
  };

  it('should render contact us', () => {
    const component = shallow(<ContactUsModalVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('should render contact us with updated props onRecentOrderPage true', () => {
    const component = shallow(<ContactUsModalVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
