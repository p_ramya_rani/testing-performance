// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { BodyCopy, Col, Image } from '../../../../../../common/atoms';
import withStyles from '../../../../../../common/hoc/withStyles';
import styles from '../styles/ReasonCodeWithIcon.style';
import { KEY_CODES } from '../../../../../../../constants/keyboard.constants';

const tabIndex = 0;
const role = 'button';

const getInteractiveProps = (onClick, onKeyDown) => {
  return {
    ...(onClick && { onClick, onKeyDown, tabIndex, role }),
  };
};

function ReasonCodeWithIcon({
  index,
  title,
  description,
  onClick,
  className,
  icon,
  fallbackImg,
  marginClasses,
  selectedReasonCodeClasses,
  containerId,
  titleClasses,
  fontSize,
  fontWeight,
  titleTextColor,
}) {
  const onKeyDown = (e) => {
    return e.keyCode === KEY_CODES.KEY_ENTER && onClick(e);
  };
  const interactiveProps = getInteractiveProps(onClick, onKeyDown);

  return (
    <Col
      colSize={{ small: 6, medium: 4, large: 4 }}
      className={`${className} ${marginClasses}`}
      ignoreGutter={(index + 1) % 3 === 0 ? { large: true } : {}}
      id={containerId}
    >
      <div
        className={`reason-code-rectangle reason-code-container ${selectedReasonCodeClasses}`}
        {...interactiveProps}
      >
        <Image
          width="28px"
          height="28px"
          src={icon || fallbackImg}
          alt="reason-code-icon"
          className="elem-pl-MED reason-code-icon"
        />
        <div className="reason-code-text-container" id="top-lvl-reason-codes">
          <BodyCopy
            component="p"
            textAlign="left"
            fontWeight={fontWeight || 'bold'}
            fontSize={fontSize || 'fs14'}
            fontFamily="secondary"
            color={titleTextColor || 'black'}
            className={titleClasses || 'elem-pl-MED elem-mb-XXS'}
          >
            {title}
          </BodyCopy>
          {description && (
            <BodyCopy
              component="p"
              textAlign="left"
              fontWeight="semibold"
              fontSize="fs12"
              fontFamily="secondary"
              color="text.dark"
              className="elem-pl-MED elem-pr-MED"
            >
              {description}
            </BodyCopy>
          )}
        </div>
      </div>
    </Col>
  );
}
ReasonCodeWithIcon.propTypes = {
  marginClasses: PropTypes.string,
  selectedReasonCodeClasses: PropTypes.string,
  containerId: PropTypes.string,
  index: PropTypes.number,
  title: PropTypes.string.isRequired,
  description: PropTypes.string,
  onClick: PropTypes.func,
  className: PropTypes.string,
  icon: PropTypes.string,
  fallbackImg: PropTypes.string,
  titleClasses: PropTypes.string,
  fontSize: PropTypes.string,
  fontWeight: PropTypes.string,
  titleTextColor: PropTypes.string,
};
ReasonCodeWithIcon.defaultProps = {
  marginClasses: '',
  selectedReasonCodeClasses: '',
  containerId: '',
  index: 0,
  onClick: () => {},
  className: '',
  icon: '',
  fallbackImg: '',
  description: '',
  titleClasses: '',
  fontSize: '',
  fontWeight: '',
  titleTextColor: '',
};

export default withStyles(ReasonCodeWithIcon, styles);
export { ReasonCodeWithIcon as ReasonCodeWithIconVanilla };
