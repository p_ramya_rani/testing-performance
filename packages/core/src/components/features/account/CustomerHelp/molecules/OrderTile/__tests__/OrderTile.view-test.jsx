// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { OrderTileVanilla } from '../views/OrderTile.view';

describe('Order tile', () => {
  const RealDate = Date;

  function mockDate(isoDate) {
    global.Date = class extends RealDate {
      constructor() {
        return new RealDate(isoDate);
      }
    };
  }

  afterEach(() => {
    global.Date = RealDate;
  });

  beforeEach(() => {
    mockDate('2020-02-25 00:00:00');
  });

  const props = {
    ordersListItems: {
      orders: [],
    },
    orderLabels: {},
    labels: {},
    orderDetailsData: {
      isEcomOrder: false,
      isOlderitem: false,
      onRecentOrderPage: false,
    },
    selected: false,
  };

  it('should render Order tile', () => {
    const component = shallow(<OrderTileVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should render Order tile with updated props', () => {
    props.orderDetailsData.orderType = 'ECOM';
    props.orderDetailsData.ItemsCount = ['1'];
    props.orderDetailsData.grandTotal = 20;

    const component = shallow(<OrderTileVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should render Order tile with updated props onRecentOrderPage true', () => {
    props.orderDetailsData.onRecentOrderPage = true;
    const component = shallow(<OrderTileVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
});
