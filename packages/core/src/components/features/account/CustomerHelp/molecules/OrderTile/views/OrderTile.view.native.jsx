// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState } from 'react';
import { TouchableOpacity, Linking } from 'react-native';
import PropTypes from 'prop-types';
import toLower from 'lodash/toLower';
import { getLabelValue, getBrand } from '@tcp/core/src/utils/utils';
import { Image, BodyCopy } from '@tcp/core/src/components/common/atoms';
import {
  PRODUCT_IMAGE_HEIGHT,
  PRODUCT_IMAGE_WIDTH,
  ORDER_ITEM_STATUS,
  BRAND_ID,
} from '../../../CustomerHelp.constants';
import {
  ImageContainer,
  DetailContainer,
  ItemContainer,
  TotalItems,
  SideBySideView,
  OrderTileContainer,
  TileIcons,
  ItemDisabled,
  ItemDisabledMessage,
  WrapContainer,
  ContactLink,
} from '../../../styles/CustomerHelp.style.native';

const crossIcon = require('../../../../../../../../../mobileapp/src/assets/images/info-icon.png');
const closeIcon = require('../../../../../../../../../mobileapp/src/assets/images/close.png');

const ContactText = ({ labels }) => {
  const brandId = getBrand();
  const customerServiceHref =
    brandId === BRAND_ID.TCP
      ? getLabelValue(labels, 'lbl_contact_us_tcp_prod_href')
      : getLabelValue(labels, 'lbl_contact_us_gym_prod_href');
  return (
    <ContactLink onPress={() => Linking.openURL(customerServiceHref)}>
      {` ${getLabelValue(labels, 'lbl_contact_us_directly', 'orders')} `}
    </ContactLink>
  );
};

const getCommonElements = (
  isEcomOrder,
  labels,
  orderTotalNonZero,
  isOlderItem,
  { isAppeasementAppliedOnOrder, isOrderCancelled, orderLabels }
) => {
  if (isOlderItem) {
    return (
      <>
        <ItemDisabledMessage>
          {getLabelValue(labels, 'lbl_older_disabled_initial', 'orders')}
          <ContactText labels={labels} />
          {getLabelValue(labels, 'lbl_older_disabled_ending', 'orders')}
        </ItemDisabledMessage>
      </>
    );
  }
  if (isAppeasementAppliedOnOrder || !orderTotalNonZero) {
    return (
      <>
        <ItemDisabledMessage>
          {getLabelValue(orderLabels, 'lbl_csh_refund_disabled_initial', 'orders')}
          <ContactText labels={labels} />
          {getLabelValue(orderLabels, 'lbl_csh_refund_disabled_ending', 'orders')}
        </ItemDisabledMessage>
      </>
    );
  }
  if (isOrderCancelled) {
    return (
      <>
        <ItemDisabledMessage>
          {getLabelValue(orderLabels, 'lbl_csh_cancelled_disabled_initial', 'orders')}
          <ContactText labels={labels} />
          {getLabelValue(orderLabels, 'lbl_csh_cancelled_disabled_ending', 'orders')}
        </ItemDisabledMessage>
      </>
    );
  }
  if (!isEcomOrder) {
    return (
      <>
        <ItemDisabledMessage>
          {getLabelValue(labels, 'lbl_pickup_disabled_initial', 'orders')}
          <ContactText labels={labels} />
          {getLabelValue(labels, 'lbl_pickup_disabled_ending', 'orders')}
        </ItemDisabledMessage>
      </>
    );
  }
  return (
    <>
      <ItemDisabledMessage>
        {getLabelValue(labels, 'lbl_older_disabled_initial', 'orders')}
        <ContactText labels={labels} />
        {getLabelValue(labels, 'lbl_older_disabled_ending', 'orders')}
      </ItemDisabledMessage>
    </>
  );
};

const renderDisableState = (orderDetailsData, orderLabels, labels) => {
  const {
    imagePath,
    productName,
    orderNumber,
    orderDate,
    status,
    ItemsCount = 0,
    grandTotal,
    currencySymbol,
  } = orderDetailsData;
  const wrapStyle = { flexShrink: 1, flex: 1 };
  return (
    <SideBySideView>
      <ImageContainer>
        <Image
          url={imagePath}
          height={PRODUCT_IMAGE_HEIGHT}
          width={PRODUCT_IMAGE_WIDTH}
          alt={productName}
        />

        {ItemsCount > 0 && (
          <TotalItems>
            <BodyCopy
              fontFamily="secondary"
              fontSize="fs12"
              fontWeight="semibold"
              color="white"
              text={`${getLabelValue(orderLabels, 'lbl_orders_totalItems', 'orders')}${ItemsCount}`}
            />
          </TotalItems>
        )}
      </ImageContainer>
      <DetailContainer>
        <ItemContainer>
          <BodyCopy
            fontFamily="primary"
            fontSize="fs14"
            fontWeight="semibold"
            spacingStyles="margin-bottom-XS"
            text={`${getLabelValue(orderLabels, 'lbl_orders_orderNumber', 'orders')}: `}
          />

          <BodyCopy
            fontFamily="primary"
            fontSize="fs14"
            spacingStyles="margin-bottom-XS"
            text={orderNumber}
          />
        </ItemContainer>

        <ItemContainer>
          <BodyCopy
            fontFamily="primary"
            fontSize="fs14"
            fontWeight="semibold"
            spacingStyles="margin-bottom-XS"
            text={`${getLabelValue(orderLabels, 'lbl_orders_orderDate', 'orders')}: `}
          />

          <BodyCopy
            fontFamily="primary"
            fontSize="fs14"
            spacingStyles="margin-bottom-XS"
            text={orderDate}
          />
        </ItemContainer>

        <ItemContainer>
          <BodyCopy
            fontFamily="primary"
            fontSize="fs14"
            fontWeight="semibold"
            spacingStyles="margin-bottom-XS"
            text={`${getLabelValue(labels, 'lbl_order_status_text')}: `}
          />

          <WrapContainer width="170">
            <BodyCopy
              style={{ ...wrapStyle }}
              fontFamily="primary"
              fontSize="fs14"
              spacingStyles="margin-bottom-XS"
              text={getLabelValue(orderLabels, status, 'orders')}
            />
          </WrapContainer>
        </ItemContainer>

        <ItemContainer>
          <BodyCopy
            fontFamily="primary"
            fontSize="fs14"
            fontWeight="semibold"
            spacingStyles="margin-bottom-XS"
            text={`${getLabelValue(orderLabels, 'lbl_orders_orderTotal', 'orders')}: `}
          />

          <BodyCopy
            fontFamily="primary"
            fontSize="fs14"
            spacingStyles="margin-bottom-XS"
            text={
              grandTotal
                ? `${currencySymbol}${grandTotal.toFixed(2)}`
                : `${currencySymbol}${getLabelValue(
                    orderLabels,
                    'lbl_orders_freeAmount',
                    'orders'
                  )}`
            }
          />
        </ItemContainer>
      </DetailContainer>
    </SideBySideView>
  );
};

const renderEnableState = (
  isEcomOrder,
  labels,
  orderTotalNonZero,
  isOlderItem,
  { isAppeasementAppliedOnOrder, isOrderCancelled, orderLabels }
) =>
  getCommonElements(isEcomOrder, labels, orderTotalNonZero, isOlderItem, {
    isAppeasementAppliedOnOrder,
    isOrderCancelled,
    orderLabels,
  });

const getIsOrderCancelled = (orderStatus, status) => {
  const { CANCELLED, CANCELED } = ORDER_ITEM_STATUS;
  return (
    toLower(orderStatus) === toLower(CANCELLED) ||
    toLower(orderStatus) === toLower(CANCELED) ||
    toLower(status) === toLower(CANCELLED) ||
    toLower(status) === toLower(CANCELED)
  );
};

const getIsDisabled = (isNonOnlineOrOlderOrder, isAppeasementAppliedOnOrder, isOrderCancelled) => {
  return isNonOnlineOrOlderOrder || isAppeasementAppliedOnOrder || isOrderCancelled;
};

const OrderTile = ({
  orderDetailsData,
  orderLabels,
  labels,
  selected,
  item,
  onPressHandler,
  isNonOnlineOrOlderOrder,
  isAppeasementAppliedOnOrder,
}) => {
  const { isEcomOrder, isOlderItem, onRecentOrderPage, orderStatus, status, grandTotal } =
    orderDetailsData || {};
  const [itemDisabled, setItemDisabled] = useState(false);
  const [iconLink, seticonLink] = useState(true);
  const isOrderCancelled = getIsOrderCancelled(orderStatus, status);
  const orderTotalNonZero = !!grandTotal;

  const isDisabled =
    (onRecentOrderPage && (!isEcomOrder || isOlderItem)) ||
    !orderTotalNonZero ||
    getIsDisabled(isNonOnlineOrOlderOrder, isAppeasementAppliedOnOrder, isOrderCancelled);

  const toggleDisableReason = () => {
    setItemDisabled(!itemDisabled);
    seticonLink(!iconLink);
  };
  const showDisableReason = () => {
    if (isDisabled) {
      toggleDisableReason();
    }

    if (!isDisabled && onRecentOrderPage) {
      onPressHandler(item);
    }
  };

  const getContainerName = () => {
    if (iconLink && isDisabled) {
      return ItemDisabled;
    }
    return TouchableOpacity;
  };

  const SelectedContainer = getContainerName();

  return (
    <>
      {orderDetailsData && (
        <SelectedContainer onPress={() => showDisableReason()}>
          <OrderTileContainer selected={selected}>
            {isDisabled && (
              <TileIcons>
                {iconLink ? (
                  <Image width="20px" height="20px" source={crossIcon} alt="info" />
                ) : (
                  <Image width="15px" height="15px" source={closeIcon} alt="close" />
                )}
              </TileIcons>
            )}

            {itemDisabled
              ? renderEnableState(isEcomOrder, labels, orderTotalNonZero, isOlderItem, {
                  isAppeasementAppliedOnOrder,
                  isOrderCancelled,
                  orderLabels,
                })
              : renderDisableState(orderDetailsData, orderLabels, labels)}
          </OrderTileContainer>
        </SelectedContainer>
      )}
    </>
  );
};

ContactText.propTypes = {
  labels: PropTypes.shape({}).isRequired,
};

OrderTile.propTypes = {
  orderDetailsData: PropTypes.shape({}).isRequired,
  labels: PropTypes.shape({}).isRequired,
  orderLabels: PropTypes.shape({}).isRequired,
  selected: PropTypes.bool,
  item: PropTypes.shape({}),
  onPressHandler: PropTypes.func,
  isNonOnlineOrOlderOrder: PropTypes.bool,
  isAppeasementAppliedOnOrder: PropTypes.bool,
};
OrderTile.defaultProps = {
  selected: false,
  item: {},
  onPressHandler: () => {},
  isNonOnlineOrOlderOrder: false,
  isAppeasementAppliedOnOrder: false,
};

export default OrderTile;
