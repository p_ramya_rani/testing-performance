// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import CookieManager from 'react-native-cookies';
import InAppWebView from 'mobileapp/src/components/features/content/InAppWebView/views';
import { REASON_CODES } from '@tcp/core/src/components/features/account/CustomerHelp/CustomerHelp.constants';
import Modal from '@tcp/core/src/components/common/molecules/Modal';
import * as utils from '@tcp/core/src/utils/utils';
import LiveChat from '../views/LiveChat.view.native';
import { readCookie } from '../../../../../../../utils/utils.app';

const utilsLocation = '@tcp/core/src/utils/utils';
jest.mock('@tcp/core/src/utils', () => {
  return {
    readCookie: jest.fn().mockImplementation(() => 'tcpSessionInfo'),
  };
});
jest.mock(utilsLocation);
const dummydomain = 'https://www.childrensplace.com/rwd/chatbot.html';
const apiConfig = {
  chatBotVersion: '1',
  chatBotEnv: 'uat',
  brandId: 'tcp',
  chatBotPageUrl: dummydomain,
  domain: 'https://test-uat1.childrensplace.com/api/',
};
const navigation = {
  navigate: () => {},
  getParam: () => {
    return {};
  },
};
utils.appendToken = jest.fn(() => {
  return true;
});
utils.getAPIConfig = jest.fn(() => {
  return apiConfig;
});
describe('Live Chat', () => {
  let useEffect;
  const mockUseEffect = () => {
    useEffect.mockImplementationOnce((f) => f());
  };
  beforeEach(() => {
    useEffect = jest.spyOn(React, 'useEffect');
    mockUseEffect();
  });
  const baseUrl =
    'https://www.childrensplace.com/rwd/chatbot.html?token=tcprwd&loggedIn=true&version=1&user=&env=uat&brand=tcp&';
  let wrapper;
  const itemNumber = '2232323234342,23233452344';
  const email = 'abc@gmail.com';
  it('should render live chat', async () => {
    const props = {
      closeChatWindow: () => {},
      isOpenChat: true,
      navigation,
      isUserLoggedIn: true,
      selectedReasonCode: { action: REASON_CODES.SOMETHING_ELSE },
      customerScore: '12',
      orderDetailsData: { orderNumber: 'R44343434343', emailAddress: email, purchasedItems: [] },
      selectedOrderItems: {},
      itemNumbers: itemNumber,
    };
    wrapper = shallow(<LiveChat {...props} />);
    expect(wrapper.children).toHaveLength(1);
    expect(wrapper).toMatchSnapshot();
  });
  it('should open Modal when Chat is clicked', () => {
    CookieManager.get = jest.fn().mockImplementation(() => {
      return {
        tcpSessionInfo: 'abc',
      };
    });
    expect(readCookie('tcpSessionInfo')).resolves.toBeTruthy();
    const setState = jest.fn();
    const useStateSpy = jest.spyOn(React, 'useState');
    useStateSpy.mockImplementation((init) => [init, setState]);
    expect(wrapper.find(Modal).props().children).toHaveLength(2);
  });
  it('should close Modal when close icon clicked', () => {
    const setState = jest.fn();
    const useStateSpy = jest.spyOn(React, 'useState');
    useStateSpy.mockImplementation((init) => [init, setState]);
    const props = {
      closeChatWindow: () => {},
      isOpenChat: false,
      navigation,
      isUserLoggedIn: true,
      selectedReasonCode: { action: REASON_CODES.SOMETHING_ELSE },
      customerScore: '12',
      orderDetailsData: { orderNumber: 'R44343434343', emailAddress: email, purchasedItems: [] },
      selectedOrderItems: {},
      itemNumbers: itemNumber,
    };
    wrapper = shallow(<LiveChat {...props} />);
    expect(wrapper.find(Modal).first().props().isOpen).toEqual(false);
  });
  it('should open LiveChat with "SOMETHING_ELSE" reasonCode', () => {
    const props = {
      closeChatWindow: () => {},
      isOpenChat: true,
      navigation: () => {},
      isUserLoggedIn: true,
      selectedReasonCode: { action: REASON_CODES.SOMETHING_ELSE },
      orderDetailsData: { orderNumber: 'R44343434343', emailAddress: email, purchasedItems: [] },
      selectedOrderItems: {},
    };
    wrapper = shallow(<LiveChat {...props} />);
    expect(wrapper.find(InAppWebView).first().props().pageUrl).toEqual(
      `${baseUrl}reasonCode=SOMETHING_ELSE&initiateLiveChat=true&orderNumber=R44343434343&emailID=abc@gmail.com&customerScore=`
    );
    expect(wrapper).toMatchSnapshot();
  });
  it('should open LiveChat with "WRONG_ITEMS" reasonCode', () => {
    const props = {
      closeChatWindow: () => {},
      isOpenChat: true,
      navigation: () => {},
      isUserLoggedIn: true,
      selectedReasonCode: { action: REASON_CODES.RECEIVED_WRONG_ITEM },
      itemNumbers: itemNumber,
      orderDetailsData: {
        orderNumber: 'R44343434343',
        emailAddress: email,
        purchasedItems: [
          {
            items: [
              {
                productInfo: {
                  lineNumber: '1517665022',
                },
              },
            ],
          },
        ],
      },
      selectedOrderItems: {},
    };
    wrapper = shallow(<LiveChat {...props} />);
    expect(wrapper.find(InAppWebView).first().props().pageUrl).toEqual(
      `${baseUrl}reasonCode=WRONG_ITEMS&initiateLiveChat=true&orderNumber=R44343434343&emailID=abc@gmail.com&itemNumbers=1517665022&selectedItems=null`
    );
    expect(wrapper).toMatchSnapshot();
  });
  it('should open LiveChat with "ITEM_MISSING" reasonCode', () => {
    const props = {
      closeChatWindow: () => {},
      isOpenChat: true,
      navigation: () => {},
      isUserLoggedIn: true,
      selectedReasonCode: { action: REASON_CODES.MISSING_ORDER },
      itemNumbers: itemNumber,
      orderDetailsData: {
        orderNumber: 'R44343434343',
        emailAddress: email,
        purchasedItems: [
          {
            items: [
              {
                productInfo: {
                  lineNumber: '1517665022',
                },
              },
            ],
          },
        ],
      },
      selectedOrderItems: {},
    };
    wrapper = shallow(<LiveChat {...props} />);
    expect(wrapper.find(InAppWebView).first().props().pageUrl).toEqual(
      `${baseUrl}reasonCode=ITEM_MISSING&initiateLiveChat=true&orderNumber=R44343434343&emailID=abc@gmail.com&itemNumbers=1517665022&selectedItems=null`
    );
    expect(wrapper).toMatchSnapshot();
  });
  it('should open LiveChat with "ITEM_MISSING" reasonCode and ITEM-NUMBER NULL and selectedItems NULL', () => {
    const props = {
      closeChatWindow: () => {},
      isOpenChat: true,
      navigation: () => {},
      isUserLoggedIn: true,
      selectedReasonCode: { action: REASON_CODES.MISSING_ORDER },
      itemNumbers: itemNumber,
      orderDetailsData: { orderNumber: 'R44343434343', emailAddress: email, purchasedItems: [] },
      selectedOrderItems: [],
    };
    wrapper = shallow(<LiveChat {...props} />);
    expect(wrapper.find(InAppWebView).first().props().pageUrl).toEqual(
      `${baseUrl}reasonCode=ITEM_MISSING&initiateLiveChat=true&orderNumber=R44343434343&emailID=abc@gmail.com&itemNumbers=null&selectedItems=null`
    );
    expect(wrapper).toMatchSnapshot();
  });
  it('should open LiveChat with "ITEM_MISSING" reasonCode and ITEM-NUMBER value and selectedItems value', () => {
    const props = {
      closeChatWindow: () => {},
      isOpenChat: true,
      navigation: () => {},
      isUserLoggedIn: true,
      selectedReasonCode: { action: REASON_CODES.MISSING_ORDER },
      itemNumbers: itemNumber,
      orderDetailsData: {
        orderNumber: 'R44343434343',
        emailAddress: email,
        purchasedItems: [
          {
            items: [
              {
                productInfo: {
                  lineNumber: '1517665022',
                },
              },
            ],
          },
        ],
      },
      selectedOrderItems: [123, 456],
    };
    wrapper = shallow(<LiveChat {...props} />);
    expect(wrapper.find(InAppWebView).first().props().pageUrl).toEqual(
      `${baseUrl}reasonCode=ITEM_MISSING&initiateLiveChat=true&orderNumber=R44343434343&emailID=abc@gmail.com&itemNumbers=1517665022&selectedItems=0,1`
    );
    expect(wrapper).toMatchSnapshot();
  });
});
