// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { PropTypes } from 'prop-types';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import { BodyCopy, Row, Col } from '@tcp/core/src/components/common/atoms';
import withStyles from '../../../../../../common/hoc/withStyles';
import Modal from '../../../../../../common/molecules/Modal';
import styles from '../styles/InfoContactUsModal.style';
import CustomerServiceHelp from '../../../organisms/TellUsMore/organisms/CustomerServiceHelp';

const ContactUsModal = ({ modalState, labels, onClose, className }) => {
  return (
    <Modal
      fixedWidth
      isOpen={modalState}
      onRequestClose={onClose}
      overlayClassName="TCPModal__Overlay"
      maxWidth="450px"
      standardHeight
    >
      <BodyCopy component="div" className={className}>
        <BodyCopy
          className="modal-align"
          fontSize="fs16"
          fontWeight="bold"
          fontFamily="secondary"
          component="p"
        >
          {getLabelValue(labels, 'lbl_connect_directly_specialist')}
        </BodyCopy>

        <Row fullBleed>
          <Col colSize={{ small: 6, medium: 8, large: 12 }}>
            <CustomerServiceHelp labels={labels} />
          </Col>
        </Row>
      </BodyCopy>
    </Modal>
  );
};

ContactUsModal.propTypes = {
  modalState: PropTypes.bool.isRequired,
  labels: PropTypes.shape({}).isRequired,
  onClose: PropTypes.func.isRequired,
  className: PropTypes.string,
};
ContactUsModal.defaultProps = {
  className: '',
};
export default withStyles(ContactUsModal, styles);
export { ContactUsModal as ContactUsModalVanilla };
