// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import toLower from 'lodash/toLower';
import { getLabelValue, getFormatedOrderDate } from '@tcp/core/src/utils/utils';
import { getIconPath, configureInternalNavigationFromCMSUrl } from '@tcp/core/src/utils';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { Image, BodyCopy, Anchor } from '@tcp/core/src/components/common/atoms';
import {
  ORDER_ITEM_STATUS,
  ORDER_STATUS,
} from '@tcp/core/src/components/features/account/CustomerHelp/CustomerHelp.constants';
import styles from '../styles/OrderTile.style';
import InfoContactUsModal from '../../InfoContactus';

const fallbackImg = getIconPath('essential-style-fallback');

const setModalVisible = (setmodalState) => {
  setmodalState(true);
};

const getOrderStatus = (orderLabels, status) => {
  const orderStatusLowerCase = status?.toLowerCase().trim();
  if (
    orderStatusLowerCase === ORDER_ITEM_STATUS.ORDER_PROCESSING ||
    orderStatusLowerCase === ORDER_ITEM_STATUS.ORDER_BEING_PROCESED ||
    orderStatusLowerCase === ORDER_ITEM_STATUS.ORDER_IN_PROCESS
  ) {
    return getLabelValue(orderLabels, 'lbl_order_being_processed', 'orders');
  }

  if (orderStatusLowerCase === ORDER_STATUS.ORDER_PARTIALLY_SHIPPED) {
    return getLabelValue(orderLabels, 'lbl_orders_statusOrderPartiallyShipped', 'orders');
  }
  return getLabelValue(orderLabels, status, 'orders');
};

const renderDisableState = (orderDetailsData, orderLabels, labels) => {
  const {
    imagePath,
    productName,
    orderNumber,
    orderDate,
    ItemsCount = 0,
    grandTotal,
    currencySymbol,
    status,
  } = orderDetailsData;
  return (
    <>
      <div className="imageContainer">
        <Image width="79px" height="91px" src={imagePath || fallbackImg} alt={productName} />
        {ItemsCount > 0 && (
          <div className="totalItems">
            <BodyCopy color="white" fontSize="fs12" fontFamily="secondary" component="p">
              {`${getLabelValue(orderLabels, 'lbl_orders_totalItems', 'orders')}${ItemsCount}`}
            </BodyCopy>
          </div>
        )}
      </div>
      <div className="detailContainer">
        <BodyCopy className="itemContainer" component="div">
          <BodyCopy fontWeight="extrabold" fontSize="fs14" fontFamily="secondary" component="span">
            {`${getLabelValue(orderLabels, 'lbl_orders_orderNumber', 'orders')}:`}
          </BodyCopy>
          <BodyCopy
            fontSize="fs14"
            fontFamily="secondary"
            component="span"
            className="elem-ml-XXS"
            fontWeight="Regular"
          >
            {orderNumber}
          </BodyCopy>
        </BodyCopy>

        <BodyCopy className="itemContainer" component="div">
          <BodyCopy fontWeight="extrabold" fontSize="fs14" fontFamily="secondary" component="span">
            {`${getLabelValue(orderLabels, 'lbl_orders_orderDate', 'orders')}:`}
          </BodyCopy>
          <BodyCopy
            fontSize="fs14"
            fontFamily="secondary"
            component="span"
            className="elem-ml-XXS"
            fontWeight="Regular"
          >
            {getFormatedOrderDate(orderDate, { month: 'short' })}
          </BodyCopy>
        </BodyCopy>

        <BodyCopy className="itemContainer" component="div">
          <BodyCopy fontWeight="extrabold" fontSize="fs14" fontFamily="secondary" component="span">
            {`${getLabelValue(labels, 'lbl_order_status_text')}:`}
          </BodyCopy>
          <BodyCopy
            fontSize="fs14"
            fontFamily="secondary"
            component="span"
            className="elem-ml-XXS"
            fontWeight="Regular"
          >
            {status === ORDER_ITEM_STATUS.CANCELLED
              ? getLabelValue(labels, 'lbl_order_cancelled', 'orders')
              : getOrderStatus(orderLabels, status)}
          </BodyCopy>
        </BodyCopy>

        <BodyCopy className="itemContainer" component="div">
          <BodyCopy fontWeight="extrabold" fontSize="fs14" fontFamily="secondary" component="span">
            {`${getLabelValue(orderLabels, 'lbl_orders_orderTotal', 'orders')}:`}
          </BodyCopy>
          <BodyCopy
            fontSize="fs14"
            fontFamily="secondary"
            component="span"
            className="elem-ml-XXS"
            fontWeight="Regular"
          >
            {grandTotal
              ? `${currencySymbol}${grandTotal.toFixed(2)}`
              : `${currencySymbol}${getLabelValue(orderLabels, 'lbl_orders_freeAmount', 'orders')}`}
          </BodyCopy>
        </BodyCopy>
      </div>
    </>
  );
};

const getCommonElements = (
  isEcomOrder,
  labels,
  setmodalState,
  orderTotalNonZero,
  isOlderItem,
  { isAppeasementAppliedOnOrder, isOrderCancelled, orderLabels }
) => {
  const contactUsLink = getLabelValue(orderLabels, 'lbl_csh_contact_us_here_link', 'orders');
  if (isOlderItem) {
    return (
      <>
        <BodyCopy textAlign="center">
          {getLabelValue(labels, 'lbl_older_disabled_initial', '')}
          &nbsp;
          <Anchor
            className="blue-link"
            underline
            fontFamily="secondary"
            fontSize="fs16"
            noLink
            onClick={(e) => {
              setModalVisible(setmodalState);
              e.preventDefault();
            }}
          >
            {getLabelValue(labels, 'lbl_contact_us_directly', 'orders')}
          </Anchor>
          &nbsp;
          {getLabelValue(labels, 'lbl_older_disabled_ending', 'orders')}
        </BodyCopy>
      </>
    );
  }
  if (isAppeasementAppliedOnOrder || !orderTotalNonZero) {
    return (
      <>
        <BodyCopy textAlign="center" onClick={() => setModalVisible(setmodalState)}>
          {getLabelValue(orderLabels, 'lbl_csh_refund_disabled_initial', 'orders')}
          &nbsp;
          <Anchor
            className="blue-link"
            underline
            to={configureInternalNavigationFromCMSUrl(contactUsLink)}
            asPath={contactUsLink}
          >
            {getLabelValue(labels, 'lbl_contact_us_directly', 'orders')}
          </Anchor>
          &nbsp;
          {getLabelValue(orderLabels, 'lbl_csh_refund_disabled_ending', 'orders')}
        </BodyCopy>
      </>
    );
  }
  if (isOrderCancelled) {
    return (
      <>
        <BodyCopy textAlign="center" onClick={() => setModalVisible(setmodalState)}>
          {getLabelValue(orderLabels, 'lbl_csh_cancelled_disabled_initial', 'orders')}
          &nbsp;
          <Anchor
            className="blue-link"
            underline
            to={configureInternalNavigationFromCMSUrl(contactUsLink)}
            asPath={contactUsLink}
          >
            {getLabelValue(labels, 'lbl_contact_us_directly', 'orders')}
          </Anchor>
          &nbsp;
          {getLabelValue(orderLabels, 'lbl_csh_cancelled_disabled_ending', 'orders')}
        </BodyCopy>
      </>
    );
  }
  if (!isEcomOrder) {
    return (
      <>
        <BodyCopy textAlign="center">
          {getLabelValue(orderLabels, 'lbl_csh_cancelled_disabled_initial', 'orders')}
          &nbsp;
          <Anchor
            className="blue-link"
            underline
            fontFamily="secondary"
            fontSize="fs16"
            noLink
            onClick={(e) => {
              setModalVisible(setmodalState);
              e.preventDefault();
            }}
          >
            {getLabelValue(labels, 'lbl_contact_us_directly', 'orders')}
          </Anchor>
          &nbsp;
          {getLabelValue(orderLabels, 'lbl_csh_cancelled_disabled_ending', 'orders')}
        </BodyCopy>
      </>
    );
  }
  return (
    <>
      <BodyCopy textAlign="center">
        {getLabelValue(orderLabels, 'lbl_csh_cancelled_disabled_initial', 'orders')}
        &nbsp;
        <Anchor
          className="blue-link"
          underline
          fontFamily="secondary"
          fontSize="fs16"
          noLink
          onClick={(e) => {
            setModalVisible(setmodalState);
            e.preventDefault();
          }}
        >
          {getLabelValue(labels, 'lbl_contact_us_directly', 'orders')}
        </Anchor>
        &nbsp;
        {getLabelValue(orderLabels, 'lbl_csh_cancelled_disabled_ending', 'orders')}
      </BodyCopy>
    </>
  );
};

const renderEnableState = (
  isEcomOrder,
  labels,
  setmodalState,
  orderTotalNonZero,
  isOlderItem,
  { isAppeasementAppliedOnOrder, isOrderCancelled, orderLabels }
) => {
  return (
    <BodyCopy
      className="item-disable-message"
      component="p"
      textAlign="center"
      fontWeight="semibold"
      fontSize="fs14"
      fontFamily="secondary"
      color="text.dark"
    >
      {getCommonElements(isEcomOrder, labels, setmodalState, orderTotalNonZero, isOlderItem, {
        isAppeasementAppliedOnOrder,
        isOrderCancelled,
        orderLabels,
      })}
    </BodyCopy>
  );
};

const getIsOrderCancelled = (orderStatus, status) => {
  const { CANCELLED, CANCELED } = ORDER_ITEM_STATUS;
  return (
    toLower(orderStatus) === toLower(CANCELLED) ||
    toLower(orderStatus) === toLower(CANCELED) ||
    toLower(status) === toLower(CANCELLED) ||
    toLower(status) === toLower(CANCELED)
  );
};

const getIsShowDisableReason = ({
  isDisabled,
  itemDisabled,
  isNonOnlineOrOlderOrder,
  fromSelectOrder,
  orderTotalNonZero,
}) => {
  return (
    isDisabled && !itemDisabled && !isNonOnlineOrOlderOrder && !fromSelectOrder && orderTotalNonZero
  );
};

const getIsDisabled = (
  isNonOnlineOrOlderOrder,
  isAppeasementAppliedOnOrder,
  isOrderCancelled,
  orderTotalNonZero
) => {
  return (
    isNonOnlineOrOlderOrder || isAppeasementAppliedOnOrder || isOrderCancelled || orderTotalNonZero
  );
};

const isDisabledItem = ({
  onRecentOrderPage,
  isEcomOrder,
  isOlderItem,
  orderTotalNonZero,
  isNonOnlineOrOlderOrder,
  isAppeasementAppliedOnOrder,
  isOrderCancelled,
}) => {
  return (
    (onRecentOrderPage && (!isEcomOrder || isOlderItem)) ||
    !orderTotalNonZero ||
    getIsDisabled(isNonOnlineOrOlderOrder, isAppeasementAppliedOnOrder, isOrderCancelled)
  );
};

const OrderTile = ({
  orderDetailsData,
  orderLabels,
  className,
  labels,
  selected,
  isNonOnlineOrOlderOrder,
  isAppeasementAppliedOnOrder,
  fromSelectOrder,
}) => {
  const { isEcomOrder, isOlderItem, onRecentOrderPage, orderStatus, status, grandTotal } =
    orderDetailsData || {};

  const [itemDisabled, setItemDisabled] = useState(false);
  const [iconlink, setIconLink] = useState(true);
  const [modalState, setmodalState] = useState(false);
  const [isOrderCancelled] = useState(getIsOrderCancelled(orderStatus, status));
  const orderTotalNonZero = !!grandTotal;
  const isDisabled = isDisabledItem({
    onRecentOrderPage,
    isEcomOrder,
    isOlderItem,
    orderTotalNonZero,
    isNonOnlineOrOlderOrder,
    isAppeasementAppliedOnOrder,
    isOrderCancelled,
  });

  const showDisableReason = () => {
    if (
      getIsShowDisableReason({
        isDisabled,
        itemDisabled,
        isNonOnlineOrOlderOrder,
        fromSelectOrder,
        orderTotalNonZero,
      })
    ) {
      setItemDisabled(true);
      setIconLink(false);
    }
  };

  const toggleDisableReason = () => {
    setItemDisabled(!itemDisabled);
    setIconLink(!iconlink);
  };

  const setClass = () => {
    return (iconlink && isDisabled && 'disabled-item') || (selected && 'selected-item') || '';
  };

  return (
    <>
      {orderDetailsData && (
        <div className={className}>
          {modalState ? (
            <InfoContactUsModal
              modalState={modalState}
              labels={labels}
              onClose={() => setmodalState(false)}
            />
          ) : (
            <div
              className={`container ${setClass()}`}
              role="presentation"
              onClick={() => showDisableReason()}
            >
              {isDisabled && (
                <BodyCopy className="tile-icon" onClick={() => toggleDisableReason()}>
                  {iconlink ? (
                    <Image
                      width="20px"
                      height="20px"
                      src={getIconPath('info-icon-blue')}
                      alt="info"
                    />
                  ) : (
                    <Image
                      width="20px"
                      height="20px"
                      src={getIconPath('circle-close-blue')}
                      alt="close"
                    />
                  )}
                </BodyCopy>
              )}
              {itemDisabled
                ? renderEnableState(
                    isEcomOrder,
                    labels,
                    setmodalState,
                    orderTotalNonZero,
                    isOlderItem,
                    {
                      isAppeasementAppliedOnOrder,
                      isOrderCancelled,
                      orderLabels,
                    }
                  )
                : renderDisableState(orderDetailsData, orderLabels, labels)}
            </div>
          )}
        </div>
      )}
    </>
  );
};

OrderTile.propTypes = {
  orderDetailsData: PropTypes.shape({}).isRequired,
  className: PropTypes.string,
  labels: PropTypes.shape({}).isRequired,
  orderLabels: PropTypes.shape({}).isRequired,
  isNonOnlineOrOlderOrder: PropTypes.bool,
  selected: PropTypes.bool,
  isAppeasementAppliedOnOrder: PropTypes.bool,
  fromSelectOrder: PropTypes.bool,
};
OrderTile.defaultProps = {
  className: '',
  selected: false,
  isNonOnlineOrOlderOrder: false,
  isAppeasementAppliedOnOrder: false,
  fromSelectOrder: false,
};

export default withStyles(OrderTile, styles);
export { OrderTile as OrderTileVanilla };
