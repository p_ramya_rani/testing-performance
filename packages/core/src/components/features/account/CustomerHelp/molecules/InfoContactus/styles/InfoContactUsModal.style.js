// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const styles = css`
  padding: ${props => props.theme.spacing.ELEM_SPACING.XXS};
  .reason-code-rectangle {
    height: 59px;
    width: 100%;
    border-radius: 8px;
    display: flex;
    align-items: center;
    justify-content: flex-start;
    border: solid 1px ${props => props.theme.colorPalette.gray[600]};
    cursor: pointer;
    box-sizing: border-box;
    .textColor {
      color: ${props => props.theme.colors.PRIMARY.DARK};
    }
  }
  .modal-align {
    padding: ${props => props.theme.spacing.ELEM_SPACING.XXS};
  }
`;

export default styles;
