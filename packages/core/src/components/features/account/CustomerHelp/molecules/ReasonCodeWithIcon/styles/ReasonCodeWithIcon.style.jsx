// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  && .reason-code-container {
    padding: ${(props) => props.theme.spacing.ELEM_SPACING.REG} 0
      ${(props) => props.theme.spacing.ELEM_SPACING.MED_1} 0;

    ${(props) => (!props.onClick ? 'padding:10px;cursor:auto;' : '')}
  }

  .reason-code-text-container {
    display: flex;
    flex-direction: column;
  }

  .reason-code-icon {
    align-self: start;
    margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XS_6};
  }
`;

export default styles;
