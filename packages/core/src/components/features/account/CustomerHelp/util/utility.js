// 9fbef606107a605d69c0edbcd8029e5d
import queryString from 'query-string';
import isEmpty from 'lodash/isEmpty';
import differenceInCalendarDays from 'date-fns/differenceInCalendarDays';
import format from 'date-fns/format';
import isValid from 'date-fns/isValid';
import parseISO from 'date-fns/parseISO';
import { extractFloat, getLabelValue } from '@tcp/core/src/utils/utils';
import CONFIRMATION_CONSTANTS from '@tcp/core/src/components/features/CnC/Confirmation/Confirmation.constants';
import { routerPush, isMobileApp, formatYmd } from '@tcp/core/src/utils';
import {
  CUSTOMER_HELP_ROUTES,
  GC_IN_ORDER,
  CUSTOMER_HELP_PAGE_TEMPLATE,
  ORDER_ITEM_STATUS,
  REASON_CODES,
} from '../CustomerHelp.constants';

export const routeToPage = (dataObj, queryParams, ...others) => {
  const { asPath } = dataObj;
  let { to } = dataObj;
  if (queryParams) {
    if (to.indexOf('?') !== -1) {
      to += '&';
    }
    to += `${queryString.stringify(queryParams)}`;
  }
  if (!isMobileApp()) {
    routerPush(to, asPath, ...others);
  }
};

export const moveToStage = (stageName, app, navigation) => {
  const stageNameParts = stageName.split('-')[0];
  const keys = Object.keys(CUSTOMER_HELP_ROUTES);
  keys.forEach((key) => {
    if (key.indexOf(stageNameParts.toUpperCase()) > -1) {
      if (app) {
        navigation.navigate(CUSTOMER_HELP_PAGE_TEMPLATE[key]);
      } else {
        routeToPage(CUSTOMER_HELP_ROUTES[key]);
      }
    }
  });
};

export const formatAmount = (value, currencySymbol) => {
  const symbol = value < 0 ? '-' : '';
  return symbol + currencySymbol + Math.abs(value).toFixed(2);
};

export const shouldOverwritePriceInfoWithApiResponse = (refundResponse) => {
  let shouldOverWrite = false;
  if (
    !isEmpty(refundResponse) &&
    ((refundResponse.status || '').toLowerCase() === 'success' ||
      refundResponse.errorLabel === GC_IN_ORDER)
  ) {
    shouldOverWrite = true;
  }
  return shouldOverWrite;
};

const getRefundedAmount = ({
  isCancelOrderCase,
  shippingTotal = 0,
  totalBeforeTax = 0,
  totalShippingTax = 0,
  tax = 0,
}) => {
  let taxAmount = 0;
  let refundedTotalAmount = 0;
  const noShippingCharges = shippingTotal === 0;
  const amountCalculationVerify = (noShippingCharges && isCancelOrderCase) || !isCancelOrderCase;
  taxAmount = amountCalculationVerify ? tax : totalShippingTax;
  refundedTotalAmount = amountCalculationVerify
    ? totalBeforeTax + tax
    : totalBeforeTax + tax + shippingTotal;
  return {
    taxAmount,
    refundedTotalAmount,
  };
};

export const getCalculatedRefund = (
  selectedOrderItems = {},
  purchasedItems = [],
  refundResponse = {},
  action = '',
  summary = {}
) => {
  let totalBeforeTax = 0;
  let tax = 0;
  let refundTotal = 0;
  let itemsCount = 0;
  const { shippingTotal = 0, totalTax: totalShippingTax = 0 } = summary;

  const isPaidAndShippingCase = action === REASON_CODES.PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME;
  const isCancelOrderCase = action === REASON_CODES.CANCEL_MODIFY;

  purchasedItems.forEach((purchasedItem) => {
    purchasedItem.items.forEach((item) => {
      const {
        lineNumber,
        itemInfo: {
          offerPrice /** This is paidUnitPrice */,
          taxUnitPrice /** Tax amount per unit */,
          quantity,
        },
      } = item;

      const quantitySelectedForRefund = selectedOrderItems[lineNumber];
      const isSelectedOrderItem = quantitySelectedForRefund > 0;
      if (isSelectedOrderItem) {
        itemsCount += quantitySelectedForRefund;
        totalBeforeTax += offerPrice * quantitySelectedForRefund;
        tax += taxUnitPrice * quantitySelectedForRefund;
      }
      /** Scenario when there are no selectedOrderItems, Cancel use case  */
      if (isEmpty(selectedOrderItems)) {
        itemsCount += quantity;
        totalBeforeTax += offerPrice * quantity;
        tax += shippingTotal === 0 ? quantity * taxUnitPrice : totalShippingTax;
      }
    });
  });

  const { taxAmount, refundedTotalAmount } = getRefundedAmount({
    isCancelOrderCase,
    shippingTotal,
    totalBeforeTax,
    totalShippingTax,
    tax,
  });
  tax = taxAmount;
  refundTotal = refundedTotalAmount;

  /**
   *  Scenarios covered other than cancel use case
   *  Overwriting tax, totalBeforeTax and refundTotal from API's response
   */
  if (
    (!isEmpty(selectedOrderItems) && shouldOverwritePriceInfoWithApiResponse(refundResponse)) ||
    (isPaidAndShippingCase && shouldOverwritePriceInfoWithApiResponse(refundResponse))
  ) {
    const {
      totalTax = 0,
      totalBeforeTax: totalBeforeTaxApiResponse = 0,
      refundedAmount = 0,
    } = refundResponse;
    tax = extractFloat(totalTax);
    totalBeforeTax = extractFloat(totalBeforeTaxApiResponse);
    refundTotal = extractFloat(refundedAmount);
  }

  return {
    totalBeforeTax,
    tax,
    refundTotal,
    itemsCount,
    shippingTotal,
  };
};

export const getAppeasedShipmentStatus = ({ orderGroup }) => {
  if (orderGroup) {
    return (
      orderGroup.items &&
      orderGroup.items.some((order) => order.productInfo && order.productInfo.appeasementApplied)
    );
  }
  return null;
};

export const getConditionalWarnMsg = ({ orderStatusValue, labels, action }) => {
  const { IN_TRANSIT, SHIPMENT_API_DELIVERED, IN_PROGRESS } = ORDER_ITEM_STATUS;
  const { PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME } = REASON_CODES;
  const isPaidShipping = action === PAID_SHIPPING_BUT_NOT_RECEIVED_ON_TIME;
  switch (orderStatusValue) {
    case IN_TRANSIT:
      return !isPaidShipping
        ? getLabelValue(labels, 'lbl_transit_shipment_not_selected', '')
        : null;
    case SHIPMENT_API_DELIVERED:
      return getLabelValue(labels, 'lbl_delivered_shipment_not_selected', '');
    case IN_PROGRESS:
      return !isPaidShipping
        ? getLabelValue(labels, 'lbl_in_progress_shipment_not_selected', '')
        : null;
    default:
      return null;
  }
};

export const getWarningMsg = ({
  orderGroup,
  filterValue,
  orderGroupExists,
  isAppeased,
  action,
  labels,
}) => {
  if (action === 'WHEN_WILL_MY_ORDER_ARRIVE') {
    return null;
  }
  if ((orderGroup && orderGroup.orderStatus !== filterValue) || isAppeased) {
    const orderStatusValue = orderGroup && orderGroup.orderStatus;
    if (isAppeased && orderGroupExists) {
      return getLabelValue(labels, 'lbl_item_already_refunded', '');
    }
    return getConditionalWarnMsg({ orderStatusValue, labels, action });
  }
  return null;
};

export const getOrderTileItems = ({ item, cshForMonthsToDisplay, noStatusDisplay }) => {
  if (item) {
    const {
      isEcomOrder,
      orderNumber,
      orderDateApi,
      currencySymbol,
      orderTotal,
      status,
      imgPath = '',
      productName,
      itemCount,
    } = item;
    let isOlderItem = false;
    const dateDifference = differenceInCalendarDays(new Date(), new Date(orderDateApi));
    if (dateDifference > cshForMonthsToDisplay) {
      isOlderItem = true;
    }
    return {
      orderNumber,
      orderDate: orderDateApi,
      currencySymbol,
      grandTotal: orderTotal,
      orderType: isEcomOrder ? CONFIRMATION_CONSTANTS.ORDER_ITEM_TYPE.ECOM : '',
      isEcomOrder,
      isOlderItem,
      onRecentOrderPage: true,
      status: noStatusDisplay ? '' : status,
      imagePath: imgPath.includes('https') ? imgPath : `https://${imgPath}`,
      productName,
      ItemsCount: itemCount,
    };
  }
  return null;
};

export const getRefundedShipmentStatus = ({ orderItems }) => {
  if (orderItems) {
    const result = orderItems.find((product) =>
      product.items.some((order) => order.productInfo && order.productInfo.appeasementApplied)
    );
    return result ? Object.keys(result).length : null;
  }
  return null;
};

export const getMaxDeliveryDate = ({ trackingDetails }) => {
  if (trackingDetails) {
    const datesGroup = trackingDetails.map(
      (r) => isValid(parseISO(r.deliveredDate)) && format(parseISO(r.deliveredDate), 'yyyy-MM-dd')
    );
    if (datesGroup) {
      let group = datesGroup.map((date) => date && new Date(`${date}T00:00:00`));
      group = group.filter((date) => date !== false);
      const maxDate = group.length && new Date(Math.max.apply(null, group));
      return maxDate ? formatYmd(maxDate) : '';
    }
    return '';
  }
  return null;
};

export const isAccountAndCouponURL = (router) => {
  return router?.asPath?.includes(CUSTOMER_HELP_PAGE_TEMPLATE.ACCOUNT_COUPONS);
};

export const isOldItem = (orderDate, allowedDifference) => {
  const daysDifference = differenceInCalendarDays(new Date(), new Date(orderDate));

  return daysDifference > allowedDifference;
};

export default {
  routeToPage,
  moveToStage,
  formatAmount,
  getCalculatedRefund,
  getAppeasedShipmentStatus,
  getConditionalWarnMsg,
  getWarningMsg,
  getOrderTileItems,
  getRefundedShipmentStatus,
  getMaxDeliveryDate,
  isAccountAndCouponURL,
};
