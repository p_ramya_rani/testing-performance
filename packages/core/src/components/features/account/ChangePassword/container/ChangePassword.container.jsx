// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { toastMessageInfo } from '@tcp/core/src/components/common/atoms/Toast/container/Toast.actions';
import { getUserEmail } from '@tcp/core/src/components/features/account/User/container/User.selectors';
import { login } from '@tcp/core/src/components/features/account/LoginPage/container/LoginPage.actions';
import {
  getLoginError,
  getLoginErrorMessage,
} from '@tcp/core/src/components/features/account/LoginPage/container/LoginPage.selectors';
import { toggleApplyNowModal } from '@tcp/core/src/components/common/molecules/ApplyNowPLCCModal/container/ApplyNowModal.actions';
import { routerPush, isMobileApp } from '../../../../../utils';
import {
  getError,
  getChangePasswordLabels,
  getChangeErrorMessage,
  getSuccessMessage,
} from './ChangePassword.selectors';
import { getSuccess } from '../../MyProfile/container/MyProfile.selectors';
import { updateProfileSuccess } from '../../MyProfile/container/MyProfile.actions';
import ChangePasswordComponent from '../views';
import { changePassword, changePasswordError } from './ChangePassword.actions';
import { getFormValidationErrorMessages } from '../../Account/container/Account.selectors';

export class ChangePasswordContainer extends PureComponent {
  static propTypes = {
    successMessage: PropTypes.string.isRequired,
    errorMessage: PropTypes.string.isRequired,
    changePasswordAction: PropTypes.func.isRequired,
    messageSateChangeAction: PropTypes.func.isRequired,
    labels: PropTypes.shape({}).isRequired,
    onClose: PropTypes.func,
    formErrorMessage: PropTypes.shape({}).isRequired,
    toastMessage: PropTypes.string.isRequired,
    changeErrorMessage: PropTypes.string.isRequired,
    successStateChangeAction: PropTypes.string.isRequired,
    logonId: PropTypes.string.isRequired,
    isPLCCFlow: PropTypes.bool,
    changeTempPassword: PropTypes.func,
    isPLCCModalFlow: PropTypes.bool,
    isPLCCModalOpen: PropTypes.bool,
    togglePLCCModal: PropTypes.func,
    isRegisteredUser: PropTypes.bool,
    loginUserAction: PropTypes.func,
    emailAddress: PropTypes.string,
    successfulChangePasswordMessage: PropTypes.string,
  };

  static defaultProps = {
    onClose: () => {},
    isPLCCFlow: false,
    changeTempPassword: () => {},
    isPLCCModalFlow: false,
    isPLCCModalOpen: false,
    togglePLCCModal: () => {},
    isRegisteredUser: false,
    loginUserAction: () => {},
    emailAddress: '',
    successfulChangePasswordMessage: '',
  };

  componentDidMount() {
    const { successStateChangeAction } = this.props;
    successStateChangeAction(null);
  }

  componentDidUpdate(prevProps) {
    const {
      successMessage,
      changeErrorMessage,
      errorMessage,
      toastMessage,
      successfulChangePasswordMessage,
    } = this.props;
    const { onClose } = this.props;
    if (prevProps.successMessage !== successMessage && successMessage === 'successMessage') {
      if (isMobileApp()) {
        toastMessage(successfulChangePasswordMessage);
        onClose();
      } else {
        this.goBackToProfile();
      }
    }

    if (prevProps.errorMessage !== errorMessage && isMobileApp() && errorMessage) {
      toastMessage(changeErrorMessage);
    }
  }

  componentWillUnmount() {
    const { messageSateChangeAction } = this.props;
    messageSateChangeAction(null);
  }

  afterLoginHandler = () => {
    const { isPLCCModalOpen, togglePLCCModal } = this.props;
    if (isPLCCModalOpen) {
      setTimeout(() => {
        togglePLCCModal({ isPLCCModalOpen: false });
      }, 1000);
    } else {
      this.goBackToProfile();
    }
  };

  changePassword = ({ password, currentPassword, confirmPassword }) => {
    const {
      changePasswordAction,
      logonId,
      changeTempPassword,
      isPLCCFlow,
      isRegisteredUser,
      loginUserAction,
      emailAddress,
      plccCardId,
    } = this.props;
    const userEmail = emailAddress || logonId;
    if (isPLCCFlow) {
      if (isRegisteredUser) {
        const payload = {
          emailAddress: userEmail,
          password,
          rememberMe: false,
          mergeCart: true,
          fullPage: false,
          plcc_checkbox: false,
          savePlcc: false,
          plccCardId,
          isApplyPlccCardFlow: true,
        };
        loginUserAction({ ...payload }, this.afterLoginHandler);
      } else {
        changeTempPassword({ password, logonId: userEmail, isPLCCFlow });
      }
    } else {
      changePasswordAction({
        currentPassword,
        newPassword: password,
        newPasswordVerify: confirmPassword,
        logonId: userEmail,
      });
    }
  };

  redirectIfNotPLCCModalFlow = (isPLCCModalFlow) => {
    if (!isPLCCModalFlow) {
      routerPush('/account?id=profile', '/account/profile');
    }
    return null;
  };

  goBackToProfile = () => {
    const { isPLCCModalFlow, isPLCCFlow } = this.props;
    if (isPLCCFlow && !isPLCCModalFlow) {
      if (window.history && window.history.back) {
        window.history.back();
      }
    } else {
      this.redirectIfNotPLCCModalFlow(isPLCCModalFlow);
    }

    return null;
  };

  showForgotPassword = () => {
    const { isPLCCModalOpen, togglePLCCModal } = this.props;
    if (isPLCCModalOpen) {
      setTimeout(() => {
        togglePLCCModal({ isPLCCModalOpen: false });
      }, 1000);
    }
    routerPush(`/change-password?page=reset`, `/change-password?page=reset`);
  };

  render() {
    const {
      successMessage,
      errorMessage,
      labels,
      onClose,
      formErrorMessage,
      changeErrorMessage,
      isPLCCFlow,
      isRegisteredUser,
      loginError,
      loginErrorMessage,
      isRtpsFlow,
    } = this.props;
    const loginFormErrorMessage = loginError ? loginErrorMessage : '';
    return (
      <ChangePasswordComponent
        successMessage={successMessage}
        errorMessage={errorMessage}
        onSubmit={this.changePassword}
        labels={labels}
        onClose={onClose}
        formErrorMessage={formErrorMessage}
        changeErrorMessage={changeErrorMessage}
        isPLCCFlow={isPLCCFlow}
        isRegisteredUser={isRegisteredUser}
        loginFormErrorMessage={loginFormErrorMessage}
        isRtpsFlow={isRtpsFlow}
        showForgotPassword={this.showForgotPassword}
      />
    );
  }
}

export const mapStateToProps = (state) => ({
  successMessage: getSuccess(state),
  errorMessage: getError(state),
  labels: getChangePasswordLabels(state),
  formErrorMessage: getFormValidationErrorMessages(state),
  changeErrorMessage: getChangeErrorMessage(state),
  logonId: getUserEmail(state),
  loginError: getLoginError(state),
  loginErrorMessage: getLoginErrorMessage(state),
  successfulChangePasswordMessage: getSuccessMessage(state),
});

export const mapDispatchToProps = (dispatch) => ({
  changePasswordAction: (payload) => {
    dispatch(changePassword(payload));
  },
  messageSateChangeAction: (payload) => {
    dispatch(changePasswordError(payload));
  },
  successStateChangeAction: (payload) => {
    dispatch(updateProfileSuccess(payload));
  },
  toastMessage: (palyoad) => {
    dispatch(toastMessageInfo(palyoad));
  },
  loginUserAction: (payload, afterLoginHandler) => {
    const modfiedPayload = { ...payload };
    dispatch(login(modfiedPayload, afterLoginHandler));
  },
  togglePLCCModal: (payload) => {
    dispatch(toggleApplyNowModal(payload));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(ChangePasswordContainer);
