// 9fbef606107a605d69c0edbcd8029e5d
import { call, takeLatest, put, select } from 'redux-saga/effects';
import { getIsSNJEnabled } from '@tcp/core/src/reduxStore/selectors/session.selectors';
import { setIsPlccDone } from '@tcp/core/src/components/features/browse/ApplyCardPage/container/ApplyCard.actions';
import { setLoaderState } from '@tcp/core/src/components/common/molecules/Loader/container/Loader.actions';
import constants from '../ChangePassword.constants';
import { changePasswordError } from './ChangePassword.actions';
import { updateProfileSuccess } from '../../MyProfile/container/MyProfile.actions';
import { UpdateProfileInfo } from '../../../../../services/abstractors/account';

export function* ChangePassword({ payload }) {
  const { fromPLCCModal } = payload;
  try {
    const { showLoader } = payload;
    const isSNJEnabled = yield select(getIsSNJEnabled);
    const updatedPayload = {
      ...payload,
      useMicroService: isSNJEnabled,
    };
    if (showLoader) {
      yield put(setLoaderState(true));
    }
    const res = yield call(UpdateProfileInfo, updatedPayload);
    if (fromPLCCModal) {
      yield put(setIsPlccDone(true));
    }
    if (showLoader) {
      yield put(setLoaderState(false));
    }
    return yield put(updateProfileSuccess(res));
  } catch (err) {
    let error = {};
    /* istanbul ignore else */
    const { showLoader } = payload;
    if (showLoader) {
      yield put(setLoaderState(false));
    }
    error = err;
    if (error && error.errorResponse) {
      return yield put(changePasswordError(error.errorResponse));
    }
    if (error && error.response.body) {
      return yield put(changePasswordError(error.response.body.errors[0]));
    }
  }
}

export function* ChangePasswordSaga() {
  yield takeLatest(constants.CHANGE_PASSWORD, ChangePassword);
}

export default ChangePasswordSaga;
