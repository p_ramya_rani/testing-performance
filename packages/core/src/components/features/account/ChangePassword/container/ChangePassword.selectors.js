// 9fbef606107a605d69c0edbcd8029e5d
import { createSelector } from 'reselect';
import { getIsSNJEnabled } from '@tcp/core/src/reduxStore/selectors/session.selectors';
import { CHANGE_PASSWORD_REDUCER_KEY } from '../../../../../constants/reducer.constants';
import { getErrorSelector } from '../../../../../utils/utils';

const getState = (state) => state[CHANGE_PASSWORD_REDUCER_KEY];

export const getError = createSelector(getState, (state) => state && state.get('error'));

export const getSuccess = createSelector(getState, (state) => state && state.get('success'));

export const getChangePasswordLabels = (state) => {
  return (state && state.Labels.global.password) || {};
};

const getSuccessPasswordLabel = (state) => state.lbl_changePassword_successMessage;
export const getSuccessMessage = createSelector(
  getChangePasswordLabels,
  (state) => state && getSuccessPasswordLabel(state)
);

export const getChangeErrorResponse = (state) => {
  return state[CHANGE_PASSWORD_REDUCER_KEY].get('error');
};

export const getChangeErrorMessage = createSelector(
  [getChangeErrorResponse, getChangePasswordLabels, getIsSNJEnabled],
  (state, labels, isSNJEnabled) => {
    if (isSNJEnabled) {
      return getErrorSelector(state, labels, 'lbl_changePassword_INVALID_CURRENT_PASSWORD');
    }
    return getErrorSelector(state, labels, 'lbl_changePassword');
  }
);
