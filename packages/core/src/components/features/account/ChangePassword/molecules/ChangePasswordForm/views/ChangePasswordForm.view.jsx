// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { reduxForm, Field } from 'redux-form';
import Notification from '@tcp/core/src/components/common/molecules/Notification';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import BodyCopy from '../../../../../../common/atoms/BodyCopy';
import RichText from '../../../../../../common/atoms/RichText';
import Row from '../../../../../../common/atoms/Row';
import Col from '../../../../../../common/atoms/Col';
import Anchor from '../../../../../../common/atoms/Anchor/views/Anchor';
import Button from '../../../../../../common/atoms/Button';
import withStyles from '../../../../../../common/hoc/withStyles';
import PasswordField from '../../../../common/molecule/PasswordField';
import PasswordRequirement from '../../../../ResetPassword/molecules/PasswordRequirement';
import createValidateMethod from '../../../../../../../utils/formValidation/createValidateMethod';
import getStandardConfig from '../../../../../../../utils/formValidation/validatorStandardConfig';
import styles from '../styles/ChangePasswordForm.style';
import constants from '../../../ChangePassword.constants';

const getLoginCTAText = (isRegisteredUser, labels, isRtpsFlow) => {
  if (isRegisteredUser) {
    if (isRtpsFlow) {
      return getLabelValue(labels, 'lbl_changePassword_loginToCheckoutCta_PLCC');
    }
    return getLabelValue(labels, 'lbl_changePassword_loginCta_PLCC');
  }
  if (isRtpsFlow) {
    return getLabelValue(labels, 'lbl_changePassword_saveAndCheckoutCta_PLCC');
  }
  return getLabelValue(labels, 'lbl_changePassword_saveCta_PLCC');
};

const getLoginFormErrorMsg = (loginFormErrorMessage, isPLCCFlow) => {
  return (
    isPLCCFlow &&
    loginFormErrorMessage && (
      <BodyCopy fontSize="fs12" fontWeight="semibold" color="red.500" className="elem-mb-XL">
        <RichText className="richTextColor" richTextHtml={loginFormErrorMessage} />
      </BodyCopy>
    )
  );
};

const getColSize = (isPLCCFlow) => {
  return isPLCCFlow ? 12 : 4;
};

export const ChangePasswordForm = ({
  className,
  labels,
  pristine,
  errorMessage,
  handleSubmit,
  changeErrorMessage,
  isPLCCFlow,
  isRegisteredUser,
  loginFormErrorMessage,
  isRtpsFlow,
  showForgotPassword,
}) => {
  return (
    <form
      name={constants.CHANGE_PASSWORD_FORM}
      noValidate
      onSubmit={handleSubmit}
      className={className}
    >
      {errorMessage && changeErrorMessage && (
        <Notification
          className="elem-mt-MED"
          status="error"
          colSize={{ large: 12, medium: 8, small: 6 }}
          message={`${changeErrorMessage}`}
        />
      )}
      {getLoginFormErrorMsg(loginFormErrorMessage, isPLCCFlow)}
      <BodyCopy component="div" className="elem-mt-LRG">
        <Row fullBleed>
          <Col
            colSize={{
              large: isPLCCFlow ? 12 : 5,
              medium: getColSize(isPLCCFlow),
              small: 6,
            }}
          >
            {!isPLCCFlow && (
              <Field
                id="currentPassword"
                placeholder={getLabelValue(labels, 'lbl_changePassword_current_password')}
                name="currentPassword"
                component={PasswordField}
                dataLocator="currentPasswordtxtfield"
                showSuccessCheck={false}
                enableSuccessCheck
                checkMarkRightMargin="40px"
                className="elem-mb-SM"
              />
            )}
            <Field
              id="password"
              placeholder={
                isPLCCFlow
                  ? getLabelValue(labels, 'lbl_changePassword_enter_password_PLCC')
                  : getLabelValue(labels, 'lbl_changePassword_new_password')
              }
              name="password"
              component={PasswordField}
              dataLocator="newPasswordtxtfield"
              showSuccessCheck={false}
              enableSuccessCheck={!isPLCCFlow}
              checkMarkRightMargin="40px"
              className={`${isPLCCFlow ? '' : 'elem-mb-SM'}`}
              tooltipContent={isPLCCFlow ? false : <PasswordRequirement labels={labels} />}
              spc={isPLCCFlow}
            />
            {!isPLCCFlow && (
              <Field
                id="confirmPassword"
                placeholder={getLabelValue(labels, 'lbl_changePassword_confirm_password')}
                name="confirmPassword"
                component={PasswordField}
                dataLocator="confirmPasswordtxtfield"
                showSuccessCheck={false}
                enableSuccessCheck
                checkMarkRightMargin="40px"
                className="elem-mb-SM"
              />
            )}
          </Col>
        </Row>
      </BodyCopy>
      <BodyCopy
        component="div"
        textAlign="center"
        className={`${!isPLCCFlow ? 'elem-mt-LRG elem-mb-LRG' : ''}`}
      >
        <Row className="Cancel_Submit_Button">
          {!isPLCCFlow && (
            <Col
              className="ChangePasswordForm_cancel"
              colSize={{
                large: 3,
                medium: 3,
                small: 6,
              }}
              offsetLeft={{
                large: 3,
                medium: 1,
              }}
            >
              <Anchor to="/account?id=profile" asPath="/account/profile">
                <Button
                  type="button"
                  buttonVariation="fixed-width"
                  dataLocator="cancelBtn"
                  fullWidth
                  className="elem-mb-XS"
                >
                  {getLabelValue(labels, 'lbl_changePassword_cancelCta')}
                </Button>
              </Anchor>
            </Col>
          )}
          <Col
            className="ChangePasswordForm_save"
            colSize={{
              large: isPLCCFlow ? 8 : 3,
              medium: getColSize(isPLCCFlow),
              small: isPLCCFlow ? 12 : 6,
            }}
          >
            {isPLCCFlow && isRegisteredUser && (
              <BodyCopy
                component="div"
                fontFamily="secondary"
                fontSize="fs12"
                textAlign="center"
                className="reset-password"
              >
                <Anchor
                  underline
                  onClick={(e) => {
                    e.preventDefault();
                    showForgotPassword();
                  }}
                >
                  {getLabelValue(labels, 'lbl_forgotPassword_resetPassword')}
                </Anchor>
              </BodyCopy>
            )}
            {isPLCCFlow && !isRegisteredUser && (
              <>
                <BodyCopy
                  color="gray.600"
                  data-locator="passwordInstructionTxt"
                  fontSize="fs12"
                  fontFamily="secondary"
                  fontWeight="regular"
                >
                  {`- ${getLabelValue(labels, 'lbl_resetPassword_requirementTips2')}`}
                </BodyCopy>

                <BodyCopy
                  color="gray.600"
                  data-locator="passwordInstructionTxt"
                  fontSize="fs12"
                  fontFamily="secondary"
                  fontWeight="regular"
                >
                  {`- ${getLabelValue(labels, 'lbl_resetPassword_requirementTips3')}`}
                </BodyCopy>

                <BodyCopy
                  color="gray.600"
                  data-locator="passwordInstructionTxt"
                  fontSize="fs12"
                  fontFamily="secondary"
                  fontWeight="regular"
                >
                  {`- ${getLabelValue(labels, 'lbl_resetPassword_requirementTips1')}`}
                </BodyCopy>

                <BodyCopy
                  color="gray.600"
                  data-locator="passwordInstructionTxt"
                  fontSize="fs12"
                  fontFamily="secondary"
                  fontWeight="regular"
                >
                  {`- ${getLabelValue(labels, 'lbl_resetPassword_requirementTips4')}`}
                </BodyCopy>
              </>
            )}

            <Button
              fill="BLUE"
              type="submit"
              buttonVariation="fixed-width"
              dataLocator="SaveBtn"
              fullWidth
              className={`elem-mb-XS ${isPLCCFlow ? 'rounded-cta elem-mt-SM' : ''}`}
              disabled={pristine}
            >
              {isPLCCFlow
                ? getLoginCTAText(isRegisteredUser, labels, isRtpsFlow)
                : getLabelValue(labels, 'lbl_changePassword_saveCta')}
            </Button>
          </Col>
        </Row>
      </BodyCopy>
    </form>
  );
};

ChangePasswordForm.propTypes = {
  labels: PropTypes.shape({
    lbl_changePassword_current_password: PropTypes.string,
    lbl_changePassword_new_password: PropTypes.string,
    lbl_changePassword_confirm_password: PropTypes.string,
    lbl_changePassword_cancelCta: PropTypes.string,
    lbl_changePassword_saveCta: PropTypes.string,
  }),
  pristine: PropTypes.bool.isRequired,
  errorMessage: PropTypes.string.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  className: PropTypes.string,
  changeErrorMessage: PropTypes.string.isRequired,
  isPLCCFlow: PropTypes.bool,
  isRegisteredUser: PropTypes.bool,
  loginFormErrorMessage: PropTypes.string,
  isRtpsFlow: PropTypes.bool,
  showForgotPassword: PropTypes.func,
};

ChangePasswordForm.defaultProps = {
  className: '',
  labels: {
    lbl_changePassword_current_password: '',
    lbl_changePassword_new_password: '',
    lbl_changePassword_confirm_password: '',
    lbl_changePassword_cancelCta: '',
    lbl_changePassword_saveCta: '',
  },
  isPLCCFlow: false,
  isRegisteredUser: false,
  loginFormErrorMessage: '',
  isRtpsFlow: false,
  showForgotPassword: () => {},
};

const validateMethod = createValidateMethod(
  getStandardConfig(['currentPassword', 'password', 'confirmPassword'])
);

export default reduxForm({
  form: constants.CHANGE_PASSWORD_FORM, // a unique identifier for this form
  enableReinitialize: true,
  ...validateMethod,
})(withStyles(ChangePasswordForm, styles));
