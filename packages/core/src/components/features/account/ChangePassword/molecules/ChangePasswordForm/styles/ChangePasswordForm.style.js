// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  @media ${(props) => props.theme.mediaQuery.mediumOnly} {
    .ChangePasswordForm_cancel {
      margin-left: 0px;
    }

    .Cancel_Submit_Button {
      justify-content: ${(props) => (props.isPLCCFlow ? 'left' : 'center')};
      margin-left: ${(props) => props.isPLCCFlow && '0px'};
    }
  }
  @media ${(props) => props.theme.mediaQuery.smallOnly} {
    .Cancel_Submit_Button {
      margin-left: ${(props) => props.isPLCCFlow && '0px'};
      margin-right: ${(props) => props.isPLCCFlow && '0px'};
      ${(props) => props.isPLCCFlow && 'width: 100%;'}
    }
  }
  @media ${(props) => props.theme.mediaQuery.large} {
    .Cancel_Submit_Button {
      margin-left: ${(props) => props.isPLCCFlow && '0px'};
    }
  }

  @media ${(props) => props.theme.mediaQuery.smallMax} {
    .ChangePasswordForm_cancel {
      order: 2;
    }

    .ChangePasswordForm_save {
      order: 1;
      margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
    }
  }
  .rightAlignedContent {
    right: ${(props) => props.isPLCCFlow && '14px'};
    top: ${(props) => props.isPLCCFlow && '19px'};
  }
  .reset-password {
    text-align: left;
    margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
    color: ${(props) => props.theme.colorPalette.blue.C900};
  }
`;
export default styles;
