// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components';

const HideShowField = styled.View`
  position: absolute;
  right: ${(props) => (props.isPLCCFlow ? props.theme.spacing.LAYOUT_SPACING.XS : '0')};
  top: ${(props) =>
    props.isPLCCFlow
      ? props.theme.spacing.LAYOUT_SPACING.SM
      : props.theme.spacing.LAYOUT_SPACING.XXS};
`;

const CurrentPasswordWrapper = styled.View`
  position: relative;
  margin-top: ${(props) => props.theme.spacing.LAYOUT_SPACING.XS};
`;

const NewPasswordWrapper = styled.View`
  position: relative;
`;

const ConfirmPasswordWrapper = styled.View`
  position: relative;
  margin-bottom: ${(props) => props.theme.spacing.LAYOUT_SPACING.SM};
`;

const CancelWrapper = styled.View`
  margin-top: ${(props) => props.theme.spacing.APP_LAYOUT_SPACING.XS};
`;

const IconContainer = styled.View`
  position: absolute;
  right: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  top: -${(props) => props.theme.spacing.ELEM_SPACING.SM};
  width: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
`;

const PasswordInfoWrapper = styled.View`
  padding-bottom: ${(props) => props.theme.spacing.LAYOUT_SPACING.XS};
`;

export {
  HideShowField,
  CurrentPasswordWrapper,
  NewPasswordWrapper,
  ConfirmPasswordWrapper,
  CancelWrapper,
  IconContainer,
  PasswordInfoWrapper,
};
