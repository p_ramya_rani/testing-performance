// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import { reduxForm, Field } from 'redux-form';
import Button from '@tcp/core/src/components/common/atoms/Button';
import ButtonRedesign from '@tcp/core/src/components/common/atoms/ButtonRedesign';
import TextBox from '@tcp/core/src/components/common/atoms/TextBox';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import ReactTooltip from '@tcp/core/src/components/common/atoms/ReactToolTip';
import ImageComp from '@tcp/core/src/components/common/atoms/Image';
import Anchor from '@tcp/core/src/components/common/atoms/Anchor';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import IconInfoLogo from '../../../../../../../../../mobileapp/src/assets/images/info-icon.png';
import createValidateMethod from '../../../../../../../utils/formValidation/createValidateMethod';
import getStandardConfig from '../../../../../../../utils/formValidation/validatorStandardConfig';
import Constants from '../../../container/CurrentPassword.utils';
import PasswordRequirement from '../../../../ResetPassword/molecules/PasswordRequirement';

import {
  HideShowField,
  CurrentPasswordWrapper,
  NewPasswordWrapper,
  ConfirmPasswordWrapper,
  CancelWrapper,
  IconContainer,
  PasswordInfoWrapper,
} from '../styles/ChangePasswordForm.style.native';

export class ChangePasswordForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      Password: {
        Current: true,
        New: true,
        Confirm: true,
      },
    };
  }

  onShowHidePassword = (type) => {
    const { Password } = this.state;
    const updatedPassord = Password;
    updatedPassord[type] = !Password[type];
    this.setState({ Password: updatedPassord });
  };

  getHideShowView = (type, isShowText) => {
    const { labels, isPLCCFlow } = this.props;
    return (
      <HideShowField isPLCCFlow={isPLCCFlow}>
        <Anchor
          fontSizeVariation="medium"
          fontFamily="secondary"
          anchorVariation="primary"
          onPress={() => this.onShowHidePassword(type)}
          noLink
          to="/#"
          underline
          dataLocator="hide-show-pwd"
          text={
            isShowText
              ? getLabelValue(labels, 'lbl_changePassword_show')
              : getLabelValue(labels, 'lbl_changePassword_hide')
          }
          hitSlop={{ top: 3, left: 10, bottom: 10, right: 10 }}
        />
      </HideShowField>
    );
  };

  render() {
    const { labels, pristine, handleSubmit, onClose, isPLCCFlow, isRtpsFlow } = this.props;
    const { Password } = this.state;
    const plccCtaLbl = isRtpsFlow
      ? 'lbl_changePassword_saveAndCheckoutCta_PLCC'
      : 'lbl_changePassword_saveCta_PLCC';
    return (
      <View>
        {!isPLCCFlow && (
          <CurrentPasswordWrapper>
            <Field
              id="currentPassword"
              label={getLabelValue(labels, 'lbl_changePassword_current_password')}
              name="currentPassword"
              component={TextBox}
              dataLocator="currentPasswordtxtfield"
              enableSuccessCheck={false}
              secureTextEntry={Password.Current}
            />
            {this.getHideShowView(Constants.Current, Password.Current)}
          </CurrentPasswordWrapper>
        )}
        <NewPasswordWrapper>
          <Field
            id="password"
            label={getLabelValue(labels, 'lbl_changePassword_new_password')}
            name="password"
            component={TextBox}
            dataLocator="newPasswordtxtfield"
            showSuccessCheck={!isPLCCFlow}
            successText={getLabelValue(labels, 'lbl_changePassword_input_success')}
            enableSuccessCheck={!isPLCCFlow}
            secureTextEntry={Password.New}
            showAnimatedBorder={isPLCCFlow}
          />
          {!isPLCCFlow && (
            <IconContainer>
              <ReactTooltip
                withOverlay={false}
                popover={<PasswordRequirement labels={labels} />}
                height={200}
                width={300}
                textAlign="left"
              >
                <ImageComp source={IconInfoLogo} height={25} width={25} />
              </ReactTooltip>
            </IconContainer>
          )}
          {this.getHideShowView(Constants.New, Password.New)}
          {isPLCCFlow && (
            <PasswordInfoWrapper>
              <BodyCopy
                color="gray.600"
                data-locator="passwordInstructionTxt"
                fontSize="fs12"
                fontFamily="secondary"
                fontWeight="regular"
                text={`- ${getLabelValue(labels, 'lbl_resetPassword_requirementTips2')}`}
              />
              <BodyCopy
                color="gray.600"
                data-locator="passwordInstructionTxt"
                fontSize="fs12"
                fontFamily="secondary"
                fontWeight="regular"
                text={`- ${getLabelValue(labels, 'lbl_resetPassword_requirementTips3')}`}
              />
              <BodyCopy
                color="gray.600"
                data-locator="passwordInstructionTxt"
                fontSize="fs12"
                fontFamily="secondary"
                fontWeight="regular"
                text={`- ${getLabelValue(labels, 'lbl_resetPassword_requirementTips1')}`}
              />
              <BodyCopy
                color="gray.600"
                data-locator="passwordInstructionTxt"
                fontSize="fs12"
                fontFamily="secondary"
                fontWeight="regular"
                text={`- ${getLabelValue(labels, 'lbl_resetPassword_requirementTips4')}`}
              />
            </PasswordInfoWrapper>
          )}
        </NewPasswordWrapper>

        {!isPLCCFlow && (
          <ConfirmPasswordWrapper>
            <Field
              id="confirmPassword"
              label={getLabelValue(labels, 'lbl_changePassword_confirm_password')}
              name="confirmPassword"
              component={TextBox}
              dataLocator="confirmPasswordtxtfield"
              showSuccessCheck
              successText={getLabelValue(labels, 'lbl_changePassword_input_success')}
              secureTextEntry={Password.Confirm}
              enableSuccessCheck={false}
            />
            {this.getHideShowView(Constants.Confirm, Password.Confirm)}
          </ConfirmPasswordWrapper>
        )}
        {!isPLCCFlow && (
          <Button
            fill="BLUE"
            color="white"
            text={getLabelValue(labels, 'lbl_changePassword_saveCta')}
            disabled={pristine}
            onPress={handleSubmit}
          />
        )}

        {isPLCCFlow && (
          <ButtonRedesign
            text={getLabelValue(labels, plccCtaLbl)}
            fill="BLUE"
            borderRadius="16px"
            fontFamily="secondary"
            fontWeight="bold"
            fontSize="fs16"
            paddings="8px"
            showShadow
            disableButton={pristine}
            onPress={handleSubmit}
          />
        )}

        {!isPLCCFlow && (
          <CancelWrapper>
            <Button
              fill="WHITE"
              text={getLabelValue(labels, 'lbl_changePassword_cancelCta')}
              onPress={onClose}
            />
          </CancelWrapper>
        )}
      </View>
    );
  }
}

ChangePasswordForm.propTypes = {
  labels: PropTypes.shape({
    lbl_changePassword_current_password: PropTypes.string,
    lbl_changePassword_new_password: PropTypes.string,
    lbl_changePassword_confirm_password: PropTypes.string,
    lbl_changePassword_cancelCta: PropTypes.string,
    lbl_changePassword_saveCta: PropTypes.string,
  }),
  pristine: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  onClose: PropTypes.func,
};

ChangePasswordForm.defaultProps = {
  labels: {
    lbl_changePassword_current_password: '',
    lbl_changePassword_new_password: '',
    lbl_changePassword_confirm_password: '',
    lbl_changePassword_cancelCta: '',
    lbl_changePassword_saveCta: '',
  },
  onClose: () => {},
};

const validateMethod = createValidateMethod(
  getStandardConfig(['currentPassword', 'password', 'confirmPassword'])
);

export default reduxForm({
  form: 'ChangePasswordForm', // a unique identifier for this form
  enableReinitialize: true,
  ...validateMethod,
})(ChangePasswordForm);
