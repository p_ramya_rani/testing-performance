// 9fbef606107a605d69c0edbcd8029e5d 
/* eslint-disable */
import React from 'react';
import { shallow } from 'enzyme';

import { routerPush } from '@tcp/core/src/utils';
import { ForgotPasswordViewVanilla } from '../ForgotPassword.view';
import ForgotPasswordFullPageView from '../ForgotPasswordFullPage.view';

jest.mock('@tcp/core/src/utils', () => ({
  isMobileApp: jest.fn(),
  routerPush: jest.fn(),
  getAPIConfig: jest.fn(),
  isClient: jest.fn(),
  getIconPath: jest.fn(),
  getStaticFilePath: jest.fn(),
}));

describe('ForgotPasswordView component', () => {
  const props = {
    labels: {
      password: {},
    },
    handleSubmit: () => {},
    onSubmit: () => {},
    showNotification: '',
    resetForgotPasswordErrorResponse: '',
    successFullResetEmail: '',
  };
  const mockedRouterPush = jest.fn();
  it('should renders correctly', () => {
    const component = shallow(<ForgotPasswordViewVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('should render full page view ', () => {
    const fullPageProps = { ...props, fullPageView: true };
    const component = shallow(<ForgotPasswordViewVanilla {...fullPageProps} />);
    expect(component).toMatchSnapshot();
    expect(component.find(ForgotPasswordFullPageView)).toHaveLength(1);
  });

  it('should not call routerpushshowCreateAccountForm', () => {
    routerPush.mockImplementation(mockedRouterPush);
    const fullPageProps = {
      ...props,
      fullPageView: true,
      fullPageAuthEnabled: false,
      successFullResetEmail: false,
      openOverlay: () => {},
    };
    const component = shallow(<ForgotPasswordFullPageView {...fullPageProps} />);
    const createAccountBtn = component.find('.create-acc-cta');
    createAccountBtn.simulate('click');
    expect(mockedRouterPush).not.toBeCalled();
  });

  it('should call routerpush with home on showCreateAccountForm', () => {
    routerPush.mockImplementation(mockedRouterPush);
    const fullPageProps = {
      ...props,
      fullPageView: true,
      fullPageAuthEnabled: true,
      successFullResetEmail: false,
    };
    const component = shallow(<ForgotPasswordFullPageView {...fullPageProps} />);
    const createAccountBtn = component.find('.create-acc-cta');
    createAccountBtn.simulate('click');
    expect(mockedRouterPush).toBeCalledWith(
      '/home?target=register&successtarget=referrer',
      '/home/register'
    );
  });
});
