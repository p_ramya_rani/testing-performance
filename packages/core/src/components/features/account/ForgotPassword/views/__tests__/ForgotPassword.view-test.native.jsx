// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';

import { ForgotPasswordViewVanilla } from '../ForgotPassword.view.native';

describe('ForgotPasswordView component', () => {
  const props = {
    labels: {
      password: {},
    },
    handleSubmit: jest.fn(),
    onSubmit: () => {},
    showNotification: '',
    resetForgotPasswordErrorResponse: '',
    successFullResetEmail: '',
    updateHeader: jest.fn(),
    toastMessage: jest.fn(),
    invalid: false,
  };
  it('should renders correctly', () => {
    const component = shallow(<ForgotPasswordViewVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('should renders correctly with OTP view', () => {
    const component = shallow(
      <ForgotPasswordViewVanilla {...props} isResetPassCodeValidationEnabled />
    );
    expect(component).toMatchSnapshot();
  });
});
