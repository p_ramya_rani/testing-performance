// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { Field, reduxForm } from 'redux-form';
import PropTypes from 'prop-types';
import Anchor from '@tcp/core/src/components/common/atoms/Anchor/views/Anchor.native';
import ModalNative from '@tcp/core/src/components/common/molecules/Modal';
import { View, SafeAreaView } from 'react-native';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import createThemeColorPalette from '@tcp/core/styles/themes/createThemeColorPalette';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import ButtonRedesign from '@tcp/core/src/components/common/atoms/ButtonRedesign';
import {
  ViewWithSpacing,
  BodyCopyWithSpacing,
} from '@tcp/core/src/components/common/atoms/styledWrapper';
import Image from '@tcp/core/src/components/common/atoms/Image';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import ResetPasswordOption from '@tcp/core/src/components/features/account/ResetPassword/views/ResetPasswordOption.view.native';
import CreateAccount from '@tcp/core/src/components/features/account/CreateAccount';
import {
  FormStyle,
  HeadingStyle,
  ForgotHeadingStyle,
  FloatWrapper,
  CustomIconWrapper,
  ForgotPasswordWrapper,
  StyledTouchableOpacity,
  StyledCrossImage,
  FormStyleView,
  LoginFormInfoStyle,
} from '../../LoginPage/molecules/LoginForm/LoginForm.style.native';
import {
  ForgotPasswordTextWrapper,
  BackButtonContainer,
  ResendCodeWrapper,
} from '../styles/ForgotPassword.style.native';
import TextBox from '../../../../common/atoms/TextBox';
import CustomButton from '../../../../common/atoms/Button';
import createValidateMethod from '../../../../../utils/formValidation/createValidateMethod';
import getStandardConfig from '../../../../../utils/formValidation/validatorStandardConfig';
import LineComp from '../../../../common/atoms/Line';
import RichText from '../../../../common/atoms/RichText';
import ResetPassword from '../../ResetPassword';

const colorPallete = createThemeColorPalette();
const closeIcon = require('../../../../../../../mobileapp/src/assets/images/close.png');
const leftArrow = require('../../../../../../../mobileapp/src/assets/images/carrot-large-left-blue.png');

const styles = {
  createAccountStyle: {
    marginTop: 30,
  },

  forgotPasswordStyle: {
    marginTop: 10,
  },
};

class ForgotPasswordView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      disableResetCTA: false,
      showRegister: false,
      showSmsTerm: false,
    };
  }

  componentDidUpdate(prevProps) {
    const {
      showNotification,
      labels,
      resetForgotPasswordErrorResponse,
      toastMessage,
      isAccountNotFoundError,
    } = this.props;
    const errorObject =
      (resetForgotPasswordErrorResponse && resetForgotPasswordErrorResponse.get('errors')) ||
      isAccountNotFoundError;
    if (
      (!prevProps.showNotification && showNotification) ||
      (!prevProps.isAccountNotFoundError && isAccountNotFoundError)
    ) {
      toastMessage(
        errorObject
          ? getLabelValue(labels, 'lbl_forgotPassword_userNotAvailable', 'password')
          : getLabelValue(labels, 'lbl_forgotPassword_apiError', 'password')
      );
      this.disableResetButton(false);
    }
  }

  // Form submit user will submit the form with email id with right format else formsubmit will not trigger
  //  due to ( redux form )
  onFormSubmit = (formData) => {
    const {
      SubmitForgot,
      isResetViaText,
      fetchPhoneActn,
      selectedPassResetOption,
      resetSendOTPError,
    } = this.props;
    resetSendOTPError(false);
    if (isResetViaText) {
      fetchPhoneActn({
        logonId: formData.Email.toUpperCase().trim(),
      });
    } else {
      SubmitForgot({
        logonId: formData.Email.toUpperCase().trim(),
        target: selectedPassResetOption,
      });
    }
    this.setState({ email: formData.Email.toUpperCase().trim() });
  };

  triggerPassResetApi = (payload) => {
    const { SubmitForgot } = this.props;
    const { email } = this.state;
    SubmitForgot({
      logonId: email,
      target: payload.target,
    });
  };

  submitHandler = () => {
    const { handleSubmit, invalid = false } = this.props;
    this.disableResetButton(!invalid);
    handleSubmit((data) => {
      this.onFormSubmit(data);
    })();
  };

  // onclick back button user will redirect to the login page
  onBackClick = ({ fromResetView = false }) => {
    const { showForgotPasswordForm, showLogin, updateHeader } = this.props;
    showForgotPasswordForm();
    showLogin(fromResetView);
    updateHeader('login');
  };

  disableResetButton = (val = true) => {
    this.setState({ disableResetCTA: val });
  };

  // inital state of forgot password reset email form
  showResetEmailSection = () => {
    const { labels } = this.props;
    const { email, disableResetCTA } = this.state;
    return (
      <React.Fragment>
        <ForgotPasswordTextWrapper>
          <BodyCopyWithSpacing
            textAlign="left"
            fontFamily="secondary"
            fontSize="fs28"
            text={getLabelValue(labels, 'lbl_forgotPassword_content1', 'password')}
            spacingStyles="margin-bottom-SM margin-top-LRG"
          />
        </ForgotPasswordTextWrapper>
        <BodyCopyWithSpacing
          textAlign="left"
          fontFamily="secondary"
          fontSize="fs13"
          text={getLabelValue(labels, 'lbl_forgotPassword_page_secondary_header', 'password')}
        />
        <ViewWithSpacing spacingStyles="margin-top-XXXL margin-bottom-MED">
          <Field
            label="Email Address"
            name="Email"
            id="Email"
            type="Email"
            component={TextBox}
            value={email}
          />
        </ViewWithSpacing>
        <ViewWithSpacing spacingStyles="margin-bottom-XXXL">
          <ButtonRedesign
            text={getLabelValue(labels, 'lbl_forgotPassword_page_next_btn', 'password')}
            fill="BLUE"
            className="login-button-test-class"
            customStyle={styles.loginButtonStyle}
            borderRadius="16px"
            fontFamily="secondary"
            fontWeight="bold"
            fontSize="fs16"
            paddings="8px"
            showShadow
            onPress={this.submitHandler}
            disableButton={disableResetCTA}
          />
        </ViewWithSpacing>
      </React.Fragment>
    );
  };

  showResetEmailLegacySection = () => {
    const { labels } = this.props;
    const { email, disableResetCTA } = this.state;
    return (
      <React.Fragment>
        <ForgotHeadingStyle>{`Forgot your password? \n No worries!`}</ForgotHeadingStyle>
        <BodyCopyWithSpacing
          textAlign="center"
          fontFamily="secondary"
          fontSize="fs12"
          text={getLabelValue(labels, 'lbl_forgotPassword_content2', 'password')}
          spacingStyles="margin-bottom-LRG padding-left-SM padding-right-SM"
          fullWidth
        />
        <Field
          label="Email Address"
          name="Email"
          id="Email"
          type="Email"
          component={TextBox}
          value={email}
        />
        <CustomButton
          color={colorPallete.white}
          fill="BLUE"
          text={getLabelValue(labels, 'lbl_forgotPassword_resetPassword', 'password')}
          customStyle={styles.createAccountStyle}
          onPress={this.submitHandler}
          disableButton={disableResetCTA}
        />
      </React.Fragment>
    );
  };

  showLegacySuccessullEmail = () => {
    const { labels, resetPasswordSuccessMsg } = this.props;
    const contentHeight = { minHeight: 70 };
    return (
      <React.Fragment>
        <HeadingStyle>
          {getLabelValue(labels, 'lbl_forgotPassword_checkMail', 'password')}
        </HeadingStyle>
        <ViewWithSpacing spacingStyles="margin-top-MED margin-bottom-LRG">
          <RichText
            source={{
              html: resetPasswordSuccessMsg,
            }}
            style={contentHeight}
          />
        </ViewWithSpacing>

        <CustomButton
          fill="BLUE"
          text={getLabelValue(labels, 'lbl_forgotPassword_returnLogin', 'password')}
          buttonVariation="variable-width"
          customStyle={styles.createAccountStyle}
          onPress={this.onBackClick}
        />
      </React.Fragment>
    );
  };

  openCreateAccountModal = () => {
    this.setState({ showRegister: true });
  };

  closeCreateAccountModal = () => {
    this.setState({ showRegister: false });
  };

  getCreateAccountOptions = (isShowOptions) => {
    const { labels } = this.props;
    return (
      isShowOptions && (
        <>
          <LineComp marginTop={26} borderColor="gray.2700" />
          <FormStyleView>
            <LoginFormInfoStyle>
              <BodyCopy
                fontFamily="secondary"
                fontWeight="semibold"
                textAlign="center"
                fontSize="fs14"
                color="gray.900"
                text={getLabelValue(labels, 'lbl_login_createAccountHelp_1_v1', 'login')}
              />
              <BodyCopy
                fontFamily="secondary"
                fontWeight="semibold"
                fontSize="fs14"
                textAlign="center"
                color="gray.900"
                text={getLabelValue(labels, 'lbl_login_createAccountHelp_2_v1', 'login')}
              />
            </LoginFormInfoStyle>
            <CustomButton
              color={colorPallete.primary.navyBlue}
              fill="WHITE"
              type="submit"
              data-locator=""
              fontSize="fs16"
              borderRadius="16px"
              onPress={this.openCreateAccountModal}
              text={getLabelValue(labels, 'lbl_login_createAccountCTA', 'login')}
              fontFamily="secondary"
              fontWeight="bold"
              showBorder
              paddings="8px"
            />
          </FormStyleView>
        </>
      )
    );
  };

  // section visible if user will get the email successfully
  showSuccessullEmail = () => {
    const {
      labels,
      isResetPassCodeValidationEnabled,
      showOnPlccModal,
      isResetViaText,
      successFullResetEmail,
      selectedPassResetOption,
      setPassResetOption,
      isShowPhoneOption,
      isShowOTPScreen,
      toggleIsShowOtpScreen,
      maskedPhoneNumber,
      navigation,
      onRequestClose,
      resetPasswordAction,
    } = this.props;
    const { email, showSmsTerm } = this.state;
    const resendViaEmail = () => {
      this.setState({ showSmsTerm: true });
      setPassResetOption('email');
      resetPasswordAction({
        logonIdResend: email.toUpperCase().trim(),
        target: 'email',
        logonId: email,
      });
    };
    return (
      <React.Fragment>
        {successFullResetEmail && (!isResetViaText || isShowOTPScreen) && (
          <ResetPassword
            labels={labels.password}
            logonId={email}
            isResetPassCodeValidationEnabled={isResetPassCodeValidationEnabled}
            hideTopSection
            showRecaptcha
            onAppBackToLogin={this.onBackClick}
            showOnPlccModal={showOnPlccModal}
            isResetViaText={isResetViaText}
            selectedPassResetOption={selectedPassResetOption}
            maskedPhoneNumber={maskedPhoneNumber}
            toggleIsShowOtpScreen={toggleIsShowOtpScreen}
            navigation={navigation}
            onRequestClose={onRequestClose}
            setPassResetOption={setPassResetOption}
          />
        )}
        {successFullResetEmail && isResetViaText && !isShowOTPScreen && (
          <>
            <ResetPasswordOption
              isResetViaText={isResetViaText}
              setPassResetOption={setPassResetOption}
              selectedPassResetOption={selectedPassResetOption}
              isShowPhoneOption={isShowPhoneOption}
              isShowOTPScreen={isShowOTPScreen}
              toggleIsShowOtpScreen={toggleIsShowOtpScreen}
              labels={labels}
              triggerPassResetApi={this.triggerPassResetApi}
              email={email}
              maskedPhoneNumber={maskedPhoneNumber}
              showSmsTerm={showSmsTerm}
            />
            <ViewWithSpacing spacingStyles="margin-top-LRG margin-bottom-MED">
              <ResendCodeWrapper>
                <BodyCopyWithSpacing
                  textAlign="center"
                  fontFamily="secondary"
                  fontSize="fs14"
                  text={getLabelValue(labels, 'lbl_dont_have_phone', 'password')}
                  spacingStyles="margin-right-XXS"
                />
                <Anchor
                  anchorVariation="custom"
                  colorName="gray.900"
                  fontSizeVariation="large"
                  centered
                  underline
                  text={getLabelValue(labels, 'lbl_send_code_via_email_instead', 'password')}
                  onPress={resendViaEmail}
                />
              </ResendCodeWrapper>
            </ViewWithSpacing>
            {this.getCreateAccountOptions(isResetViaText)}
          </>
        )}
      </React.Fragment>
    );
  };

  showSuccessEmailView = (isResetPassCodeValidationEnabled) =>
    isResetPassCodeValidationEnabled
      ? this.showSuccessullEmail()
      : this.showLegacySuccessullEmail();

  showResetEmailView = (isResetPassCodeValidationEnabled) =>
    isResetPassCodeValidationEnabled
      ? this.showResetEmailSection()
      : this.showResetEmailLegacySection();

  render() {
    const {
      labels,
      successFullResetEmail,
      updateHeader,
      onRequestClose,
      isResetPassCodeValidationEnabled,
      showOnPlccModal,
    } = this.props;
    const { showRegister } = this.state;

    if (updateHeader) updateHeader(); // remove the header and border line of the modal

    if (showRegister) {
      const overrideCustomStyle = {
        borderWidth: 0.5,
      };
      return (
        <SafeAreaView>
          <ModalNative
            heading={getLabelValue(labels, 'lbl_login_createAccountCTA', 'login')}
            isOpen
            onRequestClose={this.closeCreateAccountModal}
            horizontalBar
            showHeader
            headingFontFamily="secondary"
            fontSize="fs16"
            isLoginOrSignup
            showTextForClose
            textForClose={getLabelValue(labels, 'lbl_changePassword_cancelCta', 'password')}
            overrideCustomStyle={overrideCustomStyle}
            fullWidth
          >
            <CreateAccount
              showLogin={this.onBackClick}
              onRequestClose={this.closeCreateAccountModal}
            />
          </ModalNative>
        </SafeAreaView>
      );
    }

    return (
      <View>
        <ForgotPasswordWrapper>
          <FloatWrapper>
            <CustomIconWrapper>
              <BackButtonContainer accessibilityRole="image" onPress={this.onBackClick}>
                <Image source={leftArrow} alt="" width="9px" height="18px" />
                <BodyCopy
                  text={getLabelValue(labels, 'lbl_forgotPassword_backLogin', 'password')}
                  fontFamily="secondary"
                  fontSize="fs16"
                  color="blue.G100"
                />
              </BackButtonContainer>
            </CustomIconWrapper>
            {!showOnPlccModal && (
              <StyledTouchableOpacity
                onPress={onRequestClose}
                accessibilityRole="button"
                accessibilityLabel="close"
              >
                <StyledCrossImage source={closeIcon} />
              </StyledTouchableOpacity>
            )}
          </FloatWrapper>

          {successFullResetEmail
            ? this.showSuccessEmailView(isResetPassCodeValidationEnabled)
            : this.showResetEmailView(isResetPassCodeValidationEnabled)}
          {!isResetPassCodeValidationEnabled ? (
            <LineComp marginTop={28} marginLeft={12} marginRight={12} />
          ) : null}
        </ForgotPasswordWrapper>
      </View>
    );
  }
}

ForgotPasswordView.propTypes = {
  SubmitForgot: PropTypes.string.isRequired,
  showNotification: PropTypes.string.isRequired,
  resetForgotPasswordErrorResponse: PropTypes.string.isRequired,
  labels: PropTypes.string.isRequired,
  successFullResetEmail: PropTypes.string.isRequired,
  handleSubmit: PropTypes.string.isRequired,
  toastMessage: PropTypes.func,
  resetPasswordSuccessMsg: PropTypes.string.isRequired,
  showForgotPasswordForm: PropTypes.func.isRequired,
  showLogin: PropTypes.bool.isRequired,
  updateHeader: PropTypes.func.isRequired,
  onRequestClose: PropTypes.func.isRequired,
  invalid: PropTypes.bool.isRequired,
  isResetPassCodeValidationEnabled: PropTypes.bool.isRequired,
  showOnPlccModal: PropTypes.bool,
  isResetViaText: PropTypes.bool,
  fetchPhoneActn: PropTypes.func,
};

ForgotPasswordView.defaultProps = {
  toastMessage: () => {},
  showOnPlccModal: false,
  isResetViaText: false,
  fetchPhoneActn: () => {},
};

const validateMethod = createValidateMethod(getStandardConfig(['Email']));

export default reduxForm({
  form: 'ForgotPasswordView',
  enableReinitialize: true,
  ...validateMethod, // a unique identifier for this form
})(withStyles(ForgotPasswordView, FormStyle));
export { ForgotPasswordView as ForgotPasswordViewVanilla };
