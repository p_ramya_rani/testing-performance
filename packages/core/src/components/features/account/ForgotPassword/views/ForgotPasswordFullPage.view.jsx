// 9fbef606107a605d69c0edbcd8029e5d
import React, { useState } from 'react';
import { Field } from 'redux-form';
import PropTypes from 'prop-types';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import ResetPassword from '@tcp/core/src/components/features/account/ResetPassword';
import ResetPasswordResendEmail from '@tcp/core/src/components/features/account/ResetPassword/molecules/ResetPasswordResendEmail';
import ResetPasswordOption from '@tcp/core/src/components/features/account/ResetPassword/views/ResetPasswordOption.view';
import { routerPush } from '@tcp/core/src/utils';
import { TextBox, RichText, Button, BodyCopy } from '../../../../common/atoms';
import Notification from '../../../../common/molecules/Notification';

const ForgotPasswordFullPageView = ({
  pristine,
  className,
  showNotification,
  labels,
  successFullResetEmail,
  handleSubmit,
  onFormSubmit,
  forgotPasswordErrorMessage,
  isFormSubmitting,
  email,
  openOverlay,
  fullPageAuthEnabled,
  isResetViaText,
  setPassResetOption,
  selectedPassResetOption,
  isShowPhoneOption,
  isShowOTPScreen,
  toggleIsShowOtpScreen,
  triggerPassResetApi,
  maskedPhoneNumber,
  isAccountNotFoundError,
  resetPasswordAction,
}) => {
  const [showSmsTerm, setShowSmsTerm] = useState(false);
  const backToLogin = () => {
    routerPush('/home?target=login&redirectFromReset=true', '/home/login?redirectFromReset=true');
  };

  const showCreateAccountForm = () => {
    if (fullPageAuthEnabled) {
      routerPush('/home?target=register&successtarget=referrer', '/home/register');
    } else {
      openOverlay({
        component: 'createAccount',
        variation: 'primary',
      });
    }
  };

  const resendViaEmail = () => {
    setShowSmsTerm(true);
    setPassResetOption('email');
    resetPasswordAction({
      logonIdResend: email.toUpperCase().trim(),
      target: 'email',
      logonId: email,
    });
  };

  const getCreateAccountOptions = (isShowOptions) => {
    return (
      isShowOptions && (
        <>
          <BodyCopy component="div" className="border elem-pt-MED elem-pb-LRG">
            <BodyCopy fontFamily="secondary" fontSize="fs12" textAlign="center">
              {getLabelValue(labels, 'lbl_login_createAccountHelp', 'login')}
            </BodyCopy>
          </BodyCopy>
          <Button
            className="create-acc-cta"
            fill="WHITE"
            type="submit"
            buttonVariation="fixed-width"
            data-locator=""
            onClick={showCreateAccountForm}
          >
            {getLabelValue(labels, 'lbl_login_createAccountCTA', 'login')}
          </Button>
        </>
      )
    );
  };

  return (
    <>
      <div className={className}>
        <form onSubmit={handleSubmit(onFormSubmit)} className={`${className} forgot-password-form`}>
          {showNotification && forgotPasswordErrorMessage && (
            <Notification
              status="error"
              colSize={{ large: 11, medium: 7, small: 6 }}
              message={
                <RichText className="richTextColor" richTextHtml={forgotPasswordErrorMessage} />
              }
            />
          )}
          {isAccountNotFoundError && isResetViaText && (
            <Notification
              status="error"
              colSize={{ large: 11, medium: 7, small: 6 }}
              message={getLabelValue(labels, 'lbl_forgotPassword_userNotAvailable', 'password')}
            />
          )}

          {!successFullResetEmail && (
            <React.Fragment>
              <BodyCopy
                textAlign="center"
                fontFamily="secondary"
                fontSize="fs28"
                fontWeight="semibold"
                color="TEXT.DARK"
                className="primary-header-text"
              >
                <span className="forgot-password-text">
                  {getLabelValue(labels, 'lbl_forgotPassword_content1', 'password')}
                </span>
              </BodyCopy>
              <BodyCopy
                fontWeight="regular"
                fontFamily="secondary"
                textAlign="center"
                className="elem-mb-SM secondary-header-text"
                fontSize="fs12"
                color="TEXT.DARKERGRAY"
              >
                {getLabelValue(labels, 'lbl_forgotPassword_page_secondary_header', 'password')}
              </BodyCopy>
              <BodyCopy component="div" className="elem-mb-LRG">
                <Field
                  name="Email"
                  placeholder={getLabelValue(labels, 'lbl_forgotPassword_emailAddress', 'password')}
                  id="Email"
                  type="text"
                  component={TextBox}
                  value={email}
                />
              </BodyCopy>
              <Button
                fill="BLUE"
                disabled={pristine || isFormSubmitting}
                type="submit"
                buttonVariation="fixed-width"
              >
                {getLabelValue(labels, 'lbl_forgotPassword_page_next_btn', 'password')}
              </Button>
            </React.Fragment>
          )}
        </form>

        {getCreateAccountOptions(!successFullResetEmail)}

        {successFullResetEmail && (!isResetViaText || isShowOTPScreen) && (
          <ResetPassword
            labels={labels.password}
            fullPageView
            email={email}
            backToLoginAction={backToLogin}
            isResetViaText={isResetViaText}
            selectedPassResetOption={selectedPassResetOption}
            maskedPhoneNumber={maskedPhoneNumber}
            toggleIsShowOtpScreen={toggleIsShowOtpScreen}
            setPassResetOption={setPassResetOption}
          />
        )}
        {successFullResetEmail && isResetViaText && !isShowOTPScreen && (
          <>
            <ResetPasswordOption
              isResetViaText={isResetViaText}
              setPassResetOption={setPassResetOption}
              selectedPassResetOption={selectedPassResetOption}
              isShowPhoneOption={isShowPhoneOption}
              isShowOTPScreen={isShowOTPScreen}
              toggleIsShowOtpScreen={toggleIsShowOtpScreen}
              labels={labels}
              triggerPassResetApi={triggerPassResetApi}
              email={email}
              maskedPhoneNumber={maskedPhoneNumber}
              isFormSubmitting={isFormSubmitting}
              showSmsTerm={showSmsTerm}
            />
            <div className="create-account-options-wrapper">
              {isResetViaText && (
                <div className="send-via-email elem-mt-LRG elem-mb-LRG hide-on-desktop hide-on-tablet">
                  <ResetPasswordResendEmail
                    resendEmail={resendViaEmail}
                    lblCodeNotReceived={getLabelValue(labels, 'lbl_dont_have_phone', 'password')}
                    lblResentCode={getLabelValue(
                      labels,
                      'lbl_send_code_via_email_instead',
                      'password'
                    )}
                    isSmsSelected={false}
                    isResetViaText={isResetViaText}
                  />
                </div>
              )}
              {getCreateAccountOptions(isResetViaText)}
            </div>
          </>
        )}
      </div>
    </>
  );
};

ForgotPasswordFullPageView.propTypes = {
  className: PropTypes.string.isRequired,
  pristine: PropTypes.bool.isRequired,
  SubmitForgot: PropTypes.shape({}).isRequired,
  showNotification: PropTypes.func.isRequired,
  labels: PropTypes.shape({}).isRequired,
  successFullResetEmail: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  isFormSubmitting: PropTypes.bool.isRequired,
  email: PropTypes.string.isRequired,
  onFormSubmit: PropTypes.func.isRequired,
  forgotPasswordErrorMessage: PropTypes.string.isRequired,
  openOverlay: PropTypes.func.isRequired,
  fullPageAuthEnabled: PropTypes.bool.isRequired,
};

export default ForgotPasswordFullPageView;
