// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components';

const ForgotPasswordTextWrapper = styled.View`
  width: 160px;
`;

const ResendCodeWrapper = styled.View`
  display: flex;
  flex-direction: column;
  text-align: center;
  align-items: center;
  justify-content: center;
`;
const BackButtonContainer = styled.TouchableOpacity`
  display: flex;
  flex-direction: row;
  text-align: center;
  align-items: center;
  justify-content: center;
`;

export { ForgotPasswordTextWrapper, ResendCodeWrapper, BackButtonContainer };
