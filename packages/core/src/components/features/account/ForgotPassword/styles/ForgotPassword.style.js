// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const styles = css`
  &.forgot-password-form,
  .forgot-password-form {
    margin: 30px 0;

    .heading-link {
      margin: 10px 0;
      a {
        color: ${(props) => props.theme.colorPalette.black};
      }
    }

    .forgot-password-text {
      display: block;
    }

    .elem-mb-SM {
      max-width: 241px;
      margin: 12px auto 25px;
    }
    .secondary-header-text {
      margin-top: 0;
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        text-align: left;
        margin-left: 0;
      }
    }
    .primary-header-text {
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        text-align: left;
      }
    }
  }
  .border {
    border-top: 1px solid ${(props) => props.theme.colors.BORDER.BLUE};
    width: 91%;
    margin: 0 auto;
    p {
      max-width: 227px;
      margin: 0 auto;
    }
  }

  .create-acc-cta {
    margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
  }

  .create-account-options-wrapper {
    margin-top: 27px;
    .border {
      border-top: 4px solid ${(props) => props.theme.colorPalette.gray[300]};
    }
    .create-acc-cta {
      border-radius: ${(props) => props.isResetViaText && props.theme.spacing.ELEM_SPACING.XS_6};
    }
    @media ${(props) => props.theme.mediaQuery.large} {
      padding: 0 30px;
    }
  }

  .border.elem-pt-MED {
    padding-top: 27px;
  }
  .border.elem-pt-LRG {
    padding-bottom: 27px;
  }
  .pass-reset-option-container {
    .radio-button-checked,
    .radio-button {
      margin-top: 18px;
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        margin-top: 6px;
      }
    }
    .input-radio-title {
      color: ${(props) => props.theme.colorPalette.blue.C900};
      font-weight: ${(props) => props.theme.fonts.fontWeight.extrabold};
    }
    .secondary-header-text {
      margin-top: 0;
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        text-align: left;
        margin-left: 0;
      }
    }
    .primary-header-text {
      @media ${(props) => props.theme.mediaQuery.smallOnly} {
        text-align: left;
      }
    }
    .input-subtitle {
      font-size: ${(props) => props.theme.typography.fontSizes.fs16};
    }
  }
`;

export default styles;
