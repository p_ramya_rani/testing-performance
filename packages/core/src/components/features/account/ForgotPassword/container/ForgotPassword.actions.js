// 9fbef606107a605d69c0edbcd8029e5d
/**
 * These are temporary changes for a dummy login page
 */
// @flow
import FORGOTPASSWORD_CONSTANTS from '../ForgotPassword.constants';

export const resetPassword = (payload) => {
  return {
    type: FORGOTPASSWORD_CONSTANTS.RESET_PASSWORD,
    payload,
  };
};

export const resetLoginForgotPasswordState = (payload) => {
  return {
    type: FORGOTPASSWORD_CONSTANTS.RESET_PASSWORD_LOGIN_STATE,
    payload,
  };
};

export const successFullResetEmailState = (payload) => {
  return {
    type: FORGOTPASSWORD_CONSTANTS.RESET_PASSWORD_SUCCESSFULL_EMAIL,
    payload,
  };
};

export const getResetPasswordSuccess = (payload) => {
  return {
    type: FORGOTPASSWORD_CONSTANTS.RESET_PASSWORD_SUCCESS,
    payload,
  };
};

export const userNotAvailable = (payload) => {
  return {
    type: FORGOTPASSWORD_CONSTANTS.USER_NOT_AVAILABLE,
    payload,
  };
};

export const getResetPasswordFail = (payload) => {
  return {
    type: FORGOTPASSWORD_CONSTANTS.RESET_PASSWORD_FAIL,
    payload,
  };
};

export const forgotPasswordFormSubmitStart = () => ({
  type: FORGOTPASSWORD_CONSTANTS.FORGOT_PASSWORD_SUBMIT_START,
});

export const forgotPasswordFormSubmitStop = () => ({
  type: FORGOTPASSWORD_CONSTANTS.FORGOT_PASSWORD_SUBMIT_STOP,
});

export const setLogonId = (payload) => {
  return {
    payload,
    type: FORGOTPASSWORD_CONSTANTS.FORGOT_PASSWORD_SET_LOGON_ID,
  };
};

export const passResetOptionSelector = (payload) => {
  return {
    payload,
    type: FORGOTPASSWORD_CONSTANTS.PASS_RESET_OPTION_SELECTOR,
  };
};

export const showPhoneResetOption = (payload) => {
  return {
    payload,
    type: FORGOTPASSWORD_CONSTANTS.SHOW_PHONE_RESET_OPTION,
  };
};

export const setIsShowOtpScreen = (payload) => {
  return {
    payload,
    type: FORGOTPASSWORD_CONSTANTS.SHOW_OTP_SCREEN,
  };
};

export const setMaskedPhoneNumber = (payload) => {
  return {
    payload,
    type: FORGOTPASSWORD_CONSTANTS.MASKED_PHONE_NUMBER,
  };
};

export const sendOTPError = (payload) => {
  return {
    payload,
    type: FORGOTPASSWORD_CONSTANTS.SEND_OTP_ERROR,
  };
};
