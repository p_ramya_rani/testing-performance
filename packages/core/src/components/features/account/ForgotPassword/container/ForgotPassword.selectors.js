// 9fbef606107a605d69c0edbcd8029e5d
import { createSelector } from 'reselect';
import { FORGOTPASSWORD_REDUCER_KEY } from '@tcp/core/src/constants/reducer.constants';
import { getErrorSelector } from '../../../../../utils/utils';

export const getResetEmailResponse = (state) => {
  return state[FORGOTPASSWORD_REDUCER_KEY];
};

export const getForgotPasswordErrorResponse = (state) => {
  return state[FORGOTPASSWORD_REDUCER_KEY].get('error');
};

export const getLabels = (state) => state.Labels.global;

export const getForgotpasswordLabels = createSelector(
  getLabels,
  (labels) => labels && labels.login
);

export const getShowNotificationState = createSelector(
  getResetEmailResponse,
  (resp) => resp && resp.get('showNotification')
);

export const toggleSuccessfulEmailSection = createSelector(
  getResetEmailResponse,
  (resp) => resp && resp.get('toggleSuccessfulEmailSection')
);

export const getForgotPasswordErrorMessage = createSelector(
  [getForgotPasswordErrorResponse, getForgotpasswordLabels],
  (state, labels) => {
    return getErrorSelector(state, labels, 'lbl_login_error');
  }
);

export const showCheckEmailSectionState = createSelector(
  getResetEmailResponse,
  (resp) => resp && resp.get('showCheckEmailSection')
);

export const getIsFormSubmitting = (state) => {
  return state[FORGOTPASSWORD_REDUCER_KEY].get('formSubmitting');
};

export const getLogonId = (state) => {
  return state[FORGOTPASSWORD_REDUCER_KEY].get('logonId');
};

export const getSelectedPassResetOption = (state) => {
  return state[FORGOTPASSWORD_REDUCER_KEY].get('resetOption');
};

export const getIsShowPhoneOption = (state) => {
  return state[FORGOTPASSWORD_REDUCER_KEY].get('isShowPhoneOption');
};

export const getIsShowOTPScreen = (state) => {
  return state[FORGOTPASSWORD_REDUCER_KEY].get('isShowShowOTPScreen');
};

export const getMaskedPhoneNumber = (state) => {
  return state[FORGOTPASSWORD_REDUCER_KEY].get('maskedPhoneNumber');
};

export const getAccountNotFoundError = (state) => {
  return state[FORGOTPASSWORD_REDUCER_KEY].get('accountNotFound');
};
