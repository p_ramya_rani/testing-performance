// 9fbef606107a605d69c0edbcd8029e5d
/**
 * These are temporary changes for a dummy login page
 */

import { call, takeLatest, put } from 'redux-saga/effects';
import FORGOTPASSWORD_CONSTANTS from '../ForgotPassword.constants';
import {
  getResetPasswordSuccess,
  userNotAvailable,
  getResetPasswordFail,
  forgotPasswordFormSubmitStart,
  forgotPasswordFormSubmitStop,
  setLogonId,
  setIsShowOtpScreen,
} from './ForgotPassword.actions';
import { forgotPassword } from '../../../../../services/abstractors/account';

export function* ForgotPassword(action) {
  try {
    const body = {
      formFlag: 'true',
      isPasswordReset: 'true',
      logonId: action.payload.logonId,
      reLogonURL: 'ChangePassword',
      target: action.payload.target,
    };
    yield put(forgotPasswordFormSubmitStart());
    yield put(setLogonId(action.payload.logonId));
    const res = yield call(forgotPassword, body);
    /* istanbul ignore else */
    if (res) {
      yield put(getResetPasswordSuccess({ state: true }));
      yield put(setIsShowOtpScreen(true));
    }
    yield put(forgotPasswordFormSubmitStop());
  } catch (err) {
    let error = {};
    /* istanbul ignore else */
    error = err;
    yield put(forgotPasswordFormSubmitStop());
    if (error && error.response && error.response.body) {
      return yield put(userNotAvailable(error.response.body.errors[0]));
    }
    yield put(getResetPasswordFail({ state: true }));
  }
}
export function* ForgotPasswordSaga() {
  yield takeLatest(FORGOTPASSWORD_CONSTANTS.RESET_PASSWORD, ForgotPassword);
}

export default ForgotPasswordSaga;
