// 9fbef606107a605d69c0edbcd8029e5d
import { fromJS } from 'immutable';
import { SET_SUBMIT_SUCCEEDED, CHANGE } from 'redux-form/lib/actionTypes';
import FORGOTPASSWORD_CONSTANTS from '../ForgotPassword.constants';

const initialState = fromJS({
  showNotification: false,
  toggleSuccessfulEmailSection: null,
  error: null,
  formSubmitting: false,
  logonId: null,
  resetOption: null,
  isShowPhoneOption: false,
  isShowShowOTPScreen: false,
  maskedPhoneNumber: '',
  accountNotFound: null,
});
let checkErrorReset = false;

/**
 * @function defaultStateReducer used for return  default state reducer state
 * @param {state} state provide initial state to this function.
 * @return {state} return final state of the reducer.
 */

const defaultStateReducer = (state = initialState) => {
  // TODO: currently when initial state is hydrated on browser, List is getting converted to an JS Array
  if (state instanceof Object) {
    return fromJS(state);
  }
  return state;
};

/**
 * @function resetErrorReducer  Used to return Error state null when we change any form field .
 * @case SET_SUBMIT_SUCCEEDED used for set checkErrorReset is true when we got success form response after submit the form.
 * @case CHANGE used for reset value of checkErrorReset is false and set error to null.
 *  Using this error state we have hide form notification error.
 * @param {state} state provide initial state to this function.
 * @return {state} return final state of the reducer.
 */

const resetErrorReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_SUBMIT_SUCCEEDED: {
      if (action.meta.form === FORGOTPASSWORD_CONSTANTS.FORGOT_PASSWORD_FORM) {
        checkErrorReset = true;
      }
      return state;
    }
    case CHANGE: {
      if (checkErrorReset && action.meta.form === FORGOTPASSWORD_CONSTANTS.FORGOT_PASSWORD_FORM) {
        checkErrorReset = false;
        return state.set('error', null).set('showNotification', false);
      }
      return state;
    }
    default:
      return defaultStateReducer(state);
  }
};

const ForgotPasswordReducer = (state = initialState, action) => {
  switch (action.type) {
    case FORGOTPASSWORD_CONSTANTS.RESET_PASSWORD:
      return state.set('showNotification', false).set('error', null);
    case FORGOTPASSWORD_CONSTANTS.RESET_PASSWORD_SUCCESS:
      return state.set('toggleSuccessfulEmailSection', true);
    case FORGOTPASSWORD_CONSTANTS.RESET_PASSWORD_FAIL:
      return state.set('showNotification', action.payload.state);
    case FORGOTPASSWORD_CONSTANTS.RESET_PASSWORD_LOGIN_STATE:
      return state
        .set('showNotification', false)
        .set('toggleSuccessfulEmailSection', null)
        .set('error', null)
        .set('resetOption', null)
        .set('isShowPhoneOption', false)
        .set('isShowShowOTPScreen', false)
        .set('maskedPhoneNumber', '');
    case FORGOTPASSWORD_CONSTANTS.RESET_PASSWORD_SUCCESSFULL_EMAIL:
      return state.set('toggleSuccessfulEmailSection', true);
    case FORGOTPASSWORD_CONSTANTS.USER_NOT_AVAILABLE:
      return state.set('error', fromJS(action.payload)).set('showNotification', true);
    case FORGOTPASSWORD_CONSTANTS.FORGOT_PASSWORD_SUBMIT_START:
      return state.set('formSubmitting', true);
    case FORGOTPASSWORD_CONSTANTS.FORGOT_PASSWORD_SUBMIT_STOP:
      return state.set('formSubmitting', false);
    case FORGOTPASSWORD_CONSTANTS.FORGOT_PASSWORD_SET_LOGON_ID:
      return state.set('logonId', action.payload);
    case FORGOTPASSWORD_CONSTANTS.PASS_RESET_OPTION_SELECTOR:
      return state.set('resetOption', action.payload);
    case FORGOTPASSWORD_CONSTANTS.SHOW_PHONE_RESET_OPTION:
      return state.set('isShowPhoneOption', action.payload);
    case FORGOTPASSWORD_CONSTANTS.SHOW_OTP_SCREEN:
      return state.set('isShowShowOTPScreen', action.payload);
    case FORGOTPASSWORD_CONSTANTS.MASKED_PHONE_NUMBER:
      return state.set('maskedPhoneNumber', action.payload);
    case FORGOTPASSWORD_CONSTANTS.SEND_OTP_ERROR:
      return state.set('accountNotFound', action.payload);
    default:
      return resetErrorReducer(state, action);
  }
};

export default ForgotPasswordReducer;
