// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { toastMessageInfo } from '@tcp/core/src/components/common/atoms/Toast/container/Toast.actions';
import {
  getResetPasswordSuccessMsg,
  getCommonLabels,
} from '@tcp/core/src/components/features/account/Account/container/Account.selectors';
import { getIsResetViaText } from '@tcp/core/src/reduxStore/selectors/session.selectors';
import { getEnableFullPageAuth } from '@tcp/web/src/components/features/content/HomePage/HomePage.selectors';
import { getModuleXContent } from '@tcp/core/src/components/features/account/Account/container/Account.actions';
import { fetchPhone } from '@tcp/core/src/components/features/account/ResetPassword/container/ResetPassword.actions';
import { getViewportInfo, isMobileApp, isClient } from '@tcp/core/src/utils';
import {
  resetPassword,
  resetLoginForgotPasswordState,
  passResetOptionSelector,
  setIsShowOtpScreen,
  sendOTPError,
} from './ForgotPassword.actions';
import {
  getShowNotificationState,
  getResetEmailResponse,
  toggleSuccessfulEmailSection,
  getForgotPasswordErrorMessage,
  getIsFormSubmitting,
  getSelectedPassResetOption,
  getIsShowPhoneOption,
  getIsShowOTPScreen,
  getMaskedPhoneNumber,
  getAccountNotFoundError,
} from './ForgotPassword.selectors';
import { resetUserInfo } from '../../User/container/User.actions';
import {
  closeOverlayModal,
  openOverlayModal,
} from '../../OverlayModal/container/OverlayModal.actions';
import { getLoginError, shouldShowRecaptcha } from '../../LoginPage/container/LoginPage.selectors';
import { getUserLoggedInState } from '../../User/container/User.selectors';
import ForgotPasswordView from '../views/ForgotPassword.view';

class ForgotPasswordContainer extends React.PureComponent {
  componentDidMount() {
    const { resetPasswordSuccessMsg, getAccountModuleXContent, commonLabels } = this.props;
    if (!resetPasswordSuccessMsg && commonLabels) {
      getAccountModuleXContent(commonLabels.referred);
    }
  }

  componentWillUnmount() {
    const { resetForm } = this.props;
    if (resetForm) resetForm();
  }

  getMaskedPhoneNumber = (string, n, find, repl) => {
    if (string.length < 1) {
      return '';
    }
    const stringWithDash = string.match(/.{1,3}/g).join('-');
    const stringWithTwoDash = stringWithDash.split(find);
    return stringWithTwoDash.slice(0, n).join(find) + repl + stringWithTwoDash.slice(n).join(find);
  };

  render() {
    const {
      resetForgotPasswordErrorResponse,
      resetForm,
      SubmitForgot,
      showNotification,
      successFullResetEmail,
      resetLoginState,
      showForgotPasswordForm,
      labels,
      showLogin,
      forgotPasswordErrorMessage,
      toastMessage,
      updateHeader,
      onRequestClose,
      resetPasswordSuccessMsg,
      isFormSubmitting,
      fullPageView,
      openOverlay,
      setPassResetOption,
      selectedPassResetOption,
      isShowPhoneOption,
      isShowOTPScreen,
      toggleIsShowOtpScreen,
      maskedPhoneNumber,
      isAccountNotFoundError,
      resetSendOTPError,
      isResetViaText,
      ...otherProps
    } = this.props;
    const initialValues = {
      rememberMe: true,
      savePlcc: true,
    };
    const viewPortInfo = !isMobileApp() && getViewportInfo();
    const isMobileView = viewPortInfo && isClient() && viewPortInfo.isMobile;
    let resetOption = isResetViaText ? selectedPassResetOption : null;
    if (!selectedPassResetOption && isResetViaText) {
      resetOption = isMobileApp() || isMobileView ? 'sms' : null;
    }
    const updatedMaskedNumber =
      maskedPhoneNumber && this.getMaskedPhoneNumber(maskedPhoneNumber, 3, '-', '');

    return (
      <ForgotPasswordView
        onRequestClose={onRequestClose}
        showForgotPasswordForm={showForgotPasswordForm}
        labels={labels}
        resetPasswordSuccessMsg={resetPasswordSuccessMsg}
        initialValues={initialValues}
        resetForgotPasswordErrorResponse={resetForgotPasswordErrorResponse}
        resetForm={resetForm}
        SubmitForgot={SubmitForgot}
        showNotification={showNotification}
        successFullResetEmail={successFullResetEmail}
        resetLoginState={resetLoginState}
        showLogin={showLogin}
        forgotPasswordErrorMessage={forgotPasswordErrorMessage}
        toastMessage={toastMessage}
        updateHeader={updateHeader}
        isFormSubmitting={isFormSubmitting}
        fullPageView={fullPageView}
        openOverlay={openOverlay}
        setPassResetOption={setPassResetOption}
        selectedPassResetOption={resetOption}
        isShowPhoneOption={isShowPhoneOption}
        isShowOTPScreen={isShowOTPScreen}
        toggleIsShowOtpScreen={toggleIsShowOtpScreen}
        maskedPhoneNumber={updatedMaskedNumber}
        isAccountNotFoundError={isAccountNotFoundError}
        resetSendOTPError={resetSendOTPError}
        isResetViaText={isResetViaText}
        {...otherProps}
      />
    );
  }
}

ForgotPasswordContainer.propTypes = {
  resetLoginState: PropTypes.string,
  resetForgotPasswordErrorResponse: PropTypes.bool.isRequired,
  resetForm: PropTypes.bool.isRequired,
  SubmitForgot: PropTypes.bool.isRequired,
  showNotification: PropTypes.bool.isRequired,
  successFullResetEmail: PropTypes.bool.isRequired,
  showForgotPasswordForm: PropTypes.bool.isRequired,
  labels: PropTypes.shape({}).isRequired,
  showLogin: PropTypes.func,
  forgotPasswordErrorMessage: PropTypes.shape({}).isRequired,
  toastMessage: PropTypes.string.isRequired,
  updateHeader: PropTypes.func,
  onRequestClose: PropTypes.func,
  resetPasswordSuccessMsg: PropTypes.string,
  commonLabels: PropTypes.shape({}).isRequired,
  getAccountModuleXContent: PropTypes.func,
  isFormSubmitting: PropTypes.bool.isRequired,
  fullPageView: PropTypes.bool,
  openOverlay: PropTypes.func,
  fullPageAuthEnabled: PropTypes.bool,
  setPassResetOption: PropTypes.func,
  selectedPassResetOption: PropTypes.string,
  isShowPhoneOption: PropTypes.bool,
  isShowOTPScreen: PropTypes.bool,
  toggleIsShowOtpScreen: PropTypes.func,
  maskedPhoneNumber: PropTypes.string,
  isAccountNotFoundError: PropTypes.bool,
};

ForgotPasswordContainer.defaultProps = {
  resetLoginState: () => {},
  showLogin: () => {},
  updateHeader: () => {},
  onRequestClose: () => {},
  resetPasswordSuccessMsg: '',
  getAccountModuleXContent: () => {},
  fullPageView: false,
  openOverlay: () => {},
  fullPageAuthEnabled: false,
  setPassResetOption: () => {},
  selectedPassResetOption: '',
  isShowPhoneOption: false,
  isShowOTPScreen: false,
  toggleIsShowOtpScreen: () => {},
  maskedPhoneNumber: '',
  isAccountNotFoundError: false,
};

const mapDispatchToProps = (dispatch) => {
  return {
    resetLoginState: () => {
      dispatch(resetUserInfo());
    },
    SubmitForgot: (payload) => {
      dispatch(resetPassword(payload));
    },
    fetchPhoneActn: (payload) => {
      dispatch(fetchPhone(payload));
    },
    resetForm: (payload) => {
      dispatch(resetLoginForgotPasswordState(payload));
    },
    closeOverlay: () => {
      dispatch(closeOverlayModal());
    },
    openOverlay: (payload) => {
      dispatch(openOverlayModal(payload));
    },
    toastMessage: (palyoad) => {
      dispatch(toastMessageInfo(palyoad));
    },
    getAccountModuleXContent: (referred) => {
      dispatch(getModuleXContent(referred));
    },
    setPassResetOption: (payload) => {
      dispatch(passResetOptionSelector(payload));
    },
    toggleIsShowOtpScreen: (payload) => {
      dispatch(setIsShowOtpScreen(payload));
    },
    resetSendOTPError: (payload) => {
      dispatch(sendOTPError(payload));
    },
    resetPasswordAction: (payload) => {
      dispatch(resetPassword(payload));
    },
  };
};

const mapStateToProps = (state) => {
  return {
    showNotification: getShowNotificationState(state),
    resetForgotPasswordErrorResponse: getResetEmailResponse(state),
    successFullResetEmail: toggleSuccessfulEmailSection(state),
    isUserLoggedIn: getUserLoggedInState(state),
    loginError: getLoginError(state),
    forgotPasswordErrorMessage: getForgotPasswordErrorMessage(state),
    showRecaptcha: shouldShowRecaptcha(state),
    resetPasswordSuccessMsg: getResetPasswordSuccessMsg(state),
    commonLabels: getCommonLabels(state),
    isFormSubmitting: getIsFormSubmitting(state),
    fullPageAuthEnabled: getEnableFullPageAuth(state),
    isResetViaText: getIsResetViaText(state),
    selectedPassResetOption: getSelectedPassResetOption(state),
    isShowPhoneOption: getIsShowPhoneOption(state),
    isShowOTPScreen: getIsShowOTPScreen(state),
    maskedPhoneNumber: getMaskedPhoneNumber(state),
    isAccountNotFoundError: getAccountNotFoundError(state),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPasswordContainer);
