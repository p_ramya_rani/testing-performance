// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

const Styles = css`
  .viewAccAnchor {
    float: right;
  }
  .userName {
    padding-left: 10px;
    vertical-align: super;
  }
`;

export default Styles;
