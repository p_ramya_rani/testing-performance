// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { shallow } from 'enzyme';
import { routerPush, isCanada } from '@tcp/core/src/utils';
import { AccountDrawerBottomLinksVanilla } from '../AccountDrawerBottomLinks';

jest.mock('@tcp/core/src/utils', () => ({
  routerPush: jest.fn(),
  isCanada: jest.fn(),
}));

const props = {
  labels: {
    CREATE_ACC_MY_FAV: 'My Favorites',
    CREATE_ACC_MY_PLACE_REWARDS_CC: 'My Place Rewards Credit Card',
    CREATE_ACC_WALLET: 'Wallet',
    CREATE_ACC_ORDERS: 'Orders',
    CREATE_ACC_SIGN_OUT: 'Sign Out',
  },
  closedOverlay: jest.fn(),
};

describe('AccountDrawerBottomLinks', () => {
  it('should render correctly', () => {
    const tree = shallow(<AccountDrawerBottomLinksVanilla {...props} />);
    expect(tree).toMatchSnapshot();
  });

  it('should redirect to Orders page', () => {
    const mockedRouterPush = jest.fn();
    routerPush.mockImplementation(mockedRouterPush);
    const onLinkRedirect = jest.fn();
    const tree = shallow(<AccountDrawerBottomLinksVanilla {...props} />);
    tree.find("[dataLocator='orders']").simulate('click', { preventDefault: jest.fn() });
    onLinkRedirect.mockImplementation();
    expect(mockedRouterPush).toBeCalledWith('/account?id=orders', '/account/orders');
  });

  it('should redirect to rewardsCreditCard page', () => {
    const mockedRouterPush = jest.fn();
    isCanada.mockImplementation(() => true);
    routerPush.mockImplementation(mockedRouterPush);
    const onLinkRedirect = jest.fn();
    const tree = shallow(<AccountDrawerBottomLinksVanilla {...props} />);
    tree.find("[dataLocator='rewards']").simulate('click', { preventDefault: jest.fn() });
    onLinkRedirect.mockImplementation();
    expect(mockedRouterPush).toBeCalledWith('/account?id=rewardsCreditCard', '/rewardsCreditCard');
  });

  it('should redirect to wallet page', () => {
    const mockedRouterPush = jest.fn();
    routerPush.mockImplementation(mockedRouterPush);
    const onLinkRedirect = jest.fn();
    const tree = shallow(<AccountDrawerBottomLinksVanilla {...props} />);
    tree.find("[dataLocator='wallet']").simulate('click', { preventDefault: jest.fn() });
    onLinkRedirect.mockImplementation();
    expect(mockedRouterPush).toBeCalledWith('/account?id=wallet', '/account/wallet');
  });

  it('should redirect to favorites page', () => {
    const mockedRouterPush = jest.fn();
    routerPush.mockImplementation(mockedRouterPush);
    const onLinkRedirect = jest.fn();
    const tree = shallow(<AccountDrawerBottomLinksVanilla {...props} />);
    tree.find("[dataLocator='favorites']").simulate('click', { preventDefault: jest.fn() });
    onLinkRedirect.mockImplementation();
    expect(mockedRouterPush).toBeCalledWith('/account?id=favorites', '/account/favorites');
  });
});
