// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import { getLearnMorePayload } from '@tcp/core/src/constants/analytics';
import ClickTracker from '@tcp/web/src/components/common/atoms/ClickTracker';
import { getLabelValue } from '@tcp/core/src/utils/utils';
import internalEndpoints from '@tcp/core/src/components/features/account/common/internalEndpoints';
import { routerPush } from '@tcp/core/src/utils';
import withStyles from '../../../../../../common/hoc/withStyles';
import styles from '../styles/ExtraPointsTeaser.style';
import Anchor from '../../../../../../common/atoms/Anchor';
import BodyCopy from '../../../../../../common/atoms/BodyCopy';

const ExtraPointsTeaser = props => {
  const { className, plccUser, globalLabels, pageCategory } = props;

  /**
   * This function will handle click to go to respective links
   * @param {event, link, path} -
   */
  const onLinkRedirect = ({ e, link, path }) => {
    e.preventDefault();
    const { closedOverlay } = props;
    routerPush(link, path);
    closedOverlay();
  };

  const handleLearnMoreClick = e => {
    onLinkRedirect({
      e,
      link: internalEndpoints.extraPointsPage.link,
      path: internalEndpoints.extraPointsPage.path,
    });
  };

  return (
    <div className={`${className} elem-pt-MED elem-pr-XXL elem-pb-LRG elem-pl-XXL`}>
      <div
        className={`extraPointsWrapper${
          plccUser ? '_plcc' : ''
        } elem-pl-MED elem-pr-MED elem-pt-SM elem-pb-SM`}
      >
        <div className="earnExtra alignCenter">
          <BodyCopy fontFamily="secondary" fontWeight="extrabold" fontSize="fs14">
            {getLabelValue(globalLabels, 'lbl_drawer_earn_extra', 'accountDrawer')}
          </BodyCopy>
        </div>
        <div className="getCloser alignCenter elem-pt-XS">
          <BodyCopy
            fontFamily="secondary"
            fontWeight="extrabold"
            fontSize="fs12"
            color="text.secondary"
          >
            {getLabelValue(globalLabels, 'lbl_drawer_get_closer', 'accountDrawer')}
          </BodyCopy>
        </div>
        <div className="learnMore alignCenter elem-pt-XS">
          {pageCategory && (
            <ClickTracker clickData={getLearnMorePayload(pageCategory)}>
              <Anchor
                fontSizeVariation="medium"
                anchorVariation="primary"
                text={getLabelValue(globalLabels, 'lbl_drawer_learn_more', 'accountDrawer')}
                underline
                href="#"
                onClick={e => handleLearnMoreClick(e)}
              />
            </ClickTracker>
          )}
          {!pageCategory && (
            <Anchor
              fontSizeVariation="medium"
              anchorVariation="primary"
              text={getLabelValue(globalLabels, 'lbl_drawer_learn_more', 'accountDrawer')}
              underline
              href="#"
              onClick={e => handleLearnMoreClick(e)}
            />
          )}
        </div>
      </div>
    </div>
  );
};

ExtraPointsTeaser.propTypes = {
  className: PropTypes.string,
  labels: PropTypes.shape({}),
  plccUser: PropTypes.bool,
  globalLabels: PropTypes.shape({}),
  closedOverlay: PropTypes.func.isRequired,
  pageCategory: PropTypes.string,
};

ExtraPointsTeaser.defaultProps = {
  className: '',
  labels: {
    ACC_DRAWER_EARN_EXTRA: 'Want to Earn Extra Points?',
    ACC_DRAWER_GET_CLOSER: 'Get even closer to your next reward!',
    ACC_DRAWER_LEARN_MORE: 'Learn More',
  },
  plccUser: false,
  globalLabels: {},
  pageCategory: '',
};

export default withStyles(ExtraPointsTeaser, styles);
export { ExtraPointsTeaser as ExtraPointsTeaserVanilla };
