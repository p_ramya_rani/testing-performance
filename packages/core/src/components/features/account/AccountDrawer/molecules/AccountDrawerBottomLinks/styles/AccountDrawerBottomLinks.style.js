// 9fbef606107a605d69c0edbcd8029e5d
import { css } from 'styled-components';

const Styles = css`
  background-color: ${(props) => props.theme.colorPalette.gray[300]};
  .bottomLine {
    border-bottom: 1px solid ${(props) => props.theme.colorPalette.gray[500]};
  }
  .bottomLinks {
    display: inline-block;
    width: 100%;
    height: 100%;
  }
`;

export default Styles;
