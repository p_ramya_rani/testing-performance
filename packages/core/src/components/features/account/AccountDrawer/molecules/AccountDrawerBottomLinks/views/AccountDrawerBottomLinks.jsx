// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import internalEndpoints from '@tcp/core/src/components/features/account/common/internalEndpoints';
import { getLabelValue, isCanada } from '@tcp/core/src/utils/utils';
import { routerPush } from '@tcp/core/src/utils';
import LogOutPageContainer from '../../../../Logout/container/LogOut.container';
import withStyles from '../../../../../../common/hoc/withStyles';
import styles from '../styles/AccountDrawerBottomLinks.style';
import Anchor from '../../../../../../common/atoms/Anchor';

const AccountDrawerBottomLinks = (props) => {
  const { className, labels } = props;

  /**
   * This function will handle click to go to respective links
   * @param {event, link, path} -
   */
  const onLinkRedirect = ({ e, link, path }) => {
    e.preventDefault();
    const { closedOverlay } = props;
    routerPush(link, path);
    closedOverlay();
  };

  return (
    <div className={className}>
      <div className="linksWrapper elem-pl-MED elem-pr-MED">
        <div className="bottomLine">
          <Anchor
            fontSizeVariation="large"
            fontFamily="secondary"
            onClick={(e) =>
              onLinkRedirect({
                e,
                link: internalEndpoints.myOrderPage.link,
                path: internalEndpoints.myOrderPage.path,
              })
            }
            anchorVariation="primary"
            text={getLabelValue(labels, 'CREATE_ACC_ORDERS')}
            className="bottomLinks elem-pt-MED elem-pb-MED "
            dataLocator="orders"
          />
        </div>
        {!isCanada() && (
          <div className="bottomLine">
            <Anchor
              fontSizeVariation="large"
              fontFamily="secondary"
              href="#"
              onClick={(e) =>
                onLinkRedirect({
                  e,
                  link: internalEndpoints.rewardsCreditCard.link,
                  path: internalEndpoints.rewardsCreditCard.path,
                })
              }
              anchorVariation="primary"
              className="bottomLinks elem-pt-MED elem-pb-MED"
              text={getLabelValue(labels, 'CREATE_ACC_MY_PLACE_REWARDS_CC')}
              dataLocator="rewards"
            />
          </div>
        )}
        <div className="bottomLine">
          <Anchor
            fontSizeVariation="large"
            fontFamily="secondary"
            href="#"
            onClick={(e) =>
              onLinkRedirect({
                e,
                link: internalEndpoints.myWalletPage.link,
                path: internalEndpoints.myWalletPage.path,
              })
            }
            anchorVariation="primary"
            text={getLabelValue(labels, 'CREATE_ACC_WALLET')}
            className="bottomLinks elem-pt-MED elem-pb-MED "
            dataLocator="wallet"
          />
        </div>
        <div className="bottomLine">
          <Anchor
            fontSizeVariation="large"
            fontFamily="secondary"
            href="#"
            onClick={(e) =>
              onLinkRedirect({
                e,
                link: internalEndpoints.favorites.link,
                path: internalEndpoints.favorites.path,
              })
            }
            anchorVariation="primary"
            className="bottomLinks elem-pt-MED elem-pb-MED "
            text={getLabelValue(labels, 'CREATE_ACC_MY_FAV')}
            dataLocator="favorites"
          />
        </div>
        <div className="bottomLine">
          <LogOutPageContainer labels={labels} accountDrawer />
        </div>
      </div>
    </div>
  );
};

AccountDrawerBottomLinks.propTypes = {
  className: PropTypes.string,
  labels: PropTypes.shape({}),
  closedOverlay: PropTypes.func.isRequired,
};

AccountDrawerBottomLinks.defaultProps = {
  className: '',
  labels: {
    CREATE_ACC_MY_FAV: 'My Favorites',
    CREATE_ACC_MY_PLACE_REWARDS_CC: 'My Place Rewards Credit Card',
    CREATE_ACC_WALLET: 'Wallet',
    CREATE_ACC_ORDERS: 'Orders',
    CREATE_ACC_SIGN_OUT: 'Sign Out',
  },
};

export default withStyles(AccountDrawerBottomLinks, styles);
export { AccountDrawerBottomLinks as AccountDrawerBottomLinksVanilla };
