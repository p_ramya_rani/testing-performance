// 9fbef606107a605d69c0edbcd8029e5d 
import { mapStateToProps } from '../BirthdaySavingsPage.container';

describe('BirthdaySavingsPage container', () => {
  it('mapStateToProps should return label props', () => {
    const stateProps = mapStateToProps({
      Labels: {
        account: {
          profile: {
            lbl_profile_name: 'test',
          },
        },
      },
    });
    expect(stateProps.labels).toBeDefined();
  });
});
