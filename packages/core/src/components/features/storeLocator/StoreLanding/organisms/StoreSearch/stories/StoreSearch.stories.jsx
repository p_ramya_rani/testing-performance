// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import { ThemeProvider } from 'styled-components';
import Theme from '@tcp/core/styles/themes';
import { getImageFilePath } from '@tcp/core/src/utils';
import { storiesOf } from '@storybook/react';
import StoreSearchView from '../views/StoreSearch';
import mockLabels from '../../../container/__mocks__/storeSearchData';

storiesOf('StoreSearch', module)
  .add('with styles and redux-form', () => (
    <StoreSearchView
      searchIcon={`${getImageFilePath()}/search-icon.svg`}
      markerIcon={`${getImageFilePath()}/marker-icon.svg`}
      selectStoreType={(e) => console.log(e)}
      labels={mockLabels}
    />
  ))
  .add('with store brand selected', () => (
    <StoreSearchView
      searchIcon={`${getImageFilePath()}/search-icon.svg`}
      markerIcon={`${getImageFilePath()}/marker-icon.svg`}
      gymSelected
      selectStoreType={(e) => console.log(e)}
      labels={mockLabels}
    />
  ))
  .add('with brand selected', () => {
    console.log(Theme());
    const theme = { ...Theme(), isGymboree: true };
    return (
      <ThemeProvider theme={theme}>
        <StoreSearchView
          searchIcon={`${getImageFilePath()}/search-icon.svg`}
          markerIcon={`${getImageFilePath()}/marker-icon.svg`}
          selectStoreType={(e) => console.log(e)}
          labels={mockLabels}
        />
      </ThemeProvider>
    );
  });
