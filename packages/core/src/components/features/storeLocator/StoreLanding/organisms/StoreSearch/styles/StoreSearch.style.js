// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

export default css`
  padding: 24px 12px;
  background-color: ${props =>
    props.theme.isGymboree
      ? props.theme.colorPalette.orange[50]
      : props.theme.colors.PRIMARY.COLOR1};
  font-size: ${props => props.theme.typography.fontSizes.fs12};

  .searchFormBody {
    font-family: ${props => props.theme.fonts.secondaryFontFamily};

    @media ${props => props.theme.mediaQuery.mediumOnly} {
      display: flex;
      flex-direction: column;
      justify-content: center;
    }
  }
  .searchBar {
    position: relative;
  }

  .store-locator-field {
    .TextBox__label {
      top: 21px;
    }
    input:not([value='']) ~ .TextBox__label {
      position: absolute;
      top: ${props => props.theme.spacing.ELEM_SPACING.XXS};
    }
    .TextBox__input {
      background-color: transparent;
      width: 90%;
      padding-right: 10%;

      @media ${props => props.theme.mediaQuery.medium} {
        padding-bottom: 2px;
        padding-top: 21px;
      }
    }
  }

  .location-image {
    vertical-align: bottom;
  }

  .CheckBox__text {
    padding-top: 6px;
  }

  .storeLocatorHeading {
    font-size: ${props => props.theme.typography.fontSizes.fs16};
    color: ${props => props.theme.colors.PRIMARY.DARK};
    margin: 0 0 12px;
  }

  .button-search-store {
    position: absolute;
    right: -4px;
    top: 3px;
  }

  .currentLocationWrapper__cta {
    padding: 0;
    min-height: auto;
    margin-bottom: 15px;
    @media ${props => props.theme.mediaQuery.large} {
      margin-bottom: 16px;
    }
  }

  .currentLocation {
    font-family: ${props => props.theme.fonts.secondaryFontFamily};
    margin-left: ${props => props.theme.spacing.ELEM_SPACING.XS};
    letter-spacing: normal;
    text-transform: none;
    font-size: ${props => props.theme.fonts.fontSize.body.bodytext.copy2}px;
  }

  .storeOptionList {
    margin-top: ${props => props.theme.spacing.ELEM_SPACING.SM};

    @media ${props => props.theme.mediaQuery.mediumOnly} {
      margin-top: 0;
      margin-bottom: 34px;
    }
  }

  .storeOptions {
    display: inline-block;
    min-width: 150px;

    &:nth-of-type(odd) {
      float: left;
    }
  }

  .storeLinks {
    display: inline-block;

    a {
      text-decoration: underline;
    }
  }

  .storeLinksList {
    margin-top: ${props => props.theme.spacing.ELEM_SPACING.MED};

    @media ${props => props.theme.mediaQuery.mediumOnly} {
      margin-top: auto;
    }

    @media ${props => props.theme.mediaQuery.large} {
      margin-top: ${props => props.theme.spacing.ELEM_SPACING.XS};
    }

    .storeLinks {
      padding: 0 5px;
      border-right: 1px solid;
    }

    .storeLinks:first-child {
      padding-left: 0;
    }

    .storeLinks:last-child {
      border-right: none;
    }
  }

  @media ${props => props.theme.mediaQuery.large} {
    .mapLink {
      display: none;

      & + .storeLinks {
        padding-left: 0;
      }
    }
  }

  .storelocator_wrapper {
    align-items: center;
  }
`;
