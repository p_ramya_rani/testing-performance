// 9fbef606107a605d69c0edbcd8029e5d
import React, { Component } from 'react';
import { FlatList } from 'react-native';
import { PropTypes } from 'prop-types';
import { readCookie } from '@tcp/core/src/utils/cookie.util';
import { isGymboree } from '@tcp/core/src/utils/index.native';
import { getLabelValue } from '@tcp/core/src/utils';
import { Anchor, BodyCopy, Image } from '@tcp/core/src/components/common/atoms';
import TextBox from '@tcp/core/src/components/common/atoms/TextBox';
import InputCheckbox from '@tcp/core/src/components/common/atoms/InputCheckbox';
import { trackForterCurrentLocation } from '@tcp/core/src/utils/forter.util';
import { reduxForm, Field } from 'redux-form';
import { getAddressLocationInfo } from '@tcp/core/src/utils/addressLocation';
import { getAddressLocationInfoGmaps } from '@tcp/core/src/utils/addressLocationGmaps.native';
import {
  StyledContainer,
  StyledStoreLocator,
  StyledAutoComplete,
  StyledCheckbox,
  StyledLinks,
  StyleStoreOptionList,
  StyledFindStoreTitle,
  StyledCurrentLocation,
  StyledSearch,
  StyledCheckBoxBodyCopy,
} from '../styles/StoreSearch.style.native';
import createValidateMethod from '../../../../../../../utils/formValidation/createValidateMethod';
import getStandardConfig from '../../../../../../../utils/formValidation/validatorStandardConfig';
import constants from '../../../container/StoreLanding.constants';
// import { getAddressLocationInfo } from '@tcp/core/src/utils/addressLocation.native';

const MarkerIcon = require('../../../../../../../../../mobileapp/src/assets/images/icon-marker.png');

const SearchIcon = require('../../../../../../../../../mobileapp/src/assets/images/icon-mag-glass.png');

const { INITIAL_STORE_LIMIT } = constants;

class StoreSearch extends Component {
  state = {
    errorNotFound: null,
    gymSelected: isGymboree(),
    outletSelected: false,
    inputValue: '',
  };

  locationRef = null;

  componentDidMount() {
    const { loadStoresByCoordinates } = this.props;
    this.getLocationStores(loadStoresByCoordinates);
  }

  /**
   * @getLocationStores : To fetch the store on the based on current latitude & longitude .
   */

  getLocationStores = (loadStoresByCoordinates) => {
    const { setLocationFlag, isLocationEnabled } = this.props;
    const latLong = readCookie('tcpGeoLoc');
    if (latLong) {
      latLong.then((res) => {
        if (res) {
          const [lat, long] = res.split('|');
          if (!isLocationEnabled && setLocationFlag) {
            setLocationFlag(true);
            trackForterCurrentLocation(Number(long), Number(lat));
          }
          loadStoresByCoordinates(
            Promise.resolve({
              lat: Number(lat),
              lng: Number(long),
            }),
            INITIAL_STORE_LIMIT,
            'STORES'
          );
        }
      });
    }
  };

  renderStoreTypes = ({ name, dataLocator, storeLabel, checked } = {}) => {
    return (
      <StyledCheckbox>
        <Field
          name={name}
          component={InputCheckbox}
          dataLocator={dataLocator}
          enableSuccessCheck={false}
          isChecked={checked}
          checkBoxLabel={false}
          onChange={(...args) => this.onSelectStore(args)}
        >
          {storeLabel}
        </Field>
        <StyledCheckBoxBodyCopy>
          <BodyCopy
            fontSize="fs12"
            fontFamily="secondary"
            fontWeight="regular"
            color="#1a1a1a"
            text={storeLabel}
          />
        </StyledCheckBoxBodyCopy>
      </StyledCheckbox>
    );
  };

  onSelectStore = (selectparams) => {
    const [isSelected, , , name] = selectparams;
    const { selectStoreType } = this.props;

    if (name === 'gymboreeStoreOption') {
      this.setState(
        {
          gymSelected: isSelected,
        },
        () => {
          const { outletSelected, gymSelected } = this.state;
          selectStoreType({ outletSelected, gymSelected });
        }
      );
    }

    if (name === 'outletOption') {
      this.setState(
        {
          outletSelected: isSelected,
        },
        () => {
          const { outletSelected, gymSelected } = this.state;
          selectStoreType({ outletSelected, gymSelected });
        }
      );
    }

    return true;
  };

  onSearch = () => {
    const { inputValue } = this.state;
    const { loadStoresByCoordinates, mapboxStoreSearchTypesParam, mapboxSwitch } = this.props;
    let res = {};
    try {
      if (mapboxSwitch) {
        res = getAddressLocationInfo(inputValue, mapboxStoreSearchTypesParam);
      } else {
        res = getAddressLocationInfoGmaps(inputValue);
      }
    } catch (err) {
      this.setState({
        errorNotFound: true,
      });
    }
    loadStoresByCoordinates(res, INITIAL_STORE_LIMIT, 'STORES');
    return false;
  };

  render() {
    const { labels, error, toggleMap, mapView, loadStoresByCoordinates } = this.props;
    const { errorNotFound, gymSelected, outletSelected, inputValue } = this.state;
    const errorMessage = errorNotFound
      ? getLabelValue(labels, 'lbl_storelanding_errorLabel')
      : error;
    const viewMapListLabel = mapView
      ? getLabelValue(labels, 'lbl_storelanding_viewList')
      : getLabelValue(labels, 'lbl_storelanding_viewMap');

    const storeOptionsConfig = [
      {
        name: 'gymboreeStoreOption',
        dataLocator: 'gymboree-store-option',
        storeLabel: getLabelValue(labels, 'lbl_storelanding_gymboreeStores'),
        checked: gymSelected,
      },
      {
        name: 'outletOption',
        dataLocator: 'only-outlet-option',
        storeLabel: getLabelValue(labels, 'lbl_storelanding_outletStores'),
        checked: outletSelected,
      },
    ];

    return (
      <StyledContainer>
        <StyledFindStoreTitle>
          <BodyCopy
            mobilefontFamily="primary"
            fontWeight="extrabold"
            fontSize="fs16"
            color="#1a1a1a"
            text={getLabelValue(labels, 'lbl_storelanding_findStoreHeading')}
          />
        </StyledFindStoreTitle>
        <Anchor onPress={() => this.getLocationStores(loadStoresByCoordinates)}>
          <StyledStoreLocator>
            <Image
              source={MarkerIcon}
              alt={getLabelValue(labels, 'markerIconText')}
              height="16px"
              width="16px"
            />
            <StyledCurrentLocation>
              <BodyCopy
                mobilefontFamily="primary"
                fontWeight="regular"
                fontSize="fs12"
                color="#1a1a1a"
                text={getLabelValue(labels, 'lbl_storelanding_currentLocation')}
              />
            </StyledCurrentLocation>
          </StyledStoreLocator>
        </Anchor>
        <StyledAutoComplete>
          <>
            <StyledSearch>
              <Anchor onPress={() => this.onSearch()}>
                <Image
                  source={SearchIcon}
                  alt={getLabelValue(labels, 'searchIconText')}
                  height="25px"
                  width="25px"
                />
              </Anchor>
            </StyledSearch>
            <Field
              label={getLabelValue(labels, 'lbl_storelanding_storeSearchPlaceholder')}
              component={TextBox}
              type="text"
              dataLocator="storeAddressLocator"
              inputRef={(instance) => {
                this.locationRef = instance;
              }}
              enableSuccessCheck={false}
              forwardRef
              clearButtonMode="never"
              errorMessage={errorMessage}
              name="storeAddressLocator"
              value={inputValue}
              onChangeText={(inputVal) => {
                this.setState({ inputValue: inputVal });
              }}
            />
          </>
        </StyledAutoComplete>
        <StyleStoreOptionList>
          <FlatList
            data={storeOptionsConfig}
            renderItem={({ item }) => this.renderStoreTypes(item)}
            horizontal
          />
        </StyleStoreOptionList>
        <StyledLinks>
          <Anchor
            fontWeight="regular"
            anchorVariation="primary"
            text={viewMapListLabel}
            underline
            onPress={toggleMap}
          />
        </StyledLinks>
      </StyledContainer>
    );
  }
}

StoreSearch.propTypes = {
  error: PropTypes.bool.isRequired,
  labels: PropTypes.objectOf(PropTypes.string),
  loadStoresByCoordinates: PropTypes.func.isRequired,
  toggleMap: PropTypes.func.isRequired,
  mapView: PropTypes.bool,
  selectStoreType: PropTypes.func.isRequired,
  isLocationEnabled: PropTypes.bool,
  setLocationFlag: PropTypes.func,
  mapboxStoreSearchTypesParam: PropTypes.string,
  mapboxSwitch: PropTypes.bool,
};

StoreSearch.defaultProps = {
  labels: {},
  mapView: false,
  setLocationFlag: () => null,
  isLocationEnabled: false,
  mapboxStoreSearchTypesParam: '',
  mapboxSwitch: true,
};

const validateMethod = createValidateMethod(getStandardConfig(['storeAddressLocator']));

export default reduxForm({
  form: 'StoreSearch',
  enableReinitialize: true,
  ...validateMethod,
})(StoreSearch);

export { StoreSearch as StoreSearchVanilla };
