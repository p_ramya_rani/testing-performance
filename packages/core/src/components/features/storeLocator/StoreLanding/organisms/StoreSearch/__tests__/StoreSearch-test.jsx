// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow } from 'enzyme';
import { StoreViewVanilla } from '../views/StoreSearch';

describe('StoreSearch component', () => {
  let props = {
    className: 'test-class',
    handleSubmit: jest.fn(),
    dispatch: jest.fn(),
    isGeoCodeAPIEnabled: true,
  };

  it('StoreSearch component renders correctly', () => {
    const component = shallow(<StoreViewVanilla {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('onSubmit - calls - with submitting', () => {
    props = {
      ...props,
      submitting: true,
    };
    const component = shallow(<StoreViewVanilla {...props} />);
    const formData = {
      storeAddressLocator: null,
    };
    const onSubmitFn = component.instance().onSubmit(formData);
    expect(onSubmitFn).toBeFalsy();
  });

  it('onSelectStore should return', () => {
    const eventGym = {
      target: {
        name: 'gymboreeStoreOption',
      },
    };
    const eventOutlet = {
      target: {
        name: 'outletOption',
      },
    };
    const test = {
      target: '',
    };
    const selectStoreType = jest.fn();
    const component = shallow(<StoreViewVanilla {...props} selectStoreType={selectStoreType} />);
    const returnValueGym = component.instance().onSelectStore(eventGym);
    const returnValueOutlet = component.instance().onSelectStore(eventOutlet);
    component.instance().onSelectStore(test);
    expect(returnValueGym).toBeTruthy();
    expect(returnValueOutlet).toBeTruthy();
  });
});
