// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import { withRouter } from 'next/router';
import PropTypes from 'prop-types';
import { Row, Col, Anchor } from '@tcp/core/src/components/common/atoms';
import StoreStaticMap from '@tcp/core/src/components/common/atoms/StoreStaticMap';
import StoreStaticMapGmaps from '@tcp/core/src/components/common/atoms/StoreStaticMap/views/StoreStaticMapGmaps';
import { Grid } from '@tcp/core/src/components/common/molecules';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import StoreAddressTile from '@tcp/core/src/components/common/molecules/StoreAddressTile';
import Notification from '@tcp/core/src/components/common/molecules/Notification';
import SeoCopy from '@tcp/core/src/components/features/browse/ProductListing/molecules/SeoCopy/views';
import {
  isCanada,
  getViewportInfo,
  getAPIConfig,
  routeToStoreDetails,
  isGymboree,
  getLabelValue,
} from '@tcp/core/src/utils';
import { CALL_TO_ACTION_VISIBLE, FULLY_VISIBLE } from '@tcp/core/src/constants/rum.constants';
import RenderPerf from '@tcp/web/src/components/common/molecules/RenderPerf';
import StoreLocatorSearch from '../../organisms/StoreSearch';
import StoreDetailContainerClass from '../../../StoreDetail/container/StoreDetail.container';

import styles from '../styles/StoreLanding.style';
import { filterStores } from '../utils/userFavStore';

export class StoreLanding extends PureComponent {
  state = {
    mapView: false,
    isOutlet: false,
    isGym: isGymboree(),
    centeredStoreId: '',
    showSubmitError: false,
    mapImageHasLoaded: false,
  };

  openStoreDetails = (event, store) => {
    event.preventDefault();
    const { routerHandler } = routeToStoreDetails(store);
    routerHandler();
  };

  focusOnMap = (event, id) => {
    event.preventDefault();
    this.setState({
      centeredStoreId: id,
    });
  };

  handleMapImageLoaded = () => {
    this.setState({
      mapImageHasLoaded: true,
    });
  };

  renderMapView = (suggestedStoreList) => {
    const { setFavoriteStore, favoriteStore, labels, searchDone, mapboxSwitch, ...others } =
      this.props;
    const { centeredStoreId, showSubmitError } = this.state;

    const showNotification =
      showSubmitError ||
      (searchDone &&
        !(suggestedStoreList && (suggestedStoreList.length || suggestedStoreList.size)));

    let StoreStaticMapCommon = StoreStaticMap;
    let apiKey = getAPIConfig().mapBoxApiKey;
    if (!mapboxSwitch) {
      StoreStaticMapCommon = StoreStaticMapGmaps;
      apiKey = getAPIConfig().googleApiKey;
    }

    const storeList = showNotification ? (
      <Notification
        status={showSubmitError ? 'error' : 'info'}
        message={getLabelValue(
          labels,
          showSubmitError ? 'lbl_storelanding_errorLabel' : 'lbl_storelanding_noStoresFound'
        )}
        className="storeview__error"
      />
    ) : (
      suggestedStoreList.map((item, index) => (
        <Col
          colSize={{ large: 12, medium: 8, small: 6 }}
          ignoreGutter={{ small: true }}
          className="store_item_container"
        >
          <StoreAddressTile
            {...this.props}
            className="storelocator__tile"
            store={item}
            variation="listing"
            storeIndex={index + 1}
            setFavoriteStore={setFavoriteStore}
            isFavorite={favoriteStore && favoriteStore.basicInfo.id === item.basicInfo.id}
            key={item.basicInfo.id}
            openStoreDetails={this.openStoreDetails}
            titleClickCb={this.focusOnMap}
            selectedStoreId={centeredStoreId === item.basicInfo.id}
          />
        </Col>
      ))
    );

    const storeMap = (
      <Col colSize={{ large: 12, medium: 8, small: 6 }} ignoreGutter={{ small: true }}>
        <StoreStaticMapCommon
          storesList={suggestedStoreList}
          isCanada={isCanada()}
          isMobile={getViewportInfo().isMobile}
          apiKey={apiKey}
          labels={labels}
          centeredStoreId={centeredStoreId}
          onImageLoad={this.handleMapImageLoaded}
          {...others}
        />
      </Col>
    );

    return (
      <>
        <Col
          colSize={{ large: 6, medium: 4, small: 6 }}
          ignoreGutter={{ small: true }}
          className="store__map"
        >
          <Row fullBleed>{storeMap}</Row>
        </Col>
        <Col
          colSize={{ large: 12, medium: 4, small: 6 }}
          ignoreGutter={{ small: true }}
          className="storeList__map"
          ignoreNthRule
        >
          <Row fullBleed>{storeList}</Row>
        </Col>
      </>
    );
  };

  renderStoreList = (suggestedStoreList) => {
    const { centeredStoreId, showSubmitError } = this.state;
    const {
      setFavoriteStore,
      favoriteStore,
      openStoreDirections,
      labels,
      searchDone,
      geoLocationEnabled,
    } = this.props;
    if (
      showSubmitError ||
      (searchDone &&
        !(suggestedStoreList && (suggestedStoreList.length || suggestedStoreList.size)))
    ) {
      return (
        <Notification
          status={showSubmitError ? 'error' : 'info'}
          message={getLabelValue(
            labels,
            showSubmitError ? 'lbl_storelanding_errorLabel' : 'lbl_storelanding_noStoresFound'
          )}
          className="storeview__error"
        />
      );
    }
    return suggestedStoreList.map((item, index) => (
      <Col
        colSize={{ large: 12, medium: 4, small: 6 }}
        ignoreGutter={{ small: true }}
        className="store__list store_item_container"
        key={item.basicInfo.id}
      >
        <StoreAddressTile
          {...this.props}
          className="storelocator__tile"
          store={item}
          variation="listing"
          setFavoriteStore={setFavoriteStore}
          isFavorite={favoriteStore && favoriteStore.basicInfo.id === item.basicInfo.id}
          openStoreDirections={openStoreDirections}
          openStoreDetails={this.openStoreDetails}
          storeIndex={!!getViewportInfo().isDesktop && index + 1}
          titleClickCb={this.focusOnMap}
          selectedStoreId={centeredStoreId === item.basicInfo.id}
          geoLocationDisabled={!geoLocationEnabled}
        />
      </Col>
    ));
  };

  renderFavoriteStore = () => {
    const { favoriteStore, labels, geoLocationEnabled, userStoreId } = this.props;
    return (
      <>
        {favoriteStore && (
          <Row className="favoriteStore__container">
            <Col
              colSize={{ large: 12, medium: 8, small: 6 }}
              ignoreGutter={{ small: true }}
              className="store_item_container"
            >
              <p className="favoriteStore__heading">
                {getLabelValue(labels, 'lbl_storelanding_favStoreHeading')}
              </p>
              <StoreAddressTile
                {...this.props}
                className="storelocator__tiles"
                store={favoriteStore}
                variation="listing-header"
                isFavorite
                openStoreDetails={this.openStoreDetails}
                geoLocationDisabled={!geoLocationEnabled}
                userStoreId={userStoreId}
              />
            </Col>
          </Row>
        )}
      </>
    );
  };

  toggleMap = (event) => {
    event.preventDefault();
    this.setState((prevState) => ({
      mapView: !prevState.mapView,
    }));
  };

  selectStoreType = ({ gymSelected, outletSelected }) => {
    this.setState({
      isGym: gymSelected,
      isOutlet: outletSelected,
    });
  };

  showSubmitError = (value) => {
    const { showSubmitError } = this.state;
    if (showSubmitError !== value) this.setState({ showSubmitError: value });
  };

  render() {
    const {
      className,
      suggestedStoreList,
      labels,
      loadStoresByCoordinates,
      searchIcon,
      markerIcon,
      getLocationStores,
      setClickAnalyticsData,
      trackClick,
      trackPageLoad,
      isGeoCodeAPIEnabled,
      seoData,
      mapboxStoreSearchTypesParam,
      mapboxSwitch,
      ...others
    } = this.props;
    const { mapView, isGym, isOutlet, centeredStoreId, mapImageHasLoaded } = this.state;

    const modifiedStoreList = filterStores(suggestedStoreList, isOutlet, isGym);

    let StoreStaticMapCommon = StoreStaticMap;
    let apiKey = getAPIConfig().mapBoxApiKey;
    if (!mapboxSwitch) {
      StoreStaticMapCommon = StoreStaticMapGmaps;
      apiKey = getAPIConfig().googleApiKey;
    }

    return (
      <>
        <Anchor
          fontSizeVariation="xlarge"
          anchorVariation="secondary"
          handleLinkClick={StoreDetailContainerClass.routesBack}
          noLink
          className={`${className}__backlink`}
          title={getLabelValue(labels, 'lbl_storedetails_backLink')}
        >
          <span className="left-arrow" />
          {getLabelValue(labels, 'lbl_storedetails_backLink')}
        </Anchor>
        <Grid className={className}>
          <Row fullBleed>
            <Col colSize={{ large: 6, medium: 8, small: 6 }} ignoreGutter={{ small: true }}>
              {this.renderFavoriteStore()}
              <Row fullBleed>
                <Col colSize={{ large: 12, medium: 8, small: 6 }} ignoreGutter={{ small: true }}>
                  <StoreLocatorSearch
                    labels={labels}
                    loadStoresByCoordinates={loadStoresByCoordinates}
                    toggleMap={this.toggleMap}
                    mapView={mapView}
                    selectStoreType={this.selectStoreType}
                    searchIcon={searchIcon}
                    markerIcon={markerIcon}
                    getLocationStores={getLocationStores}
                    showSubmitError={this.showSubmitError}
                    setClickAnalyticsData={setClickAnalyticsData}
                    trackClick={trackClick}
                    trackPageLoad={trackPageLoad}
                    selectedCountry={isCanada() ? 'CA' : 'USA'}
                    isGeoCodeAPIEnabled={isGeoCodeAPIEnabled}
                    mapboxStoreSearchTypesParam={mapboxStoreSearchTypesParam}
                    mapboxSwitch={mapboxSwitch}
                  />
                  {/* UX Timer */}
                  <RenderPerf.Measure name={CALL_TO_ACTION_VISIBLE} />
                </Col>
              </Row>
              <Row
                className={`storeView__List${mapView ? ' storeView__ListAndMap' : ''}`}
                fullBleed
                aria-live="polite"
                noLastMargin
              >
                {mapView
                  ? this.renderMapView(modifiedStoreList)
                  : this.renderStoreList(modifiedStoreList)}
              </Row>
            </Col>
            <Col
              colSize={{ large: 6, medium: 8, small: 6 }}
              ignoreGutter={{ small: true }}
              className="mapView__desktop"
            >
              <StoreStaticMapCommon
                storesList={modifiedStoreList}
                isCanada={isCanada()}
                apiKey={apiKey}
                centeredStoreId={centeredStoreId}
                onImageLoad={this.handleMapImageLoaded}
                {...others}
              />
            </Col>
          </Row>
          {/* UX Timer */}
          {mapImageHasLoaded && <RenderPerf.Measure name={FULLY_VISIBLE} />}
        </Grid>
        <SeoCopy {...seoData} />
      </>
    );
  }
}

StoreLanding.propTypes = {
  fetchStoresByCoordinates: PropTypes.func.isRequired,
  suggestedStoreList: PropTypes.arrayOf({}),
  setFavoriteStore: PropTypes.func.isRequired,
  favoriteStore: PropTypes.shape(PropTypes.string),
  className: PropTypes.string.isRequired,
  labels: PropTypes.shape(PropTypes.string).isRequired,
  openStoreDirections: PropTypes.func.isRequired,
  loadStoresByCoordinates: PropTypes.func.isRequired,
  markerIcon: PropTypes.string.isRequired,
  searchIcon: PropTypes.string.isRequired,
  getLocationStores: PropTypes.func.isRequired,
  searchDone: PropTypes.bool,
  geoLocationEnabled: PropTypes.bool,
  setClickAnalyticsData: PropTypes.func.isRequired,
  trackClick: PropTypes.func.isRequired,
  trackPageLoad: PropTypes.func.isRequired,
  isGeoCodeAPIEnabled: PropTypes.bool.isRequired,
  userStoreId: PropTypes.string,
  seoData: PropTypes.shape({}),
  mapboxStoreSearchTypesParam: PropTypes.string,
  mapboxSwitch: PropTypes.bool,
  router: PropTypes.shape({}).isRequired,
};

StoreLanding.defaultProps = {
  suggestedStoreList: [],
  favoriteStore: null,
  searchDone: false,
  geoLocationEnabled: false,
  userStoreId: '',
  seoData: {},
  mapboxStoreSearchTypesParam: '',
  mapboxSwitch: true,
};

const StoreLandingStyled = withStyles(StoreLanding, styles);

export default withRouter(StoreLandingStyled);

export { StoreLanding as StoreLandingVanilla };
