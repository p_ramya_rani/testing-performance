// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components';

export const StyledFavStoreHeading = styled.View`
  padding: ${props => props.theme.spacing.ELEM_SPACING.MED}
    ${props => props.theme.spacing.ELEM_SPACING.MED} 0;
`;

export const StyleStoreLandingContainer = styled.View`
  width: 100%;
  font-size: ${props => props.theme.typography.fontSizes.fs12};
`;
