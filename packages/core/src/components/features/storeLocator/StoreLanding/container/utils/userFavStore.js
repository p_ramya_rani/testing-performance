// 9fbef606107a605d69c0edbcd8029e5d 
import { setLocalStorage, getLocalStorage } from '../../../../../../utils/localStorageManagement';
import constants from '../StoreLanding.constants';

export const setFavStoreToLocalStorage = store => {
  const params = {
    key: constants.FAV_STORE_CACHE_KEY,
    value: JSON.stringify(store),
  };
  setLocalStorage(params);
};

export const getFavStoreFromLocalStorage = () => getLocalStorage(constants.FAV_STORE_CACHE_KEY);

export const filterStores = (storeList, isOutlet, isGym) => {
  let filteredStoreList = storeList;
  if (isOutlet && isGym) {
    filteredStoreList = storeList.filter(
      item => item.isGym && item.features.storeType === 'Outlet'
    );
  } else if (isGym) {
    filteredStoreList = storeList.filter(item => item.isGym);
  } else if (isOutlet) {
    filteredStoreList = storeList.filter(item => item.features.storeType === 'Outlet');
  }
  return filteredStoreList;
};

export default {
  getFavStoreFromLocalStorage,
  setFavStoreToLocalStorage,
};
