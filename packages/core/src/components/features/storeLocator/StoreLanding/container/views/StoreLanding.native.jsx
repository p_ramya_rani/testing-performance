// 9fbef606107a605d69c0edbcd8029e5d 
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { View, Text, ScrollView } from 'react-native';
import { isCanada, getAPIConfig, getLabelValue, isGymboree, mapHandler } from '@tcp/core/src/utils';
import StoreStaticMap from '@tcp/core/src/components/common/atoms/StoreStaticMap';
import StoreStaticMapGmaps from '@tcp/core/src/components/common/atoms/StoreStaticMap/views/StoreStaticMapGmaps';
import Notification from '@tcp/core/src/components/common/molecules/Notification';
import StoreAddressTile from '@tcp/core/src/components/common/molecules/StoreAddressTile';
import {
  trackForterNavigation,
  ForterNavigationType,
} from '@tcp/core/src/utils/forter.util.native';

import { withTheme } from 'styled-components/native';
import names from '../../../../../../constants/eventsName.constants';
import withKeyboardAvoidingView from '../../../../../common/hoc/withKeyboardAvoidingView.native';
import StoreLocatorSearch from '../../organisms/StoreSearch';
import {
  StyleStoreLandingContainer,
  StyledFavStoreHeading,
} from '../styles/StoreLanding.style.native';
import { filterStores } from '../utils/userFavStore';

export class StoreLanding extends PureComponent {
  state = {
    mapView: false,
    isOutlet: false,
    isGym: isGymboree(),
    centeredStoreId: '',
  };

  componentDidMount() {
    trackForterNavigation('StoreLocatorPage', ForterNavigationType.HELP, {}, true);
    this.triggerAnalyticsPageView();
  }

  triggerAnalyticsPageView = () => {
    const {
      trackPageLoad,
      navigation,
      isExternalCampaignFired,
      setExternalCampaignFired,
    } = this.props;
    const externalCampaignId = navigation.getParam('externalCampaignId');
    const omMID = navigation.getParam('omMID');
    const omRID = navigation.getParam('omRID');
    const analyticsData = {
      currentScreen: names.screenNames.store_details,
      pageData: {
        pageName: 'storelocator',
        pageType: 'companyinfo',
        pageSection: 'storelocator',
        pageSubSection: 'storelocator',
        pageNavigationText: 'non-browse',
      },
    };
    const customEvents = [];
    if (isExternalCampaignFired) {
      customEvents.push('event19');
      setExternalCampaignFired(false);
    }
    if (externalCampaignId) {
      customEvents.push('event18');
      setExternalCampaignFired(true);
    }
    trackPageLoad(analyticsData, {
      customEvents,
      ...(externalCampaignId && {
        externalCampaignId,
      }),
      ...(omMID && { omMID }),
      ...(omRID && { omRID }),
    });
  };

  toggleMap = event => {
    event.preventDefault();
    this.setState(prevState => ({
      mapView: !prevState.mapView,
    }));
  };

  focusOnMap = (event, id) => {
    event.preventDefault();
    this.setState({
      centeredStoreId: id,
    });
  };

  selectStoreType = ({ gymSelected, outletSelected }) => {
    this.setState({
      isGym: gymSelected,
      isOutlet: outletSelected,
    });
  };

  openStoreDetails = store => {
    const { fetchCurrentStore, navigation, labels } = this.props;
    fetchCurrentStore(store);
    navigation.navigate('StoreDetails', {
      title: getLabelValue(labels, 'lbl_storedetail_storedetailTxt'),
    });
  };

  renderList = modifiedStoreList => {
    const { labels, setFavoriteStore, favoriteStore, searchDone } = this.props;
    const { mapView, centeredStoreId } = this.state;
    if (searchDone && modifiedStoreList.length > 0) {
      return modifiedStoreList.map((item, index) => (
        <StoreAddressTile
          {...this.props}
          store={item}
          variation="listing"
          storeIndex={mapView && `${index + 1}`}
          setFavoriteStore={setFavoriteStore}
          isFavorite={favoriteStore && favoriteStore.basicInfo.id === item.basicInfo.id}
          key={item.basicInfo.id}
          openStoreDetails={this.openStoreDetails}
          openStoreDirections={() => this.openStoreDirections(item)}
          selectedStoreId={centeredStoreId === item.basicInfo.id}
          titleClickCb={this.focusOnMap}
        />
      ));
    }
    return searchDone && !modifiedStoreList.length ? (
      <Notification
        status="info"
        message={getLabelValue(labels, 'lbl_storelanding_noStoresFound')}
      />
    ) : null;
  };

  openStoreDirections = store => {
    mapHandler(store);
  };

  render() {
    const {
      suggestedStoreList,
      favoriteStore,
      theme,
      labels,
      loadStoresByCoordinates,
      getLocationStores,
      geoLocationEnabled,
      setClickAnalyticsData,
      trackClick,
      isGeoCodeAPIEnabled,
      isLocationEnabled,
      setLocationFlag,
      mapboxStoreSearchTypesParam,
      mapboxSwitch,
    } = this.props;
    const { mapView, isGym, isOutlet, centeredStoreId } = this.state;

    const modifiedStoreList = filterStores(suggestedStoreList, isOutlet, isGym);

    let StoreStaticMapCommon = StoreStaticMap;
    let apiKey = getAPIConfig().mapBoxApiKey;
    if (!mapboxSwitch) {
      StoreStaticMapCommon = StoreStaticMapGmaps;
      apiKey = getAPIConfig().googleApiKey;
    }

    return (
      <StyleStoreLandingContainer>
        <View>
          <ScrollView keyboardShouldPersistTaps="handled">
            {favoriteStore && (
              <View>
                <StyledFavStoreHeading>
                  <Text
                    // eslint-disable-next-line react-native/no-inline-styles
                    style={{
                      textTransform: 'uppercase',
                      color: theme.colors.TEXT.DARK,
                      fontSize: 16,
                      margin: 0,
                      fontWeight: 'bold',
                    }}
                  >
                    {getLabelValue(labels, 'lbl_storelanding_favStoreHeading')}
                  </Text>
                </StyledFavStoreHeading>
                <StoreAddressTile
                  {...this.props}
                  store={favoriteStore}
                  variation="listing-header"
                  isFavorite
                  geoLocationDisabled={!geoLocationEnabled}
                  openStoreDetails={this.openStoreDetails}
                  openStoreDirections={() => this.openStoreDirections(favoriteStore)}
                />
              </View>
            )}
            <StoreLocatorSearch
              labels={labels}
              loadStoresByCoordinates={loadStoresByCoordinates}
              setLocationFlag={setLocationFlag}
              isLocationEnabled={isLocationEnabled}
              toggleMap={this.toggleMap}
              mapView={mapView}
              selectStoreType={this.selectStoreType}
              getLocationStores={getLocationStores}
              selectedCountry={isCanada() ? 'CA' : 'USA'}
              setClickAnalyticsData={setClickAnalyticsData}
              trackClick={trackClick}
              isGeoCodeAPIEnabled={isGeoCodeAPIEnabled}
              mapboxStoreSearchTypesParam={mapboxStoreSearchTypesParam}
            />
            {mapView && !!modifiedStoreList.length && (
              <StoreStaticMapCommon
                storesList={modifiedStoreList}
                isCanada={isCanada()}
                apiKey={apiKey}
                centeredStoreId={centeredStoreId}
                {...this.props}
              />
            )}
            {this.renderList(modifiedStoreList)}
          </ScrollView>
        </View>
      </StyleStoreLandingContainer>
    );
  }
}

StoreLanding.propTypes = {
  fetchStoresByCoordinates: PropTypes.func.isRequired,
  suggestedStoreList: PropTypes.arrayOf({}),
  setFavoriteStore: PropTypes.func.isRequired,
  favoriteStore: PropTypes.shape(PropTypes.string),
  theme: PropTypes.shape(PropTypes.string).isRequired,
  labels: PropTypes.shape(PropTypes.string).isRequired,
  fetchCurrentStore: PropTypes.func.isRequired,
  navigation: PropTypes.shape({}).isRequired,
  loadStoresByCoordinates: PropTypes.func.isRequired,
  getLocationStores: PropTypes.func.isRequired,
  geoLocationEnabled: PropTypes.bool,
  searchDone: PropTypes.bool,
  setClickAnalyticsData: PropTypes.func.isRequired,
  trackClick: PropTypes.func.isRequired,
  isGeoCodeAPIEnabled: PropTypes.bool.isRequired,
  isLocationEnabled: PropTypes.func.isRequired,
  setLocationFlag: PropTypes.bool.isRequired,
  trackPageLoad: PropTypes.func,
  isExternalCampaignFired: PropTypes.bool.isRequired,
  setExternalCampaignFired: PropTypes.func,
  mapboxStoreSearchTypesParam: PropTypes.string,
  mapboxSwitch: PropTypes.bool,
};

StoreLanding.defaultProps = {
  suggestedStoreList: [],
  favoriteStore: null,
  geoLocationEnabled: false,
  searchDone: false,
  trackPageLoad: () => {},
  setExternalCampaignFired: () => {},
  mapboxStoreSearchTypesParam: '',
  mapboxSwitch: true,
};

export default withKeyboardAvoidingView(withTheme(StoreLanding));

export { StoreLanding as StoreLandingVanilla };
