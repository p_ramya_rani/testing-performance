// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { getIconPath } from '@tcp/core/src/utils';
import { setLocationEnabledValue } from '@tcp/core/src/reduxStore/actions';
import { trackForterCurrentLocation } from '@tcp/core/src/utils/forter.util';
import { getIsGenericUser } from '@tcp/core/src/components/features/account/User/container/User.selectors';
import PropTypes from 'prop-types';
import {
  getStoreSearchTypesParam,
  getMapboxSwitch,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import {
  getStoresByCoordinates,
  setFavoriteStoreActn,
  getFavoriteStoreActn,
  initActions,
} from './StoreLanding.actions';
import { setClickAnalyticsData, trackClick, trackPageView } from '../../../../../analytics/actions';
import { getCurrentStoreInfo } from '../../StoreDetail/container/StoreDetail.actions';
import StoreLandingView from './views/StoreLanding';
import {
  getCurrentCountry,
  getPageLabels,
  getAccessibilityLabels,
  getIsGeoCodeAPIEnabled,
  getIsLocationEnabled,
} from './StoreLanding.selectors';
import { getUserStoreId } from '../../../../common/organisms/PickupStoreModal/molecules/PickupStoreSelectionForm/container/PickupStoreSelectionForm.selectors';
import constants from './StoreLanding.constants';
import { setExternalCampaignState } from '../../../../common/molecules/Loader/container/Loader.actions';
import { getExternalCampaignFired } from '../../../browse/ProductListing/container/ProductListing.selectors';
import {
  getStoreLocatorMapboxApi,
  getStoreLocatorMapboxTileIDs,
} from '../../../../../reduxStore/selectors/session.selectors';

const { INITIAL_STORE_LIMIT, STORE_LOCATOR } = constants;

export class StoreLanding extends PureComponent {
  static openStoreDirections(store) {
    const {
      basicInfo: { address },
    } = store;
    const { addressLine1, city, state, zipCode } = address;
    return `https://maps.google.com/maps?daddr=${addressLine1},%20${city},%20${state},%20${zipCode}`;
  }

  /**
   * @function getLocationStores function to fetch the
   * stores list based on the user location coordinates
   */

  getLocationStores = () => {
    if (navigator.geolocation) {
      const { loadStoresByCoordinates } = this;
      navigator.geolocation.getCurrentPosition((pos) => {
        const { isLocationEnabled, setLocationFlag } = this.props;
        if (!isLocationEnabled && setLocationFlag) {
          setLocationFlag(true);
          trackForterCurrentLocation(pos.coords.longitude, pos.coords.latitude);
        }
        loadStoresByCoordinates(
          Promise.resolve({ lat: pos.coords.latitude, lng: pos.coords.longitude }),
          INITIAL_STORE_LIMIT,
          'STORES'
        );
      });
    }
  };

  /**
   * @function loadStoresByCoordinates function to fetch the stores based on coordinates.
   * @param {Promise} coordinatesPromise - Promise that resolves with the coordinates
   * @param {Number} maxItems - The maximum number of items to be fetched
   * @param {String} type - The type of stores to be fetched i.e favorite stores or normal stores
   */
  loadStoresByCoordinates = (coordinatesPromise, maxItems, type) => {
    const {
      fetchStoresByCoordinates,
      storeLocatorMapboxApi,
      storeLocatorMapboxTileIDs,
      mapboxSwitch,
    } = this.props;
    coordinatesPromise.then((coordinates) => {
      fetchStoresByCoordinates({
        coordinates,
        maxItems,
        type,
        storeLocatorMapboxApi,
        storeLocatorMapboxTileIDs,
        mapboxSwitch,
      });
    });
    return false;
  };

  fetchCurrentStoreDetails = (store) => {
    const { fetchCurrentStore } = this.props;
    const {
      basicInfo: { id },
    } = store;
    if (id) fetchCurrentStore({ storeID: id });
  };

  render() {
    const { navigation, searchDone, userStoreId } = this.props;
    const searchIcon = getIconPath('search-icon');
    const markerIcon = getIconPath('marker-icon');
    return (
      <StoreLandingView
        {...this.props}
        loadStoresByCoordinates={this.loadStoresByCoordinates}
        searchIcon={searchIcon}
        markerIcon={markerIcon}
        fetchCurrentStore={(store) => this.fetchCurrentStoreDetails(store)}
        openStoreDirections={(store) => this.constructor.openStoreDirections(store)}
        navigation={navigation}
        getLocationStores={this.getLocationStores}
        searchDone={searchDone}
        userStoreId={userStoreId}
        {...this.state}
      />
    );
  }
}

StoreLanding.pageInfo = {
  pageId: 'storelocator',
};

StoreLanding.getInitActions = () => initActions;

StoreLanding.getInitialProps = () => {
  return {
    pageData: {
      pageName: 'storelocator',
      pageType: 'companyinfo',
      pageSection: 'storelocator',
      pageSubSection: 'storelocator',
    },
  };
};

StoreLanding.propTypes = {
  fetchStoresByCoordinates: PropTypes.func.isRequired,
  getFavoriteStore: PropTypes.func.isRequired,
  favoriteStore: PropTypes.shape(PropTypes.string),
  fetchCurrentStore: PropTypes.func.isRequired,
  navigation: PropTypes.shape({}),
  searchDone: PropTypes.bool.isRequired,
  setClickAnalyticsData: PropTypes.func.isRequired,
  trackClick: PropTypes.func.isRequired,
  isGeoCodeAPIEnabled: PropTypes.bool.isRequired,
  userStoreId: PropTypes.string,
  isLocationEnabled: PropTypes.bool.isRequired,
  setLocationFlag: PropTypes.func.isRequired,
  mapboxStoreSearchTypesParam: PropTypes.string,
  mapboxSwitch: PropTypes.bool.isRequired,
  storeLocatorMapboxApi: PropTypes.bool.isRequired,
  storeLocatorMapboxTileIDs: PropTypes.string,
};

StoreLanding.defaultProps = {
  favoriteStore: null,
  navigation: {},
  userStoreId: '',
  mapboxStoreSearchTypesParam: '',
  storeLocatorMapboxTileIDs: '',
};

/* istanbul ignore next */
const mapDispatchToProps = (dispatch) => ({
  fetchStoresByCoordinates: (storeConfig) => dispatch(getStoresByCoordinates(storeConfig)),
  setFavoriteStore: (payload) => dispatch(setFavoriteStoreActn({ ...payload, key: STORE_LOCATOR })),
  getFavoriteStore: (payload) => dispatch(getFavoriteStoreActn(payload)),
  fetchCurrentStore: (payload) => dispatch(getCurrentStoreInfo(payload)),
  setClickAnalyticsData: (payload) => dispatch(setClickAnalyticsData(payload)),
  setLocationFlag: (value) => {
    dispatch(setLocationEnabledValue(value));
  },
  trackClick: (payload) => dispatch(trackClick(payload)),
  setExternalCampaignFired: (payload) => {
    dispatch(setExternalCampaignState(payload));
  },
  trackPageLoad: (payload) => {
    dispatch(setClickAnalyticsData(payload));
    const timer = setTimeout(() => {
      dispatch(
        trackPageView({
          props: {
            initialProps: {
              pageProps: {
                pageData: {
                  ...payload,
                },
              },
            },
          },
        })
      );
      const analyticsTimer = setTimeout(() => {
        dispatch(setClickAnalyticsData({}));
        clearTimeout(analyticsTimer);
      }, 200);
      clearTimeout(timer);
    }, 100);
  },
});

/* istanbul ignore next  */
const mapStateToProps = (state) => ({
  seoData: state.SEOData && state.SEOData.storelocator,
  isLocationEnabled: getIsLocationEnabled(state),
  selectedCountry: getCurrentCountry(state),
  userStoreId: getUserStoreId(state),
  labels: {
    ...getPageLabels(state),
    ...getAccessibilityLabels(state),
  },
  suggestedStoreList: state.StoreLocatorReducer && state.StoreLocatorReducer.get('suggestedStores'),
  isStoreSearched:
    state.StoreLocatorReducer && state.StoreLocatorReducer.get('storeSuggestionCompleted'),
  favoriteStore: state.User && state.User.get('defaultStore'),
  isGenericUser: getIsGenericUser(state),
  searchDone: state.StoreLocatorReducer && state.StoreLocatorReducer.get('searchDone'),
  isGeoCodeAPIEnabled: getIsGeoCodeAPIEnabled(state),
  isExternalCampaignFired: getExternalCampaignFired(state),
  mapboxStoreSearchTypesParam: getStoreSearchTypesParam(state),
  mapboxSwitch: getMapboxSwitch(state),
  storeLocatorMapboxApi: getStoreLocatorMapboxApi(state),
  storeLocatorMapboxTileIDs: getStoreLocatorMapboxTileIDs(state),
});

export default connect(mapStateToProps, mapDispatchToProps)(StoreLanding);
