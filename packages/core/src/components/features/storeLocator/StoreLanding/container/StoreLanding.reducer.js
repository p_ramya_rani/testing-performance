// 9fbef606107a605d69c0edbcd8029e5d
import { fromJS } from 'immutable';
import { DEFAULT_REDUCER_KEY } from '@tcp/core/src/utils/cache.util';
import STORE_LOCATOR_CONSTANTS from './StoreLanding.constants';

export const initialState = fromJS({
  [DEFAULT_REDUCER_KEY]: null,
  suggestedStores: [],
  currentStore: {},
  defaultStore: null,
  geoDefaultStore: null,
  storesSummaryListUS: [],
  storesSummaryListCA: [],
  storesSummaryListOthers: [],
  bopisStoresOnCart: [],
  bopisItemInventory: [],
  searchDone: false,
  storeSearchInProgress: false,
});

const StoreLocatorReducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case STORE_LOCATOR_CONSTANTS.STORES_SET_SUGGESTED_STORES:
      return state
        .set('suggestedStores', payload)
        .set('searchDone', true)
        .set('storeSearchInProgress', false);
    case STORE_LOCATOR_CONSTANTS.RESET_SUGGESTED_STORES:
      return state.set('suggestedStores', payload).set('searchDone', false);
    case STORE_LOCATOR_CONSTANTS.UPDATE_STORE_SEARCH_STATUS:
      return state.set('storeSearchInProgress', payload.storeSearchInProgress);
    default:
      if (state instanceof Object) {
        return fromJS(state);
      }
      return state;
  }
};

export default StoreLocatorReducer;
