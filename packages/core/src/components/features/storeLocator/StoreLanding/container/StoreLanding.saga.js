// 9fbef606107a605d69c0edbcd8029e5d
import { call, takeLatest, put, select } from 'redux-saga/effects';
import { validateReduxCache } from '@tcp/core/src/utils/cache.util';
import { setClickAnalyticsData, trackClick } from '@tcp/core/src/analytics/actions';
import {
  getLocationStores,
  getFavoriteStore,
  setFavoriteStore,
  getCurrentStoreInfoApi,
} from '@tcp/core/src/services/abstractors/common/storeLocator';
import {
  setDefaultStore as setDefaultStoreUserAction,
  getUserInfo,
} from '@tcp/core/src/components/features/account/User/container/User.actions';
import {
  getIsGenericUser,
  getDefaultStore,
  getUserId,
} from '@tcp/core/src/components/features/account/User/container/User.selectors';
import { getAPIConfig } from '@tcp/core/src/utils';
import STORE_LOCATOR_CONSTANTS from './StoreLanding.constants';
import { setStoresByCoordinates, updateStoreSearchStatus } from './StoreLanding.actions';
import { isMobileApp } from '../../../../../utils';
import errorMessage from '../../../../../services/handler/stateful/errorResponseMapping/index.json';
import names from '../../../../../constants/eventsName.constants';
import { toastMessageInfo } from '../../../../common/atoms/Toast/container/Toast.actions';
import { getSuggestedStores } from './StoreLanding.selectors';
import { setLocalStorage, getLocalStorage } from '../../../../../utils/localStorageManagement';

export function* fetchLocationStoresSaga({ payload }) {
  try {
    let res;
    const payloadCopy = { ...payload };
    const currentDate = new Date();
    if (payloadCopy?.akamaiBasedLogic) {
      payloadCopy.storeLocatorMapboxTileIDs = 'childrensplace.store-locator-all';
    } else {
      yield put(updateStoreSearchStatus({ storeSearchInProgress: true }));
      res = yield call(getLocationStores, payloadCopy); // for non akamai store just calling Api directly
    }
    const defaultStore = JSON.parse(getLocalStorage('defaultStore'));
    let minutes = 1500; // default value incase of no akamaiStoreTimeStamp and no store selected
    if (defaultStore?.akamaiStoreTimeStamp) {
      const pastDate = new Date(defaultStore?.akamaiStoreTimeStamp);
      const timeDiff = Math.abs(currentDate.getTime() - pastDate.getTime());
      minutes = Math.ceil(timeDiff / (1000 * 60));
    }
    if (!defaultStore && payload?.akamaiBasedLogic && minutes > 1440) {
      // akamai store fetched if no store in local storage or refetched if store in storage added more than 24 hours back
      res = yield call(getLocationStores, payloadCopy);
      const defaultStoreStorage = res?.[0];
      defaultStoreStorage.akamaiStoreTimeStamp = currentDate;
      setLocalStorage({ key: 'defaultStore', value: JSON.stringify(defaultStoreStorage) });
    }
    if (res && Array.isArray(res)) {
      yield put(setStoresByCoordinates(res));
    } else if (!payload.akamaiBasedLogic) {
      yield put(
        setClickAnalyticsData({
          customEvents: ['event89', 'event90'],
          storeSearchCriteria: payload.radius,
          clickEvent: true,
        })
      );
      yield put(
        trackClick({
          name: names.screenNames.search_store_null,
        })
      );
      throw res;
    }
  } catch (err) {
    yield put(setStoresByCoordinates([]));
    if (isMobileApp())
      yield put(toastMessageInfo(errorMessage.ERROR_MESSAGES_BOPIS.storeSearchException));
  }
}

function checkFavoriteStoreNullSetCondition(isForceUpdate, isGenericUser, lat, long, storeId) {
  return !isForceUpdate && isGenericUser && lat === null && long === null && !storeId;
}

const hasStoreLatLong = (lat, long) => lat && long;

/**
 * @method getFavoriteStoreSagaError
 * @param {object} err - error object from the saga call
 */
export function* getFavoriteStoreSagaError(err) {
  return isMobileApp() && err
    ? yield put(toastMessageInfo(errorMessage.ERROR_MESSAGES_BOPIS.storeSearchException))
    : yield put(setDefaultStoreUserAction(null));
}

/**
 * @method getFavoriteStoreSaga
 * @description - Calls favorite store wcs api or graphql store/info
 * @param {object} payload consisting of store info
 */
export function* getFavoriteStoreSaga({ payload }) {
  try {
    const {
      geoLatLang: { lat = null, long = null } = {},
      isForceUpdate = false,
      storeId,
    } = payload;
    const isGenericUser = yield select(getIsGenericUser);
    if (checkFavoriteStoreNullSetCondition(isForceUpdate, isGenericUser, lat, long, storeId)) {
      return yield put(setDefaultStoreUserAction(null));
    }

    let response = null;
    const defaultStoreId = yield select(getDefaultStore);
    const favoriteStoreId = storeId || defaultStoreId;
    if (!isGenericUser || isForceUpdate || storeId) {
      const { brandIdCMS, siteIdCMS } = getAPIConfig();

      // Call Mule API when we have lat long, otherwise, call graphql to get the details with store loc id.
      if (hasStoreLatLong(lat, long)) {
        response = yield call(getFavoriteStore, payload);
      } else if (favoriteStoreId) {
        response = yield call(
          getCurrentStoreInfoApi.getData,
          brandIdCMS,
          siteIdCMS,
          favoriteStoreId
        );
      }
    }
    const validResponse = response && response.basicInfo ? response : null;
    if (storeId && validResponse) {
      setLocalStorage({ key: 'defaultStore', value: JSON.stringify(validResponse) });
    }
    return yield put(setDefaultStoreUserAction(validResponse));
  } catch (err) {
    return yield call(getFavoriteStoreSagaError, err);
  }
}

export function* setFavoriteStoreSaga({ payload }) {
  try {
    const state = yield select();
    const userId = yield select(getUserId);
    let suggestedStore = payload;
    const favStoreId = payload && payload.basicInfo ? payload.basicInfo.id : '';
    if (payload.key === STORE_LOCATOR_CONSTANTS.STORE_LOCATOR) {
      suggestedStore = yield getSuggestedStores(state, favStoreId);
    }
    const res = yield call(setFavoriteStore, favStoreId, state, suggestedStore, userId);
    if (res) {
      yield put(setDefaultStoreUserAction(payload));
      yield put(getUserInfo({ ignoreCache: true }));
    }
  } catch (err) {
    if (isMobileApp())
      yield put(toastMessageInfo(errorMessage.ERROR_MESSAGES_BOPIS.storeSearchException));
  }
}

export function* StoreLocatorSaga() {
  const cachedFavoriteStore = validateReduxCache(getFavoriteStoreSaga);
  yield takeLatest(STORE_LOCATOR_CONSTANTS.GET_LOCATION_STORES, fetchLocationStoresSaga);
  yield takeLatest(STORE_LOCATOR_CONSTANTS.GET_FAVORITE_STORE, cachedFavoriteStore);
  yield takeLatest(STORE_LOCATOR_CONSTANTS.SET_FAVORITE_STORE, setFavoriteStoreSaga);
}

export default StoreLocatorSaga;
