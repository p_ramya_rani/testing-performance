// 9fbef606107a605d69c0edbcd8029e5d
/* istanbul ignore file */
import { LABELS, SEO_DATA } from '@tcp/core/src/reduxStore/constants';
import { loadComponentLabelsData, loadPageSEOData } from '@tcp/core/src/reduxStore/actions';
import STORE_LOCATOR_CONSTANTS from './StoreLanding.constants';
import { isMobileApp } from '../../../../../utils';

export function getSetDefaultStoreActn(payload) {
  return {
    payload,
    type: STORE_LOCATOR_CONSTANTS.SET_DEFAULT_STORE,
  };
}

export function getSetGeoDefaultStoreActn(payload) {
  return {
    payload,
    type: STORE_LOCATOR_CONSTANTS.SET_GEO_DEFAULT_STORE,
  };
}

export function getFavoriteStoreActn(payload) {
  return {
    payload,
    type: STORE_LOCATOR_CONSTANTS.GET_FAVORITE_STORE,
  };
}

export function setFavoriteStoreActn(payload) {
  return {
    payload,
    type: STORE_LOCATOR_CONSTANTS.SET_FAVORITE_STORE,
  };
}

export function getStoresByCoordinates(payload) {
  return {
    payload,
    type: STORE_LOCATOR_CONSTANTS.GET_LOCATION_STORES,
  };
}

export function setStoresByCoordinates(payload) {
  return {
    payload,
    type: STORE_LOCATOR_CONSTANTS.STORES_SET_SUGGESTED_STORES,
  };
}

export function resetStoresByCoordinates(payload) {
  return {
    payload,
    type: STORE_LOCATOR_CONSTANTS.RESET_SUGGESTED_STORES,
  };
}

export const initActions = isMobileApp()
  ? [
      loadComponentLabelsData({ category: LABELS.global }),
      loadPageSEOData({ page: SEO_DATA.storeLocator }),
    ]
  : [
      (args) => loadComponentLabelsData({ category: LABELS.global, ...args }),
      (args) => loadPageSEOData({ page: SEO_DATA.storeLocator, ...args }),
    ];

export function updateStoreSearchStatus(payload) {
  return {
    payload,
    type: STORE_LOCATOR_CONSTANTS.UPDATE_STORE_SEARCH_STATUS,
  };
}
