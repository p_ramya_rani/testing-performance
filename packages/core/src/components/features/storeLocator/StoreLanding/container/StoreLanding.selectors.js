// 9fbef606107a605d69c0edbcd8029e5d 
import {
  STORE_LOCATOR_REDUCER_KEY,
  SESSIONCONFIG_REDUCER_KEY,
  APICONFIG_REDUCER_KEY,
} from '@tcp/core/src/constants/reducer.constants';
import { getLabelValue } from '@tcp/core/src/utils';

import { parseBoolean } from '@tcp/core/src/utils/badge.util';

export const getCurrentCountry = state => {
  return state[SESSIONCONFIG_REDUCER_KEY] && state[SESSIONCONFIG_REDUCER_KEY].siteDetails.country;
};

export const getIsGeoCodeAPIEnabled = state => {
  return (
    state[SESSIONCONFIG_REDUCER_KEY] &&
    parseBoolean(state[SESSIONCONFIG_REDUCER_KEY].siteDetails.STORELOC_GEONODE_API_ENABLED)
  );
};

export const getPageLabels = ({ Labels }) => {
  const pageLabels = Labels.StoreLocator;
  let finalLabels = {};
  if (pageLabels !== undefined) {
    const { StoreLanding, StoreDetail, StoreList } = pageLabels;
    finalLabels = {
      ...StoreLanding,
      ...StoreDetail,
      ...StoreList,
    };
  }
  return finalLabels;
};

export const getStoreInfo = state => {
  return state[STORE_LOCATOR_REDUCER_KEY];
};

export const getIsLocationEnabled = state => state[APICONFIG_REDUCER_KEY].locationEnabled || false;

export const getAccessibilityLabels = state => {
  return {
    markerIconText: getLabelValue(state.Labels, 'lbl_marker_icon', 'accessibility', 'global'),
    searchIconText: getLabelValue(state.Labels, 'lbl_search_icon', 'accessibility', 'global'),
  };
};

export const getSuggestedStores = (state, storeId) => {
  return getStoreInfo(state)
    .get('suggestedStores')
    .find(stores => stores.basicInfo.id === storeId);
};
