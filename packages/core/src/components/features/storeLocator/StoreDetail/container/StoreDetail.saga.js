// 9fbef606107a605d69c0edbcd8029e5d 
import { call, takeLatest, put, all, putResolve } from 'redux-saga/effects';
import { getModuleX } from '@tcp/core/src/services/abstractors/common/moduleXComposite';
import { getAPIConfig } from '@tcp/core/src/utils';
import { validateReduxCache } from '@tcp/core/src/utils/cache.util';
import {
  getCurrentStoreInfoApi,
  getNearByStoreApi,
} from '@tcp/core/src/services/abstractors/common/storeLocator';
import constants from './StoreDetail.constants';
import {
  setCurrentStoreInfo,
  setNearByStore,
  setModuleXContent,
  setStoreDetailLoadingState,
} from './StoreDetail.actions';

export function* getCurrentStore({ payload: { storeID, resolve, apiConfig: config } }) {
  try {
    const apiConfig = config || getAPIConfig();
    const { brandIdCMS, siteIdCMS } = apiConfig;
    yield put(setStoreDetailLoadingState(true));
    const res = yield call(
      getCurrentStoreInfoApi.getData,
      brandIdCMS,
      siteIdCMS,
      storeID,
      apiConfig
    );
    if (resolve) {
      resolve();
    }
    yield put(setStoreDetailLoadingState(false));
    return yield putResolve(setCurrentStoreInfo(res));
  } catch (err) {
    if (resolve) {
      resolve();
    }
    yield put(setStoreDetailLoadingState(false));
    return yield null;
  }
}

export function* getNearByStore({ payload }) {
  try {
    const res = yield call(getNearByStoreApi, payload);
    return yield putResolve(setNearByStore(res));
  } catch (err) {
    return yield null;
  }
}

export function* fetchModuleX({ payload = [] }) {
  try {
    const moduleXResponses = yield call(getModuleX, payload);
    const putDescriptorParams = Object.keys(moduleXResponses).map(item => ({
      key: item,
      value: moduleXResponses[item],
    }));
    return yield all([
      put(setModuleXContent(putDescriptorParams[0])),
      put(setModuleXContent(putDescriptorParams[1])),
      put(setModuleXContent(putDescriptorParams[2])),
      put(setModuleXContent(putDescriptorParams[3])),
    ]);
  } catch (err) {
    return yield null;
  }
}

export function* StoreDetailSaga() {
  const cachedModuleX = validateReduxCache(fetchModuleX);
  yield takeLatest(constants.GET_CURRENT_STORE, getCurrentStore);
  yield takeLatest(constants.GET_SUGGESTED_STORE, getNearByStore);
  yield takeLatest(constants.GET_MODULEX_CONTENT, cachedModuleX);
}

export default StoreDetailSaga;
