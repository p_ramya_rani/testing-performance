// 9fbef606107a605d69c0edbcd8029e5d 
import constants from '../StoreDetail.constants';

import {
  setNearByStore,
  getNearByStore,
  getCurrentStoreInfo,
  setCurrentStoreInfo,
  setModuleXContent,
  getModuleXContent,
  setStoreDetailLoadingState,
} from '../StoreDetail.actions';

describe('#StoreDetail Actions', () => {
  it('#setNearByStore', () => {
    expect(setNearByStore({})).toEqual({ type: constants.SET_SUGGESTED_STORE, payload: {} });
  });

  it('#getNearByStore', () => {
    expect(getNearByStore()).toEqual({ type: constants.GET_SUGGESTED_STORE, payload: undefined });
  });

  it('#getCurrentStoreInfo', () => {
    expect(getCurrentStoreInfo()).toEqual({
      type: constants.GET_CURRENT_STORE,
      payload: undefined,
    });
  });

  it('#setCurrentStoreInfo', () => {
    expect(setCurrentStoreInfo()).toEqual({
      type: constants.SET_CURRENT_STORE,
      payload: undefined,
    });
  });

  it('#setModuleXContent', () => {
    expect(setModuleXContent()).toEqual({
      type: constants.SET_MODULEX_CONTENT,
      payload: undefined,
    });
  });

  it('#getModuleXContent', () => {
    expect(getModuleXContent()).toEqual({
      type: constants.GET_MODULEX_CONTENT,
      payload: undefined,
    });
  });

  it('#setStoreDetailLoadingState -> false ', () => {
    expect(setStoreDetailLoadingState(false)).toEqual({
      type: constants.SET_LOADING_STORE_DETAIL_STATUS,
      payload: false,
    });
  });

  it('#setStoreDetailLoadingState -> true ', () => {
    expect(setStoreDetailLoadingState(true)).toEqual({
      type: constants.SET_LOADING_STORE_DETAIL_STATUS,
      payload: true,
    });
  });
});
