// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

export default css`
  padding-top: ${props => props.theme.spacing.ELEM_SPACING.MED};
  border: 1px solid ${props => props.theme.colorPalette.gray[1600]};
  background: ${props => props.theme.colors.WHITE};
  width: 100%;
  height: auto;

  .headingWrapper {
    margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.MED};
  }
  .contentWrapper {
    margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.MED};
  }

  ${props => (props.inheritedStyles ? props.inheritedStyles : '')};
`;
