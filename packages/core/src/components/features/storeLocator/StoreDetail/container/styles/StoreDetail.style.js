// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

export default css`
  .store-name.store-name--details {
    text-transform: capitalize;
  }

  &__backlink {
    margin-left: 13px;
  }

  .address-meta {
    align-items: flex-start;
  }

  .storeaddressinfo__btn {
    @media ${props => props.theme.mediaQuery.large} {
      width: 210px;
    }
    @media ${props => props.theme.mediaQuery.mediumOnly} {
      width: 188px;
    }
  }
`;
