// 9fbef606107a605d69c0edbcd8029e5d 
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { fromJS } from 'immutable';
import logger from '@tcp/core/src/utils/loggerInstance';
import { internalCampaignProductAnalyticsList } from '@tcp/core/src/utils';
import names from '../../../../../constants/eventsName.constants';
import { getNearByStore, getModuleXContent, setCurrentStoreInfo } from './StoreDetail.actions';
import StoreDetailSkelton from '../skelton/StoreDetailSkelton.view';
import { setFavoriteStoreActn } from '../../StoreLanding/container/StoreLanding.actions';
import StoreDetail from './views/StoreDetail';
import { routeToStoreDetails, routerPush, isMobileApp } from '../../../../../utils';
import {
  getCurrentStore,
  formatCurrentStoreToObject,
  getNearByStores,
  getLabels,
  isFavoriteStore,
  getReferredContentList,
  getRichTextContent,
  getSeoData,
  getStoreDetailLoadingState,
} from './StoreDetail.selectors';
import googleMapConstants from '../../../../../constants/googleMap.constants';
import { trackPageView, setClickAnalyticsData } from '../../../../../analytics/actions';
import { getMapboxSwitch } from '../../../../../reduxStore/selectors/session.selectors';

export class StoreDetailContainer extends PureComponent {
  static routesBack(e) {
    e.preventDefault();
    if (window.history.length > 2) window.history.back();
    else {
      routerPush('/', '/home');
    }
  }

  constructor(props) {
    super(props);
    import('../../../../../utils')
      .then(({ navigateToNestedRoute, UrlHandler, isAndroid, mapHandler }) => {
        this.navigateToNestedRoute = navigateToNestedRoute;
        this.UrlHandler = UrlHandler;
        this.isAndroid = isAndroid && isAndroid();
        this.mapHandler = mapHandler;
      })
      .catch(error => {
        logger.error(error);
      });
  }

  componentDidMount() {
    const { getModuleX, referredContentList, trackStoreDetailPageView } = this.props;
    const pageName = 'companyinfo:companyinfo';

    trackStoreDetailPageView({
      customEvents: ['event80', 'event96'],
      pageName,
      pageShortName: pageName,
      pageType: 'companyinfo',
      pageSection: 'companyinfo',
      pageSubSection: 'companyinfo',
      internalCampaignIdList: !isMobileApp() ? internalCampaignProductAnalyticsList() : '',
    });

    this.loadCurrentStoreInitialInfo();
    getModuleX(referredContentList);
  }

  componentDidUpdate(prevProps) {
    const { currentStoreInfo, formatStore } = this.props;
    const prevStore = formatStore(prevProps.currentStoreInfo);
    const newStore = formatStore(currentStoreInfo);
    if (
      (prevStore.basicInfo !== undefined && prevStore.basicInfo.id) !==
      (newStore.basicInfo !== undefined && newStore.basicInfo.id)
    ) {
      this.loadCurrentStoreInitialInfo();
    }
  }

  componentWillUnmount() {
    const { resetCurrentStoreInfo } = this.props;

    resetCurrentStoreInfo();
  }

  openMoreStores = () => {
    const { navigation } = this.props;
    this.navigateToNestedRoute(navigation, 'HomeStack', 'StoreLanding');
  };

  dialStoreNumber = () => {
    const { currentStoreInfo } = this.props;
    const {
      basicInfo: { phone },
    } = currentStoreInfo;
    let phoneUrl = '';
    if (this.isAndroid) {
      phoneUrl = `tel:$${`{${phone}}`}`;
    } else phoneUrl = `telprompt:$${`{${phone}}`}`;
    this.UrlHandler(phoneUrl);
  };

  openStoreDetails = (event, store) => {
    event.preventDefault();
    const { routerHandler } = routeToStoreDetails(store);
    routerHandler();
  };

  shouldRenderDetails = store => store && store.basicInfo && store.basicInfo.id;

  openStoreDirections(store) {
    const {
      basicInfo: { address },
    } = store;
    const { addressLine1, city, state, zipCode } = address;
    if (isMobileApp()) {
      this.mapHandler(store);
    } else {
      window.open(
        `${
          googleMapConstants.OPEN_STORE_DIR_WEB
        }${addressLine1},%20${city},%20${state},%20${zipCode}`,
        '_blank',
        'noopener'
      );
    }
  }

  loadCurrentStoreInitialInfo() {
    const { loadNearByStoreInfo, currentStoreInfo, formatStore } = this.props;
    const store = formatStore(currentStoreInfo);
    if (store.basicInfo && Object.keys(store.basicInfo).length > 0) {
      const { basicInfo } = store;
      const { coordinates, id } = basicInfo;
      const payloadArgs = {
        storeLocationId: id,
        getNearby: true,
        latitude: coordinates.lat,
        longitude: coordinates.long,
      };
      loadNearByStoreInfo(payloadArgs);
    }
  }

  // eslint-disable-next-line complexity
  render() {
    const {
      currentStoreInfo,
      formatStore,
      nearByStores,
      labels,
      isFavorite,
      setFavStore,
      getRichContent,
      setResStatusNotFound,
      seoData,
      mapboxSwitch,
    } = this.props;
    const { dataLoading } = this.props;
    const store = formatStore(currentStoreInfo);
    const otherStores =
      nearByStores && nearByStores.length > 0
        ? nearByStores.filter(
            nStore =>
              nStore.basicInfo && store.basicInfo && nStore.basicInfo.id !== store.basicInfo.id
          )
        : [];
    if ((!store || !store.basicInfo || !store.basicInfo.id) && setResStatusNotFound) {
      setResStatusNotFound();
    }

    if (dataLoading) {
      return <StoreDetailSkelton />;
    }

    return this.shouldRenderDetails(store) ? (
      <StoreDetail
        className="storedetailinfo"
        store={store}
        labels={labels}
        otherStores={otherStores}
        openStoreDetails={this.openStoreDetails}
        openStoreDirections={() => this.openStoreDirections(store)}
        routesBack={this.constructor.routesBack}
        dialStoreNumber={this.dialStoreNumber}
        openMoreStores={this.openMoreStores}
        setFavoriteStore={setFavStore}
        isFavorite={isFavorite}
        fetchRichContent={getRichContent}
        isDataLoading={dataLoading}
        seoData={seoData}
        mapboxSwitch={mapboxSwitch}
      />
    ) : null;
  }
}

StoreDetailContainer.getInitialProps = () => {
  return {
    pageData: {
      loadAnalyticsOnload: false,
    },
  };
};

StoreDetailContainer.propTypes = {
  currentStoreInfo: PropTypes.instanceOf(Map),
  setResStatusNotFound: PropTypes.func.isRequired,
  formatStore: PropTypes.func.isRequired,
  nearByStores: PropTypes.shape([]).isRequired,
  labels: PropTypes.shape({
    lbl_storelanding_openInterval: PropTypes.string,
    lbl_storelanding_milesAway: PropTypes.string,
    lbl_storelanding_getdirections_link: PropTypes.string,
    lbl_storelanding_favStore: PropTypes.string,
    lbl_storelanding_setfavStore: PropTypes.string,
    lbl_storelanding_atThisPlace: PropTypes.string,
    lbl_storelanding_storedetails_link: PropTypes.string,
    lbl_storedetails_getdirections_btn: PropTypes.string,
    lbl_storedetails_callstore_btn: PropTypes.string,
    lbl_storedetails_changestore_btn: PropTypes.string,
    lbl_storedetails_mallType: PropTypes.string,
    lbl_storedetails_entranceType: PropTypes.string,
    lbl_storedetails_locations_details_btn: PropTypes.string,
    lbl_storedetails_locations_title: PropTypes.string,
  }).isRequired,
  loadNearByStoreInfo: PropTypes.func.isRequired,
  isFavorite: PropTypes.bool.isRequired,
  setFavStore: PropTypes.func.isRequired,
  navigation: PropTypes.shape({}),
  getModuleX: PropTypes.func,
  referredContentList: PropTypes.shape([]),
  getRichContent: PropTypes.func,
  trackStoreDetailPageView: PropTypes.func,
  resetCurrentStoreInfo: PropTypes.func,
  seoData: PropTypes.shape({}),
  dataLoading: PropTypes.bool,
  mapboxSwitch: PropTypes.bool,
};

StoreDetailContainer.defaultProps = {
  trackStoreDetailPageView: () => {},
  currentStoreInfo: fromJS({
    basicInfo: {
      id: '',
      storeName: '',
      phone: '',
      coordinates: {},
      address: {},
    },
    hours: {},
    features: {
      mallType: '',
      entranceType: '',
    },
  }),
  navigation: {},
  getModuleX: () => null,
  referredContentList: [],
  getRichContent: () => null,
  resetCurrentStoreInfo: () => {},
  seoData: {},
  dataLoading: true,
  mapboxSwitch: true,
};

const mapStateToProps = state => {
  return {
    currentStoreInfo: getCurrentStore(state),
    formatStore: store => formatCurrentStoreToObject(store),
    nearByStores: getNearByStores(state),
    labels: getLabels(state),
    isFavorite: isFavoriteStore(state),
    referredContentList: getReferredContentList(state),
    getRichContent: key => getRichTextContent(state, key),
    seoData: getSeoData(state),
    dataLoading: getStoreDetailLoadingState(state),
    mapboxSwitch: getMapboxSwitch(state),
  };
};

export const mapDispatchToProps = dispatch => ({
  loadNearByStoreInfo: payload => {
    dispatch(getNearByStore(payload));
  },
  setFavStore: payload => dispatch(setFavoriteStoreActn({ ...payload, key: 'DETAIL' })),
  getModuleX: payload => {
    dispatch(getModuleXContent(payload));
  },
  resetCurrentStoreInfo: () => dispatch(setCurrentStoreInfo(null)),
  trackStoreDetailPageView: payload => {
    const { customEvents, ...restPayload } = payload;

    dispatch(
      setClickAnalyticsData({
        customEvents,
      })
    );

    dispatch(
      trackPageView({
        currentScreen: names.screenNames.store_details,
        pageData: {
          ...restPayload,
        },
        props: {
          initialProps: {
            pageProps: {
              pageData: {
                ...restPayload,
              },
            },
          },
        },
      })
    );

    const timer = setTimeout(() => {
      dispatch(setClickAnalyticsData({}));
      clearTimeout(timer);
    }, 600);
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StoreDetailContainer);
