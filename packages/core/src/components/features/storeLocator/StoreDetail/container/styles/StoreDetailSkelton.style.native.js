// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

const SkeletonWrapperView = styled.ScrollView`
  padding: ${props => props.theme.spacing.ELEM_SPACING.LRG}
    ${props => props.theme.spacing.ELEM_SPACING.MED};
`;

export default SkeletonWrapperView;
