// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import { Row, Col } from '@tcp/core/src/components/common/atoms';
import LoaderSkelton from '@tcp/core/src/components/common/molecules/LoaderSkelton';

import styles from '../container/styles/StoreDetailSkelton.style';

const StoreDetailSkelton = ({ className }) => {
  return (
    <div className={className}>
      <Row>
        <Col colSize={{ small: 6, medium: 4, large: 4 }}>
          <LoaderSkelton width="100%" height="400px" />
        </Col>
        <Col colSize={{ small: 6, medium: 4, large: 8 }}>
          <LoaderSkelton width="100%" height="400px" />
        </Col>
      </Row>
      <Row>
        <Col colSize={{ small: 6, medium: 8, large: 12 }}>
          <LoaderSkelton width="100%" height="200px" />
        </Col>
      </Row>
    </div>
  );
};

StoreDetailSkelton.propTypes = {
  className: PropTypes.string.isRequired,
};

export default withStyles(StoreDetailSkelton, styles);
export { StoreDetailSkelton as StoreDetailSkeltonVanilla };
