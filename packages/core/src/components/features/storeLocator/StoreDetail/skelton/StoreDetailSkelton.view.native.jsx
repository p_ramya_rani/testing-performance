// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { View } from 'react-native';
import LoaderSkelton from '@tcp/core/src/components/common/molecules/LoaderSkelton/views/SkeletonLine.view.native';
import { ViewWithSpacing } from '@tcp/core/src/components/common/atoms/styledWrapper';
import SkeletonWrapperView from '../container/styles/StoreDetailSkelton.style.native';

const AccountOverviewTileSkelton = () => {
  return (
    <SkeletonWrapperView>
      <ViewWithSpacing spacingStyles="margin-bottom-MED">
        <LoaderSkelton width="100%" height="200" />
      </ViewWithSpacing>
      <ViewWithSpacing spacingStyles="margin-bottom-MED">
        <LoaderSkelton width="100%" height="300" />
      </ViewWithSpacing>
      <View>
        <LoaderSkelton width="100%" height="100" />
      </View>
    </SkeletonWrapperView>
  );
};
export default AccountOverviewTileSkelton;
