// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

export default css`
  padding: 0 ${props => props.theme.spacing.ELEM_SPACING.SM};
`;
