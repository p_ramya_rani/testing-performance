// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { shallow, mount } from 'enzyme';
import Theme from '@tcp/core/styles/themes';
import { ThemeProvider } from 'styled-components';
import { StoresInternationalVanilla } from '../views/StoresInternational';

describe('StoresInternational component', () => {
  it('StoresInternational component renders correctly without props', () => {
    const component = shallow(<StoresInternationalVanilla />);
    expect(component).toMatchSnapshot();
  });

  it('StoresInternational component renders correctly with props', () => {
    const props = {
      className: 'test',
      content: 'test',
      dataLocator: 'test',
    };
    const component = mount(
      <ThemeProvider theme={Theme()}>
        <StoresInternationalVanilla {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
