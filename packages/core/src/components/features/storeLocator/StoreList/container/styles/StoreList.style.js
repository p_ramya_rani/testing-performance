// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

export const customStyles = css`
  .spinner-overlay {
    position: absolute;
  }
`;

export default css`
  ${props =>
    props.theme &&
    props.theme.gridDimensions &&
    props.theme.gridDimensions.gridBreakPointsKeys.map(
      key => `
      ${key !== 'small' ? `@media ${props.theme.mediaQuery[key]} {` : ''}
        margin-right: ${props.theme.gridDimensions.gridOffsetObj[key]}px;
        margin-left: ${props.theme.gridDimensions.gridOffsetObj[key]}px;
      ${key !== 'small' ? `}` : ''}`
    )}

  .storelist__countrylbl {
    margin: ${props => props.theme.spacing.APP_LAYOUT_SPACING.SM} 0
      ${props => props.theme.spacing.APP_LAYOUT_SPACING.XS};
    @media ${props => props.theme.mediaQuery.large} {
      border-bottom: 2px solid ${props => props.theme.colorPalette.gray[500]};
    }
  }

  .storelist__countryname {
    @media ${props => props.theme.mediaQuery.large} {
      display: none;
    }
  }
`;
