// 9fbef606107a605d69c0edbcd8029e5d
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import SpinnerOverlay from '@tcp/core/src/components/common/atoms/SpinnerOverlay';
import { getStoreList, initActions } from './StoreList.actions';
import StoreListView from './views/StoreList';
import { customStyles } from './styles/StoreList.style';
import { getLabels, getStoreFormattedList, getIsFetchingStoreState } from './StoreList.selectors';

export class StoreListContainer extends PureComponent {
  componentDidMount() {
    const { getStoresList } = this.props;
    getStoresList();
  }

  render() {
    const { labels, storeListUS, storeListCA, isStoresAvailable } = this.props;
    if (isStoresAvailable) return <SpinnerOverlay inheritedStyles={customStyles} />;
    return <StoreListView labels={labels} storesList={{ storeListUS, storeListCA }} />;
  }
}

StoreListContainer.pageInfo = {
  pageId: 'stores',
};

StoreListContainer.getInitActions = () => initActions;

StoreListContainer.propTypes = {
  getStoresList: PropTypes.func.isRequired,
  labels: PropTypes.shape({}),
  storeListUS: PropTypes.shape([]),
  storeListCA: PropTypes.shape([]),
  isStoresAvailable: PropTypes.bool,
};

StoreListContainer.defaultProps = {
  labels: {},
  storeListUS: [],
  storeListCA: [],
  isStoresAvailable: false,
};

const mapStateToProps = (state) => {
  return {
    seoData: state.SEOData && state.SEOData.storelocator,
    labels: getLabels(state),
    storeListUS: getStoreFormattedList(state, 'US'),
    storeListCA: getStoreFormattedList(state, 'CA'),
    isStoresAvailable: getIsFetchingStoreState(state),
  };
};

export const mapDispatchToProps = (dispatch) => ({
  getStoresList: () => {
    dispatch(getStoreList());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(StoreListContainer);
