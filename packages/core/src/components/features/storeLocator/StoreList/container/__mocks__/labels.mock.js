// 9fbef606107a605d69c0edbcd8029e5d 
export default {
  lbl_storelist_searchByStates: 'SEARCH FOR STORES BY STATE',
  lbl_storelist_searchByStates_dropdown: 'Select a State',
  lbl_storelist_backLink: 'Back',
};
