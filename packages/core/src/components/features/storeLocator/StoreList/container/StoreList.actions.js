// 9fbef606107a605d69c0edbcd8029e5d
import { SEO_DATA } from '@tcp/core/src/reduxStore/constants';
import { loadPageSEOData } from '@tcp/core/src/reduxStore/actions';
import { isMobileApp } from '../../../../../utils';
import STORE_LIST_CONSTANTS from './StoreList.constants';

export function getStoreList(payload) {
  return {
    payload,
    type: STORE_LIST_CONSTANTS.GET_STORE_LIST,
  };
}

export function setStoreList(payload) {
  return {
    payload,
    type: STORE_LIST_CONSTANTS.SET_STORE_LIST,
  };
}

export function setIsFetchingStore(payload) {
  return {
    payload,
    type: STORE_LIST_CONSTANTS.FETCHING_STORES,
  };
}

export const initActions = isMobileApp()
  ? []
  : [(args) => loadPageSEOData({ page: SEO_DATA.storeList, ...args })];
