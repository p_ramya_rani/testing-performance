// 9fbef606107a605d69c0edbcd8029e5d 
import { useState } from 'react';

export default function useBooleanState(initialState = false) {
  const [state, setState] = useState(initialState);
  const toggle = () => setState(!state);
  return [state, toggle];
}
