// 9fbef606107a605d69c0edbcd8029e5d 
/**
 * @description - Deep Link Url Patterns Config
 */

export default {
  HOME: '/home',
  CATEGORY_LANDING: '/c/',
  PRODUCT_LIST: '/p/',
  OUTFIT_DETAILS: '/outfit/',
  SEARCH_DETAIL: '/search/',
  BUNDLE_DETAIL: '/b/',
  CONTENT: '/content/',
  HELP_CENTER: '/help-center/',
  STORE_LOCATOR: '/store-locator',
  CHANGE_PASSWORD: '/change-password',
  LOGIN: '/login',
  REGISTER: '/register',
  FAVORITES: '/favorites',
  CHECKOUT: '/checkout/',
  ACCOUNT: '/account/',
  BAG: '/bag',
  STORE: '/store/',
  STORES: '/stores',
};
