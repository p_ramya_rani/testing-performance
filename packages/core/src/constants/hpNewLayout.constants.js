// 9fbef606107a605d69c0edbcd8029e5d
import theme from '@tcp/core/styles/themes/TCP';

const {
  colors: {
    BORDER: { NORMAL },
    HP_BG_COLOR,
  },
} = theme;
const MODULE_BOX_SHADOW = {
  shadowColor: NORMAL,
  shadowOffset: { width: 0, height: 1 },
  shadowOpacity: 0.8,
  shadowRadius: 5,
  elevation: 5,
};

export default {
  HP_BG_COLOR,
  BODY_PADDING: 15,
  MODULE_BOX_SHADOW,
};
