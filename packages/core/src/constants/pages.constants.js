// 9fbef606107a605d69c0edbcd8029e5d
const PAGES = {
  HOME_PAGE: 'Home',
  CHECKOUT_PAGE: 'checkout',
  SEARCH_PAGE: 'search',
  PRODUCT_LISTING_PAGE: 'c',
  OUTFIT: 'outfit',
  PRODUCT_DESCRIPTION_PAGE: 'p',
  ACCOUNT_PAGE: 'Account',
  CONTENT: 'content',
  BUNDLED_PRODUCT_DESCRIPTION_PAGE: 'b',
  STORE: 'store',
  BAG: 'bag',
  PLP: 'PLP',
  SLP: 'SLP',
  STORELOCATOR: 'storelocator',
  STOREPATH: 'store-locator',
};
export default PAGES;
