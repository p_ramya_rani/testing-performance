export const TIER1 = 'tier1';
export const TIER2 = 'tier2';
export const TIER3 = 'tier3';
export const TIER2NETWORKSPEED = ['3g'];
export const TIER3NETWORKSPEED = ['slow-2g', '2g'];
