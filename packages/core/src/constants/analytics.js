// 9fbef606107a605d69c0edbcd8029e5d

import { isClient } from '../utils/utils';

export const promoAnalyticsValue = 'aboveheader_na_na_mpr_120118_mprc';
export const loyalityAnalyticsValue = 'lp_s1_banner_na_050519_doubleup';
export const RECOMMENDATION_GA_KEY = 'reccObj';
export const EVENTNAME_ADDTOCART = 'Add To Cart';

// Loyalty clicks methods
export const getLoyaltyLocation = () => isClient() && window && window.loyaltyLocation;
export const setLoyaltyLocation = loyaltyLocation => {
  if (isClient() && window && loyaltyLocation) window.loyaltyLocation = loyaltyLocation;
};

export const getCreateAccountPayload = loyaltyLocation => {
  return {
    eventName: 'Create_Account_clicks125',
    customEvents: ['event125'],
    loyaltyLocation,
  };
};

export const getLoginPayload = loyaltyLocation => {
  return {
    eventName: 'Login_Clicks126',
    customEvents: ['event126'],
    loyaltyLocation,
  };
};

export const getCreateAccountSuccessPayload = loyaltyLocation => {
  return {
    eventName: 'Create_Accounts_Success127',
    customEvents: ['event127'],
    loyaltyLocation,
  };
};

export const getLoginSuccessPayload = loyaltyLocation => {
  return {
    eventName: 'Login_Success128',
    customEvents: ['event128'],
    loyaltyLocation,
  };
};

export const getApplyNowPayload = loyaltyLocation => {
  return {
    eventName: 'Apply_Now129',
    customEvents: ['event129'],
    loyaltyLocation,
  };
};

export const getLearnMorePayload = loyaltyLocation => {
  return {
    eventName: 'Learn_More130',
    customEvents: ['event130'],
    loyaltyLocation,
  };
};

export const getApplyOrAcceptPayload = loyaltyLocation => {
  return {
    eventName: 'Apply_or_Accept_Offer131',
    customEvents: ['event131'],
    loyaltyLocation,
  };
};

export const getPLCCSubmitPayload = loyaltyLocation => {
  return {
    eventName: 'PLCC_Submit132',
    customEvents: ['event132'],
    loyaltyLocation,
  };
};

export const getPLCCNoThanksPayload = loyaltyLocation => {
  return {
    eventName: 'PLCC_No_thanks133',
    customEvents: ['event133'],
    loyaltyLocation,
  };
};

export const getMPRImageClickPayload = loyaltyLocation => {
  return {
    eventName: 'MPR_Image_Clicks134',
    customEvents: ['event134'],
    loyaltyLocation,
  };
};

export const getPLCCImageClickPayload = loyaltyLocation => {
  return {
    eventName: 'PLCC_Image_Clicks135',
    customEvents: ['event135'],
    loyaltyLocation,
  };
};

export const getJoinTheClubPayload = loyaltyLocation => {
  return {
    eventName: 'Join_The_Club_Birthday136',
    customEvents: ['event136'],
    loyaltyLocation,
  };
};

export const getContinueAsGuestPayload = loyaltyLocation => {
  return {
    eventName: 'Continue_As_Guest137',
    customEvents: ['event137'],
    loyaltyLocation,
  };
};

const extractCheckout = (pageName, isRTPSFlow) => {
  if (pageName === 'shipping' || pageName === 'billing' || pageName === 'review')
    return isRTPSFlow ? `rtps-${pageName}` : pageName;

  if (pageName === 'bagpage') return 'cart';
  return getLoyaltyLocation();
};

export const getCustomLoyaltyLocation = (pageCategory, isRTPSFlow) => {
  if (!pageCategory) return pageCategory;
  const pageName = pageCategory.toLowerCase();
  if (pageName === 'isproductdetailview') return 'pdp';
  if (pageName === 'isaddedtobagpage') return 'added-to-bag-modal';
  if (pageName === 'minibag') return 'minibag';
  return extractCheckout(pageName, isRTPSFlow);
};
