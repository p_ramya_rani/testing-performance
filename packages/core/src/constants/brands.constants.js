const GYM = 'GYM';
const GYM_CMS = 'Gymboree';

const TCP = 'TCP';
const TCP_CMS = TCP;

const SNJ = 'SNJ';
const SNJ_CMS = SNJ;

const ALL_BRANDS = [TCP, GYM, SNJ]; // don't change sequence, being used in tabs

const brandsMapping = {
  tcp: TCP_CMS,
  gym: GYM_CMS,
  snj: SNJ_CMS,
};
module.exports = {
  ALL_BRANDS,
  GYM,
  GYM_CMS,
  TCP,
  TCP_CMS,
  SNJ,
  SNJ_CMS,
  brandsMapping,
};
