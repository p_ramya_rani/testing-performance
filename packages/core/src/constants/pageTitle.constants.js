// 9fbef606107a605d69c0edbcd8029e5d
export const PAGE_TITLES = {
  home: 'Home',
  checkout: 'Checkout',
  search: 'Search',
  c: 'Product Listing',
  outfit: 'Outfits',
  p: 'Product',
  account: 'My Account',
  content: 'Content',
  b: 'Bundles',
  bag: 'Bag',
  'help-center': 'Help Center',
};

export const BRAND_NAMES = {
  tcp: "The Children's Place",
  gym: 'Gymboree',
};

export const DEFAULT_TITLE = 'Page';
