// 9fbef606107a605d69c0edbcd8029e5d 
export const LAST_SAVED_BOOTSTRAP_DATA_KEY = 'lastSavedBootStrapData';
export const BOOTSTRAP_DATA_KEY = 'bootStrapData';

export const LAST_SAVED_CONFIGURATION_DATA_KEY = 'lastSavedConfigurationData';
export const CONFIGURATION_DATA_KEY = 'configurationData';

export const LAST_SAVED_NAVIGATION_DATA_KEY = 'lastSavedNavigationData';
export const NAVIGATION_DATA_KEY = 'navigationData';

export const LAST_SAVED_LAYOUT_DATA_KEY = 'lastSavedLayoutData';
export const LAYOUT_DATA_KEY = 'layoutData';
