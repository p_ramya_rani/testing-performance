// 9fbef606107a605d69c0edbcd8029e5d 
export const TRACE_ID_HEADER = 'x-trace-id';
export const SESSION_ID_HEADER = 'x-session-id';
export const CACHE_TAG_HEADER = 'x-cache-tag';
export const QUERY_TYPE_HEADER = 'x-query-type';

export default {
  TRACE_ID_HEADER,
  SESSION_ID_HEADER,
  CACHE_TAG_HEADER,
  QUERY_TYPE_HEADER,
};
