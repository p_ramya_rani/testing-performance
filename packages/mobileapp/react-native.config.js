// 9fbef606107a605d69c0edbcd8029e5d 
module.exports = {
  dependencies: {
    'react-native-code-push': {
      platforms: {
        android: null, // disable Android platform, other platforms will still autolink if provided
      },
    },
    'react-native-permissions': {
      platforms: {
        ios: null,
      },
    },
    'react-native-gesture-handler': {
      platforms: {
        ios: null,
      },
    },
    'react-native-akamaibmp': {
      platforms: {
        ios: null,
        android: null,
      },
    },
  },
  project: {
    ios: {},
    android: {}, // grouped into "project"
  },
  assets: ['./assets/fonts'], // stays the same
};
