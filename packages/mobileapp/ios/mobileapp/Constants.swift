// 9fbef606107a605d69c0edbcd8029e5d 
//
//  Constants.swift
//  mobileapp
//
//  Created by Rakesh Lohan on 29/09/20.
//  Copyright © 2020 Facebook. All rights reserved.
//

import Foundation

struct Constants {

  // MerchantCapabilities
    static let MerchantCapabilities = "merchantCapabilities"
    static let Supports3DS = "supports3DS"

  //Supported Networks
  static let SupportedNetworks = "supportedNetworks"
  static let ShippingType = "shippingType"
  static let Visa = "visa"
  static let MasterCard = "masterCard"
  static let Amex = "amex"
  static let Discover = "discover"

  //Shipping Type
  static let Shipping = "shipping"
  static let Delivery = "delivery"
  static let StorePickup = "storePickup"
  static let ServicePickup = "servicePickup"

  static let MerchantId = "merchantId"
  static let CountryCode = "countryCode"
  static let Country = "country"
  static let CurrencyCode = "currencyCode"
  static let ShippingContact = "shippingContact"
  static let GivenName = "givenName"
  static let FamilyName = "familyName"
  static let AdministrativeArea = "administrativeArea"
  static let PostalCode = "postalCode"
  static let Locality = "locality"
  static let EmailAddress = "emailAddress"
  static let PhoneNumber = "phoneNumber"
  static let AddressLines = "addressLines"
  static let IsGuest = "isGuest"

  static let Street = "street"
  static let City = "city"
  static let State = "state"

  static let BillingContact = "billingContact"

  static let RequiredBillingContactFields = "requiredBillingContactFields"
  static let RequiredShippingContactFields = "requiredShippingContactFields"

  static let RequiredContactFieldsName = "name"
  static let RequiredContactFieldsPhone = "phone"
  static let RequiredContactFieldsPostalAddress = "postalAddress"
  static let RequiredContactFieldsEmail = "email"

  //Line Items
  static let LineItems = "lineItems"
  static let Amount = "amount"
  static let Label = "label"
  static let ItemType = "type"
  static let ItemTypeFinal = "final"
  static let Total = "total"

  static let PayloadIncomplete = "Error: Payment  ayload Incomplete."
  static let InvalidRequest = "Error: Payment request is invalid."
  static let PaymentFailed = "Error: Payment request failed."
  static let PaymentSuccess = "Error: Payment request success."

  static let DefaultMerchantId = "merchant.com.childrensplace.tcp-ios.int"
  static let DefaultCountryCode = "US"
  static let DefaultCurrencyCode = "USD"

  static let ShippingMethods = "shippingMethods"
  static let Detail = "detail"
  static let Identifier = "identifier"

  static let Errors = "errors"
  static let Error = "error"
  static let Field = "field"

  static let ErrorType = "errorType"
  static let ErrorContact = "contact"
  static let ErrorShipping = "shipping"

  //Event
  static let EventShippingMethod = "onShippingMethodChange"
  static let EventShippingContact = "onShippingContactChange"
  static let EventCancelPayment = "cancelPayment"
  static let EventShippingContactPaymentAuthorize = "shippingContactPaymentAuthorize"
  
  //VibesEvent
  static let EventPushReceived = "pushReceived"
  static let EventPushOpened = "pushOpened"
  static let EventRegisterDevice = "registerDevice"
  
  //Vibes User Defaults keys
  static let KeyIsAppLaunchedWithNotification = "isAppLaunchedWithNotification"
  static let KeyNotificationDictionary = "notificationDictionary"
  
}
