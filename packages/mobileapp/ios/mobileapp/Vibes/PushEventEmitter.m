//
//  PushEventEmitter.m
//  mobileapp
//
//  Created by Rakesh Lohan on 03/05/21.
//  Copyright © 2021 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>

@interface RCT_EXTERN_MODULE(PushEventEmitter, RCTEventEmitter)
  RCT_EXTERN_METHOD(supportedEvents)
  RCT_EXTERN_METHOD(isAppOpenWithNotification)
@end
