//
//  PushEventEmitter.swift
//  mobileapp
//
//  Created by Rakesh Lohan on 03/05/21.
//  Copyright © 2021 Facebook. All rights reserved.
//

import Foundation

/// Event emitter for Push related stuff
@objc(PushEventEmitter)
open class PushEventEmitter: RCTEventEmitter {
    public static var emitter: RCTEventEmitter!

    @objc
    override public static func requiresMainQueueSetup() -> Bool {
        return true
    }

    override init() {
        super.init()
        PushEventEmitter.emitter = self
    }

    override open func supportedEvents() -> [String] {
        return [Constants.EventPushReceived, Constants.EventPushOpened, Constants.EventRegisterDevice]
    }

    /// Emitts the `pushReceived` event
    @objc
    class func sendPushReceivedEvent(_ payload: NSDictionary) {
        // we want to be sure this runs in the next run loop, after bridge is set
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            if let emitter = PushEventEmitter.emitter { // check that RNEventEmitter has init
                debugPrint("Emitting onPushReceived event")
                emitter.sendEvent(withName: Constants.EventPushReceived, body: payload)
            }
        }
    }
  
  /// Emitts the `pushOpened` event
  @objc
  class func sendPushOpenedEvent(_ payload: NSDictionary) {
      // we want to be sure this runs in the next run loop, after bridge is set
      DispatchQueue.main.asyncAfter(deadline: .now()) {
          if let emitter = PushEventEmitter.emitter { // check that RNEventEmitter has init
              debugPrint("Emitting onPushOpened event")
              emitter.sendEvent(withName: Constants.EventPushOpened, body: payload)
          }
      }
  }
  
  /// Emitts the `registerDevice` event
  @objc
  class func sendRegisterDeviceEvent() {
      // we want to be sure this runs in the next run loop, after bridge is set
      DispatchQueue.main.asyncAfter(deadline: .now()) {
          if let emitter = PushEventEmitter.emitter { // check that RNEventEmitter has init
              debugPrint("Emitting onRegisterDevice event")
              emitter.sendEvent(withName: Constants.EventRegisterDevice, body: nil)
          }
      }
  }
  
  @objc func isAppOpenWithNotification()  {
    if UserDefaults.standard.bool(forKey: Constants.KeyIsAppLaunchedWithNotification) {
      if let eventDic = UserDefaults.standard.object(forKey: Constants.KeyNotificationDictionary) as? NSDictionary {
        UserDefaults.standard.removeObject(forKey: Constants.KeyIsAppLaunchedWithNotification)
        UserDefaults.standard.removeObject(forKey: Constants.KeyNotificationDictionary)
        PushEventEmitter.sendPushOpenedEvent(eventDic)
      }
    }
  }
}
