// 9fbef606107a605d69c0edbcd8029e5d
/**
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

#import <Firebase.h>
#import "AppDelegate.h"
#import <CodePush/CodePush.h>
#import <React/RCTBridge.h>
#import <React/RCTBundleURLProvider.h>
#import <React/RCTRootView.h>
#import <AppCenterReactNativeShared/AppCenterReactNativeShared.h>
#import <AppCenterReactNative.h>
#import <AppCenterReactNativeCrashes.h>
//#import <raygun4apple/raygun4apple_iOS.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "BraintreeCore.h"
#import "QMNative.h"
#import <TwitterKit/TWTRKit.h>

#import <React/RCTLinkingManager.h>
#import <AVFoundation/AVFoundation.h>
#import <mobileapp-Swift.h>

@import GooglePlaces;



//  #if DEBUG
//  #import <FlipperKit/FlipperClient.h>
//  #import <FlipperKitLayoutPlugin/FlipperKitLayoutPlugin.h>
//  #import <FlipperKitUserDefaultsPlugin/FKUserDefaultsPlugin.h>
//  #import <FlipperKitNetworkPlugin/FlipperKitNetworkPlugin.h>
//  #import <SKIOSNetworkPlugin/SKIOSNetworkAdapter.h>
//  #import <FlipperKitReactPlugin/FlipperKitReactPlugin.h>
//  #endif

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
            options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {

  if ([url.scheme localizedCaseInsensitiveCompare:[self getCurrentBundleIdentifierWithPaymentString]] == NSOrderedSame) {
    return [BTAppSwitch handleOpenURL:url options:options];
  }
  else if ([url.scheme localizedCaseInsensitiveCompare:@"tcp"] == NSOrderedSame) {
    return [RCTLinkingManager application:application openURL:url options:options];
  }
  else if ([url.scheme localizedCaseInsensitiveCompare:@"gym"] == NSOrderedSame) {
    return [RCTLinkingManager application:application openURL:url options:options];
  }
  else if ([[url scheme] hasPrefix:@"twitter"] == YES) {
    return [[Twitter sharedInstance] application:application openURL:url options:options];
    }

  BOOL handled =  [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url options:options];

  // Add any custom logic here.
  return handled;
  
 

}




- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  //  NSDictionary *props = @{@"appType":launchOptions};

  // Start Init of 3P Modules/Services

  // ********************************
  // RAYGUN (Error Reporting)
  // ********************************
  //  RaygunClient *rgClient = [RaygunClient sharedInstanceWithApiKey:@"W1Hxa4blNaRqscJ9Y5A0Q"];
  //  [rgClient enableCrashReporting];
  //  NSException *exception = [NSException exceptionWithName:@"TestException" reason:@"Something went wrong" userInfo:nil];
  //  [RaygunClient.sharedInstance sendException:exception withTags:@[@"CustomTag1", @"CustomTag2"]];
  //  [rgClient enableRealUserMonitoring];
  //  [RaygunClient.sharedInstance enableNetworkPerformanceMonitoring];


  //Audio session of other app not stop
  [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:nil];


  // ********************************
  // Firebase
  // ********************************
  if ([FIRApp defaultApp] == nil) {
    [FIRApp configure];
  }

  // ********************************
  // AppCenter
  // ********************************
  [AppCenterReactNative register];
  [AppCenterReactNativeCrashes registerWithAutomaticProcessing];

  // ********************************
  // Facebook
  // ********************************
  [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];

  //Clear keychain data on app install
  [self clearKeychainData];


  // ********************************
  // Quantum Metric
  // ********************************
  [QMNative initializeWithSubscription: @"tcp" uid: @"53615a87-855a-49fc-80c5-664594de797d"
   ];
  [[QMNative sharedInstance] gotSessionCookieCallback:^(NSString *sessionCookie, NSString *userString) {
    [[NSUserDefaults standardUserDefaults] setObject:sessionCookie forKey:@"QuantumMetricSessionCookie"];
    [[NSUserDefaults standardUserDefaults] synchronize];
  }];

  [[UNUserNotificationCenter currentNotificationCenter] setDelegate: self];

  //Vibes Register Device for Push Notification
  Vibes const *vibes = [[VibesClient standard] vibes];
  [vibes setDelegate:(id) self];
  [vibes registerDevice];

  RCTBridge *bridge = [[RCTBridge alloc] initWithDelegate:self launchOptions:launchOptions];
  RCTRootView *rootView = [[RCTRootView alloc] initWithBridge:bridge
                                                   moduleName:@"mobileapp"
                                            initialProperties:nil];

  rootView.backgroundColor = [[UIColor alloc] initWithRed:1.0f green:1.0f blue:1.0f alpha:1];

  self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  UIViewController *rootViewController = [UIViewController new];
  rootViewController.view = rootView;
  self.window.rootViewController = rootViewController;
  [self.window makeKeyAndVisible];
  [BTAppSwitch setReturnURLScheme:[self getCurrentBundleIdentifierWithPaymentString]];
  [GMSPlacesClient provideAPIKey: [self getGooglePlaceAPIKey]];

  if ([launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey]) {
    // isAppLauncedWithNotification = true;
    // When the app launch after user tap on notification (originally was not running / not in background)
    NSDictionary * payload = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    NSLog(@"UIApplicationLaunchOptionsRemoteNotificationKey %@", payload);
    NSMutableDictionary *eventDic = [self getEventDictionary:payload];
    [eventDic setObject:@"true" forKey:@"isAppOpenWithNotification"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isAppLaunchedWithNotification"];
    [[NSUserDefaults standardUserDefaults] setObject:eventDic forKey:@"notificationDictionary"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [PushEventEmitter sendPushOpenedEvent:eventDic];
  }

  return YES;
}

- (NSURL *)sourceURLForBridge:(RCTBridge *)bridge
{
#if DEBUG
  return [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index" fallbackResource:nil];
#else
  return  [CodePush bundleURL];
#endif
}

/// Clear keychain data on app install
- (void)clearKeychainData
{
  NSString *const FIRST_TIME_LAUNCH = @"firstTimeLaunch";
  NSString *const LAUNCHED = @"launched";

  if ([[NSUserDefaults standardUserDefaults] objectForKey:FIRST_TIME_LAUNCH] == nil) {

    NSArray *secItemClasses = @[(__bridge id)kSecClassGenericPassword,
                                (__bridge id)kSecClassInternetPassword,
                                (__bridge id)kSecClassCertificate,
                                (__bridge id)kSecClassKey,
                                (__bridge id)kSecClassIdentity];

    for (id secItemClass in secItemClasses) {
      NSDictionary *spec = @{(__bridge id)kSecClass: secItemClass};
      SecItemDelete((__bridge CFDictionaryRef)spec);
    }

    [[NSUserDefaults standardUserDefaults] setObject:LAUNCHED forKey:FIRST_TIME_LAUNCH];
    [[NSUserDefaults standardUserDefaults] synchronize];
  }
}

+ (void) initializeFlipper:(UIApplication *)application
{
  //  #if DEBUG
  //    FlipperClient *client = [FlipperClient sharedClient];
  //    SKDescriptorMapper *layoutDescriptorMapper = [[SKDescriptorMapper alloc] initWithDefaults];
  //    [client addPlugin: [[FlipperKitLayoutPlugin alloc] initWithRootNode: application withDescriptorMapper: layoutDescriptorMapper]];
  //    [client addPlugin: [[FKUserDefaultsPlugin alloc] initWithSuiteName:nil]];
  //    [client addPlugin: [FlipperKitReactPlugin new]];
  //    [client addPlugin: [[FlipperKitNetworkPlugin alloc] initWithNetworkAdapter:[SKIOSNetworkAdapter new]]];
  //    [client start];
  //  #endif
}

// If you support iOS 8, add the following method.
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
  if ([url.scheme localizedCaseInsensitiveCompare:[self getCurrentBundleIdentifierWithPaymentString]] == NSOrderedSame) {
    return [BTAppSwitch handleOpenURL:url sourceApplication:sourceApplication];
  }
  return NO;
}
//To Capture the Universal Link In IOS
- (BOOL)application:(UIApplication *)application
continueUserActivity:(NSUserActivity *)userActivity
 restorationHandler:(void (^)(NSArray * _Nullable))restorationHandler {
  return [RCTLinkingManager application:application continueUserActivity:userActivity restorationHandler:restorationHandler];
}

- (NSString *)getCurrentBundleIdentifierWithPaymentString {
  NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
  bundleIdentifier = [bundleIdentifier stringByAppendingString:@".payments"];
  return bundleIdentifier;
}

- (NSString *)getGooglePlaceAPIKey {
  NSString *key = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"GOOGLE_PLACE_KEY"];
  return key;
}

//MARK:-  Required for the register event.
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
  NSString * tokenString = [self stringWithDeviceToken: deviceToken];
  NSLog(@"------------------>>>>Push Token String: %@", tokenString);
  Vibes const *vibes = [[VibesClient standard] vibes];
  [vibes setPushTokenFromData: deviceToken];
}

// Required for the notification event. You must call the completion handler after handling the remote notification.
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
  Vibes const *vibes = [[VibesClient standard] vibes];
  [vibes receivedPushWith:userInfo at:[NSDate new]];
  NSDictionary *eventDic = [self getEventDictionary:userInfo];
  [PushEventEmitter sendPushReceivedEvent:eventDic];
}

// Required for the registrationError event.
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{

}

// Required for localNotification event
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void (^)(void))completionHandler
{
  Vibes const *vibes = [[VibesClient standard] vibes];
  [vibes receivedPushWith:response.notification.request.content.userInfo at:[NSDate date]];
  NSDictionary * userInfo = [[[[response notification] request] content] userInfo];
  NSDictionary *eventDictionary = [self getEventDictionary:userInfo];
  [PushEventEmitter sendPushOpenedEvent:eventDictionary];
}

// Called when a notification is delivered to a foreground app.
-(void)userNotificationCenter:(UNUserNotificationCenter *)center
      willPresentNotification:(UNNotification *)notification
        withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
{

  // allow showing foreground notifications
  completionHandler(UNNotificationPresentationOptionSound | UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionBadge);
  // or if you wish to hide all notification while in foreground replace it with
  // completionHandler(UNNotificationPresentationOptionNone);
}

//MARK:- Custom Method
-(NSMutableDictionary *)getEventDictionary:(NSDictionary *)userInfo {

  NSMutableDictionary *eventDictionary = [NSMutableDictionary new];

  if (userInfo[@"aps"][@"alert"][@"title"]) {
    NSString *title = [[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] objectForKey:@"title"];
    [eventDictionary setValue:title forKey:@"title"];
  }

  if (userInfo[@"aps"][@"alert"][@"body"]) {
    NSString *body = [[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] objectForKey:@"body"];
    [eventDictionary setValue:body forKey:@"body"];
  }

  if (userInfo[@"message_uid"]) {
    NSString *messageUid = [userInfo objectForKey:@"message_uid"];
    [eventDictionary setValue:messageUid forKey:@"notificationId"];
  }

  if (userInfo[@"client_app_data"][@"deep_link"]) {
    NSString *deepLink = [[userInfo objectForKey:@"client_app_data"] objectForKey:@"deep_link"];
    [eventDictionary setValue:deepLink forKey:@"deepLink"];
  }
  NSLog(@"---------->>>>>>>>>>>Event Dictionary ✅: %@", eventDictionary);

  return eventDictionary;
}

- (NSString *)stringWithDeviceToken:(NSData*) deviceToken {
  const char *data = [deviceToken bytes];
  NSMutableString *token = [NSMutableString string];

  for (NSUInteger i = 0; i < [deviceToken length]; i++) {
    [token appendFormat:@"%02.2hhX", data[i]];
  }

  return [token copy];
}

- (void)didRegisterDeviceWithDeviceId:(NSString *)deviceId error:(NSError *)error {
  if (error == NULL) {
    NSLog(@"---------->>>>>>>>>>>Register Device with deviceID success ✅: %@", deviceId);

    [[NSUserDefaults standardUserDefaults] setObject: deviceId forKey:@"VibesDeviceID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [PushEventEmitter sendRegisterDeviceEvent];

  } else {
    NSLog(@"---------->>>>>>>>>>>didRegisterDevice Error Device with deviceID--->>>: %@", error);
  }
}

- (void)didRegisterPushWithError:(NSError*) error {
  if (error == NULL) {
    NSLog(@"---------->>>>>>>>>>>Register Push success ✅:");
  } else {
    NSLog(@"---------->>>>>>>>>>>didRegisterPush Error register device: %@", error);
  }
  [[NSNotificationCenter defaultCenter]
          postNotificationName:[VibesClient didRegisterPushNSNotifName]
          object:error];
}

- (void) didRegisterDevice:(NSString*) deviceId withError: (NSError*) error
{
  if (error == NULL) {
    NSLog(@"---------->>>>>>>>>>>Register Device success ✅:");
  } else {
    NSLog(@"didRegisterDevice Error register device: %@", error);
  }
}

- (void)didAssociatePersonWithError:(NSError *)error
{
  if (error == NULL) {
    NSLog(@"---------->>>>>>>>>>>Associate Person Success ✅:");
  } else {
    NSLog(@"---------->>>>>>>>>>>Associate Person Error: %@", error);
  }
  [[NSNotificationCenter defaultCenter]
          postNotificationName:[VibesClient didAssociatePersonNSNotifName]
          object:error];
}

-(void)didUnregisterPushWithError:(NSError *)error
{
  if (error == NULL) {
    NSLog(@"---------->>>>>>>>>>>Unregister Push Success ✅:");
  } else {
    NSLog(@"---------->>>>>>>>>>>Unregister Push Error: %@", error);
  }
  [[NSNotificationCenter defaultCenter]
          postNotificationName:[VibesClient didUnRegisterPushNSNotifName]
          object:error];
}

@end
