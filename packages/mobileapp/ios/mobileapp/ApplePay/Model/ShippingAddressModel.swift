// 9fbef606107a605d69c0edbcd8029e5d 
//
//  ShippingAddressModel.swift
//  mobileapp
//
//  Created by Rakesh Lohan on 05/10/20.
//  Copyright © 2020 Facebook. All rights reserved.
//

import Foundation
import PassKit

class ShippingAddressModel {
  func getContact(shippingContact: Dictionary<String, Any>) ->  PKContact{

    let contact = PKContact()
    if let givenName = shippingContact[Constants.GivenName] as? String, let familyName = shippingContact[Constants.FamilyName] as? String, let administrativeArea = shippingContact[Constants.AdministrativeArea] as? String, let postalCode = shippingContact[Constants.PostalCode] as? String, let locality = shippingContact[Constants.Locality] as? String, let emailAddress = shippingContact[Constants.EmailAddress] as? String, let phoneNumber = shippingContact[Constants.PhoneNumber] as? String, let addressLines = shippingContact[Constants.AddressLines] as? [String], let countryCode = shippingContact[Constants.CountryCode] as? String{

      var name = PersonNameComponents()
      name.givenName = givenName
      name.familyName = familyName
      contact.name = name

      let address = CNMutablePostalAddress()

      var addressInfo = ""
      for str in addressLines {
        if addressLines.last == str {
          addressInfo.append(str)
        }else {
          addressInfo.append(str)
          addressInfo.append("\n")
        }
      }
      address.street = addressInfo
      address.city = locality
      address.state = administrativeArea
      address.postalCode = postalCode
      address.isoCountryCode = countryCode
      contact.postalAddress = address
      contact.phoneNumber = CNPhoneNumber.init(stringValue: phoneNumber)
      contact.emailAddress = emailAddress
    }
    return contact
  }

  func getRequiredShippingContactFields(field: [String]) ->  Set<PKContactField>{

    var contactFields = Set<PKContactField>()

    for str in field {
      switch str{
      case Constants.RequiredContactFieldsName:
        contactFields.insert(.name)

      case Constants.RequiredContactFieldsPhone:
        contactFields.insert(.phoneNumber)

      case Constants.RequiredContactFieldsPostalAddress:
        contactFields.insert(.postalAddress)

      case Constants.RequiredContactFieldsEmail:
        contactFields.insert(.emailAddress)

      default:
        contactFields.insert(.postalAddress)
      }
    }

    return contactFields
  }

  func getShippingMethod(methods: [Dictionary<String, Any>]) -> [PKShippingMethod] {
    var summaryMethods = [PKShippingMethod]()

    for method in methods {

      if let label = method[Constants.Label] as? String, let amount = method[Constants.Amount] as? String, let detail = method[Constants.Detail] as? String, let identifier = method[Constants.Identifier] as? String{
        let method = PKShippingMethod.init(label: label, amount: NSDecimalNumber.init(string: amount), type: .final)
        method.detail = detail
        method.identifier = identifier
        summaryMethods.append(method)
      }
    }
    return summaryMethods
  }

  //Extract address payload from contact
  func getAddressPayload(shippingContact: PKContact?) -> [String: Any]? {

    if let contact = shippingContact{
      var dic = [String: Any]()
      if let givenName = contact.name?.givenName {
        dic[Constants.GivenName] = givenName
      }

      if let familyName = contact.name?.familyName {
        dic[Constants.FamilyName] = familyName
      }

      if let administrativeArea = contact.postalAddress?.state {
        dic[Constants.AdministrativeArea] = administrativeArea
      }

      if let postalCode = contact.postalAddress?.postalCode {
        dic[Constants.PostalCode] = postalCode
      }

      if let locality = contact.postalAddress?.city {
        dic[Constants.Locality] = locality
      }

      if let countryCode = contact.postalAddress?.isoCountryCode{
        dic[Constants.CountryCode] = countryCode
      }

      if let country = contact.postalAddress?.country{
        dic[Constants.Country] = country
      }

      if let emailAddress = contact.emailAddress {
        dic[Constants.EmailAddress] = emailAddress
      }

      if let phoneNumber = contact.phoneNumber?.stringValue{
        dic[Constants.PhoneNumber] = phoneNumber
      }

      if let addressLines = contact.postalAddress?.street.components(separatedBy: "\n") {
        dic[Constants.AddressLines] = addressLines
      }
      debugPrint("Shipping contact send payload:- ", dic)
      return dic
    }
    return nil
  }
}
