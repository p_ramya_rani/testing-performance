// 9fbef606107a605d69c0edbcd8029e5d 
//
//  Product.swift
//  mobileapp
//
//  Created by Rakesh Lohan on 25/09/20.
//  Copyright © 2020 Facebook. All rights reserved.
//

import Foundation
import PassKit

class Product {

 func getSummaryItems(items: [Dictionary<String, Any>]) -> [PKPaymentSummaryItem] {
   var summaryItems = [PKPaymentSummaryItem]()

   for item in items {

     if let label = item[Constants.Label] as? String, let amount = item[Constants.Amount] as? String, let type = item[Constants.ItemType] as? String{
       if type == Constants.ItemTypeFinal{
         summaryItems.append(PKPaymentSummaryItem.init(label: label, amount: NSDecimalNumber.init(string: amount), type: PKPaymentSummaryItemType.final))
       }else {
         summaryItems.append(PKPaymentSummaryItem.init(label: label, amount: NSDecimalNumber.init(string: amount), type: PKPaymentSummaryItemType.pending))
       }
     }else if let amount = item[Constants.Amount] as? String, let type = item[Constants.ItemType] as? String{
       if type == Constants.ItemTypeFinal{
         summaryItems.append(PKPaymentSummaryItem.init(label: "", amount: NSDecimalNumber.init(string: amount), type: PKPaymentSummaryItemType.final))
       }else {
         summaryItems.append(PKPaymentSummaryItem.init(label: "", amount: NSDecimalNumber.init(string: amount), type: PKPaymentSummaryItemType.pending))
       }
     }
   }
   return summaryItems
 }
}
