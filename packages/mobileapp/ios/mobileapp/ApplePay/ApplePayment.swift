// 9fbef606107a605d69c0edbcd8029e5d 
//
//  ApplePayment.swift
//  mobileapp
//
//  Created by Rakesh Lohan on 20/09/20.
//  Copyright © 2020 Facebook. All rights reserved.
//

import Foundation
import PassKit

@objc(ApplePayment)class ApplePayment: RCTEventEmitter, PKPaymentAuthorizationViewControllerDelegate {

  /// Variables
  var braintreeClient: BTAPIClient!
  var paymentResolve: RCTResponseSenderBlock?
  var applePayClient: BTApplePayClient!
  var isCancelPayment = true

  var shippingMethodCallback: ((PKPaymentRequestShippingMethodUpdate) -> Void)?
  var shippingContactCallback: ((PKPaymentRequestShippingContactUpdate) -> Void)?

  var paymentAuthorizationCallback: ((PKPaymentAuthorizationResult) -> Void)?
  var authorizedPayment: PKPayment?

  /// Function used for Braintree initialize
  ///
  /// - Parameter client_authorization: client_authorization key
  @objc func initialize(_ client_authorization: String!, resolve: RCTResponseSenderBlock) {
    self.braintreeClient = BTAPIClient(authorization: client_authorization)
    if let client = self.braintreeClient {
      self.applePayClient = BTApplePayClient(apiClient: client)
      resolve(["\(true)"])
      return
    }
    resolve(["\(false)"])
  }

  override func startObserving() {
    debugPrint("****************** Observing started *****************")
  }

  override func stopObserving() {
    debugPrint("****************** Observing stoped *****************")
  }

  /// Function used to check isDeviceSupportApplePay
  ///
  /// - Parameter resolve: call back status
  @objc func isDeviceSupportApplePay(_ resolve: RCTResponseSenderBlock)  {
    if PKPaymentAuthorizationViewController.canMakePayments(){
      resolve(["\(true)"])
    }else {
      resolve(["\(false)"])
    }
  }

  /// Function used to check isApplePayAvailable
  ///
  /// - Parameter resolve: call back status
  @objc func isApplePayAvailable(_ resolve: RCTResponseSenderBlock)  {
    if PKPaymentAuthorizationViewController.canMakePayments(usingNetworks: [PKPaymentNetwork.visa, PKPaymentNetwork.masterCard, PKPaymentNetwork.amex, PKPaymentNetwork.discover]) {
      resolve(["\(true)"])
    }else {
      resolve(["\(false)"])
    }
  }

  override func supportedEvents() -> [String]! {
    return [Constants.EventShippingMethod, Constants.EventShippingContact, Constants.EventCancelPayment, Constants.EventShippingContactPaymentAuthorize]
  }

  func getSummeryItems(requestPayload: Dictionary<String, Any>) -> [PKPaymentSummaryItem]{
    var summeryItems = [PKPaymentSummaryItem]()

    if let itemsArray = requestPayload[Constants.LineItems] as? [Dictionary<String, Any>] {
      summeryItems = Product().getSummaryItems(items: itemsArray)
    }

    if let totalItem = requestPayload[Constants.Total] as? Dictionary<String, Any> {
      if let label = totalItem[Constants.Label] as? String, let amount = totalItem[Constants.Amount] as? String, let type = totalItem[Constants.ItemType] as? String{
        if type == Constants.ItemTypeFinal{
          summeryItems.append(PKPaymentSummaryItem.init(label: label, amount: NSDecimalNumber.init(string: amount), type: PKPaymentSummaryItemType.final))
        }
      }
    }

    return summeryItems
  }

  //MARK:- Listener Callback
  @objc func shippingMethodValidation(_ requestPayload: Dictionary<String, Any>){
    debugPrint("Shipping Method Validation:-- ", requestPayload)

    let summeryItems = self.getSummeryItems(requestPayload: requestPayload)

    if summeryItems.count > 0 {
      if let completion = self.shippingMethodCallback {
        completion(PKPaymentRequestShippingMethodUpdate.init(paymentSummaryItems: summeryItems))
        self.shippingMethodCallback = nil
      }
    }
  }

  @objc func shippingContactValidation(_ requestPayload: Dictionary<String, Any>){
    debugPrint("Shipping Contact Validation:-- ", requestPayload)

    if let completion = self.shippingContactCallback {

      let summeryItems = self.getSummeryItems(requestPayload: requestPayload)

      var shippingMethods = [PKShippingMethod]()

      if let methodArray = requestPayload[Constants.ShippingMethods] as? [Dictionary<String, Any>] {
        shippingMethods.append(contentsOf: ShippingAddressModel().getShippingMethod(methods: methodArray))
      }

      var shippingError = [Error]()
      if let errorArray = requestPayload[Constants.Errors] as? [Dictionary<String, Any>]{
        for error in errorArray {
          if let label = error[Constants.Label] as? String, let field = error[Constants.Field] as? String {
            shippingError.append(PKPaymentRequest.paymentShippingAddressInvalidError(withKey: field, localizedDescription: label))
          }
        }
      }

      completion(PKPaymentRequestShippingContactUpdate.init(errors: shippingError , paymentSummaryItems: summeryItems, shippingMethods: shippingMethods))

      // completion(PKPaymentRequestShippingContactUpdate.init(errors: [PKPaymentRequest.paymentShippingAddressInvalidError(withKey: "country", localizedDescription: "nonEligibleCountryErrorMessage")] , paymentSummaryItems: [PKPaymentSummaryItem(label: "Razeware", amount: 100.0)], shippingMethods: [PKShippingMethod.init(label: "T", amount: 10.0)]))

      self.shippingContactCallback = nil
    }
  }


  func setupPaymentRequest(_ payload: Dictionary<String, Any>!, completion: @escaping (PKPaymentRequest?, Error?) -> Void) {
    // You can use the following helper method to create a PKPaymentRequest which will set the `countryCode`,
    // `currencyCode`, `merchantIdentifier`, and `supportedNetworks` properties.
    // You can also create the PKPaymentRequest manually. Be aware that you'll need to keep these in
    // sync with the gateway settings if you go this route.
    applePayClient.paymentRequest { (paymentRequest, error) in
      guard let paymentRequest = paymentRequest else {
        completion(nil, error)
        return
      }

      // We recommend collecting billing address information, at minimum
      // billing postal code, and passing that billing postal code with all
      // Apple Pay transactions as a best practice.

      //Set Merchant Capabilities
      if let capabilities = payload[Constants.MerchantCapabilities] as? Array<String>, capabilities.count > 0 {
        for str in capabilities{
          switch str {
          case Constants.Supports3DS:
            paymentRequest.merchantCapabilities = PKMerchantCapability.capability3DS

          default:
            paymentRequest.merchantCapabilities = PKMerchantCapability.capability3DS
          }
        }
      }

      //Set Supported Network
      if let networks = payload[Constants.SupportedNetworks] as? Array<String>, networks.count > 0 {
        var supportedNetworks = [PKPaymentNetwork]()
        for str in networks{
          switch str {
          case Constants.Visa:
            supportedNetworks.append(PKPaymentNetwork.visa)

          case Constants.MasterCard:
            supportedNetworks.append(PKPaymentNetwork.masterCard)

          case Constants.Amex:
            supportedNetworks.append(PKPaymentNetwork.amex)

          case Constants.Discover:
            supportedNetworks.append(PKPaymentNetwork.discover)

          default:
            supportedNetworks = [PKPaymentNetwork.amex, PKPaymentNetwork.visa, PKPaymentNetwork.masterCard]
          }
        }
        paymentRequest.supportedNetworks = supportedNetworks
      }

      //Set payment Shipping Type
      if let shipping = payload[Constants.ShippingType] as? Array<String>, shipping.count > 0 {
        for str in shipping{
          switch str {
          case Constants.Shipping:
            paymentRequest.shippingType = .shipping

          case Constants.Delivery:
            paymentRequest.shippingType = .delivery

          case Constants.StorePickup:
            paymentRequest.shippingType = .storePickup

          case Constants.ServicePickup:
            paymentRequest.shippingType = .servicePickup

          default:
            paymentRequest.shippingType = .shipping
          }
        }
      }

      //Merchant Id
      if let merchantIdentifier = payload[Constants.MerchantId] as? String {
        paymentRequest.merchantIdentifier = merchantIdentifier
      }

      //Country Code
      if let countryCode = payload[Constants.CountryCode] as? String {
        paymentRequest.countryCode = countryCode
      }

      //Currency Code
      if let currencyCode = payload[Constants.CurrencyCode] as? String {
        paymentRequest.currencyCode = currencyCode
      }

      paymentRequest.paymentSummaryItems = self.getSummeryItems(requestPayload: payload)

      if let shippingContactPayload = payload[Constants.ShippingContact] as? Dictionary<String, Any> {
        paymentRequest.shippingContact = ShippingAddressModel().getContact(shippingContact: shippingContactPayload)
      }

      if let shippingContactRequiredField = payload[Constants.RequiredShippingContactFields] as? [String] {
        paymentRequest.requiredShippingContactFields = ShippingAddressModel().getRequiredShippingContactFields(field: shippingContactRequiredField)
      }

      if let billingContactPayload = payload[Constants.BillingContact] as? Dictionary<String, Any> {
        paymentRequest.billingContact = ShippingAddressModel().getContact(shippingContact: billingContactPayload)
      }

      if let billingContactRequiredField = payload[Constants.RequiredBillingContactFields] as? [String] {
        paymentRequest.requiredBillingContactFields = ShippingAddressModel().getRequiredShippingContactFields(field: billingContactRequiredField)
      }

      if let methodArray = payload[Constants.ShippingMethods] as? [Dictionary<String, Any>] {
        paymentRequest.shippingMethods = ShippingAddressModel().getShippingMethod(methods: methodArray)
      }

      completion(paymentRequest, nil)
    }
  }


  @objc func paymentValidation(_ requestPayload: Dictionary<String, Any>!) {
    debugPrint("Payment Validation Request Payload:- ", requestPayload)

    if let errorArray = requestPayload[Constants.Errors] as? [Dictionary<String, Any>], let completion = self.paymentAuthorizationCallback {
      var shippingError = [Error]()
      for error in errorArray {
        if let label = error[Constants.Label] as? String, let field = error[Constants.Field] as? String,  let errorType = error[Constants.ErrorType] as? String{
          if errorType == Constants.ErrorContact {
            shippingError.append(PKPaymentRequest.paymentContactInvalidError(withContactField: PKContactField(rawValue: field), localizedDescription: label))
          }else {
            shippingError.append(PKPaymentRequest.paymentShippingAddressInvalidError(withKey: field, localizedDescription: label))
          }
        }
      }
      completion(PKPaymentAuthorizationResult(status: .failure, errors: shippingError))
      self.authorizedPayment =  nil
      self.paymentAuthorizationCallback = nil
      return
    }


    if let payment = self.authorizedPayment, let completion = self.paymentAuthorizationCallback {
      // Tokenize the Apple Pay payment
      applePayClient.tokenizeApplePay(payment) {[weak self] (nonce, error) in
        if error != nil {
          // Received an error from Braintree.
          // Indicate failure via the completion callback.
          completion(PKPaymentAuthorizationResult(status: .failure, errors: nil))
          if let resolve =  self?.paymentResolve{
            resolve([error as Any, "" ])
          }
          return
        }

        // TODO: On success, send nonce to your server for processing.
        // If requested, address information is accessible in `payment` and may
        // also be sent to your server.

        // Then indicate success or failure based on the server side result of Transaction.sale
        // via the completion callback.
        // e.g. If the Transaction.sale was successful
        completion(PKPaymentAuthorizationResult(status: .success, errors: nil))
        self?.isCancelPayment = false
        if let nonceToken = nonce?.nonce, let resolve =  self?.paymentResolve {
          resolve(["", nonceToken ])
        }
      }
    }
  }

  //MARK:- Action

  @objc func tappedApplePay(_ requestPayload: Dictionary<String, Any>!, resolve: @escaping (RCTResponseSenderBlock)) {
    debugPrint("Request Payload:- ", requestPayload)

    self.setupPaymentRequest(requestPayload) {(paymentRequest, error) in
      guard error == nil else {
        // Handle error
        resolve([error!, ""])
        return
      }
      self.paymentResolve = resolve
      // Example: Promote PKPaymentAuthorizationViewController to optional so that we can verify
      // that our paymentRequest is valid. Otherwise, an invalid paymentRequest would crash our app.
      if let vc = PKPaymentAuthorizationViewController(paymentRequest: paymentRequest!)
        as PKPaymentAuthorizationViewController? {
        vc.delegate = self
        if #available(iOS 13.0, *) {
          vc.overrideUserInterfaceStyle = .dark
        }
        if let viewController = UIApplication.shared.keyWindow!.rootViewController {
          DispatchQueue.main.async {
            viewController.present(vc, animated: true, completion: nil)
          }
        }else if let resolve =  self.paymentResolve{
          resolve([Constants.InvalidRequest, ""])
          self.paymentResolve = nil
        }
      } else {
        print(Constants.InvalidRequest)
        if let resolve =  self.paymentResolve {
          resolve([Constants.InvalidRequest, ""])
          self.paymentResolve = nil
        }
      }
    }
  }

  func sendAuthorizePaymentShippingContact(contact: [String: Any]?){
    self.sendEvent(withName: Constants.EventShippingContactPaymentAuthorize, body: ["method": contact])
  }

  //MARK:- Delegate
  //PKPaymentAuthorizationViewControllerDelegate ios 11
  func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController,
                                          didAuthorizePayment payment: PKPayment,
                                          handler completion: @escaping (PKPaymentAuthorizationResult) -> Void) {


    self.sendAuthorizePaymentShippingContact(contact: ShippingAddressModel().getAddressPayload(shippingContact: payment.shippingContact))
    self.authorizedPayment = payment
    self.paymentAuthorizationCallback = completion

    //    completion(PKPaymentAuthorizationResult(status: .failure, errors: [PKPaymentRequest.paymentContactInvalidError(withContactField: PKContactField(rawValue: "name"), localizedDescription: "Name not provided")]))

    //    // Tokenize the Apple Pay payment
    //    applePayClient.tokenizeApplePay(payment) {[weak self] (nonce, error) in
    //      if error != nil {
    //        // Received an error from Braintree.
    //        // Indicate failure via the completion callback.
    //        completion(PKPaymentAuthorizationResult(status: .failure, errors: nil))
    //        self?.paymentResolve([error as Any, ShippingAddressModel().getAddressPayload(shippingContact: payment.shippingContact) as Any, "" ])
    //        return
    //      }
    //
    //      // TODO: On success, send nonce to your server for processing.
    //      // If requested, address information is accessible in `payment` and may
    //      // also be sent to your server.
    //
    //      // Then indicate success or failure based on the server side result of Transaction.sale
    //      // via the completion callback.
    //      // e.g. If the Transaction.sale was successful
    //      completion(PKPaymentAuthorizationResult(status: .success, errors: nil))
    //      self?.isCancelPayment = false
    //      if let nonceToken = nonce?.nonce {
    //        self?.paymentResolve(["", ShippingAddressModel().getAddressPayload(shippingContact: payment.shippingContact) as Any, nonceToken ])
    //      }
    //    }
  }

  func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
    // Apple Payment finished

    if self.isCancelPayment {
      self.sendEvent(withName: Constants.EventCancelPayment, body: ["cancel": ""])
    }
    controller.dismiss(animated: true)
  }

  //    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didSelect paymentMethod: PKPaymentMethod, handler completion: @escaping (PKPaymentRequestPaymentMethodUpdate) -> Void) {
  //      debugPrint("Payment Method:-- ", paymentMethod)
  //    }

  func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didSelect shippingMethod: PKShippingMethod, handler completion: @escaping (PKPaymentRequestShippingMethodUpdate) -> Void) {
    debugPrint("Shipping Method:-- ", shippingMethod)
    if let detail = shippingMethod.detail, let identifier = shippingMethod.identifier {
      self.sendEvent(withName: Constants.EventShippingMethod, body: ["method": [Constants.Label: shippingMethod.label, Constants.Amount: shippingMethod.amount, Constants.Detail: detail, Constants.Identifier: identifier]])
      self.shippingMethodCallback = completion
    }
  }

  func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didSelectShippingContact contact: PKContact, handler completion: @escaping (PKPaymentRequestShippingContactUpdate) -> Void) {
    debugPrint("Shipping Contact:-- ", contact)
    self.sendEvent(withName: Constants.EventShippingContact, body: ["method": ShippingAddressModel().getAddressPayload(shippingContact: contact)])
    self.shippingContactCallback = completion
  }

  /// Method to handle mainque
  ///
  /// - Returns: status
  @objc override static func requiresMainQueueSetup() -> Bool {
    return false
  }
}

