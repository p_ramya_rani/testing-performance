// 9fbef606107a605d69c0edbcd8029e5d 
//
//  ApplePayment.m
//  mobileapp
//
//  Created by Rakesh Lohan on 20/09/20.
//  Copyright © 2020 Facebook. All rights reserved.
//

#import "React/RCTBridgeModule.h"
#import "React/RCTEventEmitter.h"

/**
 Bridge handled to connect ApplePay form React native
 */
@interface RCT_EXTERN_MODULE(ApplePayment, RCTEventEmitter)
RCT_EXTERN_METHOD(supportedEvents)
RCT_EXTERN_METHOD(initialize:(NSString *)client_authorization resolve:(RCTResponseSenderBlock)resolve)
RCT_EXTERN_METHOD(isDeviceSupportApplePay:(RCTResponseSenderBlock)resolve)
RCT_EXTERN_METHOD(isApplePayAvailable:(RCTResponseSenderBlock)resolve)
RCT_EXTERN_METHOD(tappedApplePay:(NSDictionary *)requestPayload resolve:(RCTResponseSenderBlock)resolve)
RCT_EXTERN_METHOD(shippingMethodValidation:(NSDictionary *)requestPayload)
RCT_EXTERN_METHOD(shippingContactValidation:(NSDictionary *)requestPayload)
RCT_EXTERN_METHOD(paymentValidation:(NSDictionary *)requestPayload)
@end

