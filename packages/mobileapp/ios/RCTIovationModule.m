// RCTIovationModule.m
#import "RCTIovationModule.h"
@import FraudForce;
@import UIKit;

@implementation RCTIovationModule

// Without passing in a name this will export the native module name as the Objective-C class name with “RCT” removed
RCT_EXPORT_MODULE();

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [FraudForce start];
}


// This is an asynchronous macro, for synchronous macro we can use RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD
RCT_EXPORT_METHOD(getBlackBoxData:(RCTResponseSenderBlock)successCallback
                  errorCallback: (RCTResponseSenderBlock)errorCallback)
{
  @try {
    NSString *blackbox = [FraudForce blackbox];
    successCallback(@[blackbox]);
  }
  @catch ( NSException *e ) {
    errorCallback(@[e]);
  }
}

@end
