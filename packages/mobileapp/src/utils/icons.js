// 9fbef606107a605d69c0edbcd8029e5d
import homeInactive from '../assets/images/icon-home-normal.png';
import shopInactive from '../assets/images/icon-shop-normal.png';
import accountInactive from '../assets/images/icon-user-normal.png';
import walletInactive from '../assets/images/icon-wallet-normal.png';
import closeIcon from '../assets/images/close.png';
import branchSwitchCloseIcon from '../assets/images/brand-close.png';

import tcpHomeActive from '../assets/images/tcp/icon-home-active.png';
import tcpBrandLogo from '../assets/images/tcp/brand-logo.png';
import tcpShopActive from '../assets/images/tcp/icon-shop-active.png';
import tcpAccountActive from '../assets/images/tcp/icon-user-active.png';
import tcpWalletActive from '../assets/images/tcp/icon-wallet-active.png';
import tcpSplashImage from '../assets/images/tcp/tcpLaunchImage.png';
import tcpPeekABooImage from '../assets/images/tcp/tcp-inactive-toggle.png';
import tcpPeekABooImageActive from '../assets/images/tcp/tcp-active-toggle.png';

import gymboreeHomeActive from '../assets/images/gymboree/icon-home-active.png';
import gymboreeBrandLogo from '../assets/images/gymboree/brand-logo.png';
import gymboreeShopActive from '../assets/images/gymboree/icon-shop-active.png';
import gymboreeAccountActive from '../assets/images/gymboree/icon-user-active.png';
import gymboreeWalletActive from '../assets/images/gymboree/icon-wallet-active.png';
import gymboreeSplashImage from '../assets/images/gymboree/gymboreeLaunchImage.png';
import gymboreeCrossBrandLogo from '../assets/images/gymboree/gymboreeCrossBrandLogo.png';
import gymboreePeekABooImage from '../assets/images/gymboree/gym-toggle.png';
import gymboreePeekABooImageActive from '../assets/images/gymboree/gym-active-toggle.png';
import l1TopNavFallback from '../assets/images/gymboree/l1-top-nav-fallback.png';
import l1NavFallback from '../assets/images/gymboree/l1-nav-fallback.png';

export default {
  homeInactive,
  shopInactive,
  accountInactive,
  closeIcon,
  branchSwitchCloseIcon,
  walletInactive,
  gymboreeCrossBrandLogo,
  tcp: {
    homeActive: tcpHomeActive,
    brandLogo: tcpBrandLogo,
    shopActive: tcpShopActive,
    accountActive: tcpAccountActive,
    walletActive: tcpWalletActive,
    splash: tcpSplashImage,
    peekABoo: tcpPeekABooImage,
    peekABooActive: tcpPeekABooImageActive,
  },
  gymboree: {
    homeActive: gymboreeHomeActive,
    brandLogo: gymboreeBrandLogo,
    shopActive: gymboreeShopActive,
    accountActive: gymboreeAccountActive,
    walletActive: gymboreeWalletActive,
    splash: gymboreeSplashImage,
    peekABoo: gymboreePeekABooImage,
    peekABooActive: gymboreePeekABooImageActive,
    l1TopNavFallback,
    l1NavFallback,
  },
};
