import {
  TIER1,
  TIER2,
  TIER3,
  TIER2NETWORKSPEED,
  TIER3NETWORKSPEED,
} from '@tcp/core/src/constants/tiering.constants';

const convertByte = bytes => {
  if (bytes === 0) return '0 Byte';
  return Math.round(bytes / 1024 ** 3, 2);
};

function forNonIOS(memory, networkSpeed) {
  let deviceType;
  if (TIER3NETWORKSPEED.includes(networkSpeed)) {
    deviceType = TIER3;
  } else if (TIER2NETWORKSPEED.includes(networkSpeed)) {
    if (memory < 4) {
      deviceType = TIER3;
    } else {
      deviceType = TIER2;
    }
  } else if (memory < 4) {
    deviceType = TIER3;
  } else if (memory < 6) {
    deviceType = TIER2;
  } else {
    deviceType = TIER1;
  }
  return deviceType;
}
function forIOS(memory, networkSpeed) {
  let deviceType;
  if (TIER3NETWORKSPEED.includes(networkSpeed)) {
    deviceType = TIER3;
  } else if (TIER2NETWORKSPEED.includes(networkSpeed)) {
    if (memory < 4) {
      deviceType = TIER3;
    } else {
      deviceType = TIER2;
    }
  } else if (memory < 4) {
    deviceType = TIER2;
  } else {
    deviceType = TIER1;
  }
  return deviceType;
}

export default function setDeviceTier(memoryTotal, Platform, network) {
  const memory = convertByte(memoryTotal);
  const networkSpeed =
    (network && network.type === 'celluler' && network.cellularGeneration) || '4g';
  if (Platform.OS === 'ios') {
    return forIOS(memory, networkSpeed);
  }
  return forNonIOS(memory, networkSpeed);
}

export function getDeviceTier(memory, Platform, networkSpeed) {
  const parseMemory = parseInt(memory, 10);
  return setDeviceTier(parseMemory, Platform, networkSpeed) || TIER1;
}

export function isTierDevice(memory, Platform, networkSpeed) {
  return getDeviceTier(memory, Platform, networkSpeed);
}
