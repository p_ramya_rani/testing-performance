// 9fbef606107a605d69c0edbcd8029e5d
import superagent from 'superagent';
import 'react-native-get-random-values';
import {v4 as uuidv4} from 'uuid';
import {getAPIConfig, getValueFromAsyncStorage} from '@tcp/core/src/utils';
import logger from '@tcp/core/src/utils/loggerInstance';
import {SBP_USER_EMAIL} from '../../components/features/shopByProfile/ShopByProfile.constants';
import {md5} from '../../../../core/src/components/features/browse/ProductListing/molecules/ProductReviews/encoding';

const secureSBPParentId = async parentId => {
  try {
    const userEmail = await getValueFromAsyncStorage(SBP_USER_EMAIL);
    const combinedParentIdUsername = `${userEmail}${parentId}`;
    const lowerCasedCombinedParentIdUsername = combinedParentIdUsername.toLowerCase();
    return md5(lowerCasedCombinedParentIdUsername);
  } catch (error) {
    logger.error(error);
  }
  return '';
};

export const getAllChildProfiles = async parentId => {
  const apiConfigObj = getAPIConfig();
  const {sbpEndPoint} = apiConfigObj;
  const GET_PROFILE_URL = `${sbpEndPoint}/getprofile`;
  try {
    const hashedParentId = await secureSBPParentId(parentId);
    const response = await superagent.get(
      `${GET_PROFILE_URL}?parent_id=${hashedParentId}&${new Date().getTime()}`,
    );
    return response.body.data;
  } catch (error) {
    logger.error(error);
    return [];
  }
};

function getRandomId() {
  return uuidv4();
}

async function createProfile(newChildProfile) {
  const parentId = newChildProfile.parent_id;
  const hashedParentId = await secureSBPParentId(parentId);
  const newChildProfileObj = newChildProfile;
  newChildProfileObj.parent_id = hashedParentId;
  const apiConfigObj = getAPIConfig();
  const {sbpEndPoint} = apiConfigObj;
  const CREATE_PROFILE_URL = `${sbpEndPoint}/createprofile`;
  try {
    await superagent.post(CREATE_PROFILE_URL).send(newChildProfileObj);
  } catch (error) {
    logger.error(error);
  }
  return newChildProfileObj;
}

const updateChildProfile = async (profile, isIncomplete, userId) => {
  const profileData = {
    parent_id: profile.parent_id,
    id: profile.id,
    theme: profile.theme,
    size: profile.size,
    relation: profile.relation,
    gender: profile.gender,
    name: profile.name,
    fit: profile.fit,
    month: profile.month,
    shoe: profile.shoe,
    year: profile.year,
    interests: profile.interests,
    styles: profile.styles,
    favorite_colors: profile.favorite_colors,
  };

  if (isIncomplete || !profileData.parent_id) {
    const hashedParentId = await secureSBPParentId(profileData.parent_id || userId);
    profileData.parent_id = hashedParentId;
  }

  const apiConfigObj = getAPIConfig();
  const {sbpEndPoint} = apiConfigObj;
  const UPDATE_PROFILE_URL = `${sbpEndPoint}/updateprofile`;
  try {
    await superagent.post(`${UPDATE_PROFILE_URL}`).send(profileData);
    return profileData;
  } catch (error) {
    logger.error(error);
    return [];
  }
};

export const insertChildProfile = async (profile, userId, isPregenerateProfile = false) => {
  if (profile.isEdit) {
    return updateChildProfile(profile, false, userId);
  }
  const {isIncomplete} = profile;
  const randomProfId = getRandomId();
  const profileData = profile;
  if (profileData.month === undefined) {
    profileData.month = 'August';
  }
  if (profileData.year === undefined) {
    profileData.year = '2015';
  }
  if (isIncomplete) {
    if (!profile.id) {
      profileData.id = randomProfId;
    } else {
      profileData.parent_id = userId;
      return updateChildProfile(profileData, isIncomplete, userId);
    }
  }
  profileData.parent_id = userId;
  profileData.id = randomProfId;
  profileData.is_pregenerate_profile = isPregenerateProfile;
  return createProfile(profileData);
};

export const removeChildProfile = async (profId, userId) => {
  if (!userId) return;
  const hashedParentId = await secureSBPParentId(userId);
  const profileId = {
    id: profId,
    parent_id: hashedParentId,
  };
  const apiConfigObj = getAPIConfig();
  const {sbpEndPoint} = apiConfigObj;
  const DELETE_PROFILE_URL = `${sbpEndPoint}/deleteprofile`;
  try {
    await superagent.post(DELETE_PROFILE_URL).send(profileId);
  } catch (error) {
    logger.error(error);
  }
};
