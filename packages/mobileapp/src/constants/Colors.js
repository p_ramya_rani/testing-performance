// 9fbef606107a605d69c0edbcd8029e5d
const tintColor = '#2f95dc';

export default {
  tintColor,
  tabIconDefault: '#ccc',
  tabIconSelected: tintColor,
  tabBar: '#fefefe',
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff',
  shopMenuBackground: 'rgb(243, 243, 243)',
  transparent: 'transparent',
};
