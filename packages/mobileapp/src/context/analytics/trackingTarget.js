// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable no-param-reassign */
/* eslint-disable complexity */
/* eslint-disable sonarjs/cognitive-complexity */
import analytics from '@react-native-firebase/analytics';
/* Adobe analytics removed 28/01/2021 */
// import {ACPCore} from '@adobe/react-native-acpcore';
import {
  getValueFromAsyncStorage,
  removeValueFromAsyncStorage,
  setValueInAsyncStorage,
} from '@tcp/core/src/utils';
import logger from '@tcp/core/src/utils/loggerInstance';
import {transformPageEvent, transformClickEvent} from '../transformEvents';

function commonLoggingEventWithProps(transformedEvents, contextData) {
  try {
    Object.keys(transformedEvents.props).map((keyName) => {
      if (transformedEvents.props[keyName] === '1' && keyName !== '') {
        if (transformedEvents.props.productsEvents) {
          delete transformedEvents.props.productsEvents;
        }

        delete transformedEvents.props.productsData;
        analytics().logEvent(keyName, {...transformedEvents.props, ...contextData});
      }
      return 0;
    });
  } catch (err) {
    logger.error('Error: Analytics:commonLoggingEventWithProps', err);
  }
}

function commonLoggingEventWithoutProps(transformedEvents, contextData) {
  try {
    Object.keys(transformedEvents.props).map((keyName) => {
      if (transformedEvents.props[keyName] === '1' && keyName !== '') {
        analytics().logEvent(keyName, {...contextData});
      }
      return 0;
    });
  } catch (err) {
    logger.error('Error: Analytics:commonLoggingEventWithoutProps', err);
  }
}

async function pageViewTypes(eventName, currentScreen, transformedEvents, contextData) {
  try {
    delete transformedEvents.props[''];
    switch (currentScreen) {
      case 'home_page':
      case 'storePageViews_e96':
      case 'internalCampaignImpressions_e80':
        analytics().logEvent(currentScreen, {...transformedEvents.props, ...contextData});
        break;

      case 'searchDetail_e20':
      case 'searchDetail_e21':
        delete transformedEvents.props[''];
        analytics().logEvent(currentScreen, {...transformedEvents.props});
        commonLoggingEventWithProps(transformedEvents);
        break;

      case 'productimpression':
        commonLoggingEventWithProps(transformedEvents, contextData);
        analytics().logViewItemList({
          items: transformedEvents.props.productsData,
        });
        break;

      case 'bagPage':
        analytics().logViewCart({
          value: 100,
          currency: 'USD',
          items: transformedEvents.props.productsData,
        });
        commonLoggingEventWithProps(transformedEvents);
        break;

      case 'billing':
        analytics().logAddPaymentInfo({
          items: transformedEvents.props.productsData,
        });
        commonLoggingEventWithProps(transformedEvents);
        analytics().logEvent('Added_to_Checkout', {
          Added_to_Checkout_86: '1',
        });
        break;

      case 'shipping':
        analytics().logAddShippingInfo({
          items: transformedEvents.props.productsData,
        });
        commonLoggingEventWithProps(transformedEvents);
        analytics().logEvent('Added_to_Checkout', {
          Added_to_Checkout_86: '1',
        });
        break;

      case 'product_listing':
      case 'product_listing_filters':
      case 'productListingPageViews_e91':
      case 'productListingImpressionse92':
        analytics().logViewItemList({
          items: transformedEvents.props.productsData,
        });
        delete transformedEvents.props.productsData;
        analytics().logEvent(currentScreen, {
          ...transformedEvents.props,
        });

        break;
      case 'ProductDetail':
      case 'outfitDetail':
        commonLoggingEventWithoutProps(transformedEvents);
        analytics().logViewItem({
          items: transformedEvents.props.productsData,
        });
        break;

      case 'review':
        analytics().logViewItemList({
          items: transformedEvents.props.productsData,
        });
        analytics().logEvent(currentScreen, transformedEvents.props.productsEvents[0]);
        commonLoggingEventWithProps(transformedEvents);
        break;

      case 'confirmation': {
        const purchaseParameters =
          transformedEvents.props.productsEvents.length &&
          transformedEvents.props.productsEvents.find((obj) => {
            return obj.Net_Revenue_e99;
          });
        const couponCode = await getValueFromAsyncStorage('coupon_code');
        removeValueFromAsyncStorage('coupon_code');
        analytics().logPurchase({
          value: parseFloat(purchaseParameters.Net_Revenue_e99),
          currency: 'USD',
          coupon: couponCode !== null ? couponCode : '',
          transaction_id: transformedEvents.eVars.orderId_v3,
          items: transformedEvents.props.productsData,
          shipping:
            purchaseParameters && purchaseParameters.Shipping_Amount_e78
              ? parseFloat(purchaseParameters.Shipping_Amount_e78)
              : 0,
          tax:
            purchaseParameters && purchaseParameters.Tax_Amount_e79
              ? parseFloat(purchaseParameters.Tax_Amount_e79)
              : 0,
        });
        analytics().logEvent(currentScreen, {
          ...transformedEvents.eVars,
          ...transformedEvents.props.productsEvents[0],
        });
        commonLoggingEventWithProps(transformedEvents, contextData);
        break;
      }
      case 'app_launch':
      case 'orderHelp_page':
      case 'orderRefund_successfull_page':
        analytics().logEvent(currentScreen, {...transformedEvents.props, ...contextData});
        commonLoggingEventWithProps(transformedEvents);
        break;

      default:
        analytics().logEvent(eventName, {...transformedEvents.props, ...contextData});
        break;
    }
  } catch (err) {
    logger.error('Error: Analytics:pageViewTypes', err);
  }
}

function clickTypes(eventName, name, transformedEvents, contextData) {
  try {
    switch (name) {
      case 'add_to_wishlist':
        if (transformedEvents.props.productsData && transformedEvents.props.productsData.length > 0)
          analytics().logEvent(name, transformedEvents.props.productsEvents[0]);
        break;
      case 'Wishlist_Remove_Product_e53':
      case 'Wishlist_Deletions_e54':
        if (transformedEvents.props.productsData && transformedEvents.props.productsData.length > 0)
          analytics().logEvent(name, transformedEvents.props.productsData[0]);

        break;
      case 'add_to_cart':
        commonLoggingEventWithoutProps(transformedEvents);
        analytics().logAddToCart({
          value: 100,
          currency: 'USD',
          items: transformedEvents.props.productsData,
        });
        break;
      case 'checkout_button':
        analytics().logBeginCheckout({
          value: 100,
          currency: 'USD',
          items: transformedEvents.props.productsData,
        });
        commonLoggingEventWithoutProps(transformedEvents);
        break;

      case 'Amount_Added_Save_for_Later':
      case 'Amount_to_Move_to_Bag':
      case 'continue_guest':
        commonLoggingEventWithProps(transformedEvents);
        analytics().logEvent(name, {...transformedEvents.props, ...contextData});
        break;

      case 'stylewith_interaction':
        commonLoggingEventWithProps(transformedEvents);
        analytics().logEvent({...transformedEvents.props, ...contextData});
        break;

      case 'outfitStylytics':
        commonLoggingEventWithoutProps(transformedEvents);
        analytics().logViewItem({
          items: transformedEvents.props.productsData,
        });
        break;

      case 'edit_cart':
      case 'apply_gift_card':
      case 'coupon_fails':
      case 'coupon_success':
      case 'internalCampaignImpressions_e80':
      case 'gift_options': {
        const {Coupon_Discount_Code_v6: couponCode} = transformedEvents.props;
        if (couponCode) {
          setValueInAsyncStorage('coupon_code', couponCode);
        }
        commonLoggingEventWithoutProps(transformedEvents);
        analytics().logEvent(name, {...transformedEvents.props, ...contextData});
        break;
      }
      case 'coupon_removed':
        removeValueFromAsyncStorage('coupon_code');
        break;

      default:
        analytics().logEvent(name, {...transformedEvents.props, ...contextData});
        break;
    }
  } catch (err) {
    logger.error('Error: Analytics:clickTypes', err);
  }
}

async function trackingTarget(events) {
  try {
    await analytics().setAnalyticsCollectionEnabled(true);

    events.forEach((event) => {
      if (event.hitType === 'pageView') {
        const {eventName, currentScreen, supressProps = [], contextData = {}} = event;
        const {googleMapTransformed} = transformPageEvent(currentScreen, supressProps);
        /* Adobe analytics removed 28/01/2021 */
        /*
        const {adobeMapTransformed, googleMapTransformed} = transformPageEvent(
          currentScreen,
          supressProps,
        );
        if (adobeMapTransformed) {
          ACPCore.trackAction(eventName, {
            ...adobeMapTransformed,
            ...contextData.adobe,
          });
        }
        */
        if (googleMapTransformed) {
          pageViewTypes(eventName, currentScreen, googleMapTransformed, contextData);
          if (currentScreen) {
            const {googleMapTransformed: googleMapTransformed1} = transformPageEvent(
              'abtest',
              supressProps,
            );
            if (googleMapTransformed1 && Object.keys(googleMapTransformed1.props).length)
              pageViewTypes('abtest', 'abtest', googleMapTransformed1, contextData);
          }

          analytics().setUserProperties({...googleMapTransformed.eVars});
          analytics().setUserId(googleMapTransformed.eVars.Email_Address_v31);
          analytics().setUserProperties({
            orderId_v3: 'null',
            order_stotal_v73: 'null',
            bill_zip_v68: 'null',
            Landing_Site_p37: 'null',
            Page_Type_p2: 'null',
            Coupon_Discount_Code_v6: 'null',
            Cart_Type_v86: 'null',
            Checkout_Type_v98: 'null',
            Payment_Method_v4: 'null',
          });
        }
      }
      if (event.hitType === 'click') {
        const {eventName, name, module, contextData = {}} = event;
        if (!name) {
          return;
        }
        const {googleMapTransformed} = transformClickEvent(name, module);
        /* Adobe analytics removed 28/01/2021 */
        /*
        const {adobeMapTransformed, googleMapTransformed} = transformClickEvent(name, module);
        if (adobeMapTransformed) {
          ACPCore.trackAction(eventName, {
            ...adobeMapTransformed,
            ...contextData.adobe,
          });
        }
        */
        if (googleMapTransformed) {
          analytics().setUserProperties({...googleMapTransformed.eVars});
          clickTypes(eventName, name, googleMapTransformed, contextData.google);
        }
      }
    });
  } catch (err) {
    logger.error('Error: Analytics:trackingTarget', err);
  }
}

export default trackingTarget;
