/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import AsyncStorage from '@react-native-community/async-storage';
import {dataLayer as defaultDataLayer} from '@tcp/core/src/analytics';
import {getUserLoggedInState} from '@tcp/core/src/components/features/account/User/container/User.selectors';
import {generateClickHandlerDataLayer} from './dataLayers';

const getDepartmentList = (store) => {
  return store
    .getState()
    .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'departmentList'], '');
};

const getCategoryList = (store) => {
  return store.getState().AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'categoryList'], '');
};

const getSubCategoryList = (store) => {
  return store
    .getState()
    .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'subCategoryList'], '');
};

const sortfilterTypeSeparator = '^';
const multiSortFilterSeparator = ':';

const generateSortFilterKeys = (ParamObj) => {
  let sortFilterKey = '';
  Object.keys(ParamObj).forEach((key) => {
    if (ParamObj[key] && ParamObj[key].length) {
      if (sortFilterKey) sortFilterKey += sortfilterTypeSeparator;
      sortFilterKey += `${key}=${ParamObj[key].join(multiSortFilterSeparator)}`;
    }
  });
  return sortFilterKey;
};

const getFilterParams = (store) => {
  const state = store.getState();
  const filterValues = (state.ProductListing && state.ProductListing.appliedFiltersIds) || {};
  const {
    categoryPath2_uFilter: category = [],
    v_tcpsize_uFilter: size = [],
    TCPColor_uFilter: color = [],
    v_tcpfit_unbxd_uFilter: fit = [],
    age_group_uFilter: age = [],
    gender_uFilter: gender = [],
    unbxd_price_range_uFilter: price = [],
  } = filterValues;
  return generateSortFilterKeys({category, color, size, fit, age, gender, price});
};

const sortMapper = {
  'min_offer_price desc': 'price: high to low',
  'min_offer_price asc': 'price: low to high',
  'newest_score desc': 'new arrivals',
  'favoritedcount desc': 'most favorited',
  'TCPBazaarVoiceRating desc': 'top rated',
};

const getSortParams = (store) => {
  const state = store.getState();
  const sortValues = state.ProductListing && state.ProductListing.appliedSortId;
  return sortValues ? `sort=${sortMapper[sortValues]}` : '';
};

/**
 * Analytics data layer object for property lookups.
 *
 * This function will create an object for the analytics
 * script to reference when mapping data elements. This
 * object should be assigned to the global context
 * (e.g., window) for easy access.
 *
 * @example
 * // In the app source
 * global._dataLater = create(store);
 * // In the analytics script
 * _dataLater.pageName; // "gl:home"
 *
 * @param {Object} store reference to the Redux store
 * @returns {Object} data layer
 */
export default function create(store) {
  const siteType = 'mobile app';
  const clickHandlerDataLayer = generateClickHandlerDataLayer(store);
  const apiState = store.getState().APIConfig;
  let loadingBrand = apiState && apiState.brandId;
  let giftService;

  AsyncStorage.getItem('appLoadingBrand').then((result) => {
    loadingBrand = result;
  });

  AsyncStorage.getItem('PREFERRED_GIFT_SERVICE').then((result) => {
    giftService = result;
  });

  return Object.create(defaultDataLayer, {
    ...clickHandlerDataLayer,
    listingSortList: {
      get() {
        const filterLiteral = getSortParams(store);
        return filterLiteral || 'sort=recommended';
      },
    },
    listingFilterList: {
      get() {
        const filterLiteral = getFilterParams(store);
        return filterLiteral || null;
      },
    },

    prop11Filter: {
      get() {
        const filterLiteral = getFilterParams(store);
        return filterLiteral ? 'D=c11' : null;
      },
    },

    pageName: {
      get() {
        /* If clickActionAnalyticsData has pageName then this will be used else
           pageName will used from pageData. This is usually require when on some event you need
           to override the pageName value. For instance, onClick event.
         */
        const {pageData, AnalyticsDataKey} = store.getState();
        const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData');
        const pageName =
          clickActionAnalyticsData && clickActionAnalyticsData.pageName
            ? clickActionAnalyticsData.pageName
            : pageData.pageName;

        return `gl:${pageName || ''}`;
      },
    },
    orderId: {
      get() {
        const {pageData, AnalyticsDataKey} = store.getState();
        const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData');
        return clickActionAnalyticsData && clickActionAnalyticsData.orderId
          ? clickActionAnalyticsData.orderId
          : pageData.orderId;
      },
    },
    paymentMethod: {
      get() {
        const {pageData, AnalyticsDataKey} = store.getState();
        const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData');
        return clickActionAnalyticsData && clickActionAnalyticsData.paymentMethod
          ? clickActionAnalyticsData.paymentMethod
          : pageData.paymentMethod;
      },
    },
    orderSubtotal: {
      get() {
        const {pageData, AnalyticsDataKey} = store.getState();
        const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData');
        return clickActionAnalyticsData && clickActionAnalyticsData.orderSubtotal
          ? clickActionAnalyticsData.orderSubtotal
          : pageData.orderSubtotal;
      },
    },
    billingCountry: {
      get() {
        return 'us';
      },
    },
    billingZip: {
      get() {
        const {pageData, AnalyticsDataKey} = store.getState();
        const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData');
        return clickActionAnalyticsData && clickActionAnalyticsData.billingZip
          ? clickActionAnalyticsData.billingZip
          : pageData.billingZip;
      },
    },
    isCurrentRoute: () => false,

    pageShortName: {
      get() {
        /* If clickActionAnalyticsData has pageShortName then this will be used else
           pageShortName will used from pageData. Also if pageShortName is not available then pageName will
           be used. This is usually require when on some event you need
           to override the pageName value. For instance, onClick event.
         */
        const {pageData, AnalyticsDataKey} = store.getState();

        const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData');
        const pageShortName =
          clickActionAnalyticsData && clickActionAnalyticsData.pageShortName
            ? clickActionAnalyticsData.pageShortName
            : pageData.pageShortName;
        const pageName =
          clickActionAnalyticsData && clickActionAnalyticsData.pageName
            ? clickActionAnalyticsData.pageName
            : pageData.pageName;
        return `${pageShortName || pageName}`;
      },
    },

    pageType: {
      get() {
        const {pageData, AnalyticsDataKey} = store.getState();
        const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData', {}) || {};
        return clickActionAnalyticsData.pageType
          ? clickActionAnalyticsData.pageType
          : pageData.pageType;
      },
    },

    countryId: {
      get() {
        return store.getState().APIConfig.storeId;
      },
    },

    switchedBrandId: {
      get() {
        const currentState = store.getState().APIConfig;
        const {brandId} = currentState;
        return brandId && brandId.toUpperCase();
      },
    },

    productId: {
      get() {
        const {pageData, AnalyticsDataKey} = store.getState();
        const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData', {}) || {};
        const {products} = clickActionAnalyticsData;
        const propValue = products ? products[0].id : pageData.productId;
        return propValue || '';
      },
    },

    loadingBrandName: {
      get() {
        return loadingBrand && loadingBrand.toUpperCase();
      },
    },

    storeId: {
      get() {
        const {APIConfig} = store.getState();
        return APIConfig.storeId;
      },
    },

    pageLocale: {
      get() {
        return 'US:en';
      },
    },

    pageSection: {
      get() {
        const {pageData, AnalyticsDataKey} = store.getState();
        const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData', {}) || {};
        return clickActionAnalyticsData && clickActionAnalyticsData.pageSection
          ? clickActionAnalyticsData.pageSection
          : pageData.pageSection;
      },
    },

    pageSubSubSection: {
      get() {
        const {pageData, AnalyticsDataKey} = store.getState();
        const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData', {}) || {};
        return clickActionAnalyticsData.pageSubSection
          ? clickActionAnalyticsData.pageSubSection
          : pageData.pageSubSection;
      },
    },

    siteType: {
      get() {
        return siteType;
      },
    },

    customerType: {
      get() {
        return getUserLoggedInState(store.getState()) ? 'no rewards:logged in' : 'no rewards:guest';
      },
    },

    checkoutType: {
      get() {
        return store.getState().User.getIn(['personalData', 'isGuest'], '')
          ? 'guest'
          : 'registered';
      },
    },

    userEmailAddress: {
      get() {
        return store
          .getState()
          .User.getIn(['personalData', 'contactInfo', 'emailAddress'], 'emailAddress');
      },
    },

    pageDate: {
      get() {
        return new Date().toISOString().split('T')[0];
      },
    },

    customerId: {
      get() {
        return store.getState().User.getIn(['personalData', 'userId'], '');
      },
    },

    currencyCode: {
      get() {
        return 'USD';
      },
    },

    customerFirstName: {
      get() {
        return store.getState().User.getIn(['personalData', 'contactInfo', 'firstName'], '');
      },
    },

    customerLastName: {
      get() {
        return store.getState().User.getIn(['personalData', 'contactInfo', 'lastName'], '');
      },
    },

    pageNavigationText: {
      get() {
        const {pageData, AnalyticsDataKey} = store.getState();
        const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData', {}) || {};
        return clickActionAnalyticsData.pageNavigationText
          ? clickActionAnalyticsData.pageNavigationText
          : pageData.pageNavigationText;
      },
    },

    giftType: {
      get() {
        const clickAnalyticsData = store
          .getState()
          .AnalyticsDataKey.get('clickActionAnalyticsData', {});
        return clickAnalyticsData.confirmationPage && giftService;
      },
    },

    listingDepartment: {
      get() {
        return getDepartmentList(store) || '';
      },
    },
    listingCategory: {
      get() {
        return getCategoryList(store) || '';
      },
    },
    listingSubCategory: {
      get() {
        return getSubCategoryList(store) || '';
      },
    },
    // TODO: This formatting logic needs to match current app
    listingCount: {
      get() {
        const {AnalyticsDataKey} = store.getState();
        const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData', {}) || {};
        return (
          clickActionAnalyticsData.listingCount ||
          (store.getState().ProductListing && store.getState().ProductListing.totalProductsCount)
        );
      },
    },
    // TODO: This formatting logic needs to match current app
    productsCount: {
      get() {
        const {AnalyticsDataKey} = store.getState();
        const clickActionAnalyticsData = AnalyticsDataKey.get('clickActionAnalyticsData', {}) || {};
        return clickActionAnalyticsData.productsCount || clickActionAnalyticsData.listingCount;
      },
    },
    cartType: {
      get() {
        const orderDetails = store.getState().CartPageReducer.get('orderDetails');
        let typeCart = 'standard';
        const isBopisOrder = orderDetails.get('isBopisOrder');
        const isBossOrder = orderDetails.get('isBossOrder');
        const isPickupOrder = orderDetails.get('isPickupOrder');
        const isShippingOrder = orderDetails.get('isShippingOrder');
        if (isShippingOrder && (isBopisOrder || isBossOrder || isPickupOrder)) {
          typeCart = 'mix';
        } else if (isBopisOrder && !isBossOrder) {
          typeCart = 'bopis';
        } else if (isBossOrder && !isBopisOrder) {
          typeCart = 'boss';
        }
        return typeCart;
      },
    },
    products: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'products'], '');
      },
    },
    storeSearchDistance: {
      get() {
        const state = store.getState();
        const defaultData =
          state.AnalyticsDataKey && state.AnalyticsDataKey.get('clickActionAnalyticsData');
        return defaultData && defaultData.storeSearchDistance;
      },
    },
    storeSearchCriteria: {
      get() {
        const state = store.getState();
        const defaultData =
          state.AnalyticsDataKey && state.AnalyticsDataKey.get('clickActionAnalyticsData');
        return defaultData && defaultData.storeSearchCriteria;
      },
    },
    CartAddProductId: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'CartAddProductId'], '');
      },
    },
    internalCampaignId: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'internalCampaignId'], '');
      },
    },
    searchType: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'searchType'], '');
      },
    },
    originType: {
      get() {
        return (
          store.getState().AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'originType'], '') ||
          store.getState().AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'searchType'], '')
        );
      },
    },
    searchText: {
      get() {
        const stateData = store.getState();
        const {AnalyticsDataKey, pageData} = stateData;
        let text = AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'searchText'], '');
        if (
          !text &&
          pageData &&
          pageData.pageSection !== 'search' &&
          pageData.pageSection !== 'homepage'
        ) {
          text = AnalyticsDataKey.getIn(['commonAnalyticsData', 'searchTextFirstTouch'], '');
        }
        return text;
      },
    },
    searchTextFirstTouch: {
      get() {
        return store.getState().get('searchTextFirstTouch') || '';
      },
    },

    NavigationV97FirstTouch: {
      get() {
        const {AnalyticsDataKey} = store.getState();
        const commonAnalyticsData = AnalyticsDataKey.get('commonAnalyticsData', {});
        return (commonAnalyticsData && commonAnalyticsData.navigationFirstTouch) || '';
      },
    },
    couponCode: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'couponCode'], '');
      },
    },
    linkName: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'linkName'], '');
      },
    },
    breadcrumb: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'breadcrumb'], '');
      },
    },
    pageErrorType: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'pageErrorType'], '');
      },
    },
    list3VarValues: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'list3VarValues'], '');
      },
    },
    mprItem: {
      get() {
        return store.getState().AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'mprItem'], '');
      },
    },
    plccItem: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'plccItem'], '');
      },
    },
    guestItem: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'guestItem'], '');
      },
    },
    externalCampaignId: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'externalCampaignId'], '');
      },
    },
    omMID: {
      get() {
        return store.getState().AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'omMID'], '');
      },
    },
    omRID: {
      get() {
        return store.getState().AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'omRID'], '');
      },
    },
    essentialsFormValue: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'essentialsFormValue']);
      },
    },
    sbp_gender: {
      get() {
        return store.getState().AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'sbp_gender']);
      },
    },
    sbp_no_of_profiles: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'sbp_no_of_profiles']);
      },
    },
    sbp_name_of_profiles: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'sbp_name_of_profiles']);
      },
    },
    sbp_categories: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'sbp_categories']);
      },
    },
    sbp_profile_name: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'sbp_profile_name']);
      },
    },
    sbp_products_name: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'sbp_products_name']);
      },
    },
    sbp_email: {
      get() {
        return store.getState().AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'sbp_email']);
      },
    },
    applePayButtonLocation: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'applePayButtonLocation']);
      },
    },
    orderHelpLocation: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'orderHelpLocation'], '');
      },
    },
    orderId_cd127: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'orderId_cd127'], '');
      },
    },
    orderHelp_path_cd128: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'orderHelp_path_cd128'], '');
      },
    },
    metric112: {
      get() {
        return store.getState().AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'metric112']);
      },
    },
    app_launch_v138: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'app_launch_v138']);
      },
    },
    deviceId: {
      get() {
        const {APIConfig} = store.getState();
        return (APIConfig && APIConfig.deviceID) || '';
      },
    },
    loyaltyLocation: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'loyaltyLocation']);
      },
    },
    afterpay_metrics_v141: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'afterpay_metrics_v141'], '');
      },
    },
    afterpay_location_v131: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'afterpay_location_v131'], '');
      },
    },
    metric150: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'event150'], '');
      },
    },
    metric151: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'event151'], '');
      },
    },
    metric152: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'event152'], '');
      },
    },
    metric153: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'event153'], '');
      },
    },
    liveChat_name_p145: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'liveChat_name_p145']);
      },
    },
    livechat_metric145: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'livechat_metric145']);
      },
    },
    livechat_event145: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'livechat_event145']);
      },
    },
    metric159: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'metric159'], '');
      },
    },
    metric160: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'metric160'], '');
      },
    },
    Sort_Options_v51: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'Sort_Options_v51'], '');
      },
    },
    Refinement_Used_v50: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(['clickActionAnalyticsData', 'Refinement_Used_v50'], '');
      },
    },
    Shipping_Preferences_Location_v137: {
      get() {
        return store
          .getState()
          .AnalyticsDataKey.getIn(
            ['clickActionAnalyticsData', 'Shipping_Preferences_Location_v137'],
            '',
          );
      },
    },
  });
}
