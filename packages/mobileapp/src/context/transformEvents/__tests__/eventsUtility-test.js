// 9fbef606107a605d69c0edbcd8029e5d 
import {getProducts, getProductsEvents} from '../clickEvents/eventsUtility';

describe('transformEvents', () => {
  it('get product data method should return string if data passed', () => {
    // eslint-disable-next-line
    global._dataLayer = {
      currencyCode: 'USD',
      giftType: 'P',
      internalCampaignIdList: ['11', '22'],
    };
    const prodData = [
      {
        color: 'MED DENIM',
        id: 8067686477,
        name: 'Girls Denim Pull-On Shorts',
        price: 16.94,
        extPrice: 16.94,
        paidPrice: 16.94,
        listPrice: 16.94,
        partNumber: '2076092_M4',
        size: 4,
        upc: '00889705446890',
        sku: 856759,
        pricingState: 'full price',
        colorId: 852212,
        quantity: 1,
        shippingMethod: 123,
        storeId: 345,
        outfitId: 678,
        stylitics: true,
        uniqueOutfitId: true,
        offerPrice: 15,
        sflExtPrice: 14,
        rating: 22,
        reviews: 45,
        type: 'test',
        gender: 'male',
        position: 'top',
        giftCardAmount: 50,
        couponAmount: 50,
        savingsAmount: 50,
        shippingAmount: 50,
        taxAmount: 50,
        netRevenue: 50,
        multipleSkus: true,
        cartPrice: 21,
        recsPageType: true,
        searchClick: true,
        plpClick: true,
        recsProductId: true,
        recsType: true,
        features: 'high',
        addType: 'outfit',
      },
    ];

    const sObj = {
      prop2: 'outfit',
      events: ['event86'],
      orderId_v3: true,
      linkName: 'cart add',
      eVar19: 33,
    };
    expect(getProducts(prodData, sObj)).toEqual([
      {
        item_brand: 'TCP',
        item_category: 'MED DENIM|22:45|outfit|4',
        item_category2: 'true|true|true',
        item_category3: '1',
        item_category4: '1|16.94|false',
        item_id: '8067686477',
        item_list_id: '16.94 USD|16.94 USD',
        item_list_name: 'browse',
        item_name: 'Girls Denim Pull-On Shorts',
      },
    ]);
    expect(getProductsEvents(prodData, sObj)).toEqual([
      {
        Cross_Sell_Clicks_e50: '1',
        Cross_Sell_Driver_v92: true,
        Cross_Sell_Location_v21: true,
        Cross_Sell_Referring_Product_v20: true,
        Gift_Card_Amount_Used_e6: '50.00',
        Net_Revenue_e99: '50.00',
        Refer_a_friend_Sign_ups_e22: '50.00',
        Shipping_Amount_e78: '50.00',
        Tax_Amount_e79: '50.00',
        coupon: '50.00',
        event136: '14.00',
        event137: '15.00',
        event85: '16.94',
        event93: '1',
        itemsCount: '1',
        itemsPrice: '16.94',
      },
    ]);
  });
  it('get product data method should return string if data not passed', () => {
    // eslint-disable-next-line
    global._dataLayer = {
      currencyCode: 'USD',
      giftType: 'P',
      internalCampaignIdList: ['11', '22'],
    };
    expect(getProducts([])).toEqual([]);
    expect(getProductsEvents([])).toEqual([]);
  });
});
