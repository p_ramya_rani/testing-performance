// 9fbef606107a605d69c0edbcd8029e5d 
import {transformClickEvent, transformPageEvent, getTransformedObject} from '../transformEvents';

describe('transformEvents', () => {
  it('transformPageEvent should return null if screenName is not passed', () => {
    expect(transformPageEvent()).toBeNull();
  });

  it('transformPageEvent should return null if no mapping is present for screenName', () => {
    expect(transformPageEvent('test')).toBeNull();
  });

  it('transformPageEvent should return object if mapping is present for screenName', () => {
    const t = transformPageEvent('Navigation');
    expect(Object.keys(t).length).toBeGreaterThan(1);
  });

  it('transformClickEvent should return null if name is not passed', () => {
    expect(transformClickEvent()).toBeNull();
  });

  it('transformClickEvent should return null if no mapping is present for name and module', () => {
    expect(transformClickEvent()).toBeNull();
  });

  it('transformClickEvent should return object if mapping is present for name and module', () => {
    // eslint-disable-next-line no-underscore-dangle
    global._dataLayer = {
      pageName: 'account',
      siteType: 'global site',
      pageType: 'account',
    };
    const t = transformClickEvent('Logins_e14', 'account');
    expect(Object.keys(t).length).toBeGreaterThan(1);
  });

  it('getTransformedObject should convert string to transformed object', () => {
    const t = getTransformedObject({
      eVars: {adobeObj: 'pageName, prop27', googleObj: 'pageName, pageType'},
      props: {adobeObj: 'prop34, prop27', googleObj: 'pageName, prop27'},
    });
    expect(t.prop27).toBeUndefined();
  });
});
