// 9fbef606107a605d69c0edbcd8029e5d
import pageMapper from './pages';
import * as clickEvents from './clickEvents';
import getConversionValue from './conversion';

const getSingleTransformedObj = (mapping, supressPropsList) => {
  return mapping.split(',').reduce((obj, current) => {
    const currentObj = obj;
    const key = current.trim();
    if (key === 'events') {
      const eventlists = getConversionValue(key);
      const eventlistsArry = eventlists.split(',');
      eventlistsArry.forEach((eventItem) => {
        currentObj[eventItem] = '1';
      });
    } else {
      currentObj[key] = getConversionValue(key);
    }
    if (!(supressPropsList && supressPropsList.includes(key))) {
      currentObj[key] = getConversionValue(key);
    }
    if (key === 'abtest') {
      return currentObj.abtest;
    }
    return currentObj;
  }, {});
};

export const getTransformedObject = (mapping, supressPropsList) => {
  const {eVars, props} = mapping;
  const adobeMap = `${eVars.adobeObj ? eVars.adobeObj : ''},${
    props.adobeObj ? props.adobeObj : ''
  }`;
  const adobeMapTransformed = adobeMap && getSingleTransformedObj(adobeMap, supressPropsList);
  const globalVarsGoogle = getSingleTransformedObj(eVars.googleObj);
  const dynamicVarsGoogle = props.googleObj && getSingleTransformedObj(props.googleObj);
  const googleMapTransformed = {eVars: globalVarsGoogle, props: dynamicVarsGoogle};
  return {adobeMapTransformed, googleMapTransformed};
};

/**
 * This function will generate the required payload for page load event
 * @param { string } screenName - name of the page, should have mapping in ./pages.js file
 */
export const transformPageEvent = (screenName, supressProps) => {
  const mappingString = pageMapper[screenName];

  if (!mappingString) {
    // if mapping is not present then event should not be logged
    return null;
  }
  return getTransformedObject(mappingString, supressProps);
};

/**
 * This function will generate the required payload for a click event
 * @param { string } name - name of the event key, should have mapping in ./clickEvents
 * @param {string } module - name of the module in which event to be searched, 'global' is the default one
 */
export const transformClickEvent = (name, module) => {
  if (!name) {
    return null;
  }
  const mappingString = clickEvents[module || 'global'][name];
  if (Object.keys(mappingString).length === 0) {
    // if mapping is not present then event should not be logged
    return null;
  }
  return getTransformedObject(mappingString);
};
