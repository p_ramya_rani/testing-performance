// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable sonarjs/no-duplicate-string */
import get from 'lodash/get';
import {abTestMonetate} from '@tcp/core/src/services/abstractors/common/abTest/abTest';
import {
  getProducts,
  getTimeParting,
  addedEventsInProds,
  getProductsEvents,
  getAdobeProducts,
} from './clickEvents/eventsUtility';

/**
 * This object will contain mapping of constants variables
 */
const staticConversion = {
  prop27: 'D=mid',
  Visitor_ID_p27: 'D=mid',
  Analytics_Code_Ver_p28: '2.10.0',
  prop28: '2.10.0',
  eVar33: 'D=pageName',
  Registration_Source_v33: 'pageName',
  eVar51: 'listingSortList',
  eVar79: '+1',
  eVar91: '+1',
};

/**
 * This object will contain mapping for standard variables other than
 * eVar and props
 */
const standardConversion = {
  pageName: 'pageName',
  pageType: 'pageType',
  currencyCode: 'currencyCode',
  '&&events': 'eventData.customEvents',
  '&&products': 'eventData.products',
  events: 'eventData.customEvents',
  events_data: 'eventData.customEvents',
  productsData: 'eventData.products',
  productsEvents: 'eventData.products',
};

const varConversion = {
  Country_ID_v29: 'countryId',
  eVar29: 'countryId',
  eVar10: 'pageName',
  Page_Name_v10: 'pageName',
  eVar0: 'externalCampaignId',
  ext_campId_v0: 'externalCampaignId',
  eVar1: 'siteType',
  Site_v1: 'siteType',
  eVar6: 'couponCode',
  Coupon_Discount_Code_v6: 'couponCode',
  eVar7: 'pageLocale',
  Cust_Country_Lang_v7: 'pageLocale',
  eVar3: 'orderId',
  orderId_v3: 'orderId',
  Payment_Method_v4: 'paymentMethod',
  eVar4: 'paymentMethod',
  eVar8: 'storeId',
  Store_ID_v8: 'storeId',
  eVar13: 'customerType',
  Cust_Login_Status_v13: 'customerType',
  eVar28: 'pageShortName',
  p_short_n_v28: 'pageShortName',
  eVar31: 'userEmailAddress',
  Email_Address_v31: 'userEmailAddress',
  eVar32: 'currencyCode',
  Currency_Code_v32: 'currencyCode',
  eVar36: 'omMID',
  omId_v36: 'omMID',
  eVar40: 'omRID',
  omRid_v40: 'omRID',
  eVar65: 'pageShortName',
  Page_Name_Detail_v65: 'pageShortName',
  eVar68: 'billingZip',
  bill_zip_v68: 'billingZip',
  eVar69: 'storeSearchCriteria',
  Store_Search_Criteria_v69: 'storeSearchCriteria',
  eVar70: 'storeSearchDistance',
  store_srch_dis_v70: 'storeSearchDistance',
  eVar73: 'orderSubtotal',
  order_stotal_v73: 'orderSubtotal',
  eVar74: 'pageDate',
  Date_v74: 'pageDate',
  eVar86: 'cartType',
  Cart_Type_v86: 'cartType',
  eVar93: 'customerId',
  Customer_ID_v93: 'customerId',
  eVar98: 'checkoutType',
  Checkout_Type_v98: 'checkoutType',
  eVar19: 'internalCampaignId',
  Internal_Campaigns_v19: 'internalCampaignId',
  eVar26: 'searchText',
  Internal_Search_Terms_v26: 'searchText',
  eVar27: 'listingCount',
  Search_Results_v27: 'listingCount',
  eVar30: 'productsCount',
  Product_Results_v30: 'productsCount',
  eVar59: 'searchType',
  Search_Type_v59: 'searchType',
  eVar22: 'originType',
  Prod_Finding_Method_v22: 'originType',
  eVar97: 'pageNavigationText',
  Click_Navigation_v97: 'NavigationV97FirstTouch',
  eVar34: 'switchedBrandId',
  brand_switch_v34: 'switchedBrandId',
  eVar37: 'loadingBrandName',
  Landing_Site_v37: 'loadingBrandName',
  eVar23: 'listingDepartment',
  list_dept_v23: 'listingDepartment',
  eVar24: 'listingCategory',
  list_cat_v24: 'listingCategory',
  eVar45: 'pageErrorType',
  pg_err_v45: 'pageErrorType',
  List_Var_3: 'list3VarValues',
  eVar25: 'listingSubCategory',
  list_sub_cat_v25: 'listingSubCategory',
  eVar50: 'prop11Filter',
  p11_filter_v50: 'prop11Filter',
  eVar102: 'mprItem',
  mprItem_v102: 'mprItem',
  eVar103: 'plccItem',
  plccItem_v103: 'plccItem',
  eVar105: 'guestItem',
  gItem_v105: 'guestItem',
  eVar107: 'essentialsFormValue',
  gender_v54: 'sbp_gender',
  sbp_no_of_profiles_v46: 'sbp_no_of_profiles',
  sbp_name_of_profiles_v42: 'sbp_name_of_profiles',
  top_categories_plp_v116: 'sbp_categories',
  profile_name_v115: 'sbp_profile_name',
  products_name_v115: 'sbp_products_name',
  sbp_email_v47: 'sbp_email',
  essnt_frmVal_v107: 'essentialsFormValue',
  eVar148: 'applePayButtonLocation',
  applePayButtonLocation_v148: 'applePayButtonLocation',
  app_launch_v138: 'app_launch_v138',
  deviceId_v200: 'deviceId',
  Loyalty_Location_v129: 'loyaltyLocation',
  abtest: 'abtest',
  afterpay_location_v131: 'afterpay_location_v131',
  afterpay_metrics_v141: 'afterpay_metrics_v141',
  Shipping_Preferences_Location_v137: 'Shipping_Preferences_Location_v137',
};

const propConversion = {
  Page_Type_p2: 'pageType',
  prop2: 'pageType',
  prop4: 'pageSection',
  prop5: 'pageSubSubSection',
  prop6: 'countryId',
  prop21: 'pageNavigationText',
  Site_Sections_Level_2_p4: 'pageSection',
  Site_Sections_Level_3_p5: 'pageSubSubSection',
  Country_ID_Pathing_p6: 'countryId',
  Navigation_Pathing_p21: 'pageNavigationText',
  prop23: 'breadcrumb',
  bcrumb_p23: 'breadcrumb',
  prop24: 'listingSortList',
  list_sort_p24: 'listingSortList',
  prop11: 'listingFilterList',
  list_fil_p11: 'listingFilterList',
  Store_ID_Pathing_p29: 'storeId',
  prop29: 'storeId',
  prop26: 'CartAddProductId',
  cart_prodId_v26: 'CartAddProductId',
  prop34: 'switchedBrandId',
  brand_switch_p34: 'switchedBrandId',
  prop35: 'switchedBrandId',
  brand_prod_p35: 'switchedBrandId',
  prop37: 'loadingBrandName',
  Landing_Site_p37: 'loadingBrandName',
  prop22: 'productId',
  prod_id_p22: 'productId',
  orderHelp_path_p128: 'orderHelp_path_cd128',
  orderHelp_location_p126: 'orderHelpLocation',
  liveChat_name_p145: 'liveChat_name_p145',
  liveChat_metric_p145: 'livechat_metric145',
  liveChat_event_p145: 'livechat_event145',
  orderId_p127: 'orderId_cd127',
  refund_metric_p112: 'metric112',
  Refinement_Used_v50: 'Refinement_Used_v50',
  Sort_Options_v51: 'Sort_Options_v51',
};

/**
 * This function will return dataLayer key corresponding to event key
 */
const getConversionKey = (key) => {
  if (/_v|eVar|List/.test(key)) {
    return varConversion[key];
  }

  if (/_p|prop/.test(key)) {
    return propConversion[key];
  }

  return standardConversion[key];
};

const getConvertedData = () => {
  let cvData = {};

  const actions = abTestMonetate?.data?.responses[0]?.actions || [];
  const length = actions.length > 25 ? 25 : actions.length;
  for (let i = 0; i < length; i += 1) {
    const abData =
      actions && actions[i] && actions[i].impressionReporting && actions[i].impressionReporting[0];
    const currentData = {};
    const expName = abData.experience_name?.replace(/\W+/g, '_').substring(0, 39);
    currentData[expName] = abData.variant_label;
    cvData = {...cvData, ...currentData};
  }
  return cvData;
};

/**
 * This function will return value of the event key
 */
// eslint-disable-next-line complexity
const getConversionValue = (key) => {
  /* eslint-disable */
  if (staticConversion[key]) {
    return staticConversion[key];
  }
  if (key === 'abtest') return getConvertedData();
  const dataLayer = global._dataLayer;
  const dataLayerKey = getConversionKey(key);
  let convertedData = get(dataLayer, dataLayerKey, '');

  const prodData = get(dataLayer, 'eventData.products');
  if (key === 'events_data') {
    return addedEventsInProds(prodData, {
      linkName: dataLayer.linkName,
      Page_Type_p2: dataLayer.pageType,
      orderId_v3: dataLayer.orderId,
    });
  }
  if (key === 'productsData') {
    const values = getProducts(prodData, {
      linkName: dataLayer.linkName,
      Page_Type_p2: dataLayer.pageType,
      orderId_v3: dataLayer.orderId,
    });
    return values;
  }
  //
  if (key === 'productsEvents') {
    const values = getProductsEvents(prodData, {
      linkName: dataLayer.linkName,
      Page_Type_p2: dataLayer.pageType,
      orderId_v3: dataLayer.orderId,
    });
    return values;
  }

  if (key === '&&events') {
    const prodEvents = addedEventsInProds(prodData, {
      linkName: dataLayer.linkName,
      prop2: dataLayer.pageType,
      eVar3: dataLayer.orderId,
    });
    return prodEvents;
  }

  if (key === '&&products') {
    return getAdobeProducts(convertedData, {
      linkName: dataLayer.linkName,
      prop2: dataLayer.pageType,
      eVar3: dataLayer.orderId,
    });
  }

  if (key === 'events') {
    return Array.isArray(convertedData) ? convertedData.join(',') : convertedData;
  }

  if (key === 'eVar94') {
    return '+1';
  }
  if (key === 'Time_Parting_v12' || key === 'eVar12') {
    const tpEls = getTimeParting('n', '-5').split('|');
    return (tpEls[0] + ' ' + tpEls[1]).toLowerCase();
  }
  if (
    key === 'Product_Results_v30' ||
    key === 'Search_Results_v27' ||
    key === 'eVar30' ||
    key === 'eVar27'
  ) {
    return convertedData.toString();
  }
  return convertedData;
};

export default getConversionValue;
