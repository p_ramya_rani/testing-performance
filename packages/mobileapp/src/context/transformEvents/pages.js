// 9fbef606107a605d69c0edbcd8029e5d
/**
 * This object will contain the mapping of event object keys with the page name.
 * This Page_Name_v10 should be same as the screenName passed on TRACK_PAGE_VIEW event payload
 */
import globalVars from './clickEvents/globalVar.constant';

const {adobe, google} = globalVars;
const prodListingVars =
  'prop34, prop37, eVar34, prop2, prop11, prop21, prop23, prop24, pageName, eVar19, eVar22, eVar23, eVar24, eVar25, eVar26, eVar27, eVar28, eVar30, eVar51, eVar59, eVar32, eVar93, eVar97, &&events, events, &&products';

const pages = {
  Navigation: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: 'eVar34, prop34, eVar37, prop37, pageName, pageType',
      googleObj: 'brand_switch_p34, Landing_Site_p37, Page_Name_v10, pageType',
    },
  },
  storePageViews_e96: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: 'prop34, prop37, pageName, prop2, eVar8, eVar28, eVar32, events',
      googleObj: 'brand_switch_p34, Landing_Site_p37, Page_Name_v10, events, Store_ID_v8',
    },
  },
  product_listing: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'eVar34, prop34, prop2, prop11, prop21, prop23, prop24, pageName, eVar19, eVar22, eVar23, eVar24, eVar25, eVar26, eVar27, eVar28, eVar30, eVar51, eVar59, eVar32, eVar93, eVar97, &&events, &&products, events',
      googleObj:
        'brand_switch_p34, Landing_Site_p37, bcrumb_p23, list_sort_p24, Page_Name_v10, productsData, events, Search_Type_v59, Internal_Search_Terms_v26, Search_Results_v27',
    },
  },
  product_listing_filters: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'eVar34, prop34, prop2, prop11, prop21, prop23, prop24, pageName, eVar19, eVar22, eVar23, eVar24, eVar25, eVar26, eVar28, eVar30, eVar50, eVar51, eVar32, eVar93, &&events, events, &&products',
      googleObj:
        'brand_switch_p34, Landing_Site_p37, list_fil_p11,  bcrumb_p23, list_sort_p24,  Page_Name_v10, productsData, events, Internal_Search_Terms_v26',
    },
  },
  home_page: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'eVar34, prop34, pageName, prop2, prop23, eVar14, eVar19, eVar22, eVar23, eVar24, eVar25, eVar26, eVar27, eVar28, eVar30, eVar59, eVar93, eVar97, &&events, events',
      googleObj:
        'brand_switch_p34, Landing_Site_p37, Page_Name_v10, Page_Type_p2, Site_Sections_Level_2_p4, Site_Sections_Level_3_p5, Country_ID_Pathing_p6, bcrumb_p23, list_sort_p24, Visitor_ID_p27, Analytics_Code_Ver_p28, events, Search_Type_v59, Internal_Search_Terms_v26, Search_Results_v27',
    },
  },
  bagPage: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'eVar34, prop34, prop2, prop21, prop27, pageName, eVar28, eVar32, eVar86, eVar93, eVar98, &&events, events, &&products',
      googleObj: 'productsData, productsEvents, events, Cart_Type_v86',
    },
  },
  pickup: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'eVar34, prop34, prop2, pageName, eVar32, eVar86, eVar93, eVar98, currencyCode, &&events, events, &&products',
      googleObj:
        'brand_switch_p34, Landing_Site_p37, Page_Type_p2, Site_Sections_Level_2_p4, Site_Sections_Level_3_p5, Country_ID_Pathing_p6, Visitor_ID_p27, Analytics_Code_Ver_p28, Page_Name_v10, Currency_Code_v32, events, productsData, Cart_Type_v86',
    },
  },
  shipping: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'eVar34, prop34, eVar37, prop37, prop2, pageName, eVar28, eVar32, eVar86, eVar93, eVar98, currencyCode, &&events, events, &&products',
      googleObj:
        'brand_switch_p34, prop35, Landing_Site_p37, Page_Type_p2, Site_Sections_Level_2_p4, Site_Sections_Level_3_p5, Country_ID_Pathing_p6, Visitor_ID_p27, Analytics_Code_Ver_p28, Page_Name_v10, Currency_Code_v32, events, productsData, Cart_Type_v86',
    },
  },
  billing: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'eVar34, prop34, prop2, pageName, eVar28, eVar32, eVar86, eVar93, eVar98, currencyCode, &&events, events, &&products',
      googleObj:
        'brand_switch_p34, prop35, Page_Name_v10, Page_Type_p2, Site_Sections_Level_2_p4, Site_Sections_Level_3_p5, Country_ID_Pathing_p6, Visitor_ID_p27, Analytics_Code_Ver_p28, Currency_Code_v32, events, productsData, Cart_Type_v86',
    },
  },
  review: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'eVar34, prop34, eVar37, prop37, pageName, prop2, eVar9, eVar28, eVar32, eVar68, eVar73, eVar86, eVar93, eVar98, currencyCode, &&events, events, &&products',
      googleObj:
        'brand_switch_p34, prop35, Landing_Site_p37, Page_Name_v10, Page_Type_p2, Site_Sections_Level_2_p4, Site_Sections_Level_3_p5, Country_ID_Pathing_p6, Visitor_ID_p27, productsEvents, Currency_Code_v32, events, productsData, bill_zip_v68, Cart_Type_v86',
    },
  },
  confirmation: {
    eVars: {
      adobeObj: adobe,
      googleObj:
        'Site_v1, Page_Name_v10, Time_Parting_v12, Cust_Login_Status_v13, Email_Address_v31, Currency_Code_v32, Page_URL_v62, Page_Name_Detail_v65, bill_zip_v68, Visitor_ID_p27, brand_switch_p34, Landing_Site_p37, Page_Type_p2, Coupon_Discount_Code_v6, Cart_Type_v86, Checkout_Type_v98, Payment_Method_v4, orderId_v3, order_stotal_v73, Internal_Search_Terms_v26, Click_Navigation_v97',
    },
    props: {
      adobeObj:
        'eVar34, prop34, eVar37, prop37, pageName, prop2, eVar3, eVar4, eVar9, eVar28, eVar32, eVar68, eVar73, eVar86, eVar93, eVar98, currencyCode, &&events, events, &&products',
      googleObj: 'productsEvents, productsData',
    },
  },
  productListingPageViews_e91: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: prodListingVars,
      googleObj:
        'Prod_Finding_Method_v22, Page_Name_v10, Page_Type_p2, Search_Results_v27 , Country_ID_Pathing_p6, Visitor_ID_p27, Analytics_Code_Ver_p28, productsData, Product_Results_v30, Click_Navigation_v97',
    },
  },
  productListingImpressionse92: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: prodListingVars,
      googleObj:
        'Prod_Finding_Method_v22, Page_Name_v10, Page_Type_p2, Site_Sections_Level_2_p4, Site_Sections_Level_3_p5, Country_ID_Pathing_p6, Visitor_ID_p27, Analytics_Code_Ver_p28, Product_Results_v30',
    },
  },
  productimpression: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: prodListingVars,
      googleObj:
        'brand_switch_p34, Landing_Site_p37, Prod_Finding_Method_v22, Page_Name_v10, Page_Type_p2, Page_Name_Detail_v65, list_sort_p24, Visitor_ID_p27, events, Product_Results_v30',
    },
  },
  searchDetail_e20: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobe:
        'eVar34, prop34, prop2, pageName, prop24, eVar8, eVar19, eVar22, eVar26, eVar27, eVar28, eVar30, eVar32, eVar51, eVar59, eVar93, eVar94, eVar97, &&products, &&events, events',
      googleObj:
        'Search_Type_v59, Internal_Search_Terms_v26, Prod_Finding_Method_v22, Page_Name_v10, Page_Type_p2, Page_Name_Detail_v65, Country_ID_Pathing_p6, list_sort_p24, Visitor_ID_p27, Internal_Campaigns_v19, Search_Results_v27, Product_Results_v30, events',
    },
  },

  searchDetail_e21: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobe:
        'eVar34, prop34, prop2, pageName, prop24, eVar8, eVar19, eVar22, eVar26, eVar27, eVar28, eVar30, eVar32, eVar51, eVar59, eVar93, eVar94, eVar97, &&products, &&events, events',
      googleObj:
        'brand_switch_p34, Landing_Site_p37, Prod_Finding_Method_v22, Page_Name_v10, Page_Type_p2, Page_Name_Detail_v65, Country_ID_Pathing_p6, list_sort_p24, Visitor_ID_p27, Search_Type_v59, events, Product_Results_v30, Internal_Search_Terms_v26, Search_Results_v27',
    },
  },
  internalCampaignImpressions_e80: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: prodListingVars,
      googleObj:
        'brand_switch_p34, Landing_Site_p37, Page_Name_v10, Internal_Campaigns_v19, list_sort_p24, events',
    },
  },
  internalCampaignImpressions_e81: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: prodListingVars,
      googleObj:
        'brand_switch_p34, Landing_Site_p37, Prod_Finding_Method_v22, Page_Name_v10, Page_Type_p2, Internal_Campaigns_v19, Search_Results_v27, Country_ID_Pathing_p6, list_sort_p24, Visitor_ID_p27, Analytics_Code_Ver_p28, productsData, events, Product_Results_v30',
    },
  },
  ProductDetail: {
    eVars: {adobeObj: adobe, googleObj: 'Email_Address_v31, Date_v74'},
    props: {
      adobeObj:
        'eVar79,eVar34, prop34, eVar37, prop37, pageName, pageType, &&products, prop2, prop22, prop29, eVar14, eVar28, eVar32, eVar62, evar93, &&events, events',
      googleObj:
        'brand_switch_p34, Landing_Site_p37, pageType, productsData, Page_Type_p2 , Site_Sections_Level_2_p4, Site_Sections_Level_3_p5, Country_ID_Pathing_p6, prod_id_p22, Visitor_ID_p27, Analytics_Code_Ver_p28, Store_ID_Pathing_p29, events, Site_v1, Cust_Country_Lang_v7, Cust_Visit_Plug_in_v11, Time_Parting_v12, Cust_Login_Status_v13, Country_ID_v29',
    },
  },
  outfitDetail: {
    eVars: {adobeObj: adobe, googleObj: 'Email_Address_v31, Date_v74'},
    props: {
      adobeObj:
        'eVar34, prop34, eVar37, prop37, pageName, pageType, &&products, prop2, prop22, eVar19, eVar22, eVar26, eVar27, eVar28, eVar32, eVar59, eVar91, &&events, events',
      googleObj:
        'brand_switch_p34, Landing_Site_p37, pageType, productsData, Page_Type_p2 , Site_Sections_Level_2_p4, Site_Sections_Level_3_p5, Country_ID_Pathing_p6, prod_id_p22, Visitor_ID_p27, Analytics_Code_Ver_p28, Store_ID_Pathing_p29, events, Site_v1, Cust_Country_Lang_v7, Cust_Visit_Plug_in_v11, Time_Parting_v12, Cust_Login_Status_v13, Country_ID_v29, Search_Type_v59, Search_Results_v27',
    },
  },
  account: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'eVar34, prop34, eVar37, prop37, pageName, prop2, prop21, eVar28, eVar32, eVar62, eVar93, events, currencyCode',
      googleObj:
        'brand_switch_p34, Landing_Site_p37, Page_Name_v10, Page_Type_p2, Site_Sections_Level_2_p4, Site_Sections_Level_3_p5, Country_ID_Pathing_p6, Navigation_Pathing_p21, Visitor_ID_p27, Analytics_Code_Ver_p28, events, Currency_Code_v32',
    },
  },
  getCandid: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: 'events,prop2,prop29,eVar8,eVar28,eVar32,eVar62,eVar93,eVar37,prop37',
      googleObj:
        'events,Page_Type_p2,Site_Sections_Level_2_p4,Site_Sections_Level_3_p5,Country_ID_Pathing_p6,Visitor_ID_p27,Analytics_Code_Ver_p28,Store_ID_Pathing_p29,Landing_Site_p37',
    },
  },
  static_page: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'eVar0, eVar36, eVar40, eVar34, prop34, eVar37, prop37, pageName, prop2, prop23, prop24, eVar19, eVar22, eVar23, eVar24, eVar25, eVar26, eVar27, eVar28, eVar30, eVar59, eVar32, eVar93, eVar97, &&events, events',
      googleObj:
        'brand_switch_p34, Landing_Site_p37, Page_Name_v10, Page_Type_p2, Site_Sections_Level_2_p4, Site_Sections_Level_3_p5, Country_ID_Pathing_p6, bcrumb_p23, list_sort_p24, Visitor_ID_p27, Analytics_Code_Ver_p28, events, Search_Type_v59, Internal_Search_Terms_v26, Search_Results_v27',
    },
  },
  orderHelp_page: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: '',
      googleObj: 'orderId_p127',
    },
  },
  orderRefund_successfull_page: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: '',
      googleObj: 'refund_metric_p112, events',
    },
  },
  app_launch: {
    eVars: {adobeObj: adobe, googleObj: `${google},app_launch_v138`},
    props: {
      adobeObj: '',
      googleObj: 'events',
    },
  },
  plcc_form: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'eVar0, eVar36, eVar40, eVar34, prop34, eVar37, prop37, pageName, prop2, prop23, prop24, eVar19, eVar22, eVar23, eVar24, eVar25, eVar26, eVar27, eVar28, eVar30, eVar59, eVar32, eVar93, eVar97, &&events, events',
      googleObj:
        'brand_switch_p34, Landing_Site_p37, Page_Name_v10, Page_Type_p2, Site_Sections_Level_2_p4, Site_Sections_Level_3_p5, Country_ID_Pathing_p6, Navigation_Pathing_p21, Visitor_ID_p27, Analytics_Code_Ver_p28, events, Currency_Code_v32',
    },
  },
  abtest: {
    eVars: {
      adobeObj: adobe,
      googleObj: google,
    },
    props: {
      adobeObj: '',
      googleObj: 'abtest',
    },
  },
};

export default pages;
