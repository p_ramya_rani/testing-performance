// 9fbef606107a605d69c0edbcd8029e5d
/**
 * This object will contain the mapping of event object keys with the event name for Checkout module.
 * This eventName should be same as the name passed on TRACK_PAGE_CLICK event payload
 */

import globalVars from './globalVar.constant';

const {adobe, google} = globalVars;

const errorProps = {
  eVars: {adobeObj: adobe, googleObj: google},
  props: {
    adobeObj:
      'prop2, prop34, prop35, prop37, eVar32, eVar34, eVar37, eVar45, eVar86, eVar93, eVar98, events, &&events',
    googleObj: 'brand_switch_p34, brand_prod_p35, Landing_Site_p37, events',
  },
};

export const checkout = {
  gift_options: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'eVar34, prop34, eVar37, prop35, prop37, pageName, prop2, eVar28, eVar32, eVar86, eVar93, eVar98, events, &&events, &&products',
      googleObj: 'events, brand_switch_p34, brand_prod_p35, Landing_Site_p37, Page_Type_p2',
    },
  },
  continue_guest: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'eVar34, prop34, eVar37, prop35, prop37, prop2, pageName, eVar28, eVar32, eVar86, eVar93, eVar98, currencyCode, events, &&events, &&products',
      googleObj: 'events, brand_switch_p34, brand_prod_p35, Landing_Site_p37, Page_Type_p2',
    },
  },
  coupon_success: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'eVar34, prop34, eVar37, prop37, pageName, prop2, eVar28,  eVar32, eVar86, eVar93, eVar98, currencyCode, events,  &&events, &&products',
      googleObj: 'Customer_ID_v93, currencyCode, events',
    },
  },
  coupon_fails: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'eVar34, prop34, eVar37, prop37, pageName, prop2, eVar28, eVar32, eVar86, eVar93, eVar98, events, &&events, &&products',
      googleObj: 'brand_switch_p34, Landing_Site_p37, pageName, events',
    },
  },
  checkout_button: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'eVar34, prop34, eVar37, prop35, prop37, pageName, prop2, prop4, prop5, prop6, prop27, eVar1, eVar7, eVar10, eVar13, eVar28, eVar29, eVar32, eVar65, eVar74, eVar86, eVar93, eVar98, events, &&events, &&products',
      googleObj:
        'brand_switch_p34, Landing_Site_v37, brand_prod_p35, Landing_Site_p37, pageName, Page_Type_p2, events, productsData',
    },
  },
  edit_cart: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'eVar34, prop34, eVar37, prop35, prop37, pageName, prop2, prop4, prop5, prop6, prop27, eVar1, eVar7, eVar10, eVar13, eVar28, eVar29, eVar32, eVar65, eVar74, eVar86, eVar93, eVar98, events, &&events, &&products, currencyCode',
      googleObj:
        'brand_switch_p34, brand_prod_p35, Landing_Site_p37, events, productsData, currencyCode, pageName',
    },
  },
  Amount_Added_Save_for_Later: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'eVar34, prop34, eVar37, prop35, prop37, pageName, prop27, eVar10, eVar13, eVar32, eVar93, events, &&products, currencyCode',
      googleObj:
        'brand_switch_p34, brand_prod_p35, Landing_Site_p37, events, currencyCode, pageName',
    },
  },
  Amount_to_Move_to_Bag: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'eVar34, prop34, eVar37, prop35, prop37, pageName, prop27, eVar10, eVar13, eVar32, eVar93, events, currencyCode, &&products, &&events',
      googleObj:
        'brand_switch_p34, brand_prod_p35, Landing_Site_p37, events, Currency_Code_v32, pageName',
    },
  },
  apply_gift_card: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'eVar34, prop34, eVar37, prop35, prop37, pageName,prop2, prop4, prop5, prop6, prop27, eVar1, eVar7, eVar10, eVar13, eVar28, eVar29, eVar32, eVar65, eVar74, eVar86, eVar93, eVar98, currencyCode, events, &&events, &&products',
      googleObj:
        'brand_switch_p34, brand_prod_p35, Landing_Site_p37, Checkout_Type_v98, currencyCode, events',
    },
  },
  coupon_removed: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'prop21, prop27, prop29, eVar8, eVar10, eVar13, eVar31, eVar32, eVar93, eVar34, eVar35, currencyCode',
      googleObj:
        'Cust_Login_Status_v13, Email_Address_v31, Currency_Code_v32, Customer_ID_v93, brand_switch_v34, Brand_v35, currencyCode',
    },
  },
  pickup_option_click: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'events, &&events, prop2, prop4, prop5, prop6, prop27, prop34, prop35, prop37, eVar1, eVar7, eVar10, eVar13, eVar28, eVar29, eVar32, eVar34, eVar37, eVar65, eVar69, eVar70, evar74, eVar93',
      googleObj: 'events, brand_switch_p34, brand_prod_p35, Landing_Site_p37',
    },
  },
  checkoutshipping_error: errorProps,
  checkoutBilling_error: errorProps,
  checkoutPickup_error: errorProps,
  applePay148: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: 'eVar148, events, &&events',
      googleObj: 'applePayButtonLocation_v148,events',
    },
  },
  afterpay_click: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: '',
      googleObj: 'events, afterpay_metrics_v141, afterpay_location_v131',
    },
  },
};

export default checkout;
