// 9fbef606107a605d69c0edbcd8029e5d
/**
 * This object will contain the mapping of event object keys with the event name for Account module.
 * This eventName should be same as the name passed on TRACK_PAGE_CLICK event payload
 */

import globalVars from './globalVar.constant';

const {adobe, google} = globalVars;

const wishListCommonEvents = {
  eVars: {adobeObj: adobe, googleObj: google},
  props:
    'brand_switch_p34, Landing_Site_p37, pageName, Visitor_ID_p27, prop29, events, events, productsData',
};

const SBP_PROPS = 'events, events_data';
const ADOBE_PROPS =
  'events, &&events,prop2,prop4,prop5,prop6,prop27,prop28,prop29,eVar1,eVar7,eVar8,eVar10,eVar12,eVar13,eVar14,eVar28,eVar29,eVar31,eVar32,eVar62,eVar65,eVar74,eVar93';

export const browse = {
  add_to_cart: {
    eVars: {
      adobeObj: adobe,
      googleObj:
        'Site_v1, Cust_Country_Lang_v7, Cust_Visit_Plug_in_v11, Time_Parting_v12, Cust_Login_Status_v13, Country_ID_v29, Email_Address_v31, Currency_Code_v32, Page_URL_v62, Date_v74, Site_Sections_Level_2_p4, Site_Sections_Level_3_p5, Country_ID_Pathing_p6, Analytics_Code_Ver_p28',
    },
    props: {
      adobeObj:
        'eVar29,eVar74,prop6,eVar34, prop34, prop35, eVar37, prop37, pageName, prop2, prop4, prop5, eVar26, prop27, prop29, eVar1, eVar7, eVar8, eVar10, eVar12, eVar14, eVar13, eVar32, eVar65, eVar93, events, &&events, currencyCode, &&products',
      googleObj:
        'brand_switch_p34, prop35, Landing_Site_p37, pageName,  cart_prodId_v26, Visitor_ID_p27, Store_ID_Pathing_p29, events, productsData, prod_id_p22, Internal_Search_Terms_v26, Navigation_First_Touch_v97',
    },
  },
  add_events: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'eVar29,eVar74,prop6,eVar34, brand_switch_p34, prop35, eVar37, prop37, pageName, prop2, prop4, prop5, prop26, prop27, prop29, eVar1, eVar7, eVar8, eVar10, eVar12, eVar14, eVar13, eVar32, eVar65, eVar93, events, &&events, currencyCode, &&products',
      googleObj:
        'brand_switch_p34, prop35, Landing_Site_p37, pageName, cart_prodId_v26, Visitor_ID_p27, Store_ID_Pathing_p29, events, productsData, prod_id_p22',
    },
  },
  add_to_wishlist: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'eVar34, prop34, eVar37, prop37, pageName, prop2, prop4, prop5, prop6, prop23, prop27, prop29, eVar1, eVar7, eVar8, eVar10, eVar13, eVar31, eVar32, eVar93, events, &&events, &&products',
      googleObj: 'events, productsData, productsEvents',
    },
  },
  Wishlist_Remove_Product_e53: wishListCommonEvents,
  Wishlist_Creations_e64: wishListCommonEvents,
  Wishlist_Deletions_e54: wishListCommonEvents,
  Pick_Up_In_Store_e131: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'eVar34, prop34, eVar37, prop37, pageName, prop27, prop29, eVar8, eVar10, eVar13, eVar14, eVar32, eVar93, event, &&events, &&products',
      googleObj:
        'brand_switch_p34, Landing_Site_p37, pageName, Visitor_ID_p27, Store_ID_Pathing_p29, events',
    },
  },
  Product_Availability_Checks_e105: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'eVar34, prop34, eVar37, prop37, pageName, prop27, eVar8, eVar10, eVar13, eVar14, eVar32, eVar69, eVar70, event, &&events, &&products',
      googleObj: 'brand_switch_p34, Landing_Site_p37, pageName, Visitor_ID_p27, events',
    },
  },
  Product_Availability_Success_e106: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'eVar34, prop34, eVar37, prop37, pageName, prop27, eVar8, eVar10, eVar13, eVar14, eVar32, eVar69, eVar70, event, &&events, &&products',
      googleObj: 'pageName, Visitor_ID_p27, events',
    },
  },
  Pick_Up_Later_e132: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'pageName, event, &&events, prop2, prop4, prop5, prop6, prop27, prop34, prop37, eVar1, eVar7, eVar10, eVar13, eVar28, eVar29, eVar32, eVar34, eVar37, eVar65, eVar69, eVar70, eVar74, eVar93',
      googleObj: 'Visitor_ID_p27, brand_switch_p34, Landing_Site_p37, pageName, events',
    },
  },
  Pick_Up_Later_e133: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'pageName, event, &&events, prop2, prop4, prop5, prop6, prop27, prop34, prop37, eVar1, eVar7, eVar10, eVar13, eVar28, eVar29, eVar32, eVar34, eVar37, eVar65, eVar69, eVar70, eVar74, eVar93',
      googleObj: 'Visitor_ID_p27, brand_switch_p34, pageName, events',
    },
  },
  outfitDetail: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: ADOBE_PROPS,
      googleObj: 'Visitor_ID_p27, prop28, productsData, Store_ID_Pathing_p29, events',
    },
  },
  outfitStylytics: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: ADOBE_PROPS,
      googleObj:
        'Visitor_ID_p27, prop28, productsData, Page_Name_v10, Store_ID_Pathing_p29, events,',
    },
  },
  SBP_Create_Profile_e23: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: ADOBE_PROPS,
      googleObj: SBP_PROPS,
    },
  },
  recom_drawer_close_e126: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: ADOBE_PROPS,
      googleObj: SBP_PROPS,
    },
  },
  recom_drawer_open_e125: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: ADOBE_PROPS,
      googleObj: SBP_PROPS,
    },
  },
  SBP_Finish_Profile_e24: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: ADOBE_PROPS,
      googleObj: SBP_PROPS,
    },
  },
  SBP_Incomplete_Profile: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: ADOBE_PROPS,
      googleObj: SBP_PROPS,
    },
  },
  SBP_Welcome_Back_Profile_e25: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: ADOBE_PROPS,
      googleObj: SBP_PROPS,
    },
  },
  SBP_Size_Click_Pdp_e28: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: ADOBE_PROPS,
      googleObj: SBP_PROPS,
    },
  },
  SBP_Top_Categories_Plp_e30: {
    eVars: {adobeObj: adobe, googleObj: `${google}, top_categories_plp_v116`},
    props: {
      adobeObj: ADOBE_PROPS,
      googleObj: SBP_PROPS,
    },
  },
  SBP_Top_Matches_Plp_e29: {
    eVars: {adobeObj: adobe, googleObj: `${google}, profile_name_v115, products_name_v115`},
    props: {
      adobeObj: ADOBE_PROPS,
      googleObj: SBP_PROPS,
    },
  },
  stylewith_interaction: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: '',
      googleObj: 'brand_switch_p34, prop35, Landing_Site_p37, pageName, events',
    },
  },
  atb_drawer_view_bag_e143: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: '',
      googleObj: 'events',
    },
  },
  atb_drawer_keep_shopping_e144: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: '',
      googleObj: 'events',
    },
  },
  Sort_filter_clicks_e161: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: '',
      googleObj: 'Sort_Options_v51,Refinement_Used_v50,events',
    },
  },
};

export default browse;
