// 9fbef606107a605d69c0edbcd8029e5d
/**
 * This object will contain the mapping of event object keys with the event name.
 * This eventName should be same as the name passed on TRACK_PAGE_CLICK event payload
 */
import globalVars from './globalVar.constant';

const {adobe, google} = globalVars;
const getCandidAdobeVars =
  'events, &&events,prop2,prop4,prop5,prop6,prop27,prop28,prop29,eVar1,eVar7,eVar8,eVar10,eVar12,eVar13,eVar14,eVar28,eVar29,eVar31,eVar32,eVar62,eVar65,eVar74,eVar93,eVar37,prop37';
const getCandidGoogleVars =
  'events,Country_ID_v29, Time_Parting_v12, Store_ID_v8, Site_Sections_Level_2_p4, Site_Sections_Level_3_p5, Country_ID_Pathing_p6, Cust_Country_Lang_v7, Cust_Login_Status_v13, Previous_Page_Name_v28, Email_Address_v31, Currency_Code_v32, Page_URL_v62, Page_Name_Detail_v65, Customer_ID_v93, Landing_Site_v37, Page_Type_p2, Visitor_ID_p27';
const loyaltyLocationVars = 'events,Loyalty_Location_v129';

export const global = {
  storeSelected_e67: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'eVar34, prop34, eVar37, prop37, pageName, prop27, prop29, eVar8, eVar10, eVar13, eVar32, eVar70, events, &&events',
      googleObj:
        'Store_ID_v8, Store_ID_v8, Country_ID_v29, Store_ID_Pathing_p29, Site_Sections_Level_3_p5, events',
    },
  },
  storeSearches_e89: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'eVar34, prop34, eVar37, prop37, pageName, prop2, prop4, prop5, prop6, prop21, prop27, prop29, eVar1, eVar7, eVar10, eVar13, eVar29, eVar32, eVar65, eVar69, eVar74, events',
      googleObj:
        'Landing_Site_p37, Page_Name_v10, Navigation_Pathing_p21, Store_ID_v8, Page_Type_p2, Visitor_ID_p27, Site_Sections_Level_2_p4, Site_Sections_Level_3_p5, Country_ID_Pathing_p6, Store_ID_Pathing_p29, events, Page_Name_Detail_v65',
    },
  },
  nullStoreSearchResults: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'eVar34, prop34, eVar37, prop37, pageName, prop27, eVar10, eVar13, eVar32, eVar69, eVar93, events, &&events, currencyCode',
      googleObj:
        'Landing_Site_p37, Store_Search_Criteria_v69, Visitor_ID_p27, events, Search_Results_v27, Product_Results_v30, Page_Name_Detail_v65, Prod_Finding_Method_v22',
    },
  },
  storeGetDirectionsClicks_e97: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: 'eVar34, prop34, eVar37, prop37, pageName, prop27, eVar10',
      googleObj: 'brand_switch_p34, Landing_Site_p37, Page_Name_v10, Visitor_ID_p27, Page_Name_v10',
    },
  },
  PLCC_Application_Submitted_e49: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'eVar34, prop34, eVar37, prop37, events, &&events, pageName, &&products, prop27, eVar10, eVar13, eVar31, eVar32, eVar93',
      googleObj: 'events, Page_Name_v10, Visitor_ID_p27',
    },
  },
  PLCC_CONTINUE_SHOPPING_e115: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'eVar1, eVar7, eVar8, eVar10, eVar12, pageName, events, &&events, &&products, prop2, prop4, prop5, prop6, prop21, prop27, prop28, prop29, eVar13, eVar14, eVar28, eVar29, eVar31, eVar32, eVar62, eVar65, eVar74, eVar86, eVar93, eVar98, currencyCode',
      googleObj:
        'Page_Name_v10, events,  Analytics_Code_Ver_p28, Cust_Country_Lang_v7, Store_ID_v8',
    },
  },
  PLCC_CONTINUE_TO_CHECKOUT_e114: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'eVar1, eVar7, eVar8, eVar10, eVar12, pageName, events, &&events, &&products, prop2, prop4, prop5, prop6, prop21, prop27, prop28, prop29, eVar13, eVar14, eVar28, eVar29, eVar31, eVar32, eVar62, eVar65, eVar74, eVar86, eVar93, eVar98, currencyCode',
      googleObj:
        'Page_Name_v10, events,  Analytics_Code_Ver_p28, Cust_Country_Lang_v7, Store_ID_v8',
    },
  },
  PLCC_APPLY_NOW_CLICKS_e116: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'eVar34, prop34, eVar37, prop37, events, &&events, pageName, prop2, prop4, prop5, prop6, &&products, prop27, eVar1, eVar7, eVar10, eVar13,  eVar28,  eVar29,  eVar32, eVar65, eVar74, eVar86, eVar93, eVar98, eVar102, eVar103, eVar105, currencyCode',
      googleObj:
        'brand_switch_p34, Landing_Site_p37, events, Page_Name_v10, Visitor_ID_p27, Site_Sections_Level_2_p4, Site_Sections_Level_3_p5, Country_ID_Pathing_p6',
    },
  },
  PLCC_LEARN_MORE_CLICKS_e117: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'eVar34, prop34, eVar37, prop37, events, &&events, pageName, &&products, prop27, eVar10, eVar13, eVar32, eVar93, eVar102, eVar103, eVar105, currencyCode',
      googleObj: 'events, Page_Name_v10, Visitor_ID_p27, Page_Name_v10, Cust_Login_Status_v13,',
    },
  },
  MPR_LOG_IN_CLICKS_e110: {
    eVars: {adobeObj: adobe, googleObj: google},
    props:
      'brand_switch_p34, Landing_Site_p37, events, Page_Name_v10, Visitor_ID_p27, Page_Name_v10, Cust_Login_Status_v13,',
  },
  CREATE_ACCOUNT_CLICKS_e111: {
    eVars: {adobeObj: adobe, googleObj: google},
    props:
      'brand_switch_p34, Landing_Site_p37, events, Page_Name_v10, Visitor_ID_p27, Page_Name_v10, Cust_Login_Status_v13,',
  },
  rtps_Yes: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: ' brand_switch_p34, Landing_Site_p37, events, Page_Name_v10, Visitor_ID_p27,',
  },
  rtps_No: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: ' brand_switch_p34, Landing_Site_p37, events, Page_Name_v10, Visitor_ID_p27,',
  },
  loyaltyclick: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: 'events, Page_Name_v10, Visitor_ID_p27,',
  },
  plcc_error: {
    eVars: {adobeObj: adobe, googleObj: google},
    props:
      'brand_switch_p34, brand_prod_p35, Landing_Site_p37, Site_v1, Cust_Country_Lang_v7, Page_Name_v10, Cust_Login_Status_v13, Country_ID_v29, events',
  },
  ONBoarding_Start_Shopping_Clicks_E156: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'events, &&events,pageName,&&products,prop2, prop4, prop5, prop6,prop21, prop27, prop28, eVar1, eVar7,  eVar10,eVar12, eVar13,eVar14, eVar28, eVar29, eVar32, eVar65, eVar74',
      googleObj:
        'events, Page_Name_v10, Navigation_Pathing_p21, Visitor_ID_p27, Analytics_Code_Ver_p28',
    },
  },
  ONBoarding_Login_Clicks_E155: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'prop34,prop37,prop35,events, &&events,pageName,&&products,prop2, prop4, prop5, prop6,prop21, prop27, prop28,eVar34,eVar35,eVar37, eVar1, eVar7,  eVar10,eVar12, eVar13,eVar14, eVar28, eVar29, eVar32, eVar65, eVar74',
      googleObj:
        'brand_switch_p34,Landing_Site_p37,brand_prod_p35,events, events,Page_Name_v10, Page_Type_p2, Navigation_Pathing_p21, Visitor_ID_p27, Analytics_Code_Ver_p28',
    },
  },
  ONBoarding_Join_Clicks_E154: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'prop34,prop37,prop35,events, &&events,pageName,&&products,prop2, prop4, prop5, prop6,prop21, prop27, prop28,eVar34,eVar35,eVar37, eVar1, eVar7,  eVar10,eVar12, eVar13,eVar14, eVar28, eVar29, eVar32, eVar65, eVar74',
      googleObj:
        'brand_switch_p34,Landing_Site_p37,brand_prod_p35, events,Page_Name_v10,Page_Type_p2, Navigation_Pathing_p21, Visitor_ID_p27, Analytics_Code_Ver_p28',
    },
  },
  Footer_Tab_Clicks_E158: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'prop35, prop34, prop2, prop21, eVar34, eVar35, eVar14, eVar28, events, &&events, &&products',
      googleObj: 'Visitor_ID_p27, brand_switch_p34, Page_Type_p2, Navigation_Pathing_p21',
    },
  },
  navigation_l1: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'events,prop34, prop37, prop35, pageName, &&products, prop2, prop4, prop5, prop6, prop21, prop27, prop28, eVar34, eVar35, eVar37, eVar1, eVar7, eVar10, eVar12, eVar13, eVar14, eVar28, eVar29, eVar32, eVar65, eVar74, eVar93',
      googleObj:
        'brand_switch_p34, Landing_Site_p37, Page_Name_v10, Page_Type_p2, Navigation_Pathing_p21, Visitor_ID_p27, Analytics_Code_Ver_p28',
    },
  },
  navigation_l2: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'events,prop34, prop37, prop35, pageName, &&products, prop2, prop4, prop5, prop6, prop21, prop27, prop28, eVar34, eVar35, eVar37, eVar1, eVar7, eVar10, eVar12, eVar13, eVar14, eVar28, eVar29, eVar32, eVar65, eVar74, eVar93',
      googleObj:
        'brand_switch_p34, Landing_Site_p37, brand_prod_p35, Page_Name_v10, Page_Type_p2, Navigation_Pathing_p21, Visitor_ID_p27, Analytics_Code_Ver_p28',
    },
  },
  form_error: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'prop2, prop4, prop5, prop6, prop27, prop34, prop35, prop37, eVar1, eVar7, eVar10, eVar13, eVar29, eVar32, eVar34, eVar37, eVar45, eVar65, eVar74, eVar86, eVar93, eVar98, events, List_Var_3',
      googleObj:
        'brand_switch_p34, brand_prod_p35, Landing_Site_p37, brand_switch_v34, eVar45, Cart_Type_v86, Checkout_Type_v98, events, List_Var_3',
    },
  },

  getCandidImageClick_e150: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: getCandidAdobeVars,
      googleObj: getCandidGoogleVars,
    },
  },

  getCandidSeeMore_e151: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: getCandidAdobeVars,
      googleObj: getCandidGoogleVars,
    },
  },
  getCandidOnLoadDetailView_e152: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: getCandidAdobeVars,
      googleObj: getCandidGoogleVars,
    },
  },
  getCandidDetailView_e153: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: getCandidAdobeVars,
      googleObj: getCandidGoogleVars,
    },
  },
  getCandidAddToBag_e154: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: getCandidAdobeVars,
      googleObj: getCandidGoogleVars,
    },
  },
  essentials: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'prop2, prop4, prop5, prop6, prop27, prop21, prop28, prop37, prop34, events, eVar107, eVar1, eVar7, eVar10, eVar13, eVar19, eVar26, eVar27, eVar28, eVar29, eVar32, eVar34, eVar37, eVar59, eVar62, eVar65, eVar74, eVar93, eVar97',
      googleObj:
        'Analytics_Code_Ver_p28, Landing_Site_p37, brand_switch_p34, events, Internal_Search_Terms_v26, Search_Results_v27',
    },
  },
  Loyalty_Create_Account_Clicks_e125: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: '',
      googleObj: loyaltyLocationVars,
    },
  },
  Loyalty_Login_Clicks_e126: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: '',
      googleObj: loyaltyLocationVars,
    },
  },
  Loyalty_Create_Account_Success_e127: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: '',
      googleObj: loyaltyLocationVars,
    },
  },
  Loyalty_Login_Success_e128: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: '',
      googleObj: loyaltyLocationVars,
    },
  },
  Loyalty_Apply_Now_Click_e129: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: '',
      googleObj: loyaltyLocationVars,
    },
  },
  Loyalty_Learn_More_e130: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: '',
      googleObj: loyaltyLocationVars,
    },
  },
  Loyalty_Apply_Or_Accept_Offer_e131: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: '',
      googleObj: loyaltyLocationVars,
    },
  },
  Loyalty_PLCC_Submit_e132: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: '',
      googleObj: loyaltyLocationVars,
    },
  },
  Loyalty_PLCC_No_Thanks_e133: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: '',
      googleObj: loyaltyLocationVars,
    },
  },
  Loyalty_MPR_Image_Click_e134: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: '',
      googleObj: loyaltyLocationVars,
    },
  },
  Loyalty_PLCC_Image_Click_e135: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: '',
      googleObj: loyaltyLocationVars,
    },
  },
  Loyalty_Join_Club_Birthday_e136: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: '',
      googleObj: loyaltyLocationVars,
    },
  },
  Loyalty_Continue_As_Guest_e137: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: '',
      googleObj: loyaltyLocationVars,
    },
  },
  OnBoarding_Skips_p155: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: '',
      googleObj: 'events',
    },
  },
  PDP_Boss_Bopis_Size_drawer: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: '',
      googleObj: 'events, Shipping_Preferences_Location_v137',
    },
  },
  homePageCardClicks: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: '',
      googleObj: 'Internal_Campaigns_v19, events',
    },
  },
  BOPIS_MODAL: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: '',
      googleObj: 'events',
    },
  },
  PDP_Boss_Bopis_No_Rush_DL_Methods: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: '',
      googleObj: 'events, Shipping_Preferences_Location_v137',
    },
  },
  SBP_Making_More_Discoverable: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: '',
      googleObj: 'events',
    },
  },
};

export default global;
