// 9fbef606107a605d69c0edbcd8029e5d
/**
 * This object will contain the mapping of event object keys with the event name for Account module.
 * This eventName should be same as the name passed on TRACK_PAGE_CLICK event payload
 */

import globalVars from './globalVar.constant';

const {adobe, google} = globalVars;

const errorProps = {
  eVars: {adobeObj: adobe, googleObj: google},
  props: {
    adobeObj:
      'prop2, prop34, prop35, prop37, eVar32, eVar34, eVar37, eVar45, eVar86, eVar93, eVar98, events',
    googleObj: 'Visitor_ID_p27, brand_switch_p34, prop35, Landing_Site_p37, events,  List_Var_3',
  },
};

export const account = {
  manageCreditAccount_e113: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'eVar34, prop34, eVar37, prop37, pageName, prop2, prop27, prop29, eVar8, eVar31, eVar93, currencyCode, events',
      googleObj:
        'brand_switch_p34, Customer_ID_v93, pageName, Cust_Login_Status_v13, Page_Name_Detail_v65, events',
    },
  },
  orderTrackingClick_e118: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'eVar34, prop34, eVar37, prop37, pageName, prop2, prop29, eVar8, eVar28, eVar32, eVar93, events',
      googleObj:
        'brand_switch_p34, Landing_Site_p37, Customer_ID_v93,Cust_Login_Status_v13, Date_v74, Page_Name_Detail_v65, Visitor_ID_p27, Store_ID_Pathing_p29, events, Cust_Country_Lang_v7',
    },
  },
  Logins_e14: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: 'eVar34, prop34, eVar37, prop37, prop21, eVar32, eVar93, events, currencyCode',
      googleObj:
        'brand_switch_p34, Landing_Site_p37, Navigation_Pathing_p21, Visitor_ID_p27, Registration_Source_v33, Email_Address_v31, Customer_ID_v93,events,Loyalty_Location_v129',
    },
  },
  siteRegistration_e13: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'eVar34, prop34, eVar37, prop37, prop21, eVar32, eVar93, eVar33, events, currencyCode',
      googleObj:
        'brand_switch_p34, Landing_Site_p37, Navigation_Pathing_p21, Visitor_ID_p27, Email_Address_v31, events, Registration_Source_v33,Loyalty_Location_v129',
    },
  },
  birthdayClubSign_ups_e16: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'eVar34, prop34, eVar37, prop37, pageName, prop2, prop21, prop29, eVar8, eVar28, eVar32, eVar62, eVar93, events',
      googleObj:
        'brand_switch_p34, Landing_Site_p37, pageName, Navigation_Pathing_p21, Visitor_ID_p27, Cust_Login_Status_v13, Cust_Country_Lang_v7,Date_v74, Store_ID_Pathing_p29, events, Customer_ID_v93',
    },
  },
  birthdayClubSuccess_e16: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'eVar34, prop34, eVar37, prop37, pageName, prop2, prop21, prop29, eVar8, eVar28, eVar32, eVar62, eVar93, events',
      googleObj:
        'brand_switch_p34, Landing_Site_p37, pageName, Navigation_Pathing_p21, Visitor_ID_p27, Cust_Login_Status_v13, Cust_Country_Lang_v7,Date_v74, Store_ID_Pathing_p29, events, Customer_ID_v93',
    },
  },
  editoraddaddress_error: errorProps,
  addoreditcreditcard_error: errorProps,
  personalinformation_error: errorProps,
  createaccount_error: errorProps,
  login_error: errorProps,
  addgiftcardcard_error: errorProps,
  addchild_error: errorProps,
  account_cta: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj:
        'eVar34, prop34, eVar37, prop37, pageName, prop2, prop21, prop29, eVar8, eVar28, eVar32, eVar93',
      googleObj:
        'brand_switch_p34, Landing_Site_p37, pageName, Navigation_Pathing_p21, Visitor_ID_p27, Analytics_Code_Ver_p28, Store_ID_Pathing_p29',
    },
  },
  SBP_Gender_v54: {
    eVars: {
      adobeObj: adobe,
      googleObj: google,
    },
    props: {
      adobeObj: '',
      googleObj: 'gender_v54',
    },
  },
  SBP_Profiles: {
    eVars: {
      adobeObj: adobe,
      googleObj: google,
    },
    props: {
      adobeObj: '',
      googleObj: 'sbp_no_of_profiles_v46, sbp_name_of_profiles_v42, sbp_email_v47',
    },
  },
  chatbot_e123: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: 'events, &&events',
      googleObj: 'events',
    },
  },
  orderHelpLocation_click: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: '',
      googleObj: 'orderHelp_location_p126',
    },
  },
  livechat_open: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: 'events, &&events',
      googleObj: 'events',
    },
  },
  orderHelpPath_click: {
    eVars: {adobeObj: adobe, googleObj: google},
    props: {
      adobeObj: '',
      googleObj: 'orderHelp_path_p128',
    },
  },
};

export default account;
