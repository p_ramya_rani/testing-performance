/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
let addedEvents = [];

const getItemCategory3 = p => {
  let str = '1';
  if (p.position && !p.outfitId) {
    str = p.position.toString();
  }
  if (p.mpackPageType) {
    str = `${str}|${p.mpackPageType}`;
  }
  if (p.mpackValue) {
    str = `${str}|${p.mpackValue}`;
  }
  return str;
};

/* eslint-disable */
export const getProducts = (prodData, s = {}) => {
  if (!prodData) {
    prodData = [];
  }
  const prods = [];
  var listing = {};
  for (let i = 0; i < prodData.length; i++) {
    let p = prodData[i];
    let thisProd;
    if (p.id) {
      var extPrice = p.extPrice ? p.extPrice.toFixed(2) : '';
      thisProd = ['', p.id, p.quantity || '', extPrice, '', ''];

      var merch = [],
        evts = [];
      var prodListing = {},
        evtListing = {};
      prodListing.item_id = p.id.toString();
      prodListing.item_name = p.name;
      prodListing.item_brand = p.prodBrand === undefined ? 'TCP' : p.prodBrand;
      prodListing.item_list_name = s.Page_Type_p2 === undefined ? 'browse' : s.Page_Type_p2;
      prodListing.item_category =
        (p.color === undefined ? ' ' : p.color) +
        '|' +
        (p.rating === undefined ? ' ' : p.rating) +
        ':' +
        (p.reviews === undefined ? ' ' : p.reviews) +
        '|' +
        (p.addType === undefined ? ' ' : p.addType) +
        '|' +
        (p.size === null || p.size === undefined ? ' ' : p.size);

      prodListing.item_category2 =
        (p.recsProductId && p.recsPageType ? p.recsProductId : ' ') +
        '|' +
        (p.recsPageType === undefined ? ' ' : p.recsPageType) +
        '|' +
        (p.recsType === undefined ? ' ' : p.recsType);
      prodListing.item_category3 = getItemCategory3(p);
      if (p.price && p.listPrice) {
        merch.push(
          'Current_vs_Original_Price_v63=' +
            p.price.toFixed(2) +
            ' ' +
            _dataLayer.currencyCode +
            ':' +
            p.listPrice.toFixed(2) +
            ' ' +
            _dataLayer.currencyCode,
        );
        prodListing.item_category4 =
          (p.quantity === undefined || p.quantity === null ? 1 : p.quantity) +
          '|' +
          (p.price * (p.quantity === undefined || p.quantity === null ? 1 : p.quantity))
            .toFixed(2)
            .toString() +
          '|' +
          (p.sbp === true && 'SBP_v38=SBP');

        prodListing.item_list_id =
          (p.price === undefined ? ' ' : p.price.toFixed(2)).toString() +
          ' ' +
          'USD' +
          '|' +
          (p.listPrice === undefined ? ' ' : p.listPrice.toFixed(2)).toString() +
          ' ' +
          'USD';
      }
      if (p.recsPageType || p.plpClick || p.searchClick || p.recsType) {
        prodListing.item_category2 =
          (p.recsProductId === undefined ? ' ' : p.recsProductId) +
          '|' +
          (p.recsPageType === undefined ? ' ' : p.recsPageType) +
          '|' +
          (p.recsType === undefined ? ' ' : p.recsType);
      }

      addedEvents = evts;
      thisProd[4] = evts.join('|');
      thisProd[5] = merch.join('|');
      prods.push(prodListing);
    }
  }

  if (!s.pageLoaded) {
    if (
      global._dataLayer &&
      _dataLayer.internalCampaignIdList &&
      _dataLayer.internalCampaignIdList.length > 0
    ) {
      var icids = _dataLayer.internalCampaignIdList;
      for (var i = 0; i < icids.length; i++) {
        listing = icids[i];
        // prods.push(';icidlink;;;internalCampaignImpressions_e80=1;eVar90=' + icids[i]);
      }
    }
    if (s.Internal_Campaigns_v19) {
      listing = s.Internal_Campaigns_v19;

      // prods.push(
      // ';icidlink;;;internalCampaignImpressions_e81=1;eVar90=' + s.Internal_Campaigns_v19,
      // );
    }
  }

  return prods;
};

export const getProductsEvents = (prodData, s = {}) => {
  if (!prodData) {
    prodData = [];
  }
  const prods = [];
  var listing = {};
  let eventsListing = [];
  let prodDataLength = prodData.length;
  let prodDataPrice = 0;
  for (let i = 0; i < prodData.length; i++) {
    prodDataPrice += prodData[i].extPrice;
  }

  for (let i = 0; i < prodData.length; i++) {
    var p = prodData[i],
      thisProd;
    if (p.id) {
      var extPrice = p.extPrice ? p.extPrice.toFixed(2) : '';
      thisProd = ['', p.id, p.quantity || '', extPrice, '', ''];

      var merch = [],
        evts = [],
        evtListing = {};

      if (p.prodBrand) {
        merch.push('Brand_v35=' + p.prodBrand.toUpperCase());
      } else if (_dataLayer.brandId) {
        merch.push('Brand_v35=' + _dataLayer.brandId.toUpperCase());
      }
      if (p.shippingMethod) {
        merch.push('eVar5=' + p.shippingMethod);
      }
      if (p.storeId) {
        merch.push('eVar53=' + p.storeId);
      }
      if (p.outfitId) {
        merch.push('eVar60=' + p.outfitId);
        if (p.stylitics && p.stylitics == 'true') {
          evts.push('event74=1');
          evtListing.event74 = '1';
          if (p.uniqueOutfitId) {
            evts.push('event95=1');
            evtListing.event95 = '1';
          }
        } else if (s.Page_Type_p2 == 'outfit') {
          evts.push('event75=1');
          evtListing.event75 = '1';
        }
      }
      if (p.offerPrice) {
        evts.push('event137=' + p.offerPrice.toFixed(2));
        evtListing.event137 = p.offerPrice.toFixed(2);
      }
      if (p.sflExtPrice) {
        evts.push('event136=' + p.sflExtPrice.toFixed(2));
        evtListing.event136 = p.sflExtPrice.toFixed(2);
      }
      if (p.price && p.listPrice) {
        merch.push(
          'Current_vs_Original_Price_v63=' +
            p.price.toFixed(2) +
            ' ' +
            _dataLayer.currencyCode +
            ':' +
            p.listPrice.toFixed(2) +
            ' ' +
            _dataLayer.currencyCode,
        );
      }
      if (p.pricingState) {
        merch.push('Product_Pricing_State_v67=' + p.pricingState);
      }
      if (p.rating && p.reviews) {
        merch.push('Product_Rating_And_Reviews_v71=' + p.rating + ':' + p.reviews);
      }
      if (p.color) {
        merch.push('eVar77=' + p.color);
      }
      if (p.type) {
        merch.push('eVar80=' + p.type);
      }
      if (p.upc) {
        merch.push('eVar82=' + p.upc);
      }
      if (p.gender) {
        merch.push('eVar83=' + p.gender);
      }
      if (p.size) {
        merch.push('eVar88=' + p.size);
      }
      if (p.colorId) {
        merch.push('Product_Color_ID_v89=' + p.colorId);
      }
      if (p.sku) {
        merch.push('eVar95=' + p.sku);
      }
      if (p.position && !p.outfitId) {
        merch.push('Product_List_Position_Clicked_v100=' + p.position);
        evts.push('event92=1');
        evtListing.event92 = '1';
      }
      if (s.Page_Type_p2 == 'checkout' && (!s.orderId_v3 || s.orderId_v3 === '')) {
        evts.push('Added_to_Checkout_e86=' + extPrice);
        evtListing.event86 = extPrice;
      }

      if (s.orderId_v3) {
        evtListing.itemsPrice = prodDataPrice.toString();
        evtListing.itemsCount = prodDataLength.toString();
        if (p.giftCardAmount) {
          evts.push('Gift_Card_Amount_Used_e6=' + p.giftCardAmount.toFixed(2));
          evtListing.Gift_Card_Amount_Used_e6 = p.giftCardAmount.toFixed(2);
        }
        if (p.couponAmount) {
          evts.push('Discount_Amount_e7=' + p.couponAmount.toFixed(2));
          evtListing.coupon = p.couponAmount.toFixed(2);
        }
        if (p.savingsAmount) {
          evts.push('Refer_a_friend_Sign_ups_e22=' + p.savingsAmount.toFixed(2));
          evtListing.Refer_a_friend_Sign_ups_e22 = p.savingsAmount.toFixed(2);
        }
        if (p.shippingAmount) {
          evts.push('Shipping_Amount_e78=' + p.shippingAmount.toFixed(2));
          evtListing.Shipping_Amount_e78 = p.shippingAmount.toFixed(2);
        }
        if (p.taxAmount) {
          evts.push('Tax_Amount_e79=' + p.taxAmount.toFixed(2));
          evtListing.Tax_Amount_e79 = p.taxAmount.toFixed(2);
        }
        if (p.netRevenue) {
          evts.push('Net_Revenue_e99=' + p.netRevenue.toFixed(2));
          evtListing.Net_Revenue_e99 = p.netRevenue.toFixed(2);
        }
        if (_dataLayer && _dataLayer.giftType) {
          merch.push('eVar57=' + _dataLayer.giftType);
        }
        if (p.multipleSkus) {
          merch.push('eVar87=' + p.multipleSkus);
        }
      } else {
        if (p.cartPrice) {
          evts.push('event61=' + p.cartPrice.toFixed(2));
          evtListing.value = p.cartPrice.toFixed(2);
        }
      }
      if (p.recsPageType || p.plpClick || p.searchClick || p.recsType) {
        if (p.searchClick && p.searchClick == 'true') {
          evts.push('event83=1');
          evtListing.event83 = '1';
        }
        if (p.plpClick && p.plpClick) {
          evts.push('event93=1');
          evtListing.event93 = '1';
        }
        var cs = false;
        if (p.recsProductId) {
          merch.push('Cross_Sell_Referring_Product_v20=' + p.recsProductId);
          evtListing.Cross_Sell_Referring_Product_v20 = p.recsProductId;
          cs = true;
        }
        if (p.recsPageType) {
          merch.push('Cross_Sell_Location_v21=' + p.recsPageType);
          evtListing.Cross_Sell_Location_v21 = p.recsPageType;
          cs = true;
        }
        if (p.recsType) {
          merch.push('Cross_Sell_Driver_v92=' + p.recsType);
          evtListing.Cross_Sell_Driver_v92 = p.recsType;
          cs = true;
        }
        if (cs) {
          evts.push('Cross_Sell_Clicks_e50=1');
          evtListing.Cross_Sell_Clicks_e50 = '1';
        }
        if (p.features) {
          merch.push('eVar72=' + p.features.toLowerCase());
        }
      }

      if (s.linkName) {
        if (s.linkName.indexOf('wishlist add item') > -1) {
          evts.push('Added_to_Wishlist_e94=' + extPrice);
          evtListing.Added_to_Wishlist_e94 = extPrice;
          evtListing.Wishlist_Add_Product_e52 = '1';
        } else if (s.linkName == 'cart add') {
          merch.push('eVar2=' + p.addType);
          evts.push('event85=' + extPrice);
          evtListing.event85 = extPrice;
        }
      }

      addedEvents = evts;
      thisProd[4] = evts.join('|');
      thisProd[5] = merch.join('|');
      prods.push(evtListing);
    }
  }

  if (!s.pageLoaded) {
    if (
      global._dataLayer &&
      _dataLayer.internalCampaignIdList &&
      _dataLayer.internalCampaignIdList.length > 0
    ) {
      var icids = _dataLayer.internalCampaignIdList;
      for (var i = 0; i < icids.length; i++) {
        listing = icids[i];
        // prods.push(';icidlink;;;internalCampaignImpressions_e80=1;eVar90=' + icids[i]);
      }
    }
    if (s.Internal_Campaigns_v19) {
      listing = s.Internal_Campaigns_v19;

      // prods.push(
      // ';icidlink;;;internalCampaignImpressions_e81=1;eVar90=' + s.Internal_Campaigns_v19,
      // );
    }
  }

  return prods;
};

export const getAdobeProducts = (prodData = [], s = {}) => {
  try {
    const prods = [];
    for (let i = 0; i < prodData.length; i++) {
      var p = prodData[i],
        thisProd;
      if (p.id) {
        var extPrice = p.extPrice ? p.extPrice.toFixed(2) : '';
        thisProd = ['', p.id, p.quantity || '', extPrice, '', ''];

        var merch = [],
          evts = [];
        if (p.prodBrand) {
          merch.push('eVar35=' + p.prodBrand.toUpperCase());
        } else if (_dataLayer.brandId) {
          merch.push('eVar35=' + _dataLayer.brandId.toUpperCase());
        }
        if (p.shippingMethod) {
          merch.push('eVar5=' + p.shippingMethod);
        }
        if (p.storeId) {
          merch.push('eVar53=' + p.storeId);
        }
        if (p.outfitId) {
          merch.push('eVar60=' + p.outfitId);
          if (p.stylitics && p.stylitics == 'true') {
            evts.push('event74=1');
            if (p.uniqueOutfitId) {
              evts.push('event95=1');
            }
          } else if (s.prop2 == 'outfit') {
            evts.push('event75=1');
          }
        }
        if (p.offerPrice) {
          evts.push('event137=' + p.offerPrice.toFixed(2));
        }
        if (p.sflExtPrice) {
          evts.push('event136=' + p.sflExtPrice.toFixed(2));
        }
        if (p.price && p.listPrice) {
          merch.push(
            'eVar63=' +
              p.price.toFixed(2) +
              ' ' +
              _dataLayer.currencyCode +
              ':' +
              p.listPrice.toFixed(2) +
              ' ' +
              _dataLayer.currencyCode,
          );
        }
        if (p.pricingState) {
          merch.push('eVar67=' + p.pricingState);
        }
        if (p.rating && p.reviews) {
          merch.push('eVar71=' + p.rating + ':' + p.reviews);
        }
        if (p.color) {
          merch.push('eVar77=' + p.color);
        }
        if (p.type) {
          merch.push('eVar80=' + p.type);
        }
        if (p.upc) {
          merch.push('eVar82=' + p.upc);
        }
        if (p.gender) {
          merch.push('eVar83=' + p.gender);
        }
        if (p.size) {
          merch.push('eVar88=' + p.size);
        }
        if (p.colorId) {
          merch.push('eVar89=' + p.colorId);
        }
        if (p.sku) {
          merch.push('eVar95=' + p.sku);
        }
        if (p.position) {
          merch.push('eVar100=' + p.position);
          evts.push('event92=1');
        }
        if (s.prop2 == 'checkout' && (!s.eVar3 || s.eVar3 === '')) {
          evts.push('event86=' + extPrice);
        }

        if (s.eVar3) {
          if (p.giftCardAmount) {
            evts.push('event6=' + p.giftCardAmount.toFixed(2));
          }
          if (p.couponAmount) {
            evts.push('event7=' + p.couponAmount.toFixed(2));
          }
          if (p.savingsAmount) {
            evts.push('event22=' + p.savingsAmount.toFixed(2));
          }
          if (p.shippingAmount) {
            evts.push('event78=' + p.shippingAmount.toFixed(2));
          }
          if (p.taxAmount) {
            evts.push('event79=' + p.taxAmount.toFixed(2));
          }
          if (p.netRevenue) {
            evts.push('event99=' + p.netRevenue.toFixed(2));
          }
          if (_dataLayer && _dataLayer.giftType) {
            merch.push('eVar57=' + _dataLayer.giftType);
          }
          if (p.multipleSkus) {
            merch.push('eVar87=' + p.multipleSkus);
          }
        } else {
          if (p.cartPrice) {
            evts.push('event61=' + p.cartPrice.toFixed(2));
          }
        }
        if (p.recsPageType || p.plpClick || p.searchClick || p.recsType) {
          if (p.searchClick && p.searchClick == 'true') {
            evts.push('event83=1');
          }
          if (p.plpClick) {
            evts.push('event93=1');
          }
          var cs = false;
          if (p.recsProductId) {
            merch.push('eVar20=' + p.recsProductId);
            cs = true;
          }
          if (p.recsPageType) {
            merch.push('eVar21=' + p.recsPageType);
            cs = true;
          }
          if (p.recsType) {
            merch.push('eVar92=' + p.recsType);
            cs = true;
          }
          if (cs) {
            evts.push('event50=1');
          }
          if (p.features) {
            merch.push('eVar72=' + p.features.toLowerCase());
          }
        }

        if (s.linkName) {
          if (s.linkName.indexOf('wishlist add item') > -1) {
            evts.push('event94=' + extPrice);
          } else if (s.linkName == 'cart add') {
            merch.push('eVar2=' + p.addType);
            evts.push('event85=' + extPrice);
          }
        }

        addedEvents = evts;
        thisProd[4] = evts.join('|');
        thisProd[5] = merch.join('|');
        prods.push(thisProd.join(';'));
      }
    }

    if (!s.pageLoaded) {
      if (
        global._dataLayer &&
        _dataLayer.internalCampaignIdList &&
        _dataLayer.internalCampaignIdList.length > 0
      ) {
        var icids = _dataLayer.internalCampaignIdList;
        for (var i = 0; i < icids.length; i++) {
          prods.push(';icidlink;;;event80=1;eVar90=' + icids[i]);
        }
      }
      if (s.eVar19) {
        prods.push(';icidlink;;;event81=1;eVar90=' + s.eVar19);
      }
    }

    return prods.join(',');
  } catch (ex) {
    console.log('Exception in getAdobeProducts', ex);
  }
};

export const addedEventsInProds = (convertedData, s) => {
  getAdobeProducts(convertedData, s);
  const evtsInProString =
    addedEvents && addedEvents.length > 0
      ? addedEvents.map(item => {
          return item.split('=')[0];
        })
      : [];
  addedEvents = [];

  return evtsInProString.join(',');
};

export const getTimeParting = new Function(
  'h',
  'z',
  '' +
    "var s=this,od;od=new Date('1/1/2000');if(od.getDay()!=6||od.getMont" +
    "h()!=0){return'Data Not Available';}else{var H,M,D,U,ds,de,tm,da=['" +
    "Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturda" +
    "y'],d=new Date();z=z?z:0;z=parseFloat(z);if(s._tpDST){var dso=s._tp" +
    "DST[d.getFullYear()].split(/,/);ds=new Date(dso[0]+'/'+d.getFullYea" +
    "r());de=new Date(dso[1]+'/'+d.getFullYear());if(h=='n'&&d>ds&&d<de)" +
    "{z=z+1;}else if(h=='s'&&(d>de||d<ds)){z=z+1;}}d=d.getTime()+(d.getT" +
    'imezoneOffset()*60000);d=new Date(d+(3600000*z));H=d.getHours();M=d' +
    ".getMinutes();M=M<30?'00':'30';D=d.getDay();U=' AM';if(H>=12){U=' P" +
    "M';H=H-12;}if(H==0){H=12;}D=da[D];tm=H+':'+M+U;return(tm+'|'+D);}",
);
