// 9fbef606107a605d69c0edbcd8029e5d 
const globalVars = {
  adobe:
    'eVar1, eVar7, eVar10, eVar11, eVar12, eVar13, eVar29, eVar31, eVar37, eVar62, eVar65, eVar74, prop4, prop5, prop6, prop27, prop28, prop30, prop37',
  google:
    'Site_v1, Cust_Country_Lang_v7, Page_Name_v10, Cust_Visit_Plug_in_v11, Time_Parting_v12, Cust_Login_Status_v13, Country_ID_v29, Email_Address_v31, Currency_Code_v32, Page_URL_v62, Page_Name_Detail_v65, Date_v74, Site_Sections_Level_2_p4, Site_Sections_Level_3_p5, Country_ID_Pathing_p6, Visitor_ID_p27, Analytics_Code_Ver_p28',
};

export default globalVars;
