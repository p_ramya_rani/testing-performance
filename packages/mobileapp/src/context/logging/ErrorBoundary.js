// 9fbef606107a605d69c0edbcd8029e5d
// Disabling eslint for temporary fix
import React from 'react';
import PropTypes from 'prop-types';
import {Box, Text} from '@fabulas/astly';
import {useErrorReporter} from './ErrorReportProvider';

export class ErrorBoundary extends React.Component {
  state = {hasError: false};

  static getDerivedStateFromError() {
    // Update state so the next render will show the fallback UI.
    return {hasError: true};
  }

  componentDidCatch(error) {
    const {logger} = this.props;
    logger.report({error});
  }

  render() {
    const {hasError} = this.state;
    const {children} = this.props;
    if (hasError) {
      return (
        // eslint-disable-next-line
        <Box
          // eslint-disable-next-line react-native/no-inline-styles
          style={{flex: 1, alignContent: 'center', justifyContent: 'center'}}>
          <Text>Oh no an error has occurred!</Text>
        </Box>
      );
    }
    return (
      // eslint-disable-next-line
      <Box style={{flex: 1, alignContent: 'center', justifyContent: 'center'}}>{children}</Box>
    );
  }
}

ErrorBoundary.propTypes = {
  logger: PropTypes.func.isRequired,
  children: PropTypes.node,
};
ErrorBoundary.defaultProps = {
  children: null,
};

export default (props) => {
  const error = useErrorReporter();
  // eslint-disable-next-line
  return <ErrorBoundary logger={error}>{props.children}</ErrorBoundary>;
};
