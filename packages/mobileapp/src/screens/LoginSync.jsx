// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import LoginPageContainer from '@tcp/core/src/components/features/account/LoginPage';

const Login = props => {
  return (
    <React.Fragment>
      <LoginPageContainer {...props} />
    </React.Fragment>
  );
};

export default Login;
