// 9fbef606107a605d69c0edbcd8029e5d
import {NavigationActions, StackActions} from 'react-navigation';
import get from 'lodash/get';
import {navigateToNestedRoute} from '@tcp/core/src/utils/utils.app';

let navigator;

const setTopLevelNavigator = navigatorRef => {
  navigator = navigatorRef;
};

const navigateToProductPage = params => {
  setTimeout(() => {
    navigator.dispatch(
      StackActions.reset({
        index: 2,
        actions: [
          NavigationActions.navigate({routeName: 'Home'}),
          NavigationActions.navigate({routeName: 'Bag'}),
          NavigationActions.navigate({
            routeName: 'Bag',
            action: NavigationActions.navigate({routeName: 'BagProductDetail', params}),
          }),
        ],
      }),
    );
  }, 1000);
};

const navigateToSBP = params => {
  setTimeout(() => {
    const profile = JSON.parse(params.sbpProfile);
    profile.refresh = true;
    navigator.dispatch(
      StackActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({
            routeName: 'Home',
            action: NavigationActions.navigate({
              routeName: 'SbpPlpStack',
              action: NavigationActions.navigate({
                routeName: 'PLP',
                params: JSON.parse(params.sbpProfile),
              }),
            }),
          }),
        ],
      }),
    );
  }, 1000);
};

/**
 * @function: resetToRoute
 * This is private method which call when reset the routes
 * @param { object } routeName navigate route name
 * @param { object } params params
 * @param { object } actions action
 */
const navigateToRoute = (routeName, params, action) => {
  return NavigationActions.navigate({
    routeName,
    params,
    action,
  });
};

/**
 * @function: resetToRoute
 * This is private method which call when reset the routes
 * @param { object } index reset route index
 * @param { object } actions action
 * @param { object } key key
 */
const resetToRoute = (index, actions, key) => {
  return StackActions.reset({
    index,
    actions,
    key,
  });
};

/**
 * This function will return the routeName and params for current active route.
 * @param { object } navigationState current navigation state
 */
const getActiveRoute = navigationState => {
  if (!navigationState) {
    return null;
  }
  const route = navigationState.routes[navigationState.index];
  // dive into nested navigators
  if (route.routes) {
    return getActiveRoute(route);
  }
  const {routeName, params} = route || {};
  return {
    routeName,
    params,
  };
};

/**
 * @function: navigateToPage
 * This method call when want to move particular page
 * @param {Object} routName - route name
 */
const navigateToPage = (routeName, params) => {
  const action = NavigationActions.navigate({
    routeName,
    params,
  });
  if (navigator) {
    navigator.dispatch(action);
  }
};

/**
 * @function: getActiveRootRouteName
 * This method return current root stack name based on the passed state
 * @param {Object} currentState - previous or current passed state
 * @param {Object} currentScreenName - matching screen name
 */
const getActiveRootRouteName = (currentState, currentScreenName) => {
  // eslint-disable-next-line sonarjs/no-duplicate-string
  const routes = get(currentState, 'routes[0].routes[0]', null);
  const route = routes.routes.filter(
    routeItem =>
      routeItem.routes &&
      routeItem.routes.length > 0 &&
      routeItem.routes.find(element => element.routeName === currentScreenName),
  );
  return route && route.length > 0 && route[0].routeName;
};

/**
 * @function: getRoute
 * This method return current route based on the matching string
 * @param {Object} routes - routes
 * @param {Object} matchString - matching string
 */
const getRoute = (routes, matchString) => {
  return (
    routes &&
    routes.filter(
      routeItem =>
        routeItem.routes &&
        routeItem.routes.length > 0 &&
        routeItem.routes.find(element => element.routeName === matchString),
    )
  );
};

/**
 * @function: reset
 * This method responsible for reset the stack
 * @param {Object} options  - route options
    {
      index: <route index>,
      key: <route key>,
      routes: <stack routes>
    }
 */
const reset = options => {
  const actions = options.routes.map(route =>
    navigateToRoute(route.routeName, {...route.params, ...options.extraParams}),
  );
  if (navigator) {
    navigator.dispatch(resetToRoute(options.index, actions, options.key));
  }
};

const sbpMatchingString = (prevScreen, sbpPdpRouteName, sbpOutfitDetailRouteName, pdpRouteName) => {
  return prevScreen.routeName === sbpPdpRouteName ||
    prevScreen.routeName === sbpOutfitDetailRouteName
    ? sbpPdpRouteName
    : pdpRouteName;
};

/**
 * @function: onNavigationStateChange
 * This method call when stack change
 * @param {Object} prevState - previous stack
 * @param {Object} currentState - current stack
 */

// eslint-disable-next-line complexity
const onNavigationStateChange = (prevState, currentState) => {
  const currentScreen = getActiveRoute(currentState);
  const prevScreen = getActiveRoute(prevState);
  const pdpRouteName = 'ProductDetail';
  const sbpPdpRouteName = 'ProductDetailSBP';
  const sbpOutfitDetailRouteName = 'OutfitDetailSBP';
  let routes;
  let route;
  // [RWD-18227] Mobile App | Account - Tapping on the bottom Nav icon again should reset the page
  if (
    prevScreen &&
    currentScreen &&
    prevScreen.routeName === 'Account' &&
    currentScreen.routeName === 'Account' &&
    currentScreen.params &&
    currentScreen.params.isResetStack
  ) {
    routes = get(currentState, 'routes[0].routes[0]', null);
    route = getRoute(routes.routes, 'Account');
    reset({
      index: 0,
      key: route[0].key,
      routes: route[0].routes,
      extraParams: {isResetStack: false},
    });
  } else if (
    prevScreen &&
    currentScreen &&
    (prevScreen.routeName === pdpRouteName ||
      prevScreen.routeName === sbpPdpRouteName ||
      prevScreen.routeName === sbpOutfitDetailRouteName) &&
    prevScreen.routeName !== currentScreen.routeName
  ) {
    // [RWD-18415] if PDP screen is opened from  any stack and try to open another pdp page from any other stack then first reset the ProductDetail open stack
    const previousRootRouteName = getActiveRootRouteName(prevState, prevScreen.routeName);
    const activeRootRouteName = getActiveRootRouteName(currentState, currentScreen.routeName);

    if (
      previousRootRouteName &&
      activeRootRouteName &&
      previousRootRouteName !== activeRootRouteName
    ) {
      routes = get(prevState, 'routes[0].routes[0]', null);
      const matchingString = sbpMatchingString(
        prevScreen,
        sbpPdpRouteName,
        sbpOutfitDetailRouteName,
        pdpRouteName,
      );

      route = getRoute(routes.routes, matchingString);
      reset({
        index: 0,
        key: route[0].key,
        routes: route[0].routes,
        extraParams: {isResetStack: true},
      });
      if (activeRootRouteName === 'HomeStack') {
        navigateToPage(currentScreen.routeName);
      } else {
        navigateToNestedRoute(navigator, activeRootRouteName, currentScreen.routeName);
      }
    }
  }
};

export default {
  navigateToProductPage,
  navigateToSBP,
  setTopLevelNavigator,
  reset,
  navigateToPage,
  onNavigationStateChange,
};
