// 9fbef606107a605d69c0edbcd8029e5d 
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {createStackNavigator} from 'react-navigation-stack';
import ApplyNowWrapper from '@tcp/core/src/components/common/molecules/ApplyNowPLCCModal';

import NavBar from '../components/common/molecules/NavBar';
import HomeStack from '../pages/home';
import PlpStack from '../pages/productListing';
import AccountStack from '../pages/account';
import WalletStack from '../pages/login';
import BrandSwitchStack from '../pages/brandSwitch';
import CheckoutStack from '../pages/checkout';
import CustomerHelpStack from '../pages/customerHelp';
import BagStack from '../pages/bag';
import QRScanner from '../components/common/molecules/QRScanner';
import NoInternetStack from '../pages/noInternet';
import {PLPStack as SbpPlpStack, CreateProfileStack} from '../pages/shopByProfile';

const TabNavigator = createBottomTabNavigator(
  {
    HomeStack,
    PlpStack,
    BrandSwitchStack,
    AccountStack,
    WalletStack,
    SbpPlpStack,
    CustomerHelpStack,
  },
  {
    tabBarComponent: NavBar,
  },
);

const RootStack = createStackNavigator(
  {
    Home: {
      screen: TabNavigator,
    },
    Bag: {
      screen: BagStack,
    },
    Checkout: {
      screen: CheckoutStack,
    },
    CustomerHelp: {
      screen: CustomerHelpStack,
    },
    ApplyNow: {
      screen: ApplyNowWrapper,
    },
    QRScanner: {
      screen: QRScanner,
    },
    ShopByProfile: {
      screen: CreateProfileStack,
    },
  },
  {
    mode: 'modal',
    headerMode: 'none',
  },
);

export const NoInternetNavigator = createAppContainer(
  createSwitchNavigator({
    NoInternet: {
      screen: NoInternetStack,
    },
  }),
);

export default createAppContainer(
  createSwitchNavigator({
    Main: {
      screen: RootStack,
    },
  }),
);
