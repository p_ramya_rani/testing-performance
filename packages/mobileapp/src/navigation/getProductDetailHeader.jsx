// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import {SafeAreaView} from 'react-navigation';
import {isAndroid} from '@tcp/core/src/utils/utils.app';
import {
  headerStyle,
  transcluscentHeaderStyle,
} from '../components/common/molecules/Header/Header.style';
import HeaderNew from '../components/common/molecules/Header/HeaderNew';

const isTransparentHeaderPdp = true;
const isAndroidDevice = isAndroid();

const TransparentHeader = ({
  props,
  capitalizedTitle,
  showSearch,
  showSearchIcon,
  isTransparent,
  isScrolled,
  showCross,
}) => (
  <HeaderNew
    {...props}
    title={!isTransparent ? capitalizedTitle : ''}
    showSearch={showSearch}
    showSearchIcon={showSearchIcon}
    isTransparent={isTransparent}
    isScrolled={isScrolled}
    showCross={showCross}
  />
);

const getNewHeader = (
  navigation,
  showSearch,
  navTitle,
  showSearchIcon = false,
  isTransparent = false,
  showCross = true,
) => {
  const title = navTitle || (navigation && navigation.getParam('title'));
  const isScrolled = navigation && navigation.getParam('isScrolled');
  const capitalizedTitle = title && title.toUpperCase();
  const navOptions = isTransparent ? {headerTransparent: true} : {headerBackground: 'transparent'};

  let safeAreaStyles = {};

  if (isTransparent) {
    if (isScrolled) {
      safeAreaStyles = transcluscentHeaderStyle;
    }
  } else {
    safeAreaStyles = headerStyle;
  }
  return {
    header: props =>
      isAndroidDevice ? (
        TransparentHeader({
          props,
          capitalizedTitle,
          showSearch,
          showSearchIcon,
          isTransparent,
          isScrolled,
          showCross,
        })
      ) : (
        <SafeAreaView style={safeAreaStyles} forceInset={{top: 'always'}}>
          {TransparentHeader({
            props,
            capitalizedTitle,
            showSearch,
            showSearchIcon,
            isTransparent,
            isScrolled,
            showCross,
          })}
        </SafeAreaView>
      ),
    ...navOptions,
  };
};

export const getPdpRouteParams = () => {
  return {isTransparentHeader: isTransparentHeaderPdp};
};

/**
 * This function will return the header for product detail navigation
 */
export const getProductDetailHeader = ({
  navigation,
  showSearch,
  navTitle,
  showSearchIcon,
  showCross,
}) => {
  return getNewHeader(
    navigation,
    showSearch,
    navTitle,
    showSearchIcon,
    isTransparentHeaderPdp,
    showCross,
  );
};

TransparentHeader.propTypes = {
  props: PropTypes.shape({}),
  capitalizedTitle: PropTypes.string,
  showSearch: PropTypes.bool,
  showSearchIcon: PropTypes.bool,
  isTransparent: PropTypes.bool,
  isScrolled: PropTypes.bool,
  showCross: PropTypes.bool,
};

TransparentHeader.defaultProps = {
  props: {},
  capitalizedTitle: '',
  showSearch: false,
  showSearchIcon: false,
  isTransparent: false,
  isScrolled: false,
  showCross: false,
};

export default {getProductDetailHeader, getPdpRouteParams};
