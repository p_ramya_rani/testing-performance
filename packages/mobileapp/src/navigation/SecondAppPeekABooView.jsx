// 9fbef606107a605d69c0edbcd8029e5d
/* istanbul ignore file */
import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {Animated, Easing} from 'react-native';
import {isAndroid} from '@tcp/core/src/utils';

import {getSecondAppLogo, getSecondBrandThemeColor, AppAnimationConfig} from '../utils/utils';
import styles from './styles/SecondAppPeekABooView.styles';

const useShowLogoAnimation = (showLogoAnimation, showPeekABooAnimation, togglePeekAction) => {
  const [transformAnimatedValue] = useState(new Animated.ValueXY({x: 0, y: 0}));
  const [imageTransformAnimatedValue] = useState(new Animated.Value(0));
  const [fadeAnimatedValue] = useState(new Animated.Value(0));

  useEffect(() => {
    if (showLogoAnimation) {
      Animated.sequence([
        Animated.parallel([
          Animated.timing(fadeAnimatedValue, {
            toValue: 1,
            duration: 50,
            useNativeDriver: true,
            easing: Easing.out(Easing.quad),
          }),
          Animated.timing(transformAnimatedValue, {
            toValue: {x: 0, y: -180},
            duration: 300,
            useNativeDriver: true,
            easing: Easing.out(Easing.quad),
          }),
        ]),
        Animated.timing(fadeAnimatedValue, {
          toValue: 0,
          duration: 100,
          useNativeDriver: true,
        }),
      ]).start();
    } else {
      Animated.timing(transformAnimatedValue, {
        toValue: {x: 0, y: 0},
        duration: 0,
        useNativeDriver: true,
      }).start();
    }
  }, [showLogoAnimation]);

  useEffect(() => {
    if (showPeekABooAnimation) {
      Animated.sequence([
        Animated.parallel([
          Animated.timing(fadeAnimatedValue, {
            toValue: 1,
            duration: 5000,
            useNativeDriver: true,
          }),
          Animated.timing(imageTransformAnimatedValue, {
            toValue: 0,
            duration: 1,
            useNativeDriver: true,
          }),
        ]),
        Animated.timing(transformAnimatedValue, {
          toValue: {x: 0, y: -70},
          duration: 1000,
          useNativeDriver: true,
        }),
        Animated.timing(imageTransformAnimatedValue, {
          toValue: 1,
          duration: 600,
          useNativeDriver: true,
          easing: Easing.elastic(4),
        }),
        Animated.timing(transformAnimatedValue, {
          toValue: {x: 0, y: 0},
          duration: 300,
          easing: Easing.back(1),
          useNativeDriver: true,
        }),
      ]).start(togglePeekAction);
    }
  }, [showPeekABooAnimation]);

  return [transformAnimatedValue, imageTransformAnimatedValue, fadeAnimatedValue];
};

const SecondAppPeekABooView = (props) => {
  const {showLogoAnimation, showPeekABooAnimation, togglePeekAction} = props;
  const [transformAnimatedValue, imageTransformAnimatedValue, fadeAnimatedValue] =
    useShowLogoAnimation(showLogoAnimation, showPeekABooAnimation, togglePeekAction);

  const borderWidth = isAndroid() || showLogoAnimation ? 2 : 0;
  const shadowColor = getSecondBrandThemeColor();
  const {image, imageContainer, fullAnimationImageStyle} = styles;

  return (
    <Animated.View
      style={[
        imageContainer,
        fullAnimationImageStyle,
        {
          transform: transformAnimatedValue.getTranslateTransform(),
          opacity: fadeAnimatedValue,
          shadowColor,
          borderColor: shadowColor,
          borderWidth,
          left: AppAnimationConfig.AppSplashMaxWidth - image.width / 2 - 10,
        },
      ]}>
      <Animated.Image
        source={getSecondAppLogo()}
        style={[
          image,
          {
            transform: [
              {
                scaleX: imageTransformAnimatedValue.interpolate({
                  inputRange: [0, 1],
                  outputRange: [1, 1.1],
                }),
              },
              {
                scaleY: imageTransformAnimatedValue.interpolate({
                  inputRange: [0, 1],
                  outputRange: [1, 1.1],
                }),
              },
            ],
          },
        ]}
      />
    </Animated.View>
  );
};

SecondAppPeekABooView.defaultProps = {
  showLogoAnimation: false,
  showPeekABooAnimation: false,
  togglePeekAction: () => {},
};

SecondAppPeekABooView.propTypes = {
  showLogoAnimation: PropTypes.bool,
  showPeekABooAnimation: PropTypes.bool,
  togglePeekAction: PropTypes.func,
};

export default SecondAppPeekABooView;
