// 9fbef606107a605d69c0edbcd8029e5d 
import { connect } from 'react-redux';

import { getIsBootstrapDone } from '@tcp/core/src/components/features/content/CustomNavigation/container/CustomNavigation.selectors';
import AppSplash from './AppSplash';

export const mapStateToProps = state => {
  return {
    isBootstrapDone: getIsBootstrapDone(state),
  };
};

export default connect(mapStateToProps)(AppSplash);
