// 9fbef606107a605d69c0edbcd8029e5d 
// styles for app splash and second app peek a boo view
const styles = {
  splash: {
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  imageContainer: {
    backgroundColor: 'white',
    borderRadius: 100,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    marginLeft: -15,
    marginTop: -5,
  },
  updateMessage: {
    color: '#1a1a1a',
    fontFamily: 'montserrat',
    fontSize: 11,
    fontWeight: 'bold',
    lineHeight: 14.5,
    padding: 16,
  },
};

export default styles;
