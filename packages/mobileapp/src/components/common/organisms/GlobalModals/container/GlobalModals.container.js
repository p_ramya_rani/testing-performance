// 9fbef606107a605d69c0edbcd8029e5d 
import {connect} from 'react-redux';

import {getUserLoggedInState} from '@tcp/core/src/components/features/account/User/container/User.selectors';
import {getModalState} from '@tcp/core/src/components/common/organisms/QuickViewModal/container/QuickViewModal.selectors';
import {getIsPickupModalOpen} from '@tcp/core/src/components/common/organisms/PickupStoreModal/container/PickUpStoreModal.selectors';
import {getOutfitProducts} from '@tcp/core/src/components/features/browse/OutfitDetails/container/OutfitDetails.selectors';
import {setLocationEnabledValue} from '@tcp/core/src/reduxStore/actions';
import {getRecommendationOrderModalState} from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.selector';
import {getBrandSwitchedState, getIsLocationEnabled} from './GlobalModals.selectors';

import GlobalModals from '../views';

const mapStateToProps = state => {
  const labels = state.Labels;
  return {
    isUserLoggedIn: getUserLoggedInState(state),
    isQVModalOpen: getModalState(state),
    isOutfitProduct: getOutfitProducts(state),
    isPickupModalOpen: getIsPickupModalOpen(state),
    isBrandSwitched: getBrandSwitchedState(state),
    isLocationEnabled: getIsLocationEnabled(state),
    isRecommendationOrderModalOpen: getRecommendationOrderModalState(state),
    labels,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setLocationFlag: value => {
      dispatch(setLocationEnabledValue(value));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(GlobalModals);
