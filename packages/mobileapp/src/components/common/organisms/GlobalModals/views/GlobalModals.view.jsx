// 9fbef606107a605d69c0edbcd8029e5d
/* istanbul ignore file */
import React, {useState, useEffect, useCallback} from 'react';
import PropTypes from 'prop-types';

import QuickViewModal from '@tcp/core/src/components/common/organisms/QuickViewModal/container/QuickViewModal.container';
import PickupStoreModal from '@tcp/core/src/components/common/organisms/PickupStoreModal';
import UserOnBoardingScreen from '@tcp/core/src/components/common/molecules/UserOnboardingScreen/container';
import PushNotificationModal from '@tcp/core/src/components/common/molecules/PushNotificationModal';
import AppVersionContainer from '@tcp/core/src/components/common/molecules/AppUpdate/container/AppVersion.container';
import LocationAccessPrompt from '@tcp/core/src/components/common/molecules/LocationAccess';
import AddedToBagContainer from '@tcp/core/src/components/features/CnC/AddedToBag';
import NotificationPrompt from '@tcp/core/src/components/common/molecules/NotificationPrompt';
import {isMobileApp, getAPIConfig, isTCP} from '@tcp/core/src/utils';
import OrderRecommendationModal from '@tcp/core/src/components/common/organisms/OrderRecommendationModal/container';
import UserOnBoardingRedesignScreen from '../../../molecules/UserOnboardingRedesign/container';

const Prompts = (props) => {
  const {
    navigation,
    isQVModalOpen,
    isPickupModalOpen,
    isOutfitProduct,
    isRecommendationOrderModalOpen,
    isFromHomePage,
  } = props;
  return (
    <React.Fragment>
      {isQVModalOpen && <QuickViewModal navigation={navigation} />}
      {!isFromHomePage && isPickupModalOpen && !isOutfitProduct && isMobileApp() ? (
        <PickupStoreModal navigation={navigation} />
      ) : null}
      {isRecommendationOrderModalOpen && isMobileApp() ? (
        <OrderRecommendationModal navigation={navigation} />
      ) : null}
    </React.Fragment>
  );
};

Prompts.propTypes = {
  navigation: PropTypes.shape({}).isRequired,
  isQVModalOpen: PropTypes.bool.isRequired,
  isPickupModalOpen: PropTypes.bool,
  isOutfitProduct: PropTypes.bool,
  isRecommendationOrderModalOpen: PropTypes.bool.isRequired,
  isFromHomePage: PropTypes.bool,
};

Prompts.defaultProps = {
  isPickupModalOpen: false,
  isOutfitProduct: false,
  isFromHomePage: false,
};

const GlobalModals = (props) => {
  const {
    navigation,
    labels,
    isUserLoggedIn,
    onInitialModalsActionComplete,
    isBrandSwitched,
    isLocationEnabled,
    setLocationFlag,
    checkFirst,
  } = props;
  const [curSequenceNum, setCurSequenceNum] = useState(0);
  const [showPNSRedesign, setShowPNSRedesign] = useState(true);

  const totalModalsCount = 4;
  const {isRedesignOnboardingDisabled} = getAPIConfig();

  useEffect(() => {
    if (curSequenceNum >= totalModalsCount) {
      onInitialModalsActionComplete();
    }
  }, [curSequenceNum]);

  const showOnboardingRedesign = !isRedesignOnboardingDisabled && isTCP();

  const showNotificationPrompt = !showOnboardingRedesign || showPNSRedesign;

  const modalActionCompleteOnboarding = useCallback((showPushNotificationOnHome) => {
    onInitialModalsActionComplete();
    if (showOnboardingRedesign) {
      if (!showPushNotificationOnHome) {
        setShowPNSRedesign(false);
        setCurSequenceNum(2);
      } else {
        setCurSequenceNum(1);
      }
    } else {
      setCurSequenceNum(1);
    }
  }, []);

  return (
    <>
      {/* ------------ SEQUENCE MODALS START  --------*/}
      {showOnboardingRedesign && curSequenceNum >= 0 && (
        <UserOnBoardingRedesignScreen
          navigation={navigation}
          onModalActionComplete={modalActionCompleteOnboarding}
        />
      )}
      {!showOnboardingRedesign && curSequenceNum >= 0 && (
        <UserOnBoardingScreen onModalActionComplete={modalActionCompleteOnboarding} />
      )}

      {curSequenceNum >= 1 && showNotificationPrompt && (
        <NotificationPrompt
          labels={labels}
          launchTime
          onModalActionComplete={() => setCurSequenceNum(2)}
          showPNSRedesign={showPNSRedesign}
        />
      )}

      {curSequenceNum >= 2 && showNotificationPrompt && <PushNotificationModal />}

      {curSequenceNum >= 2 && (
        <LocationAccessPrompt
          isUserLoggedIn={isUserLoggedIn}
          labels={labels}
          isBrandSwitched={isBrandSwitched}
          onModalActionComplete={() => setCurSequenceNum(3)}
          isLocationEnabled={isLocationEnabled}
          setLocationFlag={setLocationFlag}
          checkFirst={checkFirst}
        />
      )}

      {curSequenceNum >= 3 && (
        <AppVersionContainer labels={labels} onModalActionComplete={() => setCurSequenceNum(4)} />
      )}

      {/* ------------ SEQUENCE MODALS END  --------*/}

      {Prompts(props)}
      <AddedToBagContainer navigation={navigation} />
      <NotificationPrompt
        labels={labels}
        shouldUpdateLatestAppVersion={curSequenceNum === totalModalsCount}
        launchTime={false}
        showPNSRedesign={showPNSRedesign}
      />
    </>
  );
};

GlobalModals.propTypes = {
  isUserLoggedIn: PropTypes.bool.isRequired,
  labels: PropTypes.shape({}).isRequired,
  navigation: PropTypes.shape({}).isRequired,
  onInitialModalsActionComplete: PropTypes.func,
  isBrandSwitched: PropTypes.bool.isRequired,
  isLocationEnabled: PropTypes.bool.isRequired,
  setLocationFlag: PropTypes.func.isRequired,
};

GlobalModals.defaultProps = {
  onInitialModalsActionComplete: () => {},
};

export default GlobalModals;
export {GlobalModals as GlobalModalsVanilla};
