// 9fbef606107a605d69c0edbcd8029e5d
// @flow

import React from 'react';
import {bool, shape, string} from 'prop-types';
import {View} from 'react-native';
import Image from '@tcp/core/src/components/common/atoms/Image/index';
import {getIcon} from '../../../../utils/utils';

/**
 * This component creates icon used in Bottom Nav Bar
 * @param {*} props Props passed from Stack navigator screen
 */
const NavBarIcon = (props) => {
  const {iconActive, iconInactive, style, focused, isBrandIcon, logocomponent, accessibilityLabel} =
    props;
  const icon = focused ? iconActive : iconInactive;
  if (isBrandIcon) return logocomponent;
  return (
    <View>
      <Image source={getIcon(icon)} alt={accessibilityLabel || 'Brand Logo'} style={style.icon} />
    </View>
  );
};

NavBarIcon.propTypes = {
  iconActive: string,
  iconInactive: string,
  focused: bool,
  style: shape({
    icon: string,
  }),
  isBrandIcon: bool,
  logocomponent: shape({}),
  accessibilityLabel: string,
};

NavBarIcon.defaultProps = {
  style: {
    icon: {
      width: 28,
      height: 28,
    },
  },
  iconActive: '',
  iconInactive: '',
  focused: false,
  isBrandIcon: false,
  logocomponent: {},
  accessibilityLabel: '',
};

export default NavBarIcon;
