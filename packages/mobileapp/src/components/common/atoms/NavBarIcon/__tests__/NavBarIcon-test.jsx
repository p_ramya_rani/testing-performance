/* eslint-disable react/jsx-props-no-multi-spaces */
/* eslint-disable no-irregular-whitespace */
import React from 'react';
import {shallow} from 'enzyme';
import NavBarIcon from '../index';

describe('IconSelector', () => {
  let component;
  beforeEach(() => {
    const props = {
      isBrandIcon: true,
    };
    component = shallow(<NavBarIcon {...props} />);
  });

  it('IconSelector should be defined', () => {
    expect(component).toBeDefined();
  });

  it('IconSelector should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
});
