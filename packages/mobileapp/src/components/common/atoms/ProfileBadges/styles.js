// 9fbef606107a605d69c0edbcd8029e5d 
import {StyleSheet} from 'react-native';
import {
  colorMortar,
  colorNobel,
  fontFamilyNunitoSemiBold,
} from '../../../features/shopByProfile/ShopByProfile.styles';

const styles = StyleSheet.create({
  profileBadgesContainer: {
    alignItems: 'center',
    borderColor: colorNobel,
    borderRadius: 18,
    borderWidth: 1,
    height: 18,
    justifyContent: 'center',
    marginRight: 10,
    marginTop: 10,
    paddingHorizontal: 10,
  },

  profileBadgesText: {
    color: colorMortar,
    fontFamily: fontFamilyNunitoSemiBold,
    fontSize: 9,
  },
});

export default styles;
