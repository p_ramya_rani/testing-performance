// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import {View, Text} from 'react-native';
import {PropTypes} from 'prop-types';
import styles from './styles';

const ProfileBadges = ({text}) => {
  return (
    <View style={{...styles.profileBadgesContainer}}>
      <Text style={{...styles.profileBadgesText}}>{text}</Text>
    </View>
  );
};

ProfileBadges.propTypes = {
  text: PropTypes.string.isRequired,
};

export default ProfileBadges;
