// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import {shallow} from 'enzyme';
import ProfileBadges from '../index';

describe('ProfileBadges', () => {
  let component;
  beforeEach(() => {
    const props = {
      text: 'test',
    };
    component = shallow(<ProfileBadges {...props} />);
  });

  it('ProfileBadges should be defined', () => {
    expect(component).toBeDefined();
  });

  it('ProfileBadges should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
});
