// 9fbef606107a605d69c0edbcd8029e5d
import React, {Fragment} from 'react';
import {Text, View, TextInput, Keyboard} from 'react-native';
import {func, shape, string} from 'prop-types';
import CustomIcon from '@tcp/core/src/components/common/atoms/Icon';
import nameInputStyle from '../../../features/shopByProfile/CreateProfile/name.styles';
import {colorBlack, colorRed} from '../../../features/shopByProfile/ShopByProfile.styles';
import styles from './styles';
import {showSBPLabels} from '../../../features/shopByProfile/PLP/helper';

const MAX_NUMBER_CHARACTER = 13;
class NameTextInput extends React.PureComponent {
  state = {
    isSpecialCharacter: false,
    nameField: this.props.data.name ? this.props.data.name : '', // eslint-disable-line
  };

  componentDidMount() {
    this.keyboardWillHideListener = Keyboard.addListener(
      'keyboardWillHide',
      this.keyboardWillHide.bind(this),
    );
    this.ref && this.ref.focus(); // eslint-disable-line
  }

  componentDidUpdate(prevProps, prevState) {
    const {isSpecialCharacter} = this.state;
    if (isSpecialCharacter !== prevState.isSpecialCharacter) {
      setTimeout(() => {
        this.setState((prevState1) => ({
          ...prevState1,
          isSpecialCharacter: false,
        }));
      }, 3000);
    }
  }

  componentWillUnmount() {
    this.keyboardWillHideListener.remove();
  }

  onChangeTextHandler(text) {
    const {setValue} = this.props;
    const isRegularCharacter = /^[a-z0-9 _]+$/i.test(text);
    if (!isRegularCharacter) {
      this.setState((prevState) => ({
        ...prevState,
        isSpecialCharacter: true,
      }));
    }
    this.setState((prevState) => ({
      ...prevState,
      nameField: text,
    }));
    setValue('name', text);
    this.name = text;
  }

  keyboardWillHide() {
    this.forceUpdate();
  }

  renderErrorMessage = () => {
    const {sbpLabels} = this.props;
    const noSpecialCharMessageLabel = showSBPLabels(
      sbpLabels,
      'lbl_sbp_name_error_message',
      'Name field should not contain any special characters.',
    );
    return (
      <View style={styles.errorMessageContainer}>
        <View style={styles.errorMessageIconContainer}>
          <CustomIcon name="warning" size="fs14" style={styles.errorMessageIcon} />
        </View>
        <Text style={styles.errorMessageText}>{noSpecialCharMessageLabel}</Text>
      </View>
    );
  };

  render() {
    const {data} = this.props;
    const {nameField, isSpecialCharacter} = this.state;
    const numberOfCharactersEntered = nameField.length;
    return (
      <Fragment>
        <TextInput
          style={{
            ...nameInputStyle.textInput,
            borderBottomColor: isSpecialCharacter ? colorRed : colorBlack,
          }}
          autoCorrect={false}
          onChangeText={(text) => {
            this.onChangeTextHandler(text);
          }}
          defaultValue={this.name || data.name}
          placeholder="Name"
          underlineColorAndroid="transparent"
          ref={(ref) => {
            this.ref = ref;
          }}
          maxLength={MAX_NUMBER_CHARACTER}
        />
        <View
          style={{
            ...styles.maxNumberCharContainer,
            borderBottomColor: isSpecialCharacter ? colorRed : colorBlack,
          }}>
          <Text style={styles.maxNumberCharText}>
            {`${numberOfCharactersEntered}/${MAX_NUMBER_CHARACTER}`}
          </Text>
        </View>
        <View>{isSpecialCharacter && this.renderErrorMessage()}</View>
      </Fragment>
    );
  }
}

NameTextInput.propTypes = {
  data: shape({
    name: string,
  }).isRequired,
  setValue: func.isRequired,
  sbpLabels: shape({}).isRequired,
};

export default NameTextInput;
