// 9fbef606107a605d69c0edbcd8029e5d 
import {StyleSheet} from 'react-native';
import {
  colorMortar,
  fontFamilyNunitoRegular,
  fontFamilyNunitoSemiBold,
  fontSizeFourteen,
  colorRed,
} from '../../../features/shopByProfile/ShopByProfile.styles';

const errorMessageColor = '#FF0000';

const styles = StyleSheet.create({
  errorMessageContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-evenly',
    marginBottom: '-5%',
    marginTop: '3%',
  },
  errorMessageIcon: {
    color: colorRed,
  },
  errorMessageIconContainer: {
    marginVertical: 5,
  },
  errorMessageText: {
    color: errorMessageColor,
    fontFamily: fontFamilyNunitoSemiBold,
    width: '90%',
  },
  maxNumberCharContainer: {
    borderBottomWidth: 1,
  },
  maxNumberCharText: {
    color: colorMortar,
    fontFamily: fontFamilyNunitoRegular,
    fontSize: fontSizeFourteen,
    marginTop: 5,
  },
});

export default styles;
