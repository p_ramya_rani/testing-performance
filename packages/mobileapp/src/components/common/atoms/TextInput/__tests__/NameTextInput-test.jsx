// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import {shallow} from 'enzyme';
import NameTextInput from '../index';

describe('NameTextInput', () => {
  let component;
  let onPressSpy;

  beforeEach(() => {
    onPressSpy = jest.fn();
    const props = {
      data: {},
      setValue: onPressSpy,
    };
    component = shallow(<NameTextInput {...props} />);
  });

  it('NameTextInput should be defined', () => {
    expect(component).toBeDefined();
  });

  it('NameTextInput should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
});
