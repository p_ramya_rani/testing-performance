// 9fbef606107a605d69c0edbcd8029e5d
import {StyleSheet} from 'react-native';
import {
  colorBlack,
  colorWhite,
  colorMantis,
} from '../../../features/shopByProfile/ShopByProfile.styles';

const styles = StyleSheet.create({
  animatedView: {
    marginHorizontal: 10,
    marginTop: 10,
  },
  imageOne: {
    borderColor: colorWhite,
    borderRadius: 100,
    borderWidth: 2,
    height: 24,
    width: 24,
  },
  imageView: {
    elevation: 14,
    shadowColor: colorMantis,
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.5,
    shadowRadius: 6.11,
  },
  imageViewUnselected: {
    elevation: 14,
    shadowColor: colorBlack,
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.2,
    shadowRadius: 6.11,
  },
  selectIconImage: {
    borderColor: colorMantis,
    borderWidth: 2,
  },
  viewOne: {
    elevation: 30,
    position: 'absolute',
    right: '-30%',
    top: -3,
    zIndex: 1,
  },
});

export default styles;
