// 9fbef606107a605d69c0edbcd8029e5d 
/* eslint-disable react-native/no-inline-styles */
import React, {Component, Fragment} from 'react';
import {View, Dimensions, Image, Animated, TouchableWithoutFeedback} from 'react-native';
import {PropTypes} from 'prop-types';
import Themes from '../../../features/shopByProfile/ShopByProfile.theme';
import {isDeviceIPhone12} from '../../../features/shopByProfile/PLP/helper';
import {padding} from '../../../features/shopByProfile/ShopByProfile.constants';
import nameInputStyle from '../../../features/shopByProfile/CreateProfile/name.styles';
import styles from './styles';

const check = require('../../../../assets/images/shopByProfile/elements/interest-check3x.png');

const initialSize = Object.assign(...Themes.map((c, i) => ({[i]: new Animated.Value(1)})));
const {width} = Dimensions.get('window');
const bubbleSize = (width - padding * 2) / 4 - 15;

class IconSelector extends Component {
  state = {
    size: Object.assign({}, initialSize),
    selected: this.props && this.props.selectedTheme, // eslint-disable-line
  };

  select(i, setValue) {
    const scaleValue = isDeviceIPhone12() ? 1.12 : 1.2;
    setValue('theme', i);
    this.setState(
      prevState =>
        Object.assign({}, prevState, {
          selected: i,
          size: Object.assign(...Themes.map((c, j) => ({[j]: new Animated.Value(1)}))),
        }),
      () => {
        const {size} = this.state;
        Animated.timing(size[i], {
          toValue: scaleValue,
          duration: 200,
        }).start();
      },
    );
  }

  renderIcon({color, icon}, i, setValue) {
    let {selected} = this.state;
    const {size} = this.state;
    if (!isNaN(selected)) selected = parseInt(selected, 10); // eslint-disable-line
    if (selected === i)
      return (
        <Animated.View
          key={color}
          style={{
            ...nameInputStyle.animatedView,
            height: bubbleSize,
            width: bubbleSize,
            transform: [{scaleX: size[i]}, {scaleY: size[i]}],
            ...styles.animatedView,
          }}>
          <TouchableWithoutFeedback
            accessibilityRole="button"
            onPress={() => this.select(i, setValue)}>
            <>
              <View style={{...styles.viewOne}}>
                <Image style={{...styles.imageOne}} source={check} />
              </View>
              <View
                style={{
                  ...nameInputStyle.imageView,
                  ...styles.imageView,
                }}>
                <Image
                  source={icon}
                  style={{
                    ...nameInputStyle.iconImage,
                    ...styles.selectIconImage,
                  }}
                />
              </View>
            </>
          </TouchableWithoutFeedback>
        </Animated.View>
      );
    return (
      <Animated.View
        key={color}
        style={{
          ...nameInputStyle.animatedView,
          height: bubbleSize,
          width: bubbleSize,
        }}>
        <TouchableWithoutFeedback
          accessibilityRole="button"
          onPress={() => this.select(i, setValue)}>
          <View
            style={{
              ...nameInputStyle.imageView,
              ...styles.imageViewUnselected,
            }}>
            <Image
              source={icon}
              style={{
                ...nameInputStyle.iconImage,
              }}
            />
          </View>
        </TouchableWithoutFeedback>
      </Animated.View>
    );
  }

  render() {
    const {setValue} = this.props;
    return <Fragment>{Themes.map((c, i) => this.renderIcon(c, i, setValue))}</Fragment>;
  }
}

IconSelector.propTypes = {
  setValue: PropTypes.func.isRequired,
};

export default IconSelector;
