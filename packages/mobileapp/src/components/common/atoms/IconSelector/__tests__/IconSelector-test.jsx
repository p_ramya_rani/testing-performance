// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import {shallow} from 'enzyme';
import IconSelector from '../index';

describe('IconSelector', () => {
  let component;
  let onPressSpy;
  beforeEach(() => {
    onPressSpy = jest.fn();
    const props = {
      setValue: onPressSpy,
    };
    component = shallow(<IconSelector {...props} />);
  });

  it('IconSelector should be defined', () => {
    expect(component).toBeDefined();
  });

  it('IconSelector should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
});
