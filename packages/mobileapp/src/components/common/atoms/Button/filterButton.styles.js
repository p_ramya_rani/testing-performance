// 9fbef606107a605d69c0edbcd8029e5d
import {StyleSheet} from 'react-native';
import {colorNero, colorGrey} from '../../../features/shopByProfile/ShopByProfile.styles';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    borderColor: colorGrey,
    borderRadius: 100,
    borderWidth: 1,
    color: colorNero,
    height: 36,
    justifyContent: 'center',
    margin: 10,
    marginHorizontal: 5,
    top: -10,
    width: 'auto',
  },
  text: {
    fontSize: 12,
    letterSpacing: 0.2,
    paddingHorizontal: 15,
  },
});

export default styles;
