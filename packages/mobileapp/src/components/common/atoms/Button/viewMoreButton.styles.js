// 9fbef606107a605d69c0edbcd8029e5d
import {StyleSheet} from 'react-native';
import {
  colorBlack,
  colorWhite,
  colorViewMore,
  fontFamilyNunitoBold,
} from '../../../features/shopByProfile/ShopByProfile.styles';

const styles = StyleSheet.create({
  container: {
    backgroundColor: colorWhite,
    borderRadius: 16,
    elevation: 5,
    height: 46,
    justifyContent: 'center',
    margin: 10,
    shadowColor: colorBlack,
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.2,
    shadowRadius: 5.65,
    width: '95%',
  },
  text: {
    alignSelf: 'center',
    color: colorViewMore,
    fontFamily: fontFamilyNunitoBold,
    fontSize: 13,
    fontStyle: 'normal',
    fontWeight: '800',
    letterSpacing: 0.93,
  },
  wrapper: {
    alignItems: 'center',
    bottom: 15,
    elevation: 5,
    padding: 5,
    zIndex: 20,
  },
});

export default styles;
