// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {PropTypes} from 'prop-types';
import styles from './viewMoreButton.styles';

const ViewMoreButton = ({children, onPress}) => {
  return (
    <TouchableOpacity
      accessibilityRole="button"
      onPress={() => {
        onPress();
      }}>
      <View style={styles.wrapper}>
        <View style={styles.container}>
          <Text style={styles.text}>{children}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

ViewMoreButton.propTypes = {
  children: PropTypes.shape({}).isRequired,
  onPress: PropTypes.func.isRequired,
};

export default ViewMoreButton;
