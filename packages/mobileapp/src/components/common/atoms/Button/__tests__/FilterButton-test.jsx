// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import {shallow} from 'enzyme';
import {TouchableOpacity, View} from 'react-native';
import FilterButton from '../FilterButton';

describe('FilterButton', () => {
  let component;
  let onPressSpy;
  let updateXCordinatesSpy;
  beforeEach(() => {
    onPressSpy = jest.fn();
    updateXCordinatesSpy = jest.fn();
    const props = {
      interest: 'animals',
      selectedCategory: 'animals',
      onPress: onPressSpy,
      updateXCordinates: updateXCordinatesSpy,
    };
    component = shallow(<FilterButton {...props} />);
  });

  it('FilterButton should be defined', () => {
    expect(component).toBeDefined();
  });

  it('FilterButton should render correctly', () => {
    expect(component).toMatchSnapshot();
  });

  it('calling onPress should call onPress prop passed', () => {
    component
      .find(TouchableOpacity)
      .first()
      .props()
      .onPress();
    expect(onPressSpy).toHaveBeenCalled();
  });

  it('calling updateXCordinates should call updateXCordinates prop passed', () => {
    component
      .find(View)
      .first()
      .props()
      .onLayout({
        nativeEvent: {
          layout: {
            width: 0,
            x: 0,
          },
        },
      });
    expect(updateXCordinatesSpy).toHaveBeenCalled();
  });

  it('FilterButton should render correctly', () => {
    const props = {
      interest: 'SHOP ALL',
      selectedCategory: 'animals',
      onPress: onPressSpy,
      updateXCordinates: updateXCordinatesSpy,
    };
    component = shallow(<FilterButton {...props} />);
    expect(component).toMatchSnapshot();
  });
});
