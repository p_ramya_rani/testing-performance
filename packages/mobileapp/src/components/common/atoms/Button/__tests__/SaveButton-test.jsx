// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import {shallow} from 'enzyme';
import SaveButton from '../SaveButton';

describe('SaveButton', () => {
  let component;
  let onPressSpy;

  beforeEach(() => {
    onPressSpy = jest.fn();
    const props = {
      disabled: true,
      saveOnPress: onPressSpy,
    };
    component = shallow(<SaveButton {...props} />);
  });

  it('SaveButton should be defined', () => {
    expect(component).toBeDefined();
  });

  it('SaveButton should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
});
