// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import {shallow} from 'enzyme';
import ViewMoreButton from '../ViewMoreButton';

describe('ViewMoreButton', () => {
  let component;
  let onPressSpy;
  let showMoreSpy;

  beforeEach(() => {
    onPressSpy = jest.fn();
    showMoreSpy = jest.fn();
    const props = {
      children: {},
      category: 'test',
      interest: 'test',
      interestGrid: true,
      retrieveMoreProductsFromUnbxd: onPressSpy,
      showMoreProducts: showMoreSpy,
    };
    component = shallow(<ViewMoreButton {...props} />);
  });

  it('ViewMoreButton should be defined', () => {
    expect(component).toBeDefined();
  });

  it('ViewMoreButton should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
});
