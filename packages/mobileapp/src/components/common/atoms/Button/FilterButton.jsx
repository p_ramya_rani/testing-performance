// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {PropTypes} from 'prop-types';
import {
  colorBlue,
  colorWhite,
  colorNero,
  fontFamilyNunitoExtraBold,
  fontFamilyNunitoSemiBold,
} from '../../../features/shopByProfile/ShopByProfile.styles';
import styles from './filterButton.styles';

const FilterButton = ({interest, selectedCategory, onPress, updateXCordinates, categoryId}) => {
  if (interest === 'SHOP ALL') return null;
  return (
    <View
      onLayout={(event) => {
        /**
         * We need to record the x cordinate, and width of each pill,
         * so that we can scroll the list to the desired pill when a corresponding
         * view all button is clicked from the SBP-PLP.
         */
        updateXCordinates({
          Xcord: event.nativeEvent.layout.x,
          name: interest.toLowerCase(),
          width: event.nativeEvent.layout.width,
        });
      }}>
      <TouchableOpacity
        accessibilityRole="button"
        onPress={() => {
          onPress(interest, categoryId);
        }}>
        <View
          style={{
            ...styles.container,
            backgroundColor:
              interest.toLowerCase() === selectedCategory.toLowerCase() ? colorBlue : colorWhite,
          }}>
          <Text
            style={{
              ...styles.text,
              color:
                interest.toLowerCase() === selectedCategory.toLowerCase() ? colorWhite : colorNero,
              fontFamily:
                interest.toLowerCase() === selectedCategory.toLowerCase()
                  ? fontFamilyNunitoExtraBold
                  : fontFamilyNunitoSemiBold,
            }}>
            {interest}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

FilterButton.propTypes = {
  interest: PropTypes.string.isRequired,
  selectedCategory: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
  updateXCordinates: PropTypes.func.isRequired,
  categoryId: PropTypes.string.isRequired,
};

export default FilterButton;
