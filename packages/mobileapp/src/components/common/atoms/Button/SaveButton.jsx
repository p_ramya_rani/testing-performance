// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import {TouchableOpacity, Image} from 'react-native';
import {PropTypes} from 'prop-types';
import styles from './saveButton.styles';

const source = require('../../../../assets/images/shopByProfile/elements/check_2020-10-08/check.png');

const SaveButton = ({saveOnPress, disabled}) => {
  return (
    <TouchableOpacity
      accessibilityRole="button"
      onPress={() => {
        saveOnPress();
      }}
      disabled={disabled}
      style={styles.saveBtn}>
      <Image source={source} style={styles.image} />
    </TouchableOpacity>
  );
};

SaveButton.propTypes = {
  disabled: PropTypes.bool.isRequired,
  saveOnPress: PropTypes.func.isRequired,
};

export default SaveButton;
