// 9fbef606107a605d69c0edbcd8029e5d 
import {StyleSheet} from 'react-native';
import {colorNero} from '../../../features/shopByProfile/ShopByProfile.styles';

const styles = StyleSheet.create({
  image: {
    height: 24,
    width: 24,
  },
  saveBtn: {
    alignItems: 'center',
    backgroundColor: colorNero,
    borderRadius: 70,
    height: 70,
    justifyContent: 'center',
    width: 70,
  },
});

export default styles;
