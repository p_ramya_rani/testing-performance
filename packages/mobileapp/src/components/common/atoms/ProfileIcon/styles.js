// 9fbef606107a605d69c0edbcd8029e5d 
import {StyleSheet} from 'react-native';
import {colorWhite} from '../../../features/shopByProfile/ShopByProfile.styles';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    borderColor: colorWhite,
    borderWidth: 4,
    justifyContent: 'center',
    overflow: 'hidden',
  },
  image: {
    resizeMode: 'contain',
  },
});

export default styles;
