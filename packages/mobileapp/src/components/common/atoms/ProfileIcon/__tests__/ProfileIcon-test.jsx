// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import {shallow} from 'enzyme';
import ProfileIcon from '../index';

describe('ProfileIcon', () => {
  let component;

  beforeEach(() => {
    const props = {
      theme: '2',
      size: 86,
    };
    component = shallow(<ProfileIcon {...props} />);
  });

  it('ProfileIcon should be defined', () => {
    expect(component).toBeDefined();
  });

  it('ProfileIcon should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
});
