// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import {View, Image} from 'react-native';
import {PropTypes} from 'prop-types';
import Themes from '../../../features/shopByProfile/ShopByProfile.theme';
import styles from './styles';

const profileIconStyling = size => {
  return {
    ...styles.container,
    borderRadius: size,
    height: size,
    width: size,
  };
};

const profileIconImageStyling = size => {
  return {
    ...styles.image,
    height: size,
    width: size,
  };
};

const ProfileIcon = ({theme, size = 86}) => {
  return (
    <View style={profileIconStyling(size)}>
      <Image source={Themes[theme || 0].icon} style={profileIconImageStyling(size)} />
    </View>
  );
};

ProfileIcon.propTypes = {
  theme: PropTypes.number.isRequired,
  size: PropTypes.number.isRequired,
};

export default ProfileIcon;
