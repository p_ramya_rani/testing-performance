// 9fbef606107a605d69c0edbcd8029e5d 
import React, {PureComponent} from 'react';
import {Animated, Easing, AccessibilityInfo, findNodeHandle} from 'react-native';
import {PropTypes} from 'prop-types';
import {Image} from '@tcp/core/src/components/common/atoms';
import {isGymboree, getLabelValue} from '@tcp/core/src/utils';

import {
  Container,
  ContainerViewTouchable,
  TCPIcon,
  GymIcon,
  styles,
} from './AnimatedBrandChangeIcon.style';
import {APP_TYPE} from '../../hoc/ThemeWrapper.constants';
import icons from '../../../../utils/icons';

const BrandSwitchConfig = {
  MAX_X: 90,
  AnimationDuration: 200,
};

/**
 * kindly use this component only for the bottom tab at the center of the tab
 *
 */
class AnimatedBrandChangeIcon extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {openSwitch: true};
    this.brandTCPAnimatedValue = new Animated.ValueXY();
    this.brandGymAnimatedValue = new Animated.ValueXY();
    this.fadeAnimatedValue = new Animated.Value(0);
    this.tcpIconRef = React.createRef();
    this.gymIconRef = React.createRef();

    this.showBrands();
  }

  componentDidMount() {
    if (isGymboree() && this.gymIconRef.current) {
      AccessibilityInfo.setAccessibilityFocus(findNodeHandle(this.gymIconRef.current));
    } else if (this.tcpIconRef.current) {
      AccessibilityInfo.setAccessibilityFocus(findNodeHandle(this.tcpIconRef.current));
    }
  }

  /**
   * To change the state of position of logos before and
   * after the animation it behaves like a toggle.
   */
  changePosition = () => {
    const {openSwitch} = this.state;
    this.setState(
      {
        openSwitch: !openSwitch,
      },
      () => {
        if (!openSwitch) {
          this.showBrands();
        } else {
          this.hideBrands();
          const {toggleBrandAction} = this.props;
          if (toggleBrandAction) toggleBrandAction(false);
        }
      },
    );
  };

  /**
   * @function showBrands
   * This method shows brands animatedly
   *
   * @memberof AnimatedBrandChangeIcon
   */
  showBrands = () => {
    Animated.sequence([
      Animated.delay(350),
      Animated.timing(this.fadeAnimatedValue, {
        toValue: 1,
        duration: 50,
        useNativeDriver: true,
        easing: Easing.out(Easing.quad),
      }),
      Animated.parallel([
        Animated.timing(this.brandTCPAnimatedValue, {
          toValue: {x: -BrandSwitchConfig.MAX_X, y: 0},
          duration: BrandSwitchConfig.AnimationDuration,
          useNativeDriver: true,
          easing: Easing.out(Easing.quad),
        }),
        Animated.timing(this.brandGymAnimatedValue, {
          toValue: {x: BrandSwitchConfig.MAX_X, y: 0},
          duration: BrandSwitchConfig.AnimationDuration,
          useNativeDriver: true,
          easing: Easing.out(Easing.quad),
        }),
      ]),
    ]).start();
  };

  /**
   * @function hideBrands
   * This method hides brands and sends them to their original position
   *
   * @memberof AnimatedBrandChangeIcon
   */
  hideBrands = () => {
    Animated.parallel([
      Animated.timing(this.brandTCPAnimatedValue, {
        toValue: {x: 0, y: 0},
        duration: 0,
        useNativeDriver: true,
      }),
      Animated.timing(this.brandGymAnimatedValue, {
        toValue: {x: 0, y: 0},
        duration: 0,
        useNativeDriver: true,
      }),
    ]).start();
  };

  /**
   * @function switchToTCP
   * This function switches current app type to tcp
   *
   * @memberof AnimatedBrandChangeIcon
   */
  switchToTCP = () => {
    this.switchToBrand(APP_TYPE.TCP);
  };

  /**
   * @function switchToGymboree
   * This function switches current app type to tcp
   *
   * @memberof AnimatedBrandChangeIcon
   */
  switchToGymboree = () => {
    this.switchToBrand(APP_TYPE.GYMBOREE);
  };

  /**
   * @function switchToGymboree
   * This function switches the app to input brand
   * @param brand
   *
   * @memberof AnimatedBrandChangeIcon
   */
  switchToBrand = brand => {
    const {updateAppTypeHandler} = this.props;
    if (updateAppTypeHandler) updateAppTypeHandler(brand);
    this.changePosition();
  };

  /**
   * @function renderTCPBrand
   * returns view for tcp brand switch
   *
   * @memberof AnimatedBrandChangeIcon
   */
  renderTCPBrand = source => {
    const {brandContainer} = styles;
    const {labels} = this.props;
    const theChildrensplace = getLabelValue(labels, 'theChildrensplace');

    return (
      <Animated.View
        style={[
          brandContainer,
          {
            transform: this.brandTCPAnimatedValue.getTranslateTransform(),
            opacity: this.fadeAnimatedValue,
          },
        ]}>
        <TCPIcon
          accessible
          accessibilityLabel={theChildrensplace}
          accessibilityRole="button"
          onPress={this.switchToTCP}
          name="tcpBrand"
          ref={this.tcpIconRef}>
          <Image source={source} alt={theChildrensplace} />
        </TCPIcon>
      </Animated.View>
    );
  };

  /**
   * @function renderGymboreeBrand
   * returns view for gymboree brand switch
   *
   * @memberof AnimatedBrandChangeIcon
   */
  renderGymboreeBrand = source => {
    const {brandContainer} = styles;
    const {labels} = this.props;
    const gymboreeLink = getLabelValue(labels, 'gymboreeLink');
    return (
      <Animated.View
        style={[
          brandContainer,
          {
            transform: this.brandGymAnimatedValue.getTranslateTransform(),
            opacity: this.fadeAnimatedValue,
          },
        ]}>
        <GymIcon
          accessible
          accessibilityLabel={gymboreeLink}
          accessibilityRole="button"
          onPress={this.switchToGymboree}
          name="gymboreeBrand"
          ref={this.gymIconRef}>
          <Image source={source} alt={gymboreeLink} />
        </GymIcon>
      </Animated.View>
    );
  };

  /**
   * @function renderFirstBrand
   * returns first brand as tcp for gymboree app type and vice-versa
   * this brand appears above second brand
   *
   * @memberof AnimatedBrandChangeIcon
   */
  renderFirstBrand = () => {
    if (isGymboree()) {
      const source = icons.gymboree.peekABooActive;
      return this.renderGymboreeBrand(source);
    }
    const source = icons.tcp.peekABooActive;
    return this.renderTCPBrand(source);
  };

  /**
   * @function renderSecondBrand
   * returns first brand as tcp for gymboree app type and vice-versa
   * this brand appears below first brand
   *
   * @memberof AnimatedBrandChangeIcon
   */
  renderSecondBrand = () => {
    if (isGymboree()) {
      const source = icons.tcp.peekABoo;

      return this.renderTCPBrand(source);
    }
    const source = icons.gymboree.peekABoo;
    return this.renderGymboreeBrand(source);
  };

  /**
   * render
   * renders main view
   *
   * @returns
   * @memberof AnimatedBrandChangeIcon
   */
  render() {
    return (
      <Container>
        <ContainerViewTouchable onPress={this.changePosition} />
        {this.renderFirstBrand()}
        {this.renderSecondBrand()}
      </Container>
    );
  }
}

/* Prop types declaration */
AnimatedBrandChangeIcon.propTypes = {
  updateAppTypeHandler: PropTypes.func,
  toggleBrandAction: PropTypes.func,
  labels: PropTypes.shape({}),
};

AnimatedBrandChangeIcon.defaultProps = {
  updateAppTypeHandler: null,
  toggleBrandAction: null,
  labels: null,
};

export default AnimatedBrandChangeIcon;
