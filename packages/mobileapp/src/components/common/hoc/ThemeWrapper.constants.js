// 9fbef606107a605d69c0edbcd8029e5d 
export const APP_TYPE = {
  TCP: 'tcp',
  GYMBOREE: 'gymboree',
};
export const THEME_WRAPPER_REDUCER_KEY = 'ThemeWrapper';
export const UPDATE_APP_TYPE = 'updateAppType';
export const UPDATE_APP_TYPE_AND_REDIRECT = 'updateAppTypeAndRedirect';
