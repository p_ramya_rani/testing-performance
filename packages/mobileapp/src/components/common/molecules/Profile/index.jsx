// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import PropTypes from 'prop-types';
import CompleteProfileSBP from './Complete';
import IncompleteProfileSBP from './Incomplete';

const sequence = [
  'NameInput',
  'AgeInput',
  'GenderInput',
  'SizeInput',
  'InterestsInput',
  'ColorsInput',
];
/* eslint-disable complexity */
export const calculateCompletion = (profile) => {
  if (!profile) return 0;
  if (!profile.theme && profile.theme !== 0) return 20;
  if (profile.interests && profile.interests.length >= 1) return 100;
  if (profile.size && !profile.interests) return 90;
  if (profile.gender && !profile.size) return 70;
  if (profile.relation && !profile.gender) return 50;
  if (profile.name && profile.name.length >= 1 && !profile.gender) return 40;
  return 0;
};

export const nextScreen = (profile) => {
  const percentage = calculateCompletion(profile);
  switch (percentage) {
    case 20:
      return sequence[0];
    case 40:
      return sequence[1];
    case 50:
      return sequence[2];
    case 70:
      return sequence[3];
    case 90:
      return sequence[4];
    default:
      return sequence[0];
  }
};

const Profile = ({
  profile,
  action,
  button,
  navigation,
  routeName,
  closeModal,
  currentProfileIndex,
  profileIndex,
}) => {
  if (calculateCompletion(profile) < 100)
    return (
      <IncompleteProfileSBP
        navigation={navigation}
        profile={profile}
        action={action}
        button={button}
        percentage={calculateCompletion(profile)}
        nextScreen={nextScreen(profile)}
        closeModal={closeModal}
      />
    );

  return (
    <CompleteProfileSBP
      navigation={navigation}
      profile={profile}
      action={action}
      button={button}
      routeName={routeName}
      currentProfileIndex={currentProfileIndex}
      profileIndex={profileIndex}
    />
  );
};

Profile.propTypes = {
  profile: PropTypes.shape({}).isRequired,
  action: PropTypes.func.isRequired,
  button: PropTypes.shape({}).isRequired,
  routeName: PropTypes.string.isRequired,
  closeModal: PropTypes.func.isRequired,
  navigation: PropTypes.shape({}).isRequired,
  currentProfileIndex: PropTypes.string.isRequired,
  profileIndex: PropTypes.string.isRequired,
};
export default Profile;
