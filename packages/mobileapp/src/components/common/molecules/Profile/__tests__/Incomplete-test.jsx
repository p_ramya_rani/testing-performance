// 9fbef606107a605d69c0edbcd8029e5d 
/* eslint-disable */
import React from 'react';
import {shallow} from 'enzyme';
import {IncompleteProfile, Button} from '../Incomplete';

describe('IncompleteProfile', () => {
  let component;
  let onPressSpy;
  const profile = {
    id: 1,
    name: 'Name',
    theme: 1,
    gender: 'Boy',
    interests: ['Animals'],
  };

  beforeEach(() => {
    onPressSpy = jest.fn();
    const props = {
      profile,
      percentage: 100,
      nextScreen: '',
      closeModal: onPressSpy,
      navigation: {
        navigate: jest.fn(),
        dispatch: jest.fn(),
        state: {
          params: {},
        },
      },
    };
    component = shallow(<IncompleteProfile {...props} />);
  });

  it('Component should be defined', () => {
    expect(component).toBeDefined();
  });

  it('Component should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
});

describe('Button', () => {
  let component;
  let onPressSpy;

  beforeEach(() => {
    onPressSpy = jest.fn();
    const props = {
      color: 'white',
      deleteProfile: onPressSpy,
      id: '1234556677',
      userId: 'thei382739392',
    };

    component = shallow(<Button {...props} />);
  });

  it('Button should be defined', () => {
    expect(component).toBeDefined();
  });

  it('should match snapshot', () => {
    expect(component).toMatchSnapshot();
  });
});
