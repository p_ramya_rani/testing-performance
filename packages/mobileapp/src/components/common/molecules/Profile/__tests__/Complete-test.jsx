// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import {shallow} from 'enzyme';
import {CompleteProfile} from '../Complete';

describe('CompleteProfile', () => {
  let component;
  let onPressSpy;
  const profile = {
    id: 1,
    name: 'Name',
    theme: 1,
    gender: 'Boy',
    month: 'August',
    year: '2015',
    interests: ['Animals'],
  };

  beforeEach(() => {
    onPressSpy = jest.fn();
    const props = {
      profile,
      button: {color: 'white', icon: 'arrow-right'},
      routeName: 'test',
      action: onPressSpy,
      navigation: {
        navigate: jest.fn(),
        dispatch: jest.fn(),
        state: {
          params: {},
        },
      },
    };
    component = shallow(<CompleteProfile {...props} />);
  });

  it('CompleteProfile should be defined', () => {
    expect(component).toBeDefined();
  });

  it('CompleteProfile should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
});
