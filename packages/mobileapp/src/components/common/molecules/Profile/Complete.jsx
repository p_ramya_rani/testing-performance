// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import {func, shape, string} from 'prop-types';
import {connect} from 'react-redux';
import ProfileBadges from '../../atoms/ProfileBadges';
import ProfileIcon from '../../atoms/ProfileIcon';
import Themes from '../../../features/shopByProfile/ShopByProfile.theme';
import {Months} from '../../../features/shopByProfile/ShopByProfile.constants';
import {capitalizeWords, showSBPLabels} from '../../../features/shopByProfile/PLP/helper';
import styles from './complete.styles';
import {colorWhite, colorNero} from '../../../features/shopByProfile/ShopByProfile.styles';
import {getSBPLabels} from '../../../features/shopByProfile/ShopByProfile.selector';

const rightArrow = require('../../../../assets/images/shopByProfile/elements/btn-arrow-right_2020-12-21/btn-arrow-right.png');
const kebab = require('../../../../assets/images/shopByProfile/elements/kebab-menu_2020-10-24/kebab-menu.png');

const calculateAgeInMonths = (month, year) => {
  // Pregen profiles from birthday savings return numerical months starting from 1 NOT 0
  const selectedMonth = Number.isNaN(Number(month)) ? Months.indexOf(month) : month - 1;
  const numberOfMonths = Math.max(
    0,
    (new Date().getFullYear() - year) * 12 + (new Date().getMonth() - selectedMonth),
  );

  if (numberOfMonths >= 12) {
    const numberOfYears = Math.floor(numberOfMonths / 12);
    return `${numberOfYears} years`;
  }
  return `${numberOfMonths} months`;
};

const conditionalStyling = (color) => ({
  ...styles.kebabIconContainer,
  backgroundColor: color === colorNero ? 'transparent' : colorNero,
});

// eslint-disable-next-line complexity, sonarjs/cognitive-complexity
const CompleteProfile = ({
  profile,
  action,
  button,
  routeName,
  sbpLabels,
  currentProfileIndex,
  profileIndex,
}) => {
  const {name, theme, month, year, gender} = profile;
  const shadowColor = (Themes && Themes[theme || 0] && Themes[theme || 0].color) || '#000';
  const profileAge = calculateAgeInMonths(month, year);
  const profileDepartment = capitalizeWords(gender);
  const plpProfileDepartment = `${profileDepartment} | `;
  const placeLabel = showSBPLabels(sbpLabels, 'lbl_sbp_plp_profile_pill_place', 'Place');

  let vTheme = theme;
  if (!theme || theme.length === 0) {
    vTheme = 0;
  }

  if (routeName !== 'PLP') {
    return (
      <View
        style={{
          ...styles.container,
          shadowColor,
        }}>
        <ProfileIcon size={86} theme={vTheme} />
        <View style={styles.secondaryContainer}>
          <Text style={styles.nameText}>{name}</Text>
          <View style={styles.badgesContainer}>
            <ProfileBadges text={profileDepartment} />
            <ProfileBadges text={profileAge} />
          </View>
        </View>
        <Button action={action} color={button.color} icon={button.icon} />
      </View>
    );
  }

  return (
    <View>
      <View style={styles.plpCompleteContainer}>
        {profileIndex > currentProfileIndex ? (
          <View style={styles.leftSideProfileIcon}>
            <ProfileIcon size={75} theme={vTheme} />
          </View>
        ) : null}
        <View style={styles.viewOne}>
          <View style={styles.textContainer}>
            <Text style={styles.textName}>{`${name}'s`}</Text>
            <Text style={styles.textPlace}>{placeLabel}</Text>
          </View>
          <View style={styles.profileAgeGenderContainer}>
            <Text style={styles.genderText}>{plpProfileDepartment}</Text>
            <Text style={styles.ageText}>{profileAge}</Text>
          </View>
        </View>
        {currentProfileIndex === profileIndex && (
          <Button action={action} color={button.color} icon={button.icon} />
        )}
        {profileIndex < currentProfileIndex && (
          <View style={styles.rightSideProfileIcon}>
            <ProfileIcon size={75} theme={vTheme} />
          </View>
        )}
      </View>
    </View>
  );
};

const Button = ({action, color = colorWhite, icon = 'arrow-right'}) => (
  <TouchableOpacity onPress={action} style={styles.buttonContainer} accessibilityRole="button">
    {icon === 'arrow-right' ? (
      <Image style={styles.arrowIcon} source={rightArrow} />
    ) : (
      <View style={conditionalStyling(color)}>
        <Image source={kebab} style={styles.kebabIcon} />
      </View>
    )}
  </TouchableOpacity>
);

Button.propTypes = {
  action: func.isRequired,
  color: string.isRequired,
  icon: string.isRequired,
};

CompleteProfile.propTypes = {
  action: func.isRequired,
  button: shape({
    color: string,
    icon: string,
  }).isRequired,
  currentProfileIndex: string.isRequired,
  profile: shape({
    gender: string,
    month: string,
    name: string,
    theme: string,
    year: string,
  }).isRequired,
  profileIndex: string.isRequired,
  routeName: string.isRequired,
  sbpLabels: shape({}).isRequired,
};

const mapStateToProps = (state) => {
  return {
    sbpLabels: getSBPLabels(state),
  };
};

export {CompleteProfile};
export default connect(mapStateToProps)(CompleteProfile);
