// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import {PropTypes} from 'prop-types';
import ProgressCircle from 'react-native-progress-circle';
import {connect} from 'react-redux';
import names from '@tcp/core/src/constants/eventsName.constants';
import {parseBoolean} from '@tcp/core/src/utils';
import {getUserId} from '@tcp/core/src/components/features/account/User/container/User.selectors';
import {getSBPLabels} from '../../../features/shopByProfile/ShopByProfile.selector';
import ClickTracker from '../../atoms/ClickTracker';
import style from './incomplete.styles';
import {
  colorWhite,
  colorNero,
  colorSummerSky,
} from '../../../features/shopByProfile/ShopByProfile.styles';
import {showSBPLabels} from '../../../features/shopByProfile/PLP/helper';
import {deleteSBPProfile} from '../../../features/shopByProfile/ShopByProfileAPI/ShopByProfile.actions';

const closeIcon = require('../../../../assets/images/shopByProfile/elements/close3x.png');

const transparent = 'transparent';

const RenderProgressCircle = (percentage, onPress) => {
  return (
    <TouchableOpacity
      accessibilityRole="button"
      onPress={onPress}
      style={style.touchableOpacityOne}>
      <View style={style.viewOne}>
        <ProgressCircle
          radius={38}
          percent={percentage}
          color={colorSummerSky}
          borderWidth={6}
          bgColor="#ffffff"
          shadowColor="#f3f3f3">
          <Text style={style.textOne}>{`${percentage}%`}</Text>
        </ProgressCircle>
      </View>
    </TouchableOpacity>
  );
};

const IncompleteProfile = ({
  profile,
  navigation,
  percentage,
  nextScreen,
  closeModal,
  sbpLabels,
}) => {
  const {name, gender} = profile;
  let {is_pregenerate_profile: pregenerateProfiles} = profile;
  const finishProfileLabel = showSBPLabels(
    sbpLabels,
    'lbl_sbp_finish_incomplete_profile',
    'Finish Profile',
  );
  pregenerateProfiles = parseBoolean(pregenerateProfiles);
  return (
    <View style={style.viewTwo}>
      {RenderProgressCircle(percentage, () => {
        navigation.navigate(nextScreen, Object.assign({}, profile, {isIncomplete: true}));
        closeModal();
      })}
      <ClickTracker
        as={TouchableOpacity}
        name={names.screenNames.sbp_incomplete_profile}
        module="browse"
        clickData={{customEvents: [pregenerateProfiles === true ? 'event27' : 'event26']}}
        accessibilityRole="button"
        style={style.touchableOpacityTwo}
        onPress={() => {
          navigation.navigate(nextScreen, Object.assign({}, profile, {isIncomplete: true}));
          closeModal();
        }}>
        <Text style={style.textTwo}>{name || gender}</Text>
        <Text style={style.textTwo}>{finishProfileLabel}</Text>
      </ClickTracker>
      <ButtonWithProps color={colorNero} icon="close" profile={profile} />
    </View>
  );
};

IncompleteProfile.propTypes = {
  profile: PropTypes.shape({
    name: PropTypes.string,
    gender: PropTypes.string,
    is_pregenerate_profile: PropTypes.bool,
  }).isRequired,
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }).isRequired,
  percentage: PropTypes.number.isRequired,
  nextScreen: PropTypes.string.isRequired,
  closeModal: PropTypes.func.isRequired,
  sbpLabels: PropTypes.shape({}).isRequired,
};

const mapStateToPropsIncomplete = (state) => {
  return {
    sbpLabels: getSBPLabels(state),
  };
};

export {IncompleteProfile};
export default connect(mapStateToPropsIncomplete)(IncompleteProfile);

export const Button = ({color = colorWhite, deleteProfile, userId, profile}) => {
  const payload = {
    profile,
    userId,
  };
  return (
    <TouchableOpacity
      accessibilityRole="button"
      onPress={() => deleteProfile(payload)}
      style={{
        ...style.touchableOpacityThree,
        backgroundColor: color === colorNero ? transparent : colorNero,
      }}>
      <Image style={style.image} source={closeIcon} />
    </TouchableOpacity>
  );
};
Button.propTypes = {
  color: PropTypes.string.isRequired,
  deleteProfile: PropTypes.func.isRequired,
  userId: PropTypes.number.isRequired,
  profile: PropTypes.shape({}).isRequired,
};

const mapStateToProps = (state) => ({
  userId: getUserId(state),
});

const mapDispatchToProps = (dispatch) => {
  return {
    deleteProfile: (payload) => {
      dispatch(deleteSBPProfile(payload));
    },
  };
};

const ButtonWithProps = connect(mapStateToProps, mapDispatchToProps)(Button);
