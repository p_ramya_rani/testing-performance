// 9fbef606107a605d69c0edbcd8029e5d 
import {StyleSheet} from 'react-native';
import {
  colorWhite,
  colorNero,
  colorNobel,
  fontFamilyNunito,
  fontSizeFourteen,
  weightFont,
} from '../../../features/shopByProfile/ShopByProfile.styles';

const style = StyleSheet.create({
  image: {
    height: 17,
    width: 17,
  },
  textOne: {
    fontWeight: weightFont,
  },
  textTwo: {
    color: colorNero,
    fontFamily: fontFamilyNunito,
    fontSize: fontSizeFourteen,
    fontWeight: weightFont,
    textDecorationLine: 'underline',
  },
  touchableOpacityOne: {
    alignItems: 'center',
    backgroundColor: colorWhite,
    borderColor: colorNobel,
    borderRadius: 85,
    borderStyle: 'dashed',
    borderWidth: 1,
    height: 86,
    justifyContent: 'center',
    width: 86,
  },
  touchableOpacityThree: {
    alignItems: 'center',
    borderRadius: 50,
    justifyContent: 'center',
    margin: 25,
    position: 'absolute',
    right: 0,
  },
  touchableOpacityTwo: {
    marginLeft: 12,
  },
  viewOne: {
    backgroundColor: colorWhite,
    borderRadius: 35,
    left: 4,
    paddingRight: 9,
  },
  viewTwo: {
    alignItems: 'center',
    backgroundColor: colorWhite,
    borderColor: colorNobel,
    borderRadius: 50,
    borderStyle: 'dashed',
    borderWidth: 1,
    flexDirection: 'row',
    height: 74,
    marginBottom: 7,
    marginVertical: 24,
  },
});

export default style;
