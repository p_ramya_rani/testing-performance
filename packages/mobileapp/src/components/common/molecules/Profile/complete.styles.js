// 9fbef606107a605d69c0edbcd8029e5d
import {StyleSheet} from 'react-native';
import {
  colorBlack,
  colorWhite,
  colorMortar,
  colorNero,
  weightFont,
  fontFamilyTofinoBold,
  fontFamilyTofinoRegular,
  fontFamilyNunitoRegular,
} from '../../../features/shopByProfile/ShopByProfile.styles';

const styles = StyleSheet.create({
  ageText: {color: colorMortar, fontFamily: fontFamilyNunitoRegular, fontSize: 9},
  arrowIcon: {height: 40, width: 45},
  badgesContainer: {
    flexDirection: 'row',
  },
  buttonContainer: {
    position: 'absolute',
    right: '2%',
  },
  container: {
    alignItems: 'center',
    backgroundColor: colorWhite,
    borderRadius: 50,
    elevation: 20,
    flexDirection: 'row',
    height: 74,
    marginBottom: 7,
    marginVertical: 18,
    shadowOffset: {height: 4, width: -1},
    shadowOpacity: 0.7,
    shadowRadius: 10,
  },
  genderText: {color: colorMortar, fontFamily: fontFamilyNunitoRegular, fontSize: 9},
  kebabIcon: {height: 18, width: 4},
  kebabIconContainer: {
    alignItems: 'center',
    borderRadius: 50,
    height: 45,
    justifyContent: 'center',
    width: 45,
  },
  leftSideProfileIcon: {left: 0, position: 'absolute', zIndex: 100},
  nameText: {
    color: colorNero,
    fontFamily: fontFamilyTofinoBold,
    fontSize: 14,
    fontWeight: weightFont,
  },

  plpCompleteContainer: {
    alignItems: 'center',
    backgroundColor: colorWhite,
    borderRadius: 50,
    elevation: 20,
    flexDirection: 'column',
    height: 74,
    justifyContent: 'center',
    marginVertical: 18,
    shadowColor: colorBlack,
    shadowOffset: {height: 2, width: 0},
    shadowOpacity: 0.2,
    shadowRadius: 5,
  },
  profileAgeGenderContainer: {flexDirection: 'row', marginTop: 4},
  rightSideProfileIcon: {position: 'absolute', right: 0},
  secondaryContainer: {
    marginLeft: 12,
  },
  textContainer: {flexDirection: 'row'},
  textName: {
    color: colorNero,
    fontFamily: fontFamilyTofinoBold,
    fontSize: 16,
    fontWeight: weightFont,
  },
  textPlace: {
    color: colorNero,
    fontFamily: fontFamilyTofinoRegular,
    fontSize: 16,
    marginLeft: 4,
  },
  viewOne: {alignItems: 'center', marginBottom: -20},
});

export default styles;
