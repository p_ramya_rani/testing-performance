// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import {Animated, Easing} from 'react-native';
import PropTypes from 'prop-types';
import {withTheme} from 'styled-components/native';
import {getScreenWidth} from '@tcp/core/src/utils/utils.app';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import styles, {LoadingImageContainer, LoadingImage} from '../styles/SpinnerIcon.native.style';

const defaultLoaderIcon = require('../../../../../assets/images/loading.png');

const spinAnim = new Animated.Value(0);
const translateYLoader = getScreenWidth() / 2 - 20;

const startSpinnerAnimation = () => {
  Animated.loop(
    Animated.timing(spinAnim, {
      toValue: 1,
      duration: 1500,
      easing: Easing.linear,
      useNativeDriver: true,
    }),
  ).start();
};

const stopSpinnerAnimation = () => {
  spinAnim.stopAnimation();
};

const toggleLoaderAnimation = startSpinning => {
  if (startSpinning) {
    startSpinnerAnimation();
  } else {
    stopSpinnerAnimation();
  }
};

const spin = spinAnim.interpolate({
  inputRange: [0, 1],
  outputRange: ['0deg', '360deg'],
});

const SpinnerIcon = props => {
  const {startSpinning} = props;
  const loadingIcon = defaultLoaderIcon;

  toggleLoaderAnimation(startSpinning);

  return (
    <LoadingImageContainer translateYLoader={translateYLoader}>
      <LoadingImage {...props} style={{transform: [{rotate: spin}]}} source={loadingIcon} />
    </LoadingImageContainer>
  );
};

SpinnerIcon.propTypes = {
  startSpinning: PropTypes.bool,
  height: PropTypes.number,
  width: PropTypes.number,
};

SpinnerIcon.defaultProps = {
  startSpinning: false,
  height: 40,
  width: 40,
};

export default withStyles(withTheme(SpinnerIcon), styles);
export {SpinnerIcon as SpinnerIconVanilla};
