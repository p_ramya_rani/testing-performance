// 9fbef606107a605d69c0edbcd8029e5d 
import styled, {css} from 'styled-components/native';
import {Animated} from 'react-native';

const LoadingImageContainer = styled.View`
  transform: translateX(${props => props.translateYLoader}px) translateY(6px);
  position: absolute;
`;

const LoadingImage = styled(Animated.Image)`
  height: ${props => props.height};
  width: ${props => props.width};
`;

const styles = css``;

export {styles, LoadingImageContainer, LoadingImage};
