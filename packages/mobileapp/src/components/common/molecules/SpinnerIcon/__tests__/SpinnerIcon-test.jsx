// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import {shallow} from 'enzyme';
import SpinnerIcon from '../view/SpinnerIcon.native';

describe('SpinnerIcon', () => {
  let component;

  const toggleLoaderAnimation = jest.fn();

  beforeEach(() => {
    const props = {
      startSpinning: true,
      toggleLoaderAnimation,
    };
    component = shallow(<SpinnerIcon {...props} />);
  });

  it('should be defined', () => {
    expect(component).toBeDefined();
  });

  it('should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
});
