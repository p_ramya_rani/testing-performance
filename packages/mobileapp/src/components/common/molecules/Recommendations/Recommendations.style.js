// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';
import {isAndroid} from '@tcp/core/src/utils';

export const CarouselContainer = styled.View`
  padding-bottom: 8px;
  ${props =>
    props.isCardTypeTiles
      ? `
  margin: 0 -14px;
`
      : `
  width: 100%;
`}
`;

export const ButtonContainer = styled.View`
  align-items: center;
  display: flex;
  margin-bottom: 32px;
  margin-top: 20px;
`;

export const AccordionContainer = styled.TouchableOpacity`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding-left: ${props => props.theme.spacing.ELEM_SPACING.SM};
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.MED};
  margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.MED};
`;

export const ImageStyleWrapper = styled.View`
  justify-content: center;
  align-items: center;
`;

export const PageContainer = styled.View``;

export const Container = styled.View``;

export const InGridPlpContainer = styled.View``;

export const InMutliGridContainer = styled.View`
  ${props => (props.isAndroid ? `elevation: 5;` : ``)}
  background-color: ${props => props.theme.colorPalette.white};
  border-top-right-radius: 11px;
  border-top-left-radius: 11px;
  shadow-color: ${props => props.theme.colorPalette.black};
  shadow-opacity: 0.37;
  shadow-radius: 1.49;

  border-left-width: 1px;
  border-right-width: 1px;
  border-style: solid;
  border-color: #eee;
  margin: 12px 0 12px 0;
`;

export const InGridRecHeader = styled.View`
  background-color: ${props => props.theme.colors.PRIMARY.COLOR3};
  border-top-right-radius: 11px;
  border-top-left-radius: 11px;
  height: 19;
`;

export const InGridRecText = styled.Text`
  font-family: ${props => props.theme.typography.fonts.secondary};
  font-size: ${props => props.theme.typography.fontSizes.fs12};
  color: ${props => props.theme.colorPalette.white};
  line-height: 15;
  text-align: center;
  letter-spacing: ${props => props.theme.typography.letterSpacings.ls038};
  font-weight: ${props => props.theme.typography.fontWeights.extrabold};
`;

export const TopCurveContainer = styled.View`
  background-color: white;
  height: 10;
  border-top-left-radius: 15;
  border-top-right-radius: 15;
  top: -5;
`;

export const OosContainer = styled.View`
  margin-left: 8px;
  padding-top: 5px;
  padding-bottom: 5px;
`;

export const RecommendationParent = styled.View`
  justify-content: space-between;
  display: flex;
`;

export const RecommendationPlp = styled.View`
  flex: 1;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin-top: 23px;
`;

export const PlpContainer = styled.TouchableOpacity`
  display: flex;
  flex-direction: row;
  justify-content: center;
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.SM};
  margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.SM};
`;

export const PlpModel = styled.View`
  flex-direction: row;
  width: 90%;
  padding: 4px 26px 3px;
  border-radius: 30px;
  border: solid 1px #f3f3f3;
  background-color: #ffffff;
  shadow-color: rgba(0, 0, 0, 0.15);
  shadow-opacity: 1;
  shadow-radius: 7.5;
  elevation: 13;
`;

export const ViewStyleText = styled.Text`
  margin: 20px 10px 20px 0;
  font-family: Nunito;
  font-size: 14px;
  font-weight: bold;
  font-style: normal;
  color: #1a1a1a;
`;

export const ImageRecommendation = styled.Image`
  width: 18%;
  margin: 0 5px;
`;

export const TextHeading = styled.Text`
  margin: 15px 18px 0 19px;
  font-family: Nunito;
  font-size: 14px;
  font-weight: ${isAndroid() ? 'bold' : '900'};
  text-align: center;
  color: #1a1a1a;
`;

export const CloseWrapper = styled.TouchableOpacity`
  display: flex;
  justify-content: flex-end;
  margin: 5px 17px 0 0;
  flex-direction: row;
`;

export const PullDrawerWrapper = styled.View`
  margin-top: 12px;
  margin-left: 37%;
  border-radius: 3px;
  background-color: #e1e1e1;
  width: 30%;
`;

export const TopWrapper = styled.View``;

export const GridContainer = styled.View`
  width: 49%;
  background-color: #fff;
  margin-bottom: 24px;
  margin-left: 5px;
  border-radius: 4px;
`;

export const GridContainerPlp = styled.View`
  width: 49%;
  background-color: #fff;
  margin-bottom: 25px;
  margin-left: 5px;
  border-radius: 4px;
`;

export const ATBContainer = styled.View`
  width: 130px;
  margin-top: 5px;
  height: 265px;
  margin-left: 5px;
  flex: 1;
`;

export const OosHeading = styled.Text`
  font-family: Nunito;
  font-size: ${props => props.theme.fonts.fontSize.body.bodytext.copy3};
  font-weight: ${props => props.theme.fonts.fontWeight.bold};
  text-align: center;
  color: ${props => props.theme.colors.TEXT.DARK};
  margin-bottom: 14px;
`;

export const PlaceCashHeader = styled.Text`
  font-family: Nunito-Black;
  font-size: ${props => props.theme.fonts.fontSize.body.bodytext.copy3};
  font-weight: bold;
  text-align: center;
  color: ${props => props.theme.colors.TEXT.DARK};
  width: 84%;
  margin-left: 8%;
`;
