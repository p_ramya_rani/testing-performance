// 9fbef606107a605d69c0edbcd8029e5d
import {connect} from 'react-redux';
import {getLabelValue} from '@tcp/core/src/utils';
import {setClickAnalyticsData, trackPageView} from '@tcp/core/src/analytics/actions';
import {
  fetchRecommendationsDataMonetate,
  fetchRecommendationsDataAdobe,
  sendEventsToMonetate,
} from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.actions';
import {
  isPlccUser,
  getUserLoggedInState,
  getMyPlaceNumber,
  getUserEmail,
} from '@tcp/core/src/components/features/account/User/container/User.selectors';
import {openQuickViewWithValues} from '@tcp/core/src/components/common/organisms/QuickViewModal/container/QuickViewModal.actions';
import {
  getProducts,
  getFirstSuggestedProduct,
  getRecommendationsAbTest,
  getRecommendationModuleOLabel,
  getRecommendationModulePLabel,
  getABTestConfig,
  getMboxName,
  getLabelSuffix,
  getMonetateEnabledFlag,
} from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.selector';
import {getAddedToBagError} from '@tcp/core/src/components/features/CnC/AddedToBag/container/AddedToBag.selectors';
import {
  getCurrentProduct,
  getPlpLabels,
  getPDPLabels,
} from '@tcp/core/src/components/features/browse/ProductDetail/container/ProductDetail.selectors';
import {
  getCurrentCurrency,
  getCurrencyAttributes,
} from '@tcp/core/src/reduxStore/selectors/currency.selectors';
import {processBreadCrumbs} from '@tcp/core/src/components/features/browse/ProductListing/container/ProductListing.util';
import {addToCartEcom} from '@tcp/core/src/components/features/CnC/AddedToBag/container/AddedToBag.actions';

import {
  getLoadingState,
  getOutfitProducts,
  getAddedToBagErrorCatId,
} from '@tcp/core/src/components/features/browse/OutfitDetails/container/OutfitDetails.selectors';
import BagPageSelector from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.selectors';
import {getIsPickupModalOpen} from '@tcp/core/src/components/common/organisms/PickupStoreModal/container/PickUpStoreModal.selectors';
import * as labelsSelectors from '@tcp/core/src/reduxStore/selectors/labels.selectors';
import selectors from '@tcp/core/src/components/features/CnC/Confirmation/container/Confirmation.selectors';
import {getOOSItemIdWhoseSuggestionRequested} from '@tcp/core/src/components/features/browse/Favorites/container/Favorites.selectors';
import Constants from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.constants';
import {getIsDynamicBadgeEnabled} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import logger from '@tcp/core/src/utils/loggerInstance';
import RecommendationsView from '../Recommendations.native';

function getRecommendationFilterProducts(products, portal) {
  switch (portal) {
    case Constants.RECOMMENDATIONS_MBOXNAMES.RECENTLY_VIEWED:
      return products.filter((item) => item.isRecentViewed);
    case Constants.RECOMMENDATIONS_MBOXNAMES.PLP_PRODUCTS:
      return products.filter((item) => item.isPlpProducts);
    case Constants.RECOMMENDATIONS_MBOXNAMES.OUT_OF_STOCK_PDP:
      return products.filter((item) => item.isAbPdpOos);
    case Constants.RECOMMENDATIONS_MBOXNAMES.ADD_PRODUCT_TO_GET_PLACECASH:
      return products.filter((item) => item.isAbCartCash);
    case Constants.RECOMMENDATIONS_MBOXNAMES.BAG_CART_REWARD_PLCC:
      return products.filter((item) => item.isAbCartReward);
    case Constants.RECOMMENDATIONS_MBOXNAMES.STYLE_WITH:
      return products.filter((item) => item.styleWithEligibility || item.isRecentViewed === false);
    case Constants.RECOMMENDATIONS_MBOXNAMES.ORDER_NOTIFICATION:
      return products.filter((item) => item.isOrderNotifications);
    case Constants.RECOMMENDATIONS_MBOXNAMES.NAVLEVEL2_Girls:
      return products.filter((item) => item.isNavLevel2Girls);
    case Constants.RECOMMENDATIONS_MBOXNAMES.NAVLEVEL2_Boys:
      return products.filter((item) => item.isNavLevel2Boys);
    case Constants.RECOMMENDATIONS_MBOXNAMES.NAVLEVEL2_Toddler_Girls:
      return products.filter((item) => item.isNavLevel2ToddlerGirls);
    case Constants.RECOMMENDATIONS_MBOXNAMES.NAVLEVEL2_Toddler_Boys:
      return products.filter((item) => item.isNavLevel2ToddlerBoys);
    case Constants.RECOMMENDATIONS_MBOXNAMES.NAVLEVEL2_Baby:
      return products.filter((item) => item.isNavLevel2Baby);
    case Constants.RECOMMENDATIONS_MBOXNAMES.IN_GRID_PLP_PRODUCTS:
      return products.filter((item) => item.isInGridPlp);
    case Constants.RECOMMENDATIONS_MBOXNAMES.IN_MULTI_GRID_PLP_PRODUCTS:
      return products.filter((item) => item.isInMultiGridPlp);
    default:
      return products.filter((item) => item.isRecentViewed === false);
  }
}

function getRecommendationProducts(state, ownProps, ooSItemIdWhoseSuggestionRequested, reduxKey) {
  let {page} = ownProps;
  if (page === 'home page') page = 'homepage';

  const MONETATE_ENABLED = getMonetateEnabledFlag();

  let products = [];
  if (MONETATE_ENABLED === true)
    products = ownProps.isSuggestedItem
      ? getFirstSuggestedProduct(state, ooSItemIdWhoseSuggestionRequested)
      : getProducts(state, page || 'homepageTest');
  else {
    products = ownProps.isSuggestedItem
      ? getFirstSuggestedProduct(state, ooSItemIdWhoseSuggestionRequested)
      : getProducts(state, reduxKey);
    return products;
  }

  if (!Array.isArray(products)) return [];
  return getRecommendationFilterProducts(products, ownProps.portalValue);
}

function getCartItems(state) {
  const cartItems = [];
  const {getOrderItems, getItemPrice, getItemUPC, getItemPartNumber, getItemQuantity} =
    BagPageSelector;
  const items = getOrderItems(state);
  if (items) {
    items.forEach((item) => {
      cartItems.push({
        value: getItemPrice(item),
        sku: getItemUPC(item),
        pid: getItemPartNumber(item),
        quantity: getItemQuantity(item),
        currency: getCurrentCurrency(state),
      });
    });
  }
  return cartItems;
}
const getReduxKey = (page, portalValue) => `${page}_${portalValue || 'global'}_products`;
const getCtaText = (abTestConfig, labelSuffix) =>
  abTestConfig ? `CTA_TEXT_${labelSuffix}` : 'CTA_TEXT';
const getCtaTitle = (abTestConfig, labelSuffix) =>
  abTestConfig ? `CTA_TITLE_${labelSuffix}` : 'CTA_TITLE';
const getCtaUrl = (abTestConfig, labelSuffix) =>
  abTestConfig ? `CTA_URL_${labelSuffix}` : 'CTA_URL';

const getConfirmationItems = (item, cartItems, state) => {
  if (item) {
    cartItems.push({
      value: item.productInfo && item.productInfo.offerPrice,
      sku: item.itemAtributes.UPC,
      pid: item.productInfo && item.productInfo.productPartNumber,
      quantity: item.qty,
      currency: getCurrentCurrency(state),
    });
  }
};
// eslint-disable-next-line complexity
const mapStateToProps = (state, ownProps) => {
  const {page, portalValue, excludedIds} = ownProps;
  const ooSItemIdWhoseSuggestionRequested = getOOSItemIdWhoseSuggestionRequested(state);
  const MONETATE_ENABLED = getMonetateEnabledFlag();

  let newPortalValue = portalValue;
  let abTestConfig = null;
  let labelSuffix = `${page}_${ownProps.sequence}`;

  const abTestRecommendations = getRecommendationsAbTest(state, ownProps.isAddedToBag);

  if (abTestRecommendations) {
    abTestConfig = getABTestConfig(abTestRecommendations, ownProps);
    newPortalValue = getMboxName(abTestConfig);
    labelSuffix = getLabelSuffix(page, ownProps, abTestRecommendations);
  }

  const moduleOHeaderLabel = getRecommendationModuleOLabel(
    abTestConfig,
    state,
    labelSuffix,
    ownProps,
    getLabelValue,
  );

  const modulePHeaderLabel = getRecommendationModulePLabel(
    abTestConfig,
    state,
    labelSuffix,
    ownProps,
    getLabelValue,
  );

  const reduxKey = getReduxKey(page, portalValue);
  const ctaText = getCtaText(abTestConfig, labelSuffix);
  const ctaTitle = getCtaTitle(abTestConfig, labelSuffix);
  const ctaUrl = getCtaUrl(abTestConfig, labelSuffix);

  let cartItems = [];
  let isBagLoading = false;
  let isOutfitLoading = false;
  let outfitItems = [];

  try {
    if (page === 'cart') {
      isBagLoading = BagPageSelector.isBagLoading(state);
      if (!isBagLoading) {
        cartItems = getCartItems(state);
      }
    }

    if (page === 'checkout') {
      const items = selectors.getOrderItems(state);
      if (Array.isArray(items)) {
        items.forEach((item) => {
          getConfirmationItems(item, cartItems, state);
        });
      }
    }

    if (page === 'outfit') {
      isOutfitLoading = getLoadingState(state);
      if (!isOutfitLoading) {
        const items = getOutfitProducts(state);
        outfitItems = items.map((item) => ({productId: item.generalProductId}));
      }
    }
  } catch (e) {
    logger.error({
      error: e,
      extraData: {
        page,
        methodName: 'recommendations',
        propsReceived: ownProps,
      },
    });
  }

  return {
    products: getRecommendationProducts(
      state,
      ownProps,
      ooSItemIdWhoseSuggestionRequested,
      reduxKey,
    ),
    moduleOHeaderLabel,
    modulePHeaderLabel,
    ctaText: getLabelValue(state.Labels, ctaText, 'recommendations', 'global'),
    ctaTitle: getLabelValue(state.Labels, ctaTitle, 'recommendations', 'global'),
    ctaUrl: getLabelValue(state.Labels, ctaUrl, 'recommendations', 'global'),
    isPlcc: isPlccUser(state),
    isPickupModalOpen: getIsPickupModalOpen(state),
    labels: {
      ...labelsSelectors.getPlpTilesLabels(state),
      ...selectors.getConfirmationLabels(state),
    },
    currencyAttributes: getCurrencyAttributes(state),
    currency: getCurrentCurrency(state),
    showPriceRange: ownProps.showPriceRange,
    reduxKey,
    excludedIds,
    cartItems,
    isBagLoading,
    isOutfitLoading,
    outfitItems,
    portalValue: newPortalValue,
    MONETATE_ENABLED,
    isUserLoggedIn: getUserLoggedInState(state),
    myPlaceNumber: getMyPlaceNumber(state),
    breadCrumbs: processBreadCrumbs(state.ProductListing && state.ProductListing.breadCrumbTrail),
    currentProduct: getCurrentProduct(state),
    userEmail: getUserEmail(state),
    plpLabels: getPlpLabels(state),
    pdpLabels: getPDPLabels(state),
    addToBagError: getAddedToBagError(state),
    addToBagErrorId: getAddedToBagErrorCatId(state),
    isDynamicBadgeEnabled: getIsDynamicBadgeEnabled(state),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    loadRecommendationsMonetate: (action) => {
      dispatch(fetchRecommendationsDataMonetate(action));
    },
    loadSendEventsToMonetate: (action) => {
      dispatch(sendEventsToMonetate(action));
    },
    loadRecommendationsAdobe: (action) => {
      dispatch(fetchRecommendationsDataAdobe(action));
    },
    onQuickViewOpenClick: (payload) => {
      dispatch(openQuickViewWithValues(payload));
    },
    trackAnalyticsOnAddToBagButtonClick: (payload, customData) => {
      dispatch(setClickAnalyticsData(customData));
      dispatch(trackPageView(payload));
    },
    addToBagEcomAction: (payload) => {
      dispatch(addToCartEcom(payload));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RecommendationsView);
