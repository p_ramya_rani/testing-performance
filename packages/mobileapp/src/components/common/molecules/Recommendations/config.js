// 9fbef606107a605d69c0edbcd8029e5d 
export default {
  params: {
    moduleO: {
      dataLocator: 'moduleO_header_text',
      dataLocatorCTA: 'moduleO_cta_btn',
    },
    moduleP: {
      dataLocator: 'moduleP_header_text',
      dataLocatorCTA: 'moduleP_cta_btn',
    },
    styleWith: {
      dataLocator: 'styleWith_header_text',
      dataLocatorCTA: 'styleWith_cta_btn',
    },
  },
  variations: {
    moduleO: 'moduleO',
    moduleP: 'moduleP',
    styleWith: 'styleWith',
  },
};
