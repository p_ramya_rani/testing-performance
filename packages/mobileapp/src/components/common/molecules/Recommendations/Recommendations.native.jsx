/* eslint-disable max-lines */
/* eslint-disable complexity */
// 9fbef606107a605d69c0edbcd8029e5d
import React, {useEffect, useState, useRef} from 'react';
import {arrayOf, bool, func, shape, string} from 'prop-types';
import ModalBox from 'react-native-modalbox';
import {Image, StyleSheet, View} from 'react-native';
import {FlatList} from 'react-native-gesture-handler';
import ModuleO from '@tcp/core/src/components/common/molecules/ModuleO';
import ModuleP from '@tcp/core/src/components/common/molecules/ModuleP';
import Heading from '@tcp/core/src/components/common/atoms/Heading';
import {getLocator, getScreenHeight} from '@tcp/core/src/utils/index.native';
import {Button, BodyCopy, Anchor} from '@tcp/core/src/components/common/atoms';
import AccessibilityRoles from '@tcp/core/src/constants/AccessibilityRoles.constant';
import Constants from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.constants';
import RecomStyleWith from '@tcp/core/src/components/common/molecules/RecomStyleWith';
import PaginationDots from '@tcp/core/src/components/common/molecules/PaginationDots';
import {isAndroid, isGymboree} from '@tcp/core/src/utils';
import {
  CarouselContainer,
  ButtonContainer,
  AccordionContainer,
  ImageStyleWrapper,
  PageContainer,
  OosContainer,
  Container,
  RecommendationParent,
  PlpContainer,
  PlpModel,
  ViewStyleText,
  ImageRecommendation,
  TextHeading,
  CloseWrapper,
  RecommendationPlp,
  GridContainer,
  GridContainerPlp,
  TopWrapper,
  ATBContainer,
  PullDrawerWrapper,
  OosHeading,
  PlaceCashHeader,
  InGridPlpContainer,
  InMutliGridContainer,
  InGridRecText,
  InGridRecHeader,
  TopCurveContainer,
} from './Recommendations.style';
import config from './config';
import constant from './Recommendations.constant';

const closeIcon = require('../../../../assets/images/icons-elements-icons-close-mobile-nav.png');
const downIcon = require('../../../../assets/images/carrot-small-down.png');
const upIcon = require('../../../../assets/images/carrot-small-up.png');
const drawerPullIndicator = require('../../../../assets/images/drawer-pull-indicator.png');

const viewProducts = 6;
const black = '#000000';
const height = getScreenHeight();
const loadVariation = (variation, variationProps) => (itemProps) => {
  const {
    isPlcc,
    onQuickViewOpenClick,
    priceOnly,
    navigation,
    isRecentlyViewed,
    headerLabel,
    isPlaceCashReward,
    closeATBModal,
    portalValue,
    productImageWidth,
    isCardTypeTiles,
    trackAnalyticsOnAddToBagButtonClick,
    isNavL2,
    isDynamicBadgeEnabled,
    quickViewLoader,
    quickViewProductId,
    fireHapticFeedback,
    ...others
  } = variationProps;
  const {item, index} = itemProps;
  const title = item.name;
  const {RECOMMENDATION} = constant;
  if (variation === 'moduleO') {
    return (
      <ModuleO
        isPlcc={isPlcc}
        index={index}
        title={title}
        priceOnly={priceOnly}
        navigation={navigation}
        isPlaceCashReward={isPlaceCashReward}
        onQuickViewOpenClick={onQuickViewOpenClick}
        trackAnalyticsOnAddToBagButtonClick={trackAnalyticsOnAddToBagButtonClick}
        viaModule={RECOMMENDATION}
        portalValue={portalValue}
        viaModuleName={headerLabel}
        closeATBModal={closeATBModal}
        productImageWidth={productImageWidth}
        isCardTypeTiles={isCardTypeTiles}
        isRecommendations
        isNavL2={isNavL2}
        isDynamicBadgeEnabled={isDynamicBadgeEnabled}
        quickViewLoader={quickViewLoader}
        quickViewProductId={quickViewProductId}
        fireHapticFeedback={fireHapticFeedback}
        {...itemProps}
        {...others}
      />
    );
  }
  return (
    <ModuleP
      isPlcc={isPlcc}
      title={title}
      onQuickViewOpenClick={onQuickViewOpenClick}
      trackAnalyticsOnAddToBagButtonClick={trackAnalyticsOnAddToBagButtonClick}
      navigation={navigation}
      viaModule={RECOMMENDATION}
      viaModuleName={headerLabel}
      productImageWidth={productImageWidth}
      isCardTypeTiles={isCardTypeTiles}
      priceOnly={priceOnly}
      isRecentlyViewed={isRecentlyViewed}
      isRecommendations
      isDynamicBadgeEnabled={isDynamicBadgeEnabled}
      fireHapticFeedback={fireHapticFeedback}
      {...itemProps}
      {...others}
    />
  );
};

const loadVariationNew = (variation, variationProps) => (itemProps) => {
  const {
    isPlcc,
    onQuickViewOpenClick,
    trackAnalyticsOnAddToBagButtonClick,
    priceOnly,
    navigation,
    isRecentlyViewed,
    headerLabel,
    portalValue,
    component,
    productImageWidth,
    isCardTypeTiles,
    closeATBModal,
    isRecommendationHeight,
    boxWithShadow = {},
    isDynamicBadgeEnabled,
    fireHapticFeedback,
    ...others
  } = variationProps;
  const Component = component || View;
  const {RECOMMENDATION} = constant;
  const {
    item: {name},
  } = itemProps;

  if (variation === 'moduleO') {
    return (
      <Component style={boxWithShadow}>
        <ModuleO
          productImageWidth={productImageWidth}
          isPlcc={isPlcc}
          title={name}
          priceOnly={priceOnly}
          navigation={navigation}
          onQuickViewOpenClick={onQuickViewOpenClick}
          trackAnalyticsOnAddToBagButtonClick={trackAnalyticsOnAddToBagButtonClick}
          viaModule={RECOMMENDATION}
          isCardTypeTiles={isCardTypeTiles}
          portalValue={portalValue}
          viaModuleName={headerLabel}
          closeATBModal={closeATBModal}
          isRecommendations={isRecommendationHeight}
          isDynamicBadgeEnabled={isDynamicBadgeEnabled}
          fireHapticFeedback={fireHapticFeedback}
          {...itemProps}
          {...others}
        />
      </Component>
    );
  }
  return (
    <Component>
      <ModuleP
        isPlcc={isPlcc}
        title={name}
        onQuickViewOpenClick={onQuickViewOpenClick}
        navigation={navigation}
        viaModule={RECOMMENDATION}
        viaModuleName={headerLabel}
        priceOnly={priceOnly}
        productImageWidth={productImageWidth}
        isCardTypeTiles={isCardTypeTiles}
        portalValue={portalValue}
        isRecentlyViewed={isRecentlyViewed}
        isRecommendations={isRecommendationHeight}
        isDynamicBadgeEnabled={isDynamicBadgeEnabled}
        {...itemProps}
        {...others}
      />
    </Component>
  );
};

const ButtonView = (buttonProps) => {
  const {ctaText, ctaTitle, ctaUrl, navigation} = buttonProps;

  return (
    <ButtonContainer>
      <Button
        width="225px"
        accessibilityLabel={ctaTitle}
        text={ctaText}
        testID={getLocator('moduleD_button')}
        url={ctaUrl}
        navigation={navigation}
      />
    </ButtonContainer>
  );
};

const styles = StyleSheet.create({
  modalBox: {
    borderRadius: 20,
    height: height * 0.82,
  },
  recommendationHeaderAddedToBag: {
    color: black,
    fontFamily: 'Nunito',
    fontSize: 16,
    fontStyle: 'normal',
    fontWeight: '600',
    height: 25,
    margin: 0,
    marginLeft: 20,
    width: 'auto',
  },
});

const getGymStyling = () => {
  return isGymboree()
    ? {fontFamily: 'primary', fontWeight: 'semibold'}
    : {fontFamily: 'secondary', fontWeight: 'bold'};
};

// eslint-disable-next-line sonarjs/cognitive-complexity
const renderHeader = (prop, variation, isAccordionOpen, setIsAccordionOpen) => {
  const {
    moduleOHeaderLabel,
    modulePHeaderLabel,
    isHeaderAccordion,
    isSuggestedItem,
    portalValue,
    isPlaceCashReward,
    newRecommendationEnabled,
    plpLabels,
    isCardTypeTiles,
    isNavL2,
  } = prop;
  const params = config.params[variation];
  const headerLabel =
    variation === config.variations.moduleO ? moduleOHeaderLabel : modulePHeaderLabel;

  if (isSuggestedItem) {
    return null;
  }

  if (isCardTypeTiles) {
    return (
      <BodyCopy
        fontFamily={getGymStyling().fontFamily}
        fontWeight={getGymStyling().fontWeight}
        fontSize="fs20"
        color="gray.900"
        textAlign="left"
        letterSpacing="ls1"
        text={headerLabel}
        margin="0px 10px 10px 7px"
      />
    );
  }

  if (isHeaderAccordion) {
    return (
      <AccordionContainer
        onPress={() => !isNavL2 && setIsAccordionOpen(!isAccordionOpen)}
        accessible
        accessibilityRole={AccessibilityRoles.Button}
        accessibilityLabel={headerLabel}
        accessibilityState={{expanded: isAccordionOpen}}>
        <BodyCopy
          fontFamily={isNavL2 ? 'primary' : 'secondary'}
          fontWeight="black"
          fontSize={isNavL2 ? 'fs20' : 'fs14'}
          isAccordionOpen={isAccordionOpen}
          text={headerLabel.toUpperCase()}
          textAlign="center"
        />
        {!isNavL2 && !newRecommendationEnabled && (
          <ImageStyleWrapper>
            <Anchor onPress={() => setIsAccordionOpen(!isAccordionOpen)}>
              <Image source={isAccordionOpen ? upIcon : downIcon} />
            </Anchor>
          </ImageStyleWrapper>
        )}
      </AccordionContainer>
    );
  }

  if (portalValue === Constants.RECOMMENDATIONS_MBOXNAMES.PLP_PRODUCTS) {
    return <TextHeading>{plpLabels.plpPersonalized}</TextHeading>;
  }
  if (portalValue === Constants.RECOMMENDATIONS_MBOXNAMES.OUT_OF_STOCK_PDP) {
    return <OosHeading>{headerLabel}</OosHeading>;
  }

  if (portalValue === Constants.RECOMMENDATIONS_MBOXNAMES.ADD_TO_BAG_MODAL_CAROUSAL) {
    return (
      <Heading
        locator={params.dataLocator}
        text={headerLabel}
        style={styles.recommendationHeaderAddedToBag}
      />
    );
  }

  if (isPlaceCashReward) {
    return <PlaceCashHeader>{headerLabel}</PlaceCashHeader>;
  }

  return (
    <Heading
      locator={params.dataLocator}
      text={headerLabel}
      fontFamily="primary"
      fontSize="fs20"
      fontWeight="semibold"
      textAlign="center"
      color="gray.900"
    />
  );
};

const onKeyExtractor = (item, index, partNumber) => {
  const key = partNumber || item.uniqueId || item.generalProductId || item.reduxKey;
  return `suggested-item-${key}-${index}`;
};

const showMoreProducts = (products, showProducts, setShowProducts) => {
  let count = products.length;
  if (products.length <= viewProducts && showProducts.length < products.length) {
    count = products.length;
  } else if (products.length - showProducts.length > viewProducts) {
    count = showProducts.length + viewProducts;
  }
  setShowProducts(products.slice(0, count));
};

const ButtonViewMore = (buttonProps) => {
  const {showProducts, products, setShowProducts} = buttonProps;
  const showMoreCount =
    products.length - showProducts.length > viewProducts
      ? viewProducts
      : products.length - showProducts.length;
  const ctaText = `View More`;
  if (showMoreCount === 0) return null;
  return (
    <ButtonContainer>
      <Button
        width="225px"
        text={ctaText}
        onPress={() => showMoreProducts(products, showProducts, setShowProducts)}
      />
    </ButtonContainer>
  );
};

const plpRecommendation = (
  variationProps,
  variation,
  open,
  setOpen,
  isAccordionOpen,
  setIsAccordionOpen,
) => {
  const {
    moduleOHeaderLabel,
    modulePHeaderLabel,
    products,
    priceOnly,
    isPlcc,
    onQuickViewOpenClick,
    trackAnalyticsOnAddToBagButtonClick,
    navigation,
    isRecentlyViewed,
    partNumber,
    portalValue,
    page,
    headerLabel,
    plpLabels,
    isFavoriteRecommendation,
    isDynamicBadgeEnabled,
    fireHapticFeedback,
    ...others
  } = variationProps;

  return (
    <React.Fragment>
      {!isFavoriteRecommendation &&
        renderHeader(variationProps, variation, isAccordionOpen, setIsAccordionOpen)}
      <PlpContainer
        onPress={() => {
          setOpen(true);
        }}>
        <PlpModel>
          <ViewStyleText>{plpLabels.plpRecosProductTilesTitle}</ViewStyleText>
          {[0, 1, 2].map(
            (index) =>
              products[index] && (
                <ImageRecommendation
                  source={{
                    uri: products[index].imagePath,
                  }}
                />
              ),
          )}
        </PlpModel>
      </PlpContainer>
      <ModalBox
        animationDuration={400}
        swipeThreshold={10}
        style={styles.modalBox}
        position="bottom"
        swipeArea={20}
        isOpen={open}
        onClosed={() => setOpen(false)}
        entry="bottom"
        backdropPressToClose={false}
        coverScreen
        swipeToClose>
        <TopWrapper>
          <PullDrawerWrapper>
            <Image source={drawerPullIndicator} />
          </PullDrawerWrapper>
          <CloseWrapper onPress={() => setOpen(false)}>
            <Image source={closeIcon} />
          </CloseWrapper>
        </TopWrapper>
        <RecommendationPlp>
          <FlatList
            data={products}
            renderItem={loadVariationNew(variation, {
              priceOnly,
              isPlcc,
              onQuickViewOpenClick,
              trackAnalyticsOnAddToBagButtonClick,
              navigation,
              isRecentlyViewed,
              headerLabel,
              portalValue,
              productImageWidth: 164,
              productImageHeight: 204,
              page,
              isHidePLPAddToBag: true,
              component: GridContainerPlp,
              isRecommendationHeight: false,
              setOpen,
              isDynamicBadgeEnabled,
              ...others,
            })}
            keyExtractor={(item, index) => onKeyExtractor(item, index, partNumber)}
            horizontal={false}
            numColumns={2}
          />
        </RecommendationPlp>
      </ModalBox>
    </React.Fragment>
  );
};

const atbRecommendations = (props, variation, isAccordionOpen, setIsAccordionOpen) => {
  const {
    moduleOHeaderLabel,
    modulePHeaderLabel,
    products,
    priceOnly,
    isPlcc,
    onQuickViewOpenClick,
    trackAnalyticsOnAddToBagButtonClick,
    navigation,
    isRecentlyViewed,
    isFavoriteRecommendation,
    portalValue,
    page,
    closeATBModal,
    isDynamicBadgeEnabled,
    ...others
  } = props;
  const headerLabel = variation === 'moduleO' ? moduleOHeaderLabel : modulePHeaderLabel;
  return (
    products &&
    products.length > 0 && (
      <Container>
        {!isFavoriteRecommendation &&
          renderHeader(props, variation, isAccordionOpen, setIsAccordionOpen)}
        <CarouselContainer>
          <FlatList
            data={products}
            renderItem={loadVariationNew(variation, {
              priceOnly,
              isPlcc,
              onQuickViewOpenClick,
              trackAnalyticsOnAddToBagButtonClick,
              navigation,
              isRecentlyViewed,
              portalValue,
              page,
              headerLabel,
              closeATBModal,
              component: ATBContainer,
              isRecommendationHeight: true,
              productImageWidth: 122,
              productImageHeight: 156,
              isDynamicBadgeEnabled,
              ...others,
            })}
            refreshing={false}
            horizontal
            showsHorizontalScrollIndicator={false}
            keyExtractor={(_, index) => index.toString()}
          />
        </CarouselContainer>
      </Container>
    )
  );
};
const renderRecomStyleWith = ({
  products,
  plpLabels,
  addToBagEcomAction,
  navigation,
  pdpLabels,
  currency,
  closeATBModal,
  addToBagError,
}) => {
  if (products && products.length) {
    const product = products[0];
    const styleWithData = product.styleWithData && product.styleWithData.product;
    return styleWithData ? (
      <>
        <RecomStyleWith
          headerLabel={pdpLabels && pdpLabels.headingStyleWith}
          productItem={styleWithData}
          productData={product}
          plpLabels={plpLabels}
          addToBagEcom={addToBagEcomAction}
          navigation={navigation}
          currency={currency}
          closeATBModal={closeATBModal}
          addToBagError={addToBagError}
        />
      </>
    ) : null;
  }
  return null;
};

const getHeaderLabel = (variation, moduleOHeaderLabel, modulePHeaderLabel) => {
  return variation === 'moduleO' ? moduleOHeaderLabel : modulePHeaderLabel;
};

const getColumnWrapperStyle = () => {
  return {
    justifyContent: 'space-between',
  };
};

const isPromoAvailable = (products) =>
  products.map((product) => product.productInfo && product.productInfo.promotionalMessage);

const loadCardTypeProductTiles = (props, variation) => {
  const {
    moduleOHeaderLabel,
    modulePHeaderLabel,
    products,
    priceOnly,
    isPlcc,
    onQuickViewOpenClick,
    trackAnalyticsOnAddToBagButtonClick,
    navigation,
    isRecentlyViewed,
    isFavoriteRecommendation,
    portalValue,
    page,
    closeATBModal,
    isDynamicBadgeEnabled,
    productImageWidth,
    isCardTypeTiles,
    quickViewLoader,
    quickViewProductId,
    ...others
  } = props;
  return (
    products &&
    products.length > 0 && (
      <Container>
        {!isFavoriteRecommendation && renderHeader(props, variation)}
        <CarouselContainer isCardTypeTiles={isCardTypeTiles}>
          <FlatList
            data={products}
            renderItem={loadVariation(variation, {
              priceOnly,
              isPlcc,
              onQuickViewOpenClick,
              trackAnalyticsOnAddToBagButtonClick,
              navigation,
              isRecentlyViewed,
              portalValue,
              page,
              productImageWidth,
              isCardTypeTiles,
              closeATBModal,
              isDynamicBadgeEnabled,
              quickViewLoader,
              quickViewProductId,
              ...others,
            })}
            refreshing={false}
            horizontal
            showsHorizontalScrollIndicator={false}
            keyExtractor={(_, index) => index.toString()}
          />
        </CarouselContainer>
      </Container>
    )
  );
};

const loadInGridPlpRecommendation = (variation, props) => {
  // eslint-disable-next-line react/prop-types
  const {products, index, slots, flatSlots, portalValue, labels} = props;
  let len = 1;
  let pos = slots.indexOf(index);
  let columnStyle = '';
  if (flatSlots) {
    pos = flatSlots.indexOf(index);
    len = 2;
    columnStyle = getColumnWrapperStyle();
  }

  const headerLabel =
    portalValue === Constants.RECOMMENDATIONS_MBOXNAMES.IN_GRID_PLP_PRODUCTS
      ? labels.lbl_plp_in_grid_header
      : labels.lbl_plp_in_multi_grid_header;

  const currentRecosProduct =
    products.slice(pos, pos + len).length === 2
      ? products.slice(pos, pos + len)
      : products.slice(0, len);

  return (
    <InMutliGridContainer isAndroid={isAndroid()}>
      <InGridRecHeader>
        <InGridRecText
          accessible={headerLabel !== ''}
          accessibilityRole="text"
          accessibilityLabel={headerLabel}>
          {headerLabel}
        </InGridRecText>
      </InGridRecHeader>
      <TopCurveContainer />
      <FlatList
        data={currentRecosProduct}
        renderItem={loadVariationNew(variation, {
          component: InGridPlpContainer,
          isPromoAvailable: isPromoAvailable(currentRecosProduct),
          paddings: '0',
          ...props,
        })}
        refreshing={false}
        showsHorizontalScrollIndicator={false}
        keyExtractor={(_, index1) => index1.toString()}
        columnWrapperStyle={columnStyle}
        numColumns={len}
        windowSize={21}
        initialNumToRender={2}
        maxToRenderPerBatch={2}
      />
    </InMutliGridContainer>
  );
};

// eslint-disable-next-line sonarjs/cognitive-complexity
const renderRecommendationView = (props, variation) => {
  const [isAccordionOpen, setIsAccordionOpen] = useState(true);
  const [showProducts, setShowProducts] = useState([]);
  const [open, setOpen] = useState(false);
  const [currentActiveItem, setCurrentActiveItem] = useState(0);

  const onViewRef = useRef((viewableItems) => {
    const changes = viewableItems.changed;
    const len = (changes && changes.length) || 0;
    for (let i = 0; i < len; i += 1) {
      const item = changes[i];
      const {isViewable, index} = item;
      if (isViewable) {
        const val = Math.floor(index / 2);
        setCurrentActiveItem(val);
        break;
      }
    }
    // Use viewable items in state or as intended
  });
  const viewConfigRef = React.useRef({viewAreaCoveragePercentThreshold: 50});

  const {
    moduleOHeaderLabel,
    modulePHeaderLabel,
    products,
    isPlcc,
    onQuickViewOpenClick,
    trackAnalyticsOnAddToBagButtonClick,
    priceOnly,
    showButton,
    navigation,
    isRecentlyViewed,
    isHeaderAccordion,
    handleAccordionToggle,
    isFavoriteRecommendation,
    partNumber,
    newRecommendationEnabled,
    portalValue,
    page,
    styleWithView,
    plpLabels,
    addToBagEcomAction,
    pdpLabels,
    currency,
    closeATBModal,
    isPlaceCashReward,
    getRecommendationProducts = () => {},
    addToBagError,
    disableViewMore = false,
    isDynamicBadgeEnabled,
    handleClose,
    productImageWidth,
    isCardTypeTiles,
    isHomePage = false,
    ...others
  } = props;

  const {isSuggestedItem} = {...others};
  const showCarousel = isHeaderAccordion ? isAccordionOpen : true;
  const headerLabel = getHeaderLabel(variation, moduleOHeaderLabel, modulePHeaderLabel);
  if (styleWithView && products.length && products[0].styleWithEligibility) {
    return renderRecomStyleWith({
      products,
      plpLabels,
      addToBagEcomAction,
      navigation,
      pdpLabels,
      currency,
      closeATBModal,
      addToBagError,
    });
  }

  if (isCardTypeTiles) {
    return products && products.length > 0 && loadCardTypeProductTiles(props, variation);
  }

  if (
    products.length !== 0 &&
    newRecommendationEnabled &&
    !isSuggestedItem &&
    showProducts.length === 0
  ) {
    if (disableViewMore) setShowProducts(products);
    else showMoreProducts(products, showProducts, setShowProducts);
  }

  if (portalValue === Constants.RECOMMENDATIONS_MBOXNAMES.IN_GRID_PLP_PRODUCTS) {
    return products && products.length > 0 && loadInGridPlpRecommendation(variation, props);
  }

  if (portalValue === Constants.RECOMMENDATIONS_MBOXNAMES.IN_MULTI_GRID_PLP_PRODUCTS) {
    return products && products.length > 0 && loadInGridPlpRecommendation(variation, props);
  }

  if (portalValue === Constants.RECOMMENDATIONS_MBOXNAMES.OUT_OF_STOCK_PDP) {
    return (
      products &&
      products.length > 0 && (
        <OosContainer>
          {renderHeader(props, variation, isAccordionOpen, setIsAccordionOpen)}
          <FlatList
            data={products}
            renderItem={loadVariationNew(variation, {
              priceOnly,
              isPlcc,
              onQuickViewOpenClick,
              trackAnalyticsOnAddToBagButtonClick,
              navigation,
              isRecentlyViewed,
              headerLabel,
              portalValue,
              page,
              productImageWidth: 130,
              productImageHeight: 162,
              isHidePLPAddToBag: true,
              isRecommendationHeight: true,
              isDynamicBadgeEnabled,
              ...others,
            })}
            refreshing={false}
            horizontal
            showsHorizontalScrollIndicator={false}
            keyExtractor={(_, index) => index.toString()}
          />
        </OosContainer>
      )
    );
  }

  if (portalValue === Constants.RECOMMENDATIONS_MBOXNAMES.ADD_TO_BAG_MODAL_CAROUSAL) {
    return atbRecommendations(props, variation, isAccordionOpen, setIsAccordionOpen);
  }
  if (portalValue === Constants.RECOMMENDATIONS_MBOXNAMES.PLP_PRODUCTS) {
    return (
      products &&
      products.length > 0 &&
      plpRecommendation(props, variation, open, setOpen, isAccordionOpen, setIsAccordionOpen)
    );
  }

  let productLength = 0;
  if (products && products.length > 0) {
    productLength = Math.floor(products.length / 2);
  }

  getRecommendationProducts(products);

  return (
    products &&
    products.length > 0 && (
      <Container>
        {!isFavoriteRecommendation &&
          renderHeader(props, variation, isAccordionOpen, setIsAccordionOpen)}
        {isSuggestedItem && (
          <FlatList
            data={products}
            renderItem={loadVariation(variation, {
              priceOnly,
              isPlcc,
              onQuickViewOpenClick,
              trackAnalyticsOnAddToBagButtonClick,
              navigation,
              isRecentlyViewed,
              portalValue,
              page,
              headerLabel,
              isDynamicBadgeEnabled,
              ...others,
            })}
            keyExtractor={(item, index) => onKeyExtractor(item, index, partNumber)}
            initialNumToRender={1}
          />
        )}

        {newRecommendationEnabled && !isSuggestedItem && (
          <RecommendationParent>
            <FlatList
              data={showProducts}
              renderItem={loadVariationNew(variation, {
                priceOnly,
                isPlcc,
                onQuickViewOpenClick,
                trackAnalyticsOnAddToBagButtonClick,
                navigation,
                isRecentlyViewed,
                portalValue,
                page,
                headerLabel,
                component: GridContainer,
                isRecommendationHeight: true,
                isDynamicBadgeEnabled,
                handleClose,
                ...others,
              })}
              keyExtractor={(item, index) => onKeyExtractor(item, index, partNumber)}
              horizontal={false}
              numColumns={2}
            />
            <ButtonViewMore
              products={products}
              showProducts={showProducts}
              setShowProducts={setShowProducts}
            />
          </RecommendationParent>
        )}
        {showCarousel && !isSuggestedItem && !newRecommendationEnabled && (
          <CarouselContainer>
            {isPlaceCashReward ? (
              <>
                <FlatList
                  data={products}
                  onViewableItemsChanged={onViewRef.current}
                  viewabilityConfig={viewConfigRef.current}
                  pagingEnabled
                  renderItem={loadVariation(variation, {
                    priceOnly,
                    isPlcc,
                    onQuickViewOpenClick,
                    trackAnalyticsOnAddToBagButtonClick,
                    navigation,
                    isRecentlyViewed,
                    portalValue,
                    page,
                    headerLabel,
                    isPlaceCashReward,
                    isDynamicBadgeEnabled,
                    ...others,
                  })}
                  refreshing={false}
                  horizontal
                  height={180}
                  showsHorizontalScrollIndicator={false}
                  keyExtractor={(_, index) => index.toString()}
                />
                <PaginationDots numberOfDots={productLength} selectedIndex={currentActiveItem} />
              </>
            ) : (
              <FlatList
                data={products}
                renderItem={loadVariation(variation, {
                  priceOnly,
                  isPlcc,
                  onQuickViewOpenClick,
                  trackAnalyticsOnAddToBagButtonClick,
                  navigation,
                  isRecentlyViewed,
                  portalValue,
                  page,
                  headerLabel,
                  isDynamicBadgeEnabled,
                  ...others,
                })}
                refreshing={false}
                horizontal
                showsHorizontalScrollIndicator={false}
                keyExtractor={(_, index) => index.toString()}
              />
            )}
          </CarouselContainer>
        )}
        {!isHomePage && showButton && <ButtonView {...props} />}
      </Container>
    )
  );
};

// eslint-disable-next-line sonarjs/cognitive-complexity
const Recommendations = (props) => {
  const {
    variation,
    loadRecommendationsAdobe,
    loadRecommendationsMonetate,
    page,
    portalValue,
    partNumber,
    categoryName,
    reduxKey,
    excludedIds,
    MONETATE_ENABLED,
    cartItems,
    isBagLoading,
    isOutfitLoading,
    outfitItems,
    isUserLoggedIn,
    myPlaceNumber,
    breadCrumbs,
    currentProduct,
    userEmail,
    addedToBagData,
    isPlaceCashReward,
    pdpDataInformation,
    isPlcc,
    styleWithEligibility,
    outOfStock,
    productImageWidth,
    isCardTypeTiles,
  } = props;
  let variationArray = variation && variation.split(',');
  if (!variationArray) variationArray = [];
  const skuData =
    addedToBagData && addedToBagData.skuInfo ? addedToBagData.skuInfo : pdpDataInformation;
  let action = {
    pageType: page || 'homepageTest',
    ...(partNumber && {partNumber}),
    ...(categoryName && {categoryName}),
    excludedIds,
    MONETATE_ENABLED,
    cartItems,
    outfitItems,
    isUserLoggedIn,
    myPlaceNumber,
    isPlaceCashReward,
    productImageWidth,
    isCardTypeTiles,
    userEmail,
    skuData,
    isPlcc,
    styleWithEligibility,
    outOfStock,
  };
  if (currentProduct && currentProduct.seoToken) {
    action.seoPdp = currentProduct.seoToken;
  } else {
    action.seoPdp = partNumber;
  }
  if (breadCrumbs.length > 0) {
    const token = breadCrumbs[breadCrumbs.length - 1];
    const {pathSuffix} = token;
    action.seo = pathSuffix && pathSuffix.split('=')[1];
  }

  if (reduxKey === 'favorites_global_products') {
    action.reduxKey = reduxKey;
  } else if (!MONETATE_ENABLED) {
    action.reduxKey = reduxKey;
    action = {...action, ...(portalValue && {mbox: portalValue})};
  } else {
    action.reduxKey = action.pageType;
  }

  useEffect(() => {
    if (!(page === 'cart' && isBagLoading) && !(page === 'outfit' && isOutfitLoading)) {
      if (MONETATE_ENABLED === true) loadRecommendationsMonetate(action);
      else loadRecommendationsAdobe(action);
    }
  }, [styleWithEligibility, isBagLoading]);
  return (
    <PageContainer>
      {variationArray && variationArray.map((value) => renderRecommendationView(props, value))}
    </PageContainer>
  );
};

renderRecomStyleWith.propTypes = {
  addToBagEcomAction: func,
  addToBagError: string,
  closeATBModal: func,
  currency: string,
  navigation: shape({}).isRequired,
  pdpLabels: shape({}),
  plpLabels: shape({}),
  products: shape({}),
};

renderRecomStyleWith.defaultProps = {
  addToBagEcomAction: () => {},
  addToBagError: '',
  closeATBModal: () => {},
  currency: 'USD',
  pdpLabels: {},
  plpLabels: {},
  products: {},
};

Recommendations.propTypes = {
  addedToBagData: shape({}),
  breadCrumbs: arrayOf({}),
  cartItems: arrayOf({}),
  categoryName: string,
  currentProduct: shape({}),
  excludedIds: string,
  isBagLoading: bool,
  isCardTypeTiles: bool,
  isOutfitLoading: bool,
  isPlaceCashReward: bool,
  isPlcc: bool,
  isUserLoggedIn: bool,
  loadRecommendationsAdobe: func.isRequired,
  loadRecommendationsMonetate: func.isRequired,
  MONETATE_ENABLED: bool,
  myPlaceNumber: string,
  outfitItems: arrayOf({}),
  outOfStock: bool,
  page: string.isRequired,
  partNumber: string,
  pdpDataInformation: shape({}).isRequired,
  portalValue: string,
  productImageWidth: bool,
  reduxKey: string.isRequired,
  styleWithEligibility: bool,
  userEmail: string,
  variation: string.isRequired,
};

Recommendations.defaultProps = {
  addedToBagData: {},
  breadCrumbs: [],
  cartItems: [],
  categoryName: '',
  currentProduct: {},
  excludedIds: '',
  isBagLoading: false,
  isCardTypeTiles: false,
  isOutfitLoading: false,
  isPlaceCashReward: false,
  isPlcc: false,
  isUserLoggedIn: false,
  MONETATE_ENABLED: true,
  myPlaceNumber: '',
  outfitItems: [],
  outOfStock: false,
  partNumber: '',
  portalValue: '',
  productImageWidth: 0,
  styleWithEligibility: false,
  userEmail: '',
};

export default Recommendations;
