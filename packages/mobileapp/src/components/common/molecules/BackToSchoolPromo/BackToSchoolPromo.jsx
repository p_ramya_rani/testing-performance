import {ImageBackground} from 'react-native';
import React from 'react';
import PropTypes from 'prop-types';
import {cropImageUrl} from '@tcp/core/src/utils/utils.app';
import {getLabelValue} from '@tcp/core/src/utils';
import {
  ImageTouchableOpacity,
  Container,
  BackToSchoolBtn,
  StyledBodyCopy,
  StoreLocatorTouchable,
} from './BackToSchoolPromo.style';

const BackToSchoolPromoBTN = ({imageWidth}) => {
  const buttonText = 'ENTER NOW IN STORE';
  return (
    <BackToSchoolBtn imageWidth={imageWidth}>
      <StyledBodyCopy
        fontFamily="secondary"
        fontWeight="extrabold"
        fontSize="fs13"
        text={buttonText}
        color="gray.800"
        letterSpacing="1.3"
        btnText
      />
    </BackToSchoolBtn>
  );
};

const navigateToStore = (navigation, labels) => {
  const listLabels = labels.global.header;
  navigation.navigate({
    routeName: 'StoreLanding',
    params: {
      title: getLabelValue(listLabels, 'lbl_header_storeDefaultTitle').toUpperCase(),
    },
  });
};

const StoreLocatorText = ({bodyCopyMargin, navigation, labels}) => {
  const storeLocatorText = 'LOCATE A STORE';
  return (
    <StoreLocatorTouchable onPress={() => navigateToStore(navigation, labels)}>
      <StyledBodyCopy
        fontFamily="secondary"
        fontWeight="regular"
        fontSize="fs12"
        text={storeLocatorText}
        margin={bodyCopyMargin}
        storeLocator
      />
    </StoreLocatorTouchable>
  );
};

const BackToSchoolPromo = (props) => {
  const {onPress, imgUrl, imageWidth, imageHeight, alt, navigation, labels, holidayGiveAway} =
    props;
  const imageURI = {uri: cropImageUrl(imgUrl)};
  const styles = {
    width: imageWidth - 28,
    height: imageHeight,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 4,
  };
  const imageStyles = {
    borderRadius: 10,
  };
  const textOne = 'SCHOOL IS BACK! TO CELEBRATE ENTER OUR';
  const textTwo = '$1,000,000 GIVEAWAY!';
  const textThree = 'WE ARE GIVING AWAY $1,000';
  const textFour = 'EACH TO 1,000 WINNERS!*';
  const bodyCopyMargin = '0 0 -5px 0';
  return (
    <ImageTouchableOpacity onPress={onPress}>
      <ImageBackground
        source={imageURI}
        style={styles}
        accessibilityLabel={alt}
        imageStyle={imageStyles}>
        {holidayGiveAway ? null : (
          <Container>
            <StyledBodyCopy
              fontFamily="secondary"
              fontWeight="bold"
              fontSize="fs14"
              text={textOne}
              color="white"
              margin={bodyCopyMargin}
              textAlign="center"
            />
            <StyledBodyCopy
              fontFamily="quinary"
              fontWeight="regular"
              fontSize="fs30"
              text={textTwo}
              color="white"
              margin={bodyCopyMargin}
              giveAway
            />
            <StyledBodyCopy
              fontFamily="secondary"
              fontWeight="bold"
              fontSize="fs15"
              text={textThree}
              margin={bodyCopyMargin}
              newColor
            />
            <StyledBodyCopy
              fontFamily="secondary"
              fontWeight="bold"
              fontSize="fs15"
              text={textFour}
              margin={bodyCopyMargin}
              newColor
            />
            <BackToSchoolPromoBTN imageWidth={imageWidth} />
            <StoreLocatorText
              bodyCopyMargin={bodyCopyMargin}
              navigation={navigation}
              labels={labels}
            />
          </Container>
        )}
      </ImageBackground>
    </ImageTouchableOpacity>
  );
};

BackToSchoolPromo.propTypes = {
  onPress: PropTypes.func.isRequired,
  imgUrl: PropTypes.string.isRequired,
  imageWidth: PropTypes.string.isRequired,
  imageHeight: PropTypes.string.isRequired,
  alt: PropTypes.string,
  navigation: PropTypes.shape({}).isRequired,
  labels: PropTypes.shape({}).isRequired,
};

BackToSchoolPromo.defaultProps = {
  alt: '',
};

StoreLocatorText.propTypes = {
  bodyCopyMargin: PropTypes.string.isRequired,
  navigation: PropTypes.shape({}).isRequired,
  labels: PropTypes.shape({}).isRequired,
};

BackToSchoolPromoBTN.propTypes = {
  imageWidth: PropTypes.string.isRequired,
};

export default BackToSchoolPromo;
export {BackToSchoolPromo as BackToSchoolPromoVanilla};
