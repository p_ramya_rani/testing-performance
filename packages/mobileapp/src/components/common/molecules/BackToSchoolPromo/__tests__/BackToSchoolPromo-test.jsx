// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import {shallow} from 'enzyme';
import {BackToSchoolPromoVanilla} from '../BackToSchoolPromo';

describe('BackToSchoolPromo Component', () => {
  let component;
  const props = {
    onPress: jest.fn(),
    imgUrl: 'img/banner',
    imageWidth: '200px',
    imageHeight: '300px',
  };

  beforeEach(() => {
    component = shallow(<BackToSchoolPromoVanilla {...props} />);
  });

  it('BackToSchoolPromo should be defined', () => {
    expect(component).toBeDefined();
  });

  it('BackToSchoolPromo should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
});
