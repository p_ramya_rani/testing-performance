import React from 'react';
import {Dimensions} from 'react-native';
import PropTypes from 'prop-types';
import {BodyCopy} from '@tcp/core/src/components/common/atoms';
import {getLabelValue} from '@tcp/core/src/utils';
import {ErrorMessageContainer, MessageContainer} from './BackToSchoolPromo.style';

const BackToSchoolErrorMsg = ({labels}) => {
  const errorMessageHeader = getLabelValue(
    labels,
    'bts_error_message_header',
    'errorMessages',
    'global',
  );
  const errorMessageBody = getLabelValue(
    labels,
    'bts_error_message_body',
    'errorMessages',
    'global',
  );
  const {height} = Dimensions.get('window');
  return (
    <ErrorMessageContainer height={height}>
      <MessageContainer>
        <BodyCopy
          fontFamily="secondary"
          fontWeight="extrabold"
          fontSize="fs18"
          text={errorMessageHeader}
          color="black"
          textAlign="center"
        />
        <BodyCopy
          fontFamily="secondary"
          fontWeight="bold"
          fontSize="fs14"
          text={errorMessageBody}
          color="black"
          textAlign="center"
        />
      </MessageContainer>
    </ErrorMessageContainer>
  );
};

BackToSchoolErrorMsg.propTypes = {
  labels: PropTypes.shape({}).isRequired,
};

export default BackToSchoolErrorMsg;
