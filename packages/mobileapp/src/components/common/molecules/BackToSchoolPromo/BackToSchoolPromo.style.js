import styled from 'styled-components/native';
import {BodyCopy} from '@tcp/core/src/components/common/atoms';

export const ImageTouchableOpacity = styled.TouchableOpacity`
  align-items: center;
  width: 100%;
`;

export const Container = styled.View`
  justify-content: center;
  align-items: center;
  margin-top: -10px;
`;

export const BackToSchoolBtn = styled.View`
  width: ${props => props.imageWidth / 2};
  height: 42;
  background: ${props => props.theme.colors.WHITE};
  justify-content: center;
  align-items: center;
  margin-top: 10;
`;

const getAdditionalStyling = props => {
  const {newColor, theme, btnText, storeLocator, giveAway} = props;
  const newYellow = '#fef200';
  if (newColor) return `color: ${newYellow}`;
  if (btnText) return `color: ${theme.colors.TEXT.DARKERGRAY}; letter-spacing: 1.3;`;
  if (storeLocator) return `color: ${newYellow}; text-decoration: underline ${newYellow};`;
  if (giveAway) return `color: ${theme.colors.WHITE} margin: 4px 0px -2px 0px`;
  return `color: ${theme.colors.WHITE}`;
};

export const StyledBodyCopy = styled(BodyCopy)`
  ${getAdditionalStyling}
`;

export const StoreLocatorTouchable = styled.TouchableOpacity`
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.XS};
`;

export const ErrorMessageContainer = styled.View`
  justify-content: center;
  align-items: center;
  height: ${props => props.height};
`;

export const MessageContainer = styled.View`
  border: 2px solid ${props => props.theme.colorPalette.red[500]};
  padding: ${props => props.theme.spacing.ELEM_SPACING.LRG};
  width: 90%;
  margin-top: -75px;
`;

export default {
  ImageTouchableOpacity,
  Container,
  BackToSchoolBtn,
  StyledBodyCopy,
  StoreLocatorTouchable,
  ErrorMessageContainer,
  MessageContainer,
};
