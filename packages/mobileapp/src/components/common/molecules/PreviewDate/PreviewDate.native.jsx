// 9fbef606107a605d69c0edbcd8029e5d
import React, {PureComponent} from 'react';
import {View} from 'react-native';
import moment from 'moment';

import PropTypes from 'prop-types';
import DateTimePicker from '@react-native-community/datetimepicker';
import {isIOS, getLabelValue} from '@tcp/core/src/utils';

import {
  TextComponent,
  ButtonComponent,
  TextInputComponent,
  ButtonContainer,
} from './styles/PreviewDate.native.style';

export class PreviewDate extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      date: new Date(),
      selectedDate: '',
      mode: 'date',
    };
  }

  componentDidMount() {
    const {value} = this.props;
    this.setState({selectedDate: value});
  }

  onChangeHandler = (event, date) => {
    const {mode, selectedDate} = this.state;
    const {onPreviewChange} = this.props;
    let formattedDate;
    if (mode === 'time' && selectedDate) {
      formattedDate = `${selectedDate.split('T')[0]}T${moment(date).format('HH')}:00:00`;
    } else if (mode === 'date' && selectedDate) {
      formattedDate = `${moment(date).format('YYYY-MM-DD')}T${selectedDate.split('T')[1]}`;
    } else {
      formattedDate = `${moment(date).format('YYYY-MM-DDTHH:mm')}:00`;
    }
    this.setState({selectedDate: formattedDate, date, show: isIOS()});
    onPreviewChange(formattedDate);
  };

  showDatepicker = () => {
    this.setState({mode: 'date', show: true});
  };

  showTimepicker = () => {
    this.setState({mode: 'time', show: true});
  };

  render() {
    const {date, selectedDate, mode, show} = this.state;
    const {onSubmit, onRefresh, labels, value} = this.props;
    const pickerStyle = {
      flex: 1,
      marginLeft: '35%',
    };
    return (
      <View>
        <TextComponent>
          {getLabelValue(labels, 'lbl_preview_date', 'header', 'global')}
        </TextComponent>

        <TextInputComponent
          placeholder="YYYY-MM-DDTHH:MM:SS"
          value={selectedDate || value}
          editable={false}
        />
        {show && (
          <DateTimePicker
            testID="dateTimePicker"
            value={selectedDate ? date : new Date()}
            mode={mode}
            is24Hour
            display="default"
            style={pickerStyle}
            onChange={this.onChangeHandler}
          />
        )}
        <ButtonContainer>
          <ButtonComponent
            fill="BLUE"
            color="white"
            text={getLabelValue(labels, 'lbl_date_picker', 'header', 'global')}
            width="60%"
            onPress={this.showDatepicker}
          />
          <ButtonComponent
            fill="BLUE"
            color="white"
            text={getLabelValue(labels, 'lbl_time_picker', 'header', 'global')}
            width="60%"
            onPress={this.showTimepicker}
          />
          <ButtonComponent
            fill="BLUE"
            type="submit"
            color="white"
            text={getLabelValue(labels, 'lbl_submit', 'header', 'global')}
            width="40%"
            onPress={() => onSubmit(selectedDate)}
          />
          <ButtonComponent
            fill="BLUE"
            type="submit"
            color="white"
            text={getLabelValue(labels, 'lbl_refresh_data', 'header', 'global')}
            width="40%"
            onPress={() => onRefresh()}
          />
        </ButtonContainer>
      </View>
    );
  }
}

PreviewDate.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  onRefresh: PropTypes.func.isRequired,
  labels: PropTypes.shape({}).isRequired,
  value: PropTypes.string.isRequired,
  onPreviewChange: PropTypes.string.isRequired,
};

PreviewDate.defaultProps = {};

export default PreviewDate;

export {PreviewDate as PreviewDateVanilla};
