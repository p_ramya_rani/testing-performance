// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import {shallow} from 'enzyme';
import {PreviewDate} from '../PreviewDate.native';

describe('PreviewDate Component', () => {
  let component;
  const props = {
    onSubmit: jest.fn(),
    onRefresh: jest.fn(),
    previewDate: '2021-05-21T07:00:00',
  };

  beforeEach(() => {
    component = shallow(<PreviewDate {...props} />);
  });

  it('PreviewDate should be defined', () => {
    expect(component).toBeDefined();
  });

  it('PreviewDate should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
});
