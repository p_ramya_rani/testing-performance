// 9fbef606107a605d69c0edbcd8029e5d
import {useState, useEffect} from 'react';
import {Platform} from 'react-native';
import CookieManager from 'react-native-cookies';
import {encrypt} from 'react-native-simple-encryption';
import {
  getValueFromAsyncStorage,
  setValueInAsyncStorage,
  clearLoginCookiesFromAsyncStore,
} from '@tcp/core/src/utils/index.native';

// Set the login cookie
const setIOSLoginCookie = async () => {
  if (Platform.OS === 'ios') {
    await clearLoginCookiesFromAsyncStore();
    // save cookies in the async storage for ios to persist login
    const res = await CookieManager.getAll();
    Object.keys(res).forEach((key) => {
      if ((key.startsWith('WC_') && key !== 'WC_PERSISTENT') || key === 'tcp_isregistered') {
        setValueInAsyncStorage(key, encrypt(key, res[key].value));
      }
    });
  }
};

const useShowMainModal = (
  overviewLabels,
  isUserLoggedIn,
  onModalActionComplete,
  showPushNotificationOnHome,
) => {
  const [showMainModal, setShowMainModal] = useState(false);
  const [modalActionComplete, setModalActionComplete] = useState(false);
  const [userOnboardingScreenVisited, setUserOnboardingScreenVisited] = useState();

  /* Because asyncStorage takes time. We need to check for this ASAP and then wait
     for labels to load. As soon as labels load we need to show onboarding modal immediately.
  */
  useEffect(() => {
    getValueFromAsyncStorage('isUserOnBoardingScreenVisited').then(
      (isUserOnBoardingScreenVisited) => {
        if (isUserOnBoardingScreenVisited === null || isUserOnBoardingScreenVisited === 'false') {
          setShowMainModal(true);
          setValueInAsyncStorage('isUserOnBoardingScreenVisited', 'true');
        }
        if (isUserOnBoardingScreenVisited === 'true') {
          setModalActionComplete(true);
        }
        setUserOnboardingScreenVisited(isUserOnBoardingScreenVisited);
      },
    );
  }, [userOnboardingScreenVisited]);

  useEffect(() => {
    if (isUserLoggedIn) {
      setShowMainModal(false);
      setModalActionComplete(true);
      setIOSLoginCookie();
    }
  }, [isUserLoggedIn]);

  useEffect(() => {
    if (!showMainModal && modalActionComplete) {
      onModalActionComplete(showPushNotificationOnHome);
    }
  }, [showMainModal, modalActionComplete]);

  return {
    showMainModal,
    setShowMainModal,
    modalActionComplete,
    setModalActionComplete,
  };
};

export default useShowMainModal;
