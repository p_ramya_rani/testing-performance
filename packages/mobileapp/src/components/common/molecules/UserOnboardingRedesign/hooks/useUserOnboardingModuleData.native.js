// 9fbef606107a605d69c0edbcd8029e5d
import {useState, useEffect, useCallback} from 'react';
import UserOnboardingModuleAbstractor from '@tcp/core/src/services/abstractors/common/userOnboardingModule';
import logger from '@tcp/core/src/utils/loggerInstance';
import {isTCP, isIOS} from '@tcp/core/src/utils/index.native';
import {getLabelValue} from '@tcp/core/src/utils/utils';

const pushNotificationLottie = require('../assets/lottiePushNotification.json');

const useUserOnboardingModuleData = (moduleId, showMainModal, setShowMainModal, labels) => {
  const [moduleData, setModuleData] = useState({});
  const [lottieData, setLottieData] = useState(null);
  const [tcp, setTcp] = useState(false);
  const [removePNS, setRemovePNS] = useState(false);

  useEffect(() => {
    if (moduleId) {
      UserOnboardingModuleAbstractor.getData(moduleId)
        .then((data) => {
          setModuleData(data.composites);
        })
        .catch((err) => {
          setShowMainModal(false);
          logger.error('module fetch error:', err);
        });
    }
  }, [moduleId, showMainModal]);

  useEffect(() => {
    const {mediaList, textCarousel} = moduleData;
    const lottieJsons = [];
    if (mediaList?.length > 0) {
      mediaList.forEach((media, index) => {
        if (media.url.includes('theplace')) {
          fetch(media.url)
            .then((res) => res.json())
            .then((data) => {
              lottieJsons.splice(index, 0, data);
              if (isIOS() && !removePNS) {
                lottieJsons.splice(1, 0, pushNotificationLottie);
              }
            });
        } else {
          lottieJsons.push(media.url);
        }
      });
      setLottieData(lottieJsons);
    }
    if (textCarousel && textCarousel.length > 1 && isIOS() && !removePNS) {
      const heading = getLabelValue(labels, 'lbl_notificationPrompt_heading', 'notification').split(
        '...',
      )[0];
      const descPNS = getLabelValue(labels, 'lbl_notificationPrompt_first', 'notification');
      const content = {
        headLine: [{text: heading}],
        textItems: [{text: descPNS}],
      };
      textCarousel.splice(1, 0, content);
      moduleData.textCarousel = textCarousel;
      setModuleData(moduleData);
    }
  }, [moduleData]);

  useEffect(() => {
    setTcp(isTCP());
  }, [isTCP]);

  const removePushNotificationScreen = useCallback(() => {
    const {textCarousel} = moduleData;
    textCarousel.splice(1, 1);
    moduleData.textCarousel = textCarousel;
    lottieData.splice(1, 1);
    setRemovePNS(true);
    setLottieData(lottieData);
    setModuleData(moduleData);
  }, [lottieData]);

  return {moduleData, lottieData, tcp, removePushNotificationScreen, removePNS};
};

export default useUserOnboardingModuleData;
