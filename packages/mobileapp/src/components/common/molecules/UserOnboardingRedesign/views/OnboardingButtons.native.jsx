import React from 'react';
import PropTypes from 'prop-types';
import {getLabelValue} from '@tcp/core/src/utils/utils';
import {setValueInAsyncStorage} from '@tcp/core/src/utils';
import {isIOS} from '@tcp/core/src/utils/index.native';
import CustomRedesignButton from '@tcp/core/src/components/common/atoms/ButtonRedesign';
import GuestLoginOverview from '@tcp/core/src/components/features/account/common/molecule/GuestLoginModule';
import {ActionsWrapper, CustomWrapper} from '../styles/UserOnboardingScreen.style.native';
import constants from '../UserOnboardingScreen.config';

const OnboardingButtons = ({
  labels,
  navigation,
  isUserLoggedIn,
  loginAnalyticsData,
  joinAnalyticsData,
  carouselRef,
  currentIndex,
  totalItems,
  trackClickAction,
  userOnboardingLabels,
  openSettings,
  globalLabels,
  showPushNotification,
  removePushNotificationScreen,
  removePNS,
  animationCallback,
  setShowPushNotificationOnHome,
}) => {
  const pushNotificationCheck =
    currentIndex === 1 && !showPushNotification && isIOS() && !removePNS;
  const {MAYBE_LATER} = constants;
  const manageSlide = () => {
    if (pushNotificationCheck) {
      setValueInAsyncStorage(MAYBE_LATER, 'true').then(() => {
        setValueInAsyncStorage('firstTimeNotificationPrompt', 'true');
        removePushNotificationScreen();
        setShowPushNotificationOnHome(false);
      });
    } else {
      carouselRef.snapToNext();
    }
  };
  const loggedInWrapperStyle = {
    marginBottom: 116,
    width: '100%',
    alignItems: 'center',
    flexWrap: 'nowrap',
    flexDirection: 'column',
  };

  if (currentIndex === totalItems - 1) {
    return (
      <ActionsWrapper>
        <GuestLoginOverview
          fromOnBoarding
          hideLogoutText
          loggedInWrapperStyle={loggedInWrapperStyle}
          labels={labels}
          navigation={navigation}
          isUserLoggedIn={isUserLoggedIn}
          isNewRedesignOnboarding
          loginAnalyticsData={loginAnalyticsData}
          joinAnalyticsData={joinAnalyticsData}
          trackClickAction={trackClickAction}
          animationCallback={animationCallback}
        />
      </ActionsWrapper>
    );
  }
  return (
    <CustomWrapper pushNotificationCheck={pushNotificationCheck}>
      {pushNotificationCheck && (
        <CustomRedesignButton
          module="global"
          text={getLabelValue(globalLabels, 'lbl_notificationPrompt_button', 'notification')}
          fill="BLUE"
          borderRadius="16px"
          fontFamily="secondary"
          fontWeight="bold"
          fontSize="fs16"
          showShadow
          width="70%"
          height="46px"
          paddings="6px"
          onPress={openSettings}
        />
      )}
      <CustomRedesignButton
        module="global"
        fill={pushNotificationCheck ? 'WHITE' : 'BLUE'}
        id="next"
        type="button"
        data-locator=""
        width="70%"
        fontSize="fs16"
        fontWeight="bold"
        fontFamily="secondary"
        paddings="10px"
        textPadding="0px 5px"
        borderRadius="16px"
        margin="20px 0px 0px"
        showShadow
        buttonVariation="variable-width"
        height="46px"
        wrapContent={false}
        text={
          pushNotificationCheck
            ? getLabelValue(globalLabels, 'lbl_notificationPrompt_later', 'notification')
            : userOnboardingLabels.nextButtonLabel
        }
        onPress={manageSlide}
      />
    </CustomWrapper>
  );
};

OnboardingButtons.defaultProps = {
  labels: {},
  navigation: {},
  isUserLoggedIn: false,
  trackClickAction: () => {},
  loginAnalyticsData: {},
  joinAnalyticsData: {},
  carouselRef: {},
  currentIndex: 0,
  totalItems: 0,
  userOnboardingLabels: {},
  animationCallback: () => {},
};

OnboardingButtons.propTypes = {
  labels: PropTypes.shape({}),
  navigation: PropTypes.shape({}),
  isUserLoggedIn: PropTypes.bool,
  trackClickAction: PropTypes.func,
  loginAnalyticsData: PropTypes.shape({}),
  joinAnalyticsData: PropTypes.shape({}),
  carouselRef: PropTypes.shape({}),
  currentIndex: PropTypes.number,
  totalItems: PropTypes.number,
  userOnboardingLabels: PropTypes.shape({}),
  animationCallback: PropTypes.func,
};

export default OnboardingButtons;
