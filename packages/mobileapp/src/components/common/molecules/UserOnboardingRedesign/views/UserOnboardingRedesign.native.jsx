// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable global-require */
import React, {useState, useCallback, useRef} from 'react';
import PropTypes from 'prop-types';
import {View, Modal, Dimensions, Linking, Animated} from 'react-native';
import {Host} from 'react-native-portalize';
import LottieView from 'lottie-react-native';
import {checkNotifications, RESULTS, requestNotifications} from 'react-native-permissions';
import {BodyCopy, Anchor} from '@tcp/core/src/components/common/atoms';
import Carousel from '@tcp/core/src/components/common/molecules/Carousel';
import {getScreenWidth} from '@tcp/core/src/utils/index.native';
import names from '@tcp/core/src/constants/eventsName.constants';
import {setValueInAsyncStorage, isAndroid, isIOS} from '@tcp/core/src/utils';
import PushNotificationModal from '@tcp/core/src/components/common/molecules/PushNotificationModal';
import createThemeColorPalette from '@tcp/core/styles/themes/createThemeColorPalette';
import {isTier3Device} from '@tcp/web/src/utils/device-tiering';
import useShowMainModal from '../hooks/useShowMainModal.native';
import useUserOnboardingModuleData from '../hooks/useUserOnboardingModuleData.native';
import CarouselButtons from './OnboardingButtons.native';
import {
  Container,
  TextCarouselWrapper,
  SingleTextView,
  TextCarouselView,
  CarouselWrapper,
  ContainerView,
  CTG,
  UnderLine,
} from '../styles/UserOnboardingScreen.style.native';
import ClickTracker from '../../../atoms/ClickTracker';
import VideoOnboardingScreen from './VideoOnboardingScreen.native';
import constants from '../UserOnboardingScreen.config';
/**
 * Module height and width.
 * Height is fixed for mobile : TCP & Gymb
 * Width can vary as per device width.
 */
const HERO_CAROUSEL_MODULE_HEIGHT = 117;
const CAROUSEL_MODULE_WIDTH = getScreenWidth();
const {height: windowHeight} = Dimensions.get('window');

const BagLottie = require('../assets/Bag.json');
const StarLottie = require('../assets/star.json');
const RewardLottie = require('../assets/Rewards.json');

const isIOSDevice = isIOS();

const getLottiePath = (key) => {
  switch (key) {
    case 'Bag':
      return BagLottie;
    case 'Star':
      return StarLottie;
    case 'Rewards':
      return RewardLottie;
    default:
      return '';
  }
};

/* Carousel slide render function for Promo Text Carousel */
const renderPromoTextCarousel = (carouselPromoText, lottieData, currentIndex) => {
  const {item, index} = carouselPromoText;
  const lottieHeight = 0.5 * windowHeight;
  const {
    headLine: [headLine],
    textItems,
  } = item;

  const lottieStyleObj = {
    height: lottieHeight,
    marginTop: 10,
    alignItems: 'center',
  };
  const lottieUrl = lottieData[index];
  let lottieJson = '';
  if (lottieUrl && lottieUrl.includes && lottieUrl.includes('/onboarding')) {
    const key = lottieUrl.split('/onboarding')?.[1].split('Image')[0];
    lottieJson = getLottiePath(key);
  } else {
    lottieJson = lottieUrl;
  }

  const loadLottie = currentIndex === index;

  return (
    <TextCarouselWrapper>
      {lottieJson && loadLottie ? (
        <LottieView source={lottieJson} autoPlay style={lottieStyleObj} loop={false} />
      ) : (
        <View style={lottieStyleObj} />
      )}
      <BodyCopy
        text={headLine.text}
        fontSize="fs30"
        fontFamily="primary"
        fontWeight="medium"
        textAlign="center"
        color="white"
        margin="0 10px 15px 10px"
      />
      {textItems.map((description) => {
        return (
          <BodyCopy
            text={description.text}
            fontSize="fs16"
            fontFamily="secondary"
            fontWeight="regular"
            textAlign="center"
            color="white"
            margin="0 40px 0 40px"
          />
        );
      })}
    </TextCarouselWrapper>
  );
};

const TopLink = ({
  isLastSlide,
  shoppingAnalyticsData,
  userOnboardingLabels,
  onPressHandlerForGuest,
  guestLinkStyle,
  skipAnalyticsData,
  onPressHandlerForSkip,
}) => {
  return isLastSlide ? (
    <CTG>
      <ClickTracker
        as={Anchor}
        clickData={shoppingAnalyticsData}
        name={names.screenNames.shopping}
        module="global"
        underline
        anchorVariation="secondary"
        fontSizeVariation="medium"
        text={userOnboardingLabels.continueasGuestLabel}
        onPress={onPressHandlerForGuest}
        overrideStyle={guestLinkStyle}
        hitSlop={{top: 20, right: 20, bottom: 20, left: 20}}
      />
      <UnderLine />
    </CTG>
  ) : (
    <CTG>
      <ClickTracker
        as={Anchor}
        clickData={skipAnalyticsData}
        name={names.screenNames.skip_click_onboarding}
        module="global"
        underline
        anchorVariation="secondary"
        fontSizeVariation="medium"
        text={userOnboardingLabels.skipLabel}
        overrideStyle={guestLinkStyle}
        onPress={onPressHandlerForSkip}
        hitSlop={{top: 20, right: 20, bottom: 20, left: 20}}
      />
      <UnderLine />
    </CTG>
  );
};

const openSettingsFunctionality = (
  maybeLater,
  setShowPushNotification,
  removePushNotificationScreen,
  setShowPushNotificationOnHome,
) => {
  setValueInAsyncStorage('firstTimeNotificationPrompt', 'true');
  setShowPushNotificationOnHome(false);
  checkNotifications().then(({status}) => {
    if (status === RESULTS.BLOCKED || isAndroid()) {
      Linking.openSettings();
    } else if (isIOS()) {
      requestNotifications(['alert', 'sound', 'badge', 'lockScreen', 'notificationCenter']).then(
        () => {
          setValueInAsyncStorage(maybeLater, 'false').then(() => {
            setShowPushNotification(true);
            removePushNotificationScreen();
          });
        },
      );
    }
  });
};

const UserOnboardingScreen = React.memo((props) => {
  const {
    navigation,
    onModalActionComplete,
    userOnboardingLabels,
    overviewLabels,
    isUserLoggedIn,
    userOnboardingModuleId,
    trackClickAction,
    globalLabels,
  } = props;
  const deviceEligibleForVideo = !isTier3Device();

  const [currentIndex, setCurrentIndex] = useState(0);
  const [carouselRef, setCarouselRef] = useState({});
  const [showVideo, setShowVideo] = useState(deviceEligibleForVideo);
  const [showPushNotification, setShowPushNotification] = useState(false);
  const [showPushNotificationOnHome, setShowPushNotificationOnHome] = useState(true);
  const [endValue, setEndValue] = useState(1);

  const animated = useRef(new Animated.Value(0)).current;

  const setRef = useCallback((ref) => {
    setCarouselRef(ref);
  }, []);
  const {showMainModal, setShowMainModal, setModalActionComplete} = useShowMainModal(
    overviewLabels,
    isUserLoggedIn,
    onModalActionComplete,
    showPushNotificationOnHome,
  );
  const {moduleData, lottieData, tcp, removePushNotificationScreen, removePNS} =
    useUserOnboardingModuleData(
      userOnboardingModuleId,
      showMainModal,
      setShowMainModal,
      globalLabels,
    );
  const page = 'home page';
  const commonVariables = {
    pageName: page,
    pageType: page,
    pageSection: page,
    pageSubSection: page,
    pageSubSubSection: page,
    pageShortName: page,
  };
  const shoppingAnalyticsData = {
    ...commonVariables,
    pageNavigationText: names.screenNames.shopping,
    customEvents: 'event156',
  };
  const loginAnalyticsData = {
    ...commonVariables,
    pageNavigationText: names.screenNames.login,
    customEvents: ['event155', 'event126'],
  };
  const skipAnalyticsData = {
    customEvents: ['event155'],
  };
  const joinAnalyticsData = {
    ...commonVariables,
    pageNavigationText: names.screenNames.join,
    customEvents: ['event154', 'event125'],
  };
  const {textCarousel} = moduleData || [];
  const totalItems = textCarousel?.length;

  const onPressHandlerForGuest = () => {
    setShowMainModal(false);
    setModalActionComplete(true);
  };

  const onPressHandlerForSkip = () => {
    carouselRef.snapToItem(totalItems - 1, true);
  };

  const guestLinkStyle = {color: 'white'};
  const colorPalette = createThemeColorPalette();

  const showCarousel = moduleData && lottieData && textCarousel && textCarousel.length > 0;

  const videoCallback = useCallback(() => {
    setShowVideo(false);
  }, [setShowVideo]);

  const isLastSlide = currentIndex === totalItems - 1;
  const {MAYBE_LATER} = constants;
  const openSettings = () => {
    openSettingsFunctionality(
      MAYBE_LATER,
      setShowPushNotification,
      removePushNotificationScreen,
      setShowPushNotificationOnHome,
    );
  };

  const startAnimation = (end) => {
    Animated.spring(animated, {
      toValue: end,
      useNativeDriver: true,
    }).start(() => (end === 0 ? setEndValue(1) : setEndValue(0)));
  };
  const setModalStyleCallback = useCallback(() => {
    startAnimation(endValue);
  }, [startAnimation, endValue]);

  const animatedStyle = {
    borderRadius: animated.interpolate({inputRange: [0, 1], outputRange: [0, 12]}),
    transform: [
      {
        scale: animated.interpolate({inputRange: [0, 1], outputRange: [1, 0.92]}),
      },
    ],
    opacity: animated.interpolate({inputRange: [0, 1], outputRange: [1, 0.75]}),
  };

  return (
    <Modal visible={showMainModal}>
      {showVideo && <VideoOnboardingScreen videoCallback={videoCallback} />}
      {!showVideo && showCarousel && (
        <Host>
          <Animated.View style={animatedStyle}>
            <ContainerView tcp={tcp}>
              <TopLink
                isLastSlide={isLastSlide}
                shoppingAnalyticsData={shoppingAnalyticsData}
                userOnboardingLabels={userOnboardingLabels}
                onPressHandlerForGuest={onPressHandlerForGuest}
                guestLinkStyle={guestLinkStyle}
                skipAnalyticsData={skipAnalyticsData}
                onPressHandlerForSkip={onPressHandlerForSkip}
              />
              <Container>
                <TextCarouselView>
                  <CarouselWrapper>
                    {textCarousel.length > 1 ? (
                      <Carousel
                        height={HERO_CAROUSEL_MODULE_HEIGHT}
                        data={textCarousel}
                        width={CAROUSEL_MODULE_WIDTH}
                        renderItem={(data) =>
                          renderPromoTextCarousel(data, lottieData, currentIndex)
                        }
                        options={{
                          autoplay: false,
                        }}
                        showDots
                        loop={false}
                        setRef={setRef}
                        onSnapToItem={(index) => {
                          setCurrentIndex(index);
                        }}
                        enableMomentum={isIOSDevice}
                        enableSnap
                        paginationProps={{
                          dotStyle: {
                            width: 6,
                            height: 6,
                            backgroundColor: colorPalette.gray[700],
                          },
                          inactiveDotStyle: {
                            width: 10,
                            height: 10,
                            backgroundColor: colorPalette.white,
                          },
                          containerStyle: {
                            paddingVertical: 0,
                            paddingTop: 30,
                          },
                        }}
                      />
                    ) : (
                      <SingleTextView>
                        {renderPromoTextCarousel({item: textCarousel[0]}, lottieData, currentIndex)}
                      </SingleTextView>
                    )}
                  </CarouselWrapper>
                </TextCarouselView>
                <CarouselButtons
                  labels={overviewLabels}
                  navigation={navigation}
                  isUserLoggedIn={isUserLoggedIn}
                  loginAnalyticsData={loginAnalyticsData}
                  joinAnalyticsData={joinAnalyticsData}
                  carouselRef={carouselRef}
                  currentIndex={currentIndex}
                  totalItems={totalItems}
                  trackClickAction={trackClickAction}
                  userOnboardingLabels={userOnboardingLabels}
                  animationCallback={setModalStyleCallback}
                  openSettings={openSettings}
                  globalLabels={globalLabels}
                  showPushNotification={showPushNotification}
                  removePushNotificationScreen={removePushNotificationScreen}
                  removePNS={removePNS}
                  setShowPushNotificationOnHome={setShowPushNotificationOnHome}
                />
                {showPushNotification && <PushNotificationModal />}
              </Container>
            </ContainerView>
          </Animated.View>
        </Host>
      )}
    </Modal>
  );
});

UserOnboardingScreen.defaultProps = {
  userOnboardingLabels: {},
  overviewLabels: null,
  isUserLoggedIn: false,
  onModalActionComplete: () => {},
  userOnboardingModuleId: '',
  trackClickAction: () => {},
};

UserOnboardingScreen.propTypes = {
  userOnboardingLabels: PropTypes.shape({}),
  navigation: PropTypes.shape({}).isRequired,
  overviewLabels: PropTypes.arrayOf(PropTypes.object),
  isUserLoggedIn: PropTypes.bool,
  onModalActionComplete: PropTypes.func,
  userOnboardingModuleId: PropTypes.string,
  trackClickAction: PropTypes.func,
};

export default UserOnboardingScreen;
export {UserOnboardingScreen as UserOnboardingScreenVanilla};
