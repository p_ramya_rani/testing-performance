import React from 'react';
import {Dimensions} from 'react-native';
import Video from 'react-native-video';
import {isTCP, isGymboree} from '@tcp/core/src/utils/utils';
import logger from '@tcp/core/src/utils/loggerInstance';
import {VideoContainer} from '../styles/UserOnboardingScreen.style.native';

const tcpOnboardingVideo = require('../../../../../assets/videos/Onboarding_Screen_TCP.mp4');

const {width: screenWidth, height: screenHeight} = Dimensions.get('screen');

const backgroundVideo = {
  height: screenHeight,
  width: screenWidth,
  margin: 0,
  padding: 0,
};

const VideoScreen = React.memo(({videoCallback}) => {
  const playerRef = React.useRef(null);
  const setVideoUrl = () => {
    let videoUrl;
    if (isTCP()) {
      videoUrl = tcpOnboardingVideo;
    } else if (isGymboree()) {
      videoUrl = '';
    }
    return videoUrl;
  };

  const onVideoError = () => logger.error('error in video');

  return (
    <VideoContainer height={screenHeight} width={screenWidth}>
      <Video
        source={setVideoUrl()}
        style={backgroundVideo}
        ref={playerRef}
        resizeMode="cover"
        onEnd={videoCallback}
        onError={onVideoError}
        useCache
      />
    </VideoContainer>
  );
});

export default VideoScreen;
