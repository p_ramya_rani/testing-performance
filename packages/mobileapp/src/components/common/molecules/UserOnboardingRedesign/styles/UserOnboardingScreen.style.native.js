// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';
import {isIOS} from '@tcp/core/src/utils';

export const CTG = styled.View`
  align-self: flex-end;
  display: flex;
  padding-right: 12px;
  padding-top: 6px;
  z-index: 1;
`;

export const ContainerView = styled.View`
  display: flex;
  align-items: center;
  justify-content: space-between;
  background-color: ${(props) =>
    props.tcp ? props.theme.colors.PRIMARY.LIGHTBLUE : props.theme.colors.PRIMARY.GYMORANGE};
  height: 100%;
  padding-top: ${isIOS() ? '40px' : '10px'};
`;

export const Container = styled.View`
  margin-bottom: ${(props) => props.theme.spacing.LAYOUT_SPACING.XXL};
  height: 100%;
  width: 100%;
  align-items: center;
  justify-content: center;
  margin-top: ${(props) => props.theme.spacing.LAYOUT_SPACING.SM};
`;

export const TextCarouselWrapper = styled.View`
  align-items: center;
  justify-content: center;
`;

export const SingleTextView = styled.View`
  padding-bottom: 70px;
`;

export const LoaderContainer = styled.View`
  align-items: center;
  background-color: ${(props) => props.theme.colors.MODAL_OVERLAY};
`;

export const TextCarouselView = styled.View`
  background-color: ${(props) => props.theme.colors.PRIMARY.LIGHTBLUE};
  display: flex;
  justify-content: space-between;
  width: 100%;
  bottom: 0;
`;

export const CarouselWrapper = styled.View`
  display: flex;
  justify-content: center;
`;

export const ActionsWrapper = styled.View`
  margin-top: 5px;
  justify-content: flex-start;
  align-items: center;
  width: 100%;
`;

export const CustomWrapper = styled.View`
  margin-top: 10px;
  justify-content: flex-start;
  align-items: center;
  width: 100%;
  margin-bottom: 180px;
`;

export const VideoContainer = styled.View`
  height: ${(props) => props.height};
  width: ${(props) => props.width};
  background-color: ${(props) => props.theme.colors.WHITE};
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const UnderLine = styled.View`
  background-color: ${(props) => props.theme.colors.WHITE};
  height: 1;
`;

export default {
  Container,
  TextCarouselWrapper,
  LoaderContainer,
  SingleTextView,
  TextCarouselView,
  CarouselWrapper,
  ActionsWrapper,
  VideoContainer,
};
