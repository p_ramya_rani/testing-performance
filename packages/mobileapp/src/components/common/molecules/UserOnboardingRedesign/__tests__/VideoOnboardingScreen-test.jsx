// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import {shallow} from 'enzyme';
import VideoOnboardingScreen from '../views/VideoOnboardingScreen.native';

jest.mock('@tcp/core/src/utils/utils', () => {
  return {
    isTCP: jest.fn(() => {
      return true;
    }),
  };
});
describe('VideoOnboardingScreen Component', () => {
  const props = {
    videoCallback: {},
  };
  const focus = jest.fn();

  it('VideoOnboardingScreen should render correctly', () => {
    jest.spyOn(React, 'useRef').mockReturnValueOnce({current: {focus}});
    const component = shallow(<VideoOnboardingScreen {...props} />);
    expect(component).toMatchSnapshot();
  });
});
