// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import {shallow} from 'enzyme';
import OnboardingButtons from '../views/OnboardingButtons.native';

describe('OnboardingButtons Component', () => {
  let component;
  const props = {
    labels: {},
    navigation: {},
    isUserLoggedIn: false,
    loginAnalyticsData: {},
    joinAnalyticsData: {},
    carouselRef: {},
    currentIndex: 1,
    totalItems: 5,
    trackClickAction: () => {},
    userOnboardingLabels: {},
    animationCallback: () => {},
    openSettings: () => {},
    globalLabels: {},
    showPushNotification: false,
    removePushNotificationScreen: () => {},
    removePNS: false,
    setShowPushNotificationOnHome: false,
  };

  beforeEach(() => {
    component = shallow(<OnboardingButtons {...props} />);
  });

  it('OnboardingButtons should be defined', () => {
    expect(component).toBeDefined();
  });

  it('OnboardingButtons should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
});

describe('OnboardingButtons Component', () => {
  let component;
  const props = {
    labels: {},
    navigation: {},
    isUserLoggedIn: false,
    loginAnalyticsData: {},
    joinAnalyticsData: {},
    carouselRef: {},
    currentIndex: 5,
    totalItems: 5,
    trackClickAction: () => {},
    userOnboardingLabels: {},
    animationCallback: () => {},
    openSettings: () => {},
    globalLabels: {},
    showPushNotification: false,
    removePushNotificationScreen: () => {},
    removePNS: false,
    setShowPushNotificationOnHome: false,
  };

  beforeEach(() => {
    component = shallow(<OnboardingButtons {...props} />);
  });

  it('OnboardingButtons should be defined', () => {
    expect(component).toBeDefined();
  });
});
