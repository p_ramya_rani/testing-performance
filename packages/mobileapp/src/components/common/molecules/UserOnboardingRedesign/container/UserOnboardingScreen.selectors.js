// 9fbef606107a605d69c0edbcd8029e5d
import {createSelector} from 'reselect';

export const getLabels = (state) => state.Labels;

export const getGlobalLabels = createSelector(getLabels, (labels) => labels.global || {});

export const getAccountLabels = createSelector(getLabels, (labels) => labels.account || {});

export const getAccountOverviewLabels = createSelector(
  getAccountLabels,
  (account) => account.accountOverview,
);

export const getUserOnboardingLabels = createSelector(
  getGlobalLabels,
  (globalLabels) => globalLabels.UserOnboardingApp || {},
);

export const getUserOnboardingModuleId = createSelector(
  getUserOnboardingLabels,
  (userOnboardingLabels) => {
    return userOnboardingLabels.referred && userOnboardingLabels.referred[0].contentId;
  },
);

export default {
  getLabels,
  getAccountLabels,
  getAccountOverviewLabels,
  getUserOnboardingLabels,
  getGlobalLabels,
};
