// 9fbef606107a605d69c0edbcd8029e5d
import {connect} from 'react-redux';
import {getUserLoggedInState} from '@tcp/core/src/components/features/account/User/container/User.selectors';
import {trackClick, setClickAnalyticsData} from '@tcp/core/src/analytics/actions';
import UserOnboardingScreen from '../views';
import {
  getAccountOverviewLabels,
  getUserOnboardingLabels,
  getUserOnboardingModuleId,
  getGlobalLabels,
} from './UserOnboardingScreen.selectors';

export const mapStateToProps = (state) => {
  return {
    overviewLabels: getAccountOverviewLabels(state),
    isUserLoggedIn: getUserLoggedInState(state) || false,
    userOnboardingLabels: getUserOnboardingLabels(state),
    userOnboardingModuleId: getUserOnboardingModuleId(state),
    globalLabels: getGlobalLabels(state),
  };
};

export const mapDispatchToProps = (dispatch) => {
  return {
    trackClickAction: (data) => {
      dispatch(trackClick(data));
    },
    setClickAnalyticsDataAction: (data) => {
      dispatch(setClickAnalyticsData(data));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserOnboardingScreen);
