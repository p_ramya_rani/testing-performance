// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';
import Themes from '../../../features/shopByProfile/ShopByProfile.theme';
import {
  fontFamilyTofinoBold,
  fontFamilyTofinoSemiBold,
  fontFamilyTofinoRegular,
  fontFamilyNunitoRegular,
} from '../../../features/shopByProfile/ShopByProfile.styles';

const setBackground = (props) => {
  if (props.theme.isGymboree) {
    return `
    background-color: ${props.theme.colorPalette.orange[900]};
    `;
  }
  return `
  background-color: ${props.theme.colorPalette.blue[800]};
  `;
};

const getAdditionalStyle = (props) => {
  const {
    theme,
    showSearch,
    isTransparent,
    isAndroidDevice,
    statusBarHeight,
    showSearchBarInline,
    newPdpDesignFlag,
    isOutfitDetail,
  } = props;
  const headerHeight = showSearch
    ? theme.spacing.LAYOUT_SPACING.LRGS
    : theme.spacing.LAYOUT_SPACING.LRG;
  const transparentStyles =
    isTransparent && !isOutfitDetail
      ? {
          position: 'absolute',
          top: newPdpDesignFlag && isAndroidDevice ? 0 : statusBarHeight,
        }
      : {};

  return {
    ...(isAndroidDevice && {
      height: showSearchBarInline ? 80 : headerHeight,
      ...transparentStyles,
    }),
  };
};

const sbpAdditionalStylesAndroid = (props) => {
  const {isAndroidDevice, isSBP} = props;
  if (isAndroidDevice && isSBP) {
    return `
      height: 85;
      padding-bottom: 25;
      padding-top: 25;
    `;
  }
  return ``;
};

const additionalPadding = (isAndroidDevice, showSearchBarInline) => {
  if (isAndroidDevice && showSearchBarInline) {
    return `
      padding-top: 15px
    `;
  }
  return ``;
};

const borderLine = (isOutfitDetail, showSearchBarInline, theme, disableHeaderBottomWidth) => {
  if (showSearchBarInline || isOutfitDetail || disableHeaderBottomWidth) {
    return ``;
  }
  return `border-bottom-color: ${theme.colorPalette.gray[500]};
  border-bottom-width: 1;
  color: black;`;
};

const getHeaderBackgroundColor = (
  showSearchBarInline,
  isOutfitDetail,
  theme,
  isNewReDesignProductTile,
) => {
  if (showSearchBarInline || (isOutfitDetail && !isNewReDesignProductTile)) {
    return theme.colorPalette.gray[300];
  }
  if (isNewReDesignProductTile) return theme.colors.PRIMARY.PALEGRAY;
  return theme.colorPalette.white;
};

const cartItemsWidth = (cartItems) => {
  let width = '';
  switch (cartItems.toString().length) {
    case 2:
      width = '25px';
      break;
    case 3:
      width = '30px';
      break;
    default:
      width = '20px';
  }
  return width;
};

const getAndroidSpecificBackground = (
  isTransparent,
  isScrolled,
  isOutfitDetail,
  backGroundColor,
  isSBP,
  sbpTheme,
  theme,
) => {
  let bgColor = backGroundColor;
  if (isTransparent && isScrolled && !isOutfitDetail) {
    bgColor = theme.colors.TRANSPARENT;
  }
  if (isSBP && !isOutfitDetail) {
    bgColor = Themes[sbpTheme || 0].color;
  }
  return bgColor;
};

const getSafeAreaStyle = (props) => {
  const {
    theme,
    isTransparent,
    isScrolled,
    isAndroidDevice,
    isSBP,
    sbpTheme,
    showSearchBarInline,
    isNewReDesignProductTile,
    isOutfitDetail,
    newPdpDesignFlag,
    disableHeaderBottomWidth,
  } = props;
  let backGroundColor = isTransparent
    ? 'transparent'
    : getHeaderBackgroundColor(
        showSearchBarInline,
        isOutfitDetail,
        theme,
        isNewReDesignProductTile,
      );

  const opacity = 1;

  if (isAndroidDevice) {
    backGroundColor = getAndroidSpecificBackground(
      isTransparent,
      isScrolled,
      isOutfitDetail,
      backGroundColor,
      isSBP,
      sbpTheme,
      theme,
    );
  }
  if (isScrolled && isOutfitDetail) {
    backGroundColor = 'transparent';
  }

  return `
  background: ${backGroundColor};
  opacity: ${opacity};
  justify-content: center;
  ${
    newPdpDesignFlag
      ? `
    width: 100%;
    z-index: 101;
  `
      : ''
  }
  ${additionalPadding(isAndroidDevice, showSearchBarInline)}
  ${
    isTransparent
      ? ''
      : borderLine(isOutfitDetail, showSearchBarInline, theme, disableHeaderBottomWidth)
  }
  `;
};

export const SafeAreaViewStyle = styled.View`
  ${getSafeAreaStyle}
  ${getAdditionalStyle}
  ${sbpAdditionalStylesAndroid}
`;

const getBackgroundColor = (props) => {
  const {showSearchBarInline, theme} = props;
  const backGroundColor = showSearchBarInline
    ? theme.colorPalette.gray[300]
    : theme.colorPalette.white;
  return `
  ${showSearchBarInline ? `background-color: ${backGroundColor}` : ''}
  `;
};

export const Container = styled.View`
  height: ${(props) => props.containerHeight};
  align-items: center;
  justify-content: center;
  ${(props) => (props.isNewReDesignProductTile ? `top: 4%;` : '')}
  ${getBackgroundColor}
`;

export const HeaderContainer = styled.View`
  ${(props) =>
    props.newPdpDesignFlag
      ? `
    width: 100%;
    justify-content: space-between;
  `
      : `
    justify-content: center;
  `}
  flex-direction: row;
  align-items: center;
  padding-top: ${(props) => (props.isSBP ? props.theme.spacing.LAYOUT_SPACING.XXS : '0px')};
  padding-bottom: ${(props) => (props.isSBP ? props.theme.spacing.LAYOUT_SPACING.XXS : '0px')};
`;

export const CartCountContainer = styled.View`
  ${setBackground}
  width: ${(props) => cartItemsWidth(props.cartVal ? props.cartVal : 0)};
  height: 22px;
  border-radius: 10px;
  justify-content: center;
  align-items: center;
  position: absolute;
  right: 0;
  bottom: 0;
  border: 2px solid white;
`;

export const CartIconView = styled.Image`
  width: 25px;
  height: 26px;
  margin-right: ${(props) =>
    props.cartVal.toString().length === 1 ? 0 : props.theme.spacing.ELEM_SPACING.XXS};
`;
export const ArrowBackIcon = styled.Image`
  width: 10px;
  height: 18px;
  padding-left: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
`;
export const Touchable = styled.TouchableOpacity`
  flex-direction: row;
  height: 36;
  ${(props) =>
    props.newPdpDesignFlag ? `margin-top: ${props.theme.spacing.ELEM_SPACING.XS};` : ''}
`;

export const LeftSection = styled.View`
  justify-content: center;
  padding-left: ${(props) => (props.newDesignFlag ? '0' : props.theme.spacing.ELEM_SPACING.XS)};
  width: 30%;
  height: 100%;
`;

export const MiddleSection = styled.View`
  justify-content: center;
  align-items: center;
  width: 40%;
  height: 100%;
  flex-direction: ${(props) =>
    props.isSBP && props.routeName !== 'EditChildProfile' ? 'row' : 'column'};
  ${(props) =>
    props.showSearchBarInline
      ? `
    width: 85%;
    margin-top: 18px;
    padding-left: 10px;
  `
      : ''}
`;

export const HeaderView = styled.View`
  justify-content: center;
  align-items: center;
  flex-direction: row;
`;

export const CartIconSection = styled.View`
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  width: ${(props) =>
    props.searchIconVisible || props.showSearchBarInline || props.newPdpDesignFlag ? '15%' : '30%'};
  height: 100%;
  padding-right: ${(props) =>
    props.isFromHomePage ? '20px' : props.theme.spacing.ELEM_SPACING.XS};
`;

export const SearchIconSection = styled.View`
  ${(props) =>
    !props.newPdpDesignFlag
      ? `
    flex-direction: row;
    justify-content: flex-end;
    margin-top: ${props.theme.spacing.ELEM_SPACING.SM};
    `
      : `
    justify-content: center;
    `}
  width: 15%;
  align-items: center;
  height: 100%;
`;

const outfitPageTitleText = (props) => {
  return {
    color: props.isSBP ? props.theme.colors.TEXT.DARK : props.theme.colorPalette.gray[900],
    fontFamily: props.isSBP ? fontFamilyTofinoSemiBold : props.theme.typography.fonts.secondary,
    fontSize: props.theme.typography.fontSizes.fs16,
    fontWeight: props.theme.typography.fontWeights.bold,
    lineHeight: props.isSBP ? 18 : 16.5,
    textTransform:
      props.isSBP && props.routeName === 'EditChildProfile' ? 'uppercase' : 'capitalize',
    ...(props.overrideStyle ? props.overrideStyle : {}),
    height: '16px',
    width: '100%',
    textAlign: 'center',
  };
};

const getAdditionalTitleTextStyles = (props) => {
  const {theme, isNewReDesignProductTile} = props;
  if (isNewReDesignProductTile) {
    return `
    font-family: ${theme.fonts.nunitoBold}
    color: ${theme.colors.PRIMARY.DARK}
    letter-spacing: .25
    `;
  }
  return ``;
};

export const TitleText = styled.Text`
  ${(props) =>
    props.isOutfitDetail
      ? outfitPageTitleText(props)
      : {
          color: props.isSBP ? props.theme.colors.TEXT.DARK : props.theme.colorPalette.gray[900],
          fontFamily: props.isSBP ? fontFamilyTofinoSemiBold : props.theme.typography.fonts.primary,
          fontSize: props.theme.typography.fontSizes.fs12,
          fontWeight: props.theme.typography.fontWeights.semibold,
          lineHeight: props.isSBP ? 18 : 14.5,
          textTransform:
            (props.isSBP && props.routeName === 'EditChildProfile') ||
            props.isNewReDesignProductTile
              ? 'uppercase'
              : 'capitalize',
          ...(props.overrideStyle ? props.overrideStyle : {}),
        }}
  ${getAdditionalTitleTextStyles}
`;

export const TitleTextCount = styled.Text`
  color: ${(props) => props.theme.colorPalette.gray[900]};
  font-family: ${fontFamilyNunitoRegular};
  font-size: ${(props) => props.theme.typography.fontSizes.fs12};
  font-weight: ${(props) => props.theme.fonts.fontWeight.normal};
  line-height: 12;
`;

export const SBPTitleText = styled.Text`
  color: #1a1a1a;
  font-family: ${(props) => (props.isProfileName ? fontFamilyTofinoBold : fontFamilyTofinoRegular)};
  font-size: 16;
  font-weight: ${(props) => (props.isProfileName ? 'bold' : 'normal')};
  line-height: 18;
  text-transform: capitalize;
  margin-right: 5;
`;

export const AddProfileIcon = styled.Image`
  height: 27;
  margin-right: 10%;
  width: 27;
`;

export const SearchIconImage = styled.Image`
  height: 24;
  width: 24;
`;
