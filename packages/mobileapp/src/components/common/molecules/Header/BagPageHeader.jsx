// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import {PropTypes} from 'prop-types';
import {connect} from 'react-redux';
import {getLocator, isGymboree} from '@tcp/core/src/utils';
import {TouchableOpacity} from 'react-native';
import {toastMessageInfo} from '@tcp/core/src/components/common/atoms/Toast/container/Toast.actions';
import BagPageSelectors from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.selectors';
import {setCouponMergeError} from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.actions.util';
import {isIOS, isDeviceHasNotch} from '@tcp/core/src/utils/utils.app';
import {
  SafeAreaViewStyle,
  BrandIcon,
  CloseIcon,
  BrandIconSection,
  CloseIconTouchable,
  CloseContainer,
  BagPageContainer,
  ArrowBackIcon,
} from './Header.style';

import {BagPageBackContainer} from './BagPageHeader.style';
import {INTERNET_OFF} from './Header.constants';

/**
 * This component creates Mobile Header
 * @param {*} props Props passed from Header screen
 */

const closeIcon = require('../../../../assets/images/close.png');
const backIcon = require('../../../../assets/images/carrot-large-left.png');
const tcpIcon = require('../../../../assets/images/tcp/tcpPeekABoo.png');
const gymIcon = require('../../../../assets/images/gymboree/gymboreePeekABoo.png');

/**
 * This component creates Mobile Header.
 * 1. To Manage the store locator
 * 2. To Navigate the cart page & show cart quantity
 * 3. To show the welcome text for guest user
 *     and shoe the name fro register user
 */
class BagPageHeader extends React.PureComponent {
  componentDidUpdate(prevProps) {
    const {
      screenProps: {
        network: {isConnected},
      },
      toastMessage,
    } = this.props;
    const {
      screenProps: {
        network: {isConnected: PrevIsConnected},
      },
    } = prevProps;
    if (isConnected !== PrevIsConnected && !isConnected) {
      toastMessage(INTERNET_OFF);
    }
  }

  closeIconAction = () => {
    const {navigation, screenProps} = this.props;
    try {
      const {setCouponMergeErrorAction} = this.props;
      const navRoutes = navigation.dangerouslyGetParent().state.routes;

      if (screenProps && navRoutes.length < 1) {
        screenProps.checkFirst('HomeStack');
      }
      navigation.goBack();
      setCouponMergeErrorAction(false);
    } catch (err) {
      navigation.goBack();
    }
  };

  render() {
    const {
      showBrandIcon,
      showCloseButton,
      showGobackIcon,
      isPayPalWebViewEnable,
      bagLoading,
      disableBagLoading,
    } = this.props;

    const isBagLoading = disableBagLoading ? false : bagLoading;
    const isDeviceIPhoneHasNotch = isIOS() && !!isDeviceHasNotch();
    return (
      <SafeAreaViewStyle isDeviceIPhoneHasNotch={isDeviceIPhoneHasNotch}>
        {!isPayPalWebViewEnable && (
          <BagPageContainer data-locator={getLocator('global_bagpageheaderpanel')}>
            {!isBagLoading && showGobackIcon && (
              <BagPageBackContainer>
                <TouchableOpacity
                  accessible
                  onPress={this.closeIconAction}
                  accessibilityRole="button"
                  accessibilityLabel="back button"
                  data-locator={getLocator('global_bagpagebackbutton')}
                  hitSlop={{top: 30, right: 30, bottom: 30, left: 30}}>
                  <ArrowBackIcon source={backIcon} />
                </TouchableOpacity>
              </BagPageBackContainer>
            )}

            {showBrandIcon && (
              <BrandIconSection>
                <BrandIcon
                  source={isGymboree() ? gymIcon : tcpIcon}
                  data-locator={getLocator('global_bagpageheaderpanelbrandicon')}
                  accessibilityRole="image"
                />
              </BrandIconSection>
            )}

            {!isBagLoading && showCloseButton && (
              <CloseContainer>
                <CloseIconTouchable
                  onPress={this.closeIconAction}
                  hitSlop={{top: 35, right: 35, bottom: 35, left: 35}}>
                  <CloseIcon
                    source={closeIcon}
                    data-locator={getLocator('global_bagpageheaderpanelcloseicon')}
                    accessibilityRole="button"
                  />
                </CloseIconTouchable>
              </CloseContainer>
            )}
          </BagPageContainer>
        )}
      </SafeAreaViewStyle>
    );
  }
}

export const mapStateToProps = (state) => {
  return {
    labels: state.Labels.global && state.Labels.global.header,
    isPayPalWebViewEnable:
      state.CartPageReducer.getIn(['uiFlags', 'isPayPalWebViewEnable']) || false,
    bagLoading: BagPageSelectors.isBagLoading(state),
  };
};

BagPageHeader.propTypes = {
  showBrandIcon: PropTypes.bool,
  showCloseButton: PropTypes.bool,
  showGobackIcon: PropTypes.bool,
  isPayPalWebViewEnable: PropTypes.shape({}),
  screenProps: PropTypes.shape({
    network: PropTypes.shape({isConnected: PropTypes.func}),
    checkFirst: PropTypes.shape({}),
  }),
  toastMessage: PropTypes.func,
  setCouponMergeErrorAction: PropTypes.func.isRequired,
  bagLoading: PropTypes.bool,
  disableBagLoading: PropTypes.bool,
  navigation: PropTypes.objectOf(PropTypes.shape({})).isRequired,
};

BagPageHeader.defaultProps = {
  showBrandIcon: true,
  showCloseButton: true,
  showGobackIcon: false,
  isPayPalWebViewEnable: false,
  screenProps: {},
  toastMessage: () => {},
  bagLoading: true,
  disableBagLoading: false,
};

export const mapDispatchToProps = (dispatch) => {
  return {
    toastMessage: (payload) => {
      dispatch(toastMessageInfo(payload));
    },
    setCouponMergeErrorAction: (payload) => {
      dispatch(setCouponMergeError(payload));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BagPageHeader);
export {BagPageHeader as BagPageHeaderVanilla};
