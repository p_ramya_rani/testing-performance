/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import {connect} from 'react-redux';
import {TouchableOpacity, StatusBar} from 'react-native';
import {arrayOf, bool, func, number, shape, string} from 'prop-types';
import get from 'lodash/get';
import {isAndroid, getValueFromAsyncStorage} from '@tcp/core/src/utils/utils.app';
import {BodyCopy} from '@tcp/core/src/components/common/atoms';
import CustomIcon from '@tcp/core/src/components/common/atoms/Icon';
import {getLabelValue} from '@tcp/core/src/utils/utils';
import {
  ICON_NAME,
  ICON_FONT_CLASS,
} from '@tcp/core/src/components/common/atoms/Icon/Icon.constants';
import {SearchBar} from '@tcp/core/src/components/common/molecules';
import SearchProduct from '@tcp/core/src/components/common/organisms/SearchProduct';
import InitialPropsHOC from '@tcp/core/src/components/common/hoc/InitialPropsHOC/InitialPropsHOC.native';
import {getLocator, navigateToNestedRoute} from '@tcp/core/src/utils';
import ToastContainer from '@tcp/core/src/components/common/atoms/Toast/container/Toast.container.native';
import {toastMessageInfo} from '@tcp/core/src/components/common/atoms/Toast/container/Toast.actions';
import {isOpenAddedToBag as getIsOpenAddedToBag} from '@tcp/core/src/components/features/CnC/AddedToBag/container/AddedToBag.selectors';
import BAGPAGE_SELECTORS from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.selectors';
import {getIsNewReDesignEnabled} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import {
  updateCartCount,
  updateCartManually,
  updateSBPEvent,
} from '@tcp/core/src/components/common/organisms/Header/container/Header.actions';
import {readCookie} from '@tcp/core/src/utils/cookie.util';
import names from '@tcp/core/src/constants/eventsName.constants';
import {CUSTOMER_HELP_PAGE_TEMPLATE} from '@tcp/core/src/components/features/account/CustomerHelp/CustomerHelp.constants';
import {getLabels} from '@tcp/core/src/components/features/account/Account/container/Account.selectors';
import {selectOrder} from '@tcp/core/src/components/features/account/CustomerHelp/container/CustomerHelp.actions';
// eslint-disable-next-line import/no-named-as-default
import HapticFeedback from '@tcp/core/src/components/common/atoms/hapticFeedback/container/HapticFeedback.container.native';
import {
  Container,
  HeaderContainer,
  CartIconView,
  Touchable,
  LeftSection,
  MiddleSection,
  CartIconSection,
  TitleText,
  CartCountContainer,
  SafeAreaViewStyle,
  SearchIconSection,
  SBPTitleText,
  AddProfileIcon,
  SearchIconImage,
  HeaderView,
  TitleTextCount,
} from './HeaderNew.style';
import {MAX_NUMBER_PROFILES} from '../../../features/shopByProfile/ShopByProfile.constants';
import {isDeviceIPhone12} from '../../../features/shopByProfile/PLP/helper';
import {StyledIconTouchableOpacity} from './Header.style';
import {ArrowBackIconPLP} from '../../../features/content/Navigation/molecules/NavMenuLevel2/NavMenuLevel2.style';
import {INTERNET_OFF} from './Header.constants';
import ClickTracker from '../../atoms/ClickTracker';
import {getSBPLabels, getSBPProfiles} from '../../../features/shopByProfile/ShopByProfile.selector';

const cartIcon = require('../../../../assets/images/empty-bag.png');
const addProfileIcon = require('../../../../assets/images/shopByProfile/elements/icon-addprofile_2020-10-08/icon-addprofile.png');
const Icon = require('../../../../assets/images/carrot-large-left.png');
const blackArrowIcon = require('../../../../assets/images/carrot-left-black.png');
const blackCloseIcon = require('../../../../assets/images/shopByProfile/elements/close3x.png');
const redesignedIcon = require('../../../../assets/images/icons-standard-caret-large-left.png');
const redesignedSearchIcon = require('../../../../assets/images/search.png');

const CART_ITEM_COUNTER = 'cartItemsCount';
/**
 * This component creates Mobile Header.
 * 1. To Manage the store locator
 * 2. To Navigate the cart page & show cart quantity
 * 3. To show the welcome text for guest user and show the name fro register user
 */
class HeaderNew extends React.PureComponent {
  static propTypes = {
    title: string,
    showSearch: bool,
    showCross: bool,
  };

  static defaultProps = {
    title: '',
    showSearch: false,
    showCross: false,
  };

  /**
   * To manage the state of icons on the basis of expand & collapse .
   */
  constructor(props) {
    super(props);
    this.state = {
      showSearchModal: false,
    };
    this.isAndroidDevice = isAndroid();
    this.statusBarHeight = this.isAndroidDevice ? StatusBar.currentHeight || 24 : 0;
    this.showHeaderTitle = this.showHeaderTitle.bind(this);
  }

  componentDidMount() {
    this.getInitialProps();
  }

  componentDidUpdate(prevProps) {
    const {isUpdateCartCount, updateCartManuallyAction, isOpenAddedToBag, totalItems, cartVal} =
      this.props;
    if (
      isUpdateCartCount !== prevProps.isUpdateCartCount ||
      (totalItems !== -1 && totalItems !== cartVal) ||
      (isOpenAddedToBag !== prevProps.isOpenAddedToBag && isOpenAddedToBag === true)
    ) {
      this.getInitialProps();
      updateCartManuallyAction(false);
    }
    this.noInterNetHandle(prevProps);
  }

  getInitialProps() {
    const {updateCartCountAction, totalItems} = this.props;
    if (totalItems) {
      updateCartCountAction(totalItems);
    } else {
      const cartValuePromise = readCookie(CART_ITEM_COUNTER);
      cartValuePromise.then((res) => {
        updateCartCountAction(parseInt(res || 0, 10));
      });
    }
  }

  /**
   * @function openSearchProductPage
   * opens search product modal
   *
   * @memberof HeaderNew
   */
  openSearchProductPage = () => {
    this.setState({showSearchModal: true});
  };

  /**
   * @function closeSearchProductPage
   * closes search product modal
   *
   * @memberof HeaderNew
   */
  closeSearchProductPage = () => {
    this.setState({showSearchModal: false});
  };

  /**
   * @function goToSearchResultsPage
   * navigates to search results page
   *
   * @memberof HeaderNew
   */
  goToSearchResultsPage = (searchText, typeahead) => {
    this.closeSearchProductPage();
    const {navigation} = this.props;
    navigateToNestedRoute(navigation, 'HomeStack', 'SearchDetail', {
      title: searchText,
      isForceUpdate: true,
      searchedType: typeahead ? 'typeahead:keyword' : 'keyword',
    });
  };

  handleCSHNavigation = () => {
    const {navigation, orderLabels, selectOrderAction} = this.props;
    const orderId = navigation.getParam('orderId');
    const fromPage = navigation.getParam('fromPage');
    if (fromPage) {
      if (fromPage === 'TrackOrder') {
        selectOrderAction({navigation});
      } else {
        const router = {
          query: {
            orderId,
          },
        };
        navigation.navigate(fromPage, {
          title: `${getLabelValue(orderLabels, 'lbl_orderDetail_heading', 'orders')} #${orderId}`,
          router,
        });
      }
    }
  };

  onBack = async (isSBP, profile, routeName) => {
    const {navigation, screenProps} = this.props;
    const goBackRoute = get(navigation, 'state.params.backTo', false);
    const isReset = get(navigation, 'state.params.reset', false);
    const route = navigation.state.routeName;
    if (routeName === 'ProductDetailSBP' || route === 'PLP') {
      navigation.goBack(null);
    } else if (route === 'EditChildProfile') {
      const plpRefresh = await getValueFromAsyncStorage('plpRefresh');
      navigation.navigate(
        'PLP',
        Object.assign({}, profile, {
          categoriesLoading: plpRefresh,
          refresh: true,
        }),
      );
    } else if (isSBP && routeName !== 'OutfitDetail') {
      navigation.navigate(
        'PLP',
        Object.assign({}, profile, {refresh: false, categoriesLoading: false}),
      );
    } else if (isReset) {
      navigation.pop();
    } else if (goBackRoute) {
      navigation.navigate(goBackRoute);
    } else if (route === CUSTOMER_HELP_PAGE_TEMPLATE.TELL_US_MORE) {
      this.handleCSHNavigation();
    } else {
      if (routeName === 'SearchDetail') {
        screenProps.checkFirst('HomeStack');
      }
      navigation.goBack(null);
    }
  };

  showHeaderTitle = (
    title,
    isSBP,
    routeName,
    overrideStyle,
    isOutfitDetail,
    isNewReDesignProductTile,
    totalProductCount,
  ) => {
    const {sbpLabels, profileName, showTitleHeaderPLP} = this.props;
    const headerTitle = this.headerTitle(
      sbpLabels,
      profileName,
      isSBP,
      routeName,
      showTitleHeaderPLP,
      title,
    );
    const text = headerTitle && headerTitle.split(' ');
    if (isSBP && text && routeName !== 'EditChildProfile') {
      return (
        <React.Fragment>
          <SBPTitleText
            numberOfLines={1}
            accessibilityRole="text"
            accessibilityLabel={text[0]}
            isProfileName>
            {text[0]}
          </SBPTitleText>
          <SBPTitleText numberOfLines={1} accessibilityRole="text" accessibilityLabel={text[1]}>
            {text[1]}
          </SBPTitleText>
        </React.Fragment>
      );
    }

    if (totalProductCount) {
      return (
        <HeaderView>
          <TitleText
            numberOfLines={1}
            accessibilityRole="text"
            accessibilityLabel={title}
            isSBP={isSBP}
            routeName={routeName}
            overrideStyle={overrideStyle}
            isNewReDesignProductTile={isNewReDesignProductTile}
            isOutfitDetail={isOutfitDetail}>
            {headerTitle}
          </TitleText>
          <TitleTextCount
            totalProductCount={totalProductCount}
            numberOfLines={1}
            accessibilityRole="text"
            accessibilityLabel={title}>
            {` (${totalProductCount})`}
          </TitleTextCount>
        </HeaderView>
      );
    }

    return (
      <TitleText
        numberOfLines={1}
        accessibilityRole="text"
        accessibilityLabel={title}
        isSBP={isSBP}
        routeName={routeName}
        overrideStyle={overrideStyle}
        isNewReDesignProductTile={isNewReDesignProductTile}
        isOutfitDetail={isOutfitDetail}>
        {headerTitle}
      </TitleText>
    );
  };

  showCreateProfileIconSBP = (routeName) => {
    const {isSBP, profiles, navigation, isSBPUpdateEvent, updateSBPEventAction} = this.props;
    const numberChildProfiles = profiles.length;
    let events = ['event23'];
    if (isSBPUpdateEvent) {
      events = [...events, 'event164'];
    }
    const shouldNavigateCreate =
      isSBP && numberChildProfiles < MAX_NUMBER_PROFILES && routeName !== 'EditChildProfile';
    return shouldNavigateCreate ? (
      <ClickTracker
        as={TouchableOpacity}
        name={names.screenNames.sbp_create_profile}
        module="browse"
        clickData={{customEvents: events}}
        onPress={() => {
          navigateToNestedRoute(navigation, 'ShopByProfile', 'NameInput', {
            fromPLP: true,
          });
          setTimeout(() => {
            updateSBPEventAction(false);
          }, 2000);
        }}
        accessibilityRole="button">
        <AddProfileIcon source={addProfileIcon} />
      </ClickTracker>
    ) : null;
  };

  headerTitle = (sbpLabels, profileName, isSBP, routeName, showTitleHeaderPLP, title) => {
    const sbpTitleCMSLabel =
      sbpLabels && sbpLabels.lbl_sbp_plp_title
        ? sbpLabels.lbl_sbp_plp_title.replace('[Name]', `${profileName}'s`)
        : `${profileName}'s PLACE`;

    return isSBP &&
      sbpTitleCMSLabel &&
      (showTitleHeaderPLP || routeName !== 'PLP') &&
      profileName &&
      routeName !== 'EditChildProfile'
      ? sbpTitleCMSLabel
      : title;
  };

  bagPageNavigation = (screenRouteName) => {
    const {navigation, isSBP} = this.props;
    if (isSBP && (screenRouteName === 'ProductDetailSBP' || screenRouteName === 'OutfitDetail'))
      navigation.popToTop();
    navigation.navigate('BagPage');
  };

  noInterNetHandle(prevProps) {
    const {
      screenProps: {
        network: {isConnected},
      },
      toastMessage,
    } = this.props;
    const {
      screenProps: {
        network: {isConnected: PrevIsConnected},
      },
    } = prevProps;
    if (isConnected !== PrevIsConnected && !isConnected) {
      toastMessage(INTERNET_OFF);
    }
  }

  /* eslint-disable complexity, sonarjs/cognitive-complexity */
  render() {
    const {
      title,
      showSearch,
      showSearchIcon,
      cartVal,
      slpLabels,
      navigation,
      isTransparent,
      isScrolled,
      hideBack,
      isSBP,
      profile,
      overrideStyle,
      showSearchBarInline,
      showCross,
      isNewReDesignProductTile,
      isNewReDesignEnabled,
      disableHeaderBottomWidth,
      totalProductCount,
    } = this.props;
    // when the search bar is showed to the user then search icon will not be visible
    const isSearchIconShow = showSearch ? !showSearch : showSearchIcon;
    const {showSearchModal} = this.state;
    const {routeName} = navigation.state;
    const isiPhone12 = isDeviceIPhone12();
    const routeIndex = navigation.state.index;
    const screenRouteName = routeIndex && navigation.state.routes[routeIndex].routeName;
    const isOutfitDetail = screenRouteName === 'OutfitDetail';
    let containerHeight = '100%';
    if (isiPhone12) {
      containerHeight = '68%';
      if (isSBP) {
        containerHeight = '92%';
      }
    }
    const newPdpDesignFlag = showCross && isNewReDesignEnabled;
    const iconTrans = newPdpDesignFlag ? blackCloseIcon : blackArrowIcon;
    const newIcon = isNewReDesignProductTile ? redesignedIcon : Icon;

    const cartValueNew = cartVal === -1 ? 0 : cartVal; // safety check for DRP-21822
    return (
      <SafeAreaViewStyle
        disableHeaderBottomWidth={disableHeaderBottomWidth}
        showSearch={showSearch}
        isTransparent={isTransparent}
        isScrolled={isScrolled}
        statusBarHeight={this.statusBarHeight}
        isAndroidDevice={this.isAndroidDevice}
        isSBP={isSBP}
        sbpTheme={profile && profile.theme}
        showSearchBarInline={showSearchBarInline}
        pointerEvents="box-none"
        isNewReDesignProductTile={isNewReDesignProductTile}
        isOutfitDetail={isOutfitDetail}
        newPdpDesignFlag={newPdpDesignFlag}>
        <ToastContainer />
        <HapticFeedback />
        <Container
          containerHeight={containerHeight}
          showSearchBarInline={showSearchBarInline}
          pointerEvents="box-none">
          <HeaderContainer
            isSBP={isSBP}
            data-locator={getLocator('global_headerpanel')}
            newPdpDesignFlag={newPdpDesignFlag}
            pointerEvents="box-none">
            {showSearchBarInline ? null : (
              <LeftSection newDesignFlag={isNewReDesignEnabled}>
                {hideBack ? null : (
                  <StyledIconTouchableOpacity
                    accessible
                    onPress={() => this.onBack(isSBP, profile, screenRouteName)}
                    accessibilityRole="button"
                    accessibilityLabel="back button"
                    isArrowIcon
                    isTransparent={isTransparent && !isScrolled}
                    newPdpDesignFlag={newPdpDesignFlag}
                    isBackIcon>
                    <ArrowBackIconPLP
                      source={isTransparent && !isOutfitDetail ? iconTrans : newIcon}
                      isSBP={isSBP}
                      newPdpDesignFlag={newPdpDesignFlag}
                      isNewReDesignProductTile={isNewReDesignProductTile}
                    />
                  </StyledIconTouchableOpacity>
                )}
              </LeftSection>
            )}
            {!newPdpDesignFlag && (
              <MiddleSection
                isSBP={isSBP}
                showSearchBarInline={showSearchBarInline}
                isOutfitDetail={isOutfitDetail}>
                {showSearchBarInline ? (
                  <SearchBar
                    openSearchProductPage={this.openSearchProductPage}
                    navigation={navigation}
                    labels={slpLabels}
                    showSearchBarInline={showSearchBarInline}
                  />
                ) : (
                  this.showHeaderTitle(
                    title,
                    isSBP,
                    routeName,
                    overrideStyle,
                    isOutfitDetail,
                    isNewReDesignProductTile,
                    totalProductCount,
                  )
                )}
              </MiddleSection>
            )}
            {isSearchIconShow && !isSBP && (
              <SearchIconSection newPdpDesignFlag={newPdpDesignFlag}>
                <Touchable
                  newPdpDesignFlag={newPdpDesignFlag}
                  accessibilityRole="button"
                  onPress={this.openSearchProductPage}>
                  {isNewReDesignProductTile ? (
                    <SearchIconImage source={redesignedSearchIcon} />
                  ) : (
                    <CustomIcon
                      iconFontName={ICON_FONT_CLASS.Icomoon}
                      name={ICON_NAME.search}
                      size="fs25"
                      color="gray.900"
                    />
                  )}
                </Touchable>
              </SearchIconSection>
            )}
            <CartIconSection
              searchIconVisible={isSearchIconShow}
              newPdpDesignFlag={newPdpDesignFlag}
              showSearchBarInline={showSearchBarInline}>
              {this.showCreateProfileIconSBP(routeName)}
              <StyledIconTouchableOpacity
                accessibilityRole="button"
                onPress={() => this.bagPageNavigation(screenRouteName)}
                isTransparent={isTransparent && !isScrolled}
                newPdpDesignFlag={newPdpDesignFlag}>
                <CartIconView
                  source={cartIcon}
                  data-locator={getLocator('global_headerpanelbagicon')}
                  cartVal={cartValueNew}
                />
                <CartCountContainer cartVal={cartValueNew}>
                  <BodyCopy
                    text={cartValueNew}
                    color="white"
                    fontSize="fs10"
                    data-locator={getLocator('global_headerpanelbagitemtext')}
                    accessibilityText="Mini bag with count"
                    fontWeight="extrabold"
                  />
                </CartCountContainer>
              </StyledIconTouchableOpacity>
            </CartIconSection>
          </HeaderContainer>
          {showSearch && !showSearchBarInline && (
            <SearchBar
              openSearchProductPage={this.openSearchProductPage}
              navigation={navigation}
              labels={slpLabels}
            />
          )}
          {showSearchModal && (
            <SearchProduct
              closeSearchModal={this.closeSearchProductPage}
              goToSearchResultsPage={this.goToSearchResultsPage}
              navigation={navigation}
            />
          )}
        </Container>
      </SafeAreaViewStyle>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    cartVal: state.Header && state.Header.cartItemCount,
    isUpdateCartCount: state.Header && state.Header.updateCartCount,
    slpLabels: state.Labels.SLPs && state.Labels.SLPs.SLPs_Sub,
    isOpenAddedToBag: getIsOpenAddedToBag(state),
    totalItems: BAGPAGE_SELECTORS.getTotalItemsCheck(state),
    sbpLabels: getSBPLabels(state),
    profiles: getSBPProfiles(state),
    orderLabels: getLabels(state),
    isNewReDesignProductTile: getIsNewReDesignEnabled(state),
    isNewReDesignEnabled: getIsNewReDesignEnabled(state),
    isSBPUpdateEvent: state.Header && state.Header.isUpdateSBPEvent,
  };
};

export const mapDispatchToProps = (dispatch) => {
  return {
    updateCartCountAction: (payload) => {
      dispatch(updateCartCount(payload));
    },
    updateCartManuallyAction: (payload) => {
      dispatch(updateCartManually(payload));
    },
    toastMessage: (payload) => {
      dispatch(toastMessageInfo(payload));
    },
    selectOrderAction: (payload) => {
      dispatch(selectOrder(payload));
    },
    updateSBPEventAction: (payload) => {
      dispatch(updateSBPEvent(payload));
    },
  };
};

HeaderNew.propTypes = {
  cartVal: number,
  hideBack: bool,
  isNewReDesignEnabled: bool,
  isNewReDesignProductTile: bool,
  isOpenAddedToBag: bool,
  isSBP: bool,
  isScrolled: bool,
  isTransparent: bool,
  isUpdateCartCount: bool,
  navigation: shape({
    getParam: func,
    goBack: func,
    navigate: func,
    pop: func,
    popToTop: func,
    state: shape({
      index: number,
      routeName: string,
      routes: arrayOf({}),
    }),
  }),
  orderLabels: shape({}),
  overrideStyle: shape({}),
  profile: shape({
    theme: string,
  }),
  profileName: string,
  profiles: shape([]),
  sbpLabels: shape({}),
  screenProps: shape({network: shape({isConnected: func})}),
  selectOrderAction: func,
  showSearchBarInline: bool,
  showSearchIcon: bool,
  showTitleHeaderPLP: bool,
  slpLabels: shape({}),
  toastMessage: func,
  totalItems: number,
  updateCartCountAction: func,
  updateCartManuallyAction: func,
  isSBPUpdateEvent: bool,
  updateSBPEventAction: func,
};

HeaderNew.defaultProps = {
  isUpdateCartCount: false,
  updateCartManuallyAction: () => {},
  updateCartCountAction: () => {},
  cartVal: 0,
  slpLabels: {},
  navigation: {
    navigate: () => {},
  },
  screenProps: {},
  toastMessage: () => {},
  showSearchIcon: false,
  isOpenAddedToBag: false,
  totalItems: 0,
  isTransparent: false,
  isScrolled: false,
  showTitleHeaderPLP: false,
  hideBack: false,
  isSBP: false,
  sbpLabels: {},
  profileName: '',
  profile: {},
  profiles: [],
  overrideStyle: {},
  showSearchBarInline: false,
  orderLabels: {},
  selectOrderAction: () => {},
  isNewReDesignProductTile: false,
  isNewReDesignEnabled: false,
  isSBPUpdateEvent: false,
  updateSBPEventAction: () => {},
};

export default connect(mapStateToProps, mapDispatchToProps)(InitialPropsHOC(HeaderNew));

export {HeaderNew as HeaderNewVanilla};
