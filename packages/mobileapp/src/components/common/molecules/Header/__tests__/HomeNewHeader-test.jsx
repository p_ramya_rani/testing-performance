// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import {shallow} from 'enzyme';
import DeviceInfo from 'react-native-device-info';
import {HomeNewHeaderVanilla, mapDispatchToProps} from '../HomeNewHeader';
import {SearchContainer} from '../Header.style';
import {CartIconSection, CartCountContainer} from '../HeaderNew.style';

describe('HomeNewHeader Component', () => {
  let component;
  const props = {
    labels: {},
    stickyHeader: false,
    navigation: jest.fn(),
    navigateToNestedRoute: () => {},
  };

  beforeEach(() => {
    DeviceInfo.hasNotch = () => false;

    component = shallow(<HomeNewHeaderVanilla {...props} />);
  });

  it('Header should be defined', () => {
    expect(component).toBeDefined();
  });

  it('Header should render correctly', () => {
    expect(component).toMatchSnapshot();
  });

  it('Header should return SearchContainer component value one', () => {
    expect(component.find(SearchContainer)).toHaveLength(1);
  });

  it('Header should return CartIconSection component value one', () => {
    expect(component.find(CartIconSection)).toHaveLength(1);
  });

  it('Header should return ContCartCountContainerainer component value one', () => {
    expect(component.find(CartCountContainer)).toHaveLength(1);
  });

  describe('#mapDispatchToProps', () => {
    it('should return an action updateCartCountAction which will call dispatch function on execution', () => {
      const dispatch = jest.fn();
      const dispatchProps = mapDispatchToProps(dispatch);
      dispatchProps.updateCartCountAction();
      expect(dispatch.mock.calls).toHaveLength(1);
    });

    it('should return an action updateCartManuallyAction which will call dispatch function on execution', () => {
      const dispatch = jest.fn();
      const dispatchProps = mapDispatchToProps(dispatch);
      dispatchProps.updateCartManuallyAction();
      expect(dispatch.mock.calls).toHaveLength(1);
    });
  });
  describe('#getNewWelcomeMessage', () => {
    it('getNewWelcomeMessage Method should be return proper result', () => {
      const instance = component.instance();
      const getNewWelcomeMessage = instance.getNewWelcomeMessage('a|b|c', 1);
      expect(getNewWelcomeMessage).toEqual('a');
    });
    it('getNewWelcomeMessage Method should be return proper result with stored counter 2', () => {
      const instance = component.instance();
      const getNewWelcomeMessage = instance.getNewWelcomeMessage('a|b|c', 2);
      expect(getNewWelcomeMessage).toEqual('b');
    });
    it('getNewWelcomeMessage Method should be defined', () => {
      const instance = component.instance();
      const getNewWelcomeMessage = instance.getNewWelcomeMessage('a|b|c', 1);
      expect(getNewWelcomeMessage).toBeDefined();
    });
  });
});
