// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import {shallow} from 'enzyme';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import {fromJS} from 'immutable';
import {CheckoutHeaderVanilla, mapDispatchToProps, mapStateToProps} from '../CheckoutHeader';
import {
  CheckoutMessageContainer,
  StoreContainer,
  BackIconTouchable,
  SafeAreaViewStyle,
  CheckoutHeaderContainer,
  CheckoutHeaderTextSection,
  CartIconContainer,
  StyledIconTouchableOpacity,
} from '../Header.style';
import {CartCountContainer} from '../HeaderNew.style';

describe('CheckoutHeader Component', () => {
  let component;
  const props = {
    navigation: {
      goBack: jest.fn(),
      navigate: jest.fn(),
    },
    screenProps: {
      network: {
        isConnected: true,
      },
    },

    isExpressCheckoutPage: false,
    checkoutHeaderLabels: {
      backButtonLbl: 'back button',
      checkoutHeaderLabel: 'Checkout',
      expressCheckoutLbl: 'EXPRESS CHECKOUT',
    },
    isPayPalWebViewEnable: false,
    hideCartIconTest: '',
    setCouponMergeErrorAction: jest.fn(),
    setCheckoutStage: jest.fn(),
    cartVal: '1',
    currentStage: 'review',
  };

  beforeEach(() => {
    component = shallow(<CheckoutHeaderVanilla {...props} />);
  });

  it('Checkout Header should be defined', () => {
    expect(component).toBeDefined();
  });

  it(' Checkout Header should render correctly', () => {
    expect(component).toMatchSnapshot();
  });

  it('Checkout Header should return Container component value with shipping', () => {
    const newProps = {...props, currentStage: 'shipping'};
    const wrapper = shallow(<CheckoutHeaderVanilla {...newProps} />);
    expect(wrapper.find(CheckoutMessageContainer)).toHaveLength(1);
    expect(wrapper.find(StoreContainer)).toHaveLength(1);
    expect(wrapper.find(BackIconTouchable)).toHaveLength(1);
    wrapper.find(BackIconTouchable).first().props().onPress();
    expect(component).toMatchSnapshot();
  });
  it('When isExpressCheckoutPage is true value', () => {
    const newProps = {...props, isExpressCheckoutPage: true};
    const wrapper = shallow(<CheckoutHeaderVanilla {...newProps} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('Checkout Header should return Container component value one', () => {
    expect(component.find(CheckoutMessageContainer)).toHaveLength(1);
    expect(component.find(StoreContainer)).toHaveLength(1);
    expect(component.find(BackIconTouchable)).toHaveLength(1);
    component.find(BackIconTouchable).first().props().onPress();
    expect(component).toMatchSnapshot();
  });

  it('Checkout Header should return SafeAreaViewStyle component value one', () => {
    expect(component.find(SafeAreaViewStyle)).toHaveLength(1);
  });
  it('Checkout Header should return CartCountContainer component value one', () => {
    expect(component.find(CartCountContainer)).toHaveLength(1);
    const data = component.find(BodyCopy).first().props().text;
    expect(data).toBe('Checkout');
  });

  it('Header should return CartIconContainer component value one', () => {
    expect(component.find(CartIconContainer)).toHaveLength(1);
    expect(component.find(StyledIconTouchableOpacity)).toHaveLength(1);
    component.find(StyledIconTouchableOpacity).first().props().onPress();
  });

  it('Header should return CheckoutHeaderContainer component value one', () => {
    expect(component.find(CheckoutHeaderContainer)).toHaveLength(1);
  });

  it('Header should return CheckoutHeaderTextSection component value one', () => {
    expect(component.find(CheckoutHeaderTextSection)).toHaveLength(1);
  });

  it('Header should return BackIconTouchable component value one', () => {
    expect(component.find(BackIconTouchable)).toHaveLength(1);
  });

  it('Checkout Header should render correctly', () => {
    const componentInstance = component.instance();
    componentInstance.onBackPress();
    component.setProps({currentStage: 'shipping'});
    expect(component).toMatchSnapshot();
  });

  it('Header noInterNetHandle to be called', () => {
    const mock = jest.fn();
    const prop = {
      screenProps: {network: {isConnected: false}},
      toastMessage: mock,
    };
    component.setState({isIconIn: false});
    component.setProps(prop);
    expect(prop.toastMessage).toHaveBeenCalled();
    expect(component).toMatchSnapshot();
  });

  describe('#mapDispatchToProps', () => {
    it('should return an action setCheckoutStage which will call dispatch function on execution', () => {
      const dispatch = jest.fn();
      const dispatchProps = mapDispatchToProps(dispatch);
      dispatchProps.setCheckoutStage();
      expect(dispatch.mock.calls).toHaveLength(1);
    });

    it('should return an action setCouponMergeErrorAction which will call dispatch function on execution', () => {
      const dispatch = jest.fn();
      const dispatchProps = mapDispatchToProps(dispatch);
      dispatchProps.setCouponMergeErrorAction();
      expect(dispatch.mock.calls).toHaveLength(1);
    });

    it('should return an action toastMessage which will call dispatch function on execution', () => {
      const dispatch = jest.fn();
      const dispatchProps = mapDispatchToProps(dispatch);
      dispatchProps.toastMessage();
      expect(dispatch.mock.calls).toHaveLength(1);
    });
  });

  describe('mapStateToProps', () => {
    it('mapStateToProps', () => {
      const CheckoutState = fromJS({
        values: {
          shipping: {
            emailAddress: 'abc123@test.com',
            address: {
              addressLine: ['abc', 'def'],
            },
          },
        },
      });
      const UserState = fromJS({
        personalData: {
          email: 'test',
        },
      });

      const state = {
        isExpressEligible: '',
        personalDate: '',
        User: UserState,
        Checkout: CheckoutState,
        Header: {
          cartItemCount: 1,
        },
        Labels: {
          global: {
            header: {
              lbl_header_storeDefaultTitle: 'Find a Store',
              lbl_header_welcomeMessage: 'Welcome!',
            },
          },
          StoreLocator: {
            StoreLanding: 'Test',
          },
        },
        CartPageReducer: fromJS({
          isPayPalWebViewEnable: {},
        }),
      };
      mapStateToProps(state);
    });
  });
});
