// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import {TouchableOpacity} from 'react-native';
import {fromJS} from 'immutable';
import {shallow} from 'enzyme';
import {isGymboree} from '@tcp/core/src/utils';
import DeviceInfo from 'react-native-device-info';
import {BagPageHeaderVanilla, mapDispatchToProps, mapStateToProps} from '../BagPageHeader';
import {
  SafeAreaViewStyle,
  BagPageContainer,
  BrandIconSection,
  CloseContainer,
  BrandIcon,
  CloseIcon,
  CloseIconTouchable,
} from '../Header.style';
import {BagPageBackContainer} from '../BagPageHeader.style';

describe('BagPageHeader Component', () => {
  let component;
  const props = {
    navigation: {
      goBack: jest.fn(),
      dangerouslyGetParent: () => {
        return {
          state: {
            routeName: '',
            index: 1,
            routes: ['A', 'B'],
          },
        };
      },
    },
    bagLoading: false,
    showBrandIcon: true,
    showCloseButton: true,
    showGobackIcon: false,
    isPayPalWebViewEnable: false,
    disableBagLoading: false,
    screenProps: {
      checkFirst: () => {
        return 'HomeFirst';
      },
    },
  };

  beforeEach(() => {
    DeviceInfo.hasNotch = () => false;
    component = shallow(<BagPageHeaderVanilla {...props} />);
  });

  it('BagPage Header should be defined', () => {
    expect(component).toBeDefined();
  });

  it('BagPage Header should render correctly', () => {
    expect(component).toMatchSnapshot();
  });

  it('BagPage Header should return SafeAreaViewStyle component value one', () => {
    expect(component.find(SafeAreaViewStyle)).toHaveLength(1);
  });

  it('BagPage should return BagPageHeaderContainer component value one', () => {
    const newProps = {...props, disableBagLoading: true, showGobackIcon: true};
    const wrapper = shallow(<BagPageHeaderVanilla {...newProps} />);
    expect(wrapper.find(BagPageContainer)).toHaveLength(1);
    expect(wrapper.find(BagPageBackContainer)).toHaveLength(1);
    wrapper.find(TouchableOpacity).first().props().onPress();
    expect(component).toMatchSnapshot();
  });

  it('BagPage should return BrandIconSection component value one', () => {
    expect(component.find(BrandIconSection)).toHaveLength(1);
  });

  it('BagPage should return CloseContainer component value one', () => {
    expect(component.find(CloseContainer)).toHaveLength(1);
    component.find(CloseIconTouchable).first().props().onPress();
    expect(component).toMatchSnapshot();
  });

  it('BagPage should return BrandIcon component value one', () => {
    expect(component.find(BrandIcon)).toHaveLength(1);
    expect(isGymboree()).toBe(false);
  });

  it('BagPage should return CloseIcon component value one', () => {
    expect(component.find(CloseIcon)).toHaveLength(1);
  });
});

describe('BagPageHeader Component With Loading State', () => {
  let component;
  const props = {
    navigation: {},
    bagLoading: true,
  };

  beforeEach(() => {
    component = shallow(<BagPageHeaderVanilla {...props} />);
  });

  it('BagPage should not return CloseContainer component value one', () => {
    expect(component.find(CloseContainer)).toHaveLength(0);
  });

  it('BagPage should not return CloseIcon component value one', () => {
    expect(component.find(CloseIcon)).toHaveLength(0);
  });
});

describe('mapDispatchToProps', () => {
  const dispatch = jest.fn();
  it('should return trackClickAction', () => {
    const props = mapDispatchToProps(dispatch);
    props.toastMessage();
    expect(dispatch).toHaveBeenCalled();
  });

  it('should return setClickAnalyticsDataAction', () => {
    const props = mapDispatchToProps(dispatch);
    props.setCouponMergeErrorAction();
    expect(dispatch).toHaveBeenCalled();
  });
});

describe('mapStateToProps', () => {
  it('mapStateToProps', () => {
    const state = {
      Labels: {
        global: {
          header: {
            lbl_header_storeDefaultTitle: 'Find a Store',
            lbl_header_welcomeMessage: 'Welcome!',
          },
        },
        StoreLocator: {
          StoreLanding: 'Test',
        },
      },
      CartPageReducer: fromJS({
        isPayPalWebViewEnable: {},
      }),
    };
    const mappedProps = mapStateToProps(state);
    const expected = {
      labels: {
        lbl_header_storeDefaultTitle: 'Find a Store',
        lbl_header_welcomeMessage: 'Welcome!',
      },
      isPayPalWebViewEnable: false,
      bagLoading: undefined,
    };
    expect(mappedProps).toEqual(expected);
  });
});
