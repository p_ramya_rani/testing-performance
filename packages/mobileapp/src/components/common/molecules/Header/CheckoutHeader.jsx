// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import {arrayOf, bool, func, objectOf, shape, string} from 'prop-types';
import {connect} from 'react-redux';
import BodyCopy from '@tcp/core/src/components/common/atoms/BodyCopy';
import {getLocator} from '@tcp/core/src/utils';
import ToastContainer from '@tcp/core/src/components/common/atoms/Toast/container/Toast.container.native';
import {toastMessageInfo} from '@tcp/core/src/components/common/atoms/Toast/container/Toast.actions';
import CheckoutSelectors, {
  isExpressCheckout,
} from '@tcp/core/src/components/features/CnC/Checkout/container/Checkout.selector';
import {getSetCheckoutStage} from '@tcp/core/src/components/features/CnC/Checkout/container/Checkout.action';
import {setCouponMergeError} from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.actions.util';
import {getCartIconCheckoutNavigationABTest} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import checkoutUtil from '@tcp/core/src/components/features/CnC/Checkout/util/utility';
import BagPageSelector from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.selectors';
import CHECKOUT_CONSTANTS from '@tcp/core/src/components/features/CnC/Checkout/Checkout.constants';
import {getLabelValue} from '@tcp/core/src/utils/utils';
import {
  CheckoutMessageContainer,
  StoreContainer,
  SafeAreaViewStyle,
  ImageColor,
  CheckoutHeaderContainer,
  CheckoutHeaderTextSection,
  CartIconView,
  CartIconContainer,
  StyledIconTouchableOpacity,
  BackIcon,
  BackIconTouchable,
} from './Header.style';
import {CartCountContainer} from './HeaderNew.style';
import {INTERNET_OFF} from './Header.constants';

const {getCurrentCheckoutStage} = CheckoutSelectors;

/**
 * This component creates Mobile Header
 * @param {*} props Props passed from Header screen
 */
const leftIcon = require('../../../../../../mobileapp/src/assets/images/carrot-large-left.png');
const cartIcon = require('../../../../../../mobileapp/src/assets/images/empty-bag.png');

/**
 * This component creates Mobile Header.
 * 1. To Manage the store locator
 * 2. To Navigate the cart page & show cart quantity
 * 3. To show the welcome text for guest user
 *     and shoe the name fro register user
 */
class CheckoutHeader extends React.PureComponent {
  constructor(props) {
    super(props);
    const {cartOrderItems, currentStage} = props;
    const {CHECKOUT_STAGES} = CHECKOUT_CONSTANTS;
    this.state = {currentStage: currentStage || 'Shipping'};
    this.availableStages = checkoutUtil.getAvailableStages(cartOrderItems) || CHECKOUT_STAGES;
  }

  componentDidUpdate(prevProps) {
    const {currentStage: prevCurrentStage} = prevProps;
    const {currentStage} = this.props;
    if (prevCurrentStage !== currentStage) {
      this.updateState({currentStage});
    }
    this.noInterNetHandle(prevProps);
  }

  onBackPress = () => {
    const {navigation, setCheckoutStage, setCouponMergeErrorAction} = this.props;
    const {currentStage} = this.state;
    const currentStageIndex = this.availableStages.indexOf(currentStage.toLowerCase());
    if (currentStageIndex > 0) {
      const stageToRoute = this.availableStages[currentStageIndex - 1];
      setCheckoutStage(stageToRoute);
    } else {
      navigation.goBack();
    }
    setCouponMergeErrorAction(false);
  };

  updateState({currentStage}) {
    this.setState({currentStage});
  }

  noInterNetHandle(prevProps) {
    const {
      screenProps: {
        network: {isConnected},
      },
      toastMessage,
    } = this.props;
    const {
      screenProps: {
        network: {isConnected: PrevIsConnected},
      },
    } = prevProps;
    if (isConnected !== PrevIsConnected && !isConnected) {
      toastMessage(INTERNET_OFF);
    }
  }

  render() {
    const {
      isExpressCheckoutPage,
      checkoutHeaderLabels,
      isPayPalWebViewEnable,
      hideCartIconTest,
      setCouponMergeErrorAction,
      navigation,
      cartVal,
    } = this.props;
    const {expressCheckoutLbl, checkoutHeaderLabel, backButtonLbl} = checkoutHeaderLabels;
    return (
      <SafeAreaViewStyle>
        {!isPayPalWebViewEnable && (
          <>
            <ToastContainer />
            <CheckoutHeaderContainer data-locator={getLocator('global_headerpanel')}>
              <CheckoutMessageContainer>
                <StoreContainer accessibilityRole="button" accessibilityLabel={backButtonLbl}>
                  <BackIconTouchable onPress={this.onBackPress}>
                    <BackIcon
                      source={leftIcon}
                      style={ImageColor}
                      data-locator={getLocator('global_headerpanelcollapsedicon')}
                    />
                  </BackIconTouchable>
                </StoreContainer>
              </CheckoutMessageContainer>
              <CheckoutHeaderTextSection>
                <BodyCopy
                  fontFamily="primary"
                  fontSize="fs12"
                  fontWeight="semibold"
                  text={isExpressCheckoutPage ? expressCheckoutLbl : checkoutHeaderLabel}
                  color="gray.900"
                />
              </CheckoutHeaderTextSection>
              <CartIconContainer>
                {!hideCartIconTest && (
                  <StyledIconTouchableOpacity
                    accessibilityRole="button"
                    onPress={() => {
                      setCouponMergeErrorAction(false);
                      navigation.goBack();
                      navigation.navigate('BagPage', {fromAppCheckout: true});
                    }}>
                    <CartIconView
                      source={cartIcon}
                      data-locator={getLocator('global_headerpanelbagicon')}
                      cartVal={cartVal}
                    />
                    <CartCountContainer cartVal={cartVal}>
                      <BodyCopy
                        text={cartVal}
                        color="white"
                        fontSize="fs10"
                        data-locator={getLocator('global_headerpanelbagitemtext')}
                        accessibilityText="Mini bag with count"
                        fontWeight="extrabold"
                      />
                    </CartCountContainer>
                  </StyledIconTouchableOpacity>
                )}
              </CartIconContainer>
            </CheckoutHeaderContainer>
          </>
        )}
      </SafeAreaViewStyle>
    );
  }
}

export const mapStateToProps = (state) => {
  return {
    checkoutHeaderLabels: {
      checkoutHeaderLabel: getLabelValue(
        state.Labels,
        'lbl_checkoutheader_checkout',
        'checkoutHeader',
        'checkout',
      ),
      expressCheckoutLbl: getLabelValue(
        state.Labels,
        'lbl_checkoutHeader_expressCheckout',
        'checkoutHeader',
        'checkout',
      ),
      backButtonLbl: getLabelValue(state.Labels, 'lbl_back_button', 'accessibility', 'global'),
    },
    isExpressCheckoutPage: isExpressCheckout(state),
    cartOrderItems: BagPageSelector.getOrderItems(state),
    hideCartIconTest: getCartIconCheckoutNavigationABTest(state),
    currentStage: getCurrentCheckoutStage(state),
    isPayPalWebViewEnable: BagPageSelector.getPayPalWebViewStatus(state),
    cartVal: state.Header && state.Header.cartItemCount,
  };
};

export const mapDispatchToProps = (dispatch) => {
  return {
    setCheckoutStage: (payload) => {
      dispatch(getSetCheckoutStage(payload));
    },
    setCouponMergeErrorAction: (payload) => {
      dispatch(setCouponMergeError(payload));
    },
    toastMessage: (payload) => {
      dispatch(toastMessageInfo(payload));
    },
  };
};

CheckoutHeader.propTypes = {
  cartOrderItems: shape([]).isRequired,
  cartVal: string,
  checkoutHeaderLabels: objectOf(shape({})),
  currentStage: string.isRequired,
  hideCartIconTest: bool.isRequired,
  isExpressCheckoutPage: bool,
  isPayPalWebViewEnable: bool,
  navigation: arrayOf(shape({})).isRequired,
  screenProps: objectOf(
    shape({
      network: shape({
        isConnected: bool,
      }),
    }),
  ),
  setCheckoutStage: func.isRequired,
  setCouponMergeErrorAction: func.isRequired,
  toastMessage: func,
};

CheckoutHeader.defaultProps = {
  isExpressCheckoutPage: false,
  checkoutHeaderLabels: {
    lbl_checkoutheader_checkout: '',
    lbl_checkoutHeader_expressCheckout: '',
  },
  isPayPalWebViewEnable: false,
  screenProps: {},
  toastMessage: () => {},
  cartVal: '0',
};

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutHeader);
export {CheckoutHeader as CheckoutHeaderVanilla};
