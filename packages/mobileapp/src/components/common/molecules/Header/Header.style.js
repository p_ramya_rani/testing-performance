// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';
import {SafeAreaView} from 'react-navigation';
import createThemeColorPalette from '@tcp/core/styles/themes/createThemeColorPalette';
import {ViewWithSpacing} from '@tcp/core/src/components/common/atoms/styledWrapper';
import {colorNero} from '../../../features/shopByProfile/ShopByProfile.styles';

const colorPallete = createThemeColorPalette();
const headerTranscluscentColor = '#FFFFFFCC';

const setBackground = (props) => {
  if (props.theme.isGymboree) {
    return `
    background-color: ${props.theme.colorPalette.orange[900]};
    `;
  }
  return `
  background-color: ${props.theme.colorPalette.blue[800]};
  `;
};

const cartItemsWidth = (cartItems) => {
  let width = '';
  switch (cartItems.toString().length) {
    case 2:
      width = '20px';
      break;
    case 3:
      width = '25px';
      break;
    default:
      width = '15px';
  }
  return width;
};

export const SafeAreaViewStyle = styled(SafeAreaView)`
  background: ${(props) => props.theme.colorPalette.white};
  ${(props) =>
    props.isDeviceIPhoneHasNotch ? `padding-top: ${props.theme.spacing.APP_LAYOUT_SPACING.XS}` : ``}
`;

export const RewardBadge = styled(ViewWithSpacing)`
  border: 1px solid
    ${(props) =>
      props.isPlcc
        ? props.theme.colorPalette.userTheme.plcc
        : props.theme.colorPalette.userTheme.mpr};
  border-style: solid;
  border-left-width: 0px;
  border-left-color: transparent;
  border-radius: 12px;
`;
export const RewardBadgeIcon = styled.Image`
  width: 20px;
  height: 25px;
  position: relative;
  left: 7px;
  z-index: 100;
`;
export const Container = styled.View`
  align-items: center;
  justify-content: center;
  width: 100%;
`;

export const SearchContainer = styled.View`
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
`;

export const HeaderContainer = styled.View`
  flex-direction: row;
  padding-left: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  padding-right: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  display: flex;
  background: ${(props) => props.theme.colorPalette.white};
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
`;

export const MessageContainer = styled.View`
  ${(props) => (props.position === 'store' ? `flex-direction: row;` : null)};
  align-items: flex-start;
  z-index: ${(props) => props.theme.zindex.zOverlay};
  justify-content: space-between;
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  width: 70%;
  display: flex;
`;

export const CheckoutMessageContainer = styled.View`
  ${(props) => (props.position === 'store' ? `flex-direction: row;` : null)};
  align-items: flex-start;
  justify-content: center;
  flex: 0.2;
  padding-left: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
`;

export const BackContainer = styled.View`
  flex-direction: row;
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  width: 70%;
`;

export const CartContainer = styled.View`
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  align-self: flex-end;
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
  width: ${(props) => (props.searchIconVisible ? '15%' : '30%')};
  margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
`;

export const CartIconContainer = styled.View`
  align-items: flex-end;
  align-self: flex-end;
  flex: 0.2;
  margin-right: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
`;

export const SearchIconSection = styled.View`
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  width: 15%;
  height: 100%;
`;

export const StoreContainer = styled.TouchableOpacity`
  align-items: center;
  display: flex;
  flex-direction: row;
  justify-content: center;
`;

export const Icon = styled.Image`
  width: 9px;
  height: 5px;
  margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
`;

export const RoundView = styled.View`
  ${setBackground}
  width: ${(props) => cartItemsWidth(props.cartVal ? props.cartVal : 0)};
  height: 15px;
  border-radius: 8;
  margin: 2px;
`;

export const RoundCircle = styled.View`
  background-color: ${(props) =>
    props.isFromHomePage ? 'transparent' : props.theme.colorPalette.white};
  width: ${(props) => cartItemsWidth(props.cartVal ? props.cartVal : 0)};
  height: 22px;
  border-radius: 10px;
  justify-content: center;
  align-items: center;
  position: absolute;
  right: 0;
  bottom: 0;
  border: 2px solid white;
`;

export const CartIconView = styled.Image`
  width: 25px;
  height: 26px;
  margin-top: ${(props) => (props.isFromHomePage ? '-10px' : '0px')};
  margin-right: ${(props) =>
    props.cartVal.toString().length === 1 ? 0 : props.theme.spacing.ELEM_SPACING.XXS};
`;

export const TextStyle = {
  paddingLeft: 4,
  paddingTop: 1.5,
};

export const ImageColor = {
  tintColor: 'gray',
};

export const HeaderPromoContainer = styled.View`
  height: 45px;
  align-items: center;
  justify-content: center;
  background: ${(props) => props.theme.colors.PRIMARY.PALEGRAY};
  margin-top: ${(props) => props.theme.spacing.LAYOUT_SPACING.XXS};
`;

export const Touchable = styled.TouchableOpacity`
  align-items: flex-end;
  align-self: flex-end;
  width: 45px;
`;

export const CheckoutHeaderContainer = styled.View`
  display: flex;
  flex-direction: row;
  background: ${(props) => props.theme.colorPalette.white};
  border-bottom-color: ${(props) => props.theme.colorPalette.gray[500]};
  border-bottom-width: 1;
`;

export const CheckoutHeaderTextSection = styled.View`
  height: 54px;
  display: flex;
  flex: 0.6;
  align-items: center;
  justify-content: center;
`;

export const BackIcon = styled.Image`
  width: 10px;
  height: 18px;
  margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.XXS};
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
`;

export const BrandIcon = styled.Image`
  width: 90px;
  height: 35px;
`;

export const BrandIconSection = styled.View`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: row;
  width: 100%;
  border-bottom-color: ${(props) => props.theme.colorPalette.gray[500]};
  border-bottom-width: 1;
  height: 54px;
`;

export const BackIconTouchable = styled.TouchableOpacity`
  width: 45px;
`;

export const CloseIcon = styled.Image`
  width: 15px;
  height: 15px;
`;

export const CloseIconTouchable = styled.TouchableOpacity`
  width: auto;
  padding-top: 10px;
  padding-bottom: 10px;
  padding-right: 10px;
  padding-left: 50px;
`;

export const CloseContainer = styled.View`
  position: absolute;
  z-index: ${(props) => props.theme.zindex.zOverlay};
  justify-content: space-between;
  right: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  top: 10px;
`;

export const BagPageContainer = styled.View`
  display: flex;
  flex-direction: row;
`;

export const LeftSection = styled.View`
  justify-content: center;
  align-items: center;
  padding-left: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  width: 20%;
  height: 100%;
`;

export const ArrowBackIcon = styled.Image`
  width: 10px;
  height: 18px;
  margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.XS};
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXXS};
`;

export const MiddleSection = styled.View`
  justify-content: center;
  align-items: center;
`;

export const TitleText = styled.Text`
  color: ${(props) => props.theme.colorPalette.gray[900]};
  font-family: ${(props) => props.theme.typography.fonts.primary};
  font-size: ${(props) => props.theme.typography.fontSizes.fs12};
  font-weight: ${(props) => props.theme.typography.fontWeights.semibold};
  line-height: 14.5;
  margin-left: 120px;
`;

export const headerStyle = {
  backgroundColor: colorPallete.white,
};

export const transcluscentHeaderStyle = {
  backgroundColor: headerTranscluscentColor,
  flex: 0,
};

export const StyledIconTouchableOpacity = styled.TouchableOpacity`
  border-radius: 10px;
  display: flex;
  justify-content: center;
  ${(props) =>
    props.newPdpDesignFlag
      ? `
    background-color: ${headerTranscluscentColor};
    align-items: center;
    height: 40px;
    width: 44px;
    `
      : `
    background-color: ${props.isTransparent ? headerTranscluscentColor : 'transparent'};
    align-items: ${props.isArrowIcon ? 'flex-start' : 'center'};
    margin-right: 5px;
    height: 40px;
    width: 40px;
    padding-left: ${props.isBackIcon ? '10px' : '0'};
  `}
`;

export const androidPdpHeaderStyle = {
  backgroundColor: 'white',
  height: 100,
  width: '100%',
};

export const outfitDetailStyle = {
  top: 48,
};

export const androidHeaderStyle = {
  height: 100,
  width: '100%',
  top: 30,
};

export const HomePageHeaderContainer = styled.View`
  padding-top: ${(props) => (props.hasNotch ? '40px' : '20px')};
  background-color: ${(props) =>
    props.stickyHeader ? props.theme.colorPalette.gray[300] : 'transparent'};
`;

export const TitleWrapper = styled.View`
  margin-left: 14px;
  margin-right: 14px;
  flex-direction: row;
  align-items: center;
  padding-top: 10px;
`;

export const WelcomeMessageWrapper = styled.View`
  flex-direction: row;
  align-items: center;
  height: 20px;
`;

export const UserNameTextStyle = styled.Text`
  flex-shrink: 1;
`;

export const WelcomeTextStyle = styled.Text`
  color: ${colorNero};
  font-family: ${(props) => props.theme.typography.fonts.primary};
  font-size: ${(props) => props.theme.typography.fontSizes.fs18};
  font-weight: normal;
  line-height: 18px;
  letter-spacing: 0.2px;
`;

export const TitleTextStyle = styled.Text`
  color: ${colorNero};
  font-family: ${(props) => props.theme.typography.fonts.primary};
  font-size: ${(props) => props.theme.typography.fontSizes.fs18};
  font-weight: bold;
  line-height: 18px;
  letter-spacing: 0.2px;
  flex-shrink: 1;
`;

export const SearchBarContainer = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin-top: ${(props) => (props.stickyHeader ? '5px' : '10px')};
  margin-bottom: ${(props) => (props.stickyHeader ? '5px' : '0px')};
  padding-right: 14px;
  height: auto;
`;
export const BagIconWrapper = styled.View`
  margin-top: -10px;
`;

export default {
  BackContainer,
  Container,
  MessageContainer,
  StoreContainer,
  Icon,
  CartContainer,
  RoundView,
  TextStyle,
  CartIconView,
  ImageColor,
  HeaderPromoContainer,
  Touchable,
  CheckoutHeaderContainer,
  CheckoutHeaderTextSection,
  BackIcon,
  BackIconTouchable,
  BrandIconSection,
  CloseIconTouchable,
  CloseIcon,
  CloseContainer,
  BagPageContainer,
  LeftSection,
  MiddleSection,
  TitleText,
  SafeAreaViewStyle,
  CheckoutMessageContainer,
  headerStyle,
  transcluscentHeaderStyle,
  RewardBadge,
  RewardBadgeIcon,
  HomePageHeaderContainer,
  WelcomeTextStyle,
  TitleWrapper,
  TitleTextStyle,
  SearchBarContainer,
  BagIconWrapper,
  WelcomeMessageWrapper,
  UserNameTextStyle,
};
