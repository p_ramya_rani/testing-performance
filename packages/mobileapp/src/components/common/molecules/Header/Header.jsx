// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import {bool, func, objectOf, number, shape, string} from 'prop-types';
import {TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import {SafeAreaView} from 'react-navigation';

import {BodyCopy} from '@tcp/core/src/components/common/atoms';
import {
  getLocator,
  capitalize,
  navigateToNestedRoute,
  getLabelValue,
  getStoreHours,
  isMobileApp,
} from '@tcp/core/src/utils';
import {getValueFromAsyncStorage, onBack, dayChange} from '@tcp/core/src/utils/utils.app';
import {resetStoresByCoordinates} from '@tcp/core/src/components/features/storeLocator/StoreLanding/container/StoreLanding.actions';
import InitialPropsHOC from '@tcp/core/src/components/common/hoc/InitialPropsHOC/InitialPropsHOC.native';
import BAGPAGE_SELECTORS from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.selectors';
import {
  updateCartCount,
  updateCartManually,
} from '@tcp/core/src/components/common/organisms/Header/container/Header.actions';
import ToastContainer from '@tcp/core/src/components/common/atoms/Toast/container/Toast.container.native';
import {toastMessageInfo} from '@tcp/core/src/components/common/atoms/Toast/container/Toast.actions';

import {
  getUserLoggedInState,
  getUserName,
  getTotalRewardsState,
  isPlccUser,
} from '@tcp/core/src/components/features/account/User/container/User.selectors';
import {SearchBar} from '@tcp/core/src/components/common/molecules';
import SearchProduct from '@tcp/core/src/components/common/organisms/SearchProduct';
import {readCookie} from '@tcp/core/src/utils/cookie.util';
import CustomIcon from '@tcp/core/src/components/common/atoms/Icon';
import {
  ICON_NAME,
  ICON_FONT_CLASS,
} from '@tcp/core/src/components/common/atoms/Icon/Icon.constants';
import {trackForterAction, ForterActionType} from '@tcp/core/src/utils/forter.util';
import {isOpenAddedToBag as getIsOpenAddedToBag} from '@tcp/core/src/components/features/CnC/AddedToBag/container/AddedToBag.selectors';
import {getIsNewReDesignEnabled} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import {INTERNET_OFF} from './Header.constants';

import {
  Container,
  MessageContainer,
  StoreContainer,
  CartContainer,
  Icon,
  RoundView,
  SafeAreaViewStyle,
  TextStyle,
  CartIconView,
  ImageColor,
  Touchable,
  HeaderContainer,
  SearchContainer,
  RoundCircle,
  ArrowBackIcon,
  MiddleSection,
  TitleText,
  BackContainer,
  SearchIconSection,
  StyledIconTouchableOpacity,
  RewardBadge,
  RewardBadgeIcon,
  headerStyle,
  WelcomeMessageWrapper,
  UserNameTextStyle,
} from './Header.style';

const CART_ITEM_COUNTER = 'cartItemsCount';

/**
 * This component creates Mobile Header
 * @param {*} props Props passed from Header screen
 */
const backicon = require('../../../../assets/images/carrot-large-left.png');
const downIcon = require('../../../../assets/images/carrot-small-down.png');
const cartIcon = require('../../../../assets/images/empty-bag.png');

const rewardsBlue = require('../../../../assets/images/rewards-blue.png');
const rewardsOrange = require('../../../../assets/images/rewards-orange.png');

const STORE_TYPE = 'store';

/**
 * This component creates Mobile Header.
 * 1. To Manage the store locator
 * 2. To Navigate the cart page & show cart quantity
 * 3. To show the welcome text for guest user
 *     and shoe the name fro register user
 */
class Header extends React.PureComponent {
  /**
   * To manage the state of icons on the
   * basis of expand & collaps .
   */
  constructor(props) {
    super(props);
    this.state = {
      isDownIcon: false,
      showSearchModal: false,
      storedCounter: 1,
    };
  }

  componentDidMount() {
    dayChange().then(() => {
      this.getCounter();
    });
  }

  componentDidUpdate(prevProps) {
    const {
      isUpdateCartCount,
      updateCartManuallyAction,
      isUserLoggedIn,
      isOpenAddedToBag,
      totalItems,
      cartVal,
    } = this.props;
    dayChange().then(() => {
      this.getCounter();
    });
    if (
      isUpdateCartCount !== prevProps.isUpdateCartCount ||
      isUserLoggedIn !== prevProps.isUserLoggedIn ||
      totalItems !== cartVal ||
      (isOpenAddedToBag !== prevProps.isOpenAddedToBag && isOpenAddedToBag === true)
    ) {
      this.getInitialProps();
      updateCartManuallyAction(false);
    }
    this.noInterNetHandle(prevProps);
  }

  getInitialProps() {
    const {updateCartCountAction, totalItems} = this.props;
    if (totalItems) {
      updateCartCountAction(totalItems);
    } else {
      const cartValuePromise = readCookie(CART_ITEM_COUNTER);
      cartValuePromise.then((res) => {
        updateCartCountAction(parseInt(res || 0, 10));
      });
    }
  }

  getCounter = async () => {
    const storedCounter = await getValueFromAsyncStorage('COUNTER');
    this.setState({storedCounter});
  };

  /**
   * This function validate the iconView.
   */
  validateIcon = () => {
    const {navigation, labels, resetSuggestedStores} = this.props;
    const {isDownIcon} = this.state;
    resetSuggestedStores([]);
    navigation.navigate({
      routeName: 'StoreLanding',
      params: {
        title: getLabelValue(labels, 'lbl_header_storeDefaultTitle').toUpperCase(),
      },
    });
    this.setState({
      isDownIcon: !isDownIcon,
    });
  };

  handleUserRewards = (userRewards = 0) => {
    return userRewards % 1 ? userRewards : Math.floor(userRewards);
  };

  /**
   * @function openSearchProductPage
   * opens search product modal
   *
   * @memberof Header
   */
  openSearchProductPage = () => {
    this.setState({showSearchModal: true});
  };

  /**
   * @function closeSearchProductPage
   * closes search product modal
   *
   * @memberof Header
   */
  closeSearchProductPage = () => {
    this.setState({showSearchModal: false});
  };

  /**
   * @function goToSearchResultsPage
   * navigates to search results page
   *
   * @memberof Header
   */
  goToSearchResultsPage = (searchText, typeahead) => {
    this.closeSearchProductPage();

    const {navigation, isNewReDesignProductTile} = this.props;

    if (isMobileApp()) {
      trackForterAction(ForterActionType.SEARCH_QUERY, searchText);
    }

    navigateToNestedRoute(navigation, 'HomeStack', 'SearchDetail', {
      title: searchText,
      isForceUpdate: true,
      searchedType: typeahead ? 'typeahead:keyword' : 'keyword',
      newDesignColor: isNewReDesignProductTile,
    });
  };

  renderSearchBar = () => {
    const {showSearch, slpLabels, navigation, showSearchIcon} = this.props;
    const {showSearchModal} = this.state;
    if (!showSearch && !showSearchIcon) return null;
    return (
      <SearchContainer>
        {showSearch && (
          <SearchBar
            openSearchProductPage={this.openSearchProductPage}
            navigation={navigation}
            labels={slpLabels}
          />
        )}
        {showSearchModal && (
          <SearchProduct
            closeSearchModal={this.closeSearchProductPage}
            goToSearchResultsPage={this.goToSearchResultsPage}
            navigation={navigation}
          />
        )}
      </SearchContainer>
    );
  };

  getNewWelcomeMessage = (messageString, storedCounter) => {
    const messageArray = messageString.split('|');
    return messageArray[storedCounter - 1];
  };

  getWelcomeMessageText = () => {
    const {labels, isUserLoggedIn} = this.props;
    const {storedCounter} = this.state;
    let welcomeMessage = '';
    if (labels && labels.lbl_header_welcomeTxt) {
      welcomeMessage = this.getNewWelcomeMessage(labels.lbl_header_welcomeTxt, storedCounter);
    }
    if (welcomeMessage === '') {
      return welcomeMessage;
    }
    if (welcomeMessage === 'Ready to shop') {
      return isUserLoggedIn ? `${welcomeMessage}, ` : `${welcomeMessage}?`;
    }
    return isUserLoggedIn ? `${welcomeMessage}, ` : `${welcomeMessage}!`;
  };

  getLoggedInName = (message) => {
    const {isUserLoggedIn, userName} = this.props;
    let welcomeMessage = '';
    if (message === 'Ready to shop, ') {
      welcomeMessage = isUserLoggedIn ? `${userName}?` : '';
    } else {
      welcomeMessage = isUserLoggedIn ? `${userName}!` : '';
    }

    return welcomeMessage;
  };

  getFavStoreText = () => {
    const {favStore, labels, storeLocatorLabels} = this.props;
    const basicInfo = favStore && favStore.basicInfo;
    const {hours = {}} = favStore || {};
    const currentDate = new Date();
    const storeTime =
      hours && Object.keys(hours).length > 0
        ? getStoreHours(hours, storeLocatorLabels, currentDate)
        : null;
    const isInfoPresent = basicInfo && basicInfo.storeName && storeTime;
    let favStoreText;
    if (labels && labels.lbl_header_openUntil) {
      favStoreText = isInfoPresent
        ? `${capitalize(basicInfo.storeName)} ${storeTime}`
        : labels.lbl_header_storeDefaultTitle;
    }
    return favStoreText;
  };

  noInterNetHandle(prevProps) {
    const {
      screenProps: {
        network: {isConnected},
      },
      toastMessage,
    } = this.props;
    const {
      screenProps: {
        network: {isConnected: PrevIsConnected},
      },
    } = prevProps;
    if (isConnected !== PrevIsConnected && !isConnected) {
      toastMessage(INTERNET_OFF);
    }
  }

  render() {
    const {
      labels,
      cartVal,
      navigation,
      headertype,
      title,
      showSearch,
      showSearchIcon,
      userRewards,
      isPlcc,
      isUserLoggedIn,
    } = this.props;
    const {routeName} = navigation.state;
    const rewardIcon = isPlcc ? rewardsBlue : rewardsOrange;
    const {isDownIcon} = this.state;
    const favStoreTxt = this.getFavStoreText();
    const welcomeMessage = this.getWelcomeMessageText();
    // when the search bar is showed to the user then search icon will not be visible
    const isSearchIconShow = showSearch ? !showSearch : showSearchIcon;
    if (routeName === 'HomeStack') return null;
    return (
      <SafeAreaView style={headerStyle} forceInset={{top: 'always', bottom: 'never'}}>
        <SafeAreaViewStyle>
          <ToastContainer />
          <Container>
            <HeaderContainer data-locator={getLocator('global_headerpanel')}>
              {headertype === STORE_TYPE ? (
                <BackContainer>
                  <TouchableOpacity
                    accessible
                    onPress={() => onBack(navigation)}
                    accessibilityRole="button"
                    accessibilityLabel="back button">
                    <ArrowBackIcon source={backicon} />
                  </TouchableOpacity>
                  <MiddleSection>
                    <TitleText
                      numberOfLines={1}
                      accessibilityRole="text"
                      accessibilityLabel={title}>
                      {title}
                    </TitleText>
                  </MiddleSection>
                </BackContainer>
              ) : (
                <MessageContainer>
                  <WelcomeMessageWrapper>
                    <BodyCopy
                      fontFamily="secondary"
                      fontSize="fs14"
                      textAlign="center"
                      color="black"
                      fontWeight="semibold"
                      text={welcomeMessage}
                      data-locator={getLocator('global_headerpanelwelcometext')}
                    />
                    <UserNameTextStyle numberOfLines={1}>
                      <BodyCopy
                        fontFamily="secondary"
                        fontSize="fs14"
                        textAlign="center"
                        color="black"
                        fontWeight="extrabold"
                        text={this.getLoggedInName(welcomeMessage)}
                        data-locator={getLocator('global_headerpanelwelcometext')}
                      />
                    </UserNameTextStyle>
                  </WelcomeMessageWrapper>
                  <StoreContainer onPress={this.validateIcon} accessibilityRole="button">
                    <BodyCopy
                      fontFamily="secondary"
                      fontSize="fs12"
                      textAlign="center"
                      color="text.primary"
                      fontWeight="regular"
                      text={favStoreTxt}
                      data-locator={getLocator('global_findastoretext')}
                      accessibilityText={favStoreTxt}
                      numberOfLines={1}
                      ellipsizeMode="tail"
                    />
                    {isDownIcon ? (
                      <Icon
                        source={downIcon}
                        style={ImageColor}
                        data-locator={getLocator('global_headerpanelexpandedicon')}
                      />
                    ) : (
                      <Icon
                        source={downIcon}
                        style={ImageColor}
                        data-locator={getLocator('global_headerpanelcollapsedicon')}
                      />
                    )}
                  </StoreContainer>
                </MessageContainer>
              )}

              {isSearchIconShow && (
                <SearchIconSection>
                  <Touchable accessibilityRole="button" onPress={this.openSearchProductPage}>
                    <CustomIcon
                      iconFontName={ICON_FONT_CLASS.Icomoon}
                      name={ICON_NAME.search}
                      size="fs25"
                      color="gray.900"
                    />
                  </Touchable>
                </SearchIconSection>
              )}

              <CartContainer searchIconVisible={isSearchIconShow}>
                {this.handleUserRewards(userRewards) !== 0 && isUserLoggedIn && (
                  <>
                    <RewardBadgeIcon source={rewardIcon} />
                    <RewardBadge
                      isPlcc={isPlcc}
                      spacingStyles="padding-top-XXXS padding-bottom-XXXS padding-left-XS padding-right-XS">
                      <BodyCopy
                        fontFamily="secondary"
                        fontWeight="bold"
                        fontSize="fs9"
                        text={`$${this.handleUserRewards(userRewards)} Rewards`}
                      />
                    </RewardBadge>
                  </>
                )}
                <StyledIconTouchableOpacity
                  accessibilityRole="button"
                  onPress={() => {
                    // eslint-disable-next-line react/destructuring-assignment
                    // if labels not null then click work .
                    if (labels) navigation.navigate('BagPage');
                  }}>
                  <CartIconView
                    source={cartIcon}
                    data-locator={getLocator('global_headerpanelbagicon')}
                    cartVal={cartVal}
                  />
                  <RoundCircle cartVal={cartVal}>
                    <RoundView cartVal={cartVal}>
                      <BodyCopy
                        text={cartVal}
                        color="white"
                        style={TextStyle}
                        fontSize="fs10"
                        data-locator={getLocator('global_headerpanelbagitemtext')}
                        accessibilityText="Mini bag with count"
                        fontWeight="extrabold"
                      />
                    </RoundView>
                  </RoundCircle>
                </StyledIconTouchableOpacity>
              </CartContainer>
            </HeaderContainer>
            {this.renderSearchBar()}
          </Container>
        </SafeAreaViewStyle>
      </SafeAreaView>
    );
  }
}

Header.propTypes = {
  cartVal: number.isRequired,
  favStore: shape({
    basicInfo: shape({}),
  }),
  headertype: string,
  isNewReDesignProductTile: bool,
  isOpenAddedToBag: bool,
  isPlcc: bool,
  isUpdateCartCount: bool,
  isUserLoggedIn: bool,
  labels: objectOf(shape({})).isRequired,
  navigation: shape({navigate: func}),
  resetSuggestedStores: func,
  screenProps: shape({network: shape({isConnected: func})}),
  showSearch: bool,
  showSearchIcon: bool,
  slpLabels: shape({}),
  storeLocatorLabels: shape({}),
  title: string,
  toastMessage: func,
  totalItems: number,
  updateCartCountAction: func,
  updateCartManuallyAction: func,
  userName: string,
  userRewards: string.isRequired,
};

Header.defaultProps = {
  favStore: {},
  showSearch: false,
  title: '',
  isUpdateCartCount: false,
  updateCartManuallyAction: () => {},
  isUserLoggedIn: false,
  updateCartCountAction: () => {},
  navigation: {},
  slpLabels: {},
  userName: '',
  headertype: '',
  screenProps: {},
  toastMessage: () => {},
  resetSuggestedStores: () => {},
  showSearchIcon: false,
  storeLocatorLabels: {},
  isOpenAddedToBag: false,
  totalItems: 0,
  isNewReDesignProductTile: false,
  isPlcc: false,
};

const mapStateToProps = (state) => {
  return {
    labels: state.Labels.global && state.Labels.global.header,
    favStore: state.User && state.User.get('defaultStore'),
    cartVal: state.Header && state.Header.cartItemCount,
    isUpdateCartCount: state.Header && state.Header.updateCartCount,
    isUserLoggedIn: getUserLoggedInState(state),
    userName: getUserName(state),
    slpLabels: state.Labels.SLPs && state.Labels.SLPs.SLPs_Sub,
    storeLocatorLabels: state.Labels.StoreLocator && state.Labels.StoreLocator.StoreLanding,
    isOpenAddedToBag: getIsOpenAddedToBag(state),
    totalItems: BAGPAGE_SELECTORS.getTotalItems(state),
    userRewards: getTotalRewardsState(state),
    isNewReDesignProductTile: getIsNewReDesignEnabled(state),
    isPlcc: isPlccUser(state),
  };
};

export const mapDispatchToProps = (dispatch) => {
  return {
    updateCartCountAction: (payload) => {
      dispatch(updateCartCount(payload));
    },
    updateCartManuallyAction: (payload) => {
      dispatch(updateCartManually(payload));
    },
    toastMessage: (payload) => {
      dispatch(toastMessageInfo(payload));
    },
    resetSuggestedStores: (payload) => dispatch(resetStoresByCoordinates(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(InitialPropsHOC(Header));
export {Header as HeaderVanilla};
