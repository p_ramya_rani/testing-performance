// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import {bool, func, objectOf, number, shape, string} from 'prop-types';
import {connect} from 'react-redux';
import DeviceInfo from 'react-native-device-info';

import {BodyCopy} from '@tcp/core/src/components/common/atoms';
import {getLocator, navigateToNestedRoute, isMobileApp} from '@tcp/core/src/utils';
import InitialPropsHOC from '@tcp/core/src/components/common/hoc/InitialPropsHOC/InitialPropsHOC.native';
import BAGPAGE_SELECTORS from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.selectors';
import {
  updateCartCount,
  updateCartManually,
} from '@tcp/core/src/components/common/organisms/Header/container/Header.actions';
import {
  getUserLoggedInState,
  getUserName,
  isPlccUser,
} from '@tcp/core/src/components/features/account/User/container/User.selectors';
import {SearchBar} from '@tcp/core/src/components/common/molecules';
import SearchProduct from '@tcp/core/src/components/common/organisms/SearchProduct';
import {readCookie} from '@tcp/core/src/utils/cookie.util';
import {getValueFromAsyncStorage, dayChange} from '@tcp/core/src/utils/utils.app';
import {trackForterAction, ForterActionType} from '@tcp/core/src/utils/forter.util';
import {isOpenAddedToBag as getIsOpenAddedToBag} from '@tcp/core/src/components/features/CnC/AddedToBag/container/AddedToBag.selectors';
import {getIsNewReDesignEnabled} from '@tcp/core/src/reduxStore/selectors/session.selectors';

import {
  CartIconView,
  SearchContainer,
  StyledIconTouchableOpacity,
  HomePageHeaderContainer,
  WelcomeTextStyle,
  TitleWrapper,
  TitleTextStyle,
  SearchBarContainer,
} from './Header.style';
import {CartIconSection, CartCountContainer} from './HeaderNew.style';

const CART_ITEM_COUNTER = 'cartItemsCount';

/**
 * This component creates Mobile Header
 * @param {*} props Props passed from Header screen
 */

const cartIcon = require('../../../../assets/images/empty-bag.png');

/**
 * This component creates Mobile Header.
 * 1. To Navigate the cart page & show cart quantity
 * 2. To show the welcome text for guest user
 *     and shoe the name fro register user
 */
class HomeNewHeader extends React.PureComponent {
  /**
   * To manage the state of icons on the
   * basis of expand & collaps .
   */
  constructor(props) {
    super(props);
    this.state = {
      showSearchModal: false,
      storedCounter: 0,
    };
  }

  componentDidMount() {
    dayChange().then(() => {
      this.getCounter();
    });
  }

  componentDidUpdate(prevProps) {
    const {
      isUpdateCartCount,
      updateCartManuallyAction,
      isUserLoggedIn,
      isOpenAddedToBag,
      totalItems,
      cartVal,
    } = this.props;
    dayChange().then(() => {
      this.getCounter();
    });
    if (
      isUpdateCartCount !== prevProps.isUpdateCartCount ||
      isUserLoggedIn !== prevProps.isUserLoggedIn ||
      totalItems !== cartVal ||
      (isOpenAddedToBag !== prevProps.isOpenAddedToBag && isOpenAddedToBag === true)
    ) {
      this.getInitialProps();
      updateCartManuallyAction(false);
    }
  }

  getInitialProps() {
    const {updateCartCountAction, totalItems} = this.props;
    if (totalItems) {
      updateCartCountAction(totalItems);
    } else {
      const cartValuePromise = readCookie(CART_ITEM_COUNTER);
      cartValuePromise.then((res) => {
        updateCartCountAction(parseInt(res || 0, 10));
      });
    }
  }

  getCounter = async () => {
    const storedCounter = await getValueFromAsyncStorage('COUNTER');
    this.setState({storedCounter});
  };

  /**
   * @function openSearchProductPage
   * opens search product modal
   *
   * @memberof Header
   */
  openSearchProductPage = () => {
    this.setState({showSearchModal: true});
  };

  /**
   * @function closeSearchProductPage
   * closes search product modal
   *
   * @memberof Header
   */
  closeSearchProductPage = () => {
    this.setState({showSearchModal: false});
  };

  /**
   * @function goToSearchResultsPage
   * navigates to search results page
   *
   * @memberof Header
   */
  goToSearchResultsPage = (searchText, typeahead) => {
    this.closeSearchProductPage();

    const {navigation, isNewReDesignProductTile} = this.props;

    if (isMobileApp()) {
      trackForterAction(ForterActionType.SEARCH_QUERY, searchText);
    }

    navigateToNestedRoute(navigation, 'HomeStack', 'SearchDetail', {
      title: searchText,
      isForceUpdate: true,
      searchedType: typeahead ? 'typeahead:keyword' : 'keyword',
      newDesignColor: isNewReDesignProductTile,
    });
  };

  renderBagIcon = () => {
    const {cartVal, navigation} = this.props;
    return (
      <CartIconSection
        isFromHomePage={true}
        searchIconVisible={false}
        newPdpDesignFlag={false}
        showSearchBarInline={true}>
        <StyledIconTouchableOpacity
          accessibilityRole="button"
          newPdpDesignFlag={false}
          onPress={() => {
            navigation.navigate('BagPage');
          }}
          hitSlop={{top: 40, right: 40, bottom: 40, left: 10}}>
          <CartIconView
            isFromHomePage={true}
            source={cartIcon}
            data-locator={getLocator('global_headerpanelbagicon')}
            cartVal={cartVal}
          />
          <CartCountContainer cartVal={cartVal}>
            <BodyCopy
              text={cartVal}
              color="white"
              fontSize="fs10"
              data-locator={getLocator('global_headerpanelbagitemtext')}
              accessibilityText="Mini bag with count"
              fontWeight="extrabold"
            />
          </CartCountContainer>
        </StyledIconTouchableOpacity>
      </CartIconSection>
    );
  };

  renderSearchBar = () => {
    const {showSearch, slpLabels, navigation} = this.props;
    const {showSearchModal} = this.state;
    return (
      <SearchContainer>
        <SearchBar
          openSearchProductPage={this.openSearchProductPage}
          navigation={navigation}
          labels={slpLabels}
          isFromHomePage={true}
        />
        {showSearch && (
          <SearchBar
            openSearchProductPage={this.openSearchProductPage}
            navigation={navigation}
            labels={slpLabels}
          />
        )}
        {showSearchModal && (
          <SearchProduct
            closeSearchModal={this.closeSearchProductPage}
            goToSearchResultsPage={this.goToSearchResultsPage}
            navigation={navigation}
          />
        )}
      </SearchContainer>
    );
  };

  getUserName = (welcomeMessage) => {
    const {isUserLoggedIn, userName} = this.props;
    let updatedName = '';
    if (welcomeMessage === 'Ready to shop, ') {
      updatedName = isUserLoggedIn ? `${userName}?` : '';
    } else {
      updatedName = isUserLoggedIn ? `${userName}!` : '';
    }
    return updatedName;
  };

  getNewWelcomeMessage = (messageString, storedCounter) => {
    if (storedCounter === 0) {
      return '';
    }
    const messageArray = messageString.split('|');
    return messageArray[storedCounter - 1];
  };

  getWelcomeMessageText = () => {
    const {labels, isUserLoggedIn} = this.props;
    const {storedCounter} = this.state;
    let welcomeMessage = '';
    if (labels && labels.lbl_header_welcomeTxt) {
      welcomeMessage = this.getNewWelcomeMessage(labels.lbl_header_welcomeTxt, storedCounter);
    }
    if (welcomeMessage === '') {
      return welcomeMessage;
    }
    if (welcomeMessage === 'Ready to shop') {
      return isUserLoggedIn ? `${welcomeMessage}, ` : `${welcomeMessage}?`;
    }

    return isUserLoggedIn ? `${welcomeMessage}, ` : `${welcomeMessage}!`;
  };

  render() {
    const {stickyHeader} = this.props;
    const welcomeMessage = this.getWelcomeMessageText();
    // when the search bar is showed to the user then search icon will not be visible
    return (
      <HomePageHeaderContainer stickyHeader={stickyHeader} hasNotch={DeviceInfo.hasNotch()}>
        {!stickyHeader ? (
          <TitleWrapper>
            <WelcomeTextStyle>{welcomeMessage}</WelcomeTextStyle>
            <TitleTextStyle numberOfLines={1}>{this.getUserName(welcomeMessage)}</TitleTextStyle>
          </TitleWrapper>
        ) : null}
        <SearchBarContainer stickyHeader={stickyHeader}>
          {this.renderSearchBar()}
          {this.renderBagIcon()}
        </SearchBarContainer>
      </HomePageHeaderContainer>
    );
  }
}

HomeNewHeader.propTypes = {
  cartVal: number.isRequired,
  isNewReDesignProductTile: bool,
  isOpenAddedToBag: bool,
  isPlcc: bool,
  isUpdateCartCount: bool,
  isUserLoggedIn: bool,
  labels: objectOf(shape({})).isRequired,
  navigation: shape({navigate: func}),
  screenProps: shape({network: shape({isConnected: func})}),
  showSearch: bool,
  showSearchIcon: bool,
  slpLabels: shape({}),
  title: string,
  totalItems: number,
  updateCartCountAction: func,
  updateCartManuallyAction: func,
  userName: string,
  stickyHeader: string,
};

HomeNewHeader.defaultProps = {
  showSearch: false,
  title: '',
  isUpdateCartCount: false,
  updateCartManuallyAction: () => {},
  isUserLoggedIn: false,
  updateCartCountAction: () => {},
  navigation: {},
  slpLabels: {},
  userName: '',
  screenProps: {},
  showSearchIcon: false,
  isOpenAddedToBag: false,
  totalItems: 0,
  isNewReDesignProductTile: false,
  isPlcc: false,
  stickyHeader: false,
};

const mapStateToProps = (state) => {
  return {
    labels: state.Labels.global && state.Labels.global.header,
    cartVal: state.Header && state.Header.cartItemCount,
    isUpdateCartCount: state.Header && state.Header.updateCartCount,
    isUserLoggedIn: getUserLoggedInState(state),
    userName: getUserName(state),
    slpLabels: state.Labels.SLPs && state.Labels.SLPs.SLPs_Sub,
    isOpenAddedToBag: getIsOpenAddedToBag(state),
    totalItems: BAGPAGE_SELECTORS.getTotalItems(state),
    isNewReDesignProductTile: getIsNewReDesignEnabled(state),
    isPlcc: isPlccUser(state),
  };
};

export const mapDispatchToProps = (dispatch) => {
  return {
    updateCartCountAction: (payload) => {
      dispatch(updateCartCount(payload));
    },
    updateCartManuallyAction: (payload) => {
      dispatch(updateCartManually(payload));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(InitialPropsHOC(HomeNewHeader));
export {HomeNewHeader as HomeNewHeaderVanilla};
