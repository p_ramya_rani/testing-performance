// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

export const BagPageBackContainer = styled.TouchableOpacity`
  top: ${props => props.theme.spacing.ELEM_SPACING.MED};
  left: ${props => props.theme.spacing.ELEM_SPACING.XXS};
  position: absolute;
  z-index: ${props => props.theme.zindex.zOverlay};
`;

export default {
  BagPageBackContainer,
};
