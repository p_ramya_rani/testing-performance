// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable complexity */
import React from 'react';
import {arrayOf, bool, func, number, objectOf, shape, string} from 'prop-types';
import {connect} from 'react-redux';
import {
  navigateToNestedRoute,
  resetNavigationStack,
  isGymboree,
  getLabelValue,
} from '@tcp/core/src/utils';

import {trackClick, setClickAnalyticsData} from '@tcp/core/src/analytics/actions';

import {getIsNewReDesignEnabled} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import style from './NavBar.style';
import SecondAppPeekABooView from '../../../../navigation/SecondAppPeekABooView';
import {THEME_WRAPPER_REDUCER_KEY} from '../../hoc/ThemeWrapper.constants';

const ORDER_DETAILS_PAGE = 'OrderDetailPage';
const PRODUCT_DETAIL_PAGE = 'ProductDetail';
const PRODUCT_DETAIL_PAGE_SBP = 'ProductDetailSBP';
const PRODUCT_BUNDLE_PAGE = 'BundleDetail';
/**
 * This function returns default label for specific route in Nav Bar
 * @param {*} label
 */
const getDefaultLabels = (label) => {
  const labels = {
    home: 'HOME',
    shop: 'SHOP',
    account: 'ACCOUNT',
    wallet: 'WALLET',
    brand_logo: '',
  };
  return labels[label];
};

/**
 *
 * @param {*} props
 */
const getTestID = (route) => {
  const testIDs = {
    home: 'HOME',
    shop: 'SHOP',
    account: 'ACCOUNT',
    wallet: 'WALLET',
    brand_logo: '',
  };
  return testIDs[route];
};

const getIsOrderDetailsPage = (routes, activeRouteIndex) => {
  try {
    const activeStack = routes[activeRouteIndex];
    const currentPage = activeStack.routes[activeStack.routes.length - 1].routeName;
    return currentPage === ORDER_DETAILS_PAGE;
  } catch (e) {
    return false;
  }
};

const getIsProductDetailPage = (routes, activeRouteIndex, isNewReDesignEnabled) => {
  try {
    if (!isNewReDesignEnabled) return false;
    const activeStack = routes[activeRouteIndex];
    const currentPage = activeStack.routes[activeStack.routes.length - 1].routeName;
    return currentPage === PRODUCT_DETAIL_PAGE || currentPage === PRODUCT_DETAIL_PAGE_SBP;
  } catch (e) {
    return false;
  }
};

const getIsProductBundlePage = (routes, activeRouteIndex) => {
  try {
    const activeStack = routes[activeRouteIndex];
    const currentPage = activeStack.routes[activeStack.routes.length - 1].routeName;
    return currentPage === PRODUCT_BUNDLE_PAGE;
  } catch (e) {
    return false;
  }
};

const checkSBPCondition = (routes, activeRouteIndex) => {
  return (
    activeRouteIndex === 1 &&
    routes[1].routes &&
    routes[1].routes[1] &&
    routes[1].routes[1].params &&
    routes[1].routes[1].params.isSBP
  );
};

/**
 * This Component creates custom Bottom Nav Bar for the app
 * @param {*} props Props passed from BottomTabNavigator react native feature
 */
class NavBar extends React.PureComponent {
  /* eslint-disable-next-line */
  UNSAFE_componentWillReceiveProps(nextProps) {
    const {appType: prevAppType} = this.props;
    const {appType} = nextProps;

    if (appType !== prevAppType && prevAppType) {
      const {navigation} = this.props;
      // reset home stack
      resetNavigationStack(navigation);
      // On brand switch, Reset all stacks and navigate to home page of each stack
      // navigate to home page of Home Stack at the end
      navigateToNestedRoute(navigation, 'PlpStack', 'Navigation');
      navigateToNestedRoute(navigation, 'AccountStack', 'Account');
      navigateToNestedRoute(navigation, 'WalletStack', 'walletPage');
      navigateToNestedRoute(navigation, 'HomeStack', 'Home');
    }
  }

  getFooterStacks = () => {
    const {
      renderIcon,
      getLabelText,
      onTabPress,
      navigation,
      labels,
      screenProps,
      trackClickAction,
      setClickAnalyticsDataAction,
    } = this.props;

    const {showBrands, toggleBrandAction, checkFirst} = screenProps;
    const {routes, index: activeRouteIndex} = navigation.state;
    const name = 'Footer_Tab_Clicks_E158';
    const module = 'global';
    const page = 'home page';
    const commonVariables = {
      pageName: page,
      pageType: page,
      pageSection: page,
      pageSubSection: page,
      pageSubSubSection: page,
      pageShortName: page,
    };
    const footerTabAnalyticsData = {
      ...commonVariables,
      pageNavigationText: 'footer',
      customEvents: 'Event158',
    };

    /* eslint-disable sonarjs/cognitive-complexity */
    return routes.map((route, routeIndex) => {
      let isRouteActive = routeIndex === activeRouteIndex;
      let StyledTouchableOpacity = style.tabButton;
      let StyledText = style.textStyle;
      const routeId = getLabelText({route});
      const label = (labels && labels[routeId]) || getDefaultLabels(routeId);

      let accessibilityLabel = `Inactive ${label}`;
      let accessibilityState;

      if (routeId === 'brand_logo') {
        accessibilityLabel = isGymboree() ? labels.gymBrandName : labels.tcpBrandName;
        accessibilityLabel = isRouteActive
          ? `${accessibilityLabel} ${labels.brandSwitch}`
          : accessibilityLabel;
        accessibilityState = {
          expanded: showBrands,
        };
      }

      const contextData = {
        adobe: {eVar151: label},
        google: {Footer_Tab_Name_V151: label},
      };

      if (isRouteActive) {
        const {highlightedTextStyleGYM, highlightedTextStyle} = style;
        StyledText = isGymboree() ? highlightedTextStyleGYM : highlightedTextStyle;
        accessibilityLabel = `Active ${label}`;
      }

      if (!label) {
        StyledTouchableOpacity = style.logoStyle;
      }
      if (route.key === 'BrandSwitchStack') {
        isRouteActive = showBrands;
      }
      if (route.key === 'SbpPlpStack') {
        return null;
      }
      if (route.key === 'CustomerHelpStack') {
        return null;
      }

      return (
        <StyledTouchableOpacity
          // eslint-disable-next-line react/no-array-index-key
          key={`nav-bar_${routeIndex}`}
          onPress={() => {
            if (route.key === 'BrandSwitchStack') {
              // show brands switch as an option in view
              if (toggleBrandAction) {
                toggleBrandAction(!showBrands);
              }
              return;
            }

            setClickAnalyticsDataAction(footerTabAnalyticsData);
            trackClickAction({name, module, contextData});
            setClickAnalyticsDataAction({});
            onTabPress({route});
            checkFirst(route.key);
          }}
          accessibilityRole="button"
          accessible
          accessibilityLabel={accessibilityLabel}
          accessibilityState={accessibilityState}
          testID={getTestID(routeId)}
          activeOpacity={1}>
          {renderIcon({route, focused: isRouteActive})}

          <StyledText>{label}</StyledText>
        </StyledTouchableOpacity>
      );
    });
  };

  render() {
    const {navigation, screenProps, isNewReDesignEnabled} = this.props;
    const {showBrands, showPeekABooAnimation, togglePeekAction} = screenProps;
    const {routes, index: activeRouteIndex} = navigation.state;
    const StyledView = style.container;
    const NavContainer = style.navContainer;
    if (getIsOrderDetailsPage(routes, activeRouteIndex)) {
      return null;
    }
    if (checkSBPCondition(routes, activeRouteIndex)) return null;
    if (getIsProductDetailPage(routes, activeRouteIndex, isNewReDesignEnabled)) return null;
    if (getIsProductBundlePage(routes, activeRouteIndex)) return null;
    return (
      <NavContainer>
        <StyledView>{this.getFooterStacks()}</StyledView>
        <SecondAppPeekABooView
          showPeekABooAnimation={showPeekABooAnimation}
          showLogoAnimation={showBrands}
          togglePeekAction={togglePeekAction}
        />
      </NavContainer>
    );
  }
}

NavBar.propTypes = {
  appType: string,
  getLabelText: func,
  isNewReDesignEnabled: bool.isRequired,
  labels: shape({}),
  navigation: shape({
    state: shape({
      index: number,
      routes: arrayOf({}),
    }),
  }),
  onTabPress: func,
  renderIcon: func,
  screenProps: objectOf(shape({})),
  setClickAnalyticsDataAction: func,
  trackClickAction: func,
};

NavBar.defaultProps = {
  renderIcon: () => {},
  getLabelText: () => {},
  onTabPress: () => {},
  trackClickAction: () => {},
  setClickAnalyticsDataAction: () => {},
  navigation: {},
  labels: {},
  appType: '',
  screenProps: {},
};

const mapStateToProps = (state) => {
  return {
    labels: {
      ...((state.Labels.MobileApp && state.Labels.MobileApp.navigation) || {}),
      gymBrandName: getLabelValue(
        state.Labels,
        'lbl_brandlogo_gymAltText',
        'accessibility',
        'global',
      ),
      tcpBrandName: getLabelValue(
        state.Labels,
        'lbl_brandlogo_tcpAltText',
        'accessibility',
        'global',
      ),
      brandSwitch: getLabelValue(state.Labels, 'lbl_brand_switch', 'accessibility', 'global'),
      close: getLabelValue(state.Labels, 'closeIconButton', 'accessibility', 'global'),
    },
    appType: state[THEME_WRAPPER_REDUCER_KEY].get('APP_TYPE'),
    isNewReDesignEnabled: getIsNewReDesignEnabled(state),
  };
};

export const mapDispatchToProps = (dispatch) => {
  return {
    trackClickAction: (data) => {
      dispatch(trackClick(data));
    },
    setClickAnalyticsDataAction: (data) => {
      dispatch(setClickAnalyticsData(data));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NavBar);
