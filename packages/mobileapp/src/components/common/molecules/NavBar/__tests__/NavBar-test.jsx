// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import {shallow} from 'enzyme';
import NavBar, {mapDispatchToProps} from '../NavBar';

describe('NavBar Component', () => {
  let component;
  beforeEach(() => {
    const props = {
      isNewReDesignEnabled: false,
      getLabelText: () => {},
      navigation: {
        navigate: jest.fn(),
        state: {
          index: 0,
          routes: [],
        },
      },
      onTabPress: () => {},
      renderIcon: () => {},
      setClickAnalyticsDataAction: () => {},
      trackClickAction: () => {},
      screenProps: {
        showBrands: false,
        showPeekABooAnimation: false,
        togglePeekAction: () => {},
      },
    };
    component = shallow(<NavBar {...props} />);
  });

  it('NavBar should be defined', () => {
    expect(component).toBeDefined();
  });

  it('NavBar should render correctly', () => {
    expect(component).toMatchSnapshot();
  });

  it('should return an trackClickAction  which will call dispatch function on execution', () => {
    const dispatch = jest.fn();
    const dispatchProps = mapDispatchToProps(dispatch);
    dispatchProps.trackClickAction();
    expect(dispatch.mock.calls).toHaveLength(1);
  });

  it('should return an setClickAnalyticsDataAction  which will call dispatch function on execution', () => {
    const dispatch = jest.fn();
    const dispatchProps = mapDispatchToProps(dispatch);
    dispatchProps.setClickAnalyticsDataAction();
    expect(dispatch.mock.calls).toHaveLength(1);
  });
});
