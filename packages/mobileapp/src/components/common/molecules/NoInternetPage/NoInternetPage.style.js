// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

export const SafeAreaViewStyle = styled.SafeAreaView`
  flex: 1;
`;

export const Container = styled.View`
  height: 100%;
  align-items: center;
  justify-content: center;
`;

export const TextConatiner = styled.View`
  margin-bottom: 40px;
`;
