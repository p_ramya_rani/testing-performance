// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import renderer from 'react-test-renderer';
import {ApplePayPromoVanilla} from '../ApplePayPromo';

describe('Component', () => {
  let component;

  beforeEach(() => {
    const props = {
      navigation: jest.fn(),
      labels: {},
    };
    component = renderer.create(<ApplePayPromoVanilla {...props} />).toJSON();
  });

  it('Component should be defined', () => {
    expect(component).toBeDefined();
  });

  it('Component should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
});
