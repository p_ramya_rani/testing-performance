import styled from 'styled-components/native';

export const ImageTouchableOpacity = styled.TouchableOpacity`
  align-items: center;
  width: 100%;
  margin-top: 3px;
  margin-bottom: 5px;
`;

export default {
  ImageTouchableOpacity,
};
