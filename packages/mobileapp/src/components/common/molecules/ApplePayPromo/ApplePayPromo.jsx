import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {ImageBackground, Dimensions} from 'react-native';
import {cropImageUrl} from '@tcp/core/src/utils/utils.app';
import {isTCP} from '@tcp/core/src/utils/utils';
import Modal from '@tcp/core/src/components/common/molecules/Modal/index';
import InAppWebView from '../../../features/content/InAppWebView/views/InAppWebView.view';
import {ImageTouchableOpacity} from './ApplePayPromo.style';

const ApplePayPromo = (props) => {
  const [showApplePayModal, setApplePayModal] = useState(false);
  const {labels} = props;
  const getFallbackRedirectUrl = () =>
    isTCP()
      ? 'https://www.childrensplace.com/us/content/applepay-promo'
      : 'https://www.gymboree.com/us/content/applepay-promo';

  const redirectUrl = labels?.lbl_promo_url || getFallbackRedirectUrl();

  const promoImageUrl =
    labels?.lbl_promo_img_url || '/ecom/assets/content/tcp/us/homepage/apple_pay_mobile_app.png';

  const imageURI = {
    uri: cropImageUrl(promoImageUrl),
  };
  const {width} = Dimensions.get('window');
  const imgHeight = 50;
  const styles = {
    width: width - 28,
    height: imgHeight,
  };
  const imageStyles = {
    borderRadius: 10,
  };

  const toggleModal = () => {
    setApplePayModal(!showApplePayModal);
  };

  const {navigation} = props;

  return (
    <>
      <ImageTouchableOpacity onPress={toggleModal}>
        <ImageBackground
          source={imageURI}
          style={styles}
          accessibilityLabel="Apple Pay Promo"
          imageStyle={imageStyles}
          resizeMode="contain"
        />
      </ImageTouchableOpacity>

      <Modal
        isOpen={showApplePayModal}
        overlayClassName="TCPModal__Overlay"
        className="TCPModal__Content"
        fixedWidth
        fullWidth
        onRequestClose={toggleModal}
        headingAlign="center"
        heading
        stickyHeader
        stickyCloseIcon
        horizontalBar={false}>
        <InAppWebView
          pageUrl={redirectUrl}
          navigation={navigation}
          isBTSWebView
          callback={toggleModal}
        />
      </Modal>
    </>
  );
};

ApplePayPromo.propTypes = {
  navigation: PropTypes.shape({}).isRequired,
  labels: PropTypes.shape({}).isRequired,
};

export default ApplePayPromo;
export {ApplePayPromo as ApplePayPromoVanilla};
