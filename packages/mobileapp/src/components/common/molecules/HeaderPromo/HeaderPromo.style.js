// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';
import colors from '@tcp/core/styles/themes/TCP/colors';

export const HeaderPromoContainer = styled.View`
  height: 45px;
  align-items: center;
  justify-content: center;
  background: ${(props) => props.theme.colors.PRIMARY.PALEGRAY};
`;

export const MessageContainer = styled.View`
  flex-direction: column;
  height: 100%;
  align-items: center;
  justify-content: center;
`;

export const Container = styled.View`
  align-items: center;
  justify-content: center;
  flex-direction: row;
  margin: 0 10px 0 10px;
  border-radius: 10px;
  overflow: hidden;
`;

/* TODO - To use the style1, style2, style3 when the styles start coming up from CMS */

export const TextStyle = {
  color: colors.WHITE,
  marginRight: 5,
  bgColor: colors.BLACK,
};
export const TextStyle1 = {
  color: colors.BLACK,
  marginRight: 5,
  bgColor: colors.NAVIGATION.GREEN,
};
export const TextStyle2 = {
  color: colors.BLACK,
  marginRight: 5,
  bgColor: colors.NAVIGATION.YELLOW,
};

export default {
  HeaderPromoContainer,
  MessageContainer,
  TextStyle,
  TextStyle1,
  TextStyle2,
  Container,
};
