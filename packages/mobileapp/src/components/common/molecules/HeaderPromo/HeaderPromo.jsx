// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { ImageBackground } from 'react-native';
import PropTypes from 'prop-types';
import { BodyCopy, Anchor } from '@tcp/core/src/components/common/atoms';
import Carousel from '@tcp/core/src/components/common/molecules/Carousel';
import { getScreenWidth, getLocator } from '@tcp/core/src/utils';
import Espot from '@tcp/core/src/components/common/molecules/Espot';
import get from 'lodash/get';
import {
  MessageContainer,
  TextStyle,
  TextStyle1,
  TextStyle2,
  Container,
  HeaderPromoContainer,
} from './HeaderPromo.style';

/**
 * Module height and width.
 * Height is fixed for mobile
 * Width can vary as per device width.
 */
const MODULE_HEIGHT = 30;
const MODULE_WIDTH = getScreenWidth() + 64;
/**
 * To manage the TextStyle on the basis of CMS
 */
const manageTextStyles = style => {
  if (style === 'style11') {
    return TextStyle1;
  }
  if (style === 'style12') {
    return TextStyle2;
  }
  if (style === 'style13') {
    return {};
  }
  return TextStyle;
};

const getBgColor = style => {
  return { backgroundColor: manageTextStyles(style).bgColor };
};
/**
 * This Component return the mobile Promo Banner
 */
class HeaderPromo extends React.PureComponent {
  renderInnerView = item => {
    const { navigation } = this.props;
    return (
      <Anchor navigation={navigation} url={item.linkClass.url} width={MODULE_WIDTH}>
        <MessageContainer style={getBgColor(item.textItems[0].style)}>
          {item.textItems[0] && (
            <BodyCopy
              fontFamily="secondary"
              fontSize="fs12"
              textAlign="center"
              fontWeight="black"
              text={item.textItems[0].text}
              style={manageTextStyles(item.textItems[0].style)}
              data-locator={getLocator('global_promobanner_title_0')}
            />
          )}
          {item.textItems[1] && (
            <BodyCopy
              fontFamily="secondary"
              fontSize="fs12"
              textAlign="center"
              style={manageTextStyles(item.textItems[0].style)}
              fontWeight="regular"
              text={item.textItems[1].text}
              data-locator={getLocator('global_promobanner_title_1')}
            />
          )}
        </MessageContainer>
      </Anchor>
    );
  };

  /**
   * @desc Returns updated Banner text details with styles.
   * Content render on the basis of style type .
   */
  renderView = ({ item }) => {
    const { headerBgImage } = this.props;
    const imageStyle = { width: '100%' };
    return (
      item.textItems &&
      item.textItems.length &&
      (item.textItems[0].style === 'style13' ? (
        <ImageBackground style={imageStyle} source={{ uri: headerBgImage }}>
          {this.renderInnerView(item)}
        </ImageBackground>
      ) : (
        this.renderInnerView(item)
      ))
    );
  };

  render() {
    const { headerPromo, promoHtmlBannerCarousel, navigation, addBackNavigation } = this.props;
    const routeName = addBackNavigation ? get(navigation, 'state.routeName', false) : '';
    if (headerPromo) {
      return (
        <HeaderPromoContainer>
          <Carousel
            data={headerPromo}
            renderItem={this.renderView}
            height={MODULE_HEIGHT}
            width={MODULE_WIDTH}
            variation="show-arrow"
            carouselConfig={{
              autoplay: true,
            }}
            isHeaderPromo
          />
        </HeaderPromoContainer>
      );
    }

    return (
      <Container>
        {promoHtmlBannerCarousel && (
          <Espot
            richTextHtml={promoHtmlBannerCarousel && promoHtmlBannerCarousel[0].text}
            navigation={navigation}
            isNativeView={false}
            routeName={routeName}
          />
        )}
      </Container>
    );
  }
}

HeaderPromo.propTypes = {
  headerPromo: PropTypes.shape({}),
  promoHtmlBannerCarousel: PropTypes.shape([]),
  headerBgImage: PropTypes.string,
  navigation: PropTypes.shape({}).isRequired,
  addBackNavigation: PropTypes.bool,
};

HeaderPromo.defaultProps = {
  headerPromo: {},
  promoHtmlBannerCarousel: [],
  headerBgImage: '',
  addBackNavigation: false,
};

export default HeaderPromo;
export { HeaderPromo as HeaderPromoVanilla };
