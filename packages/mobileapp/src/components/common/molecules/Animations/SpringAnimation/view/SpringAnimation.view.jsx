// 9fbef606107a605d69c0edbcd8029e5d
import React, {useEffect} from 'react';
import {Animated} from 'react-native';
import {PropTypes} from 'prop-types';

const SpringAnimation = (props) => {
  const {startValue, endValue, friction, children} = props;
  const springStartValue = new Animated.Value(startValue);

  useEffect(() => {
    Animated.spring(springStartValue, {
      toValue: endValue,
      friction,
      useNativeDriver: true,
    }).start();
  }, [springStartValue, endValue]);

  return (
    <Animated.View
      style={{
        transform: [
          {
            scale: springStartValue,
          },
        ],
      }}>
      {children}
    </Animated.View>
  );
};

SpringAnimation.propTypes = {
  startValue: PropTypes.number,
  endValue: PropTypes.number,
  friction: PropTypes.number,
  children: PropTypes.shape({}).isRequired,
};

SpringAnimation.defaultProps = {
  startValue: 0.8,
  friction: 6,
  endValue: 1,
};

export default SpringAnimation;
