// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';

// Usage: https://childrensplace.atlassian.net/wiki/spaces/DRP/pages/1527545886/Tab+List+Vertical

// tab styles
const TabListVertical = styled.View`
  flex-direction: row;
  flex: 1;
`;

const TabListHeaderContainer = styled.View`
  background-color: ${(props) => props.theme.colorPalette.gray[2000]};
  width: 43%;
  flex-direction: column;
`;

const TabListHeader = styled.TouchableOpacity`
  height: 50px;
  padding: 8px 0;
  ${(props) => (props.activeHeader ? 'background-color:white' : '')};
`;

const TabListBodyContainer = styled.View`
  width: 67%;
  padding: 20px 15px 0 15px;
  height: 100%;
`;

const FilterPillsListBodyContainer = styled.View`
  width: 100%;
  padding: 16px 15px 0 15px;
  height: 80%;
`;

const getActiveHeaderContentStyles = (props) => {
  return `
    padding-left: 8px;
    border-left-width: 6px;
    border-style : solid;
    border-left-color : ${props.theme.colorPalette.primary.main};
  `;
};

const TabListHeaderContent = styled.View`
  flex: 1;
  padding: 0 15px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  ${(props) => (props.activeHeader ? getActiveHeaderContentStyles : '')};
`;

const TabListBodyContent = styled.View`
  display: ${(props) => (props.showTab ? 'flex' : 'none')};
`;

export {
  TabListVertical,
  TabListHeaderContainer,
  TabListHeader,
  TabListBodyContainer,
  TabListHeaderContent,
  TabListBodyContent,
  FilterPillsListBodyContainer,
};
