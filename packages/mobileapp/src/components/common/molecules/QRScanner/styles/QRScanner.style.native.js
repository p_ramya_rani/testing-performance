// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

export const HelpText = styled.Text`
  margin-top: 24px;
  height: 22px;
  text-align: center;
  color: ${props => props.theme.colorPalette.white};
  justify-content: center;
  width: ${props => props.width}px;
`;

export const BackContainer = styled.View`
  flex-direction: row;
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.XS};
  width: 60%;
`;

export const Container = styled.View`
  position: absolute;
  top: ${props => props.top};
  align-items: center;
  justify-content: center;
  width: ${props => props.width}px;
`;
