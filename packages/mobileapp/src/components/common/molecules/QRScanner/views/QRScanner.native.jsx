// 9fbef606107a605d69c0edbcd8029e5d 
import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import {connect} from 'react-redux';
import QRCodeScanner from 'react-native-qrcode-scanner';
import withStyles from '@tcp/core/src/components/common/hoc/withStyles';
import {Modal} from 'react-native';
import {getScreenWidth, isAndroid} from '@tcp/core/src/utils/utils.app';
import {trackForterAction, ForterActionType} from '@tcp/core/src/utils/forter.util.native';

import BagPageHeader from '../../Header/BagPageHeader';
import ScanErrorModal from '../../scanErrorModal';
import Style, {HelpText, Container} from '../styles/QRScanner.style.native';

/**
 * Module height and width.
 * @Height is fixed for mobile
 * @Width can vary as per device width.
 * @isCheckAndroidPermissions used for Mobile .
 * @cameraStyle can vary as per device.
 * @markerStyle is fixed for mobile.
 * @DEVICE_WIDTH is fixed for mobile.
 */

const isCheckAndroidPermissions = true;
const cameraStyle = {
  width: '100%',
  height: '100%',
};
const markerStyle = {borderColor: '#1ba5e0'};
const DEVICE_WIDTH = getScreenWidth();

class QRCode extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isQRNotReadable: false,
      isQRActive: true,
    };
    this.scanner = '';
  }

  /**
   * It redirects to PLP page once camera detected QR.
   * If there is no readable content in QR then it will not redirect to PLP
   * and open error modal
   *
   */
  redirectToPLP = data => {
    const qrValidatorString = '/scan-it/';
    if (data && data.match(qrValidatorString)) {
      const {navigation} = this.props;
      navigation.navigate('ProductListing', {
        url: `/c?cid=${data.replace(qrValidatorString, '')}`,
        showCustomLoader: true,
      });
      trackForterAction(ForterActionType.APP_ACTIVE, 'QR_SCAN');
    } else {
      this.noQRFound();
    }
  };

  /**
   * Set state to show no QR found modal
   */
  noQRFound = () => {
    this.setState({isQRNotReadable: true, isQRActive: false});
  };

  /**
   * Set state to close no QR found error modal and reactive the QR scanner to scan another QR code.
   */
  closeNoQRFoundModal = () => {
    this.setState({isQRNotReadable: false, isQRActive: true}, () => this.activateQRScanner());
  };

  /**
   * Activate QR scanner programmatically because QR scanner will scan only once
   * If another QR need to scan then QR scanner need to be reactivate
   */
  activateQRScanner = () => {
    this.scanner.reactivate();
  };

  /**
   * QR scanner scanned callback.
   */
  onSuccess = e => {
    this.setState({isQRActive: false}, () => this.redirectToPLP(e && e.data));
  };

  /**
   * Render top content on QR scanner page
   */
  topContent = () => {
    const {qrLabels} = this.props;

    return (
      <Container width={DEVICE_WIDTH} top={isAndroid() ? 70 : 140}>
        <HelpText width={DEVICE_WIDTH}>{qrLabels.lbl_qrscanner_help_one}</HelpText>
        <HelpText width={DEVICE_WIDTH}>{qrLabels.lbl_qrscanner_help_two}</HelpText>
      </Container>
    );
  };

  render() {
    const {isQRNotReadable, isQRActive} = this.state;
    const {navigation, qrLabels} = this.props;
    return (
      <Modal animationType="fade" transparent={false}>
        <ScanErrorModal
          labels={qrLabels}
          isOpen={isQRNotReadable}
          navigation={navigation}
          onClose={this.closeNoQRFoundModal}
        />
        <BagPageHeader
          showGobackIcon
          navigation={navigation}
          showBrandIcon
          showCloseButton={false}
          disableBagLoading
        />
        <QRCodeScanner
          fadeIn={false}
          checkAndroid6Permissions={isCheckAndroidPermissions}
          cameraStyle={cameraStyle}
          markerStyle={markerStyle}
          ref={node => {
            this.scanner = node;
          }}
          onRead={this.onSuccess}
          reactivate={isQRActive}
          showMarker
          reactivateTimeout={2}
          notAuthorizedView={<HelpText>{qrLabels.lbl_qrscanner_not_authorized}</HelpText>}
        />
        {this.topContent()}
      </Modal>
    );
  }
}

const mapStateToProps = state => {
  return {
    qrLabels: get(state, 'Labels.global.qrScanner', {}),
  };
};

QRCode.propTypes = {
  qrLabels: PropTypes.objectOf(PropTypes.shape({})).isRequired,
  navigation: PropTypes.objectOf(PropTypes.shape({})),
};

QRCode.defaultProps = {
  navigation: {},
};

export default connect(mapStateToProps)(withStyles(QRCode, Style));
export {QRCode as QRCodeVanilla};
