// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable */
import React from 'react';
import {NativeModules} from 'react-native';
import {connect} from 'react-redux';
import {PropTypes} from 'prop-types';
import {is} from 'immutable';
import {getAllCoupons} from '@tcp/core/src/components/features/CnC/common/organism/CouponAndPromos/container/Coupon.selectors';
import {
  getUserLoggedInState,
  getUserEmail,
  isPlccUser,
} from '@tcp/core/src/components/features/account/User/container/User.selectors';
import AsyncStorage from '@react-native-community/async-storage';

export class AnalyticsContainer extends React.PureComponent {
  componentDidMount() {
    this.sendGuestUserEvents();
  }

  componentDidUpdate(prevProps) {
    const {isUserLoggedIn, email, isPLCCUser, allCouponList} = this.props;

    if (prevProps.isUserLoggedIn !== isUserLoggedIn) {
      this.sendUserEvents(isUserLoggedIn, email, isPLCCUser);
    }

    if (!is(prevProps.allCouponList, allCouponList)) {
      this.sendCouponsEvents(allCouponList);
    }
  }

  sendUserEvents = async (isUserLoggedIn, email, isPLCCUser) => {
    if (isUserLoggedIn) {
      // set UA named user to email id
      try {
        const result = await NativeModules.VibesModule.associatePerson(email);
        console.log(result);
      } catch (error) {
        console.error(error);
      }
    }
  };

  sendGuestUserEvents = async () => {
    const {isUserLoggedIn} = this.props;
    const count = '1';
    if (!isUserLoggedIn) {
      const guestShopperVisitCount = await AsyncStorage.getItem('guestShopperVisitCount');
      if (guestShopperVisitCount && guestShopperVisitCount == count) {
        AsyncStorage.removeItem('guestShopperVisitCount');
      } else {
        AsyncStorage.setItem('guestShopperVisitCount', count.toString());
      }
    }
  };

  sendCouponsEvents = (allCouponList) => {
    let expiredCoupon;
    let couponType;
    allCouponList.forEach(async (coupon, index) => {
      const dayInMilliseconds = 1000 * 60 * 60 * 24; // milliseconds in one day
      const previousTime = await AsyncStorage.getItem('previousTime');
      const currentTime = await AsyncStorage.getItem('currentTime');
      const nowTime = new Date().getTime();

      if (previousTime) {
        AsyncStorage.setItem('previousTime', currentTime);
        AsyncStorage.setItem('currentTime', nowTime.toString());
      } else {
        AsyncStorage.setItem('currentTime', nowTime.toString());
        AsyncStorage.setItem('previousTime', nowTime.toString());
      }

      // check for new coupons since last login
      if (coupon && parseInt(previousTime) < new Date(coupon.effectiveDate).getTime()) {
        AsyncStorage.setItem('previousTime', currentTime);
      }

      // check for coupons expiring within 2 days
      if (
        coupon &&
        new Date(coupon.expirationDate).getTime() - parseInt(currentTime) <= 2 * dayInMilliseconds
      ) {
        if (
          expiredCoupon &&
          new Date(expiredCoupon.expirationDate).getTime() <
            new Date(coupon.expirationDate).getTime()
        ) {
          expiredCoupon = coupon;
        } else if (
          expiredCoupon &&
          new Date(expiredCoupon.expirationDate).getTime() ===
            new Date(coupon.expirationDate).getTime()
        ) {
          if (coupon.offerType === 'Loyalty') {
            couponType = 'Loyalty';
            expiredCoupon = coupon;
          } else if (coupon.offerType === 'PLACECASH' && couponType !== 'Loyalty') {
            couponType = 'PLACECASH';
            expiredCoupon = coupon;
          } else if (coupon.offerType !== 'Loyalty' && couponType !== 'PLACECASH') {
            couponType = 'SC';
          } else {
            couponType = 'SC';
            expiredCoupon = coupon;
          }
        } else {
          expiredCoupon = coupon;
        }
      }
    });
  };

  render() {
    return null;
  }
}

export const mapStateToProps = (state) => {
  return {
    isUserLoggedIn: getUserLoggedInState(state),
    email: getUserEmail(state),
    isPLCCUser: isPlccUser(state),
    allCouponList: getAllCoupons(state),
  };
};

AnalyticsContainer.propTypes = {
  isUserLoggedIn: PropTypes.bool.isRequired,
  email: PropTypes.string.isRequired,
  isPLCCUser: PropTypes.bool.isRequired,
  allCouponList: PropTypes.shape([]).isRequired,
};

export default connect(mapStateToProps)(AnalyticsContainer);
