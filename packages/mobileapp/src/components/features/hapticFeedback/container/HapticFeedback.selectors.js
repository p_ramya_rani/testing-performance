import {getABtestFromState} from '../../../../../../core/src/utils';

const getEventType = (state) => {
  return state && state.HapticFeedback.eventType;
};

const getIsHapticFeedbackEnabled = (state) => {
  return getABtestFromState(state.AbTest).isHapticFeedbackEnabled;
};

export {getEventType, getIsHapticFeedbackEnabled};
