// 9fbef606107a605d69c0edbcd8029e5d 
import {StyleSheet} from 'react-native';
import {
  colorMortar,
  colorNero,
  colorGainsboro,
  fontFamilyNunitoRegular,
  fontFamilyNunitoSemiBold,
  fontFamilyTofinoBold,
  fontSizeFourteen,
  fontSizeTwelve,
} from '../ShopByProfile.styles';

const styles = StyleSheet.create({
  contentContainer: {
    margin: 14,
  },
  customIcon: {
    color: colorMortar,
  },
  emptyView: {
    backgroundColor: colorNero,
    height: 1,
    width: 68,
  },
  menuContainer: {
    backgroundColor: colorGainsboro,
    height: 1,
    marginBottom: 20,
  },
  menuItemButton: {
    alignItems: 'center',
    flexDirection: 'row',
    height: 50,
    marginBottom: 10,
  },
  menuItemContainer: {
    backgroundColor: colorGainsboro,
    height: 1,
    marginBottom: 15,
  },
  menuItemImg: {
    height: 20,
    marginLeft: 10,
    marginRight: 20,
    width: 20,
  },
  menuItemText: {
    color: colorMortar,
    fontFamily: fontFamilyNunitoSemiBold,
    fontSize: 13,
  },
  modalContainer: {
    flexDirection: 'row',
    height: 120,
    marginTop: 10,
    paddingHorizontal: 10,
  },
  modalContainerInner: {
    height: 85,
    justifyContent: 'center',
    marginLeft: 16,
    paddingVertical: 14,
  },
  nameText: {
    color: colorNero,
    fontFamily: fontFamilyTofinoBold,
    fontSize: fontSizeFourteen,
    fontWeight: 'bold',
    height: 30,
  },
  profileIcon: {
    elevation: 14,
    shadowOffset: {
      height: 3,
      width: 0,
    },
    shadowOpacity: 0.2,
    shadowRadius: 6.11,
  },
  profileText: {
    color: colorNero,
    fontFamily: fontFamilyNunitoRegular,
    fontSize: fontSizeTwelve,
  },
});

export default styles;
