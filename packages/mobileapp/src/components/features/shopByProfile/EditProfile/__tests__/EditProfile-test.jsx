// 9fbef606107a605d69c0edbcd8029e5d 
/* eslint-disable */
import React from 'react';
import {shallow} from 'enzyme';
import Component from '../index';

describe('Component', () => {
  let component;
  let onPressSpyDeleteProfile;
  let onPressSpyDeleteWishList;
  const profile = {
    id: 1,
    name: 'Name',
    theme: 1,
  };

  beforeEach(() => {
    onPressSpyDeleteProfile = jest.fn();
    onPressSpyDeleteWishList = jest.fn();
    const props = {
      profile,
      deleteWishList: onPressSpyDeleteWishList,
      deleteProfile: onPressSpyDeleteProfile,
      navigation: {
        navigate: jest.fn(),
        dispatch: jest.fn(),
        state: {
          params: {},
        },
      },
    };
    component = shallow(<Component {...props} />);
  });

  it('Component should be defined', () => {
    expect(component).toBeDefined();
  });

  it('Component should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
});
