// 9fbef606107a605d69c0edbcd8029e5d
import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Image, ScrollView, BackHandler} from 'react-native';
import {func, number, shape, string} from 'prop-types';
import {selectWishlistsSummaries} from '@tcp/core/src/components/features/browse/Favorites/container/Favorites.selectors';
import {deleteWishListAction} from '@tcp/core/src/components/features/browse/Favorites/container/Favorites.actions';
import {getUserId} from '@tcp/core/src/components/features/account/User/container/User.selectors';
import CustomIcon from '@tcp/core/src/components/common/atoms/Icon';
import {connect} from 'react-redux';
import {navigateToNestedRoute, setValueInAsyncStorage} from '@tcp/core/src/utils';
import {isDeviceIPhone12, showSBPLabels} from '../PLP/helper';
import ProfileIcon from '../../../common/atoms/ProfileIcon';
import Modal from '../Modal';
import {deleteCache} from '../Caching';
import styles from './styles';
import {getSBPLabels} from '../ShopByProfile.selector';
import {deleteSBPProfile} from '../ShopByProfileAPI/ShopByProfile.actions';
import {SBP_PROFILE_DELETED} from '../ShopByProfile.constants';

const trashIcon = require('../../../../assets/images/shopByProfile/elements/trash-can_2020-08-06/trash-can.png');

class EditProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showDialog: false,
    };
    BackHandler.addEventListener('hardwareBackPress', () => {
      const {navigation, profile} = props;
      navigation.navigate(
        'PLP',
        Object.assign({}, profile, {
          categoriesLoading: true,
          refresh: true,
        }),
      );
      return true;
    });
    this.closeModal = this.closeModal.bind(this);
  }

  componentWillReceiveProps(props) {
    const {profile, navigation} = props;
    if (profile && profile.theme !== navigation.state.params.theme)
      navigation.setParams(Object.assign({}, navigation.state.params, {theme: profile.theme}));
  }

  getEditMenu = () => {
    const {sbpLabels} = this.props;
    const nameAndIconLabel = showSBPLabels(
      sbpLabels,
      'lbl_sbp_name_and_icon_edit_menu',
      'Name & Icon',
    );
    const ageAndRelationLabel = showSBPLabels(
      sbpLabels,
      'lbl_sbp_age_and_relationship_edit_menu',
      'Age & Relationship',
    );
    const departmentAndSizeLabel = showSBPLabels(
      sbpLabels,
      'lbl_sbp_department_and_size_edit_menu',
      'Department & Size',
    );
    const interestsLabel = showSBPLabels(sbpLabels, 'lbl_sbp_interests_edit_menu', 'Interests');
    const favoriteColorsLabel = showSBPLabels(
      sbpLabels,
      'lbl_sbp_favorite_colors_edit_menu',
      'Favorite Colors',
    );
    return {
      [nameAndIconLabel]: 'NameInput',
      [ageAndRelationLabel]: 'AgeInput',
      [departmentAndSizeLabel]: 'GenderInput',
      [interestsLabel]: 'InterestsInput',
      [favoriteColorsLabel]: 'ColorsInput',
    };
  };

  deleteProfileLabel = () => {
    const {sbpLabels} = this.props;
    return showSBPLabels(sbpLabels, 'lbl_sbp_delete_profile_edit', 'Delete Profile');
  };

  async handlePress(text) {
    const {profile, deleteWishList, wishlistsSummaries, deleteProfile, navigation, userId} =
      this.props;
    const favoriteList = wishlistsSummaries.filter(
      (favorite) => favorite.displayName === profile.name,
    )[0];
    const isDelete = text === this.deleteProfileLabel();
    const editMenu = this.getEditMenu();
    if (isDelete) {
      if (favoriteList) deleteWishList({isDefault: false, wishListId: favoriteList.id});
      const payload = {
        profile,
        userId,
      };
      deleteProfile(payload);
      deleteCache(profile);
      await setValueInAsyncStorage(SBP_PROFILE_DELETED, '1');
      this.setState({showDialog: false});
      navigation.navigate('Home', {});
    } else {
      navigateToNestedRoute(
        navigation,
        'ShopByProfile',
        editMenu[text],
        Object.assign({}, profile, {isEdit: true}),
      );
    }
  }

  closeModal() {
    this.setState((prevState) => ({
      showDialog: !prevState.showDialog,
    }));
  }

  renderMenuItem(text) {
    const isDelete = text === this.deleteProfileLabel();
    let justifyContent = {
      justifyContent: 'space-between',
    };
    if (isDelete) {
      justifyContent = {
        justifyContent: 'flex-start',
      };
    }

    return (
      <TouchableOpacity
        onPress={() => (isDelete ? this.setState({showDialog: true}) : this.handlePress(text))}
        accessibilityRole="button"
        style={[styles.menuItemButton, justifyContent]}>
        {isDelete && <Image source={trashIcon} style={styles.menuItemImg} />}
        <Text style={styles.menuItemText}>{text}</Text>
        {!isDelete && <CustomIcon name="chevron-right" size="fs12" style={styles.customIcon} />}
      </TouchableOpacity>
    );
  }

  renderMenu() {
    const editMenu = this.getEditMenu();
    return Object.keys(editMenu).map(this.renderMenuItem.bind(this));
  }

  render() {
    const {profile, navigation, sbpLabels} = this.props;
    const containerStylesiPhone12 = isDeviceIPhone12() ? {margin: 22} : {};
    const viewProfileLabel = showSBPLabels(sbpLabels, 'lbl_sbp_view_profile_edit', 'View Profile');
    const deleteProfileLabel = this.deleteProfileLabel();
    if (!profile) return null;
    const {name, theme} = profile;
    const {showDialog} = this.state;
    return (
      <ScrollView contentContainerStyle={[styles.contentContainer, containerStylesiPhone12]}>
        <Modal
          showDialog={showDialog}
          name={name}
          theme={theme}
          hide={this.closeModal}
          action={() => this.handlePress(deleteProfileLabel)}
          sbpLabels={sbpLabels}
        />
        <View style={styles.modalContainer}>
          <View style={styles.profileIcon}>
            <ProfileIcon theme={theme} size={85} shadow />
          </View>
          <View style={styles.modalContainerInner}>
            <Text style={styles.nameText}>{name}</Text>
            <Text
              onPress={() =>
                navigation.navigate(
                  'PLP',
                  Object.assign({}, profile, {
                    categoriesLoading: false,
                    refresh: true,
                  }),
                )
              }
              style={styles.profileText}>
              {viewProfileLabel}
            </Text>
            <View style={styles.emptyView} />
          </View>
        </View>
        <View style={styles.menuContainer} />
        {this.renderMenu()}
        <View style={styles.menuItemContainer} />
        {this.renderMenuItem(deleteProfileLabel)}
      </ScrollView>
    );
  }
}

EditProfile.propTypes = {
  deleteProfile: func.isRequired,
  deleteWishList: func.isRequired,
  navigation: shape({
    navigate: func,
    setParams: func,
    state: shape({
      params: shape({
        theme: string,
      }),
    }),
  }).isRequired,
  profile: shape({
    name: string,
    theme: string,
  }).isRequired,
  sbpLabels: shape({}).isRequired,
  userId: number.isRequired,
  wishlistsSummaries: shape([]).isRequired,
};

const mapStateToProps = (state, props) => ({
  profile: state.SBPProfiles.sbpProfiles.find((p) => p.id === props.navigation.state.params.id),
  wishlistsSummaries: selectWishlistsSummaries(state),
  userId: getUserId(state),
  sbpLabels: getSBPLabels(state),
});

const mapDispatchToProps = (dispatch) => {
  return {
    deleteProfile: (payload) => {
      dispatch(deleteSBPProfile(payload));
    },
    deleteWishList: (payload) => dispatch(deleteWishListAction(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);
