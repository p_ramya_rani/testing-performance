// 9fbef606107a605d69c0edbcd8029e5d 
import {
  getValueFromAsyncStorage,
  removeMultipleFromAsyncStorage,
  setValueInAsyncStorage,
  getBrand,
} from '@tcp/core/src/utils';

const monetateRefreshKey = 'MON_PRODUCTS_REFRESHED_AT_';
const unbxdRefreshKey = 'UNBXD_PRODUCTS_REFRESHED_AT_';
const monetateProdsCacheKey = 'MON_PRODUCTS_CACHE_';
const unbxdProdsCacheKey = 'UNBXD_PRODUCTS_CACHE_';

const getCacheKey = async (key, profileId) => {
  const brandId = getBrand();
  return `${brandId}_${key + profileId}`;
};

export const deleteCache = async (profile, type = 'all') => {
  const profileId = profile.id;
  let asyncKeyArr = [];
  if (type === 'unbxd') {
    const cacheKey1 = await getCacheKey(unbxdRefreshKey, profileId);
    const cacheKey2 = await getCacheKey(unbxdProdsCacheKey, profileId);
    asyncKeyArr = [cacheKey1, cacheKey2];
  } else {
    const cacheKey1 = await getCacheKey(monetateRefreshKey, profileId);
    const cacheKey2 = await getCacheKey(unbxdRefreshKey, profileId);
    const cacheKey3 = await getCacheKey(monetateProdsCacheKey, profileId);
    const cacheKey4 = await getCacheKey(unbxdProdsCacheKey, profileId);
    asyncKeyArr = [cacheKey1, cacheKey2, cacheKey3, cacheKey4];
  }
  removeMultipleFromAsyncStorage(asyncKeyArr);
};

export const getMonetateProductsFromCache = async profileId => {
  const cacheKey = await getCacheKey(monetateProdsCacheKey, profileId);
  let prodData = await getValueFromAsyncStorage(cacheKey);
  prodData = await JSON.parse(prodData);
  return prodData;
};

export const setMonetateProductsToCache = async (data, profileId) => {
  const cacheKey = await getCacheKey(monetateProdsCacheKey, profileId);
  setValueInAsyncStorage(cacheKey, JSON.stringify(data));
  const timeNow = new Date().getTime();
  const cacheRefreshKey = await getCacheKey(monetateRefreshKey, profileId);

  setValueInAsyncStorage(cacheRefreshKey, timeNow.toString());
};

export const getUnbxdProductsFromCache = async profileId => {
  const cacheKey = await getCacheKey(unbxdProdsCacheKey, profileId);
  let prodData = await getValueFromAsyncStorage(cacheKey);
  prodData = await JSON.parse(prodData);
  return prodData;
};

export const setUnbxdProdsNextRefreshTime = async (configValue, profileId, type = 'all') => {
  const timeNow = new Date().getTime();
  const frequencyInMilliSeconds = configValue * 60 * 1000;
  const nextRefreshTime = timeNow + frequencyInMilliSeconds;
  const cacheKey = await getCacheKey(unbxdRefreshKey, profileId);
  setValueInAsyncStorage(cacheKey, nextRefreshTime.toString());
  if (configValue === 0) {
    const profile = {id: profileId};
    deleteCache(profile, type);
  }
};

export const setUnbxdProductsToCache = async (data, configValue, profileId) => {
  const cacheKey = await getCacheKey(unbxdProdsCacheKey, profileId);
  setValueInAsyncStorage(cacheKey, JSON.stringify(data));
  setUnbxdProdsNextRefreshTime(configValue, profileId);
};

export const canRefreshMonetateProds = async (configValue, profileId) => {
  const configValueHour = Math.round(configValue / 100);
  const configValueMinute = Math.round(configValue % 100);
  const todayDate = new Date();
  const currentHour = todayDate.getHours();
  todayDate.setHours(configValueHour);
  todayDate.setMinutes(configValueMinute);
  const todayConfValTimestamp = todayDate.getTime();
  const yesterdayDate = new Date(todayDate);
  yesterdayDate.setDate(yesterdayDate.getDate() - 1);
  const yesterdayConfValTimestamp = yesterdayDate.getTime();
  const cacheKey = await getCacheKey(monetateRefreshKey, profileId);
  const lastUpdatedTime = await getValueFromAsyncStorage(cacheKey);

  let returnVal = false;
  if (
    !lastUpdatedTime ||
    ((currentHour >= configValueHour && lastUpdatedTime < todayConfValTimestamp) ||
      (currentHour <= configValueHour && lastUpdatedTime < yesterdayConfValTimestamp))
  ) {
    returnVal = true;
  }
  return returnVal;
};

export const canRefreshUnbxdProds = async profileId => {
  const timeNow = new Date().getTime();
  const cacheKey = await getCacheKey(unbxdRefreshKey, profileId);

  let lastUpdatedTime = await getValueFromAsyncStorage(cacheKey);

  if (!lastUpdatedTime) {
    lastUpdatedTime = timeNow;
  }
  let returnVal = false;
  if (timeNow >= lastUpdatedTime) {
    returnVal = true;
  }
  return returnVal;
};
