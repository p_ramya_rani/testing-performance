// 9fbef606107a605d69c0edbcd8029e5d 
import {StyleSheet} from 'react-native';
import {
  colorWhite,
  colorMortar,
  colorNero,
  colorNobel,
  fontFamilyNunitoExtraBold,
  fontFamilyNunitoRegular,
  fontFamilyTofinoBold,
  fontSizeFourteen,
} from '../ShopByProfile.styles';

const styles = StyleSheet.create({
  buttonsContainer: {
    flexDirection: 'row',
    height: 100,
    paddingBottom: 34,
  },
  cancelButton: {
    backgroundColor: colorWhite,
    borderColor: colorNobel,
    borderWidth: 1,
    flex: 1,
    height: 42,
    justifyContent: 'center',
    marginRight: 14,
  },
  cancelButtonText: {
    color: colorMortar,
    fontFamily: fontFamilyNunitoExtraBold,
    fontSize: 13,
    fontWeight: '800',
    letterSpacing: 1,
    textAlign: 'center',
  },
  closeButton: {
    height: 25,
    left: '7%',
    top: '8%',
    width: 25,
    zIndex: 100,
  },
  closeButtonImg: {
    height: 15,
    width: 15,
  },
  container: {
    backgroundColor: colorWhite,
    flex: 1,
    height: '100%',
  },
  containerInner: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
    padding: 32,
  },
  deleteButton: {
    backgroundColor: colorNero,
    flex: 1,
    height: 42,
    justifyContent: 'center',
    marginHorizontal: 14,
  },
  deleteButtonText: {
    color: colorWhite,
    fontFamily: fontFamilyNunitoExtraBold,
    fontSize: 13,
    fontWeight: '800',
    letterSpacing: 1,
    textAlign: 'center',
  },
  textOne: {
    color: colorNero,
    fontFamily: fontFamilyTofinoBold,
    fontSize: 14,
    fontWeight: 'bold',
    marginTop: 15,
  },
  textThree: {
    color: colorNero,
    fontFamily: fontFamilyNunitoRegular,
    fontSize: fontSizeFourteen,
    fontWeight: '300',
    marginVertical: 30,
    textAlign: 'center',
  },
  textTwo: {
    color: colorNero,
    fontFamily: fontFamilyTofinoBold,
    fontSize: 22,
    fontWeight: 'bold',
    marginVertical: 30,
    textAlign: 'center',
  },
});

export default styles;
