// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import {TouchableOpacity} from 'react-native';
import {shallow} from 'enzyme';
import Component from '../index';

describe('Component', () => {
  let component;
  const onPressSpyHide = jest.fn();
  const onPressSpyAction = jest.fn();
  beforeEach(() => {
    const props = {
      showDialog: false,
      name: 'Name',
      theme: 1,
      hide: onPressSpyHide,
      action: onPressSpyAction,
    };
    component = shallow(<Component {...props} />);
  });

  it('Component should be defined', () => {
    expect(component).toBeDefined();
  });

  it('Component should render correctly', () => {
    expect(component).toMatchSnapshot();
  });

  it('Component to match snapshot', () => {
    component.setProps({showDialog: true});
    expect(component).toMatchSnapshot();
  });

  it('should call onPressSpyHide', () => {
    component
      .find(TouchableOpacity)
      .first()
      .props()
      .onPress();
    expect(onPressSpyHide).toHaveBeenCalled();
  });

  it('should call onPressSpyHide when pressing cancel', () => {
    component
      .find(TouchableOpacity)
      .last()
      .props()
      .onPress();
    expect(onPressSpyHide).toHaveBeenCalled();
  });

  it('should call onPressSpyAction when pressing delete', () => {
    component
      .find(TouchableOpacity)
      .at(1)
      .props()
      .onPress();
    expect(onPressSpyAction).toHaveBeenCalled();
  });
});
