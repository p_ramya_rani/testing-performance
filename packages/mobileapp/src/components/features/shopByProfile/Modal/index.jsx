// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import {View, Text, TouchableOpacity, Modal, Image} from 'react-native';
import {PropTypes} from 'prop-types';
import ProfileIcon from '../../../common/atoms/ProfileIcon';
import styles from './styles';
import {showSBPLabels} from '../PLP/helper';

const closeButtonSrc = require('../../../../assets/images/shopByProfile/elements/close3x.png');

const sbpModal = ({showDialog, name, theme, hide, action, sbpLabels}) => {
  const messageBeforeDeleteLabel = showSBPLabels(
    sbpLabels,
    'lbl_sbp_message_before_profile_deletion',
    'Are you sure you want to delete this profile?',
  );
  const deleteLabel = showSBPLabels(sbpLabels, 'lbl_sbp_delete_text_btn', 'DELETE');
  const cancelLabel = showSBPLabels(sbpLabels, 'lbl_sbp_cancel_text_btn', 'CANCEL');
  return (
    <Modal animationType="slide" transparent={false} visible={showDialog}>
      <View style={styles.container}>
        <TouchableOpacity onPress={hide} style={styles.closeButton} accessibilityRole="button">
          <Image source={closeButtonSrc} style={styles.closeButtonImg} />
        </TouchableOpacity>
        <View style={styles.containerInner}>
          <ProfileIcon size={86} theme={theme} shadow />
          <Text style={styles.textOne}>{name}</Text>
          <Text style={styles.textTwo}>{messageBeforeDeleteLabel}</Text>
          <Text style={styles.textThree}>
            {`You will no longer receive personalized shopping recommendations for ${name}.`}
          </Text>
        </View>
        <View style={styles.buttonsContainer}>
          <TouchableOpacity onPress={action} style={styles.deleteButton} accessibilityRole="button">
            <Text style={styles.deleteButtonText}>{deleteLabel}</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={hide} style={styles.cancelButton} accessibilityRole="button">
            <Text style={styles.cancelButtonText}>{cancelLabel}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

sbpModal.propTypes = {
  name: PropTypes.string.isRequired,
  theme: PropTypes.number.isRequired,
  hide: PropTypes.func.isRequired,
  action: PropTypes.func.isRequired,
  showDialog: PropTypes.bool.isRequired,
  sbpLabels: PropTypes.shape({}).isRequired,
};

export default sbpModal;
