// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable */
import React from 'react';
import {shallow} from 'enzyme';
import HomeOverlay from '../index';

describe('HomeOverlay', () => {
  let component;

  beforeEach(() => {
    const props = {
      navigation: {
        navigate: jest.fn(),
      },
      onCardOpen: jest.fn(),
    };
    component = shallow(<HomeOverlay {...props} />);
  });

  it('HomeOverlay should be defined', () => {
    expect(component).toBeDefined();
  });

  it('HomeOverlay should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
});
