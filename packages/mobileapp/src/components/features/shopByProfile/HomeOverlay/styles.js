// 9fbef606107a605d69c0edbcd8029e5d
import {Image} from '@tcp/core/src/components/common/atoms';
import styled from 'styled-components';
import {Platform} from 'react-native';
import {colorcloseFill, colorFill} from '../ShopByProfile.styles';

export const SpinnerWrapper = styled.View`
  flex: 1;
  background-color: '${(props) => (props.closeOverlay ? colorFill : colorcloseFill)}';
  opacity: 0.9;
`;

export const MainTextWrapper = styled.View`
  position: absolute;
  bottom: ${Platform.OS === 'ios' ? 260 : 230};
  align-self: center;
`;

export const SubTextWrapper = styled.View`
  position: absolute;
  bottom: ${Platform.OS === 'ios' ? 170 : 160};
  padding-left: 30px;
  padding-right: 30px;
`;

export const ShopWrapper = styled.View`
  margin-top: ${Platform.OS === 'ios' ? 135 : 100};
`;

export const ImageContainer = styled.View`
  position: absolute;
  right: 20;
  top: ${Platform.OS === 'ios' ? 50 : 20};
`;

export const StyledImage = styled(Image)`
  /* stylelint-disable-next-line */
  tint-color: #fff;
  width: 20;
  height: 20;
`;

export default {
  SpinnerWrapper,
  MainTextWrapper,
  SubTextWrapper,
  ShopWrapper,
  ImageContainer,
  StyledImage,
};
