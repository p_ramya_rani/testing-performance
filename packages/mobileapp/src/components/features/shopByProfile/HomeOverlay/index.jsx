/* eslint-disable react/no-unused-prop-types */
// 9fbef606107a605d69c0edbcd8029e5d
import React, {PureComponent} from 'react';
import {shape, func, bool, PropTypes} from 'prop-types';
import {BodyCopy} from '@tcp/core/src/components/common/atoms';
import {connect} from 'react-redux';
import {TouchableOpacity, View, Image, Text} from 'react-native';
import {getLabelValue} from '@tcp/core/src/utils';
import {getSBPLabels} from '../ShopByProfile.selector';
import {
  MainTextWrapper,
  SpinnerWrapper,
  SubTextWrapper,
  ShopWrapper,
  ImageContainer,
  StyledImage,
} from './styles';
import {isDeviceIPhone12, showSBPLabels} from '../PLP/helper';
import styles from '../ListProfile/styles';

const closeIconSrc = require('../../../../assets/images/shopByProfile/elements/close3x.png');
const iconShop = require('../../../../assets/images/shopByProfile/elements/icon-shop-stars_2020-12-11/icon-shop-stars.png');
const arrowUpSrc = require('../../../../assets/images/shopByProfile/elements/btn-circle-arrow-up-blue_2020-12-11/btn-circle-arrow-up-blue.png');

class HomeOverlay extends PureComponent {
  componentDidMount() {
    const {gotoOutSideClick} = this.props;
    setTimeout(() => {
      gotoOutSideClick();
    }, 9000);
  }

  renderTab() {
    const {sbpLabels, gotoOutSideClick} = this.props;
    const createACustomShopLabel = showSBPLabels(
      sbpLabels,
      'lbl_sbp_title',
      'CREATE A CUSTOM SHOP!',
    );
    const createHorizontalMargin = isDeviceIPhone12()
      ? styles.toggleCardInnerTxtIPhone12
      : styles.toggleCardInnerTxt;

    return (
      <TouchableOpacity
        onPress={gotoOutSideClick}
        style={styles.toggleCardContainer11}
        accessibilityRole="button">
        <View style={styles.toggleCardInner}>
          <View style={styles.toggleCardInner22}>
            <View style={styles.toggleCardInner3}>
              <Image style={styles.toggleCardInnerImg} source={iconShop} />
            </View>
            <Text style={createHorizontalMargin}>{createACustomShopLabel}</Text>
          </View>
          <View style={styles.toggleCardInner4}>
            <Image style={styles.toggleCardInnerImg2} source={arrowUpSrc} />
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    const {closeOverlay, gotoOutSideClick, labels} = this.props;
    return (
      <>
        {closeOverlay && labels && (
          <SpinnerWrapper closeOverlay={closeOverlay} onStartShouldSetResponder={gotoOutSideClick}>
            <ImageContainer>
              <StyledImage source={closeIconSrc} />
            </ImageContainer>
            <>
              <MainTextWrapper onStartShouldSetResponder={gotoOutSideClick}>
                <BodyCopy
                  text={getLabelValue(labels, 'lbl_shop_by_profile_title')}
                  textAlign="center"
                  color="white"
                  fontSize="fs20"
                  fontWeight="semibold"
                  letterSpacing="ls05"
                  fontFamily="secondary"
                />
              </MainTextWrapper>
              <SubTextWrapper>
                <BodyCopy
                  text={getLabelValue(labels, 'lbl_shop_by_profile_description')}
                  textAlign="center"
                  color="white"
                  fontSize="fs14"
                  fontWeight="regular"
                  letterSpacing="ls05"
                  fontFamily="secondary"
                />
              </SubTextWrapper>
            </>
            <ShopWrapper>{this.renderTab(gotoOutSideClick)}</ShopWrapper>
          </SpinnerWrapper>
        )}
      </>
    );
  }
}

HomeOverlay.propTypes = {
  navigation: shape({}).isRequired,
  labels: PropTypes.shape({}).isRequired,
  gotoOutSideClick: func,
  sbpLabels: shape({}).isRequired,
  closeOverlay: bool,
};

HomeOverlay.defaultProps = {
  gotoOutSideClick: () => {},
  closeOverlay: false,
};

const mapStateToProps = (state) => ({
  sbpLabels: getSBPLabels(state),
  labels: state.Labels.global && state.Labels.global.ShopByProfile,
});

export default connect(mapStateToProps, null)(HomeOverlay);
