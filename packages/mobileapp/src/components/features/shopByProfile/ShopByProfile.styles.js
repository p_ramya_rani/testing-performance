// 9fbef606107a605d69c0edbcd8029e5d
export const colorBlack = '#000000';
export const colorWhite = '#ffffff';
export const colorMortar = '#595959';
export const colorNero = '#1a1a1a';
export const colorNobel = '#9b9b9b';
export const colorNero2 = '#1c1c1c';
export const colorGainsboro = '#d8d8d8';
export const colorFireEngingeRed = '#c8102e';
export const colorPrussian = '#003057';
export const colorNero3 = '#262626';
export const colorWhiteSmoke = '#f3f3f3';
export const colorWhiteSmoke2 = '#f5f5f5';
export const colorGainsboro2 = '#e8e8e8';
export const colorShadyLady = '#979797';
export const colorMantis = '#7dc24c';
export const colorNobel3 = '#9c9c9c';
export const colorSummerSky = '#3498db';
export const colorRed = '#FF0000';
export const colorCharcoal = '#4a4a4a';
export const colorFill = 'rgba(0,0,0,0.9)';
export const colorcloseFill = 'rgba(0,0,0,0)';
export const colorViewMore = '#162f42';
export const colorGrey = '#e3e3e3';
export const colorBlue = '#254f6e';

export const fontFamilyNunitoExtraBold = 'Nunito-ExtraBold';
export const fontFamilyNunitoSemiBold = 'Nunito-SemiBold';
export const fontFamilyNunitoRegular = 'Nunito-Regular';
export const fontFamilyNunitoBlack = 'Nunito-Black';
export const fontFamilyNunito = 'Nunito';
export const fontFamilyNunitoBold = 'Nunito-Bold';
export const fontFamilyTofinoBold = 'Tofino-Bold';
export const fontFamilyTofinoRegular = 'Tofino-Regular';
export const fontFamilyTofinoSemiBold = 'Tofino-Semibold';
export const fontFamilyTofinoPersonal = 'TofinoPersonal';

export const fontSizeTwelve = 12;
export const fontSizeFourteen = 14;

export const weightFont = 'bold';
