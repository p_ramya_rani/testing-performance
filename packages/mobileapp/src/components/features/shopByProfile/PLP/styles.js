// 9fbef606107a605d69c0edbcd8029e5d
import {Dimensions} from 'react-native';
import {
  colorWhite,
  colorWhiteSmoke,
  colorNero,
  colorGainsboro2,
  fontFamilyNunitoRegular,
  fontFamilyNunitoSemiBold,
} from '../ShopByProfile.styles';

const {width} = Dimensions.get('window');

export default {
  container: {flex: 1, marginBottom: '-45%', backgroundColor: colorWhiteSmoke},
  animatedView: {
    position: 'absolute',
    top: -10,
    right: 0,
    left: 0,
    zIndex: 100,
    backgroundColor: colorWhiteSmoke,
  },
  cover: {
    width,
    position: 'absolute',
    height: 50,
  },
  androidCover: {width: '100%', height: 0, top: 45},
  androidProfile: {zIndex: 10, alignSelf: 'center', position: 'absolute', top: -36},
  emptyContainer: {height: 30},
  scrollView: {
    height: 50,
    borderBottomWidth: 1,
    borderBottomColor: colorGainsboro2,
  },
  topMatching: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 75,
    marginTop: '4%',
  },
  toast: {backgroundColor: colorNero},
  toastText: {
    color: colorWhite,
    fontFamily: fontFamilyNunitoRegular,
    fontSize: 16,
  },
  noProduct: {marginBottom: 10},
  noProductContainer: {
    alignItems: 'center',
    height: '80%',
    justifyContent: 'center',
  },
  noProductText: {
    fontFamily: fontFamilyNunitoSemiBold,
    fontSize: 18,
  },
  topMatchingContainter: {
    height: 345,
    marginBottom: '12%',
  },
  topMatchingScroll: {
    marginTop: -20,
  },
  topMatchingScrollContainter: {paddingBottom: 320, paddingTop: 180},
  productContainer: {
    paddingBottom: 200,
    paddingTop: 145,
  },
  sectionListContainer: {},
};
