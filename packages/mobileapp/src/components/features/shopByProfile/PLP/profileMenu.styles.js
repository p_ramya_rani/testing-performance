// 9fbef606107a605d69c0edbcd8029e5d 
import {StyleSheet, Dimensions} from 'react-native';
import {
  colorWhite,
  colorMortar,
  colorGainsboro,
  fontFamilyNunitoSemiBold,
} from '../ShopByProfile.styles';

const {height} = Dimensions.get('window');
const modalBG = 'transparent';

const styles = StyleSheet.create({
  closeIcon: {
    height: 20,
    marginRight: 24,
    width: 20,
  },
  container: {
    elevation: 10,
    height,
    left: 0,
    position: 'absolute',
    top: 0,
    width: '100%',
    zIndex: 200,
  },
  editIcon: {
    marginRight: 22,
  },
  goToEditProfile: {
    flexDirection: 'row',
    padding: 10,
    paddingLeft: 20,
  },
  modal: {
    backgroundColor: modalBG,
    height: 'auto',
  },
  modalContainer: {
    backgroundColor: colorWhite,
    borderBottomWidth: 0,
    borderColor: colorGainsboro,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    borderWidth: 1,
    height: 200,
    paddingBottom: 20,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 20,
  },
  modalText: {
    color: colorMortar,
    fontFamily: fontFamilyNunitoSemiBold,
    fontSize: 16,
    fontWeight: '600',
    top: 1,
  },
  toggleMenu: {
    flexDirection: 'row',
    padding: 10,
    paddingLeft: 20,
  },
});

export default styles;
