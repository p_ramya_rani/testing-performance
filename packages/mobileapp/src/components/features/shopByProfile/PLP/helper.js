// 9fbef606107a605d69c0edbcd8029e5d
import superagent from 'superagent';
import {getAPIConfig} from '@tcp/core/src/utils';
import logger from '@tcp/core/src/utils/loggerInstance';
import DeviceInfo from 'react-native-device-info';
import {getCustomHeaders} from '@tcp/core/src/utils/utils.app';

export function capitalizeWords(text) {
  const wordsArray = text.toLowerCase().split(' ');
  const capsArray = wordsArray.map((word) => {
    return word[0].toUpperCase() + word.slice(1);
  });
  return capsArray.join(' ');
}

const departments = [
  'Baby And Toddler Boys',
  'Baby And Toddler Girls',
  'Toddler Girls',
  'Toddler Boys',
  'Baby Boys',
  'Baby Girls',
  'Girls',
  'Boys',
];

/* eslint-disable no-param-reassign */
export function streamlineProductName(productName, isCollection) {
  if (isCollection) {
    return productName;
  }
  departments.forEach((department) => {
    productName = productName.replace(department, '');
  });
  return productName.trim();
}
/* eslint-enable no-param-reassign */

export function getMaximumDiscount(listOfProducts) {
  if (!listOfProducts) return 0;
  const maxDiscountList = [];
  listOfProducts.forEach((product) => {
    if (product.productInfo && product.productInfo.TCPMerchantTagUSStore) {
      const discountPercentage = product.productInfo.TCPMerchantTagUSStore.split('%')[0];
      maxDiscountList.push(discountPercentage);
    } else if (product.TCPMerchantTagUSStore) {
      const discountPercentage = product.TCPMerchantTagUSStore.split('%')[0];
      maxDiscountList.push(discountPercentage);
    }
  });

  return Math.max(...maxDiscountList);
}

export async function getSizeMapping(gender) {
  const genderCap = capitalizeWords(gender);
  const apiConfigObj = getAPIConfig();
  const {sbpSizes} = apiConfigObj;
  const URL = `${sbpSizes}?department=${genderCap}`;
  const sizeMapping = [];
  try {
    const response = await superagent.get(URL).set(getCustomHeaders());
    const {size_mapping: sizeMappingResponse} = response.body;
    sizeMappingResponse.forEach((sizeObj) => {
      sizeMapping.push({
        category: sizeObj.category,
        fit: sizeObj.fit,
        size: sizeObj.size,
        all_sizes: (sizeObj.all_sizes && sizeObj.all_sizes.split(',')) || [],
        ranking: sizeObj.ranking,
      });
    });
    return sizeMapping;
  } catch (error) {
    logger.error(error);
  }
  return [];
}

export function isDeviceIPhone12() {
  const deviceInfo = DeviceInfo && DeviceInfo.getDeviceId ? DeviceInfo.getDeviceId() : '';
  return deviceInfo.split(',')[0] === 'iPhone13' || deviceInfo.split(',')[0] === 'iPhone14';
}

export function showSBPLabels(sbpLabels, labelID, defaultText) {
  return sbpLabels && sbpLabels[labelID] ? sbpLabels[labelID] : defaultText;
}
