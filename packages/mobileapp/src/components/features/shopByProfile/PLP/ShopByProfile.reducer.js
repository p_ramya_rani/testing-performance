// 9fbef606107a605d69c0edbcd8029e5d 
const initialState = {};
/* eslint-disable */
const PLPReducer = (state, action) => {
  if (typeof state === 'undefined') {
    state = initialState;
  }
  switch (action.type) {
    case 'UPDATE_PRODUCTS_BY_PROFILE': {
      const newState = {...state};
      newState[action.payload.profileId] = action.payload;
      return newState;
    }
    default:
      return state;
  }
};
/* eslint-enable */

export default PLPReducer;
