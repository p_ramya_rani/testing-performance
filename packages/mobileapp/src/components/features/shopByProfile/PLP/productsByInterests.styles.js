// 9fbef606107a605d69c0edbcd8029e5d
import {StyleSheet} from 'react-native';
import {colorWhite, colorNero3, fontFamilyNunitoExtraBold} from '../ShopByProfile.styles';

const styles = StyleSheet.create({
  bannerContainer: {
    alignItems: 'center',
    backgroundColor: colorWhite,
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: 10,
    overflow: 'hidden',
    width: '100%',
  },
  bannerImageOne: {
    height: 68,
    marginLeft: '-10%',
    resizeMode: 'contain',
    tintColor: colorWhite,
    width: 68,
  },
  bannerImageTwo: {
    height: 40,
    marginLeft: -15,
    marginTop: '5%',
    resizeMode: 'contain',
    tintColor: colorNero3,
    width: 40,
  },
  bannerText: {
    color: colorNero3,
    fontFamily: fontFamilyNunitoExtraBold,
    fontSize: 18,
    fontWeight: '800',
    marginLeft: -15,
  },
  bannerTextContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginLeft: 35,
    marginVertical: 0,
    width: '50%',
  },
  productsContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginBottom: 5,
    marginHorizontal: 4,
  },
  viewMore: {
    marginBottom: 5,
    marginTop: 10,
  },
  viewMoreEmpty: {},
});

export default styles;
