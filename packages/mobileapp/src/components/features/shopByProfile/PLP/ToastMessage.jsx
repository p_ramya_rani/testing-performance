// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import {PropTypes} from 'prop-types';
import styles from './toastMessage.styles';

const imgSource = require('../../../../assets/images/shopByProfile/elements/close3x.png');

const ToastMessage = ({name, toggleToastMessage}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>
        Added to
        {name}
        &apos;s List
      </Text>
      <TouchableOpacity
        onPress={() => toggleToastMessage()}
        style={styles.touchableImage}
        accessibilityRole="button">
        <Image style={styles.img} source={imgSource} />
      </TouchableOpacity>
    </View>
  );
};

ToastMessage.propTypes = {
  name: PropTypes.string.isRequired,
  toggleToastMessage: PropTypes.func.isRequired,
};

export default ToastMessage;
