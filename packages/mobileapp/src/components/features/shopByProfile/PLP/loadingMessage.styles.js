// 9fbef606107a605d69c0edbcd8029e5d 
import {StyleSheet} from 'react-native';
import {
  colorWhite,
  colorMortar,
  fontFamilyNunitoSemiBold,
  fontSizeFourteen,
} from '../ShopByProfile.styles';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    height: '90%',
    justifyContent: 'center',
    width: '90%',
  },
  message: {
    color: colorMortar,
    fontFamily: fontFamilyNunitoSemiBold,
    fontSize: fontSizeFourteen,
    marginTop: '10%',
    textAlign: 'center',
  },
  modal: {
    alignItems: 'center',
    backgroundColor: colorWhite,
    borderRadius: 15,
    height: 220,
    width: 220,
  },
});

export default styles;
