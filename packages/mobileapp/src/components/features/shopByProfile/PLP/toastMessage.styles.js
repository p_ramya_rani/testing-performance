// 9fbef606107a605d69c0edbcd8029e5d 
import {StyleSheet} from 'react-native';
import {colorWhite, colorNero, fontFamilyNunitoRegular} from '../ShopByProfile.styles';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: colorNero,
    bottom: 20,
    flexDirection: 'row',
    height: 54,
    justifyContent: 'space-between',
  },
  img: {
    height: 18,
    width: 18,
  },
  text: {
    color: colorWhite,
    fontFamily: fontFamilyNunitoRegular,
    fontSize: 16,
    marginLeft: 25,
  },
  touchableImage: {
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 0,
    width: 100,
  },
});

export default styles;
