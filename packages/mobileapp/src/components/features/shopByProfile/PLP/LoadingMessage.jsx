// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import {Text, View, ActivityIndicator} from 'react-native';
import Modal from 'react-native-modalbox';
import {connect} from 'react-redux';
import {PropTypes} from 'prop-types';
import Themes from '../ShopByProfile.theme';
import styles from './loadingMessage.styles';
import {showSBPLabels} from './helper';
import {getSBPLabels} from '../ShopByProfile.selector';

export const LoadingMessage = props => {
  const {showMessage, sbpLabels, theme} = props;
  const loadingMessageLabel = showSBPLabels(
    sbpLabels,
    'lbl_sbp_plp_processing',
    'Please wait while your custom shop is getting ready.',
  );
  return (
    <Modal
      animationDuration={400}
      backdropPressToClose={false}
      swipeThreshold={100}
      swipeToClose={false}
      style={styles.modal}
      position="center"
      isOpen={showMessage}
      entry="bottom"
      coverScreen>
      <View style={styles.container}>
        <ActivityIndicator size="large" color={Themes[theme || 0].accent} />
        <Text style={styles.message}>{loadingMessageLabel}</Text>
      </View>
    </Modal>
  );
};

LoadingMessage.propTypes = {
  theme: PropTypes.number.isRequired,
  showMessage: PropTypes.bool.isRequired,
  sbpLabels: PropTypes.shape({}).isRequired,
};

const mapStateToProps = state => ({
  sbpLabels: getSBPLabels(state),
});

export default connect(mapStateToProps)(LoadingMessage);
