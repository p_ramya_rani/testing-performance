// 9fbef606107a605d69c0edbcd8029e5d 
import {StyleSheet} from 'react-native';
import {
  colorWhite,
  colorMortar,
  colorNero,
  colorNobel,
  colorPrussian,
  fontFamilyNunitoExtraBold,
  fontFamilyNunitoBlack,
  fontFamilyNunitoBold,
  fontSizeFourteen,
} from '../ShopByProfile.styles';

const gymBorder = null;

const styles = StyleSheet.create({
  buttonContainer: {
    alignItems: 'center',
    borderRadius: 21,
    height: 42,
    marginVertical: '7%',
    width: 263,
  },
  buttonContainerGym: {
    backgroundColor: colorPrussian,
    borderColor: gymBorder,
    borderWidth: 0,
  },
  buttonContainerTcp: {
    backgroundColor: colorWhite,
    borderColor: colorNobel,
    borderWidth: 1,
  },
  buttonText: {
    fontFamily: fontFamilyNunitoExtraBold,
    fontSize: 13,
    letterSpacing: 0.93,
    marginVertical: '5%',
  },
  buttonTextGym: {
    color: colorWhite,
  },
  buttonTextTcp: {
    color: colorMortar,
  },
  container: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
    marginTop: '10%',
  },
  header: {
    color: colorNero,
    fontFamily: fontFamilyNunitoBlack,
    fontSize: 22,
    textAlign: 'center',
  },
  mainContent: {
    color: colorMortar,
    fontFamily: fontFamilyNunitoBold,
    fontSize: fontSizeFourteen,
    textAlign: 'center',
    width: '70%',
  },
  squiggleImg: {
    height: 6,
    marginVertical: '5%',
    width: 60,
  },
});

export default styles;
