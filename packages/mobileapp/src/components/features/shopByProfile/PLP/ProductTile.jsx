/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import {getAPIConfig, getBadgeParam} from '@tcp/core/src/utils';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import {PropTypes} from 'prop-types';
import ProductRating from '@tcp/core/src/components/features/browse/ProductDetail/molecules/ProductRating/ProductRating.view.native';
import processHelpers from '@tcp/core/src/services/abstractors/productListing/processHelpers';
import {getColorsMap} from '@tcp/core/src/services/abstractors/productListing/productParser';
import ColorSwitch from '@tcp/core/src/components/features/browse/ProductListing/molecules/ColorSwitch';
import ButtonRedesign from '@tcp/core/src/components/common/atoms/ButtonRedesign';
import {streamlineProductName} from './helper';
import {IMAGES_PARAMS} from '../ShopByProfile.constants';
import styles from './productTile.styles';

function getProductImagePath(id, excludeExtension, imageExtension) {
  try {
    const imageName = (id && id.split('_')) || [];
    const imagePath = imageName[0];
    const extension = imageExtension ? `.${imageExtension}` : '.jpg';

    return {
      125: `${imagePath}/${id}${excludeExtension ? '' : extension}`,
      380: `${imagePath}/${id}${excludeExtension ? '' : extension}`,
      500: `${imagePath}/${id}${excludeExtension ? '' : extension}`,
      900: `${imagePath}/${id}${excludeExtension ? '' : extension}`,
    };
  } catch (e) {
    return null;
  }
}
export const getSwatchImgPath = (id, excludeExtension) => {
  try {
    const imageName = id.split('_');
    const imagePath = imageName[0];
    return `${imagePath}/${id}${excludeExtension ? '' : '.jpg'}`;
  } catch (e) {
    return '';
  }
};

function getImgPath(id, excludeExtension, imageExtension) {
  try {
    return {
      colorSwatch: getSwatchImgPath(id, excludeExtension),
      productImages: getProductImagePath(id, excludeExtension, imageExtension),
    };
  } catch (e) {
    return {
      colorSwatch: '',
      productImages: null,
    };
  }
}

const RenderColorSwitch = (values) => {
  const {setSelectedColorIndex, colorsMap, item, isFavorite, widthPer} = values;
  return (
    <ColorSwitch
      colorsMap={colorsMap}
      setSelectedColorIndex={setSelectedColorIndex}
      itemBrand="tcp"
      itemData={item}
      isFavorite={isFavorite}
      widthPer={widthPer}
      isNewReDesignProductTile
    />
  );
};
const favoriteImageFilled = require('../../../../assets/images/shopByProfile/elements/heart-filled_2020-09-02/heart-filled.png');
const favoriteImage = require('../../../../assets/images/shopByProfile/elements/heart_2020-09-02/heart.png');

/* eslint-disable */
const getInterestGrid = (
  product,
  goToPDP,
  openQuickAdd,
  onAddItemToFavorites,
  isBundle,
  colorsMap,
  plpLabels,
  brandId,
  numberOfDisplayedProds,
  position,
  quickViewLoader,
  quickViewProductId,
) => {
  const {streamLinedName, imagePath, productInfo, uniqueId} = product;
  const productBadge = product.colorsMap[0].miscInfo.badge1.defaultBadge || '';
  const shopCollectionLabel = plpLabels.lbl_plpTiles_shop_collection;
  const addToBagLabel = plpLabels.lbl_add_to_bag_redesign;
  const addToBagStylingGym = brandId === 'gym' ? {borderRadius: 25} : {};
  const productDiscount = productInfo.TCPMerchantTagUSStore
    ? productInfo.TCPMerchantTagUSStore
    : null;
  let removeBottomBorder = false;
  let bottomBorderWidth = {};
  const modifiedListPrice =
    productInfo &&
    productInfo.listPrice &&
    productInfo.listPrice.toString().split('.') &&
    productInfo.listPrice.toString().split('.')[1] &&
    productInfo.listPrice.toString().split('.')[1].length > 1
      ? productInfo.listPrice
      : `${productInfo.listPrice}0`;

  if (numberOfDisplayedProds > 4) {
    if (
      numberOfDisplayedProds - 3 === position ||
      (numberOfDisplayedProds - 4 === position && numberOfDisplayedProds % 2 === 0)
    ) {
      removeBottomBorder = true;
    }
  }
  if (numberOfDisplayedProds === 4) {
    if (position === 3 || position === 2) {
      removeBottomBorder = true;
    }
  }
  if (removeBottomBorder) {
    bottomBorderWidth = {borderBottomWidth: 0};
  }

  const showLoadingSpinner = uniqueId === quickViewProductId && quickViewLoader;

  return (
    <View style={[styles.container, bottomBorderWidth]}>
      <View style={styles.productTileSection1}>
        <Text style={styles.textOne}>{productBadge}</Text>
        <TouchableOpacity onPress={() => onAddItemToFavorites(product)} accessibilityRole="button">
          {product.favorite ? (
            <Image source={favoriteImageFilled} style={{...styles.favoriteImage}} />
          ) : (
            <Image source={favoriteImage} style={{...styles.favoriteImage}} />
          )}
        </TouchableOpacity>
      </View>
      <TouchableOpacity onPress={() => goToPDP(product, isBundle)} accessibilityRole="button">
        <Image source={{uri: imagePath, cache: 'force-cache'}} style={styles.goToPDP} />
        <View style={styles.productTileSection3}>
          <Text style={styles.textThree}>
            {product.productInfo.offerPrice
              ? `$${product.productInfo.offerPrice.toFixed(2)}`
              : `$${product.productInfo.listPrice.toFixed(2)}`}
          </Text>
          <RenderColorSwitch
            colorsMap={colorsMap}
            item={product}
            setSelectedColorIndex={0}
            isSuggestedItem={false}
            isFavorite={false}
            widthPer="100"
          />
        </View>
        <View style={styles.listPrice}>
          <Text style={styles.textFour}>{`$${modifiedListPrice}`}</Text>
          <Text style={styles.offerPrice}>{productDiscount}</Text>
        </View>
        <View style={styles.nameContainer}>
          <Text style={[styles.textOne, styles.textOneAdd]} numberOfLines={2} ellipsizeMode="tail">
            {streamLinedName}
          </Text>
        </View>

        {product.productInfo.reviewsCount && product.productInfo.ratings ? (
          <ProductRating
            ratings={product.productInfo.ratings}
            reviewsCount={product.productInfo.reviewsCount}
          />
        ) : (
          <View style={styles.productRating} />
        )}
        {isBundle ? (
          <View style={[styles.bundleView, addToBagStylingGym]}>
            <Text style={styles.bundleText}>{shopCollectionLabel}</Text>
          </View>
        ) : (
          <ButtonRedesign
            paddings="9px 5px"
            fill="BLUE"
            text={addToBagLabel}
            onPress={() => openQuickAdd(product, isBundle)}
            accessibilityLabel="button"
            fontSize="fs16"
            fontFamily="secondary"
            fontWeight="extrabold"
            showLoadingSpinner={showLoadingSpinner}
          />
        )}
      </TouchableOpacity>
    </View>
  );
};
/* eslint-enable */

/* eslint-disable */
const getNotInterestedGrid = (
  product,
  goToPDP,
  openQuickAdd,
  onAddItemToFavorites,
  isBundle,
  colorsMap,
  plpLabels,
  brandId,
  numberOfDisplayedProds,
  position,
  quickViewLoader,
  quickViewProductId,
) => {
  const {
    uniqueId,
    min_list_price: minListPrice,
    max_list_price: maxListPrice,
    min_offer_price: minOfferPrice,
    max_offer_price: maxOfferPrice,
    topRated,
    streamLinedName,
    imagePath,
  } = product;

  const shopCollectionLabel = plpLabels.lbl_plpTiles_shop_collection;
  const addToBagLabel = plpLabels.lbl_add_to_bag_redesign;
  const addToBagStylingGym = brandId === 'gym' ? {borderRadius: 25} : {};
  let removeBottomBorder = false;
  const productDiscount = product.TCPMerchantTagUSStore ? product.TCPMerchantTagUSStore : null;
  let bottomBorderWidth = {};
  const modifiedListPrice =
    minListPrice &&
    minListPrice.toString().split('.') &&
    minListPrice.toString().split('.')[1] &&
    minListPrice.toString().split('.')[1].length > 1
      ? minListPrice
      : `${minListPrice}0`;
  if (numberOfDisplayedProds > 4) {
    if (
      numberOfDisplayedProds - 1 === position ||
      (numberOfDisplayedProds - 2 === position && numberOfDisplayedProds % 2 === 0)
    ) {
      removeBottomBorder = true;
    }
  }
  if (numberOfDisplayedProds === 4) {
    if (position === 3 || position === 2) {
      removeBottomBorder = true;
    }
  }
  if (removeBottomBorder) {
    bottomBorderWidth = {borderBottomWidth: 0};
  }
  const showLoadingSpinner = uniqueId === quickViewProductId && quickViewLoader;
  return (
    <View style={[styles.container, bottomBorderWidth]}>
      <View style={styles.productTileSection1}>
        <Text style={styles.textOne}>{topRated ? 'TOP RATED' : ''}</Text>
        <TouchableOpacity onPress={() => onAddItemToFavorites(product)} accessibilityRole="button">
          {product.favorite ? (
            <Image source={favoriteImageFilled} style={{...styles.favoriteImage}} />
          ) : (
            <Image source={favoriteImage} style={{...styles.favoriteImage}} />
          )}
        </TouchableOpacity>
      </View>
      <TouchableOpacity onPress={() => goToPDP(product, isBundle)} accessibilityRole="button">
        <Image source={{uri: imagePath, cache: 'force-cache'}} style={styles.goToPDP} />
        <View style={styles.productTileSection3}>
          <Text style={styles.textThree}>
            {`$${minOfferPrice.toFixed(2)} ${
              maxOfferPrice ? `- $${maxOfferPrice.toFixed(2)}` : ''
            }`}
          </Text>
          <RenderColorSwitch
            colorsMap={colorsMap}
            item={product}
            setSelectedColorIndex={0}
            isSuggestedItem={false}
            isFavorite={false}
            widthPer="100"
          />
        </View>
        <View style={styles.listPrice}>
          <Text style={styles.textFour}>
            {`$${modifiedListPrice} ${maxListPrice ? `- $${maxListPrice}` : ''}`}
          </Text>
          <Text style={styles.percentage}>{productDiscount}</Text>
        </View>
        <View style={styles.nameContainer}>
          <Text style={[styles.textOne, styles.textOneAdd]} numberOfLines={2} ellipsizeMode="tail">
            {streamLinedName}
          </Text>
        </View>
        {product.TCPBazaarVoiceReviewCount && product.TCPBazaarVoiceRating ? (
          <ProductRating
            ratings={product.TCPBazaarVoiceRating}
            reviewsCount={product.TCPBazaarVoiceReviewCount}
          />
        ) : (
          <View style={styles.productRating} />
        )}
        {isBundle ? (
          <View style={[styles.bundleView, addToBagStylingGym]}>
            <Text style={styles.bundleText}>{shopCollectionLabel}</Text>
          </View>
        ) : (
          <ButtonRedesign
            paddings="9px 5px"
            fill="BLUE"
            text={addToBagLabel}
            onPress={() => openQuickAdd(product, isBundle)}
            accessibilityLabel="button"
            fontSize="fs16"
            fontFamily="secondary"
            fontWeight="extrabold"
            showLoadingSpinner={showLoadingSpinner}
          />
        )}
      </TouchableOpacity>
    </View>
  );
};
/* eslint-enable */
/* eslint-disable no-param-reassign */
/**
 * Removing React memo. The data structure (state structure) of this component is quite complex.
 * React memo only do the shallow checking, which is why it is not able to detect the changes
 * done - deep down the state and hence not updating itself properly.
 */
const ProductTile = (props) => {
  const {
    product,
    goToPDP,
    openQuickAdd,
    interestGrid,
    onAddItemToFavorites,
    isBundle,
    plpLabels,
    brandId,
    numberOfDisplayedProds,
    position,
    isDynamicBadgeEnabled,
    quickViewLoader,
    quickViewProductId,
  } = props;
  if (typeof product === 'string') return null;
  const {prodpartno, product_name: productName} = product;
  const isCollection = productName
    ? productName.split(' ').includes('Matching')
    : product.name.split(' ').includes('Matching');
  const prodName = product.product_name ? product.product_name : product.name;
  const streamLinedName = streamlineProductName(prodName, isCollection);
  const prodIdForImg = prodpartno || product.generalProductId;
  const prodIdForImgNoUnderscore = prodIdForImg.split('_')[0];
  product.swatchimage = `${prodIdForImg}_swatch.jpg`;

  const colorsMap = getColorsMap({
    uniqueId: prodIdForImg,
    product,
    attributesNames: processHelpers.getProductAttributes(),
    categoryType: '',
    excludeBadge: false,
    isBOPIS: false,
    bossDisabledFlags: true,
    defaultColor: null,
    getImgPath,
    isBundleProduct: false,
  });

  const {assetHostTCP, productAssetPathTCP, productAssetPathGYM} = getAPIConfig();
  const assetPath = brandId === 'gym' ? productAssetPathGYM : productAssetPathTCP;
  const badgeParam = getBadgeParam(
    isDynamicBadgeEnabled,
    product?.TCPStyleTypeUS || product?.productInfo?.tcpStyleType,
    product?.TCPStyleQTYUS || product?.productInfo?.tcpStyleQty,
  );
  const modifiedImagePath = `${assetHostTCP}${badgeParam}${IMAGES_PARAMS}${assetPath}/${prodIdForImgNoUnderscore}/${prodIdForImg}.jpg`;
  const productModified = product;
  productModified.streamLinedName = streamLinedName;
  productModified.imagePath = modifiedImagePath;
  if (interestGrid) {
    return getInterestGrid(
      productModified,
      goToPDP,
      openQuickAdd,
      onAddItemToFavorites,
      isBundle,
      colorsMap,
      plpLabels,
      brandId,
      numberOfDisplayedProds,
      position,
      quickViewLoader,
      quickViewProductId,
    );
  }
  return getNotInterestedGrid(
    productModified,
    goToPDP,
    openQuickAdd,
    onAddItemToFavorites,
    isBundle,
    colorsMap,
    plpLabels,
    brandId,
    numberOfDisplayedProds,
    position,
    quickViewLoader,
    quickViewProductId,
  );
};
/* eslint-enable no-param-reassign */

ProductTile.propTypes = {
  goToPDP: PropTypes.func.isRequired,
  openQuickAdd: PropTypes.func.isRequired,
  interestGrid: PropTypes.bool.isRequired,
  onAddItemToFavorites: PropTypes.func.isRequired,
  isBundle: PropTypes.bool.isRequired,
  product: PropTypes.shape({}).isRequired,
  plpLabels: PropTypes.shape({}).isRequired,
  brandId: PropTypes.string.isRequired,
  numberOfDisplayedProds: PropTypes.number.isRequired,
  position: PropTypes.number.isRequired,
  quickViewLoader: PropTypes.bool.isRequired,
  quickViewProductId: PropTypes.string.isRequired,
};
export default ProductTile;
