// 9fbef606107a605d69c0edbcd8029e5d
import React, {PureComponent} from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import {func, shape, string} from 'prop-types';
import {connect} from 'react-redux';
import {getBrand} from '@tcp/core/src/utils';
import {NavigationActions, StackActions} from 'react-navigation';
import {updateAppTypeWithParams} from '@tcp/core/src/components/common/organisms/QuickViewModal/container/QuickViewModal.actions';
import styles from './noProductsScreen.styles';
import {showSBPLabels} from './helper';
import {BRAND_ID} from '../ShopByProfile.constants';
import {getSBPLabels} from '../ShopByProfile.selector';

export function Button({text, onPress}) {
  const brand = getBrand();
  let containerStyle = styles.buttonContainerTcp;
  let containerText = styles.buttonTextTcp;
  if (brand === BRAND_ID.GYM) {
    containerStyle = styles.buttonContainerGym;
    containerText = styles.buttonTextGym;
  }
  return (
    <TouchableOpacity onPress={onPress} accessibilityRole="button">
      <View style={[styles.buttonContainer, containerStyle]}>
        <Text style={[styles.buttonText, containerText]}>{text.toUpperCase()}</Text>
      </View>
    </TouchableOpacity>
  );
}

Button.propTypes = {
  text: string.isRequired,
  onPress: func.isRequired,
};

const squiggleImgSrc = require('../../../../assets/images/shopByProfile/elements/squiggle2x.png');

export class NoResultsPage extends PureComponent {
  render() {
    const {profile, navigation, sbpLabels, updateAppTypeHandler} = this.props;
    const {name} = profile;
    const brand = getBrand();

    const mainTextCMSLabel = showSBPLabels(
      sbpLabels,
      'lbl_sbp_gym_profile_no_products_text',
      'Gymboree is out of size X; but toggle to our other brand, The Children’s Place, for more recommendations.',
    );
    const goToTCPButtonCMSLabel = showSBPLabels(
      sbpLabels,
      'lbl_sbp_gym_profile_no_products_cta2',
      "Shop The Children's Place",
    );

    const goToGYMButtonCMSLabel = showSBPLabels(
      sbpLabels,
      'lbl_sbp_gym_profile_no_products_cta1',
      'Shop The Rest of Gymboree',
    );

    const profileCompletionGYMCMSLabel =
      sbpLabels && sbpLabels.lbl_sbp_gym_profile_completion_header
        ? sbpLabels.lbl_sbp_gym_profile_completion_header.replace('Nico!', `${name}!`)
        : `Thanks for creating a shopping profile for ${name}!`;
    return (
      <View style={{...styles.container}}>
        <Text style={{...styles.header}}>{profileCompletionGYMCMSLabel}</Text>
        <Image source={squiggleImgSrc} style={styles.squiggleImg} />
        <Text style={{...styles.mainContent}}>{mainTextCMSLabel}</Text>
        {brand === BRAND_ID.GYM ? (
          <View>
            <Button
              text={goToGYMButtonCMSLabel}
              profile={profile}
              onPress={() => {
                navigation.dispatch(
                  StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({routeName: 'Home'})],
                  }),
                );
              }}
            />
            <Button
              text={goToTCPButtonCMSLabel}
              profile={profile}
              onPress={() => {
                updateAppTypeHandler({
                  type: brand === BRAND_ID.GYM ? BRAND_ID.TCP : 'gymboree',
                  params: {
                    sbpProfile: JSON.stringify(profile),
                  },
                });
              }}
            />
          </View>
        ) : (
          <Button
            text={goToTCPButtonCMSLabel}
            profile={profile}
            onPress={() => {
              updateAppTypeHandler({
                type: brand === BRAND_ID.GYM ? 'gymboree' : BRAND_ID.TCP,
                params: {
                  sbpProfile: JSON.stringify(profile),
                },
              });
            }}
          />
        )}
      </View>
    );
  }
}

NoResultsPage.propTypes = {
  navigation: shape({
    dispatch: func,
  }).isRequired,
  profile: shape({
    name: string,
  }).isRequired,
  sbpLabels: shape({
    lbl_sbp_gym_profile_completion_header: string,
  }).isRequired,
  updateAppTypeHandler: func.isRequired,
};

const mapStateToProps = (state) => ({
  sbpLabels: getSBPLabels(state),
});

const mapDispatchToProps = (dispatch) => {
  return {
    updateAppTypeHandler: (payload) => {
      dispatch(updateAppTypeWithParams(payload));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NoResultsPage);
