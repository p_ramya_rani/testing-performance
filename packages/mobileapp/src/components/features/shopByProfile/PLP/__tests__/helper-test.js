// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable */
import {
  capitalizeWords,
  getMaximumDiscount,
  streamlineProductName,
  showSBPLabels,
  getSizeMapping,
} from '../helper';
import superagent from 'superagent';

describe('capitalizeWords', () => {
  it('should capitalise first character', () => {
    const testLabel = 'hello world';
    const testCapitalizedLabel = 'Hello World';
    expect(capitalizeWords(testLabel)).toBe(testCapitalizedLabel);
  });
});

describe('getMaximumDiscount', () => {
  const products = [
    {
      productInfo: {
        listPrice: 100,
        offerPrice: 80,
        TCPMerchantTagUSStore: '50% OFF',
      },
    },
    {
      productInfo: {
        listPrice: 100,
        offerPrice: 70,
        TCPMerchantTagUSStore: '60% OFF',
      },
    },
  ];
  it('should get max discount', () => {
    expect(getMaximumDiscount(products)).toBe(60);
  });
  it('should return 0 if no discount present', () => {
    const value = false;
    expect(getMaximumDiscount(value)).toBe(0);
  });
});

describe('streamlineProductName', () => {
  const department = 'Boys';
  it('should get streamline name 1', () => {
    const name = department + ' product name';
    expect(streamlineProductName(name, false)).toBe('product name');
  });
  it('should get streamline name 2', () => {
    const name = department + ' product name';
    expect(streamlineProductName(name, true)).toBe(name);
  });
});

describe('showSBPLabels', () => {
  const sbpLabels = {
    lbl_sbp_welcome_newuser: 'Create a custom shop for your child now!',
    lbl_sbp_text_relationship: 'How are you related?',
  };
  it('should return CMS label', () => {
    const labelId = 'lbl_sbp_text_relationship';
    const defaultText = 'Default value';
    expect(showSBPLabels(sbpLabels, labelId, defaultText)).toBe('How are you related?');
  });

  it('should return default text if CMS label is not present', () => {
    const labelId = 'lbl_sbp_department_and_size_edit_menu';
    const defaultText = 'Default value';
    expect(showSBPLabels(sbpLabels, labelId, defaultText)).toBe(defaultText);
  });
});

describe('getSizeMapping', () => {
  jest.mock('superagent');
  it('return size mapping', async () => {
    const data = [
      {
        category: 'Pajamas & Nightgowns',
        fit: '',
        size: '3-6 M',
        ranking: '22',
        all_sizes: '3-6 M',
      },
      {
        category: 'Bottoms',
        fit: '',
        size: '6-9 M',
        ranking: '11',
        all_sizes: '6-9 M',
      },
    ];
    const department = 'BABY GIRL';
    superagent.get.mockResolvedValue({data});
    const value = await getSizeMapping(department);
    expect(value).toHaveLength(0);
  });
});
