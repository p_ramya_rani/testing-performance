// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import {TouchableHighlight} from 'react-native';
import {shallow} from 'enzyme';
import {ProfileMenu} from '../ProfileMenu';

describe('Component', () => {
  let component;
  let onPressGoToEditProfile;
  let onPressToggleMenu;

  beforeEach(() => {
    onPressGoToEditProfile = jest.fn();
    onPressToggleMenu = jest.fn();
    const props = {
      theme: 1,
      goToEditProfile: onPressGoToEditProfile,
      showMenu: true,
      toggleMenu: onPressToggleMenu,
    };
    component = shallow(<ProfileMenu {...props} />);
  });

  it('Component should be defined', () => {
    expect(component).toBeDefined();
  });

  it('Component should render correctly', () => {
    expect(component).toMatchSnapshot();
  });

  it('Component should match snapshot', () => {
    component.setProps({showMenu: true});
    expect(component).toMatchSnapshot();
  });

  it('Component should match snapshot when menu is closed', () => {
    component.setProps({showMenu: false});
    expect(component).toMatchSnapshot();
  });

  it('calling onPress should call goToEditProfile', () => {
    component
      .find(TouchableHighlight)
      .first()
      .props()
      .onPress();
    expect(onPressGoToEditProfile).toHaveBeenCalled();
  });

  it('closes menu when onPress is called', () => {
    component
      .find(TouchableHighlight)
      .last()
      .props()
      .onPress();
    expect(onPressToggleMenu).toHaveBeenCalled();
  });
});
