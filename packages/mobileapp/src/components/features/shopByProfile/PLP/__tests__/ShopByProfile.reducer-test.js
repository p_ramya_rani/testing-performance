// 9fbef606107a605d69c0edbcd8029e5d 
import PLPReducer from '../ShopByProfile.reducer';

const obj = {};
const action1 = {
  type: 'UPDATE_PRODUCTS_BY_PROFILE',
  payload: {
    profileId: '1234',
  },
};
const action2 = {
  type: undefined,
  payload: {
    profileId: '1234',
  },
};

describe('PLPReducer', () => {
  it('should return value payload 1', () => {
    expect(PLPReducer(obj, action1)).toStrictEqual({
      '1234': {
        profileId: '1234',
      },
    });
  });

  it('should return value payload as is', () => {
    expect(PLPReducer(obj, action2)).toStrictEqual(obj);
  });

  it('should return value payload', () => {
    expect(PLPReducer(undefined, action1)).toStrictEqual({
      '1234': {
        profileId: '1234',
      },
    });
  });
});
