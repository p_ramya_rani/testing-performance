// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable */
import React from 'react';
import {shallow} from 'enzyme';
import Component from '../ProductsByCategories';
import renderer from 'react-test-renderer';

describe('Component', () => {
  let component;

  beforeEach(() => {
    const props = {
      category: 'Category',
      products: [],
      clickedCategory: '',
      goToPDP: jest.fn(),
      bookmarks: [],
      onAddItemToFavorites: jest.fn(),
      activeWishListProducts: [],
      favoriteList: {},
      numberOfProducts: 0,
      plpLabels: {
        lbl_plpTiles_shop_collection: '',
        lbl_add_to_bag: '',
      },
      brandId: 'tcp',
    };
    component = renderer.create(<Component {...props} />).toJSON();
  });

  it('Component should be defined', () => {
    expect(component).toBeDefined();
  });

  it('Component should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
});
