// 9fbef606107a605d69c0edbcd8029e5d 
import {
  addProductsByProfile,
  deleteProductsByProfile,
  updateProductsByProfile,
} from '../ShopByProfile.action';

const payload = {
  dummy: 'data',
};

describe('addProductsByProfile', () => {
  it('should return value payload 1', () => {
    expect(addProductsByProfile(payload)).toStrictEqual({
      type: 'ADD_PRODUCTS_BY_PROFILE',
      payload,
    });
  });
});

describe('deleteProductsByProfile', () => {
  it('should return value payload 2', () => {
    expect(deleteProductsByProfile(payload)).toStrictEqual({
      type: 'DELETE_PRODUCTS_BY_PROFILE',
      payload,
    });
  });
});

describe('updateProductsByProfile', () => {
  it('should return value payload 3', () => {
    expect(updateProductsByProfile(payload)).toStrictEqual({
      type: 'UPDATE_PRODUCTS_BY_PROFILE',
      payload,
    });
  });
});
