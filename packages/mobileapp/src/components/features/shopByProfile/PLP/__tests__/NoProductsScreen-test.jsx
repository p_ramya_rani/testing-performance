// 9fbef606107a605d69c0edbcd8029e5d 
/* eslint-disable */
import React from 'react';
import {shallow} from 'enzyme';
import {NoResultsPage} from '../NoProductsScreen';
import Button from '../NoProductsScreen';

describe('Component', () => {
  let component;
  const profile = {
    id: 1,
    name: 'Name',
    theme: 1,
    gender: 'Boy',
    month: 'August',
    year: '2015',
    interests: ['Animals'],
  };

  beforeEach(() => {
    const props = {
      profile,
      navigation: {
        navigate: jest.fn(),
        dispatch: jest.fn(),
        state: {
          params: {},
        },
      },
      updateAppTypeHandler: jest.fn(),
    };
    component = shallow(<NoResultsPage {...props} />);
  });

  it('Component should be defined', () => {
    expect(component).toBeDefined();
  });

  it('Component should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
});

describe('Button', () => {
  let component;

  beforeEach(() => {
    const props = {
      text: 'No Products',
      onPress: jest.fn(),
    };
    component = shallow(<Button {...props} />);
  });

  it('Component should be defined', () => {
    expect(component).toBeDefined();
  });

  it('Component should match snapshot', () => {
    expect(component).toMatchSnapshot();
  });
});
