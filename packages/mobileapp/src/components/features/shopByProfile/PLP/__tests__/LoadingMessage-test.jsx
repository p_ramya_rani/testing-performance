// 9fbef606107a605d69c0edbcd8029e5d 
/* eslint-disable */
import React from 'react';
import {shallow} from 'enzyme';
import {LoadingMessage} from '../LoadingMessage';

describe('Component', () => {
  let component;

  beforeEach(() => {
    const props = {
      theme: 1,
      showMessage: false,
      tabToggleHandler: jest.fn(),
    };
    component = shallow(<LoadingMessage {...props} />);
  });

  it('Component should be defined', () => {
    expect(component).toBeDefined();
  });

  it('Component should render correctly', () => {
    expect(component).toMatchSnapshot();
  });

  it('expect component to match snapshot', () => {
    component.setProps({showMessage: true});
    expect(component).toMatchSnapshot();
  });
});
