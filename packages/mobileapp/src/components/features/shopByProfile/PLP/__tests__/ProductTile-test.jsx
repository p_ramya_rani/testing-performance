// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable */
import React from 'react';
import {shallow} from 'enzyme';
import Component from '../ProductTile';
import getSwatchImgPath from '../ProductTile';

describe('Component', () => {
  let component;
  let onPressSpyGoToPDP;
  let onPressOnAddItemToFavorites;

  beforeEach(() => {
    onPressSpyGoToPDP = jest.fn();
    onPressOnAddItemToFavorites = jest.fn();
    const props = {
      goToPDP: onPressSpyGoToPDP,
      interestGrid: true,
      onAddItemToFavorites: onPressOnAddItemToFavorites,
      isBundle: false,
      product: {
        productInfo: {
          listPrice: 100,
          offerPrice: 80,
          uniqueId: '1',
          name: 'Name Prod',
        },
        favorite: false,
        productName: 'Name Prod',
        name: 'Name Prod',
        prodpartno: '1_0',
        generalProductId: '1_0',
        colorsMap: [
          {
            miscInfo: {
              badge1: {
                defaultBadge: '',
              },
            },
          },
        ],
      },
      brandId: 'tcp',
      numberOfDisplayedProds: 4,
      position: 20,
      plpLabels: {
        lbl_plpTiles_shop_collection: '',
        lbl_add_to_bag: '',
      },
    };
    component = shallow(<Component {...props} />);
  });

  it('Component should be defined', () => {
    expect(component).toBeDefined();
  });

  it('Component should render correctly', () => {
    expect(component).toMatchSnapshot();
  });

  it('Component should render correctly for 4 L2s', () => {
    onPressSpyGoToPDP = jest.fn();
    onPressOnAddItemToFavorites = jest.fn();
    const props = {
      goToPDP: onPressSpyGoToPDP,
      interestGrid: false,
      onAddItemToFavorites: onPressOnAddItemToFavorites,
      isBundle: false,
      product: {
        min_list_price: 100,
        max_list_price: 100,
        min_offer_price: 20,
        max_offer_price: 20,
        topRated: true,
        streamLinedName: 'Name',
        imagePath: 'image_path',
        favorite: false,
        productName: 'Name Prod',
        name: 'Name Prod',
        prodpartno: '1_0',
      },
      brandId: 'tcp',
      numberOfDisplayedProds: 4,
      position: 20,
      plpLabels: {
        lbl_plpTiles_shop_collection: '',
        lbl_add_to_bag: '',
      },
    };
    component = shallow(<Component {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('Component should render correctly for 20 L2s', () => {
    onPressSpyGoToPDP = jest.fn();
    onPressOnAddItemToFavorites = jest.fn();
    const props = {
      goToPDP: onPressSpyGoToPDP,
      interestGrid: false,
      onAddItemToFavorites: onPressOnAddItemToFavorites,
      isBundle: false,
      product: {
        min_list_price: 100,
        max_list_price: 100,
        min_offer_price: 20,
        max_offer_price: 20,
        topRated: true,
        streamLinedName: 'Name',
        imagePath: 'image_path',
        favorite: false,
        productName: 'Name Prod',
        name: 'Name Prod',
        prodpartno: '1_0',
      },
      brandId: 'tcp',
      numberOfDisplayedProds: 20,
      position: 17,
      plpLabels: {
        lbl_plpTiles_shop_collection: '',
        lbl_add_to_bag: '',
      },
    };
    component = shallow(<Component {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('Component should render correctly for 20 L2s', () => {
    onPressSpyGoToPDP = jest.fn();
    onPressOnAddItemToFavorites = jest.fn();
    const props = {
      goToPDP: onPressSpyGoToPDP,
      interestGrid: false,
      onAddItemToFavorites: onPressOnAddItemToFavorites,
      isBundle: false,
      product: {
        min_list_price: 100,
        max_list_price: 100,
        min_offer_price: 20,
        max_offer_price: 20,
        topRated: true,
        streamLinedName: 'Name',
        imagePath: 'image_path',
        favorite: false,
        productName: 'Name Prod',
        name: 'Name Prod',
        prodpartno: '1_0',
      },
      brandId: 'tcp',
      numberOfDisplayedProds: 4,
      position: 3,
      plpLabels: {
        lbl_plpTiles_shop_collection: '',
        lbl_add_to_bag: '',
      },
    };
    component = shallow(<Component {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('Component should handle bad product', () => {
    const props = {
      product: 'Product',
    };
    component = shallow(<Component {...props} />);
    expect(component).toMatchSnapshot();
  });
});

describe('Component for dynamic badges', () => {
  let component;
  let onPressSpyGoToPDP;
  let onPressOnAddItemToFavorites;

  beforeEach(() => {
    onPressSpyGoToPDP = jest.fn();
    onPressOnAddItemToFavorites = jest.fn();
    const props = {
      goToPDP: onPressSpyGoToPDP,
      interestGrid: true,
      onAddItemToFavorites: onPressOnAddItemToFavorites,
      isBundle: false,
      product: {
        productInfo: {
          listPrice: 100,
          offerPrice: 80,
          uniqueId: '1',
          name: 'Name Prod',
          TCPStyleTypeUS: '0002',
          TCPStyleQTYUS: '2',
        },
        favorite: false,
        productName: 'Name Prod',
        name: 'Name Prod',
        prodpartno: '1_0',
        generalProductId: '1_0',
        colorsMap: [
          {
            miscInfo: {
              badge1: {
                defaultBadge: '',
              },
            },
          },
        ],
      },
      brandId: 'tcp',
      numberOfDisplayedProds: 4,
      position: 20,
      plpLabels: {
        lbl_plpTiles_shop_collection: '',
        lbl_add_to_bag: '',
      },
      isDynamicBadgeEnabled: {
        dynamicBadgeMPACKEnabled: true,
        dynamicBadgeVirtualMPACKEnabled: true,
        dynamicBadgeSetEnabled: true,
        dynamicBadgeVirtualSetEnabled: true,
      },
    };
    component = shallow(<Component {...props} />);
  });

  it('Component should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
});

describe('Component for dynamic badges for interested product', () => {
  let component;
  let onPressSpyGoToPDP;
  let onPressOnAddItemToFavorites;

  beforeEach(() => {
    onPressSpyGoToPDP = jest.fn();
    onPressOnAddItemToFavorites = jest.fn();
    const props = {
      goToPDP: onPressSpyGoToPDP,
      interestGrid: true,
      onAddItemToFavorites: onPressOnAddItemToFavorites,
      isBundle: false,
      product: {
        TCPStyleTypeUS: '0002',
        TCPStyleQTYUS: '2',
        product_name: 'Product Name',
        name: 'name',
        generalProductId: '232323232_12',
        colorsMap: [
          {
            miscInfo: {
              badge1: {
                defaultBadge: '',
              },
            },
          },
        ],
        productInfo: {
          favorite: false,
          productName: 'Name Prod',
          name: 'Name Prod',
          prodpartno: '1_0',
          generalProductId: '1_0',
          offerPrice: 1.11,
          listPrice: 20.12,
        },
      },
      brandId: 'tcp',
      numberOfDisplayedProds: 4,
      position: 20,
      plpLabels: {
        lbl_plpTiles_shop_collection: '',
        lbl_add_to_bag: '',
      },
      isDynamicBadgeEnabled: {
        dynamicBadgeMPACKEnabled: true,
        dynamicBadgeVirtualMPACKEnabled: true,
        dynamicBadgeSetEnabled: true,
        dynamicBadgeVirtualSetEnabled: true,
      },
    };
    component = shallow(<Component {...props} />);
  });

  it('Component should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
});
