// 9fbef606107a605d69c0edbcd8029e5d 
/* eslint-disable */
import React from 'react';
import {shallow} from 'enzyme';
import {TopMatchingMsg} from '../TopMatchingMsg';

describe('Component', () => {
  let component;

  beforeEach(() => {
    const props = {
      name: '',
      theme: 1,
      sbpLabels: {},
    };
    component = shallow(<TopMatchingMsg {...props} />);
  });

  it('Component should be defined', () => {
    expect(component).toBeDefined();
  });

  it('Component should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
});
