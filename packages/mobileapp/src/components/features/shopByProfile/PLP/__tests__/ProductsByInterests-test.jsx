// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable */
import React from 'react';
import {shallow} from 'enzyme';
import Component from '../ProductsByInterests';
import renderer from 'react-test-renderer';

describe('Component', () => {
  let component;

  beforeEach(() => {
    const props = {
      interest: 'Interest',
      goToPDP: jest.fn(),
      theme: 1,
      products: [],
      favoriteList: {},
      showAllProducts: [],
      interestGrid: true,
      retrieveMoreProductsFromUnbxd: jest.fn(),
      unbxdInterestCached: [],
      interestOffSetArray: {},
      onAddItemToFavorites: jest.fn(),
      activeWishListProducts: null,
      plpLabels: {
        lbl_plpTiles_shop_collection: '',
        lbl_add_to_bag: '',
      },
      brandId: 'tcp',
      productCount: 20,
    };
    component = renderer.create(<Component {...props} />).toJSON();
  });

  it('Component should be defined', () => {
    expect(component).toBeDefined();
  });

  it('Component should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
});
