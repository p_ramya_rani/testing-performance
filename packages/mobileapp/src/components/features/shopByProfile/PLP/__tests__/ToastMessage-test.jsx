// 9fbef606107a605d69c0edbcd8029e5d 
/* eslint-disable */
import React from 'react';
import {shallow} from 'enzyme';
import ToastMessage from '../ToastMessage';
import {TouchableOpacity} from 'react-native';

describe('ToastMessage', () => {
  let component;
  let onPressSpy;

  beforeEach(() => {
    onPressSpy = jest.fn();
    const props = {
      name: 'test',
      toggleToastMessage: onPressSpy,
    };
    component = shallow(<ToastMessage {...props} />);
  });

  it('ToastMessage should be defined', () => {
    expect(component).toBeDefined();
  });

  it('ToastMessage should render correctly', () => {
    expect(component).toMatchSnapshot();
  });

  it('dismisses notification onPress', () => {
    component
      .find(TouchableOpacity)
      .first()
      .props()
      .onPress();
    expect(onPressSpy).toHaveBeenCalled();
  });
});
