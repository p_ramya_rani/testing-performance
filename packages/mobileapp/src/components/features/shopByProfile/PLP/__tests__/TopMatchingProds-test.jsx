// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable */
import React from 'react';
import {shallow} from 'enzyme';
import Component from '../TopMatchingProds';

describe('Component', () => {
  let component;

  beforeEach(() => {
    const props = {
      product: {
        productInfo: {
          listPrice: 100,
          offerPrice: 80,
          uniqueId: '1',
          name: 'Name Prod',
        },
        favorite: false,
      },
      goToPDP: jest.fn(),
      onAddItemToFavorites: jest.fn(),
    };
    component = shallow(<Component {...props} />);
  });

  it('Component should be defined', () => {
    expect(component).toBeDefined();
  });

  it('Component should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
});

describe('Component for dynamic badges for MPACK', () => {
  let component;

  beforeEach(() => {
    const props = {
      product: {
        productInfo: {
          listPrice: 100,
          offerPrice: 80,
          uniqueId: '1',
          name: 'Name Prod',
          tcpStyleType: '0002',
          tcpStyleQty: '2',
        },
        favorite: false,
      },
      goToPDP: jest.fn(),
      onAddItemToFavorites: jest.fn(),
      isDynamicBadgeEnabled: {
        dynamicBadgeMPACKEnabled: true,
        dynamicBadgeVirtualMPACKEnabled: true,
        dynamicBadgeSetEnabled: true,
        dynamicBadgeVirtualSetEnabled: true,
      },
    };
    component = shallow(<Component {...props} />);
  });

  it('Component should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
});
