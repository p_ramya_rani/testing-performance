// 9fbef606107a605d69c0edbcd8029e5d
import superagent from 'superagent';
import React from 'react';
import {shallow} from 'enzyme';
import {PLP} from '../index';

describe('Component', () => {
  let component;
  beforeEach(() => {
    const props = {
      navigation: {
        state: {
          params: {
            categoriesLoading: true,
          },
          actions: {},
        },
      },
      profiles: [],
      productsByProfile: jest.fn(),
      userId: '1',
      wishlistsSummaries: [],
      activeWishListProducts: [],
      activeWishListId: '1',
      isDataLoading: false,
      L2Categories: [],
      plpLabels: {},
      monetateProductsRefreshTime: 1000,
      unbxdProductsRefreshFrequency: 10000,
    };
    component = shallow(<PLP {...props} />);
  });

  it('Component should be defined', () => {
    expect(component).toBeDefined();
  });

  it('Component should render correctly', () => {
    expect(component).toMatchSnapshot();
  });

  it('Component should match snapshot loading interests', () => {
    component.setState({interestsLoading: true});
    expect(component).toMatchSnapshot();
  });

  it('Component should match snapshot loading top matches', () => {
    component.setState({topMatchingLoading: true});
    expect(component).toMatchSnapshot();
  });

  it('Component should not show menu', () => {
    component.setState({showMenu: false});
    expect(component).toMatchSnapshot();
  });

  it('should sortProductsByFavoriteColor', () => {
    const favoriteColors = [];
    const data = [
      {
        generalProductId: '3019347_071',
        pdpUrl: '',
        department: '',
        name: 'Baby Boys Monkey Bodysuit 5-Pack',
        imagePath:
          'https://assets.theplace.com/image/upload/v1/ecom/assets/products/TCP/3019347/3019347_071.jpg',
        childInterest: 'Animals',
        uniqueId: '3019347_071',
        miscInfo: {
          rating: 0,
          isInDefaultWishlist: false,
        },
        categoryPath2: ['Tops', 'Bodysuits'],
        TCPColor: 'BLUE',
        categoryPath2_catMap: ['47504>714003|Baby>Tops', '47504>453004|Baby>Bodysuits'],
        productCatID: '714003',
      },
      {
        generalProductId: '3019350_BQ',
        pdpUrl: '',
        department: '',
        name: 'Baby Boys Safari Bodysuit 5-Pack',
        imagePath:
          'https://assets.theplace.com/image/upload/v1/ecom/assets/products/TCP/3019350/3019350_BQ.jpg',
        childInterest: 'Animals',
        uniqueId: '3019350_BQ',
        miscInfo: {
          rating: 4.8,
          isInDefaultWishlist: false,
        },
        categoryPath2: ['Tops', 'Bodysuits'],
        TCPColor: 'MULTI',
        categoryPath2_catMap: ['47504>714003|Baby>Tops', '47504>453004|Baby>Bodysuits'],
        productCatID: '714003',
      },
      {
        generalProductId: '3017907_14',
        pdpUrl: '',
        department: '',
        name: 'Baby Boys Bear Straw Hat',
        imagePath:
          'https://assets.theplace.com/image/upload/v1/ecom/assets/products/TCP/3017907/3017907_14.jpg',
        childInterest: 'Animals',
        uniqueId: '3017907_14',
        miscInfo: {
          rating: 0,
          isInDefaultWishlist: false,
        },
        categoryPath2: ['Tops'],
        TCPColor: 'GRAY',
        categoryPath2_catMap: ['47511>49012|Girl>Tops'],
      },
    ];

    expect(component.instance().sortProductsByFavoriteColor(data, favoriteColors)).toEqual(data);
  });

  it('getDepartmentID should return id', () => {
    const department = 'Girl';
    const sampleData = [
      {
        categoryContent: {
          id: '12323',
          name: 'Girl',
        },
        categoryId: '12345',
      },
      {
        categoryContent: {
          id: '76543',
          name: 'Boy',
        },
        categoryId: '76543',
      },
      {
        categoryContent: {
          id: '98765',
          name: 'Toddler Girl',
        },
        categoryId: '98765',
      },
    ];
    const expectedResult = '12323';
    component.setProps({L2Categories: sampleData});
    expect(component.instance().getDepartmentID(department)).toEqual(expectedResult);
  });

  it('getProfilesL2Categories calls getL2Categories', () => {
    const profile = {
      id: 'bd009621-7689-4add-86d8-2130eff23b19',
      theme: 10,
      size:
        '{"Tops":{"fit":"Regular","size":["10"]},"Bottoms":{"fit":"Regular","size":["10"]},"Jackets & Outerwear":{"size":["10"]},"Pajamas":{"size":["10"]},"Shoes":{"size":["YOUTH 13"]}}',
      relation: 'Parent',
      gender: 'BOY',
      name: 'Boy90',
      fit: 'REGULAR',
      month: 'August',
      shoe: 'TODDLER 1',
      year: '2015',
      interests: 'Ninjas,Gaming,Sports',
      styles: '0',
      favorite_colors: 'pink,blue',
      refresh: true,
      isSBP: true,
      index: 0,
    };
    component.instance().getL2Categories = jest.fn(() => []);
    component.setState({profile});
    component.instance().getProfilesL2Categories();
    expect(component.instance().getL2Categories).toBeCalled();
  });

  it('should return true or false category is loading', () => {
    const isLoading = component.instance().props.navigation.state.params.categoriesLoading;
    expect(component.instance().categoriesLoading()).toEqual(isLoading);
  });

  it('should fetch unbxd sizes', async () => {
    const getAPICall = jest.spyOn(superagent, 'get').mockImplementation(() => []);
    await component.instance().fetchSizesFromUnbxd();
    expect(getAPICall).toHaveBeenCalled();
  });

  it('calls componentDidMount', () => {
    const spy = jest.spyOn(PLP.prototype, 'componentDidMount');
    const props = {
      navigation: {
        state: {
          params: {
            categoriesLoading: true,
          },
          actions: {},
        },
      },
      profiles: [],
      productsByProfile: jest.fn(),
      userId: '1',
      wishlistsSummaries: [],
      activeWishListProducts: [],
      activeWishListId: '1',
      isDataLoading: false,
      L2Categories: [],
      plpLabels: {},
      monetateProductsRefreshTime: 1000,
      unbxdProductsRefreshFrequency: 10000,
    };
    shallow(<PLP {...props} />);
    expect(spy).toHaveBeenCalledTimes(1);
  });
});
