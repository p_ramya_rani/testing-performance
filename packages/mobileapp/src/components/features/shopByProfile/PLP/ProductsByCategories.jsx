// 9fbef606107a605d69c0edbcd8029e5d
import React, {useEffect, useState, useCallback} from 'react';
import {View, Text, Image, FlatList} from 'react-native';
import {executeUnbxdAPICall} from '@tcp/core/src/services/handler/handler';
import endpoints from '@tcp/core/src/services/endpoints';
import {PropTypes} from 'prop-types';
import {checkIfBundleProduct} from '@tcp/core/src/services/abstractors/productListing/productParser';
import ProductTile from './ProductTile';
import ViewMoreButton from '../../../common/atoms/Button/ViewMoreButton';
import {getMaximumDiscount} from './helper';
import styles from './productsByCategories.styles';

/**
 * @function getProducts This function modifies the product data
 * @param {Array} products The products which needs to be modified
 * @param {Array} activeWishListProducts The favorite products list of current profile.
 */
function getProducts(products, activeWishListProducts) {
  if (!Array.isArray(activeWishListProducts)) return products;
  const favoriteProducts = {};
  activeWishListProducts.forEach((product) => {
    favoriteProducts[product.skuInfo.colorProductId] = product;
  });
  return products.map((product) => {
    let {generalProductId} = product;
    generalProductId = generalProductId || product.prodpartno;
    const productMod = product;
    if (favoriteProducts[generalProductId]) {
      productMod.itemInfo = favoriteProducts[generalProductId].itemInfo;
      productMod.favorite = true;
    } else {
      productMod.favorite = false;
    }
    return productMod;
  });
}

const arrowImgSource = require('../../../../assets/images/shopByProfile/elements/category-header-arrow2x.png');

/* eslint-disable */
const ProductsByCategories = (props) => {
  const {
    category,
    products,
    clickedCategory,
    goToPDP,
    openQuickAdd,
    onAddItemToFavorites,
    activeWishListProducts,
    numberOfProducts,
    plpLabels,
    brandId,
    profile,
    categoryPathID,
    filterCategoryHandler,
    filter,
    isDynamicBadgeEnabled,
    quickViewLoader,
    quickViewProductId,
  } = props;

  const [productNumber, setProductNumber] = useState(4);
  const [initialNumberProducts, setInitialNumberProducts] = useState(products.length);
  const [productsForRendering, setProductsForRendering] = useState(products);
  useEffect(() => {
    setProductsForRendering(products);
    if (!numberOfProducts || productNumber === numberOfProducts) return;
    setProductNumber(numberOfProducts);
  }, [products]);

  const productsMod = getProducts(products, activeWishListProducts);
  let showDiscount = true;
  let discountMessage;
  const maxPercentage = getMaximumDiscount(productsMod);
  if (maxPercentage <= 0) showDiscount = false;
  if (showDiscount) {
    discountMessage = `Up to ${maxPercentage}% OFF ${category}`;
  } else {
    const profileName = profile && profile.name && profile.name;
    discountMessage = `For ${profileName} In ${category}`;
  }

  /**
   * @function fetchNextProducts This function fetches the next set of products when the
   *                             scroll reaches of the screen
   */
  const fetchNextProducts = async () => {
    const payload = {
      body: {
        start: initialNumberProducts + 1,
        rows: 10,
        version: 'V2',
        'facet.multiselect': true,
        'variants.count': 0,
        selectedfacet: true,
        'p-id': categoryPathID,
        filter,
        pagetype: 'boolean',
        fields:
          'productimage,alt_img,style_partno,giftcard,TCPProductIndUSStore,TCPWebOnlyFlagUSStore,TCPWebOnlyFlagCanadaStore,TCPFitMessageUSSstore,TCPFit,product_name,TCPColor,top_rated,imagename,productid,uniqueId,favoritedcount,TCPBazaarVoiceReviewCount,categoryPath3_catMap,categoryPath2_catMap,product_short_description,style_long_description,min_list_price,min_offer_price,TCPBazaarVoiceRating,product_long_description,seo_token,variantCount,prodpartno,variants,v_tcpfit,v_qty,v_tcpsize,style_name,v_item_catentry_id,v_listprice,v_offerprice,v_qty,variantId,auxdescription,list_of_attributes,additional_styles,TCPLoyaltyPromotionTextUSStore,TCPLoyaltyPLCCPromotionTextUSStore,v_variant,low_offer_price,high_offer_price,low_list_price,high_list_price,long_product_title,TCPMerchantTagUSStore,TCPStyleTypeUS,TCPStyleQTYUS',
      },
      webService: endpoints.getProductviewbyCategory,
    };
    const unbxdResponse = await executeUnbxdAPICall(payload);
    const listOfProducts = unbxdResponse.body.response.products;
    setInitialNumberProducts(initialNumberProducts + 10);
    setProductsForRendering((prevState) => [...prevState, ...listOfProducts]);
  };

  const productsToRender = productsMod && productsMod.slice(0, productNumber);
  const keyExtractor = useCallback((item) => item.uniqueId, []);

  const renderItem = useCallback(
    ({item, index}) => (
      <ProductTile
        product={item}
        onAddItemToFavorites={onAddItemToFavorites}
        isBundle={checkIfBundleProduct(item)}
        goToPDP={goToPDP}
        openQuickAdd={openQuickAdd}
        quickViewLoader={quickViewLoader}
        quickViewProductId={quickViewProductId}
        plpLabels={plpLabels}
        brandId={brandId}
        numberOfDisplayedProds={productNumber}
        position={index}
        isDynamicBadgeEnabled={isDynamicBadgeEnabled}
      />
    ),
    [quickViewLoader],
  );

  return (
    <View>
      <View
        style={{
          ...styles.bannerContainer,
        }}>
        <View style={styles.bannerTextContainer}>
          <Text style={styles.bannerText}>{discountMessage}</Text>
        </View>
        <Image source={arrowImgSource} style={styles.bannerImage} />
      </View>
      <View style={styles.productsContainer}>
        {category === clickedCategory ? (
          <FlatList
            data={productsForRendering}
            keyExtractor={keyExtractor}
            initialNumToRender={6}
            numColumns={2}
            renderItem={renderItem}
            onEndReached={fetchNextProducts}
            onEndReachedThreshold={0.9}
          />
        ) : (
          productsToRender &&
          productsToRender.length && (
            <FlatList
              data={productsToRender}
              keyExtractor={keyExtractor}
              initialNumToRender={6}
              numColumns={2}
              renderItem={renderItem}
            />
          )
        )}
      </View>
      <View style={styles.viewMore}>
        {productsMod.length > productNumber && clickedCategory !== category ? (
          <ViewMoreButton category={category} onPress={() => filterCategoryHandler(category)}>
            VIEW ALL
          </ViewMoreButton>
        ) : (
          <View style={styles.viewMoreEmpty} />
        )}
      </View>
    </View>
  );
};

ProductsByCategories.propTypes = {
  category: PropTypes.string.isRequired,
  products: PropTypes.shape({}).isRequired,
  clickedCategory: PropTypes.string.isRequired,
  goToPDP: PropTypes.func.isRequired,
  openQuickAdd: PropTypes.func.isRequired,
  onAddItemToFavorites: PropTypes.func.isRequired,
  activeWishListProducts: PropTypes.shape([]).isRequired,
  favoriteList: PropTypes.shape({}).isRequired,
  numberOfProducts: PropTypes.number.isRequired,
  plpLabels: PropTypes.shape({}).isRequired,
  brandId: PropTypes.string.isRequired,
  profile: PropTypes.shape({}).isRequired,
  categoryPathID: PropTypes.string.isRequired,
  filterCategoryHandler: PropTypes.func.isRequired,
  filter: PropTypes.string.isRequired,
  quickViewLoader: PropTypes.bool.isRequired,
  quickViewProductId: PropTypes.string.isRequired,
};

export default ProductsByCategories;
