// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import {View, Text, Image} from 'react-native';
import {connect} from 'react-redux';
import {number, shape, string} from 'prop-types';
import Themes from '../ShopByProfile.theme';
import styles from './topMatchingMsg.styles';
import {getSBPLabels} from '../ShopByProfile.selector';

export const TopMatchingMsg = (props) => {
  const {name, theme, sbpLabels} = props;
  const topMatchMessageCMSLabel =
    sbpLabels && sbpLabels.lbl_sbp_top_matching_title
      ? sbpLabels.lbl_sbp_top_matching_title.replace('[Name]', name)
      : `TOP MATCHES For ${name}`;

  const leftStarsImage = Themes[theme || 0].leftStar;
  const rightStatsImage = Themes[theme || 0].rightStar;

  return (
    <View style={{...styles.container}}>
      <Image source={leftStarsImage} style={styles.img} />
      <View style={{...styles.textContainer}}>
        <Text
          style={{
            color: Themes[theme || 0].accent,
            ...styles.topText,
          }}>
          {topMatchMessageCMSLabel.split(' ').slice(0, 2).join(' ')}
        </Text>
        <Text style={{...styles.bottomText}}>
          {topMatchMessageCMSLabel.split(' ').slice(2, 4).join(' ')}
        </Text>
      </View>
      <Image source={rightStatsImage} style={styles.img} />
    </View>
  );
};

TopMatchingMsg.propTypes = {
  name: string.isRequired,
  sbpLabels: shape({
    lbl_sbp_top_matching_title: string,
  }).isRequired,
  theme: number.isRequired,
};

const mapStateToProps = (state) => ({
  sbpLabels: getSBPLabels(state),
});

export default connect(mapStateToProps)(TopMatchingMsg);
