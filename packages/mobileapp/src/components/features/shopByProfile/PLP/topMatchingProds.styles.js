// 9fbef606107a605d69c0edbcd8029e5d
import {StyleSheet} from 'react-native';
import {
  colorBlack,
  colorWhite,
  colorMortar,
  colorNero,
  colorFireEngingeRed,
  fontFamilyNunitoRegular,
  fontFamilyNunitoSemiBold,
  fontFamilyNunitoExtraBold,
  fontSizeTwelve,
} from '../ShopByProfile.styles';

const styles = StyleSheet.create({
  container: {
    backgroundColor: colorWhite,
    borderRadius: 11,
    elevation: 5,
    height: '80%',
    marginLeft: 14,
    marginTop: 5,
    shadowColor: colorBlack,
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.2,
    shadowRadius: 5.65,
    width: 150,
  },
  favoriteContainer: {
    height: 24,
    position: 'absolute',
    right: '2%',
    width: 24,
  },
  favoriteImage: {
    alignSelf: 'flex-end',
    height: 24,
    position: 'absolute',
    right: 5,
    top: 5,
    width: 24,
  },
  goToPDPButton: {
    marginBottom: 18,
    marginTop: 20,
    zIndex: -10,
  },
  productContainer: {
    alignItems: 'flex-start',
    marginTop: '-37%',
    paddingHorizontal: 10,
  },
  productDiscountText: {
    color: colorFireEngingeRed,
    fontFamily: fontFamilyNunitoSemiBold,
    fontSize: 10,
    marginLeft: 5,
  },
  productImage: {
    height: '88%',
    marginTop: 5,
    width: '100%',
  },
  productName: {
    color: colorNero,
    fontFamily: fontFamilyNunitoRegular,
    fontSize: fontSizeTwelve,
  },
  productNameContainer: {
    width: '90%',
  },
  productPriceContainer: {
    flexDirection: 'row',
    marginBottom: 5,
  },
  productPriceText: {
    color: colorMortar,
    fontFamily: fontFamilyNunitoRegular,
    fontSize: 10,
    textDecorationLine: 'line-through',
  },
  productText: {
    color: colorFireEngingeRed,
    fontFamily: fontFamilyNunitoExtraBold,
    fontSize: 15,
  },
});

export default styles;
