// 9fbef606107a605d69c0edbcd8029e5d
import React, {useState, useCallback} from 'react';
import {View, Text, Image, FlatList} from 'react-native';
import {PropTypes} from 'prop-types';
import ProductTile from './ProductTile';
import ViewMoreButton from '../../../common/atoms/Button/ViewMoreButton';
import {CTInterestIcons} from '../ShopByProfile.constants';
import Themes from '../ShopByProfile.theme';
import {capitalizeWords, isDeviceIPhone12} from './helper';
import styles from './productsByInterests.styles';

/**
 * @function getProducts This function modifies the product data
 * @param {Array} products The products which needs to be modified
 * @param {Array} activeWishListProducts The favorite products list of current profile.
 */
function getProducts(listProducts, activeWishListProducts) {
  if (!Array.isArray(activeWishListProducts)) return listProducts;
  const favoriteProducts = {};
  activeWishListProducts.forEach((product) => {
    favoriteProducts[product.skuInfo.colorProductId] = product;
  });
  return listProducts.map((product) => {
    const {generalProductId} = product;
    const productMod = product;
    if (favoriteProducts[generalProductId]) {
      productMod.itemInfo = favoriteProducts[generalProductId].itemInfo;
      productMod.favorite = true;
    } else {
      productMod.favorite = false;
    }
    return productMod;
  });
}

const bannerImgPath = require('../../../../assets/images/shopByProfile/elements/category-header-arrow2x.png');

/* eslint-disable */
const ProductsByInterests = (props) => {
  const {
    interest,
    products,
    theme,
    goToPDP,
    openQuickAdd,
    favoriteList,
    showAllProducts,
    interestGrid,
    retrieveMoreProductsFromUnbxd,
    unbxdInterestCached,
    interestOffSetArray,
    onAddItemToFavorites,
    activeWishListProducts,
    plpLabels,
    brandId,
    productCount,
    maxDiscount,
    profile,
    filterInterestHandler,
    isDynamicBadgeEnabled,
    quickViewLoader,
    quickViewProductId,
  } = props;
  const [viewMorePressed] = useState(false);
  const capitalizeInterest = capitalizeWords(interest);
  const iconPath = CTInterestIcons[capitalizeInterest];
  let showDiscount = true;
  let discountMessage;
  const listProducts = showAllProducts ? showAllProducts : products.products;
  const productsModified = getProducts(listProducts, activeWishListProducts);
  const specificInterest = unbxdInterestCached.filter((product) => product.component === interest);
  const bannerHeightAdjustment = capitalizeInterest.split(' ').length >= 3 ? 90 : 80;
  const specificInterestLength =
    specificInterest && specificInterest[0] ? specificInterest[0].items.length : 0;

  let productsLength = viewMorePressed
    ? interestOffSetArray[interest]
    : productCount
    ? productCount
    : interestOffSetArray[interest];

  if (showAllProducts) productsLength = showAllProducts.length;
  const iPhone12StylingArrow = isDeviceIPhone12() ? {marginLeft: 0} : {};

  const productsToRender = productsModified && productsModified.slice(0, productsLength);

  const keyExtractor = useCallback((item) => item.uniqueId, []);
  const renderItem = useCallback(
    ({item, index}) => (
      <ProductTile
        onAddItemToFavorites={onAddItemToFavorites}
        product={item}
        goToPDP={goToPDP}
        openQuickAdd={openQuickAdd}
        favoriteList={favoriteList}
        interestGrid={interestGrid}
        plpLabels={plpLabels}
        quickViewLoader={quickViewLoader}
        quickViewProductId={quickViewProductId}
        brandId={brandId}
        numberOfDisplayedProds={productsLength}
        position={index}
        isDynamicBadgeEnabled={isDynamicBadgeEnabled}
      />
    ),
    [quickViewLoader],
  );

  if (maxDiscount <= 0) showDiscount = false;
  if (showDiscount) {
    discountMessage = `Up to ${maxDiscount}% OFF Select ${capitalizeInterest} Products`;
  } else {
    discountMessage = `For ${profile.name} In ${capitalizeInterest}`;
  }

  return (
    <View>
      <View style={{...styles.bannerContainer, height: bannerHeightAdjustment}}>
        <Image
          source={iconPath}
          style={{...styles.bannerImageOne, tintColor: Themes[theme || 0].accent}}
        />
        <View style={styles.bannerTextContainer}>
          <Text style={styles.bannerText}>{discountMessage}</Text>
        </View>
        <Image source={bannerImgPath} style={[styles.bannerImageTwo, iPhone12StylingArrow]} />
      </View>
      <View style={styles.productsContainer}>
        {productsToRender && productsToRender.length && (
          <FlatList
            data={productsToRender}
            keyExtractor={keyExtractor}
            initialNumToRender={6}
            numColumns={2}
            renderItem={renderItem}
          />
        )}
      </View>
      {showAllProducts && showAllProducts.length > 0 ? null : (
        <View style={styles.viewMore}>
          {productsLength >= specificInterestLength ? (
            <View style={styles.viewMoreEmpty} />
          ) : (
            <ViewMoreButton
              onPress={() => filterInterestHandler(interest)}
              interest={interest}
              retrieveMoreProductsFromUnbxd={retrieveMoreProductsFromUnbxd}
              interestGrid={interestGrid}>
              VIEW ALL
            </ViewMoreButton>
          )}
        </View>
      )}
    </View>
  );
};
/* eslint-enable */

ProductsByInterests.propTypes = {
  interest: PropTypes.string.isRequired,
  goToPDP: PropTypes.func.isRequired,
  openQuickAdd: PropTypes.func.isRequired,
  theme: PropTypes.number.isRequired,
  products: PropTypes.shape({}).isRequired,
  favoriteList: PropTypes.shape({}).isRequired,
  showAllProducts: PropTypes.shape([]).isRequired,
  interestGrid: PropTypes.bool.isRequired,
  retrieveMoreProductsFromUnbxd: PropTypes.func.isRequired,
  unbxdInterestCached: PropTypes.shape([]).isRequired,
  interestOffSetArray: PropTypes.shape({}).isRequired,
  onAddItemToFavorites: PropTypes.func.isRequired,
  activeWishListProducts: PropTypes.shape([]).isRequired,
  plpLabels: PropTypes.shape({}).isRequired,
  brandId: PropTypes.string.isRequired,
  productCount: PropTypes.number.isRequired,
  profile: PropTypes.shape({}).isRequired,
  filterInterestHandler: PropTypes.func.isRequired,
  quickViewLoader: PropTypes.bool.isRequired,
  quickViewProductId: PropTypes.string.isRequired,
};

export default ProductsByInterests;
