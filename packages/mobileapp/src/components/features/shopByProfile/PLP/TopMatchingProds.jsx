// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import FastImage from '@stevenmasini/react-native-fast-image';
import {getAPIConfig, getBadgeParam} from '@tcp/core/src/utils';
import {bool, func, number, shape, string} from 'prop-types';
import {streamlineProductName} from './helper';
import {IMAGES_PARAMS} from '../ShopByProfile.constants';
import styles from './topMatchingProds.styles';

const heartFilledImgSrc = require('../../../../assets/images/shopByProfile/elements/heart-filled_2020-09-02/heart-filled.png');
const heartImgSrc = require('../../../../assets/images/shopByProfile/elements/heart_2020-09-02/heart.png');

const TopMatchingProducts = ({product, goToPDP, onAddItemToFavorites, isDynamicBadgeEnabled}) => {
  const {productInfo} = product;
  const productDiscount = productInfo.TCPMerchantTagUSStore
    ? productInfo.TCPMerchantTagUSStore
    : null;

  const {assetHostTCP, productAssetPathTCP, brandId, productAssetPathGYM} = getAPIConfig();
  const assetPath = brandId === 'gym' ? productAssetPathGYM : productAssetPathTCP;
  const prodIdForImg = productInfo.uniqueId;
  const prodIdForImgNoUnderscore = prodIdForImg.split('_')[0];
  const badgeParam = getBadgeParam(
    isDynamicBadgeEnabled,
    product?.productInfo?.tcpStyleType,
    product?.productInfo?.tcpStyleQty,
  );
  const imagePath = `${assetHostTCP}${badgeParam}${IMAGES_PARAMS}${assetPath}/${prodIdForImgNoUnderscore}/${prodIdForImg}.jpg`;
  const modifiedListPrice =
    productInfo &&
    productInfo.listPrice &&
    productInfo.listPrice.toString().split('.') &&
    productInfo.listPrice.toString().split('.')[1] &&
    productInfo.listPrice.toString().split('.')[1].length > 1
      ? productInfo.listPrice
      : `${productInfo.listPrice}0`;
  const streamLinedName = streamlineProductName(productInfo.name);

  return (
    <View style={{...styles.container}}>
      <TouchableOpacity
        style={styles.favoriteContainer}
        onPress={() => onAddItemToFavorites(product)}
        accessibilityRole="button">
        {product.favorite ? (
          <Image source={heartFilledImgSrc} style={{...styles.favoriteImage}} />
        ) : (
          <Image source={heartImgSrc} style={{...styles.favoriteImage}} />
        )}
      </TouchableOpacity>
      <View style={styles.goToPDPButton}>
        <TouchableOpacity
          onPress={() => {
            goToPDP(product);
          }}
          accessibilityRole="button">
          <FastImage
            style={styles.productImage}
            source={{
              uri: imagePath,
            }}
            resizeMode="contain"
          />
        </TouchableOpacity>
      </View>
      <View style={styles.productContainer}>
        <Text style={styles.productText}>
          {`$${
            productInfo.offerPrice === undefined
              ? productInfo.listPrice.toFixed(2)
              : productInfo.offerPrice.toFixed(2)
          }`}
        </Text>
        <View style={styles.productPriceContainer}>
          <Text style={styles.productPriceText}>{`$${modifiedListPrice}`}</Text>
          <Text style={styles.productDiscountText}>{productDiscount}</Text>
        </View>
        <View style={styles.productNameContainer}>
          <Text style={styles.productName} numberOfLines={2} ellipsizeMode="tail">
            {streamLinedName}
          </Text>
        </View>
      </View>
    </View>
  );
};

TopMatchingProducts.propTypes = {
  goToPDP: func.isRequired,
  isDynamicBadgeEnabled: shape({}).isRequired,
  onAddItemToFavorites: func.isRequired,
  product: shape({
    favorite: bool,
    productInfo: shape({
      listPrice: number,
      name: string,
      offerPrice: number,
      TCPMerchantTagUSStore: bool,
      tcpStyleQty: number,
      tcpStyleType: string,
      uniqueId: string,
    }),
  }).isRequired,
};

export default TopMatchingProducts;
