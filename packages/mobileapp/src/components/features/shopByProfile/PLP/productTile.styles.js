// 9fbef606107a605d69c0edbcd8029e5d
import {StyleSheet, Dimensions} from 'react-native';
import {
  colorBlack,
  colorWhite,
  colorMortar,
  colorNero,
  colorFireEngingeRed,
  colorGainsboro,
  colorPrussian,
  fontFamilyNunitoRegular,
  fontFamilyNunitoSemiBold,
  fontFamilyNunitoExtraBold,
  fontSizeTwelve,
} from '../ShopByProfile.styles';

const {width} = Dimensions.get('window');

const styles = StyleSheet.create({
  bundleText: {
    color: colorWhite,
    fontFamily: fontFamilyNunitoExtraBold,
    fontSize: 16,
    margin: 12,
    textAlign: 'center',
  },
  bundleView: {
    backgroundColor: colorPrussian,
    borderRadius: 16,
    marginTop: 5,
  },
  container: {
    backgroundColor: colorWhite,
    borderBottomColor: colorGainsboro,
    borderBottomWidth: 1,
    borderRadius: 11,
    elevation: 5,
    margin: 5,
    minHeight: 320,
    padding: 10,
    shadowColor: colorBlack,
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.2,
    shadowRadius: 5.65,
    width: (width - 30) / 2,
  },
  favoriteImage: {
    alignSelf: 'flex-end',
    flex: 2,
    height: 24,
    width: 24,
  },
  goToPDP: {
    height: 184,
    marginTop: '-1%',
    resizeMode: 'stretch',
    width: 150,
  },
  listPrice: {
    flexDirection: 'row',
    marginTop: 5,
  },
  nameContainer: {
    height: 45,
    width: '100%',
  },
  offerPrice: {
    color: colorFireEngingeRed,
    fontFamily: fontFamilyNunitoExtraBold,
    fontSize: 14,
  },
  percentage: {
    color: colorFireEngingeRed,
    fontFamily: fontFamilyNunitoSemiBold,
    fontSize: 10,
    marginLeft: 5,
  },
  productRating: {
    height: 27,
  },
  productTileSection1: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  productTileSection3: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: -10,
    marginTop: 10,
  },
  textFour: {
    color: colorMortar,
    fontFamily: fontFamilyNunitoRegular,
    fontSize: 10,
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid',
  },
  textOne: {
    color: colorNero,
    flex: 6,
    fontFamily: fontFamilyNunitoExtraBold,
    fontSize: fontSizeTwelve,
    minHeight: 24,
  },
  textOneAdd: {
    fontFamily: fontFamilyNunitoRegular,
    fontSize: 12,
    marginBottom: -18,
    paddingRight: 10,
  },
  textThree: {
    color: colorFireEngingeRed,
    fontFamily: fontFamilyNunitoExtraBold,
    fontSize: 15,
    fontWeight: '800',
  },
});

export default styles;
