// 9fbef606107a605d69c0edbcd8029e5d 
import {StyleSheet} from 'react-native';
import {
  colorMortar,
  fontFamilyNunitoSemiBold,
  fontFamilyNunitoBlack,
  fontSizeFourteen,
} from '../ShopByProfile.styles';

const styles = StyleSheet.create({
  bottomText: {
    color: colorMortar,
    fontFamily: fontFamilyNunitoSemiBold,
    fontSize: fontSizeFourteen,
    fontWeight: '600',
  },
  container: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    width: '100%',
  },
  img: {
    height: 30,
    width: 47,
  },
  textContainer: {
    alignItems: 'center',
    marginHorizontal: '2%',
  },
  topText: {
    fontFamily: fontFamilyNunitoBlack,
    fontSize: 18,
    fontWeight: '800',
    letterSpacing: 1.58,
    lineHeight: 25,
  },
});

export default styles;
