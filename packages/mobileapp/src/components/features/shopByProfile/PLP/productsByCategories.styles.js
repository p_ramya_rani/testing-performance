// 9fbef606107a605d69c0edbcd8029e5d
import {StyleSheet} from 'react-native';
import {colorWhite, colorNero3, fontFamilyNunitoExtraBold} from '../ShopByProfile.styles';

const styles = StyleSheet.create({
  bannerContainer: {
    alignItems: 'center',
    backgroundColor: colorWhite,
    flexDirection: 'row',
    height: 70,
    justifyContent: 'center',
    marginBottom: 10,
    width: '100%',
  },
  bannerImage: {
    height: 40,
    marginLeft: 10,
    marginTop: 14,
    resizeMode: 'contain',
    tintColor: colorNero3,
    width: 40,
  },
  bannerText: {
    color: colorNero3,
    fontFamily: fontFamilyNunitoExtraBold,
    fontSize: 18,
    fontWeight: '800',
  },
  bannerTextContainer: {
    marginLeft: 25,
    marginVertical: 15,
  },
  productsContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginBottom: 5,
    marginHorizontal: 4,
  },
  viewMore: {
    marginBottom: 5,
    marginTop: 10,
  },
  viewMoreEmpty: {
    backgroundColor: colorWhite,
  },
});

export default styles;
