/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React, {Fragment} from 'react';
import {Dimensions, ScrollView, Text, View, VirtualizedList, SectionList} from 'react-native';
import {withNavigationFocus} from 'react-navigation';
import {arrayOf, bool, func, number, string, shape} from 'prop-types';
import isEqual from 'lodash/isEqual';
import sortBy from 'lodash/sortBy';
import indexOf from 'lodash/indexOf';
import {connect} from 'react-redux';
import {executeUnbxdAPICall} from '@tcp/core/src/services/handler/handler';
import endpoints from '@tcp/core/src/services/endpoints';
import logger from '@tcp/core/src/utils/loggerInstance';
import {
  getAPIConfig,
  navigateToNestedRoute,
  getUserAgent,
  setValueInAsyncStorage,
  getValueFromAsyncStorage,
  removeValueFromAsyncStorage,
  removeMultipleFromAsyncStorage,
} from '@tcp/core/src/utils';
import Carousel from 'react-native-snap-carousel';
import superagent from 'superagent';
import {executeExternalAPICall} from '@tcp/core/src/services/handler';
import {
  getMonetateProductsRefreshTime,
  getUnbxdProductsRefreshFrequency,
  getIsDynamicBadgeEnabled,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import RecommendationsAbstractor from '@tcp/core/src/services/abstractors/common/recommendations/recommendations';
import {
  addItemsToWishlist,
  createNewWishListAddItemAction,
  getActiveWishlistAction,
  setLastDeletedItemIdAction,
} from '@tcp/core/src/components/features/browse/Favorites/container/Favorites.actions';
import {openQuickViewWithValues} from '@tcp/core/src/components/common/organisms/QuickViewModal/container/QuickViewModal.actions';
import {getUniqueId} from '@tcp/core/src/components/common/organisms/QuickViewModal/container/QuickViewModal.selectors';
import {getQuickViewLoader} from '@tcp/core/src/components/common/molecules/Loader/container/Loader.selector';
import {
  getIsDataLoading,
  selectActiveWishlistId,
  selectActiveWishlistProducts,
  selectWishlistsSummaries,
} from '@tcp/core/src/components/features/browse/Favorites/container/Favorites.selectors';
import {getUserId} from '@tcp/core/src/components/features/account/User/container/User.selectors';
import PLPSkeleton from '@tcp/core/src/components/common/atoms/PLPSkeleton/views/PLPSkeleton';
import {getCustomHeaders, isAndroid} from '@tcp/core/src/utils/utils.app';
import {trackClick, trackPageView, setClickAnalyticsData} from '@tcp/core/src/analytics/actions';
import names from '@tcp/core/src/constants/eventsName.constants';
import Themes from '../ShopByProfile.theme';
import Profile from '../../../common/molecules/Profile';
import FilterButton from '../../../common/atoms/Button/FilterButton';
import {updateProductsByProfile as updateProductsByProfileAction} from './ShopByProfile.action';
import {LoadingMessage} from './LoadingMessage';
import {ProfileMenu} from './ProfileMenu';
import ProductsByCategories from './ProductsByCategories';
import ProductsByInterests from './ProductsByInterests';
import {TopMatchingMsg} from './TopMatchingMsg';
import TopMatchingProducts from './TopMatchingProds';
import {
  capitalizeWords,
  getSizeMapping,
  getMaximumDiscount,
  isDeviceIPhone12,
  showSBPLabels,
} from './helper';
import NoProductsScreen from './NoProductsScreen';
import {setChildProfiles as setChildProfilesAction} from '../ShopByProfile.action';
import {getAllChildProfiles} from '../../../../services/db/api2';
import {
  ACCESSORIES_SIZES,
  SBP_CATEGORY_ID_MAPPING,
  DEPARTMENTS,
  BRAND_ID,
  DEFAULT_L2_CATEGORY_ORDER_CAT_ID,
  INITIAL_NUM_PRODS_INTEREST_GRID,
  NUMBER_INTERESTS_FOR_TOPMATCHING,
  SBP_CURRENT_PROFILE_ID,
  SBP_PROFILE_CREATED,
  SBP_PROFILE_DELETED,
  L2_CATEGORY_END,
  NUMBER_ADDITIONAL_GRID_LAZY_LOAD,
  SBP_UNBXD_SIZES,
  SBP_SIZE_MAPPING,
} from '../ShopByProfile.constants';
import {colorNero} from '../ShopByProfile.styles';
import ProfileIcon from '../../../common/atoms/ProfileIcon';
import styles from './styles';
import {
  canRefreshMonetateProds,
  setMonetateProductsToCache,
  getMonetateProductsFromCache,
  canRefreshUnbxdProds,
  setUnbxdProductsToCache,
  getUnbxdProductsFromCache,
  setUnbxdProdsNextRefreshTime,
} from '../Caching';
import GlobalModals from '../../../common/organisms/GlobalModals';
import {getSBPLabels, getSBPProfiles} from '../ShopByProfile.selector';

const {width} = Dimensions.get('window');

/**
 * Removing Pure component. The data structure (state structure) of this component is quite complex.
 * Pure components only do the shallow checking, which is why it is not able to detect the changes
 * done - deep down the state and hence not updating itself properly.
 */
class PLP extends React.Component {
  static childInterestsFormatter = (interests) => {
    const stringWithSlashes = interests.replace(/,/g, '/');
    const formattedInterest = `/${stringWithSlashes}/`;
    return formattedInterest.toLowerCase();
  };

  static generateFilters(type, options) {
    if (!options.length) return '';
    const filteredOptions = [];
    options.forEach((option) => {
      if (option) {
        filteredOptions.push(`${type}:"${option}"`);
      }
    });
    return filteredOptions.join(' OR ');
  }

  static superEncodeURI = (URI) => {
    let url = URI;
    let encodedStr = '';
    const encodeChars = ['(', ')'];
    url = encodeURI(url);

    for (let i = 0, len = url.length; i < len; i += 1) {
      if (encodeChars.indexOf(url[i]) >= 0) {
        const hex = parseInt(url.charCodeAt(i), 10).toString(16);
        encodedStr += `%${hex}`;
      } else {
        encodedStr += url[i];
      }
    }
    return encodedStr;
  };

  static checkTopMatchingDuplicates(id, arrayOne, arrayTwo, arrayThree) {
    const combinedArrays = [...arrayOne, ...arrayTwo, ...arrayThree];
    return combinedArrays.find((product) => product.uniqueId === id);
  }

  isResetStack = false;

  constructor(props) {
    super(props);
    const {
      navigation: {
        state: {params},
      },
    } = this.props;
    this.state = {
      products: [],
      profile: params,
      productsByInterests: [],
      favoriteList: [],
      favoriteProducts: [],
      showMenu: false,
      selectedCategories: [],
      selectedCategory: 'All',
      selectedInterest: 'All',
      productsByCategories: [],
      numberOfProducts: 4,
      clickedCategory: '',
      clickedInterest: '',
      topMatching: [],
      currentProfileIndex: 0,
      hasNoProducts: false,
      interestOffSet: {},
      monetateInterests: [],
      isViewMoreVisible: {},
      unbxdInterestCached: [],
      showFilterOnScrollStop: false,
      offset: 0,
      interestsLoading: true,
      topMatchingLoading: true,
      categoriesLoading: true,
      scrollYoffset: 0,
      scrollYoffsetPDP: 0,
      stackReset: false,
      error: false,
      carouselLoading: false,
      prodsByCategory: null,
      l2CategoryStart: 0,
      l2CategoryEnd: L2_CATEGORY_END,
    };
    /**
     * pillXCordinates will record the x co-ordinate of each pill
     */
    this.analyticsFired = false;
    this.pillXCordinates = {};
    this.windowWidth = Dimensions.get('window').width;
    // This variable will tell us if the current profile has a wish list yet or not.
    // Until, you favorite a product from a profile,  its corresponding wish list is not created.
    this.profileHasWishList = false;
    this.goToPDP = this.goToPDP.bind(this);
    this.openQuickAdd = this.openQuickAdd.bind(this);
    this.showMoreProducts = this.showMoreProducts.bind(this);
    this.retrieveMoreProductsFromUnbxd = this.retrieveMoreProductsFromUnbxd.bind(this);
    this.changeProfile = this.changeProfile.bind(this);
    this.filterInterestHandler = this.filterInterestHandler.bind(this);
    this.filterCategoryHandler = this.filterCategoryHandler.bind(this);
    this.toggleMenu = this.toggleMenu.bind(this);
    this.goToEditProfile = this.goToEditProfile.bind(this);
    this.getClientCategories = this.getClientCategories.bind(this);
    this.getProducts = this.getProducts.bind(this);
  }

  async componentDidMount() {
    this.pillXCordinates = {};
    let monId = await getValueFromAsyncStorage('monetateId');
    const {navigation, getActiveWishlist} = this.props;
    const {profile} = this.state;
    const {currentIndex} = this.carousel;
    this.isResetStack = navigation && navigation.getParam('isResetStack');
    this.setState((prevState) => ({
      ...prevState,
      currentProfileIndex: currentIndex,
      stackReset: this.isResetStack,
    }));

    /**
     * There is a bug in the application. The Selector file is always returning the wish list products
     * of the last profile created, instead of the current profile. So in this block of code, I am first getting
     *  the active wish list ID from wishlistsSummaries and then fetching the wishlist products against that ID.
     * This will provide us the products of the current profile.
     * TODO: This code need a clean up as the fix should ideally go in selector file.
     */
    try {
      let wishId = '';
      const {wishlistsSummaries} = this.props;
      const activeWishListProfile = wishlistsSummaries.find(
        (itm) => itm.displayName === navigation.state.params.name,
      );
      if (activeWishListProfile) {
        this.profileHasWishList = true;
        wishId = activeWishListProfile.id;
        getActiveWishlist(wishId, true);
      } else {
        this.profileHasWishList = false;
      }
    } catch (err) {
      logger.error(err);
    }

    const {profiles, userId, setChildProfiles} = this.props;
    if (!profiles || !profiles.length) {
      setTimeout(async () => {
        if (profile && profile.gender) {
          this.getClientCategories(profile.gender);
        }
        const allProfiles = await getAllChildProfiles(userId);
        setChildProfiles(allProfiles);
        monId = await getValueFromAsyncStorage('monetateId');
        this.retrieveRecommendedProducts(monId);
      }, 2000);
    } else {
      this.getClientCategories(profile.gender);
      monId = await getValueFromAsyncStorage('monetateId');
      this.retrieveRecommendedProducts(monId);
      this.setState((prevState) => ({
        ...prevState,
        currentProfileIndex: this.carousel.currentIndex,
      }));
    }
  }

  async componentDidUpdate(prevProps) {
    const {navigation, getActiveWishlist} = this.props;
    const {navigation: prevNavigation} = prevProps;
    if (isEqual(navigation.state.params, prevNavigation.state.params) === false) {
      await this.onComponentUpdate();
      this.updateListing(navigation.state.params);

      /**
       * There is a bug in the application. The Selector file is always returning the wish list products
       * of the last profile created, instead of the current profile. So in this block of code, I am first getting
       *  the active wish list ID from wishlistsSummaries and then fetching the wishlist products against that ID.
       * This will provide us the products of the current profile.
       * TODO: This code need a clean up as the fix should ideally go in selector file.
       */
      try {
        let wishId = '';
        const {wishlistsSummaries} = this.props;
        const activeWishListProfile = wishlistsSummaries.find(
          (itm) => itm.displayName === navigation.state.params.name,
        );
        if (activeWishListProfile) {
          this.profileHasWishList = true;
          wishId = activeWishListProfile.id;
          getActiveWishlist(wishId, true);
        } else {
          this.profileHasWishList = false;
        }
      } catch (err) {
        logger.error(err);
      }
    }
    if (navigation.state.routeName === 'PLP') {
      this.triggerAnalyticsTracking();
    }
  }

  async onComponentUpdate() {
    const {profiles} = this.props;
    const completeProfiles = profiles.filter(
      (prof) => prof.interests && prof.interests.length >= 1,
    );
    const profileId = await getValueFromAsyncStorage(SBP_CURRENT_PROFILE_ID);
    let currentIndex = completeProfiles.findIndex((profile) => profileId === profile.id);

    if (currentIndex >= 0 && this.carousel) {
      this.carousel.snapToItem(currentIndex);
      const isDeleted = await getValueFromAsyncStorage(SBP_PROFILE_DELETED);
      if (isDeleted) {
        this.carousel.snapToItem(currentIndex + 1);
        this.carousel.snapToItem(currentIndex);
        this.setState({carouselLoading: true});
        await removeValueFromAsyncStorage(SBP_PROFILE_DELETED);
      }
      const isFromCP = await getValueFromAsyncStorage(SBP_PROFILE_CREATED);
      if (isFromCP) {
        currentIndex = profiles.length - 1;
        setTimeout(async () => {
          await removeMultipleFromAsyncStorage([SBP_PROFILE_CREATED, SBP_CURRENT_PROFILE_ID]);
        }, 3000);
      }
      this.carousel.snapToItem(currentIndex);
    }
    const {
      navigation: {
        state: {params},
      },
    } = this.props;
    this.setState({
      profile: params,
      interestsLoading: true,
      topMatchingLoading: true,
      categoriesLoading: true,
    });
    setTimeout(() => {
      this.setState({carouselLoading: false});
    }, 1);
  }

  getRecommendationObject = (recommendations, filters = [], rows = 10) => {
    return RecommendationsAbstractor.getProductsByFilter(
      recommendations.map((recommendation) => recommendation.generalProductId),
      filters,
      rows,
    ).then((prices) => {
      return {
        products: recommendations
          .filter((recommendation) => {
            return prices && typeof prices[recommendation.generalProductId] !== 'undefined';
          })
          .map((recommendation) => {
            return {
              ...recommendation,
              ...prices[recommendation.generalProductId],
            };
          }),
      };
    });
  };

  getDepartmentID = (department) => {
    const {L2Categories} = this.props;
    let departmentId;
    let departmentMod = department;
    if (
      departmentMod.toLowerCase() === DEPARTMENTS.BABY_GIRL.toLowerCase() ||
      departmentMod.toLowerCase() === DEPARTMENTS.BABY_BOY.toLowerCase()
    ) {
      departmentMod = 'Baby';
    }
    L2Categories.forEach(({categoryContent}) => {
      if (categoryContent.name.toLowerCase() === departmentMod.toLowerCase()) {
        departmentId = categoryContent.id;
      }
    });
    return departmentId;
  };

  // eslint-disable-next-line sonarjs/cognitive-complexity,complexity
  getL2Categories(dept) {
    let department = dept;
    const l2Categories = [];
    try {
      const {screenProps, L2Categories} = this.props;
      const {brandId} = screenProps.apiConfig;
      if (brandId.toLowerCase() === BRAND_ID.GYM.toLowerCase()) {
        department =
          department.toLowerCase().indexOf('baby') > -1 ? department.substr(5) : department;
      }
      let babyDepartment;
      let categories;
      let subCategories;
      if (
        department.toLowerCase() === DEPARTMENTS.BABY_GIRL.toLowerCase() ||
        department.toLowerCase() === DEPARTMENTS.BABY_BOY.toLowerCase() ||
        brandId.toLowerCase() === BRAND_ID.GYM.toLowerCase()
      ) {
        department = capitalizeWords(department);
        babyDepartment = capitalizeWords(department);
      }
      if (
        department.toLowerCase() === DEPARTMENTS.BABY_GIRL.toLowerCase() ||
        department.toLowerCase() === DEPARTMENTS.BABY_BOY.toLowerCase()
      ) {
        department = 'Baby';
        [{subCategories}] = L2Categories.filter(
          (category) => category.categoryContent.name === department,
        );
      } else {
        categories = L2Categories.filter(
          (category) => category.categoryContent.name.toLowerCase() === department.toLowerCase(),
        );
        if (categories.length < 1) return;
        [{subCategories}] = categories;
      }
      const filteringGender =
        babyDepartment === capitalizeWords(DEPARTMENTS.BABY_GIRL)
          ? capitalizeWords(DEPARTMENTS.BABY_BOY)
          : capitalizeWords(DEPARTMENTS.BABY_GIRL);

      Object.entries(subCategories).forEach(([key, value]) => {
        if (
          value.hasSubCategory === true &&
          key !== 'UNIDENTIFIED_GROUP' &&
          key !== filteringGender
        ) {
          value.items.forEach((item) => {
            if (item.categoryContent.displayToCustomer) {
              l2Categories.push(item.categoryContent);
            }
          });
        }
      });
    } catch (error) {
      logger.error('GETL2CATEGORIES', error);
      this.setState((prevState) => ({
        ...prevState,
        error: true,
      }));
    }
    // eslint-disable-next-line consistent-return
    return l2Categories;
  }

  getRecommendedProductsFromMonetate = async (monetateId) => {
    const {
      profile: {interests, gender},
    } = this.state;
    const {screenProps} = this.props;
    let department = capitalizeWords(gender);
    const {brandId} = screenProps.apiConfig;
    if (
      brandId === DEPARTMENTS.GYM &&
      (department.toLowerCase() === DEPARTMENTS.TODDLER_BOY.toLowerCase() ||
        department.toLowerCase() === DEPARTMENTS.BABY_BOY.toLowerCase())
    ) {
      department = 'Boy';
    }
    if (
      brandId === DEPARTMENTS.GYM &&
      (department.toLowerCase() === DEPARTMENTS.TODDLER_GIRL.toLowerCase() ||
        department.toLowerCase() === DEPARTMENTS.BABY_GIRL.toLowerCase())
    ) {
      department = 'Girl';
    }

    const apiConfigObj = getAPIConfig();
    const {monetateUrl, monetateChannel} = apiConfigObj;
    const childInterests = PLP.childInterestsFormatter(interests);
    const monUrl = `/topmatching${childInterests}`;
    const requestBod = {
      channel: monetateChannel,
      monetateId,
      events: [
        {
          eventType: 'monetate:decision:DecisionRequest',
          requestId: `${Math.round(Math.random() * 10 ** 4)}`,
        },
        {
          eventType: 'monetate:context:UserAgent',
          userAgent: getUserAgent(),
        },
        {
          eventType: 'monetate:context:PageView',
          url: monUrl,
          pageType: 'sbp',
        },
        {
          eventType: 'monetate:context:CustomVariables',
          customVariables: [
            {variable: 'deviceType', value: 'Mobile'},
            {variable: 'department', value: department},
            {variable: 'SBPCategoryMapping', value: 'true'},
          ],
        },
      ],
    };
    return executeExternalAPICall({
      webService: {
        method: 'POST',
        URI: monetateUrl,
      },
      body: requestBod,
    })
      .then(async (result) => {
        const categoryIdMapping = result?.body?.data?.responses[0].actions.find(
          (element) => element.json.SBPCategoryMapping,
        );
        if (result.body.meta.monetateId) {
          await setValueInAsyncStorage('monetateId', result.body.meta.monetateId);
        }
        if (categoryIdMapping) {
          await setValueInAsyncStorage(SBP_CATEGORY_ID_MAPPING, JSON.stringify(categoryIdMapping));
        }

        return result.body.data.responses;
      })
      .catch((error) => {
        logger.error('retrieveRecommendedProduct', error);
        this.setState(
          (prevState) => ({
            ...prevState,
            error: true,
          }),
          () => {
            return [];
          },
        );
      });
  };

  /**
   * @function getFilter This function calculates the filter of the current category.
   * @param {object} item The current category
   * @param {String} gender The gender of the current child profile
   * @return The filter of the current category.
   */
  getFilter = (item, gender) => {
    const sizes = item && item.filter && item.filter.length ? item.filter : [];
    let gen;
    if (
      gender === DEPARTMENTS.BOY ||
      gender === DEPARTMENTS.TODDLER_BOY ||
      gender === DEPARTMENTS.BABY_BOY
    )
      gen = ['Boy', 'Unisex'];
    else gen = ['Girl', 'Unisex'];
    let filteredGender = encodeURI(PLP.generateFilters('gender_uFilter', gen));
    filteredGender = `${filteredGender}&filter=${sizes}`;
    return filteredGender;
  };

  getProfilesL2Categories = () => {
    const {
      profile: {interests, gender},
    } = this.state;
    const AllL2Categories = this.getL2Categories(gender);
    const listOfProfileInterests = interests.split(',');
    const defaultOrder = DEFAULT_L2_CATEGORY_ORDER_CAT_ID[gender];
    const profileL2Categories = [];
    AllL2Categories.forEach(({name, id}) => {
      if (listOfProfileInterests.includes(name)) {
        profileL2Categories.push(id);
      }
    });
    const orderToSortBy = [...profileL2Categories, ...defaultOrder];
    return [...new Set(orderToSortBy)];
  };

  getProductCategoryID = (currentProduct) => {
    const {profile} = this.state;
    let productCatId;
    const L2Categories = this.getL2Categories(profile.gender);
    const categoryIDList = L2Categories.map((category) => category.id);
    const {categoryPath2_catMap: categoryPath2CatMap = []} = currentProduct;

    /* eslint-disable no-restricted-syntax */
    for (const catMap of categoryPath2CatMap) {
      const value = catMap.split('>')[1].split('|')[0];
      const catID = categoryIDList.find((element) => element === value);
      if (catID) {
        productCatId = catID;
        break;
      }
    }
    return productCatId;
  };

  getSortedL2Categories = () => {
    const {
      profile: {gender, interests},
    } = this.state;
    const l2Categories = this.getL2Categories(gender, true);
    const listOfInterests = interests && interests.split(',');
    const selectedL2Cat = l2Categories
      .filter((c) => listOfInterests.indexOf(c.name) > -1)
      .sort((a, b) => {
        const categoryA = a.name.toLowerCase();
        const categoryB = b.name.toLowerCase();
        let returnValue = 0;
        if (categoryA < categoryB) returnValue = -1;
        else if (categoryA > categoryB) returnValue = 1;
        return returnValue;
      });
    const nonSelectedL2Cat = l2Categories.filter((c) => listOfInterests.indexOf(c.name) < 0);
    return [...selectedL2Cat, ...nonSelectedL2Cat];
  };

  getSizeFilterForL2Categories = async (filterSizes, id) => {
    const {
      profile: {gender},
    } = this.state;
    const sbpCatIDMapping = await getValueFromAsyncStorage(SBP_CATEGORY_ID_MAPPING);
    if (!sbpCatIDMapping) return [];
    const parsedSBPCatIDMapping = JSON.parse(sbpCatIDMapping);
    const {
      json: {SBPCategoryMapping},
    } = parsedSBPCatIDMapping;
    const singleCategory = SBPCategoryMapping.find(
      ({categoryIds, department}) =>
        department.toLowerCase() === gender.toLowerCase() &&
        categoryIds.split(',').indexOf(id) > -1,
    );
    let categoryName = '';
    if (singleCategory) {
      const {category} = singleCategory;
      categoryName = category;
    }
    return filterSizes[categoryName] ? filterSizes[categoryName] : [];
  };

  async getProductsForCategories(gender, filterSizes, fromFilter, categoryId) {
    const categories = this.getSortedL2Categories();
    const {l2CategoryStart, l2CategoryEnd} = this.state;
    if (fromFilter) {
      const category = categories.find((element) => element.id === categoryId);
      const {name, id} = category;
      const sizes = await this.getSizeFilterForL2Categories(filterSizes, id);
      const filteredSizes = PLP.superEncodeURI(PLP.generateFilters('v_tcpsize_uFilter', sizes));
      return {
        category: name,
        id,
        products: await this.getUnboxResultForCategory(id, filteredSizes, gender),
        filter: filteredSizes,
      };
    }
    return Promise.all(
      categories.slice(l2CategoryStart, l2CategoryEnd).map(async ({name, id}) => {
        if (!name) {
          return {
            category: name,
            id,
            products: [],
          };
        }
        const sizes = await this.getSizeFilterForL2Categories(filterSizes, id);
        const filteredSizes = PLP.superEncodeURI(PLP.generateFilters('v_tcpsize_uFilter', sizes));
        return {
          category: name,
          id,
          products: await this.getUnboxResultForCategory(id, filteredSizes, gender),
        };
      }),
    );
  }

  async getClientCategories(
    gender,
    retry = 0,
    fromFilter = false,
    categoryId = null,
    reachedScrollEnd = false,
  ) {
    try {
      let productsByCategories;
      const filterSizes = await this.retrieveSizesByCatFromUnbxd();
      const {sendCategoryAnalytics} = this.props;
      if (fromFilter) {
        let prodsByCategory;
        prodsByCategory = await this.getProductsForCategories(
          gender,
          filterSizes,
          fromFilter,
          categoryId,
        );
        const filteredGender = this.getFilter(prodsByCategory, gender);
        prodsByCategory = {
          category: prodsByCategory.category,
          id: prodsByCategory.id,
          products: prodsByCategory.products,
          data: prodsByCategory.products,
          filter: filteredGender,
        };
        this.setState({
          prodsByCategory,
          categoriesLoading: false,
        });
        return;
      }

      productsByCategories = await this.getProductsForCategories(gender, filterSizes);
      if (retry === 0 && productsByCategories.length === 0) {
        this.getClientCategories(gender, 1);
      }
      /**
       * Here we are adding an extra information of the filter against each category.
       * This will later be used when we lazy load the products on the corresponding filtered page.
       */
      productsByCategories = productsByCategories.map((item) => {
        const filteredGender = this.getFilter(item, gender);
        return {
          category: item.category,
          id: item.id,
          products: item.products,
          data: item.data,
          filter: filteredGender,
        };
      });

      if (reachedScrollEnd) {
        this.setState((prevState) => ({
          ...prevState,
          productsByCategories: [...prevState.productsByCategories, ...productsByCategories],
          categoriesLoading: false,
          l2CategoryStart: prevState.l2CategoryEnd,
          l2CategoryEnd: prevState.l2CategoryEnd + NUMBER_ADDITIONAL_GRID_LAZY_LOAD,
        }));
        return;
      }
      sendCategoryAnalytics(productsByCategories);
      this.setState({
        productsByCategories,
        categoriesLoading: false,
        l2CategoryStart: L2_CATEGORY_END,
        l2CategoryEnd: L2_CATEGORY_END + NUMBER_ADDITIONAL_GRID_LAZY_LOAD,
      });
    } catch (error) {
      logger.error('CLIENT CATEGORIES', error);
      this.setState((prevState) => ({
        ...prevState,
        error: true,
      }));
    }
  }

  categoriesLoading = () => {
    const {navigation} = this.props;
    return navigation.state && navigation.state.params && navigation.state.params.categoriesLoading;
  };

  // eslint-disable-next-line complexity, sonarjs/cognitive-complexity, max-statements
  retrieveRecommendedProducts = async (monetateId) => {
    const {
      activeWishListId,
      isDataLoading,
      getActiveWishlist,
      screenProps,
      monetateProductsRefreshTime,
      unbxdProductsRefreshFrequency,
      sendTopMatchingProductsAnalytics,
      navigation,
    } = this.props;

    const {
      favoriteList,
      profile: {name, id, favorite_colors: favoriteColors},
    } = this.state;

    let filteredSizes = '';
    const profileFavoriteColors = favoriteColors ? favoriteColors.split(',') : [];
    const {brandId} = screenProps.apiConfig;
    const canRefresh = await canRefreshMonetateProds(monetateProductsRefreshTime, id);
    let responses = [];
    if (canRefresh) {
      const {itemsCount} = favoriteList;
      if (
        typeof itemsCount !== 'undefined' &&
        !(isDataLoading && activeWishListId === favoriteList.id)
      ) {
        let wishId = '';
        const {wishlistsSummaries} = this.props;
        wishId = wishlistsSummaries.find(
          (itm) => itm.displayName === navigation.state.params.name,
        ).id;
        getActiveWishlist(wishId, true);
        this.profileHasWishList = true;
      }

      const filterSizes = await this.retrieveSizesFromUnbxd();
      filteredSizes = PLP.superEncodeURI(PLP.generateFilters('v_tcpsize_uFilter', filterSizes));

      responses = await this.getRecommendedProductsFromMonetate(monetateId);
      if (responses.length > 0) {
        await setMonetateProductsToCache(responses, id);
        await setUnbxdProdsNextRefreshTime(0, id, 'unbxd'); // To refresh unbxd products immediately
      }
    } else {
      responses = await getMonetateProductsFromCache(id);
    }
    if (!responses.length) {
      this.setState({
        topMatching: [],
        productsByInterests: [],
        monetateInterests: [],
        unbxdInterestCached: [],
        topMatchingLoading: false,
        interestsLoading: false,
      });
      return;
    }
    const {actions} = responses[0];
    if (!actions.length) {
      this.setState({
        topMatching: [],
        productsByInterests: [],
        monetateInterests: [],
        unbxdInterestCached: [],
        topMatchingLoading: false,
        interestsLoading: false,
      });
      return;
    }

    const canRefreshUnbxd = await canRefreshUnbxdProds(id);
    let topMatchingCarouselWithProfileInterests;
    const unbxdInterestCached = [];
    const productsByInterestGrid = [];
    let unbxdProductsCache;

    if (canRefreshUnbxd) {
      let topMatchingProdsFromMonetate = [];
      let interestProdsFromMonetate = [];
      const interestTypes = [];
      actions.forEach((action) => {
        if (action.component === 'topmatching') {
          topMatchingProdsFromMonetate = topMatchingProdsFromMonetate.concat(
            action.items.map((item) => ({...item, isTopMatchingProduct: true})),
          );
        } else if (action.component) {
          interestTypes.push(action.component);
          const childInt = action.component;
          this.setState((prevState) => ({
            ...prevState,
            interestOffSet: {
              ...prevState.interestOffSet,
              [childInt]: INITIAL_NUM_PRODS_INTEREST_GRID,
            },
          }));
          interestProdsFromMonetate = interestProdsFromMonetate.concat(
            action.items.map((item) => ({
              ...item,
              childInterest: action.component,
            })),
          );
        }
      });
      const listOfProductsIdsTopMatching = topMatchingProdsFromMonetate.map(
        (item) => item.itemGroupId,
      );

      const uniqueProductIdsTopMatching = [...new Set(listOfProductsIdsTopMatching)];

      const slicedListOfTopMatchingProducts = uniqueProductIdsTopMatching.slice(0, 30);
      const filterProductsIds = PLP.superEncodeURI(
        PLP.generateFilters('prodpartno', slicedListOfTopMatchingProducts),
      );

      let filtersTopMatching = [filterProductsIds, filteredSizes];
      if (brandId === BRAND_ID.GYM) {
        filtersTopMatching = [filterProductsIds];
      }
      const productsTopMatchingUnbxd = await this.parseProductResponseForMonetate(
        topMatchingProdsFromMonetate,
        filtersTopMatching,
        listOfProductsIdsTopMatching.length,
      );

      const uniqueInterestTypes = [...new Set(interestTypes)];

      const unbxdResultsInterests = await Promise.all(
        uniqueInterestTypes.map(async (interest) => {
          const listOfInterest = interestProdsFromMonetate.filter(
            (product) => product.childInterest === interest,
          );
          const uniqueListOfInterests = listOfInterest.filter(
            (product, index, self) =>
              index === self.findIndex((t) => t.itemGroupId === product.itemGroupId),
          );
          const listOfProductsIdsInterestGrids = uniqueListOfInterests.map(
            (item) => item.itemGroupId,
          );
          const uniquelistOfProductsIdsInterestGrids = [...new Set(listOfProductsIdsInterestGrids)];
          const slicedListOfProductsIdsInterestGrids = uniquelistOfProductsIdsInterestGrids.slice(
            0,
            100,
          );

          const filterProductsInterestIds = PLP.superEncodeURI(
            PLP.generateFilters('prodpartno', slicedListOfProductsIdsInterestGrids),
          );

          let interestGridFilter = [filterProductsInterestIds, filteredSizes];
          if (brandId === BRAND_ID.GYM) {
            interestGridFilter = [filterProductsInterestIds];
          }
          return {
            component: interest,
            products: await this.retrieveInterestsProductsFromUnbxd(
              uniqueListOfInterests,
              interestGridFilter,
              listOfProductsIdsInterestGrids.length,
            ),
          };
        }),
      );

      unbxdResultsInterests.forEach(({component, products}) => {
        const clearanceProducts = [];
        const nonClearanceProducts = [];

        products.products.forEach((product) => {
          if (product.categoryPath2) {
            if (product.categoryPath2.includes('Clearance')) clearanceProducts.push(product);
            else nonClearanceProducts.push(product);
          }
        });

        const prodsFromUnbxdModifiedCatID = nonClearanceProducts.map((product) => ({
          ...product,
          productCatID: this.getProductCategoryID(product),
        }));

        const orderToGroupBy = this.getProfilesL2Categories();
        const sortedProdsByColor = this.sortProductsByFavoriteColor(
          prodsFromUnbxdModifiedCatID,
          profileFavoriteColors,
        );

        const sortedProductsByCategory = this.sortProductsByCategory(
          sortedProdsByColor,
          orderToGroupBy,
        );

        const sortedProductsIncludingClearance = [
          ...sortedProductsByCategory,
          ...clearanceProducts,
        ];

        const productsForRendering = {
          products: sortedProductsIncludingClearance,
        };

        unbxdInterestCached.push({
          component,
          items: productsForRendering.products,
        });

        const allProductsFromUnbxd = JSON.parse(JSON.stringify(productsForRendering));
        productsForRendering.products = productsForRendering.products.slice(
          0,
          INITIAL_NUM_PRODS_INTEREST_GRID,
        );
        const maxDiscountPercentage = getMaximumDiscount(allProductsFromUnbxd.products);

        productsByInterestGrid.push({
          component,
          items: productsForRendering,
          allProducts: allProductsFromUnbxd,
          maxDiscount: maxDiscountPercentage,
        });
      });

      // START-EXTRACT FIRST TWO PRODUCTS FROM EACH GRID TO APPEND TO TOP MATCHES
      const interestGridProductsForTopMatching = [];
      const listOfIndexToRemoveFromGrid = [];

      productsByInterestGrid.forEach((interestGrid, gridIndex) => {
        interestGrid.items.products.forEach((product, index) => {
          if (
            index < NUMBER_INTERESTS_FOR_TOPMATCHING &&
            product.categoryPath2 &&
            !product.categoryPath2.includes('Clearance')
          ) {
            interestGridProductsForTopMatching.push(product);
            listOfIndexToRemoveFromGrid.push({
              gridIndex,
              prodId: product.uniqueId,
            });
          }
        });
      });

      listOfIndexToRemoveFromGrid.forEach((item) => {
        const findIndex = productsByInterestGrid[item.gridIndex].items.products.findIndex(
          (product) => product.uniqueId === item.prodId,
        );
        productsByInterestGrid[item.gridIndex].items.products.splice(findIndex, 1);
      });
      // END-EXTRACT FIRST TWO PRODUCTS FROM EACH GRID TO APPEND TO TOP MATCHES

      const topMatchingProdsFromUnbxd = productsTopMatchingUnbxd.products.filter(
        (product) => product.isTopMatchingProduct === true,
      );

      const prodsFromUnbxdModifiedCatIDTopMatch = topMatchingProdsFromUnbxd.map((product) => ({
        ...product,
        productCatID: this.getProductCategoryID(product),
      }));

      const orderToGroupByTopMatch = this.getProfilesL2Categories();
      const sortedProdsByColorTopMatch = this.sortProductsByFavoriteColor(
        prodsFromUnbxdModifiedCatIDTopMatch,
        profileFavoriteColors,
      );

      const sortedProductsByCategoryTopMatch = this.sortProductsByCategory(
        sortedProdsByColorTopMatch,
        orderToGroupByTopMatch,
      );

      topMatchingCarouselWithProfileInterests = [
        ...interestGridProductsForTopMatching,
        ...sortedProductsByCategoryTopMatch,
      ];

      const uniquetopMatchingCarouselWithProfileInterests =
        topMatchingCarouselWithProfileInterests.filter(
          (product, index, self) =>
            index === self.findIndex((t) => t.uniqueId === product.uniqueId),
        );

      unbxdProductsCache = {
        topMatching: uniquetopMatchingCarouselWithProfileInterests,
        productsByInterests: productsByInterestGrid,
        unbxdInterestCached,
      };
      if (productsByInterestGrid) {
        await setUnbxdProductsToCache(unbxdProductsCache, unbxdProductsRefreshFrequency, id);
      }
    } else {
      unbxdProductsCache = await getUnbxdProductsFromCache(id);
      if (!(unbxdProductsCache && unbxdProductsCache.productsByInterests)) {
        await setUnbxdProdsNextRefreshTime(0, id, 'unbxd');
        this.retrieveRecommendedProducts(monetateId);
        return;
      }
      unbxdProductsCache.productsByInterests.forEach((action) => {
        const childInt = action.component;
        this.setState((prevState) => ({
          ...prevState,
          topMatchingLoading: false,
          interestsLoading: false,
          interestOffSet: {
            ...prevState.interestOffSet,
            [childInt]: INITIAL_NUM_PRODS_INTEREST_GRID,
          },
        }));
      });
    }
    sendTopMatchingProductsAnalytics(unbxdProductsCache.topMatching, name);
    if (isDeviceIPhone12() && this.categoriesLoading())
      setTimeout(() => {
        this.setState({
          ...unbxdProductsCache,
          topMatchingLoading: false,
          interestsLoading: false,
        });
      }, 4000);
    else
      this.setState({
        ...unbxdProductsCache,
        topMatchingLoading: false,
        interestsLoading: false,
      });
  };

  sortProductsByCategory = (listProducts, orderToSortBy) => {
    return sortBy(listProducts, (obj) => {
      return indexOf(orderToSortBy, obj.productCatID);
    });
  };

  sortProductsByFavoriteColor = (listProducts, listOfFavColors) => {
    if (listOfFavColors.length === 0) return listProducts;
    return listProducts.sort((a, b) => {
      return (
        listOfFavColors.indexOf(b.TCPColor.toLowerCase()) -
        listOfFavColors.indexOf(a.TCPColor.toLowerCase())
      );
    });
  };

  formatProductsData = (products) => {
    return products.map((tile, index) => {
      if (tile.productInfo) {
        const {
          productInfo: {listPrice, offerPrice, name, generalProductId, priceRange} = {},
          miscInfo: {categoryName} = {},
        } = tile;
        const lowOfferPrice = priceRange && priceRange.lowOfferPrice ? priceRange.lowOfferPrice : 0;
        const productId = generalProductId && generalProductId.split('_')[0];
        const productName = name;
        return {
          id: productId,
          colorId: generalProductId,
          name: productName,
          price: offerPrice,
          listPrice,
          extPrice: lowOfferPrice,
          position: index + 1,
          type: categoryName,
          pricingState: offerPrice < listPrice ? 'on sale' : 'full price',
          sbp: true,
        };
      }
      const {
        productid: productId,
        prodpartno: productPartNo,
        product_name: productName,
        low_list_price: lowListPrice,
        low_offer_price: lowOfferPrice,
        miscInfo: {categoryName} = {},
      } = tile;
      return {
        id: productId,
        colorId: productPartNo,
        name: productName,
        price: lowOfferPrice,
        listPrice: lowListPrice,
        extPrice: lowOfferPrice,
        position: index + 1,
        type: categoryName,
        pricingState: lowOfferPrice < lowListPrice ? 'on sale' : 'full price',
        sbp: true,
      };
    });
  };

  triggerAnalyticsTracking = () => {
    if (this.analyticsFired) return;
    const {topMatching, productsByCategories, selectedCategory} = this.state;
    const {trackPageLoad} = this.props;
    const products = [];
    if (topMatching.length) products.push(...topMatching);
    if (selectedCategory === 'All') {
      productsByCategories.forEach((productsByCategory) =>
        products.push(...productsByCategory.products),
      );
    } else {
      const productsByCategory = productsByCategories.find((p) => p.category === selectedCategory);
      if (productsByCategory) products.push(...productsByCategory.products);
    }
    if (products.length && productsByCategories.length) {
      const currentScreen = names.screenNames.product_listing;
      const productsFormatted = this.formatProductsData(products).slice(0, 20);
      const nonInternalStr = 'non-internal search';
      const origin = 'sbp';
      const eventsData = ['event91', 'event82'];
      const pageNavigationText = 'non-browse';
      const pageData = {
        pageType: 'browse',
        customEvents: eventsData,
        pageNavigationText,
        products: productsFormatted,
        searchType: nonInternalStr,
        searchText: nonInternalStr,
        listingCount: nonInternalStr,
        originType: origin,
        productsCount: products.length,
      };
      trackPageLoad(pageData, currentScreen);
      this.analyticsFired = true;
    }
  };

  getUnboxResultForCategory = (categoryId, filterSizes, genderInput) => {
    let gender;
    if (
      genderInput === DEPARTMENTS.BOY ||
      genderInput === DEPARTMENTS.TODDLER_BOY ||
      genderInput === DEPARTMENTS.BABY_BOY
    )
      gender = ['Boy', 'Unisex'];
    else gender = ['Girl', 'Unisex'];

    const departmentID = this.getDepartmentID(genderInput);
    const categoryPathID = `categoryPathId:"${departmentID}>${categoryId}"`;
    let filteredGender = encodeURI(PLP.generateFilters('gender_uFilter', gender));
    filteredGender = `${filteredGender}&filter=${filterSizes.replace(/&/g, '%26')}`;
    return executeUnbxdAPICall({
      body: {
        rows: 50,
        version: 'V2',
        'facet.multiselect': true,
        'variants.count': 0,
        selectedfacet: true,
        'p-id': categoryPathID,
        filter: filteredGender,
        pagetype: 'boolean',
        fields:
          'productimage,alt_img,style_partno,giftcard,TCPProductIndUSStore,TCPWebOnlyFlagUSStore,TCPWebOnlyFlagCanadaStore,TCPFitMessageUSSstore,TCPFit,product_name,TCPColor,top_rated,imagename,productid,uniqueId,favoritedcount,TCPBazaarVoiceReviewCount,categoryPath3_catMap,categoryPath2_catMap,product_short_description,style_long_description,min_list_price,min_offer_price,TCPBazaarVoiceRating,product_long_description,seo_token,variantCount,prodpartno,variants,v_tcpfit,v_qty,v_tcpsize,style_name,v_item_catentry_id,v_listprice,v_offerprice,v_qty,variantId,auxdescription,list_of_attributes,additional_styles,TCPLoyaltyPromotionTextUSStore,TCPLoyaltyPLCCPromotionTextUSStore,v_variant,low_offer_price,high_offer_price,low_list_price,high_list_price,long_product_title,TCPMerchantTagUSStore,TCPStyleTypeUS,TCPStyleQTYUS',
      },
      webService: endpoints.getProductviewbyCategory,
    }).then((res) => res.body.response.products);
  };

  // eslint-disable-next-line sonarjs/cognitive-complexity
  updateListing = async (profileInput) => {
    let profile = profileInput;
    const {wishlistsSummaries, profiles, navigation} = this.props;
    const favoriteList = wishlistsSummaries.filter(
      (favorite) => favorite.displayName === profile.name,
    )[0];
    profile = Object.assign(
      {},
      profile,
      profiles.find((p) => p.id === profile.id),
    );
    navigation.setParams(profile);
    const currentProfileIndex = this.carousel.currentIndex;
    const monetateId = (await getValueFromAsyncStorage('monetateId')) || null;
    if (!profile.refresh) return;
    this.filterScrollView.scrollTo({x: 0, animated: false});
    this.setState(
      (prevState) =>
        Object.assign({}, prevState, {
          profile,
          selectedCategories: [],
          selectedCategory: 'All',
          currentProfileIndex,
          clickedInterest: '',
          numberOfProducts: 4,
          showFilterOnScrollStop: false,
          favoriteList: favoriteList || [],
          topMatchingLoading: true,
          interestsLoading: true,
          categoriesLoading: true,
          stackReset: false,
          l2CategoryStart: 0,
          l2CategoryEnd: L2_CATEGORY_END,
          error: false,
        }),
      () => {
        this.getClientCategories(profile.gender);
        const {selectedCategory} = this.state;
        this.filterCategoryHandler(selectedCategory);
        this.retrieveRecommendedProducts(monetateId);
      },
    );

    setTimeout(async () => {
      try {
        const {updateProductsByProfile} = this.props;
        const {productsByCategories} = this.state;
        const profileId = profile.id;
        const payload = {productsByCategories, profileId};
        updateProductsByProfile(payload);
        this.setState({
          profile,
        });
      } catch (e) {
        logger.error('ERROR SETTIMEOUT', e);
        this.setState((prevState) => ({
          ...prevState,
          error: true,
        }));
      }
    }, 1000);
  };

  getPriceRange = (productInfo, product) => {
    return {
      highListPrice: productInfo?.priceRange?.highListPrice || 0,
      highOfferPrice: productInfo?.priceRange?.highOfferPrice || 0,
      lowListPrice: productInfo?.priceRange?.lowListPrice || product.min_list_price,
      lowOfferPrice: productInfo?.priceRange?.lowOfferPrice || product.min_offer_price,
    };
  };

  goToPDP = (product) => {
    const {navigation} = this.props;
    navigation.navigate('ProductDetailSBP', this.getSBPNavigation(product));
  };

  getSBPNavigation = (product) => {
    const {
      profile: {name, theme},
      profile,
    } = this.state;
    const {activeWishListProducts, screenProps} = this.props;
    let isFavorite = false;

    if (activeWishListProducts) {
      activeWishListProducts.forEach((prodInWishList) => {
        if (
          prodInWishList.skuInfo.colorProductId === product.prodpartno ||
          prodInWishList.skuInfo.colorProductId === product.uniqueId
        ) {
          isFavorite = true;
        }
      });
    }

    let {categoryPath2} = product;
    const {productInfo} = product;
    if (!categoryPath2 && product.categoryPath2_catMap && product.categoryPath2_catMap.length)
      categoryPath2 = product.categoryPath2_catMap.map(
        (c) => c.split('>')[c.split('>').length - 1],
      );
    const imageName = product.uniqueId;
    const extraImageName = `${product.uniqueId.split('_')[0]}/${product.uniqueId}.jpg`;

    const retrievedProductInfo = Object.assign({}, product, {
      isSBP: true,
      name: product.product_name,
      colorFitsSizesMap: [
        {
          colorProductId: product.uniqueId,
          imageName: product.uniqueId,
          productImage: `${product.uniqueId}.jpg`,
          miscInfo: {
            isClearance: null,
            isBopisEligible: false,
            isBossEligible: true,
            badge1: {
              defaultBadge: '',
            },
            badge2: null,
            badge3: null,
            videoUrl: null,
            hasOnModelAltImages: false,
            listPrice: productInfo?.listPrice || product.min_list_price,
            offerPrice: productInfo?.offerPrice || product.min_offer_price,
            keepAlive: false,
          },
          color: {
            name: product.TCPColor,
            imagePath: null,
            swatchImage: `${imageName}_swatch.jpg`,
          },
        },
      ],
      imagesByColor: {
        [product.TCPColor]: {
          basicImageUrl: extraImageName,
          extraImages: [
            {
              isShortImage: false,
              isOnModalImage: false,
              iconSizeImageUrl: extraImageName,
              listingSizeImageUrl: extraImageName,
              regularSizeImageUrl: extraImageName,
              bigSizeImageUrl: extraImageName,
              superSizeImageUrl: extraImageName,
            },
          ],
        },
      },
      listPrice: productInfo?.listPrice || product.min_list_price,
      offerPrice: productInfo?.offerPrice || product.min_offer_price,
      priceRange: this.getPriceRange(productInfo, product),
    });
    return {
      title: `${name}'S PLACE`,
      pdpUrl: product.prodpartno || product.uniqueId,
      selectedColorProductId: product.prodpartno || product.uniqueId,
      reset: true,
      profile,
      isSBP: true,
      isFavorite,
      onAddItemToFavorites: this.onAddItemToFavorites,
      backgroundColor: Themes[theme || 0].color,
      brandId: screenProps.apiConfig.brandId,
      productCategories: categoryPath2,
      clickOrigin: 'browse',
      retrievedProductInfo,
    };
  };

  openQuickAdd = (product) => {
    const {onQuickViewOpenClick} = this.props;
    const payload = {
      colorProductId: product.prodpartno || product.generalProductId,
      fromPage: 'PLP',
      isSBP: true,
      sbpNavigation: this.getSBPNavigation(product),
    };
    onQuickViewOpenClick(payload);
  };

  productsByInterest = (component, items, allProducts, maxDiscount) => {
    const {
      clickedInterest,
      interestOffSet,
      favoriteList,
      unbxdInterestCached,
      profile,
      profile: {theme},
    } = this.state;
    const {
      navigation,
      activeWishListProducts,
      plpLabels,
      screenProps,
      isDynamicBadgeEnabled = {},
      quickViewLoader,
      quickViewProductId,
    } = this.props;
    const {brandId} = screenProps.apiConfig;
    return items.products.length === 0 ? null : (
      <ScrollView
        ref={(ref) => {
          this.interestScrollView = ref;
        }}>
        <ProductsByInterests
          onAddItemToFavorites={this.onAddItemToFavorites}
          activeWishListProducts={this.profileHasWishList ? activeWishListProducts : []}
          profile={profile}
          theme={theme}
          interest={component}
          products={items}
          goToPDP={this.goToPDP}
          quickViewLoader={quickViewLoader}
          quickViewProductId={quickViewProductId}
          openQuickAdd={this.openQuickAdd}
          showMoreProducts={this.showMoreProducts}
          clickedInterest={clickedInterest}
          interestOffSetArray={interestOffSet}
          favoriteList={favoriteList}
          showAllProducts={allProducts && allProducts.products}
          navigation={navigation}
          interestGrid
          retrieveMoreProductsFromUnbxd={this.retrieveMoreProductsFromUnbxd}
          unbxdInterestCached={unbxdInterestCached}
          plpLabels={plpLabels}
          brandId={brandId}
          maxDiscount={maxDiscount}
          filterInterestHandler={this.filterInterestHandler}
          isDynamicBadgeEnabled={isDynamicBadgeEnabled}
        />
      </ScrollView>
    );
  };

  /**
   * @function getCurrentWishListProfile This function searches the current active wish list profile
   *                                     from all available wishlist summaries.
   * @return {Object} returns the active wish list profile. If it does not find the same then it returns
   *                  undefined.
   */

  getCurrentWishListProfile = () => {
    const {wishlistsSummaries} = this.props;
    const {
      profile: {name},
    } = this.state;
    // The comparison is being done by display name. This is not an ideal way to do this
    // but the way we are saving active wish list in data needs a fix.
    return wishlistsSummaries.find((itm) => itm.displayName === name);
  };

  /**
   * @function checkIfCorrectWishListId This function checks if the activeWishListId received in props is actually the
   *                                    the wish list id of the current profile. Now there is a bug in our
   *                                    application where the selector file always returns the last saved wish list as
   *                                    active wish list, instead of giving the wish list of the current profile.
   *                                    TODO: Ideally the fix should be done inside selector file.
   * @param {Object} activeWishListProfile The current wish list profile.
   * @return {Boolean} returns true if the activeWishListId passed in props is actually the wish list id of the current profile,
   *                   false otherwise.
   */
  checkIfCorrectWishListId = (activeWishListProfile) => {
    const {activeWishListId} = this.props;
    if (activeWishListProfile) {
      return activeWishListProfile.id === activeWishListId;
    }
    return false;
  };

  onAddItemToFavorites = (product) => {
    const {onAddToFavorites, createNewWishListAddItem, setLastDeletedItemId, activeWishListId} =
      this.props;
    const {productInfo} = product;
    const payload = {
      colorProductId: product.prodpartno || product.generalProductId,
      page: 'SBP',
      products: productInfo || product,
    };
    const {
      profile: {name},
    } = this.state;
    const {favoriteList} = this.state;
    let wishListId = activeWishListId;
    const activeWishListProfile = this.getCurrentWishListProfile();
    // Checking if the activeWishListId is correct or not
    if (!this.checkIfCorrectWishListId(activeWishListProfile)) {
      // if it is not correct then get the correct id and set it accordingly.
      if (activeWishListProfile) {
        wishListId = activeWishListProfile.id;
      } else {
        wishListId = '';
      }
    }
    if (product.favorite) {
      let {
        itemInfo: {itemId},
      } = product;
      itemId = itemId || product.productid;
      return setLastDeletedItemId({itemId, isSBP: true});
    }
    if (favoriteList.id) {
      payload.activeWishListId = wishListId;
      payload.loading = true;
      onAddToFavorites(payload);
    } else {
      payload.wishListName = name;
      payload.isDefault = false;
      payload.activeWishListId = wishListId;
      this.profileHasWishList = true;
      createNewWishListAddItem(payload);
    }
    return null;
  };

  getTopMatchingProduct = () => {
    const {topMatching, favoriteList} = this.state;
    const {activeWishListProducts} = this.props;
    if (!Array.isArray(activeWishListProducts)) return topMatching;
    if (typeof favoriteList.id === 'undefined') return topMatching;
    const favoriteProducts = {};
    activeWishListProducts.forEach((product) => {
      favoriteProducts[product.skuInfo.colorProductId] = product;
    });
    return topMatching.map((topMatchingProd) => {
      const product = topMatchingProd;
      const {generalProductId} = product;
      if (favoriteProducts[generalProductId]) {
        product.itemInfo = favoriteProducts[generalProductId].itemInfo;
        product.favorite = true;
      } else product.favorite = false;
      return product;
    });
  };

  parseProductResponseForMonetate = (products, filters = [], rows = 10) => {
    const recommendations = products.map((product) => ({
      generalProductId: product.itemGroupId.replace(/_(US|CA)$/, ''),
      pdpUrl: '',
      department: '',
      name: product.title,
      imagePath: product.imageLink,
      isTopMatchingProduct: product.isTopMatchingProduct,
      childInterest: product.childInterest,
    }));

    return this.getRecommendationObject(recommendations, filters, rows);
  };

  /**
   * @function updateXCordinates This function calculates the scroll position of each pill
   *  and then record the same in a global variable
   * @param {{Number}}  Xcord The x- co ordinate of the pill. This calculated from the start of the pill.
   * @param {{String}}  name The name of the pill.
   * @param {{Number}}  width The width of the pill.
   * @return Void
   */
  // eslint-disable-next-line no-shadow
  updateXCordinates = ({Xcord, name, width}) => {
    /**
     * In here we are first calculating the x-co ordinate till the mid of the pill.
     * Then we are saving it against its name in the pillXCordinates variable.
     */
    this.pillXCordinates[name] = Xcord + width / 2;
  };

  findCategory = (interest) => {
    const {productsByCategories} = this.state;
    return productsByCategories.find((p) => p.category === interest);
  };

  findInterest = (interest) => {
    const {productsByInterests} = this.state;
    return productsByInterests.find((p) => p.component.toLowerCase() === interest.toLowerCase());
  };

  // eslint-disable-next-line complexity, sonarjs/cognitive-complexity
  filterButton = (interest, type, catID) => {
    const {selectedInterest, selectedCategory} = this.state;
    let selected = type === 'category' ? selectedCategory : selectedInterest;
    if (type !== 'category') {
      if (selectedCategory !== 'All') selected = '';
      const productByInterest = this.findInterest(interest);
      if (!productByInterest && interest !== 'All') return null;
      if (
        productByInterest &&
        productByInterest.items &&
        productByInterest.items.products &&
        productByInterest.items.products.length === 0 &&
        interest !== 'All'
      )
        return null;
    }

    return (
      <FilterButton
        interest={interest}
        categoryId={catID}
        selectedCategory={selected}
        updateXCordinates={this.updateXCordinates}
        onPress={type === 'category' ? this.filterCategoryHandler : this.filterInterestHandler}
      />
    );
  };

  fetchSizesFromUnbxd = async () => {
    try {
      const apiConfigObj = getAPIConfig();
      const {unbxdTCP, unboxKeyTCP} = apiConfigObj;
      const URL = `${unbxdTCP}/${unboxKeyTCP}/search?start=0&rows=0&variants=true&variants.count=0&version=V2&facet.multiselect=true&selectedfacet=true&q=*`;
      let fetchedSizes = await getValueFromAsyncStorage(SBP_UNBXD_SIZES);
      fetchedSizes = JSON.parse(fetchedSizes);
      if (!fetchedSizes) {
        const response = await superagent.get(URL).set(getCustomHeaders());
        fetchedSizes = response;
        setValueInAsyncStorage(SBP_UNBXD_SIZES, JSON.stringify(response));
      }
      const sizeArray = fetchedSizes.body.facets.text.list.filter(
        (item) => item.facetName === 'v_tcpsize_uFilter',
      );
      return sizeArray[0].values.filter((value) => typeof value === 'string');
    } catch (error) {
      logger.error(error);
      this.setState((prevState) => ({
        ...prevState,
        error: true,
      }));
    }
    return [];
  };

  skeletonLoaderFilterCarousel = () => {
    return (
      <Fragment>
        <PLPSkeleton col={1} />
        <PLPSkeleton col={1} />
        <PLPSkeleton col={1} />
        <PLPSkeleton col={1} />
      </Fragment>
    );
  };

  /**
   * @function getCategoryPathId This function calculates the category path id of the current selected category.
   * @param {String} selectedCategory The category which has been selected from view all button
   * @param {String} gender The gender of the current child profile.
   * @return {String} The category path id of the current selected category.
   */
  getCategoryPathId = (selectedCategory, gender) => {
    const categories = this.getL2Categories(gender, true);
    let categoryId =
      categories &&
      categories.length &&
      categories.find((category) => {
        return (
          category &&
          category.name &&
          category.name.toLowerCase() === selectedCategory.toLowerCase()
        );
      });
    if (categoryId) {
      categoryId = categoryId.id;
      const departmentID = this.getDepartmentID(gender);
      return `categoryPathId:"${departmentID}>${categoryId}"`;
    }
    return '';
  };

  /**
   * @function getProducts This function modifies the product data
   * @param {Array} products The products which needs to be modified
   * @param {Array} activeWishListProducts The favorite products list of current profile.
   */

  getProducts = (products, activeWishListProducts) => {
    // If the wish list is empty, then return the products as it is.
    if (!Array.isArray(activeWishListProducts)) return products;
    const favoriteProducts = {};
    // Build the favorite products map
    activeWishListProducts.forEach((product) => {
      favoriteProducts[product.skuInfo.colorProductId] = product;
    });
    return products.map((product) => {
      let {generalProductId} = product;
      generalProductId = generalProductId || product.prodpartno;
      const productMod = product;
      // if the current products is a favorite then add some additional fields inside it.
      if (favoriteProducts[generalProductId]) {
        productMod.itemInfo = favoriteProducts[generalProductId].itemInfo;
        productMod.favorite = true;
      } else {
        productMod.favorite = false;
      }
      return productMod;
    });
  };

  /* eslint-disable sonarjs/cognitive-complexity */
  getSizesFromProfile = (parsedSize, sizeMapping) => {
    const profileSizes = [];
    Object.entries(parsedSize).forEach(([key, value]) => {
      if (value.size !== undefined && value.size.length > 0) {
        value.size.forEach((singleSize) => {
          const existingValueIndex = profileSizes.findIndex((s) => {
            return Object.keys(s)[0] === key;
          });
          if (existingValueIndex < 0) {
            const sizeObj = sizeMapping.find(
              (element) => element.size === singleSize && element.category === key,
            );
            if (sizeObj) {
              profileSizes.push({[key]: sizeObj.all_sizes});
            }
          } else {
            const existingValue = profileSizes[existingValueIndex];
            const sizeObj = sizeMapping.find(
              (element) => element.size === singleSize && element.category === key,
            );
            profileSizes.splice(existingValueIndex, 1);
            const moreSizes = Object.values(existingValue)[0];
            if (sizeObj) {
              profileSizes.push({
                [key]: [...sizeObj.all_sizes, ...moreSizes],
              });
            }
          }
        });
      }
    });
    return profileSizes;
  };

  lazyLoadL2Categories = () => {
    const {
      profile: {gender},
      l2CategoryEnd,
    } = this.state;
    const categories = this.getL2Categories(gender, true);
    if (l2CategoryEnd - NUMBER_ADDITIONAL_GRID_LAZY_LOAD >= categories.length) {
      return;
    }
    this.getClientCategories(gender, 0, false, null, true);
  };

  async filterCategoryHandler(categoryInput, categoryId) {
    const fromFilter = categoryId;
    let category = categoryInput;
    const {selectedCategory, profile, productsByCategories} = this.state;
    const isL2CategoryRendered = productsByCategories.find((element) => element.id === categoryId);
    if (fromFilter && selectedCategory !== category && !isL2CategoryRendered) {
      this.getClientCategories(profile.gender, 0, fromFilter, categoryId);
    }
    if (this.timeout) clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      if (
        typeof this.flatListRef === 'object' &&
        this.flatListRef !== null &&
        typeof this.flatListRef.scrollToOffset === 'function'
      ) {
        this.flatListRef.scrollToOffset({offset: 0, animated: false});
      }
    }, 750);
    if (category === selectedCategory) {
      category = 'All';
      this.filterScrollView.scrollTo({x: 0, animated: false});
    }
    this.setState((prevState) => ({
      ...prevState,
      selectedCategory: category,
      clickedCategory: category,
      selectedInterest: 'All',
    }));
    /**
     * If the x- co ordinate of the pill is less than half the width of the screen,
     * then we do not need to scroll it.
     */
    if (this.pillXCordinates[category.toLowerCase()] > this.windowWidth / 2) {
      /**
       * We are now scrolling to the pill which has been selected. We need to center the pill
       * in the center of the screen which is why we are subtracting the half of the window width.
       */
      this.filterScrollView.scrollTo({
        x: this.pillXCordinates[category.toLowerCase()] - this.windowWidth / 2,
        animated: true,
      });
    }
  }

  async filterInterestHandler(categoryInput) {
    let category = categoryInput;
    const {selectedInterest} = this.state;
    if (this.timeout) clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      if (
        typeof this.interestScrollView === 'object' &&
        this.interestScrollView !== null &&
        typeof this.interestScrollView.scrollTo === 'function'
      ) {
        this.interestScrollView.scrollTo({y: 0, animated: false});
      }
    }, 700);

    if (category === selectedInterest) {
      category = 'All';
      this.filterScrollView.scrollTo({x: 0, animated: false});
    }
    this.setState((prevState) =>
      Object.assign(
        {},
        {
          ...prevState,
          selectedInterest: category,
          clickedInterest: category,
          selectedCategory: 'All',
          clickedCategory: 'All',
        },
      ),
    );
    /**
     * If the x- co ordinate of the pill is less than half the width of the screen,
     * then we do not need to scroll it.
     */
    if (this.pillXCordinates[category.toLowerCase()] > this.windowWidth / 2) {
      /**
       * We are now scrolling to the pill which has been selected. We need to center the pill
       * in the center of the screen which is why we are subtracting the half of the window width.
       */
      this.filterScrollView.scrollTo({
        x: this.pillXCordinates[category.toLowerCase()] - this.windowWidth / 2,
        animated: true,
      });
    }
  }

  goToPrevScreen() {
    const {navigation} = this.props;
    navigation.navigate('Home', {});
  }

  goToEditProfile() {
    const {navigation} = this.props;
    const {profile} = this.state;
    removeValueFromAsyncStorage('plpRefresh');
    this.toggleMenu(false);
    navigateToNestedRoute(
      navigation,
      'SbpPlpStack',
      'EditChildProfile',
      Object.assign({}, profile, {
        backTo: {routeName: 'PLP', params: {refresh: true}},
      }),
    );
  }

  async changeProfile(i) {
    const {profiles, navigation} = this.props;
    const profile = profiles[i];
    await setValueInAsyncStorage(SBP_CURRENT_PROFILE_ID, profile.id.toString());
    this.setState(
      (prevState) => Object.assign({}, prevState, {profile, currentProfileIndex: i}),
      // () => this.updateListing(Object.assign({}, profiles[i], {refresh: true})),
    );
    this.analyticsFired = false;
    navigation.setParams(profiles[i]);
  }

  toggleMenu(showMenu) {
    this.setState((prevState) => Object.assign({}, prevState, {showMenu}));
  }

  showMoreProducts(category, interest) {
    this.setState((prevState) => ({
      ...prevState,
      numberOfProducts: prevState.numberOfProducts + 10,
      clickedCategory: category,
      clickedInterest: interest,
    }));
  }

  async retrieveSizesByCatFromUnbxd() {
    try {
      const {profile} = this.state;
      const {size, gender} = profile;
      const sizeStringArray = await this.fetchSizesFromUnbxd();
      const parsedSize = JSON.parse(size);
      let sizeMapping = await getValueFromAsyncStorage(SBP_SIZE_MAPPING);
      sizeMapping = JSON.parse(sizeMapping);

      if (!sizeMapping || !sizeMapping[gender]) {
        const response = await getSizeMapping(gender);
        sizeMapping = {
          ...sizeMapping,
          [gender]: response,
        };
        setValueInAsyncStorage(SBP_SIZE_MAPPING, JSON.stringify(sizeMapping));
      }

      const profileSizes = this.getSizesFromProfile(parsedSize, sizeMapping[gender]);
      const uniqueValues = [...new Set(profileSizes)];
      const sizeStringArrayNoCode = sizeStringArray.map((s) => s.split('_')[1]);
      const sizeUsedForFilter1 = uniqueValues.map((v) => {
        return {
          [Object.keys(v)[0]]: Object.values(v)[0].map(
            (s) => sizeStringArray[sizeStringArrayNoCode.indexOf(s)],
          ),
        };
      });
      return sizeUsedForFilter1.reduce((acc, curr) => {
        // eslint-disable-next-line
        acc[Object.keys(curr)[0]] = Object.values(curr)[0];
        return acc;
      }, {});
    } catch (error) {
      logger.error('retrieveSizesByCatFromUnbxd', error);
      this.setState((prevState) => ({
        ...prevState,
        error: true,
      }));
    }
    return [];
  }

  retrieveMoreProductsFromUnbxd(interest) {
    const {interestOffSet, productsByInterests, unbxdInterestCached} = this.state;
    const offSet = interestOffSet[interest];

    const specificInterest = unbxdInterestCached.filter(
      (product) => product.component.toLowerCase() === interest.toLowerCase(),
    );

    const slicedInterest =
      specificInterest[0].items.length > offSet + 10
        ? specificInterest[0].items.slice(offSet, 10 + offSet)
        : specificInterest[0].items.slice(offSet, specificInterest[0].items.length);

    const findSpecificInterest = productsByInterests.find(
      (item) => item.component.toLowerCase() === interest.toLowerCase(),
    );

    const appendedInterests = findSpecificInterest.items
      ? [...findSpecificInterest.items.products, ...slicedInterest]
      : slicedInterest;
    const findIndexOfInterest = productsByInterests.findIndex(
      (element) => element.component.toLowerCase() === interest.toLowerCase(),
    );

    const productsCopyInState = JSON.parse(JSON.stringify(productsByInterests));
    productsCopyInState[findIndexOfInterest].items.products = appendedInterests;

    this.setState((prevState) => ({
      ...prevState,
      interestOffSet: {
        ...prevState.interestOffSet,
        [interest]:
          prevState.interestOffSet[interest] +
          (specificInterest[0].items.length > offSet + 10
            ? 10
            : specificInterest[0].items.length - offSet),
      },
      productsByInterests: productsCopyInState,
    }));
  }

  async retrieveSizesFromUnbxd() {
    try {
      const {profile} = this.state;
      const {size, gender} = profile;
      const sizeStringArray = await this.fetchSizesFromUnbxd();
      const sizeUsedForFilter = [];
      let profileSizes = [];
      const parsedSize = JSON.parse(size);
      let sizeMapping = await getValueFromAsyncStorage(SBP_SIZE_MAPPING);
      sizeMapping = JSON.parse(sizeMapping);

      if (!sizeMapping || !sizeMapping[gender]) {
        const response = await getSizeMapping(gender);
        sizeMapping = {
          ...sizeMapping,
          [gender]: response,
        };
        setValueInAsyncStorage(SBP_SIZE_MAPPING, JSON.stringify(sizeMapping));
      }

      Object.entries(parsedSize).forEach(([key, value]) => {
        if (value.size !== undefined && value.size.length > 0) {
          value.size.forEach((singleSize) => {
            const sizeObj = sizeMapping[gender].find(
              (element) => element.size === singleSize && element.category === key,
            );
            const sizeObject = sizeObj && sizeObj.all_sizes ? sizeObj.all_sizes : [];
            profileSizes = [...profileSizes, ...sizeObject];
          });
        }
      });

      profileSizes = [...profileSizes, ...ACCESSORIES_SIZES[gender]];
      const uniqueValues = [...new Set(profileSizes)];
      uniqueValues.forEach((value) => {
        sizeStringArray.forEach((size1) => {
          const array = size1.split('_');
          if (array[1] === value) {
            sizeUsedForFilter.push(size1);
          }
        });
      });

      return sizeUsedForFilter;
    } catch (error) {
      logger.error(error);
      this.setState((prevState) => ({
        ...prevState,
        error: true,
      }));
    }
    return [];
  }

  async retrieveInterestsProductsFromUnbxd(productsList, filters, rows = 100) {
    return this.parseProductResponseForMonetate(productsList, filters, rows);
  }

  renderTopMatchesProducts() {
    const {topMatchingLoading, topMatching} = this.state;
    const {activeWishListProducts, isDynamicBadgeEnabled} = this.props;
    const MAX_NUM_PRODS_TOPMATCH = 20;
    let topMatchesProducts = !topMatchingLoading && topMatching.slice(0, MAX_NUM_PRODS_TOPMATCH);
    topMatchesProducts = this.getProducts(
      topMatchesProducts,
      this.profileHasWishList ? activeWishListProducts : [],
    );
    const isHorizontal = true;
    return (
      <VirtualizedList
        data={topMatchesProducts}
        keyExtractor={(item) => item.uniqueId}
        horizontal={isHorizontal}
        initialNumToRender={6}
        getItem={(data, index) => data[index]}
        getItemCount={(data) => data.length}
        renderItem={({item}) => (
          <TopMatchingProducts
            onAddItemToFavorites={this.onAddItemToFavorites}
            product={item}
            goToPDP={this.goToPDP}
            isDynamicBadgeEnabled={isDynamicBadgeEnabled}
          />
        )}
      />
    );
  }

  renderNoProductsAvailable = () => {
    const {sbpLabels} = this.props;
    return (
      <View style={styles.noProductContainer}>
        <Text style={styles.noProductText}>
          {showSBPLabels(
            sbpLabels,
            'lbl_sbp_plp_no_products',
            'No Products Available For Selected Size.',
          )}
        </Text>
      </View>
    );
  };

  // eslint-disable-next-line complexity, sonarjs/cognitive-complexity
  renderProductsByInterestsAndCategories() {
    const {
      selectedCategory,
      selectedInterest,
      productsByInterests,
      interestsLoading,
      productsByCategories,
      clickedCategory,
      favoriteList,
      profile,
      profile: {theme},
      prodsByCategory,
    } = this.state;
    const {
      activeWishListProducts,
      navigation,
      plpLabels,
      screenProps,
      isDynamicBadgeEnabled,
      quickViewLoader,
      quickViewProductId,
    } = this.props;

    const {brandId} = screenProps.apiConfig;
    const flatListData = [];
    if (interestsLoading) return <PLPSkeleton col={4} />;
    const sortedInterestGrid = productsByInterests.sort((a, b) => {
      const interestA = a.component.toLowerCase();
      const interestB = b.component.toLowerCase();
      let returnValue = 0;
      if (interestA < interestB) returnValue = -1;
      else if (interestA > interestB) returnValue = 1;
      return returnValue;
    });
    if (selectedCategory && selectedCategory !== 'All') {
      const productsByCategory =
        productsByCategories.find((p) => p.category === selectedCategory) || prodsByCategory;
      if (
        productsByCategory &&
        productsByCategory.products &&
        productsByCategory.products.length === 0
      ) {
        return this.renderNoProductsAvailable();
      }
      if (productsByCategory) {
        flatListData.push({
          category: selectedCategory,
          products: productsByCategory.products || [],
          filter: productsByCategory.filter,
        });
        const categoryPathID = this.getCategoryPathId(selectedCategory, profile.gender);
        return (
          <VirtualizedList
            data={flatListData}
            ref={(ref) => {
              this.flatListRef = ref;
            }}
            keyExtractor={(item, index) => item.category + index}
            initialNumToRender={2}
            getItem={(data, index) => data[index]}
            getItemCount={(data) => data.length}
            renderItem={({item}) => (
              <ProductsByCategories
                favoriteList={favoriteList}
                onAddItemToFavorites={this.onAddItemToFavorites}
                activeWishListProducts={this.profileHasWishList ? activeWishListProducts : []}
                profile={profile}
                theme={theme}
                category={item.category}
                products={item.products}
                clickedCategory={clickedCategory}
                numberOfProducts={100}
                goToPDP={this.goToPDP}
                quickViewLoader={quickViewLoader}
                quickViewProductId={quickViewProductId}
                openQuickAdd={this.openQuickAdd}
                showMoreProducts={this.showMoreProducts}
                navigation={navigation}
                plpLabels={plpLabels}
                brandId={brandId}
                filterCategoryHandler={this.filterCategoryHandler}
                categoryPathID={categoryPathID}
                filter={item.filter}
                isDynamicBadgeEnabled={isDynamicBadgeEnabled}
              />
            )}
          />
        );
      }
    }
    if (selectedInterest && selectedInterest !== 'All') {
      return sortedInterestGrid
        .filter((i) => i.component.toLowerCase() === selectedInterest.toLowerCase())
        .map(({component, items, allProducts, maxDiscount}) =>
          this.productsByInterest(
            component,
            items,
            allProducts,
            maxDiscount,
            isDynamicBadgeEnabled,
          ),
        );
    }
    return <View>{this.renderSBPSectionList(sortedInterestGrid)}</View>;
  }

  renderFilterCarousel = () => {
    const {
      profile: {gender, interests},
      productsByCategories,
    } = this.state;
    const AllL2Categories = this.getL2Categories(gender);
    const sortedInterestFilters = (interests && interests.split(',').sort()) || '';
    const filterInterestCarousel = ['All', ...sortedInterestFilters];
    return (
      <Fragment>
        {filterInterestCarousel
          .filter((i) => productsByCategories.map((c) => c.category).indexOf(i) < 0)
          .sort()
          .map((interest) => this.filterButton(interest))}
        {AllL2Categories.filter((c) => filterInterestCarousel.indexOf(c.name) > -1)
          .sort((a, b) => {
            const catA = a.name.toLowerCase();
            const catB = b.name.toLowerCase();
            let returnValue = 0;
            if (catA < catB) returnValue = -1;
            else if (catA > catB) returnValue = 1;
            return returnValue;
          })
          .map((c) => ({name: c.name, catID: c.id}))
          .map(({name, catID}) => this.filterButton(name, 'category', catID))}
        {AllL2Categories.filter((c) => filterInterestCarousel.indexOf(c.name) < 0)
          .map((c) => ({name: c.name, catID: c.id}))
          .map(({name, catID}) => this.filterButton(name, 'category', catID))}
      </Fragment>
    );
  };

  renderSBPSectionList(interestGrid) {
    const {
      clickedInterest,
      interestOffSet,
      favoriteList,
      unbxdInterestCached,
      profile,
      profile: {theme, name, interests},
      productsByCategories,
      topMatching: topMatchingProds,
      topMatchingLoading,
      selectedInterest,
      selectedCategory,
      clickedCategory,
      numberOfProducts,
    } = this.state;
    const {
      navigation,
      activeWishListProducts,
      plpLabels,
      screenProps,
      isDynamicBadgeEnabled,
      quickViewLoader,
      quickViewProductId,
    } = this.props;
    const {brandId} = screenProps.apiConfig;
    const topMatching = this.getTopMatchingProduct();
    const selectedL2Categories = productsByCategories
      .filter((p) => interests.indexOf(p.category) > -1)
      .sort((a, b) => {
        const categoryA = a.category.toLowerCase();
        const categoryB = b.category.toLowerCase();
        let returnValue = 0;
        if (categoryA < categoryB) returnValue = -1;
        else if (categoryA > categoryB) returnValue = 1;
        return returnValue;
      });
    const nonSelectedL2Categories = productsByCategories.filter(
      (p) => interests.indexOf(p.category) < 0,
    );
    const combinedL2Categories = [...selectedL2Categories, ...nonSelectedL2Categories];
    const isInterestGrid = true;
    const mergedData = [...interestGrid, ...combinedL2Categories];
    const sectionData = [];
    const interestData = [];
    const l2CategoriesData = [];
    const androidStyling = isAndroid() ? {marginTop: '7%'} : {};

    mergedData.forEach((section) => {
      const modifiedSection = section;
      modifiedSection.data = section.items ? section.items.products : section.products;
      if (section.items) {
        interestData.push(modifiedSection);
      } else {
        l2CategoriesData.push(modifiedSection);
      }
    });

    sectionData.push({
      title: 'Interests',
      data: [{sections: interestData}],
    });

    sectionData.push({
      title: 'Categories',
      data: [{sections: l2CategoriesData}],
    });

    return (
      <SectionList
        contentContainerStyle={styles.sectionListContainer}
        stickySectionHeadersEnabled={false}
        sections={sectionData}
        onEndReached={this.lazyLoadL2Categories}
        onEndReachedThreshold={1.4}
        keyExtractor={(item, index) => item.sections.component || item.sections.category + index}
        renderSectionHeader={({section}) => {
          if (section.title === 'Categories') return null;
          return (
            selectedCategory === 'All' &&
            selectedInterest === 'All' && (
              <Fragment>
                <View
                  style={
                    topMatchingLoading || topMatching.length !== 0
                      ? [styles.topMatchingContainter, styles.topMatchingScroll]
                      : styles.topMatchingScroll
                  }>
                  {topMatchingProds.length ? (
                    <View style={[styles.topMatching, androidStyling]}>
                      <TopMatchingMsg name={name} theme={theme} />
                    </View>
                  ) : null}
                  <View
                    style={
                      topMatchingLoading || topMatching.length !== 0
                        ? styles.topMatchingContainter
                        : null
                    }>
                    {topMatchingLoading && <PLPSkeleton col={3} />}
                    {this.renderTopMatchesProducts()}
                  </View>
                </View>
              </Fragment>
            )
          );
        }}
        /* eslint-disable sonarjs/cognitive-complexity */
        renderItem={({item}) => {
          return (
            <VirtualizedList
              data={item.sections}
              keyExtractor={(item2, index) => item2.component || item2.category + index}
              initialNumToRender={4}
              removeClippedSubviews
              getItem={(data, index) => data[index]}
              getItemCount={(data) => data.length}
              renderItem={({item: item2}) => {
                if (item2.items && item2.items.products && item2.items.products.length === 0)
                  return null;
                if (item2.products && item2.products.length === 0) return null;
                if (typeof item2.component !== 'undefined') {
                  return (
                    <ProductsByInterests
                      onAddItemToFavorites={this.onAddItemToFavorites}
                      activeWishListProducts={this.profileHasWishList ? activeWishListProducts : []}
                      profile={profile}
                      theme={theme}
                      interest={item2.component}
                      products={item2.items}
                      goToPDP={this.goToPDP}
                      quickViewLoader={quickViewLoader}
                      quickViewProductId={quickViewProductId}
                      openQuickAdd={this.openQuickAdd}
                      showMoreProducts={this.showMoreProducts}
                      clickedInterest={clickedInterest}
                      interestOffSetArray={interestOffSet}
                      favoriteList={favoriteList}
                      toggleFavorite={this.toggleFavorite}
                      navigation={navigation}
                      interestGrid={isInterestGrid}
                      retrieveMoreProductsFromUnbxd={this.retrieveMoreProductsFromUnbxd}
                      unbxdInterestCached={unbxdInterestCached}
                      plpLabels={plpLabels}
                      brandId={brandId}
                      productCount={4}
                      maxDiscount={item2.maxDiscount}
                      filterInterestHandler={this.filterInterestHandler}
                      isDynamicBadgeEnabled={isDynamicBadgeEnabled}
                    />
                  );
                }

                if (typeof item2.category !== 'undefined') {
                  return (
                    <ProductsByCategories
                      favoriteList={favoriteList}
                      onAddItemToFavorites={this.onAddItemToFavorites}
                      activeWishListProducts={this.profileHasWishList ? activeWishListProducts : []}
                      profile={profile}
                      theme={theme}
                      category={item2.category}
                      products={item2.products}
                      clickedCategory={clickedCategory}
                      numberOfProducts={numberOfProducts}
                      goToPDP={this.goToPDP}
                      quickViewLoader={quickViewLoader}
                      quickViewProductId={quickViewProductId}
                      openQuickAdd={this.openQuickAdd}
                      showMoreProducts={this.showMoreProducts}
                      navigation={navigation}
                      plpLabels={plpLabels}
                      brandId={brandId}
                      filterCategoryHandler={this.filterCategoryHandler}
                      filter={item2.filter}
                      isDynamicBadgeEnabled={isDynamicBadgeEnabled}
                    />
                  );
                }
                return null;
              }}
            />
          );
        }}
        /* eslint-enable complexity */
      />
    );
  }

  // eslint-disable-next-line complexity
  render() {
    const {navigation, profiles, isFocused} = this.props;
    if (!profiles.length) return <Text>Loading...</Text>;

    const {
      profile: {theme, id},
      showMenu,
      topMatchingLoading,
      currentProfileIndex,
      interestsLoading,
      hasNoProducts,
      stackReset,
      error,
      profile,
      carouselLoading,
    } = this.state;
    const {routeName} = navigation.state;
    let completeProfiles = [];
    if (!carouselLoading) {
      completeProfiles = profiles;
    }

    const iPhone12Styling = isDeviceIPhone12() ? {top: 0} : {};
    const android = isAndroid() ? {top: 18} : {};
    const androidStyling = isAndroid() ? {height: 105, top: -40} : {};
    const iPhone12ContainerStyling = isDeviceIPhone12() ? {paddingTop: 150} : {};
    const iPhone12StylingView = isDeviceIPhone12() ? {height: 70, top: -12} : {};
    return (
      <View style={styles.container}>
        <GlobalModals navigation={navigation} onInitialModalsActionComplete={() => {}} />
        <View style={[styles.animatedView, android, iPhone12Styling]}>
          {!carouselLoading ? (
            <View
              style={[
                styles.cover,
                {backgroundColor: Themes[theme || 0].color},
                iPhone12StylingView,
                androidStyling,
              ]}
            />
          ) : (
            <View style={styles.emptyContainer} />
          )}
          {isAndroid() && (
            <View style={[styles.androidCover, {backgroundColor: Themes[theme || 0].color}]} />
          )}
          {isAndroid() && (
            <View style={styles.androidProfile}>
              <ProfileIcon size={86} theme={theme} />
            </View>
          )}

          <Carousel
            data={completeProfiles}
            renderItem={({item, index}) => (
              <Profile
                key={item.id}
                profile={item}
                button={{icon: 'ellipsis-v', color: colorNero}}
                action={() => this.toggleMenu(true)}
                currentProfileIndex={currentProfileIndex}
                profileIndex={index}
                routeName={routeName}
              />
            )}
            sliderWidth={width}
            itemWidth={width - 115}
            onSnapToItem={this.changeProfile}
            activeSlideOffset={10}
            inactiveSlideOpacity={0.8}
            firstItem={profiles.findIndex((p) => p.id === id)}
            getItemLayout={(data, index) => ({
              length: width - 90,
              offset: (width - 100) * index,
              index,
            })}
            ref={(c) => {
              this.carousel = c;
            }}
          />
          {carouselLoading && <View style={styles.emptyContainer} />}
          <ScrollView
            horizontal
            showsHorizontalScrollIndicator={false}
            style={styles.scrollView}
            ref={(ref) => {
              this.filterScrollView = ref;
            }}>
            {topMatchingLoading && interestsLoading
              ? this.skeletonLoaderFilterCarousel()
              : this.renderFilterCarousel()}
          </ScrollView>
        </View>
        <View style={[styles.productContainer, iPhone12ContainerStyling]}>
          <View style={styles.noProduct}>
            {hasNoProducts ? <NoProductsScreen navigation={navigation} profile={profile} /> : null}
          </View>
          {this.renderProductsByInterestsAndCategories()}
        </View>
        {isFocused && interestsLoading && topMatchingLoading && !stackReset && !error ? (
          <LoadingMessage showMessage={interestsLoading || topMatchingLoading} theme={theme} />
        ) : null}
        {showMenu ? (
          <ProfileMenu
            showMenu={showMenu}
            toggleMenu={this.toggleMenu}
            theme={theme}
            goToEditProfile={this.goToEditProfile}
          />
        ) : null}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    profiles: getSBPProfiles(state).filter((prof) => prof.interests && prof.interests.length >= 1),
    userId: getUserId(state),
    wishlistsSummaries: selectWishlistsSummaries(state),
    activeWishListProducts: selectActiveWishlistProducts(state),
    activeWishListId: selectActiveWishlistId(state),
    isDataLoading: getIsDataLoading(state),
    L2Categories: state.Navigation && state.Navigation.navigationData,
    plpLabels: state.Labels.PLP.plpTiles,
    monetateProductsRefreshTime: getMonetateProductsRefreshTime(state),
    unbxdProductsRefreshFrequency: getUnbxdProductsRefreshFrequency(state),
    isDynamicBadgeEnabled: getIsDynamicBadgeEnabled(state),
    sbpLabels: getSBPLabels(state),
    quickViewLoader: getQuickViewLoader(state),
    quickViewProductId: getUniqueId(state),
  };
};

PLP.propTypes = {
  activeWishListId: number.isRequired,
  activeWishListProducts: shape([]).isRequired,
  createNewWishListAddItem: func.isRequired,
  getActiveWishlist: func.isRequired,
  isDataLoading: bool.isRequired,
  isDynamicBadgeEnabled: shape({}).isRequired,
  isFocused: bool.isRequired,
  L2Categories: shape({}).isRequired,
  monetateProductsRefreshTime: number.isRequired,
  navigation: shape({
    state: shape({
      getParam: func,
      params: shape({
        categoriesLoading: bool,
        name: string,
        size: string,
        routeName: string,
      }),
      navigate: func,
    }),
  }).isRequired,
  onAddToFavorites: func.isRequired,
  onQuickViewOpenClick: func.isRequired,
  plpLabels: shape({}).isRequired,
  profiles: arrayOf(Object).isRequired,
  sbpLabels: shape({}).isRequired,
  screenProps: arrayOf(Object).isRequired,
  sendCategoryAnalytics: func.isRequired,
  sendTopMatchingProductsAnalytics: func.isRequired,
  setChildProfiles: func.isRequired,
  setLastDeletedItemId: func.isRequired,
  trackPageLoad: func.isRequired,
  unbxdProductsRefreshFrequency: number.isRequired,
  updateProductsByProfile: func.isRequired,
  userId: string.isRequired,
  wishlistsSummaries: shape([]).isRequired,
  quickViewLoader: bool.isRequired,
  quickViewProductId: string.isRequired,
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateProductsByProfile: (payload) => {
      dispatch(updateProductsByProfileAction(payload));
    },
    getActiveWishlist: (payload, isSBP) => dispatch(getActiveWishlistAction(payload, isSBP)),
    setChildProfiles: (profiles) => dispatch(setChildProfilesAction(profiles)),
    createNewWishListAddItem: (formData) => dispatch(createNewWishListAddItemAction(formData)),
    onAddToFavorites: (payload) => dispatch(addItemsToWishlist(payload)),
    setLastDeletedItemId: (payload) => dispatch(setLastDeletedItemIdAction(payload)),
    onQuickViewOpenClick: (payload) => {
      dispatch(openQuickViewWithValues(payload));
    },
    sendCategoryAnalytics: (categories) => {
      if (categories.length === 0) return;
      dispatch(
        setClickAnalyticsData({
          sbp_categories: categories.map((category) => category.category).join(','),
          customEvents: ['event30'],
        }),
      );
      dispatch(
        trackClick({
          name: names.screenNames.sbp_top_categories_plp,
          module: 'browse',
        }),
      );
    },
    sendTopMatchingProductsAnalytics: (products, profileName) => {
      if (products.length === 0) return;
      dispatch(
        setClickAnalyticsData({
          sbp_profile_name: profileName,
          sbp_products_name: products
            .map((product) => {
              const {productInfo} = product;
              return productInfo.name;
            })
            .join(','),
          customEvents: ['event29'],
        }),
      );
      dispatch(
        trackClick({
          name: names.screenNames.sbp_top_matches_plp,
          module: 'browse',
        }),
      );
    },
    trackPageLoad: (payload, currentScreen) => {
      const {
        products,
        customEvents,
        searchType,
        departmentList,
        categoryList,
        listingCount,
        breadcrumb,
        subCategoryList,
        internalCampaignId,
        searchText,
        originType,
        supressProps,
        productsCount,
        storeId,
        externalCampaignId,
        omMID,
        omRID,
      } = payload;

      dispatch(
        setClickAnalyticsData({
          products,
          customEvents,
          searchType,
          departmentList,
          categoryList,
          listingCount,
          breadcrumb,
          subCategoryList,
          internalCampaignId,
          searchText,
          originType,
          supressProps,
          productsCount,
          storeId,
          ...(externalCampaignId && {externalCampaignId}),
          ...(omMID && {omMID}),
          ...(omRID && {omRID}),
        }),
      );
      setTimeout(() => {
        dispatch(
          trackPageView({
            currentScreen,
            pageData: {
              ...payload,
            },
            props: {
              initialProps: {
                pageProps: {
                  pageData: {
                    ...payload,
                  },
                },
              },
            },
            supressProps,
          }),
        );
      }, 100);
    },
  };
};
export {PLP};
export default withNavigationFocus(connect(mapStateToProps, mapDispatchToProps)(PLP));
