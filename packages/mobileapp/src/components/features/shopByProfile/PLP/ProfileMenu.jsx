// 9fbef606107a605d69c0edbcd8029e5d 
import React, {Fragment} from 'react';
import {View, Text, TouchableHighlight, Image} from 'react-native';
import Modal from 'react-native-modalbox';
import CustomIcon from '@tcp/core/src/components/common/atoms/Icon';
import {connect} from 'react-redux';
import {PropTypes} from 'prop-types';
import Themes from '../ShopByProfile.theme';
import styles from './profileMenu.styles';
import {showSBPLabels} from './helper';
import {colorGainsboro} from '../ShopByProfile.styles';
import {getSBPLabels} from '../ShopByProfile.selector';

const closeImg = require('../../../../assets/images/shopByProfile/elements/close3x.png');

export const ProfileMenu = props => {
  const {showMenu, toggleMenu, goToEditProfile, theme, sbpLabels} = props;

  const editTextCMSLabel = showSBPLabels(sbpLabels, 'lbl_sbp_edit_profile_cta', 'Edit Profile');
  const cancelLabel = showSBPLabels(sbpLabels, 'lbl_sbp_cancel_text_edit_profile_cta', 'Cancel');

  return (
    <View style={styles.container}>
      <Modal
        animationDuration={400}
        swipeThreshold={100}
        style={styles.modal}
        position="bottom"
        swipeToClose
        isOpen={showMenu}
        entry="bottom"
        onClosed={() => toggleMenu(false)}
        coverScreen>
        <View style={styles.modalContainer}>
          <TouchableHighlight
            onPress={() => goToEditProfile()}
            activeOpacity={0.66}
            underlayColor={colorGainsboro}
            style={styles.goToEditProfile}
            accessibilityRole="button">
            <Fragment>
              <CustomIcon
                name="pencil"
                size="fs24"
                style={[styles.editIcon, {color: Themes[theme || 0].accent}]}
              />
              <Text style={styles.modalText}>{editTextCMSLabel}</Text>
            </Fragment>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={() => toggleMenu(false)}
            activeOpacity={0.66}
            underlayColor={colorGainsboro}
            style={styles.toggleMenu}
            accessibilityRole="button">
            <Fragment>
              <Image
                source={closeImg}
                style={[styles.closeIcon, {tintColor: Themes[theme || 0].accent}]}
              />
              <Text style={styles.modalText}>{cancelLabel}</Text>
            </Fragment>
          </TouchableHighlight>
        </View>
      </Modal>
    </View>
  );
};

ProfileMenu.propTypes = {
  theme: PropTypes.number.isRequired,
  goToEditProfile: PropTypes.func.isRequired,
  showMenu: PropTypes.bool.isRequired,
  toggleMenu: PropTypes.func.isRequired,
  sbpLabels: PropTypes.shape({}).isRequired,
};

const mapStateToProps = state => ({
  sbpLabels: getSBPLabels(state),
});

export default connect(mapStateToProps)(ProfileMenu);
