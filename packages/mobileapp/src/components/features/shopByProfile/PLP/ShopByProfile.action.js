// 9fbef606107a605d69c0edbcd8029e5d 
export const addProductsByProfile = payload => {
  return {
    type: 'ADD_PRODUCTS_BY_PROFILE',
    payload,
  };
};

export const updateProductsByProfile = payload => {
  return {
    type: 'UPDATE_PRODUCTS_BY_PROFILE',
    payload,
  };
};

export const deleteProductsByProfile = payload => {
  return {
    type: 'DELETE_PRODUCTS_BY_PROFILE',
    payload,
  };
};
