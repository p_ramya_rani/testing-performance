// 9fbef606107a605d69c0edbcd8029e5d 
export const addChildProfile = payload => {
  return {
    type: 'ADD_CHILD_PROFILE',
    payload,
  };
};

export const addChildProfiles = payload => {
  return {
    type: 'ADD_CHILD_PROFILES',
    payload,
  };
};

export const setChildProfiles = payload => {
  return {
    type: 'SET_CHILD_PROFILES',
    payload,
  };
};

export const deleteChildProfile = payload => {
  return {
    type: 'DELETE_CHILD_PROFILE',
    payload,
  };
};
