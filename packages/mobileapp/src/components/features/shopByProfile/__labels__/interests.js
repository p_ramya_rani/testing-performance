// 9fbef606107a605d69c0edbcd8029e5d 
export default name => ({
  TITLE: `What does ${name} like?`,
  SUBHEADER: `What other categories are you looking for?`,
});
