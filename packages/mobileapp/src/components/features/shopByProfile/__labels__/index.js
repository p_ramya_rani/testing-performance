// 9fbef606107a605d69c0edbcd8029e5d 
import name from './name';
import age from './age';
import gender from './gender';
import size from './size';
import interests from './interests';
import colors from './colors';
import styles from './styles';
import plpHeaderTitle from './plpHeaderTitle';

export default {
  name,
  age,
  gender,
  size,
  interests,
  colors,
  styles,
  plpHeaderTitle,
};
