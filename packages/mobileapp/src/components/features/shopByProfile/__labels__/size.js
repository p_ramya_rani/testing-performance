// 9fbef606107a605d69c0edbcd8029e5d 
export default name => ({
  TITLE: `What size(s) does ${name} wear?`,
  SUBTITLE: `Select 1-3 sizes per style to help filter available sizes while shopping.`,
});
