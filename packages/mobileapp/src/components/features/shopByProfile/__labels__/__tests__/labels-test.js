// 9fbef606107a605d69c0edbcd8029e5d 
/* eslint-disable */
import age from '../age';
import colors from '../colors';
import gender from '../gender';
import interests from '../interests';
import Name from '../name';
import plpHeaderTitle from '../plpHeaderTitle';
import size from '../size';
import styles from '../styles';

const name = 'Name';

describe('labels', () => {
  const ageLabel = age(name);
  it('should return right label', () => {
    expect(ageLabel.SUBTITLE).toBe(`How are you related?`);
  });
  it('should return right label', () => {
    expect(ageLabel.TITLE).toBe(`When is ${name}'s birthday?`);
  });

  const colorsL = colors(name);
  it('should return right label', () => {
    expect(colorsL.TITLE).toBe(`What are ${name}'s favorite colors?`);
  });

  const genderL = gender();
  it('should return right label', () => {
    expect(genderL.TITLE).toBe(`Select a department`);
  });

  const interestsL = interests(name);
  it('should return right label', () => {
    expect(interestsL.TITLE).toBe(`What does ${name} like?`);
  });
  it('should return right label', () => {
    expect(interestsL.SUBHEADER).toBe(`What other categories are you looking for?`);
  });

  const nameL = Name(name);
  it('should return right label', () => {
    expect(nameL).toStrictEqual({
      TITLE: 'Who are you shopping for',
      SUBTITLE: `Select an icon for ${name}`,
      PLACEHOLDER: 'Name',
      INSTRUCTION: 'Choose an icon',
    });
  });

  it('should return right label', () => {
    expect(plpHeaderTitle(name)).toStrictEqual({
      TITLE: `${name}'s Shop`,
    });
  });

  it('should return right label', () => {
    expect(size(name)).toStrictEqual({
      TITLE: `What size(s) does ${name} wear?`,
      SUBTITLE: `Select 1-3 sizes per style to help filter available sizes while shopping.`,
    });
  });

  it('should return right label', () => {
    expect(styles(name)).toStrictEqual({
      TITLE: `What is ${name}'s style?`,
    });
  });
});
