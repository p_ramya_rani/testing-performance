// 9fbef606107a605d69c0edbcd8029e5d 
export default name => ({
  TITLE: 'Who are you shopping for',
  SUBTITLE: `Select an icon for ${name}`,
  PLACEHOLDER: 'Name',
  INSTRUCTION: 'Choose an icon',
});
