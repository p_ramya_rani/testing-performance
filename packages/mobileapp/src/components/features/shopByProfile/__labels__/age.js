// 9fbef606107a605d69c0edbcd8029e5d 
export default name => ({
  TITLE: `When is ${name}'s birthday?`,
  SUBTITLE: `How are you related?`,
});
