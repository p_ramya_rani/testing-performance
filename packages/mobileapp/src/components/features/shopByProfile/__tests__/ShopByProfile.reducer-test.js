// 9fbef606107a605d69c0edbcd8029e5d
import ShopByProfileReducer from '../ShopByProfile.reducer';

const arr = [];
const payload = {
  id: '1234',
};
const action1 = {
  type: 'ADD_CHILD_PROFILE',
  payload,
};
const action3 = {
  type: 'ADD_CHILD_PROFILES',
  payload: [payload],
};
const action4 = {
  type: 'SET_CHILD_PROFILES',
  payload: [payload],
};
const action5 = {
  type: 'DELETE_CHILD_PROFILE',
  payload: [payload],
};
const action = {
  type: undefined,
  payload,
};

describe('ShopByProfileReducer', () => {
  it('should return value payload for no state', () => {
    expect(ShopByProfileReducer(undefined, action1)).toStrictEqual(new Array(action1.payload));
    expect(ShopByProfileReducer(undefined, action3)).toStrictEqual(action3.payload);
    expect(ShopByProfileReducer(undefined, action4)).toStrictEqual(action4.payload);
    expect(ShopByProfileReducer(undefined, action5)).toStrictEqual(new Array(payload));
  });
  it('should return value payload 1', () => {
    expect(ShopByProfileReducer(arr, action1)).toStrictEqual(new Array(action1.payload));
    expect(ShopByProfileReducer(new Array(action1.payload), action1)).toStrictEqual(
      new Array(action1.payload),
    );
  });
  it('should return value payload 3', () => {
    expect(ShopByProfileReducer(arr, action3)).toStrictEqual(action3.payload);
  });
  it('should return value payload 4', () => {
    expect(ShopByProfileReducer(arr, action4)).toStrictEqual(action4.payload);
  });

  it('should return value payload 5', () => {
    expect(ShopByProfileReducer(arr, action5)).toStrictEqual(new Array(payload));
    expect(ShopByProfileReducer(new Array(action5.payload), action5.payload)).toStrictEqual(
      new Array([payload]),
    );
  });

  it('should return value payload for delete', () => {
    expect(
      ShopByProfileReducer([{id: 1}, {id: 2}], {
        type: 'DELETE_CHILD_PROFILE',
        payload: {id: 1},
      }),
    ).toStrictEqual(new Array({id: 2}));
  });

  it('should return value payload as is', () => {
    expect(ShopByProfileReducer(arr, action)).toStrictEqual(arr);
  });

  it('should return value payload as is', () => {
    expect(ShopByProfileReducer(undefined, action)).toStrictEqual(arr);
  });
});
