// 9fbef606107a605d69c0edbcd8029e5d 
/* eslint-disable */
import {
  getIsSbpEnabled,
  getIsSbpEnabledAbTest,
  getIsSbpGymEnabledAbTest,
  isSBPEnabled,
  isSbpGymEnabled,
} from '../ShopByProfile.selector';

const state = {
  session: {
    siteDetails: {
      IS_SHOP_BY_PROFILE_ENABLED: true,
    },
  },
  AbTest: {
    parsedAbtestResponse: {
      IS_SHOP_BY_PROFILE_ENABLED: true,
      IS_SHOP_BY_PROFILE_ENABLED_GYM: true,
    },
  },
};
describe('getIsSbpEnabled', () => {
  it('should be true', () => {
    expect(getIsSbpEnabled(state)).toBeTruthy();
  });
});

describe('getIsSbpEnabledAbTest', () => {
  it('should be true', () => {
    expect(getIsSbpEnabledAbTest(state)).toBeTruthy();
  });
});

describe('isSBPEnabled', () => {
  it('should be true', () => {
    expect(isSBPEnabled(state)).toBeTruthy();
  });
});

describe('getIsSbpGymEnabledAbTest', () => {
  it('should be true', () => {
    expect(getIsSbpGymEnabledAbTest(state)).toBeTruthy();
  });
});

describe('isSbpGymEnabled', () => {
  it('should be true', () => {
    expect(isSbpGymEnabled(state)).toBeTruthy();
  });
});
