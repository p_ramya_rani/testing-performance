// 9fbef606107a605d69c0edbcd8029e5d 
import {
  addChildProfile,
  addChildProfiles,
  deleteChildProfile,
  setChildProfiles,
} from '../ShopByProfile.action';

const payload = {
  dummy: 'data',
};

describe('addChildProfile', () => {
  it('should return value payload 1', () => {
    expect(addChildProfile(payload)).toStrictEqual({
      type: 'ADD_CHILD_PROFILE',
      payload,
    });
  });
});

describe('addChildProfiles', () => {
  it('should return value payload 2', () => {
    expect(addChildProfiles(payload)).toStrictEqual({
      type: 'ADD_CHILD_PROFILES',
      payload,
    });
  });
});

describe('deleteChildProfile', () => {
  it('should return value payload 3', () => {
    expect(deleteChildProfile(payload)).toStrictEqual({
      type: 'DELETE_CHILD_PROFILE',
      payload,
    });
  });
});

describe('setChildProfiles', () => {
  it('should return value payload 3', () => {
    expect(setChildProfiles(payload)).toStrictEqual({
      type: 'SET_CHILD_PROFILES',
      payload,
    });
  });
});
