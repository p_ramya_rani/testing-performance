// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable */
import {
  canRefreshMonetateProds,
  canRefreshUnbxdProds,
  deleteCache,
  getMonetateProductsFromCache,
  getUnbxdProductsFromCache,
  setMonetateProductsToCache,
  setUnbxdProdsNextRefreshTime,
  setUnbxdProductsToCache,
} from '../Caching';
import * as caching from '../Caching';
import * as dependency from '@tcp/core/src/utils';

describe('Caching', () => {
  it('should not be match values', () => {
    canRefreshMonetateProds(100, '1').then(data => {
      expect(data).toBeFalsy();
    });
  });

  it('should not be match values', () => {
    canRefreshUnbxdProds('1').then(data => {
      expect(data).toBeFalsy();
    });
  });

  it('should not be match values', async () => {
    Object.defineProperty(dependency, 'getBrand', {
      getBrand: () => {
        return 'TCP';
      },
      getValueFromAsyncStorage: () => {
        return null;
      },
    });
    const val = await canRefreshUnbxdProds('1');
    expect(val).toBeTruthy();
  });

  it('should not be match values', async () => {
    Object.defineProperty(dependency, 'getBrand', {
      getBrand: () => {
        return 'TCP';
      },
    });
    await deleteCache({id: '1'}, 'all');
  });

  it('should not be match values', async () => {
    Object.defineProperty(dependency, 'getBrand', {
      getBrand: () => {
        return 'TCP';
      },
    });
    await deleteCache({id: '1'}, 'unbxd');
  });

  it('should not be match values', async () => {
    Object.defineProperty(dependency, 'getBrand', {
      getBrand: () => {
        return 'TCP';
      },
      getValueFromAsyncStorage: () => {
        return '1';
      },
    });
    const parse = jest.spyOn(JSON, 'parse').mockImplementation(() => 1);
    const val = await getMonetateProductsFromCache('1');
    expect(parse).toHaveBeenCalledTimes(1);
    expect(parse).toHaveBeenCalledWith(null);
    expect(val).toBe(1);
  });

  it('should not be match values', async () => {
    Object.defineProperty(dependency, 'getBrand', {
      getBrand: () => {
        return 'TCP';
      },
      getValueFromAsyncStorage: () => {
        return '1';
      },
      setValueInAsyncStorage: () => {
        return null;
      },
    });
    const val = await setMonetateProductsToCache({id: '1'}, '1');
    expect(val).toBeUndefined();
  });

  it('should not be match values', async () => {
    Object.defineProperty(dependency, 'getBrand', {
      getBrand: () => {
        return 'TCP';
      },
      setValueInAsyncStorage: () => {
        return null;
      },
    });
    const deleteSpy = jest.spyOn(caching, 'deleteCache');
    const val = await setUnbxdProdsNextRefreshTime(10, '1', 'all');
    expect(val).toBeUndefined();
    expect(deleteSpy).toHaveBeenCalledTimes(0);
  });
  it('should not be match values', async () => {
    Object.defineProperty(dependency, 'getBrand', {
      getBrand: () => {
        return 'TCP';
      },
      setValueInAsyncStorage: () => {
        return null;
      },
    });
    const deleteSpy = jest.spyOn(caching, 'deleteCache');
    const val = await setUnbxdProdsNextRefreshTime(0, '1', 'all');
    expect(val).toBeUndefined();
    expect(deleteSpy).toHaveBeenCalledTimes(0);
  });

  it('should not be match values', async () => {
    Object.defineProperty(dependency, 'getBrand', {
      getBrand: () => {
        return 'TCP';
      },
      setValueInAsyncStorage: () => {
        return null;
      },
    });
    const spy = jest.spyOn(caching, 'setUnbxdProdsNextRefreshTime');
    const val = await setUnbxdProductsToCache({id: '1'}, 0, '1');
    expect(val).toBeUndefined();
    expect(spy).toHaveBeenCalledTimes(0);
  });

  it('should not be match values', () => {
    deleteCache('1').then(data => {
      expect(data).toBeUndefined();
    });
  });

  it('should not be match values', () => {
    getMonetateProductsFromCache('1').then(data => {
      expect(data).toBeNull();
    });
  });

  it('should not be match values', () => {
    getUnbxdProductsFromCache('1').then(data => {
      expect(data).toBeNull();
    });
  });

  it('should not be match values', () => {
    let setValueInAsyncStorageSpy = dependency.setValueInAsyncStorage;
    setMonetateProductsToCache({}, '1').then(data => {
      expect(data).toBeUndefined();
      expect(setValueInAsyncStorage).toHaveBeenCalledTimes(2);
    });
  });

  it('should not be match values', () => {
    let setValueInAsyncStorageSpy = dependency.setValueInAsyncStorage;
    setUnbxdProdsNextRefreshTime(100, '1').then(data => {
      expect(data).toBeUndefined();
      expect(setValueInAsyncStorage).toHaveBeenCalledTimes(1);
    });
  });

  it('should not be match values', () => {
    let setValueInAsyncStorageSpy = dependency.setValueInAsyncStorage;
    setUnbxdProdsNextRefreshTime(0, '1').then(data => {
      expect(data).toBeUndefined();
      expect(setValueInAsyncStorage).toHaveBeenCalledTimes(1);
    });
  });

  it('should not be match values', () => {
    setUnbxdProductsToCache({}, 100, '1').then(data => {
      expect(data).toBeUndefined();
    });
  });
});
