// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable */
const ShopByProfileReducer = (state, action) => {
  if (typeof state === 'undefined') {
    state = [];
  }
  const actionId = action.payload && action.payload.id;
  const i = state.findIndex(p => p.id === actionId);

  switch (action.type) {
    case 'ADD_CHILD_PROFILE':
      if (i > -1) {
        const newState = new Array(...state);
        newState[i] = action.payload;
        return newState;
      }
      return new Array(...state).concat(action.payload);
    case 'DELETE_CHILD_PROFILE':
      if (i > -1) {
        const newState = new Array(...state);
        newState.splice(i, 1);
        return newState;
      }
      return new Array(...state).concat(action.payload);
    case 'ADD_CHILD_PROFILES':
      return [...state, ...action.payload];
    case 'SET_CHILD_PROFILES':
      const listOfProfiles = new Array(...action.payload);
      if (listOfProfiles.length) {
        return listOfProfiles;
      } else {
        return state;
      }
    default:
      return state;
  }
};
/* eslint-enable */

export default ShopByProfileReducer;
