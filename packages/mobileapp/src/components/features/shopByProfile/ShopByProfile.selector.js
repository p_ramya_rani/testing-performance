// 9fbef606107a605d69c0edbcd8029e5d
import {parseBoolean} from '@tcp/core/src/utils';
import {getABtestFromState} from '@tcp/core/src/utils/utils.app';

export const getIsSbpEnabled = (state) => {
  return (
    state.session &&
    state.session.siteDetails &&
    parseBoolean(state.session.siteDetails.IS_SHOP_BY_PROFILE_ENABLED)
  );
};

export const getIsSbpEnabledAbTest = (state) => {
  return parseBoolean(getABtestFromState(state.AbTest).IS_SHOP_BY_PROFILE_ENABLED);
};

export const getIsSbpGymEnabledAbTest = (state) => {
  return parseBoolean(getABtestFromState(state.AbTest).IS_SHOP_BY_PROFILE_ENABLED_GYM);
};

export const isSBPEnabled = (state) => {
  return getIsSbpEnabled(state) && getIsSbpEnabledAbTest(state);
};

export const isSbpGymEnabled = (state) => {
  return getIsSbpGymEnabledAbTest(state);
};

export const getSBPLabels = (state) => {
  return state && state.Labels && state.Labels.global && state.Labels.global.ShopByProfile;
};

export const getSBPInterests = (state) => {
  return state && state.SBPProfiles && state.SBPProfiles.childInterests;
};

export const getSBPDepartmentSizes = (state) => {
  return state && state.SBPProfiles && state.SBPProfiles.departmentSizes;
};

export const getSBPProfiles = (state) => {
  return state && state.SBPProfiles && state.SBPProfiles.sbpProfiles;
};

export const isSBPShopMenuABTestEnabled = (state) => {
  return parseBoolean(getABtestFromState(state.AbTest).IS_SHOP_BY_PROFILE_ENABLED_SHOP_MENU);
};

export const getSBPEnabledShopMenu = (state) => {
  return getIsSbpEnabled(state) && isSBPShopMenuABTestEnabled(state);
};
