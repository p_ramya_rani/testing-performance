import {calculateCompletion} from '../../../common/molecules/Profile';

export const checkIfProfileIsComplete = (sbpProfiles) => {
  let numCompleteProfiles = 0;
  sbpProfiles.forEach((profile) => {
    if (calculateCompletion(profile) === 100) {
      numCompleteProfiles += 1;
    }
  });
  return numCompleteProfiles;
};

export const checkIfUserHasIncompleteProfiles = (sbpProfiles) => {
  let numberOfIncompleteProfiles = 0;
  sbpProfiles.forEach((profile) => {
    if (calculateCompletion(profile) < 100) {
      numberOfIncompleteProfiles += 1;
    }
  });
  return numberOfIncompleteProfiles;
};

export default {checkIfProfileIsComplete, checkIfUserHasIncompleteProfiles};
