/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React, {Component, Fragment} from 'react';
import isEqual from 'lodash/isEqual';
import {Dimensions, Image, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {func, number, string, shape, bool} from 'prop-types';
import {
  getValueFromAsyncStorage,
  navigateToNestedRoute,
  parseBoolean,
  setValueInAsyncStorage,
} from '@tcp/core/src/utils';
import {getChildrenAction} from '@tcp/core/src/components/features/account/common/organism/BirthdaySavingsList/container/BirthdaySavingsList.actions';
import ModalBox from 'react-native-modalbox';
import {
  getChildren,
  getSBPProfile,
  getUserId,
  getUserName,
  getUserEmail,
} from '@tcp/core/src/components/features/account/User/container/User.selectors';
import logger from '@tcp/core/src/utils/loggerInstance';
import {USER_REDUCER_KEY} from '@tcp/core/src/constants/reducer.constants';
import {createNewWishListAction} from '@tcp/core/src/components/features/browse/Favorites/container/Favorites.actions';
import {connect} from 'react-redux';
import names from '@tcp/core/src/constants/eventsName.constants';
import {trackClick, setClickAnalyticsData} from '@tcp/core/src/analytics/actions';
import {isAndroid} from '@tcp/core/src/utils/utils.app';
import {getBirthdaySavingsCallEnabled} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import {updateSBPEvent} from '@tcp/core/src/components/common/organisms/Header/container/Header.actions';
import ShopByProfileTab from './molecules/ShopByProfileTab/views/ShopByProfileTab.native';
import {showSBPLabels} from '../PLP/helper';
import Profile, {calculateCompletion} from '../../../common/molecules/Profile';
import ClickTracker from '../../../common/atoms/ClickTracker';
import {getSBPLabels, getSBPProfiles} from '../ShopByProfile.selector';
import {checkIfUserHasIncompleteProfiles, checkIfProfileIsComplete} from './utils';
import styles from './styles';
import {
  INITIAL_DATA,
  MAX_CARD_HEIGHT,
  MAX_NUMBER_PROFILES,
  SWIPE_AREA,
  SBP_USER_EMAIL,
  SBP_GET_PROFILES_DELAY,
  SBP_CURRENT_PROFILE_ID,
} from '../ShopByProfile.constants';
import {createSBPProfile, getSBPProfilesAction} from '../ShopByProfileAPI/ShopByProfile.actions';

const {height} = Dimensions.get('window');
const iconShop = require('../../../../assets/images/shopByProfile/elements/icon-shop-stars_2020-12-11/icon-shop-stars.png');
const grpIconSrc = require('../../../../assets/images/shopByProfile/elements/btn-circle-plus-blue_2020-12-21/btn-circle-plus-blue.png');
const closeIconSrc = require('../../../../assets/images/shopByProfile/elements/close3x.png');

class ListProfile extends Component {
  state = {cardOpen: false, first: '', showTab: true};

  async componentDidMount() {
    const {getProfilesSBP, userId, userEmail, shouldBirthdayAPIBeCalled} = this.props;
    await setValueInAsyncStorage(SBP_USER_EMAIL, userEmail);
    getProfilesSBP(userId);
    setTimeout(async () => {
      if (shouldBirthdayAPIBeCalled > 0) {
        await this.checkChildrenBirthdays();
      }
      await this.checkFirst();
    }, SBP_GET_PROFILES_DELAY);
  }

  componentDidUpdate(prevProps, prevState) {
    if (isEqual(prevProps, this.props) && isEqual(prevState, this.state)) return;
    const {childrenBirthdays} = this.props;
    if (prevProps.childrenBirthdays === null && childrenBirthdays) {
      this.updateProfiles();
    }
    const {sbpProfiles} = this.props;
    if (sbpProfiles.length !== 0) {
      let isPregenerateProfile = false;
      sbpProfiles.forEach((profile) => {
        if (
          profile &&
          profile.is_pregenerate_profile === true &&
          calculateCompletion(profile) < 100
        )
          isPregenerateProfile = true;
      });
      const {pregeneratedProfile} = this.state;
      if (isPregenerateProfile !== pregeneratedProfile)
        // eslint-disable-next-line react/no-did-update-set-state
        this.setState((state) => ({
          ...state,
          pregeneratedProfile: isPregenerateProfile,
        }));
    }
  }

  checkChildrenBirthdays = async () => {
    const {getChildrenBirthdays} = this.props;
    getChildrenBirthdays({skipLoader: true});
  };

  checkFirst = async () => {
    const value = await getValueFromAsyncStorage('appFirst');
    if (value === null) {
      await setValueInAsyncStorage('appFirst', 'false');
      await this.updateProfiles();
      await this.setState({first: 'false'});
    } else {
      await setValueInAsyncStorage('appFirst', 'true');
      await this.updateProfiles();
      await this.setState({first: 'true'});
    }
  };

  addChildrenProfile = async (profiles, total) => {
    let {childrenBirthdays} = this.props;
    const {addChildProfile} = this.props;
    const {userId} = this.props;
    const profilesNew = [...profiles];
    childrenBirthdays = [...childrenBirthdays];
    for (let i = 0; i < total; i += 1) {
      const profile = getSBPProfile(childrenBirthdays[i]);
      profile.gender = undefined;
      profile.relation = INITIAL_DATA.relation;
      profile.is_pregenerate_profile = true;
      const payload = {
        data: profile,
        userId,
      };
      setTimeout(() => {
        addChildProfile(payload);
      }, 2000 * i);
    }
    return profilesNew;
  };

  closeMenu = async () => {
    await setValueInAsyncStorage('appFirst', 'true');
    await this.toggleCard();
    this.setState({first: 'true'});
  };

  goToPlP = async (profile, index) => {
    const {navigation} = this.props;
    if (this.modal) this.modal.close();
    this.setState((prevState) => ({
      ...prevState,
      showTab: false,
    }));
    await setValueInAsyncStorage(SBP_CURRENT_PROFILE_ID, profile.id.toString());
    setTimeout(() => {
      navigateToNestedRoute(
        navigation,
        'SbpPlpStack',
        'PLP',
        Object.assign({}, profile, {refresh: true, isSBP: true, index}),
      );
      this.setState((prevState) => ({
        ...prevState,
        showTab: true,
      }));
    }, 500);
  };

  goToCreateProfile = () => {
    const {navigation, updateSBPEventAction} = this.props;
    if (this.modal) this.modal.close();
    this.setState((prevState) => ({
      ...prevState,
      showTab: false,
    }));
    setTimeout(() => {
      navigateToNestedRoute(navigation, 'ShopByProfile', 'NameInput');
      this.setState((prevState) => ({
        ...prevState,
        showTab: true,
      }));
    }, 400);
    setTimeout(() => {
      updateSBPEventAction(false);
    }, 2000);
  };

  toggleCard = async () => {
    const {getProfilesSBP, userId} = this.props;
    const {cardOpen} = this.state;
    if (!cardOpen) {
      getProfilesSBP(userId);
    }
    this.setState((prevState) => Object.assign({}, prevState, {cardOpen: !prevState.cardOpen}));
  };

  closeModal = () => {
    this.setState((prevState) => ({
      ...prevState,
      cardOpen: false,
    }));
  };

  async updateProfiles() {
    const {childrenBirthdays, sendProfileAnalytics, userEmail, sbpProfiles} = this.props;
    try {
      const SBProfiles = sbpProfiles;
      const isPregeneratedExists = SBProfiles.filter((profile) =>
        parseBoolean(profile && profile.is_pregenerate_profile),
      );
      if (isPregeneratedExists.length > 0 || SBProfiles.length > 0 || !childrenBirthdays)
        return Promise.resolve();
      const value = await getValueFromAsyncStorage('isBirthdayProfilesGenerated');
      if (typeof value === 'undefined' || value !== null) return Promise.resolve(SBProfiles);
      let left = MAX_NUMBER_PROFILES - SBProfiles.length;
      left = left && left > childrenBirthdays.size ? childrenBirthdays.size : left;
      if (!left) return Promise.resolve(SBProfiles);
      const profiles = await this.addChildrenProfile(SBProfiles, left);
      sendProfileAnalytics(profiles, userEmail);
      return setValueInAsyncStorage('isBirthdayProfilesGenerated', 'true');
    } catch (error) {
      logger.error('SBP_LIST_PROFILE', error);
    }
    return Promise.resolve();
  }

  renderEmptyProfile() {
    const {sbpLabels, isSBPUpdateEvent} = this.props;
    const createCustomShopLabel = showSBPLabels(
      sbpLabels,
      'lbl_sbp_create_profile_CTA',
      'CREATE CUSTOM SHOP',
    );

    let events = ['event23'];
    if (isSBPUpdateEvent) {
      events = [...events, 'event164'];
    }
    return (
      <View>
        <ClickTracker
          as={TouchableOpacity}
          name={names.screenNames.sbp_create_profile}
          module="browse"
          clickData={{customEvents: events}}
          onPress={this.goToCreateProfile}
          accessibilityRole="button"
          style={styles.createProfileBtn}>
          <Image style={styles.createProfileImg} source={iconShop} />
          <Text style={styles.createProfileText}>{createCustomShopLabel}</Text>
          <View style={styles.createProfileIconMain}>
            <Image source={grpIconSrc} style={styles.createProfileIcon} />
          </View>
        </ClickTracker>
      </View>
    );
  }

  renderProfiles() {
    const {sbpProfiles, navigation} = this.props;
    return (
      <View
        style={[
          styles.profilesContainer,
          isAndroid() && sbpProfiles.length >= 4 ? styles.profilesContainerFour : {},
        ]}>
        {sbpProfiles.slice(0, 4).map((profile, index) => {
          return (
            <ClickTracker
              as={TouchableOpacity}
              name={names.screenNames.sbp_welcome_back_profile}
              module="browse"
              clickData={{customEvents: ['event25']}}
              onPress={() =>
                calculateCompletion(profile) === 100 ? this.goToPlP(profile, index) : {}
              }
              accessibilityRole="button">
              <Profile
                profile={profile}
                navigation={navigation}
                action={() => this.goToPlP(profile, index)}
                button={{icon: 'arrow-right', color: 'white'}}
                closeModal={this.closeModal}
              />
            </ClickTracker>
          );
        })}
      </View>
    );
  }

  renderTab() {
    const {sbpLabels, sbpProfiles} = this.props;
    const numCompleteProfiles = checkIfProfileIsComplete(sbpProfiles);
    const numIncompleteProfiles = checkIfUserHasIncompleteProfiles(sbpProfiles);
    return (
      <ShopByProfileTab
        onPress={this.toggleCard}
        numCompleteProfiles={numCompleteProfiles}
        numIncompleteProfiles={numIncompleteProfiles}
        sbpLabels={sbpLabels}
        isHomePage
      />
    );
  }

  renderFragmentInner() {
    const {sbpLabels} = this.props;
    const setCustomShopLabel = showSBPLabels(
      sbpLabels,
      'lbl_sbp_welcome_unfinished_header',
      'Set up a custom shop for your child',
    );
    const incompleteProfileLabel = showSBPLabels(
      sbpLabels,
      'lbl_sbp_welcome_unfinished',
      'Pick up where you left off! Finish setting up your profile(s) for an easy personalized shopping experience every time you shop!',
    );
    return (
      <Fragment>
        <Text style={styles.fragmentTextThree}>{setCustomShopLabel}</Text>
        <Text style={styles.fragmentTextFour}>{incompleteProfileLabel}</Text>
      </Fragment>
    );
  }

  renderFragment(numberOfProfilesNew) {
    const {sbpLabels, sbpProfiles} = this.props;
    const incompleteProfiles = checkIfUserHasIncompleteProfiles(sbpProfiles);
    const yourShopsLabel = showSBPLabels(sbpLabels, 'lbl_sbp_title', 'YOUR SHOPS');
    const maxNumberProfileLabel = showSBPLabels(
      sbpLabels,
      'lbl_sbp_max_limit',
      'You’ve reached the limit of profiles you can create! (4 max.). Edit or delete an existing profile to customize it for another child.',
    );
    return (
      <Fragment>
        <Text style={styles.fragmentText}>{yourShopsLabel}</Text>
        {numberOfProfilesNew === MAX_NUMBER_PROFILES && (
          <Text style={styles.fragmentTextTwo}>{maxNumberProfileLabel}</Text>
        )}
        {incompleteProfiles >= 1 && numberOfProfilesNew < 4 ? this.renderFragmentInner() : null}
      </Fragment>
    );
  }

  renderCardContainer() {
    const {userName, sbpLabels} = this.props;
    const {pregeneratedProfile} = this.state;
    const createCustomShopLabel = showSBPLabels(
      sbpLabels,
      'lbl_sbp_welcome_newuser',
      'Create a custom shop for your child now!',
    );
    const welcomeMessageLabel = showSBPLabels(
      sbpLabels,
      'lbl_sbp_welcome_newuser_row2',
      'Simply add their birthday, sizing & a few other preferences for an easy, personalized shopping experience every time!',
    );
    const pregenProfileLabel = showSBPLabels(
      sbpLabels,
      'lbl_sbp_welcome_existing',
      'We’ve pulled some info from your My Place Rewards account to help jumpstart the process. Finish the profile(s) below or create a new one. For a personalized shopping experience every time!',
    );
    return (
      <View style={styles.cardContainerInner2}>
        <Text style={styles.cardInnerText1}>
          {sbpLabels && sbpLabels.lbl_sbp_welcome_header
            ? `${sbpLabels.lbl_sbp_welcome_header}${userName}`
            : `Welcome back, ${userName}`}
        </Text>
        <Text style={styles.cardInnerText2}>{createCustomShopLabel}</Text>
        {!pregeneratedProfile ? (
          <Text style={styles.cardInnerText3}>{welcomeMessageLabel}</Text>
        ) : (
          <View>
            <Text style={styles.cardInnerText4}>{pregenProfileLabel}</Text>
          </View>
        )}
      </View>
    );
  }

  /* eslint-disable complexity, sonarjs/cognitive-complexity */
  renderCard(numberOfProfiles) {
    const {pregeneratedProfile, first} = this.state;
    const {sbpProfiles} = this.props;
    const numCompleteProfiles = checkIfProfileIsComplete(sbpProfiles);
    const numIncompleteProfiles = checkIfUserHasIncompleteProfiles(sbpProfiles);
    let additionalHeight = 0;
    let multiplier = numberOfProfiles === 4 ? 80 : 110;
    let numberOfProfilesNew = numberOfProfiles;
    if (first === 'true' && numberOfProfilesNew === 0) {
      numberOfProfilesNew += 1;
    }
    if (first !== 'true' && numCompleteProfiles < 1) {
      additionalHeight = 85;
    }
    if (first === 'true' && numberOfProfilesNew >= 1) {
      multiplier += 14;
      additionalHeight = 45;
    }
    if (
      first === 'true' &&
      numberOfProfilesNew >= 1 &&
      (numCompleteProfiles > 0 || numIncompleteProfiles === 0)
    ) {
      multiplier += 13;
      additionalHeight = -7 * (numCompleteProfiles + numIncompleteProfiles);
    }
    if (
      first === 'true' &&
      (numberOfProfiles === 2 || numberOfProfiles === 3) &&
      numIncompleteProfiles === 0
    ) {
      additionalHeight = -40 * numberOfProfiles;
    }
    return (
      <View
        style={[
          styles.cardContainer,
          {
            height: MAX_CARD_HEIGHT + additionalHeight + numberOfProfilesNew * multiplier,
          },
        ]}>
        <TouchableOpacity
          style={styles.cardButton}
          accessibilityRole="button"
          onPress={() => {
            this.closeMenu();
          }}>
          <Image style={styles.cardButtonImg} source={closeIconSrc} />
        </TouchableOpacity>
        <View style={styles.cardContainerInner} />
        {!pregeneratedProfile && (numCompleteProfiles > 0 || numIncompleteProfiles === 1)
          ? this.renderFragment(numberOfProfilesNew)
          : this.renderCardContainer()}
        <ScrollView
          style={[
            styles.scrollViewContainer,
            {
              maxHeight: MAX_CARD_HEIGHT + numberOfProfilesNew * 75,
            },
          ]}>
          {this.renderProfiles()}
          {numberOfProfilesNew < MAX_NUMBER_PROFILES && this.renderEmptyProfile()}
        </ScrollView>
      </View>
    );
  }

  render() {
    const {sbpProfiles} = this.props;
    const numberOfProfiles = sbpProfiles.length;
    const {cardOpen, showTab} = this.state;
    let heightCal = {
      height: 0,
    };
    if (cardOpen) {
      heightCal = {height};
    }
    return (
      <View style={[styles.modalboxContaner, heightCal]}>
        {showTab ? this.renderTab() : null}
        {cardOpen ? (
          <ModalBox
            ref={(ref) => {
              this.modal = ref;
            }}
            animationDuration={300}
            swipeThreshold={10}
            style={[styles.modalbox, {height: MAX_CARD_HEIGHT + (isAndroid() ? 400 : 200)}]}
            position="bottom"
            swipeArea={SWIPE_AREA}
            isOpen={cardOpen}
            onClosed={() => this.toggleCard()}
            entry="bottom"
            coverScreen>
            {this.renderCard(numberOfProfiles)}
          </ModalBox>
        ) : null}
      </View>
    );
  }
}

function sendAnalytics(noOfProfiles, nameOfProfiles, email, dispatch) {
  if (noOfProfiles === 0) return;
  dispatch(
    setClickAnalyticsData({
      sbp_no_of_profiles: String(noOfProfiles),
      sbp_name_of_profiles: nameOfProfiles,
      sbp_email: email,
    }),
  );
  dispatch(trackClick({name: names.screenNames.sbp_profiles, module: 'account'}));
}

ListProfile.propTypes = {
  addChildProfile: func.isRequired,
  childrenBirthdays: shape([]).isRequired,
  getChildrenBirthdays: func.isRequired,
  getProfilesSBP: func.isRequired,
  navigation: shape({}).isRequired,
  sbpLabels: shape({
    lbl_sbp_welcome_header: string,
  }).isRequired,
  sbpProfiles: shape([]).isRequired,
  sendProfileAnalytics: func.isRequired,
  userEmail: string.isRequired,
  userId: number.isRequired,
  userName: string.isRequired,
  shouldBirthdayAPIBeCalled: number.isRequired,
  isSBPUpdateEvent: bool.isRequired,
  updateSBPEventAction: func.isRequired,
};

const mapStateToProps = (state) => ({
  surveyData:
    state[USER_REDUCER_KEY] &&
    state[USER_REDUCER_KEY].get('survey') &&
    state[USER_REDUCER_KEY].get('survey').get('answers'),
  height: 300 + state.ChildProfiles.length * 120,
  userId: getUserId(state),
  userName: getUserName(state),
  userEmail: getUserEmail(state),
  childrenBirthdays: getChildren(state),
  sbpLabels: getSBPLabels(state),
  sbpProfiles: getSBPProfiles(state),
  shouldBirthdayAPIBeCalled: getBirthdaySavingsCallEnabled(state),
  isSBPUpdateEvent: state.Header && state.Header.isUpdateSBPEvent,
});

const mapDispatchToProps = (dispatch) => ({
  getChildrenBirthdays: (payload) => {
    dispatch(getChildrenAction(payload));
  },
  createNewWishList: (formData) => {
    dispatch(createNewWishListAction(formData));
  },
  sendProfileAnalytics: (profiles, email) => {
    const sbpProfile = profiles.map((profile) => profile.name).join(',');
    sendAnalytics(profiles.length, sbpProfile, email, dispatch);
  },
  getProfilesSBP: (payload) => {
    dispatch(getSBPProfilesAction(payload));
  },
  addChildProfile: (payload) => {
    dispatch(createSBPProfile(payload));
  },
  updateSBPEventAction: (payload) => {
    dispatch(updateSBPEvent(payload));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(ListProfile);
