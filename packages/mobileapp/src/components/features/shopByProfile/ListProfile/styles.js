/* eslint-disable react-native/sort-styles */
// 9fbef606107a605d69c0edbcd8029e5d
import {Dimensions, StyleSheet} from 'react-native';
import {isAndroid} from '@tcp/core/src/utils/utils.app';
import {
  colorBlack,
  colorWhite,
  colorMortar,
  colorNero,
  colorShadyLady,
  fontFamilyNunitoRegular,
  fontFamilyNunitoSemiBold,
  fontFamilyNunitoExtraBold,
  fontFamilyTofinoBold,
  fontSizeFourteen,
} from '../ShopByProfile.styles';

const {height, width} = Dimensions.get('window');
const deviceHeight = Dimensions.get('screen').height;
let widthDivider = 37;
let topPosition = 90;
if (isAndroid()) {
  const bottomNavBarHeight = deviceHeight - height;
  topPosition = bottomNavBarHeight > 0 ? 15 : 40;
  widthDivider = 175;
}

const transparent = 'transparent';

const styles = StyleSheet.create({
  cardButton: {
    height: 50,
    position: 'absolute',
    right: -10,
    top: 20,
    width: 50,
    zIndex: 9999,
  },
  cardButtonImg: {
    height: 18,
    width: 18,
  },
  cardContainer: {
    backgroundColor: colorWhite,
    borderTopEndRadius: 30,
    borderTopStartRadius: 30,
    bottom: 5,
    elevation: 20,
    paddingBottom: 32,
    position: 'absolute',
    shadowOpacity: 0.4,
    shadowRadius: 5,
    width: '100%',
  },
  cardContainerInner: {
    alignSelf: 'center',
    backgroundColor: colorMortar,
    borderRadius: 5,
    height: 4,
    marginVertical: 16,
    width: '30%',
  },
  cardContainerInner2: {
    marginBottom: -15,
  },
  cardInnerText1: {
    color: colorNero,
    fontFamily: fontFamilyNunitoSemiBold,
    fontSize: fontSizeFourteen,
    marginBottom: 10,
    textAlign: 'center',
    width: '100%',
  },
  cardInnerText2: {
    color: colorNero,
    fontFamily: fontFamilyTofinoBold,
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 10,
    textAlign: 'center',
    width: '100%',
  },
  cardInnerText3: {
    color: colorNero,
    fontFamily: fontFamilyNunitoRegular,
    fontSize: fontSizeFourteen,
    marginBottom: 10,
    paddingHorizontal: 20,
    textAlign: 'center',
    width: '100%',
  },
  cardInnerText4: {
    color: colorNero,
    fontFamily: fontFamilyNunitoRegular,
    fontSize: fontSizeFourteen,
    paddingHorizontal: 20,
    textAlign: 'center',
    width: '100%',
  },
  createProfileBtn: {
    alignItems: 'center',
    backgroundColor: colorWhite,
    borderColor: colorShadyLady,
    borderRadius: 50,
    borderStyle: 'dashed',
    borderWidth: 1,
    flexDirection: 'row',
    height: 74,
    margin: 24,
  },
  createProfileIcon: {
    height: 45,
    width: 45,
  },
  createProfileIconMain: {
    alignItems: 'center',
    backgroundColor: colorBlack,
    borderRadius: 50,
    height: 45,
    justifyContent: 'center',
    margin: 12,
    position: 'absolute',
    right: 0,
    width: 45,
  },
  createProfileImg: {
    height: 50,
    marginHorizontal: 20,
    resizeMode: 'contain',
    width: 50,
  },
  createProfileText: {
    color: colorNero,
    fontFamily: fontFamilyNunitoExtraBold,
    fontSize: fontSizeFourteen,
  },
  fragmentText: {
    color: colorNero,
    fontFamily: fontFamilyNunitoSemiBold,
    fontSize: fontSizeFourteen,
    textAlign: 'center',
  },
  fragmentTextFour: {
    color: colorNero,
    fontFamily: fontFamilyNunitoRegular,
    fontSize: fontSizeFourteen,
    marginBottom: -15,
    marginTop: 10,
    paddingHorizontal: 20,
    textAlign: 'center',
    width: '100%',
  },
  fragmentTextThree: {
    color: colorNero,
    fontFamily: fontFamilyTofinoBold,
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: 10,
    textAlign: 'center',
    width: '100%',
  },
  fragmentTextTwo: {
    color: colorNero,
    fontFamily: fontFamilyNunitoRegular,
    fontSize: fontSizeFourteen,
    marginTop: 20,
    paddingHorizontal: 20,
    textAlign: 'center',
    width: '100%',
  },
  modalbox: {
    backgroundColor: transparent,
  },
  modalboxContaner: {
    left: 0,
    position: 'absolute',
    top: topPosition,
    width: '100%',
  },
  profilesContainer: {
    marginBottom: -10,
    marginHorizontal: 24,
  },
  profilesContainerFour: {
    marginBottom: 50,
  },
  scrollViewContainer: {
    marginTop: 15,
  },
  toggleCardContainer: {
    height: 52,
    paddingHorizontal: 16,
    top: height - (isAndroid() ? 175 : 225),
    width: '100%',
  },
  toggleCardContainer11: {
    height: 52,
    paddingHorizontal: 15,
    top: height - (isAndroid() ? 150 : 280),
    width: '100%',
  },
  toggleCardInner: {
    alignItems: 'center',
    backgroundColor: colorWhite,
    borderRadius: 25.5,
    elevation: 9,
    flex: 1,
    flexDirection: 'row',
    height: height / 12,
    justifyContent: 'space-around',
    shadowColor: colorBlack,
    shadowOffset: {
      height: 4,
      width: 0,
    },
    shadowOpacity: 0.32,
    shadowRadius: 5.46,
    width: '100%',
  },
  toggleCardInner2: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  toggleCardInner22: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  toggleCardInner3: {
    height: 55,
    width: 55,
    right: width / widthDivider,
  },
  toggleCardInner4: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  toggleCardInnerImg: {
    height: 55,
    resizeMode: 'contain',
    width: 55,
  },
  toggleCardInnerImg2: {
    height: 45,
    width: 45,
  },
  toggleCardInnerTxt: {
    color: colorNero,
    fontFamily: fontFamilyNunitoExtraBold,
    fontSize: 16,
    marginHorizontal: '5%',
  },
  toggleCardInnerTxtIPhone12: {
    color: colorNero,
    fontFamily: fontFamilyNunitoExtraBold,
    fontSize: 16,
    marginHorizontal: '-2%',
    paddingLeft: 7,
  },
  toggleCardInnerTxtTwo: {
    color: colorNero,
    fontFamily: fontFamilyNunitoExtraBold,
    fontSize: 16,
    marginHorizontal: '18%',
  },
  toggleCardInnerTxtTwoIPhone12: {
    color: colorNero,
    fontFamily: fontFamilyNunitoExtraBold,
    fontSize: 16,
    marginLeft: '22%',
  },
});

export default styles;
