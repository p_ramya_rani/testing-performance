import React from 'react';
import {shallow} from 'enzyme';
import ProfileTile from '../views/ProfileTile';

describe('ProfileTile', () => {
  let component;

  beforeEach(() => {
    const props = {
      profile: {
        id: 'ce6fbb26-9655-418d-b7c9-4f8f8d6cb9c1',
        theme: 7,
        size: {
          Tops: {fit: 'Regular', size: ['8']},
          Bottoms: {fit: 'Regular', size: ['8']},
          'Jackets & Outerwear': {size: ['8']},
          Pajamas: {size: ['8']},
          Shoes: {size: ['YOUTH 2']},
        },
        relation: 'Parent',
        gender: 'BOY',
        name: 'Mikey',
        fit: 'REGULAR',
        month: 'August',
        shoe: 'TODDLER 1',
        year: 2015,
        interests: 'Gaming,Animals,Positivity',
        styles: 0,
        favorite_colors: 'blue,red',
      },
      navigateProfile: jest.fn(),
      index: 1,
    };
    component = shallow(<ProfileTile {...props} />);
  });

  it('ProfileTile should be defined', () => {
    expect(component).toBeDefined();
  });

  it('should render ProfileTile correctly when complete', () => {
    expect(component).toMatchSnapshot();
  });

  it('should render ProfileTile correctly when incomplete', () => {
    const props = {
      profile: {
        id: 'f990f3f9-fc80-4f4b-b9ed-8305e429e296',
        theme: 11,
        size: {
          Tops: {fit: 'Regular', size: ['8']},
          Bottoms: {fit: 'Regular', size: ['8']},
          'Jackets & Outerwear': {size: ['8']},
          Pajamas: {size: ['8']},
          Shoes: {size: ['YOUTH 2']},
        },
        relation: 'Grandparent',
        gender: 'BOY',
        name: 'Josh',
        month: 'August',
        year: 2015,
      },
    };
    const wrapper = shallow(<ProfileTile {...props} />);
    expect(wrapper).toMatchSnapshot();
  });
});
