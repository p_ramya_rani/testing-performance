import styled from 'styled-components/native';
import Themes from '../../../../ShopByProfile.theme';

const getProfileBackgroundThemeColor = (props) => {
  const {profileTheme, percentage, theme} = props;
  if (percentage < 100) return `background: ${theme.colors.WHITE}`;
  const backgroundColor = Themes && Themes[profileTheme || 0] && Themes[profileTheme || 0].color;
  return `background: ${backgroundColor}`;
};

export const TouchableContainer = styled.TouchableOpacity`
  width: 119px;
  height: 110px;
  ${getProfileBackgroundThemeColor}
  border-radius: 16px;
  justify-content: center;
  align-items: center;
  box-shadow: 0 5px 10px rgba(0, 0, 0, 0.12);
  margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
`;

export const CompletedProfileWrapper = styled.View`
  justify-content: center;
  align-items: center;
`;

export const IncompleteProfileWrapper = styled.View`
  justify-content: center;
  align-items: center;
`;

export default {TouchableContainer, CompletedProfileWrapper, IncompleteProfileWrapper};
