import {BodyCopy} from '@tcp/core/src/components/common/atoms';
import React from 'react';
import ProgressCircle from 'react-native-progress-circle';
import PropTypes from 'prop-types';
import {
  TouchableContainer,
  CompletedProfileWrapper,
  IncompleteProfileWrapper,
} from '../styles/ProfileTile.style';
import ProfileIcon from '../../../../../../common/atoms/ProfileIcon';
import {calculateCompletion} from '../../../../../../common/molecules/Profile';
import {colorSummerSky} from '../../../../ShopByProfile.styles';

const CompletedProfile = ({profile}) => {
  const {theme, name} = profile;
  return (
    <CompletedProfileWrapper>
      <ProfileIcon size={70} theme={theme} />
      <BodyCopy
        fontFamily="secondary"
        fontWeight="semibold"
        fontSize="fs12"
        text={name}
        margin="5px 0px 0px 0px"
      />
    </CompletedProfileWrapper>
  );
};

const IncompleteProfile = ({profile}) => {
  const percentage = calculateCompletion(profile);
  const {name} = profile;
  return (
    <IncompleteProfileWrapper>
      <ProgressCircle
        radius={38}
        percent={percentage}
        color={colorSummerSky}
        borderWidth={6}
        bgColor="#ffffff"
        shadowColor="#f3f3f3">
        <BodyCopy
          fontFamily="secondary"
          fontWeight="extrabold"
          fontSize="fs18"
          text={`${percentage}%`}
        />
      </ProgressCircle>
      <BodyCopy
        fontFamily="secondary"
        fontWeight="semibold"
        fontSize="fs12"
        text={name}
        margin="5px 0px 0px 0px"
      />
    </IncompleteProfileWrapper>
  );
};

const Profile = ({profile}) => {
  if (calculateCompletion(profile) < 100) return <IncompleteProfile profile={profile} />;
  return <CompletedProfile profile={profile} />;
};

const ProfileTile = ({profile, navigateProfile, index}) => {
  const {theme} = profile;
  const percentage = calculateCompletion(profile);
  return (
    <TouchableContainer
      profileTheme={theme}
      percentage={percentage}
      onPress={() => navigateProfile(profile, index)}>
      <Profile profile={profile} />
    </TouchableContainer>
  );
};

ProfileTile.propTypes = {
  profile: PropTypes.shape({}).isRequired,
  navigateProfile: PropTypes.func.isRequired,
  index: PropTypes.number.isRequired,
};

CompletedProfile.propTypes = {
  profile: PropTypes.shape({}).isRequired,
};

IncompleteProfile.propTypes = {
  profile: PropTypes.shape({}).isRequired,
};

Profile.propTypes = {
  profile: PropTypes.shape({}).isRequired,
};

export default ProfileTile;
