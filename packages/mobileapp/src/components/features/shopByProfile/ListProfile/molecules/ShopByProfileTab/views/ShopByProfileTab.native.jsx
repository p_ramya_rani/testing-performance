import React from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';
import {isDeviceIPhone12, showSBPLabels} from '../../../../PLP/helper';
import styles from '../../../styles';

const iconShop = require('../../../../../../../assets/images/shopByProfile/elements/icon-shop-stars_2020-12-11/icon-shop-stars.png');
const arrowUpSrc = require('../../../../../../../assets/images/shopByProfile/elements/btn-circle-arrow-up-blue_2020-12-11/btn-circle-arrow-up-blue.png');
const blueCirclePlus = require('../../../../../../../assets/images/shopByProfile/elements/btn-circle-plus-blue_2020-12-21/btn-circle-plus-blue.png');

const containerStyling = (isHomePage) => {
  if (isHomePage) return styles.toggleCardContainer;
  return {
    height: 60,
    borderRadius: 37,
    marginVertical: 10,
    paddingHorizontal: 16,
  };
};

const ShopByProfileTab = ({
  onPress,
  numCompleteProfiles,
  numIncompleteProfiles,
  sbpLabels,
  isHomePage,
}) => {
  const yourShopsLabel = showSBPLabels(sbpLabels, 'lbl_sbp_title_profile_exists', 'YOUR SHOPS');
  const createACustomShopLabel = showSBPLabels(sbpLabels, 'lbl_sbp_title', 'CREATE A CUSTOM SHOP!');

  const createHorizontalMargin = isDeviceIPhone12()
    ? styles.toggleCardInnerTxtIPhone12
    : styles.toggleCardInnerTxt;
  const yourShopHorizontalMargin = isDeviceIPhone12()
    ? styles.toggleCardInnerTxtTwoIPhone12
    : styles.toggleCardInnerTxtTwo;

  const circularIcon = isHomePage ? arrowUpSrc : blueCirclePlus;

  return (
    <TouchableOpacity
      onPress={onPress}
      style={containerStyling(isHomePage)}
      accessibilityRole="button">
      <View style={styles.toggleCardInner}>
        <View style={styles.toggleCardInner2}>
          <View style={styles.toggleCardInner3}>
            <Image style={styles.toggleCardInnerImg} source={iconShop} />
          </View>
          {numCompleteProfiles > 0 || (numIncompleteProfiles > 0 && isHomePage) ? (
            <Text style={yourShopHorizontalMargin}>{yourShopsLabel}</Text>
          ) : (
            <Text style={createHorizontalMargin}>{createACustomShopLabel}</Text>
          )}
        </View>
        <View style={styles.toggleCardInner4}>
          <Image style={styles.toggleCardInnerImg2} source={circularIcon} />
        </View>
      </View>
    </TouchableOpacity>
  );
};

ShopByProfileTab.propTypes = {
  onPress: PropTypes.func,
  numCompleteProfiles: PropTypes.number,
  numIncompleteProfiles: PropTypes.number,
  sbpLabels: PropTypes.shape({}).isRequired,
  isHomePage: PropTypes.bool,
};

ShopByProfileTab.defaultProps = {
  isHomePage: false,
  onPress: () => {},
  numCompleteProfiles: 0,
  numIncompleteProfiles: 0,
};

export default ShopByProfileTab;
