import React from 'react';
import {shallow} from 'enzyme';
import ShopByProfileTab from '../views/ShopByProfileTab.native';

describe('ShopByProfileTab', () => {
  let component;

  beforeEach(() => {
    const props = {
      onPress: jest.fn(),
      numCompleteProfiles: 2,
      numIncompleteProfiles: 0,
      sbpLabels: {},
      isHomePage: true,
    };
    component = shallow(<ShopByProfileTab {...props} />);
  });

  it('ShopByProfileTab should be defined', () => {
    expect(component).toBeDefined();
  });

  it('should render ShopByProfileTab correctly', () => {
    expect(component).toMatchSnapshot();
  });
});
