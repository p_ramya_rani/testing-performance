import styled from 'styled-components/native';

export const Container = styled.View``;

export const CreateNewProfileIcon = styled.Image`
  width: 70px;
  height: 70px;
`;

export const TouchableComponent = styled.TouchableOpacity`
  margin-left: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
`;

export default {Container, CreateNewProfileIcon, TouchableComponent};
