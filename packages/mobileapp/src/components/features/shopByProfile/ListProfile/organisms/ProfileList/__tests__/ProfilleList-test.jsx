import React from 'react';
import {shallow} from 'enzyme';
import {ProfileListVanilla} from '../views/ProfileList';

describe('ProfileListVanilla', () => {
  let component;

  beforeEach(() => {
    const props = {
      sbpLabels: {},
      navigation: {
        navigate: jest.fn(),
        dispatch: jest.fn(),
      },
      sbpProfiles: [],
    };
    component = shallow(<ProfileListVanilla {...props} />);
  });

  it('ProfileListVanilla should be defined', () => {
    expect(component).toBeDefined();
  });

  it('should render ProfileListVanilla correctly', () => {
    expect(component).toMatchSnapshot();
  });
});
