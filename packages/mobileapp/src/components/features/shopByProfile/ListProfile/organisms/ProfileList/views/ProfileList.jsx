import React, {Component} from 'react';
import {FlatList, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {navigateToNestedRoute} from '@tcp/core/src/utils';
import names from '@tcp/core/src/constants/eventsName.constants';
import {
  getChildren,
  getUserEmail,
  getUserId,
  getUserName,
} from '@tcp/core/src/components/features/account/User/container/User.selectors';
import {getBirthdaySavingsCallEnabled} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import {BodyCopy} from '@tcp/core/src/components/common/atoms';
import {setValueInAsyncStorage} from '@tcp/core/src/utils/utils';
import {getSBPLabels, getSBPProfiles} from '../../../../ShopByProfile.selector';
import ShopByProfileTab from '../../../molecules/ShopByProfileTab/views/ShopByProfileTab.native';
import ProfileTile from '../../../molecules/ProfileTile/views/ProfileTile';
import {showSBPLabels} from '../../../../PLP/helper';
import {Container, CreateNewProfileIcon} from '../styles/ProductList.style';
import {SBP_CURRENT_PROFILE_ID} from '../../../../ShopByProfile.constants';
import {calculateCompletion, nextScreen} from '../../../../../../common/molecules/Profile';
import {checkIfProfileIsComplete} from '../../../utils';
import ClickTracker from '../../../../../../common/atoms/ClickTracker';

const createNewProfileIcon = require('../../../../../../../assets/images/shopByProfile/elements/icon-addprofile-btn_2021-11-01/icon-addprofile-btn.png');

const listFooterStyling = {
  justifyContent: 'center',
  alignItems: 'center',
  paddingRight: 16,
};

const flatListStyling = {
  paddingTop: 10,
  paddingBottom: 20,
};

const createButtonStyling = {
  marginLeft: 16,
};

class ProfileList extends Component {
  state = {};

  keyExtractor = (item) => item.id;

  renderItem = ({item, index}) => (
    <ProfileTile profile={item} navigateProfile={this.navigateProfile} index={index} />
  );

  navigateToCreateProfileFlow = () => {
    const {navigation} = this.props;
    navigateToNestedRoute(navigation, 'ShopByProfile', 'NameInput', {fromNavMenu: true});
  };

  navigateProfile = (profile, index) => {
    const {navigation} = this.props;
    if (calculateCompletion(profile) === 100) {
      setValueInAsyncStorage(SBP_CURRENT_PROFILE_ID, profile.id.toString());
      navigateToNestedRoute(
        navigation,
        'SbpPlpStack',
        'PLP',
        Object.assign({}, profile, {refresh: true, isSBP: true, index}),
      );
    } else {
      const navigateToScreen = nextScreen(profile);
      navigation.navigate(
        navigateToScreen,
        Object.assign({}, profile, {isIncomplete: true, fromNavMenu: true}),
      );
    }
  };

  renderCreateNewProfileIcon = () => {
    const {sbpProfiles} = this.props;
    if (sbpProfiles.length >= 4) return null;
    return (
      <ClickTracker
        as={TouchableOpacity}
        onPress={this.navigateToCreateProfileFlow}
        name={names.screenNames.sbp_create_profile}
        module="browse"
        accessibilityRole="button"
        clickData={{customEvents: ['event23', 'event171']}}
        style={createButtonStyling}>
        <CreateNewProfileIcon source={createNewProfileIcon} />
      </ClickTracker>
    );
  };

  sortedProfiles = () => {
    const {sbpProfiles} = this.props;
    return sbpProfiles.sort(
      (profileA, profileB) => calculateCompletion(profileB) - calculateCompletion(profileA),
    );
  };

  renderListProfiles = () => {
    return (
      <FlatList
        data={this.sortedProfiles()}
        renderItem={this.renderItem}
        keyExtractor={this.keyExtractor}
        horizontal
        ListFooterComponent={this.renderCreateNewProfileIcon}
        ListFooterComponentStyle={listFooterStyling}
        showsHorizontalScrollIndicator={false}
        style={flatListStyling}
      />
    );
  };

  renderCopy = () => {
    const {sbpLabels, sbpProfiles} = this.props;
    if (sbpProfiles.length === 0) return null;
    const createCustomizeShopLabel = showSBPLabels(
      sbpLabels,
      'lbl_sbp_create_custom_shop_nav_menu',
      'Create & Customize Their Shop!',
    );
    const yourShopsLabel = showSBPLabels(sbpLabels, 'lbl_sbp_shop_nav_menu', 'Your Shops:');
    const numberOfCompleteProfiles = checkIfProfileIsComplete(sbpProfiles);
    const textValue = numberOfCompleteProfiles > 0 ? yourShopsLabel : createCustomizeShopLabel;
    return (
      <BodyCopy
        fontFamily="secondary"
        fontWeight="bold"
        fontSize="fs20"
        text={textValue}
        margin="0px 0px 6px 16px"
      />
    );
  };

  renderShopByProfileTab = () => {
    const {sbpLabels} = this.props;
    return <ShopByProfileTab sbpLabels={sbpLabels} onPress={this.navigateToCreateProfileFlow} />;
  };

  renderContainer = () => {
    const {sbpProfiles} = this.props;
    return (
      <Container>
        {this.renderCopy()}
        {sbpProfiles.length > 0 ? this.renderListProfiles() : this.renderShopByProfileTab()}
      </Container>
    );
  };

  render() {
    return this.renderContainer();
  }
}

const mapStateToProps = (state) => ({
  userId: getUserId(state),
  userName: getUserName(state),
  userEmail: getUserEmail(state),
  childrenBirthdays: getChildren(state),
  sbpLabels: getSBPLabels(state),
  sbpProfiles: getSBPProfiles(state),
  shouldBirthdayAPIBeCalled: getBirthdaySavingsCallEnabled(state),
});

ProfileList.propTypes = {
  sbpLabels: PropTypes.shape({}).isRequired,
  navigation: PropTypes.shape({}).isRequired,
  sbpProfiles: PropTypes.shape([]),
};

ProfileList.defaultProps = {
  sbpProfiles: [],
};

export default connect(mapStateToProps)(ProfileList);

export {ProfileList as ProfileListVanilla};
