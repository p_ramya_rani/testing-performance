// 9fbef606107a605d69c0edbcd8029e5d 
/* eslint-disable */
import React from 'react';
import {shallow} from 'enzyme';
import Component from '../index';

describe('Component', () => {
  let component;

  beforeEach(() => {
    const props = {
      navigation: {
        navigate: jest.fn(),
        dispatch: jest.fn(),
      },
      onCardOpen: jest.fn(),
      profiles: [],
    };
    component = shallow(<Component {...props} />);
  });

  it('Component should be defined', () => {
    expect(component).toBeDefined();
  });

  it('Component should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
});
