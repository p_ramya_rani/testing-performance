// 9fbef606107a605d69c0edbcd8029e5d
import {call, put, takeLatest} from 'redux-saga/effects';
import logger from '@tcp/core/src/utils/loggerInstance';
import SHOP_BY_PROFILE_CONSTANTS from './ShopByProfile.constants';
import {
  deleteProfile,
  fetchChildInterests,
  fetchDepartmentSizes,
  fetchSBPProfiles,
  insertChildProfile,
} from './ShopByProfileAPIHandlers';
import {
  removeSBPProfile,
  setChildInterests,
  setDepartmentSizes,
  setSBPProfiles,
  setCreatedSBPProfile,
  setUpdatedSBPProfile,
} from './ShopByProfile.actions';

export function* getChildInterests() {
  try {
    const initialResponse = yield call(fetchChildInterests);
    const {body} = initialResponse;
    yield put(setChildInterests(body));
  } catch (error) {
    logger.error('SBP_SAGA_getChildInterests', error);
  }
}

export function* getDepartmentSizes({payload}) {
  try {
    const initialResponse = yield call(fetchDepartmentSizes, payload);
    const {body} = initialResponse;
    yield put(setDepartmentSizes(body));
  } catch (error) {
    logger.error('SBP_SAGA_getDepartmentSizes', error);
  }
}

export function* getSBPProfiles({payload}) {
  try {
    const initialResponse = yield call(fetchSBPProfiles, payload);
    const {body} = initialResponse;
    yield put(setSBPProfiles(body.data));
  } catch (error) {
    if (error.statusCode !== null && error.statusCode.toString()[0] !== '2') {
      logger.error('SBP_SAGA_getSBPProfiles', error);
    }
    yield put(setSBPProfiles([]));
  }
}

export function* deleteSBPProfile({payload}) {
  try {
    yield call(deleteProfile, payload);
    yield put(removeSBPProfile(payload));
  } catch (error) {
    logger.error('SBP_SAGA_deleteSBPProfile', error);
  }
}

export function* createSBPProfile({payload}) {
  try {
    if (payload.data.isEdit || payload.data.isIncomplete) {
      const newPayload = JSON.parse(JSON.stringify(payload));
      yield call(insertChildProfile, payload.data, payload.userId);
      /* eslint-disable no-param-reassign */
      delete payload.data.isEdit;
      const isProfileComplete = newPayload.data.interests && newPayload.data.interests.length >= 1;
      if (payload.data.isIncomplete && !isProfileComplete) {
        yield put(setUpdatedSBPProfile(payload.data));
      } else {
        yield put(setUpdatedSBPProfile(newPayload.data));
      }
    } else {
      yield call(insertChildProfile, payload.data, payload.userId);
      yield put(setCreatedSBPProfile(payload.data));
    }
  } catch (error) {
    logger.error('SBP_SAGA_createSBPProfile', error);
  }
}

function* ShopByProfileSaga() {
  yield takeLatest(SHOP_BY_PROFILE_CONSTANTS.GET_CHILD_INTERESTS, getChildInterests);
  yield takeLatest(SHOP_BY_PROFILE_CONSTANTS.GET_DEPARTMENT_SIZES, getDepartmentSizes);
  yield takeLatest(SHOP_BY_PROFILE_CONSTANTS.GET_SBP_PROFILES, getSBPProfiles);
  yield takeLatest(SHOP_BY_PROFILE_CONSTANTS.DELETE_SBP_PROFILE, deleteSBPProfile);
  yield takeLatest(SHOP_BY_PROFILE_CONSTANTS.CREATE_SBP_PROFILE, createSBPProfile);
  yield takeLatest(SHOP_BY_PROFILE_CONSTANTS.UPDATE_SBP_PROFILE, createSBPProfile);
}

export default ShopByProfileSaga;
