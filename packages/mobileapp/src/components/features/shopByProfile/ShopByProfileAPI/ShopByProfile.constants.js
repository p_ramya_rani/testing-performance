// 9fbef606107a605d69c0edbcd8029e5d 
const GET_CHILD_INTERESTS = 'GET_CHILD_INTERESTS';
const SET_CHILD_INTERESTS = 'SET_CHILD_INTERESTS';
const GET_DEPARTMENT_SIZES = 'GET_DEPARTMENT_SIZES';
const SET_DEPARTMENT_SIZES = 'SET_DEPARTMENT_SIZES';
const GET_SBP_PROFILES = 'GET_SBP_PROFILES';
const SET_SBP_PROFILES = 'SET_SBP_PROFILES';
const DELETE_SBP_PROFILE = 'DELETE_SBP_PROFILE';
const REMOVE_SBP_PROFILE = 'REMOVE_SBP_PROFILE';
const CREATE_SBP_PROFILE = 'CREATE_SBP_PROFILE';
const SET_CREATED_SBP_PROFILE = 'SET_CREATED_SBP_PROFILE';
const UPDATE_SBP_PROFILE = 'UPDATE_SBP_PROFILE';
const SET_UPDATED_SBP_PROFILE = 'SET_UPDATED_SBP_PROFILE ';
const SET_INCOMPLETE_SBP_PROFILE = 'SET_INCOMPLETE_SBP_PROFILE';

const SHOP_BY_PROFILE_CONSTANTS = {
  GET_CHILD_INTERESTS,
  SET_CHILD_INTERESTS,
  GET_DEPARTMENT_SIZES,
  SET_DEPARTMENT_SIZES,
  GET_SBP_PROFILES,
  SET_SBP_PROFILES,
  DELETE_SBP_PROFILE,
  REMOVE_SBP_PROFILE,
  CREATE_SBP_PROFILE,
  SET_CREATED_SBP_PROFILE,
  UPDATE_SBP_PROFILE,
  SET_UPDATED_SBP_PROFILE,
  SET_INCOMPLETE_SBP_PROFILE,
};

export default SHOP_BY_PROFILE_CONSTANTS;
