// 9fbef606107a605d69c0edbcd8029e5d 
import SHOP_BY_PROFILE_CONSTANTS from './ShopByProfile.constants';

export const setChildInterests = payload => {
  return {
    type: SHOP_BY_PROFILE_CONSTANTS.SET_CHILD_INTERESTS,
    payload,
  };
};

export const getChildInterests = payload => {
  return {
    type: SHOP_BY_PROFILE_CONSTANTS.GET_CHILD_INTERESTS,
    payload,
  };
};

export const setDepartmentSizes = payload => {
  return {
    type: SHOP_BY_PROFILE_CONSTANTS.SET_DEPARTMENT_SIZES,
    payload,
  };
};

export const getDepartmentSizes = payload => {
  return {
    type: SHOP_BY_PROFILE_CONSTANTS.GET_DEPARTMENT_SIZES,
    payload,
  };
};

export const getSBPProfilesAction = payload => {
  return {
    type: SHOP_BY_PROFILE_CONSTANTS.GET_SBP_PROFILES,
    payload,
  };
};

export const setSBPProfiles = payload => {
  return {
    type: SHOP_BY_PROFILE_CONSTANTS.SET_SBP_PROFILES,
    payload,
  };
};

export const deleteSBPProfile = payload => {
  return {
    type: SHOP_BY_PROFILE_CONSTANTS.DELETE_SBP_PROFILE,
    payload,
  };
};

export const removeSBPProfile = payload => {
  return {
    type: SHOP_BY_PROFILE_CONSTANTS.REMOVE_SBP_PROFILE,
    payload,
  };
};

export const createSBPProfile = payload => {
  return {
    type: SHOP_BY_PROFILE_CONSTANTS.CREATE_SBP_PROFILE,
    payload,
  };
};

export const setCreatedSBPProfile = payload => {
  return {
    type: SHOP_BY_PROFILE_CONSTANTS.SET_CREATED_SBP_PROFILE,
    payload,
  };
};

export const updateSBPProfile = payload => {
  return {
    type: SHOP_BY_PROFILE_CONSTANTS.UPDATE_SBP_PROFILE,
    payload,
  };
};

export const setUpdatedSBPProfile = payload => {
  return {
    type: SHOP_BY_PROFILE_CONSTANTS.SET_UPDATED_SBP_PROFILE,
    payload,
  };
};
