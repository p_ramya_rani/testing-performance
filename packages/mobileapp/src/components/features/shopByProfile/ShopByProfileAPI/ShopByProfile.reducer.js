// 9fbef606107a605d69c0edbcd8029e5d 
import SHOP_BY_PROFILE_CONSTANTS from './ShopByProfile.constants';

const initialState = {
  childInterests: [],
  departmentSizes: [],
  sbpProfiles: [],
};
const SBPProfileReducer = (state = initialState, action) => {
  switch (action.type) {
    case SHOP_BY_PROFILE_CONSTANTS.SET_CHILD_INTERESTS:
      return {
        ...state,
        childInterests: action.payload,
      };
    case SHOP_BY_PROFILE_CONSTANTS.SET_DEPARTMENT_SIZES:
      return {
        ...state,
        departmentSizes: action.payload.size_mapping,
      };
    case SHOP_BY_PROFILE_CONSTANTS.SET_SBP_PROFILES:
      return {
        ...state,
        sbpProfiles: action.payload,
      };
    case SHOP_BY_PROFILE_CONSTANTS.REMOVE_SBP_PROFILE:
      return {
        ...state,
        sbpProfiles: state.sbpProfiles.filter(profile => profile.id !== action.payload.profile.id),
      };
    case SHOP_BY_PROFILE_CONSTANTS.SET_CREATED_SBP_PROFILE:
      return {
        ...state,
        sbpProfiles: [...state.sbpProfiles, action.payload],
      };
    case SHOP_BY_PROFILE_CONSTANTS.SET_UPDATED_SBP_PROFILE:
      return {
        ...state,
        sbpProfiles: state.sbpProfiles.map(profile => {
          if (profile.id === action.payload.id) {
            return {...profile, ...action.payload};
          }
          return profile;
        }),
      };
    default:
      return state;
  }
};

export default SBPProfileReducer;
