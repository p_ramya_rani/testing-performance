// 9fbef606107a605d69c0edbcd8029e5d
import {getAPIConfig, getValueFromAsyncStorage} from '@tcp/core/src/utils';
import superagent from 'superagent';
import logger from '@tcp/core/src/utils/loggerInstance';
import 'react-native-get-random-values';
import {v4 as uuidv4} from 'uuid';
import {getCustomHeaders, isMobileApp} from '@tcp/core/src/utils/utils.app';
import {capitalizeWords} from '../PLP/helper';
import {SBP_USER_EMAIL} from '../ShopByProfile.constants';
import {md5} from '../../../../../../core/src/components/features/browse/ProductListing/molecules/ProductReviews/encoding';

function getRandomId() {
  return uuidv4();
}

const secureSBPParentId = async parentId => {
  try {
    const userEmail = await getValueFromAsyncStorage(SBP_USER_EMAIL);
    const combinedParentIdUsername = `${userEmail}${parentId}`;
    const lowerCasedCombinedParentIdUsername = combinedParentIdUsername.toLowerCase();
    return md5(lowerCasedCombinedParentIdUsername);
  } catch (error) {
    logger.error('secureSBPParentId', error);
  }
  return '';
};

const headers = isMobileApp() ? getCustomHeaders() : {};

export function fetchChildInterests() {
  const apiConfigObj = getAPIConfig();
  const {sbpInterests} = apiConfigObj;
  return superagent.get(`${sbpInterests}`).set(headers);
}

export function fetchDepartmentSizes(action) {
  const capitalizedDepartment = capitalizeWords(action);
  const apiConfigObj = getAPIConfig();
  const {sbpSizes} = apiConfigObj;
  const URL = `${sbpSizes}?department=${capitalizedDepartment}`;
  return superagent.get(URL).set(headers);
}

export async function fetchSBPProfiles(action) {
  const parentId = action;
  const apiConfigObj = getAPIConfig();
  const {sbpEndPoint} = apiConfigObj;
  const URL = `${sbpEndPoint}/getprofile`;
  try {
    const hashedParentId = await secureSBPParentId(parentId);
    return superagent
      .get(`${URL}?parent_id=${hashedParentId}&${new Date().getTime()}`)
      .set(headers);
  } catch (error) {
    logger.error('fetchSBPProfiles', error);
  }
  return [];
}

export const deleteProfile = async action => {
  try {
    const profId = action.profile.id;
    const {userId} = action;
    if (!userId) return;
    const hashedParentId = await secureSBPParentId(userId);
    const profileId = {
      id: profId,
      parent_id: hashedParentId,
    };
    const apiConfigObj = getAPIConfig();
    const {sbpEndPoint} = apiConfigObj;
    const DELETE_PROFILE_URL = `${sbpEndPoint}/deleteprofile`;
    /* eslint-disable consistent-return */
    return await superagent
      .post(DELETE_PROFILE_URL)
      .set(headers)
      .send(profileId);
  } catch (error) {
    logger.error('deleteProfile', error);
  }
};

async function createProfile(newChildProfile) {
  const parentId = newChildProfile.parent_id;
  const hashedParentId = await secureSBPParentId(parentId);
  const newChildProfileObj = newChildProfile;
  newChildProfileObj.parent_id = hashedParentId;
  const apiConfigObj = getAPIConfig();
  const {sbpEndPoint} = apiConfigObj;
  const CREATE_PROFILE_URL = `${sbpEndPoint}/createprofile`;
  try {
    return superagent
      .post(CREATE_PROFILE_URL)
      .set(headers)
      .send(newChildProfileObj);
  } catch (error) {
    logger.error(error);
  }
  return null;
}

const updateChildProfile = async (profile, isIncomplete, userId) => {
  const profileData = {
    parent_id: profile.parent_id,
    id: profile.id,
    theme: profile.theme,
    size: profile.size,
    relation: profile.relation,
    gender: profile.gender,
    name: profile.name,
    fit: profile.fit,
    month: profile.month,
    shoe: profile.shoe,
    year: profile.year,
    interests: profile.interests,
    styles: profile.styles,
    favorite_colors: profile.favorite_colors,
  };

  if (isIncomplete || !profileData.parent_id) {
    const hashedParentId = await secureSBPParentId(profileData.parent_id || userId);
    profileData.parent_id = hashedParentId;
  }

  const apiConfigObj = getAPIConfig();
  const {sbpEndPoint} = apiConfigObj;
  const UPDATE_PROFILE_URL = `${sbpEndPoint}/updateprofile`;
  try {
    return superagent
      .post(`${UPDATE_PROFILE_URL}`)
      .set(headers)
      .send(profileData);
  } catch (error) {
    logger.error(error);
    return [];
  }
};

export const insertChildProfile = async (profile, userId) => {
  if (profile.isEdit) {
    return updateChildProfile(profile, false, userId);
  }
  const {isIncomplete} = profile;
  const randomProfId = getRandomId();
  const profileData = profile;
  if (profileData.month === undefined) {
    profileData.month = 'August';
  }
  if (profileData.year === undefined) {
    profileData.year = '2015';
  }
  if (isIncomplete) {
    if (!profile.id) {
      profileData.id = randomProfId;
    } else {
      profileData.parent_id = userId;
      return updateChildProfile(profileData, isIncomplete, userId);
    }
  }
  profileData.parent_id = userId;
  profileData.id = randomProfId;
  return createProfile(profileData);
};
