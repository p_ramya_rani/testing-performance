import superagent from 'superagent';
import {
  deleteProfile,
  fetchChildInterests,
  fetchDepartmentSizes,
  fetchSBPProfiles,
  insertChildProfile,
} from '../ShopByProfileAPIHandlers';

describe('ShopByProfile API', () => {
  it('should handle create', async () => {
    const childProfile = {
      name: 'Name',
      parent_id: 1,
      isIncomplete: false,
    };
    const postCall = jest
      .spyOn(superagent, 'post')
      .mockImplementation(() => Promise.resolve({send: () => {}}));
    await insertChildProfile(childProfile, 1);
    expect(postCall).toHaveBeenCalledTimes(1);
  });
  it('should handle create', async () => {
    const childProfile = {
      name: 'Name',
      parent_id: 1,
      isIncomplete: false,
      month: 'August',
      year: 2015,
    };
    const postCall = jest
      .spyOn(superagent, 'post')
      .mockImplementation(() => Promise.resolve({send: () => {}}));
    await insertChildProfile(childProfile, 1);
    expect(postCall).toHaveBeenCalledTimes(2);
  });
  it('should handle update', async () => {
    const childProfile = {
      name: 'Name',
      parent_id: 1,
      isIncomplete: true,
    };
    const postCall = jest
      .spyOn(superagent, 'post')
      .mockImplementation(() => Promise.resolve({send: () => {}}));
    await insertChildProfile(childProfile, 1);
    expect(postCall).toHaveBeenCalledTimes(3);
  });
  it('should handle update', async () => {
    const childProfile = {
      name: 'Name',
      isIncomplete: true,
    };
    const postCall = jest
      .spyOn(superagent, 'post')
      .mockImplementation(() => Promise.resolve({send: () => {}}));
    await insertChildProfile(childProfile, 1);
    expect(postCall).toHaveBeenCalledTimes(4);
  });
  it('should handle fetch', async () => {
    const getCall = jest.spyOn(superagent, 'get').mockImplementation(() => Promise.resolve());
    await fetchSBPProfiles();
    expect(getCall).toHaveBeenCalledTimes(1);
  });
  it('should handle fetchDepartmentSizes', async () => {
    const getCall = jest.spyOn(superagent, 'get').mockImplementation(() => ({set: jest.fn()}));
    await fetchDepartmentSizes('GIRL');
    expect(getCall).toHaveBeenCalledTimes(2);
  });
  it('should handle fetchChildInterests', async () => {
    const getCall = jest.spyOn(superagent, 'get').mockImplementation(() => ({set: jest.fn()}));
    await fetchChildInterests();
    expect(getCall).toHaveBeenCalledTimes(3);
  });
  it('should handle deleteProfile', async () => {
    const deleteCall = jest.spyOn(superagent, 'post').mockImplementation(() => Promise.resolve());
    const childProfile = {
      name: 'Name',
      parent_id: 1,
      isIncomplete: false,
      month: 'August',
      year: 2015,
      id: 1,
    };
    await deleteProfile({profile: childProfile, userId: 1});
    expect(deleteCall).toHaveBeenCalledTimes(5);
  });
  it('should handle deleteProfile', async () => {
    const deleteCall = jest.spyOn(superagent, 'post').mockImplementation(() => Promise.resolve());
    const childProfile = {
      name: 'Name',
      parent_id: 1,
      isIncomplete: false,
      month: 'August',
      year: 2015,
      id: 1,
    };
    await deleteProfile({profile: childProfile});
    expect(deleteCall).toHaveBeenCalledTimes(5);
  });
});
