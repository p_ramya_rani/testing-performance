// 9fbef606107a605d69c0edbcd8029e5d 
import SBPProfileReducer from '../ShopByProfile.reducer';
import SHOP_BY_PROFILE_CONSTANTS from '../ShopByProfile.constants';

const initialState = {
  childInterests: [],
  departmentSizes: [],
  sbpProfiles: [],
};

const twoProfiles = [
  {name: 'Ted', interests: 'Animals,Sports', theme: 2, gender: 'BOY', id: 1234},
  {name: 'Nancy', interests: 'Hearts,Unicorns', theme: 4, gender: 'GIRL', id: 4321},
];

const action1 = {
  type: undefined,
  payload: {
    data: [],
  },
};

const action2 = {
  type: SHOP_BY_PROFILE_CONSTANTS.SET_CHILD_INTERESTS,
  payload: [{interest: 'Animals'}, {interest: 'Butterflies'}, {interest: 'Hearts'}],
};

const action3 = {
  type: SHOP_BY_PROFILE_CONSTANTS.SET_DEPARTMENT_SIZES,
  payload: {
    size_mapping: [{size: '3T'}, {size: '4T'}],
  },
};

const action4 = {
  type: SHOP_BY_PROFILE_CONSTANTS.SET_SBP_PROFILES,
  payload: twoProfiles,
};

const action5 = {
  type: SHOP_BY_PROFILE_CONSTANTS.SET_CREATED_SBP_PROFILE,
  payload: {name: 'Drew', interests: 'Dinos', theme: 7, gender: 'TODDLER BOY', id: 4738},
};

const action6 = {
  type: SHOP_BY_PROFILE_CONSTANTS.REMOVE_SBP_PROFILE,
  payload: {
    profile: {name: 'Ted', interests: 'Animals,Sports', theme: 2, gender: 'BOY', id: 1234},
  },
};

const action7 = {
  type: SHOP_BY_PROFILE_CONSTANTS.SET_UPDATED_SBP_PROFILE,
  payload: twoProfiles,
};

const action8 = {
  type: SHOP_BY_PROFILE_CONSTANTS.SET_UPDATED_SBP_PROFILE,
  payload: {name: 'Drew', interests: 'Dinos,Hearts', theme: 11, gender: 'TODDLER BOY', id: 4738},
};

describe('SBPProfileReducer', () => {
  it('should return initial state', () => {
    expect(SBPProfileReducer(initialState, action1)).toStrictEqual(initialState);
  });

  it('should set child interests', () => {
    expect(SBPProfileReducer(initialState, action2)).toStrictEqual({
      ...initialState,
      childInterests: action2.payload,
    });
  });

  it('should set department sizes', () => {
    expect(SBPProfileReducer(initialState, action3)).toStrictEqual({
      ...initialState,
      departmentSizes: action3.payload.size_mapping,
    });
  });

  it('should set sbp profiles', () => {
    expect(SBPProfileReducer(initialState, action4)).toStrictEqual({
      ...initialState,
      sbpProfiles: action4.payload,
    });
  });

  it('should set sbp profiles', () => {
    expect(SBPProfileReducer(initialState, action4)).toStrictEqual({
      ...initialState,
      sbpProfiles: action7.payload,
    });
  });

  it('should add a new sbp profile', () => {
    expect(SBPProfileReducer(initialState, action5)).toStrictEqual({
      ...initialState,
      sbpProfiles: [...initialState.sbpProfiles, action5.payload],
    });
  });

  it('should remove sbp profile', () => {
    expect(SBPProfileReducer(initialState, action6)).toStrictEqual({
      ...initialState,
      sbpProfiles: initialState.sbpProfiles.filter(
        profile => profile.id !== action6.payload.profile.id,
      ),
    });
  });

  it('should update and set sbp profile', () => {
    expect(SBPProfileReducer(initialState, action8)).toStrictEqual({
      ...initialState,
      sbpProfiles: initialState.sbpProfiles.map(profile => {
        if (profile.id === action8.payload.id) {
          return {...profile, ...action8.payload};
        }
        return profile;
      }),
    });
  });
});
