import {takeLatest} from 'redux-saga/effects';
import {runSaga} from 'redux-saga';
import ShopByProfileSaga, {
  getChildInterests,
  getDepartmentSizes,
  getSBPProfiles,
  deleteSBPProfile,
  createSBPProfile,
} from '../ShopByProfile.saga';
import SHOP_BY_PROFILE_CONSTANTS from '../ShopByProfile.constants';

describe('ShopByProfileSaga', () => {
  describe('SBP', () => {
    it('should return correct takeLatest effect from getChildInterests', () => {
      const generator = ShopByProfileSaga();
      const takeLatestDescriptor = generator.next().value;
      const expected = takeLatest(SHOP_BY_PROFILE_CONSTANTS.GET_CHILD_INTERESTS, getChildInterests);
      expect(takeLatestDescriptor).toEqual(expected);
    });

    it('should return correct takeLatest effect from getDepartmentSizes', () => {
      const generator = ShopByProfileSaga();
      generator.next();
      const takeLatestDescriptor = generator.next().value;
      const expected = takeLatest(
        SHOP_BY_PROFILE_CONSTANTS.GET_DEPARTMENT_SIZES,
        getDepartmentSizes,
      );
      expect(takeLatestDescriptor).toEqual(expected);
    });

    it('should return correct takeLatest effect from getSBPProfiles', () => {
      const generator = ShopByProfileSaga();
      generator.next();
      generator.next();
      const takeLatestDescriptor = generator.next().value;
      const expected = takeLatest(SHOP_BY_PROFILE_CONSTANTS.GET_SBP_PROFILES, getSBPProfiles);
      expect(takeLatestDescriptor).toEqual(expected);
    });

    it('should return correct takeLatest effect from deleteSBPProfile', () => {
      const generator = ShopByProfileSaga();
      generator.next();
      generator.next();
      generator.next();
      const takeLatestDescriptor = generator.next().value;
      const expected = takeLatest(SHOP_BY_PROFILE_CONSTANTS.DELETE_SBP_PROFILE, deleteSBPProfile);
      expect(takeLatestDescriptor).toEqual(expected);
    });

    it('should return correct takeLatest effect from createSBPProfile', () => {
      const generator = ShopByProfileSaga();
      generator.next();
      generator.next();
      generator.next();
      generator.next();
      const takeLatestDescriptor = generator.next().value;
      const expected = takeLatest(SHOP_BY_PROFILE_CONSTANTS.CREATE_SBP_PROFILE, createSBPProfile);
      expect(takeLatestDescriptor).toEqual(expected);
    });

    it('should return correct takeLatest effect from createSBPProfile', () => {
      const generator = ShopByProfileSaga();
      generator.next();
      generator.next();
      generator.next();
      generator.next();
      generator.next();
      const takeLatestDescriptor = generator.next().value;
      const expected = takeLatest(SHOP_BY_PROFILE_CONSTANTS.UPDATE_SBP_PROFILE, createSBPProfile);
      expect(takeLatestDescriptor).toEqual(expected);
    });

    it.skip('should return correct call effect from getChildInterests', async () => {
      const call = jest.fn(); // Remove when `it.skip()` is removed
      const put = jest.fn(); // Remove when `it.skip()` is removed
      const dispatched = [];
      await runSaga(
        {
          dispatch: (action) => dispatched.push(action),
        },
        getChildInterests,
      );
      expect(call).toHaveBeenCalledTimes(1);
      expect(put).toHaveBeenCalledTimes(1);
    });

    it.skip('should return correct call effect from getDepartmentSizes', async () => {
      const call = jest.fn(); // Remove when `it.skip()` is removed
      const put = jest.fn(); // Remove when `it.skip()` is removed
      const dispatched = [];
      await runSaga(
        {
          dispatch: (action) => dispatched.push(action),
        },
        getDepartmentSizes,
      );
      expect(call).toHaveBeenCalledTimes(1);
      expect(put).toHaveBeenCalledTimes(1);
    });

    it.skip('should return correct call effect from profiles', async () => {
      const call = jest.fn(); // Remove when `it.skip()` is removed
      const put = jest.fn(); // Remove when `it.skip()` is removed
      const dispatched = [];
      await runSaga(
        {
          dispatch: (action) => dispatched.push(action),
        },
        getSBPProfiles,
      );
      expect(call).toHaveBeenCalledTimes(1);
      expect(put).toHaveBeenCalledTimes(1);
    });

    it.skip('should return correct call effect from delete', async () => {
      const call = jest.fn(); // Remove when `it.skip()` is removed
      const put = jest.fn(); // Remove when `it.skip()` is removed
      const dispatched = [];
      await runSaga(
        {
          dispatch: (action) => dispatched.push(action),
        },
        deleteSBPProfile,
      );
      expect(call).toHaveBeenCalledTimes(1);
      expect(put).toHaveBeenCalledTimes(1);
    });

    it.skip('should return correct call effect from createSBPProfile', async () => {
      const call = jest.fn(); // Remove when `it.skip()` is removed
      const put = jest.fn(); // Remove when `it.skip()` is removed
      const dispatched = [];
      await runSaga(
        {
          dispatch: (action) => dispatched.push(action),
        },
        createSBPProfile,
      );
      expect(call).toHaveBeenCalledTimes(1);
      expect(put).toHaveBeenCalledTimes(1);
    });
  });
});
