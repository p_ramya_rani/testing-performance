// 9fbef606107a605d69c0edbcd8029e5d 
import {
  setChildInterests,
  getChildInterests,
  setDepartmentSizes,
  getDepartmentSizes,
  getSBPProfilesAction,
  setSBPProfiles,
  deleteSBPProfile,
  removeSBPProfile,
  createSBPProfile,
  setCreatedSBPProfile,
  updateSBPProfile,
  setUpdatedSBPProfile,
} from '../ShopByProfile.actions';
import SHOP_BY_PROFILE_CONSTANTS from '../ShopByProfile.constants';

const payload = {
  fake: 'data',
};

describe('setChildInterests', () => {
  it('should set child interests in redux', () => {
    expect(setChildInterests(payload)).toStrictEqual({
      type: SHOP_BY_PROFILE_CONSTANTS.SET_CHILD_INTERESTS,
      payload,
    });
  });
});

describe('getChildInterests', () => {
  it('should return payload for child interests', () => {
    expect(getChildInterests(payload)).toStrictEqual({
      type: SHOP_BY_PROFILE_CONSTANTS.GET_CHILD_INTERESTS,
      payload,
    });
  });
});

describe('setDepartmentSizes', () => {
  it('should set department sizes in redux', () => {
    expect(setDepartmentSizes(payload)).toStrictEqual({
      type: SHOP_BY_PROFILE_CONSTANTS.SET_DEPARTMENT_SIZES,
      payload,
    });
  });
});

describe('getDepartmentSizes', () => {
  it('should return payload for department sizes', () => {
    expect(getDepartmentSizes(payload)).toStrictEqual({
      type: SHOP_BY_PROFILE_CONSTANTS.GET_DEPARTMENT_SIZES,
      payload,
    });
  });
});

describe('getSBPProfilesAction', () => {
  it('should return sbp profiles in payload', () => {
    expect(getSBPProfilesAction(payload)).toStrictEqual({
      type: SHOP_BY_PROFILE_CONSTANTS.GET_SBP_PROFILES,
      payload,
    });
  });
});

describe('setSBPProfiles', () => {
  it('should set profiles in redux', () => {
    expect(setSBPProfiles(payload)).toStrictEqual({
      type: SHOP_BY_PROFILE_CONSTANTS.SET_SBP_PROFILES,
      payload,
    });
  });
});

describe('deleteSBPProfile', () => {
  it('should remove profile', () => {
    expect(deleteSBPProfile(payload)).toStrictEqual({
      type: SHOP_BY_PROFILE_CONSTANTS.DELETE_SBP_PROFILE,
      payload,
    });
  });
});

describe('removeSBPProfile', () => {
  it('should remove profile in redux', () => {
    expect(removeSBPProfile(payload)).toStrictEqual({
      type: SHOP_BY_PROFILE_CONSTANTS.REMOVE_SBP_PROFILE,
      payload,
    });
  });
});

describe('createSBPProfile', () => {
  it('should create a new profile', () => {
    expect(createSBPProfile(payload)).toStrictEqual({
      type: SHOP_BY_PROFILE_CONSTANTS.CREATE_SBP_PROFILE,
      payload,
    });
  });
});

describe('setCreatedSBPProfile', () => {
  it('should set new profile in redux', () => {
    expect(setCreatedSBPProfile(payload)).toStrictEqual({
      type: SHOP_BY_PROFILE_CONSTANTS.SET_CREATED_SBP_PROFILE,
      payload,
    });
  });
});

describe('updateSBPProfile', () => {
  it('should update existing profile', () => {
    expect(updateSBPProfile(payload)).toStrictEqual({
      type: SHOP_BY_PROFILE_CONSTANTS.UPDATE_SBP_PROFILE,
      payload,
    });
  });
});

describe('setUpdatedSBPProfile', () => {
  it('should set updated profile in redux', () => {
    expect(setUpdatedSBPProfile(payload)).toStrictEqual({
      type: SHOP_BY_PROFILE_CONSTANTS.SET_UPDATED_SBP_PROFILE,
      payload,
    });
  });
});
