// 9fbef606107a605d69c0edbcd8029e5d 
import {connect} from 'react-redux';
import {getSBPLabels, getSBPDepartmentSizes} from '../ShopByProfile.selector';
import {getDepartmentSizes} from '../ShopByProfileAPI/ShopByProfile.actions';
import SizeInput from './size.component';

const mapStateToProps = state => ({
  sbpLabels: getSBPLabels(state),
  departmentSizes: getSBPDepartmentSizes(state),
});

const mapDispatchToProps = dispatch => {
  return {
    getDepartmentSizes: payload => {
      dispatch(getDepartmentSizes(payload));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SizeInput);
