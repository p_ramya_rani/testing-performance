// 9fbef606107a605d69c0edbcd8029e5d 
import 'react-native';
import React from 'react';
import {shallow} from 'enzyme';
import Component from '../size';

describe('Component', () => {
  let component;
  let navigation;
  let onPressSpy;
  beforeAll(() => {
    global.fetch = jest.fn();
  });
  beforeEach(() => {
    onPressSpy = jest.fn();
    navigation = {
      navigate: jest.fn(),
      dispatch: jest.fn(),
      state: {params: {name: 'Test'}},
    };
    const props = {
      navigation,
      setValue: onPressSpy,
      fetch: jest.fn(() => Promise.resolve()),
      sbpLabels: {lbl_sbp_header_age: 'WHere [Name]'},
    };
    component = shallow(<Component {...props} />);
  });

  it('Component should be defined', () => {
    expect(component).toBeDefined();
  });

  it('Component should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
});
