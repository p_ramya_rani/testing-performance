// 9fbef606107a605d69c0edbcd8029e5d 
import 'react-native';
import React from 'react';
import {shallow} from 'enzyme';
import Component from '../interests';

describe('Component', () => {
  let component;
  let navigation;
  let onPressSpy;

  beforeEach(() => {
    onPressSpy = jest.fn();
    navigation = {
      navigate: jest.fn(),
      dispatch: jest.fn(),
      state: {params: {name: 'Test'}},
    };
    const props = {
      navigation,
      screenProps: {
        apiConfig: {
          brandId: 'tcp',
        },
      },
      isLoading: false,
      L2Categories: [],
      setValue: onPressSpy,
      sbpLabels: {lbl_sbp_header_age: 'WHere [Name]'},
    };
    component = shallow(<Component {...props} />);
  });

  it('Component should be defined', () => {
    expect(component).toBeDefined();
  });

  it('Component should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
});
