// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import {shallow} from 'enzyme';
import Component from '../colors.component';

describe('Component', () => {
  let component;
  let navigation;
  let onPressSpy;

  beforeEach(() => {
    onPressSpy = jest.fn();
    navigation = {
      navigate: jest.fn(),
      dispatch: jest.fn(),
      state: {params: {name: 'Colors'}},
    };
    const props = {
      navigation,
      setValue: onPressSpy,
      sbpLabels: {lbl_sbp_header_age: 'Where [Name]'},
    };
    component = shallow(<Component {...props} />);
  });

  it('Component should be defined', () => {
    expect(component).toBeDefined();
  });

  it('Component should render correctly', () => {
    expect(component).toMatchSnapshot();
  });

  it('Color should set correctly', () => {
    const instance = component.instance();
    instance.setColor('blue', onPressSpy);
    instance.renderColors(onPressSpy, 'blue');
    expect(component.state('colors')).toBe('blue');
  });
});
