// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import {shallow} from 'enzyme';
import {Text, ActivityIndicator} from 'react-native';
import Component from '../size.component';

const sizeTest = {
  'Tops & Bodysuits': {
    Regular: {
      sizes: ['UP TO 7LBS', '0-3 M', '3-6 M', '6-9 M', '9-12 M', '12-18 M', '18-24 M'],
    },
  },
  Bottoms: {
    Regular: {
      sizes: ['UP TO 7LBS', '0-3 M', '3-6 M', '6-9 M', '9-12 M', '12-18 M', '18-24 M'],
    },
  },
  Rompers: {
    Regular: {
      sizes: ['0-3 M', '3-6 M', '6-9 M', '9-12 M', '12-18 M', '18-24 M'],
    },
  },
  Pajamas: {
    Regular: {
      sizes: ['0-3 M', '3-6 M', '6-9 M', '9-12 M', '12-18 M', '18-24 M'],
    },
  },
  Shoes: {
    Regular: {
      sizes: ['0-3 M', '3-6 M', '6-12 M'],
    },
  },
};

const props1 = {
  setValue: jest.fn(),
  data: {
    id: '39f0e736-602d-4611-ac4f-83be41c3e17d',
    theme: 6,
    size:
      '{"Tops & Bodysuits":{"size":["9-12 M"]},"Bottoms":{"size":["9-12 M"]},"Rompers":{"size":["12-18 M"]},"Pajamas":{"size":["12-18 M"]},"Shoes":{"size":["3-6 M"]}}',
    relation: 'Parent',
    gender: 'BABY BOY',
    name: 'Lucky',
    fit: 'REGULAR',
    month: 'August',
    shoe: 'TODDLER 1',
    year: '2019',
    interests:
      'Hearts,Humor & Emojis,Gaming,Food,Dinos,Butterflies,Animals,Books & Education,Jackets & Outerwear,Shoes,Tops,Sets,Bodysuits',
    styles: '0',
    favorite_colors: 'pink,blue',
    isEdit: true,
    departmentChange: false,
  },
  goToNextScreen: jest.fn(),
};

describe('Size Screen Component', () => {
  let component;
  let navigation;
  let onPressSpy;
  beforeAll(() => {
    global.fetch = jest.fn();
  });
  beforeEach(() => {
    onPressSpy = jest.fn();
    navigation = {
      navigate: jest.fn(),
      dispatch: jest.fn(),
      state: {
        params: {
          name: 'Test',
          gender: 'BABY GIRL',
          departmentChange: false,
          size:
            '{"Tops & Bodysuits":{"size":["9-12 M"]},"Bottoms":{"size":["9-12 M"]},"Rompers":{"size":["12-18 M"]},"Pajamas":{"size":["12-18 M"]},"Shoes":{"size":["3-6 M"]}}',
        },
      },
    };
    const props = {
      navigation,
      setValue: onPressSpy,
      sbpLabels: {lbl_sbp_header_age: 'WHere [Name]'},
      getDepartmentSizes: jest.fn(),
      departmentSizes: [],
    };
    component = shallow(<Component {...props} />);
  });

  it('Component should be defined', () => {
    expect(component).toBeDefined();
  });

  it('Component should render correctly', () => {
    expect(component).toMatchSnapshot();
  });

  it('Component should match snapshot when loading', () => {
    component.setState({loading: true});
    expect(component).toMatchSnapshot();
  });

  it('Component should match snapshot when error', () => {
    component.setState({error: true});
    expect(component).toMatchSnapshot();
  });

  it('should call processSizeMapping method', () => {
    component.instance().processSizeMapping = jest.fn();
    component.instance().setSizesFromResponse();
    expect(component.instance().processSizeMapping).toBeCalled();
  });

  it('should call setSelectedValue method', () => {
    component.instance().setSelectedValue = jest.fn();
    component.instance().select();
    expect(component.instance().setSelectedValue).toBeCalled();
  });

  it('calls componentDidMount', () => {
    const spy = jest.spyOn(Component.prototype, 'componentDidMount');
    const props = {
      navigation,
      setValue: onPressSpy,
      sbpLabels: {lbl_sbp_header_age: 'WHere [Name]'},
      getDepartmentSizes: jest.fn(),
      departmentSizes: [],
    };
    shallow(<Component {...props} />);
    expect(spy).toHaveBeenCalledTimes(1);
  });

  it('should set selected values', () => {
    const setValueMock = jest.fn();
    const selectedValueTest = {
      'Tops & Bodysuits': {
        size: ['9-12 M', '12-18 M'],
      },
      Bottoms: {
        size: ['9-12 M'],
      },
      Rompers: {
        size: ['12-18 M'],
      },
      Pajamas: {
        size: ['12-18 M'],
      },
      Shoes: {
        size: ['3-6 M'],
      },
    };
    component.setState({size: sizeTest});
    component.instance().setSelectedValue(selectedValueTest, setValueMock);
    expect(setValueMock).toHaveBeenCalledWith('size', JSON.stringify(selectedValueTest));
  });

  it('should set selected values with empty object', () => {
    const setValueMock = jest.fn();
    const selectedValueTest = {
      'Tops & Bodysuits': {
        size: ['9-12 M', '12-18 M'],
      },
      Bottoms: {
        size: ['9-12 M'],
      },
      Rompers: {
        size: ['12-18 M'],
      },
      Pajamas: {
        size: ['12-18 M'],
      },
      Shoes: {
        size: [],
      },
    };
    component.setState({size: sizeTest});
    component.instance().setSelectedValue(selectedValueTest, setValueMock);
    expect(setValueMock).toHaveBeenCalledWith('size', JSON.stringify({}));
  });

  it('should set correct saved sizes in state', () => {
    const spy = jest.spyOn(Component.prototype, 'setState');
    const selectedValues = {};
    component.instance().processSizeMapping(sizeTest, selectedValues);
    expect(spy).not.toHaveBeenCalled();
  });

  it('should render error message', () => {
    component.setState({error: true, loading: false});
    expect(component.instance().renderChildren(props1)).toEqual(<Text>Error</Text>);
  });

  it('should render loading indicator', () => {
    component.setState({loading: true});
    expect(component.instance().renderChildren(props1)).toEqual(
      <ActivityIndicator animating color="#903769" size="large" />,
    );
  });
});
