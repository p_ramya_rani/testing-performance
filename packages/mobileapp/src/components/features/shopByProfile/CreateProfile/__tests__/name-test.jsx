// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import {shallow} from 'enzyme';
import {NameInput} from '../name';
import NameTextInput from '../../../../common/atoms/TextInput/index';
import IconSelector from '../../../../common/atoms/IconSelector/index';

describe('Component', () => {
  let component;
  let navigation;

  beforeEach(() => {
    navigation = {
      navigate: jest.fn(),
      dispatch: jest.fn(),
    };
    const props = {
      navigation,
    };
    component = shallow(<NameInput {...props} />);
  });

  it('Component should be defined', () => {
    expect(component).toBeDefined();
  });

  it('Component should render correctly', () => {
    expect(component).toMatchSnapshot();
  });

  it('should render NameTextInput', () => {
    expect(component.find(NameTextInput)).toBeDefined();
  });

  it('should render IconSelector', () => {
    expect(component.find(IconSelector)).toBeDefined();
  });
});
