// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import {shallow} from 'enzyme';
import Component from '../container.component';
import {routeNames} from '../../ShopByProfile.constants';

const testProfile = {
  theme: 6,
  size:
    '{"Tops & Bodysuits":{"size":["9-12 M"]},"Bottoms":{"size":["9-12 M"]},"Rompers":{"size":["12-18 M"]},"Pajamas":{"size":["12-18 M"]},"Shoes":{"size":["3-6 M"]}}',
  relation: 'Parent',
  gender: 'BABY BOY',
  name: 'Lucky',
  fit: 'REGULAR',
  month: 'August',
  shoe: 'TODDLER 1',
  year: '2019',
  interests: 'Hearts,Humor & Emojis,Gaming,Food,Dinos,Butterflies,Animals',
  styles: '0',
  favorite_colors: 'pink,blue',
  isEdit: true,
  departmentChange: false,
};

describe('Component', () => {
  let component;
  let navigation;
  let onPressSpy;
  const sbpLabels = {
    lbl_sbp_header_age: 'WHere [Name]',
    lbl_sbp_finish_text_create_profile: 'FINISH',
    lbl_sbp_save_text_edit_profile: 'SAVE',
    lbl_sbp_name_cta: 'NEXT',
    lbl_sbp_toast_message_name: 'Please enter a name.',
    lbl_sbp_toast_message_nospecial_char: 'Special characters not allowed in name.',
    lbl_sbp_toast_message_interest: 'Please select at least 1 interest.',
    lbl_sbp_toast_message_size: 'Please select at least 1 size per category.',
  };

  beforeEach(() => {
    onPressSpy = jest.fn();
    navigation = {
      navigate: jest.fn(),
      dispatch: jest.fn(),
      state: {params: {name: 'Test'}},
    };
    const props = {
      navigation,
      setValue: onPressSpy,
      sbpLabels,
      title: jest.fn(),
      subtitle: 'Sub title',
    };
    component = shallow(<Component {...props} />);
  });

  it('Component should be defined', () => {
    expect(component).toBeDefined();
  });

  it('Component should render correctly', () => {
    expect(component).toMatchSnapshot();
  });

  it('Component should render correctly for all routes', () => {
    Object.keys(routeNames).forEach(routeName => {
      const props = {
        navigation: {
          navigate: jest.fn(),
          dispatch: jest.fn(),
          state: {params: {name: 'Test'}, routeName},
        },
        setValue: onPressSpy,
        sbpLabels,
        title: jest.fn(),
        subtitle: 'Sub title',
      };
      component = shallow(<Component {...props} />);
      expect(component).toMatchSnapshot();
    });
  });

  it('Component should render correctly for editing', () => {
    const instance = component.instance();
    instance.data = {isEdit: true};
    expect(component).toMatchSnapshot();
    instance.data = {isEdit: false};
    expect(component).toMatchSnapshot();
  });

  it('Check the route has a error', () => {
    const instance = component.instance();
    let isExists = instance.hasError('NameInput');
    expect(isExists).toBe(true);
    isExists = instance.hasError('AgeInput');
    expect(isExists).toBe(true);
    isExists = instance.hasError('GenderInput');
    expect(isExists).toBe(true);
    isExists = instance.hasError('InterestsInput');
    expect(isExists).toBe(true);
    isExists = instance.hasError('ColorsInput');
    expect(isExists).toBe(true);
    instance.data = {size: '{"Tops":[1]}'};
    isExists = instance.hasError('SizeInput');
    expect(isExists).toBe(true);
  });

  it('calls componentDidMount', () => {
    const spy = jest.spyOn(Component.prototype, 'componentDidMount');
    const props = {
      navigation,
      setValue: onPressSpy,
      sbpLabels,
      title: jest.fn(),
      subtitle: 'Sub title',
    };
    shallow(<Component {...props} />);
    expect(spy).toHaveBeenCalledTimes(1);
  });

  it('shows the correct text on edit', () => {
    const testData = {
      relation: 'Parent',
      gender: 'BABY BOY',
      name: 'Test90',
      interests: 'Hearts,Humor & Emojis,Gaming,Food,Dinos,Butterflies',
      favorite_colors: 'pink,blue',
      isEdit: true,
      departmentChange: false,
    };
    onPressSpy = jest.fn();
    navigation = {
      navigate: jest.fn(),
      dispatch: jest.fn(),
      state: {params: {name: 'Test'}, routeName: 'NameInput'},
    };
    const props1 = {
      navigation,
      setValue: onPressSpy,
      sbpLabels,
      title: jest.fn(),
      subtitle: 'Sub title',
    };
    component = shallow(<Component {...props1} />);
    const expectedResult = 'SAVE';
    expect(component.instance().handleShowText(testData)).toEqual(expectedResult);
  });

  it('should show correct text on profile finish', () => {
    const testData = {
      relation: 'Parent',
      gender: 'BABY BOY',
      name: 'Test90',
      favorite_colors: 'pink,blue',
      isEdit: false,
      departmentChange: false,
    };
    navigation = {
      navigate: jest.fn(),
      dispatch: jest.fn(),
      state: {params: {name: 'Test'}, routeName: 'ColorsInput'},
    };
    const props2 = {
      navigation,
      sbpLabels,
      title: jest.fn(),
      subtitle: 'Sub title',
    };
    component = shallow(<Component {...props2} />);
    const expectedResult = 'FINISH';
    expect(component.instance().handleShowText(testData)).toEqual(expectedResult);
  });

  it('should show correct text on gender screen on edit', () => {
    const testData = {
      relation: 'Parent',
      gender: 'BABY BOY',
      name: 'Test90',
      interests: 'Hearts,Humor & Emojis,Gaming',
      favorite_colors: 'pink,blue',
      isEdit: true,
      departmentChange: false,
    };
    navigation = {
      navigate: jest.fn(),
      dispatch: jest.fn(),
      state: {params: {name: 'Test'}, routeName: 'GenderInput'},
    };
    const props3 = {
      navigation,
      sbpLabels,
      title: jest.fn(),
      subtitle: 'Sub title',
    };
    component = shallow(<Component {...props3} />);
    const expectedResult = 'NEXT';
    expect(component.instance().handleShowText(testData)).toEqual(expectedResult);
  });

  it('it should not edit profile on department screen if selecting same department', () => {
    navigation = {
      navigate: jest.fn(),
      dispatch: jest.fn(),
      state: {params: testProfile, routeName: 'GenderInput'},
    };
    const props3 = {
      navigation,
      sbpLabels,
      title: jest.fn(),
      subtitle: 'Sub title',
    };
    component = shallow(<Component {...props3} />);
    expect(component.instance().goToNextScreen()).toBeUndefined();
  });

  it('it should create profile', () => {
    const modifiedTestProfile = {...testProfile, isEdit: false};
    navigation = {
      navigate: jest.fn(),
      dispatch: jest.fn(),
      state: {params: modifiedTestProfile, routeName: 'ColorsInput'},
    };
    const props4 = {
      navigation,
      sbpLabels,
      title: jest.fn(),
      subtitle: 'Sub title',
    };
    component = shallow(<Component {...props4} />);
    expect(component.instance().goToNextScreen()).toBeUndefined();
  });
});
