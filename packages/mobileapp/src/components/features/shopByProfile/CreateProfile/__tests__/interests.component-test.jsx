// 9fbef606107a605d69c0edbcd8029e5d
import 'react-native';
import React from 'react';
import {shallow} from 'enzyme';
import Component from '../interests.component';
import {MAX_NUMBER_INTERESTS} from '../../ShopByProfile.constants';

const data = {
  Items: [
    {
      unbxd_url: {
        S: '',
      },
      interest_type: {
        S: 'child',
      },
      keywords: {
        S: 'Love, Heart',
      },
      brand: {
        S: 'TCP',
      },
      create_rank: {
        N: '0',
      },
      id: {
        S: '',
      },
      name: {
        S: 'Hearts',
      },
      plp_rank: {
        N: '0',
      },
    },
    {
      unbxd_url: {
        S: '',
      },
      interest_type: {
        S: 'child',
      },
      keywords: {
        S: 'Minecraft, Videogame, Mario, Luigi, Video game, Gamer, Gaming, Level',
      },
      brand: {
        S: 'TCP',
      },
      create_rank: {
        N: '0',
      },
      id: {
        S: '',
      },
      name: {
        S: 'Gaming',
      },
      plp_rank: {
        N: '0',
      },
    },
  ],
};

describe('Component', () => {
  let component;
  let navigation;
  let onPressSpy;
  const labelSbp = {lbl_sbp_header_age: 'WHere [Name]'};
  beforeEach(() => {
    onPressSpy = jest.fn();
    navigation = {
      navigate: jest.fn(),
      dispatch: jest.fn(),
      state: {params: {name: 'Test', gender: 'GIRL'}},
    };
    const props = {
      navigation,
      screenProps: {
        apiConfig: {
          brandId: 'tcp',
        },
      },
      isLoading: false,
      L2Categories: [],
      setValue: onPressSpy,
      sbpLabels: labelSbp,
      getInterests: jest.fn(),
      childInterests: [],
    };
    component = shallow(<Component {...props} />);
  });

  it('Component should be defined', () => {
    expect(component).toBeDefined();
  });

  it('Component should render correctly', () => {
    expect(component).toMatchSnapshot();
  });

  it('should not be selected', () => {
    expect(component.instance().state.selected).toBe(null);
  });

  it('should not have child interests selected', () => {
    expect(component.instance().state.numberOfChildInterestSelected).toBe(0);
  });

  it('should not have parent interests selected', () => {
    expect(component.instance().state.numberOfParentInterestSelected).toBe(0);
  });

  it('should call filterAndSortInterests method', () => {
    const type = 'child';
    const response = [
      {
        unbxd_url: {
          S: '',
        },
        interest_type: {
          S: 'child',
        },
        keywords: {
          S: 'Love, Heart',
        },
        brand: {
          S: 'TCP',
        },
        create_rank: {
          N: '0',
        },
        id: {
          S: '',
        },
        name: {
          S: 'Hearts',
        },
        plp_rank: {
          N: '0',
        },
      },
      {
        unbxd_url: {
          S: '',
        },
        interest_type: {
          S: 'child',
        },
        keywords: {
          S: 'Minecraft, Videogame, Mario, Luigi, Video game, Gamer, Gaming, Level',
        },
        brand: {
          S: 'TCP',
        },
        create_rank: {
          N: '0',
        },
        id: {
          S: '',
        },
        name: {
          S: 'Gaming',
        },
        plp_rank: {
          N: '0',
        },
      },
    ];
    const expectedResult = ['Gaming', 'Hearts'];
    expect(component.instance().filterAndSortInterests(response, type)).toEqual(expectedResult);
  });

  it('should call extractChildInterest when toggle', () => {
    component.instance().extractChildInterest = jest.fn(() => []);
    const option = 'Animals';
    const setValueMock = jest.fn();
    component.instance().toggle(option, setValueMock);
    expect(component.instance().extractChildInterest).toBeCalled();
  });

  it('calls componentDidMount', () => {
    const spy = jest.spyOn(Component.prototype, 'componentDidMount');
    const props = {
      navigation,
      screenProps: {
        apiConfig: {
          brandId: 'tcp',
        },
      },
      isLoading: false,
      L2Categories: [],
      setValue: onPressSpy,
      sbpLabels: labelSbp,
      getInterests: jest.fn(),
    };
    shallow(<Component {...props} />);
    expect(spy).toHaveBeenCalledTimes(1);
  });

  it('extractChildInterest should return list of interests', () => {
    component.setProps({childInterests: data});
    const expectedResult = ['Hearts', 'Gaming'];
    expect(component.instance().extractChildInterest()).toEqual(expectedResult);
  });

  it('should not add more child interests if max number is reached', () => {
    const option = 'Gaming';
    const selectedTest = ['Animals', 'Hearts'];
    const setValueMock = jest.fn();
    component.setState({
      numberOfChildInterestSelected: MAX_NUMBER_INTERESTS,
      selected: selectedTest,
    });
    component.setProps({childInterests: data});
    component.instance().toggle(option, setValueMock);
    expect(setValueMock).not.toHaveBeenCalled();
  });

  it('should not add more parent interests if max number is reached', () => {
    const option = 'Bottoms';
    const selectedTest = ['Animals', 'Hearts', 'Tops'];
    const setValueMock = jest.fn();
    component.setProps({childInterests: data});
    component.setState({
      numberOfParentInterestSelected: MAX_NUMBER_INTERESTS,
      selected: selectedTest,
    });
    component.instance().toggle(option, setValueMock);
    expect(setValueMock).not.toHaveBeenCalled();
  });

  it('expects setValue to have been called when pressing interests', () => {
    const option = 'Animals';
    const setValueMock = jest.fn();
    component.setProps({childInterests: data});
    component.instance().toggle(option, setValueMock);
    expect(setValueMock).toHaveBeenCalled();
  });

  it('renders interests in sorted manner', () => {
    const props = {
      navigation,
      screenProps: {
        apiConfig: {
          brandId: 'tcp',
        },
      },
      isLoading: false,
      L2Categories: [],
      setValue: jest.fn(),
      sbpLabels: labelSbp,
      getInterests: jest.fn(),
      childInterests: data,
    };

    expect(component.instance().renderChildren(props)).not.toBeNull();
  });

  it('should decrease number of parent interests when toggle', () => {
    const option = 'Tops';
    const selectedTest = ['Animals', 'Hearts', 'Tops'];
    const setValueMock = jest.fn();
    component.setProps({childInterests: data});
    component.setState({
      selected: selectedTest,
    });
    component.instance().toggle(option, setValueMock);
    expect(setValueMock).toBeCalledWith('interests', 'Animals,Hearts');
  });

  it('should decrease number of child interests when toggle', () => {
    const option = 'Animals';
    const selectedTest = ['Animals', 'Hearts', 'Tops'];
    const setValueMock = jest.fn();
    component.setProps({childInterests: data});
    component.setState({
      selected: selectedTest,
    });
    component.instance().toggle(option, setValueMock);
    expect(setValueMock).toBeCalledWith('interests', 'Hearts,Tops');
  });

  it('should not render L2 (parent interests)', () => {
    const interest = 'Bottoms';
    const selectedTest = ['Animals', 'Hearts', 'Tops'];
    const setValueMock = jest.fn();
    const content = {categoryContent: {displayToCustomer: false}};
    component.setState({
      selected: selectedTest,
    });
    expect(component.instance().renderParentInterests(interest, setValueMock, content)).toBeNull();
  });
});
