// 9fbef606107a605d69c0edbcd8029e5d 
import 'react-native';
import React from 'react';
import {shallow} from 'enzyme';
import Component from '../age.component';

describe('Component', () => {
  let component;
  let onPressSpy;

  beforeEach(() => {
    onPressSpy = jest.fn();
    const props = {
      navigation: {state: {params: {name: 'Age'}}},
      setValue: onPressSpy,
      sbpLabels: {lbl_sbp_header_age: 'WHere [Name]'},
      data: {lbl_sbp_text_relationship: 'Test', profileMonth: 6, is_pregenerate_profile: true},
    };
    component = shallow(<Component {...props} />);
    component.setState({year: 2016, month: 'August', gender: 'BOYS'});
  });

  it('Component should be defined', () => {
    expect(component).toBeDefined();
  });

  it('Component should render correctly', () => {
    expect(component).toMatchSnapshot();
  });

  it('Month, Year and releation should set properly', () => {
    const instance = component.instance();
    instance.setMonth('January', 0, onPressSpy);
    instance.setYear(2021, 6, onPressSpy);
    instance.setRelation('Test', onPressSpy);
    const props = {
      sbpLabels: {lbl_sbp_header_age: 'Where [Name]'},
      data: {lbl_sbp_text_relationship: 'Test', profileMonth: 6, is_pregenerate_profile: true},
    };
    instance.renderChildren(props);
    expect(component.state('year')).toBe(2021);
    expect(component.state('month')).toBe('January');
  });
});
