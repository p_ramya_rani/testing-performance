// 9fbef606107a605d69c0edbcd8029e5d 
import {StyleSheet} from 'react-native';
import {isAndroid} from '@tcp/core/src/utils/utils.app';
import {
  colorBlack,
  colorWhite,
  colorNero,
  weightFont,
  fontSizeTwelve,
} from '../ShopByProfile.styles';

const sizeInputStyle = StyleSheet.create({
  renderButton: {
    width: '33.33%',
  },
  renderButtonText: {
    fontSize: fontSizeTwelve,
    fontWeight: '600',
  },
  renderButtonView: {
    alignItems: 'center',
    borderColor: colorBlack,
    borderRadius: 6,
    borderWidth: 1,
    flex: 1,
    height: 30,
    justifyContent: 'center',
    marginBottom: 6,
    marginLeft: 3,
    marginTop: 6,
    width: '90%',
  },
  renderCardContainer: {
    backgroundColor: colorWhite,
    borderRadius: 30,
    elevation: 9,
    marginBottom: 35,
    marginHorizontal: isAndroid() ? 6 : 0,
    padding: 20,
    shadowColor: colorBlack,
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.2,
    shadowRadius: 9.46,
  },
  renderCardInternalText: {
    color: colorNero,
    fontSize: 17,
    fontWeight: weightFont,
    left: 5,
    marginBottom: 8,
    marginTop: 8,
  },
  renderCardText: {
    color: colorNero,
    fontSize: 20,
    fontWeight: weightFont,
    left: 5,
    marginBottom: 8,
    textAlign: 'center',
  },
  renderCardView: {
    flexDirection: 'row',
    flex: 1,
    flexWrap: 'wrap',
  },
  renderChildren: {
    marginBottom: -60,
  },
});

export default sizeInputStyle;
