// 9fbef606107a605d69c0edbcd8029e5d
import React, {Component, Fragment} from 'react';
import {View, Text} from 'react-native';
import {PropTypes} from 'prop-types';
import {connect} from 'react-redux';
import CreateProfileContainer from './container';
import nameInputStyle from './name.styles';
import NameTextInput from '../../../common/atoms/TextInput';
import IconSelector from '../../../common/atoms/IconSelector';
import {showSBPLabels} from '../PLP/helper';
import {getSBPLabels} from '../ShopByProfile.selector';

class NameInput extends Component {
  constructor(props) {
    super(props);
    this.renderChildren = this.renderChildren.bind(this);
  }

  renderChildren = props => {
    const {sbpLabels} = props;
    const selectedTheme = props.data.theme;
    const nameFieldLabel = showSBPLabels(sbpLabels, 'lbl_sbp_name_field', 'Name');
    const chooseIconCMSLabel = showSBPLabels(sbpLabels, 'lbl_sbp_icon_text', 'Pick an icon');
    return (
      <Fragment>
        <Text style={{...nameInputStyle.nameText}}>{nameFieldLabel}</Text>
        <View style={{...nameInputStyle.nameTextInput}}>
          <NameTextInput setValue={props.setValue} data={props.data} sbpLabels={sbpLabels} />
        </View>
        <Text style={{...nameInputStyle.chooseIconText}}>{chooseIconCMSLabel}</Text>
        <View style={{...nameInputStyle.textInputIcon}}>
          <IconSelector setValue={props.setValue} selectedTheme={selectedTheme} />
        </View>
      </Fragment>
    );
  };

  render() {
    const RenderChildren = this.renderChildren.bind(this);
    const {sbpLabels, navigation} = this.props;
    const headerCMSLabel = showSBPLabels(
      sbpLabels,
      'lbl_sbp_header_name',
      'Who are you shopping for?',
    );

    return (
      <CreateProfileContainer title={() => headerCMSLabel} navigation={navigation}>
        <RenderChildren sbpLabels={sbpLabels} />
      </CreateProfileContainer>
    );
  }
}

NameInput.propTypes = {
  sbpLabels: PropTypes.shape({}).isRequired,
  navigation: PropTypes.shape({}).isRequired,
};

const mapStateToProps = state => ({
  sbpLabels: getSBPLabels(state),
});

export {NameInput};
export default connect(mapStateToProps)(NameInput);
