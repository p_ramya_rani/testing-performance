// 9fbef606107a605d69c0edbcd8029e5d 
import {connect} from 'react-redux';
import {createNewWishListAction} from '@tcp/core/src/components/features/browse/Favorites/container/Favorites.actions';
import {getUserId} from '@tcp/core/src/components/features/account/User/container/User.selectors';
import CreateProfileContainer from './container.component';
import {getSBPLabels} from '../ShopByProfile.selector';
import {createSBPProfile, updateSBPProfile} from '../ShopByProfileAPI/ShopByProfile.actions';

const mapStateToProps = state => {
  return {
    userId: getUserId(state),
    sbpLabels: getSBPLabels(state),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addChildProfile: payload => {
      dispatch(createSBPProfile(payload));
    },
    updateProfile: payload => {
      dispatch(updateSBPProfile(payload));
    },
    createNewWishList: payload => dispatch(createNewWishListAction(payload)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CreateProfileContainer);
