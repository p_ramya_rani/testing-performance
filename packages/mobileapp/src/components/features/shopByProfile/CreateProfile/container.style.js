// 9fbef606107a605d69c0edbcd8029e5d 
import {StyleSheet, Dimensions} from 'react-native';
import {isAndroid} from '@tcp/core/src/utils/utils.app';
import {
  colorWhite,
  colorMortar,
  colorNero,
  colorWhiteSmoke,
  fontFamilyNunitoRegular,
  fontFamilyNunitoSemiBold,
  fontFamilyNunitoExtraBold,
  fontFamilyTofinoBold,
  fontSizeFourteen,
  fontSizeTwelve,
} from '../ShopByProfile.styles';

const transparent = 'transparent';
const {height} = Dimensions.get('window');

const createProfileContainerStyle = StyleSheet.create({
  groupImg: {
    height: 75,
    width: 75,
  },
  imageOne: {
    bottom: -200,
    height: '100%',
    position: 'absolute',
    resizeMode: 'cover',
    width: '100%',
  },
  imageTwo: {
    height: '90%',
    left: 200,
    position: 'absolute',
    resizeMode: 'contain',
    top: -400,
    width: '90%',
  },
  linearGradient: {
    alignItems: 'flex-end',
    backgroundColor: transparent,
    flexDirection: 'row',
    height: 105,
    justifyContent: 'flex-end',
    padding: 20,
    position: 'absolute',
    top: height - 105,
    width: '100%',
  },
  nextButton: {
    alignItems: 'center',
    backgroundColor: colorNero,
    borderRadius: 70,
    height: 70,
    justifyContent: 'center',
    marginRight: 10,
    width: 70,
  },
  profileIcon: {
    height: 220,
    width: '100%',
  },
  profileIconView: {
    transform: [{translateX: -25}, {translateY: 125}],
    width: '50%',
  },
  profileIconViewEmpty: {
    height: 100,
    width: '100%',
    zIndex: -20,
  },
  renderContainer: {
    height: isAndroid() ? 25 : 50,
  },
  saveButtonContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginBottom: -5,
    width: '100%',
  },
  saveButtonText: {
    color: colorNero,
    fontFamily: fontFamilyNunitoExtraBold,
    fontSize: 18,
    marginRight: '3%',
  },
  scrollViewCont: {
    paddingTop: 0,
  },
  textFive: {
    bottom: 21,
    color: colorNero,
    fontFamily: fontFamilyNunitoExtraBold,
    fontSize: 18,
    fontWeight: 'bold',
    marginRight: 14,
  },
  textFour: {
    color: colorNero,
    fontFamily: fontFamilyNunitoSemiBold,
    fontSize: fontSizeTwelve,
    fontWeight: '600',
    paddingHorizontal: 4,
    paddingVertical: 24,
  },
  textOne: {
    color: colorNero,
    fontFamily: fontFamilyNunitoSemiBold,
    fontSize: fontSizeTwelve,
    fontWeight: '600',
  },
  textThree: {
    color: colorNero,
    fontFamily: fontFamilyNunitoSemiBold,
    fontSize: fontSizeFourteen,
    fontWeight: '600',
    marginBottom: 25,
    marginTop: 15,
  },
  textTwo: {
    color: colorNero,
    fontFamily: fontFamilyTofinoBold,
    fontSize: 34,
    fontWeight: 'bold',
    lineHeight: 45,
    marginTop: 25,
  },
  toastMessage: {
    color: colorWhite,
    fontFamily: fontFamilyNunitoRegular,
    fontSize: 16,
  },
  toastMessageBg: {
    backgroundColor: colorNero,
  },
  viewEmpty: {
    height: 250,
  },
  viewFour: {
    height: 20,
    position: 'absolute',
    top: '12%',
    width: '100%',
    zIndex: isAndroid() ? 0 : 100,
  },
  viewFourGrad: {
    height: '100%',
    width: '100%',
  },
  viewOne: {
    alignSelf: 'center',
    borderRadius: 2,
    flexDirection: 'row',
    height: 4,
    marginTop: isAndroid() ? 0 : 50,
    paddingHorizontal: 24,
    width: '100%',
  },

  viewThree: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 24,
  },
  viewThreeButton: {
    height: 30,
    marginTop: 23,
    width: 30,
  },
  viewThreeButton2: {
    marginTop: 20,
  },
  viewThreeButtonImg1: {
    height: 15,
    width: 15,
  },
  viewThreeButtonImg2: {
    height: 18,
    width: 10,
  },
  viewThreeEditButton: {
    height: 30,
    marginTop: 0,
    width: 30,
  },
  viewTwo: {
    backgroundColor: colorWhiteSmoke,
    borderColor: colorMortar,
    borderRadius: 7,
    flex: 1,
    marginRight: 4,
  },
});

export default createProfileContainerStyle;
