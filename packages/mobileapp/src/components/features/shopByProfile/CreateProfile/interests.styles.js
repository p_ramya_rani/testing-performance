// 9fbef606107a605d69c0edbcd8029e5d 
import {StyleSheet} from 'react-native';
import {
  colorWhite,
  colorMortar,
  colorNero,
  colorNobel,
  fontFamilyNunitoSemiBold,
  fontFamilyNunitoExtraBold,
  fontFamilyNunitoBold,
  fontSizeFourteen,
  fontSizeTwelve,
} from '../ShopByProfile.styles';

const interestsInputStyle = StyleSheet.create({
  childCategories: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  childInterestImg: {
    height: 42,
    width: 42,
  },
  childInterestsButton: {
    marginBottom: '15%',
  },
  loadingIcon: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  renderChildInterestsIconContainer: {
    alignItems: 'center',
    borderColor: colorNobel,
    borderRadius: 25,
    height: 48,
    justifyContent: 'center',
    width: 88,
  },
  renderChildInterestsIconText: {
    color: colorNero,
    fontFamily: fontFamilyNunitoBold,
    fontSize: fontSizeTwelve,
    lineHeight: 14,
    marginHorizontal: 2,
  },
  renderChildInterestsIconTextContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    marginTop: '7%',
    width: 90,
  },
  renderChildrenView: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    marginHorizontal: '4%',
  },
  renderInterestContainer: {
    alignItems: 'center',
    borderColor: colorMortar,
    borderRadius: 30,
    borderWidth: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    margin: 8,
    padding: 9,
  },
  renderInterestContainerText: {
    fontFamily: fontFamilyNunitoBold,
    fontSize: fontSizeTwelve,
    fontWeight: '800',
    marginHorizontal: 8,
  },
  renderInterestContainerView: {
    alignItems: 'center',
    backgroundColor: colorWhite,
    borderRadius: 20,
    borderWidth: 1,
    height: 25,
    justifyContent: 'center',
    padding: 4,
    width: 25,
  },
  subCategoriesKey: {
    color: colorNero,
    fontFamily: fontFamilyNunitoSemiBold,
    fontSize: fontSizeFourteen,
    marginVertical: '5%',
  },
  subHeaderText: {
    color: colorNero,
    fontFamily: fontFamilyNunitoExtraBold,
    fontSize: fontSizeFourteen,
    lineHeight: 20,
    marginBottom: -5,
    marginTop: 23,
    textAlign: 'left',
  },
});

export default interestsInputStyle;
