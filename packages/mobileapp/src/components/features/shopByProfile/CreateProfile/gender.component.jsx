// 9fbef606107a605d69c0edbcd8029e5d 
import React, {PureComponent} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {PropTypes} from 'prop-types';
import names from '@tcp/core/src/constants/eventsName.constants';
import ClickTracker from '../../../common/atoms/ClickTracker';
import CreateProfileContainer from './container';
import genderInputStyle from './gender.styles';
import {DEPARTMENTS} from '../ShopByProfile.constants';
import {showSBPLabels} from '../PLP/helper';
import {colorPrussian, colorWhite, colorMortar} from '../ShopByProfile.styles';

const GENDER = [
  {name: DEPARTMENTS.GIRL, range: ['4', '16']},
  {name: DEPARTMENTS.BOY, range: ['4', '18']},
  {name: DEPARTMENTS.TODDLER_GIRL, range: ['6M', '5T']},
  {name: DEPARTMENTS.TODDLER_BOY, range: ['6M', '5T']},
  {name: DEPARTMENTS.BABY_GIRL, range: ['0', '18M']},
  {name: DEPARTMENTS.BABY_BOY, range: ['0', '18M']},
];

class GenderInput extends PureComponent {
  state = {
    gender: null,
  };

  setGender(gender, setValue, goToNextScreen) {
    setValue('gender', gender);
    goToNextScreen();
    this.setState(prevState => Object.assign({}, prevState, {gender}));
  }

  renderChildren(props) {
    return <View>{this.renderGender(props)}</View>;
  }

  renderGender(props) {
    const obj = [];
    for (let i = 0; i < GENDER.length; i += 2) {
      obj.push(
        <View style={genderInputStyle.renderGenderContainer}>
          {this.renderButton(
            GENDER[i].name,
            props.setValue,
            props.data.gender,
            GENDER[i].range,
            props.goToNextScreen,
          )}
          {GENDER[i + 1]
            ? this.renderButton(
                GENDER[i + 1].name,
                props.setValue,
                props.data.gender,
                GENDER[i + 1].range,
                props.goToNextScreen,
              )
            : null}
        </View>,
      );
    }
    return obj;
  }

  renderButton(text, setValue, defaultValue, sizeRange, goToNextScreen) {
    const {gender} = this.state;
    const isSelected = text === (gender || defaultValue);
    const {sbpLabels} = this.props;
    const sizesLabel = showSBPLabels(sbpLabels, 'lbl_sbp_department_screen', 'SIZES');
    const bgColor = {
      backgroundColor: isSelected ? colorPrussian : colorWhite,
    };
    const textColor = {
      color: isSelected ? colorWhite : colorMortar,
    };
    return (
      <ClickTracker
        as={TouchableOpacity}
        name={names.screenNames.sbp_gender}
        module="account"
        clickData={{
          sbp_gender: text,
        }}
        accessibilityRole="button"
        onPress={() => this.setGender(text, setValue, goToNextScreen)}
        style={[
          {
            ...genderInputStyle.renderButtonContainer,
          },
          bgColor,
        ]}>
        <Text style={[{...genderInputStyle.renderButtonText}, textColor]}>{text}</Text>

        <Text
          style={[
            {
              ...genderInputStyle.renderButtonTextRange,
            },
            textColor,
          ]}>
          {`${sizesLabel} ${sizeRange[0]}-${sizeRange[1]}`}
        </Text>
      </ClickTracker>
    );
  }

  render() {
    const RenderChildren = this.renderChildren.bind(this);
    const {sbpLabels, navigation} = this.props;
    const selectDepartmentLabel = showSBPLabels(
      sbpLabels,
      'lbl_sbp_dept_header',
      'Select a department',
    );
    return (
      <CreateProfileContainer title={() => selectDepartmentLabel} navigation={navigation}>
        <RenderChildren />
      </CreateProfileContainer>
    );
  }
}

GenderInput.propTypes = {
  sbpLabels: PropTypes.shape({}).isRequired,
  navigation: PropTypes.shape({}).isRequired,
};

export default GenderInput;
