// 9fbef606107a605d69c0edbcd8029e5d 
import {connect} from 'react-redux';
import {getSBPLabels, getSBPInterests} from '../ShopByProfile.selector';
import {getChildInterests} from '../ShopByProfileAPI/ShopByProfile.actions';
import InterestsInput from './interests.component';

const mapStateToProps = state => {
  return {
    L2Categories: state.Navigation && state.Navigation.navigationData,
    sbpLabels: getSBPLabels(state),
    childInterests: getSBPInterests(state),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getInterests: () => {
      dispatch(getChildInterests());
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(InterestsInput);
