// 9fbef606107a605d69c0edbcd8029e5d
import React, {PureComponent} from 'react';
import {View, Text, TouchableOpacity, Picker} from 'react-native';
import {shape, string} from 'prop-types';
import CreateProfileContainer from './container';
import {Months, Years, RELATIONS} from '../ShopByProfile.constants';
import {colorWhite, colorMortar, colorPrussian} from '../ShopByProfile.styles';
import ageInputStyle from './age.styles';
import {showSBPLabels} from '../PLP/helper';

class AgeInput extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      month: 'August',
      year: '2015',
    };
  }

  setRelation(relation, setValue) {
    setValue('relation', relation);
    this.setState((prevState) => Object.assign({}, prevState, {relation}));
  }

  setMonth(itemValue, itemIndex, setValue) {
    setValue('month', itemValue);
    this.setState((prevState) => Object.assign({}, prevState, {month: itemValue}));
  }

  setYear(itemValue, itemIndex, setValue) {
    setValue('year', itemValue);
    this.setState((prevState) => Object.assign({}, prevState, {year: itemValue}));
  }

  renderChildren(props) {
    const {sbpLabels, data} = props;
    const {month, year} = this.state;
    let newMonth;
    const {is_pregenerate_profile: isPregenProfile, month: profileMonth} = data;
    if (isPregenProfile) {
      newMonth = Months[profileMonth - 1];
    } else {
      newMonth = profileMonth;
    }
    const relationCMSLabel = showSBPLabels(
      sbpLabels,
      'lbl_sbp_text_relationship',
      'How are you related?',
    );
    return (
      <View style={ageInputStyle.renderChildrenView}>
        <View style={ageInputStyle.renderChildrenContainer}>
          <Picker
            style={ageInputStyle.renderChildrenPicker}
            selectedValue={newMonth || month}
            onValueChange={(itemValue, itemIndex) =>
              this.setMonth(itemValue, itemIndex, props.setValue)
            }>
            {Months.map((m) => (
              <Picker.Item label={m} value={m} />
            ))}
          </Picker>
          <Picker
            style={ageInputStyle.renderChildrenPicker}
            selectedValue={props.data.year || year}
            onValueChange={(itemValue, itemIndex) =>
              this.setYear(itemValue, itemIndex, props.setValue)
            }>
            {Years.map((y) => (
              <Picker.Item label={y} value={y} />
            ))}
          </Picker>
        </View>
        <Text style={{...ageInputStyle.childRelationshipText}}>{relationCMSLabel}</Text>
        <View style={{...ageInputStyle.childRelationshipView}}>
          {RELATIONS.map((rel) => this.renderButton(rel, props.setValue, props.data.relation))}
        </View>
      </View>
    );
  }

  renderButton(text, setValue, defaultValue) {
    const {gender} = this.state;

    const isSelected = text === (gender || defaultValue);

    const bgColor = {
      backgroundColor: isSelected ? colorPrussian : colorWhite,
    };
    const textColor = {
      color: isSelected ? colorWhite : colorMortar,
    };
    return (
      <TouchableOpacity
        accessibilityRole="button"
        onPress={() => this.setRelation(text, setValue)}
        style={[
          {
            ...ageInputStyle.renderButtonContainer,
          },
          bgColor,
        ]}>
        <Text style={[{...ageInputStyle.renderButtonText}, textColor]}>{text.toUpperCase()}</Text>
      </TouchableOpacity>
    );
  }

  render() {
    const {sbpLabels, navigation} = this.props;
    const {name} = navigation.state.params;
    const RenderChildren = this.renderChildren.bind(this);
    const headerCMSLabel =
      sbpLabels && sbpLabels.lbl_sbp_header_age
        ? `${sbpLabels.lbl_sbp_header_age.replace('[Name]', `${name}'s`)}`
        : `When is ${name}'s birthday?`;
    return (
      <CreateProfileContainer title={() => headerCMSLabel} navigation={navigation}>
        <RenderChildren sbpLabels={sbpLabels} />
      </CreateProfileContainer>
    );
  }
}

AgeInput.propTypes = {
  navigation: shape({
    state: shape({
      params: shape({
        name: string,
      }),
    }),
  }).isRequired,
  sbpLabels: shape({
    lbl_sbp_header_age: string,
  }).isRequired,
};

export default AgeInput;
