// 9fbef606107a605d69c0edbcd8029e5d
import React, {PureComponent} from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import {shape, string} from 'prop-types';
import LinearGradient from 'react-native-linear-gradient';
import CreateProfileContainer from './container';
import {COLORS} from '../ShopByProfile.constants';
import colorInputStyle from './colors.styles';
import {showSBPLabels} from '../PLP/helper';
import {colorBlack, colorGainsboro} from '../ShopByProfile.styles';

const closeBlack = require('../../../../assets/images/shopByProfile/elements/icons-mobile-close_2020-10-08-black/icons-mobile-close.png');
const closeWhite = require('../../../../assets/images/shopByProfile/elements/icons-mobile-close_2020-10-08-white/icons-mobile-close.png');

class ColorInput extends PureComponent {
  static capitalizeColorName(color) {
    return color[0].toUpperCase() + color.slice(1);
  }

  constructor(props) {
    super(props);
    const {navigation} = this.props;
    this.state = {
      colors:
        navigation &&
        navigation.state &&
        navigation.state.params &&
        navigation.state.params.favorite_colors,
    };
  }

  setColor(color, setValue) {
    const {colors: colors_} = this.state;
    const colors = colors_ ? colors_.split(',') : [];
    if (colors.indexOf(color) > -1) {
      colors.splice(colors.indexOf(color), 1);
    } else {
      colors.push(color);
    }
    this.setState(
      (prevState) => Object.assign({}, prevState, {colors: colors.join(',')}),
      () => setValue('favorite_colors', colors.toString()),
    );
  }

  renderColors(setValue, defaultValue) {
    return COLORS.map((colorObj) => this.renderColor(colorObj, setValue, defaultValue));
  }

  renderColor(colorObj, setValue) {
    const {colorName, hexValue} = colorObj;
    const {colors} = this.state;
    const isSelected = colors ? colors.indexOf(colorName) > -1 : null;
    if (colorName === 'metallic' || colorName === 'multi') {
      return this.renderMultiColor(colorName, setValue, isSelected);
    }
    const colorContainerStyle = {
      backgroundColor: hexValue,
      borderWidth: isSelected ? 1 : 2,
      borderColor: isSelected ? colorBlack : colorGainsboro,
    };
    return (
      <View>
        <TouchableOpacity
          accessibilityRole="button"
          onPress={() => this.setColor(colorName, setValue)}
          style={[
            {
              ...colorInputStyle.colorContainer,
            },
            colorContainerStyle,
          ]}>
          {isSelected ? (
            <View
              style={{
                ...colorInputStyle.renderColorView,
              }}>
              {colorName === 'white' || colorName === 'yellow' ? (
                <Image source={closeBlack} style={colorInputStyle.closeImg} />
              ) : (
                <Image source={closeWhite} style={colorInputStyle.closeImg} />
              )}
            </View>
          ) : null}
        </TouchableOpacity>
        <Text style={colorInputStyle.colorName}>{ColorInput.capitalizeColorName(colorName)}</Text>
      </View>
    );
  }

  renderMultiColor(color, setValue, isSelected) {
    let gradientColor;
    let isMetallic;
    if (color === 'metallic') {
      gradientColor = ['#c4b194', '#dcd2c2', '#c6b59b'];
      isMetallic = true;
    }
    if (color === 'multi') gradientColor = ['#f13faf', '#ffc000', '#b1ee6f', '#00a5ff', '#a100ff'];
    const colorContainerStyle = {
      borderWidth: isSelected ? 1 : 2,
      borderColor: isSelected ? colorBlack : colorGainsboro,
    };
    return (
      <TouchableOpacity accessibilityRole="button" onPress={() => this.setColor(color, setValue)}>
        <LinearGradient
          colors={gradientColor}
          style={[
            {
              ...colorInputStyle.colorContainer,
            },
            colorContainerStyle,
          ]}
          start={{x: 0, y: 0}}
          end={isMetallic ? {x: 1, y: 0} : {x: 0, y: 1}}>
          {isSelected ? (
            <View
              style={{
                ...colorInputStyle.renderColorView,
              }}>
              <Image source={closeBlack} style={colorInputStyle.closeImg} />
            </View>
          ) : null}
        </LinearGradient>
        <Text style={colorInputStyle.colorText}>{ColorInput.capitalizeColorName(color)}</Text>
      </TouchableOpacity>
    );
  }

  renderChildren(props) {
    return (
      <View style={{...colorInputStyle.renderChildrenView}}>
        {this.renderColors(props.setValue, props.data.favorite_colors)}
      </View>
    );
  }

  render() {
    const {navigation, sbpLabels} = this.props;
    const RenderChildren = this.renderChildren.bind(this);

    const headerCMSLabel = showSBPLabels(
      sbpLabels,
      'lbl_sbp_header_color',
      'What are their favorite colors?',
    );

    return (
      <CreateProfileContainer title={() => headerCMSLabel} navigation={navigation}>
        <RenderChildren />
      </CreateProfileContainer>
    );
  }
}

ColorInput.propTypes = {
  navigation: shape({
    state: shape({
      params: shape({
        favorite_colors: string,
      }),
    }),
  }).isRequired,
  sbpLabels: shape({}).isRequired,
};

export default ColorInput;
