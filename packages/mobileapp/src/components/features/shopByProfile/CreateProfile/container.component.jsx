/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React, {Component, Fragment} from 'react';
import {View, Dimensions, Text, TouchableOpacity, Image, ScrollView} from 'react-native';
import {NavigationActions} from 'react-navigation';
import {bool, func, number, shape, string} from 'prop-types';
import isEqual from 'lodash/isEqual';
import LinearGradient from 'react-native-linear-gradient';
import {navigateToNestedRoute} from '@tcp/core/src/utils';
import Toast from 'react-native-easy-toast';
import names from '@tcp/core/src/constants/eventsName.constants';
import {setValueInAsyncStorage} from '@tcp/core/src/utils/utils.app';
import {
  padding,
  INITIAL_DATA,
  sequence,
  routeNames,
  SBP_TIME_DELAY_CREATE_TO_PLP,
  SBP_PROFILE_CREATED,
  SBP_CURRENT_PROFILE_ID,
} from '../ShopByProfile.constants';
import ROUTE_NAMES from '../../../../reduxStore/routes/index';
import {colorWhite} from '../ShopByProfile.styles';
import createProfileContainerStyle from './container.style';
import Themes from '../ShopByProfile.theme';
import SaveButton from '../../../common/atoms/Button/SaveButton';
import {setUnbxdProdsNextRefreshTime} from '../Caching';
import ClickTracker from '../../../common/atoms/ClickTracker';
import {showSBPLabels} from '../PLP/helper';

const {height} = Dimensions.get('window');

const doesNotExist = (...args) => args.some((arg) => arg === undefined || arg.length === 0);

const doesSizeNotExist = (size_) => {
  if (!size_) return true;
  try {
    const size = JSON.parse(size_);
    let isSizeExists = false;
    Object.values(size).forEach((value) => {
      const sizes = value.size;
      if (typeof sizes === 'object' && sizes.length > 0) isSizeExists = true;
    });
    return !isSizeExists;
  } catch (e) {
    return true;
  }
};

export function hexToRgbNew(hex) {
  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return `${parseInt(result[1], 16)},${parseInt(result[2], 16)},${parseInt(result[3], 16)}`;
}

const toastNotificationDuration = 1500;
const closeImgSrc = require('../../../../assets/images/shopByProfile/elements/close3x.png');
const caretsImgSrc = require('../../../../assets/images/shopByProfile/elements/icons-elements-carets-medium-medium-left_2020-10-15/icons-elements-carets-medium-medium-left.png');
const groupImgSrc = require('../../../../assets/images/shopByProfile/elements/group-2-3x.png');

class CreateProfileContainer extends Component {
  constructor(props) {
    super(props);
    const {navigation} = this.props;
    this.state = {
      scrollEnabled: false,
      disabled: true,
      saveButtonDisabled: true,
      completedProfile: false,
      isLoading: false,
    };
    this.data = navigation.state.params || INITIAL_DATA;
    this.oldData = this.data;
    this.handleFinishLater = this.handleFinishLater.bind(this);
    this.goToPrevScreen = this.goToPrevScreen.bind(this);
  }

  componentDidMount() {
    const {navigation} = this.props;
    const {routeName} = navigation.state;
    const departmentChange =
      navigation.state && navigation.state.params && navigation.state.params.departmentChange;
    const shouldBeDisabled = departmentChange || this.hasError(routeName);

    this.setState({
      disabled: this.hasError(routeName),
      saveButtonDisabled: shouldBeDisabled,
    });
  }

  setValue(label, input) {
    const {navigation} = this.props;
    this.data = Object.assign({}, this.data, {[label]: input});
    const {routeName} = navigation.state;
    this.setState((prevState) => ({
      ...prevState,
      disabled: this.hasError(routeName),
      saveButtonDisabled: false,
    }));
  }

  handleShowText = (data) => {
    const {navigation, sbpLabels} = this.props;
    const finishLabel = showSBPLabels(sbpLabels, 'lbl_sbp_finish_text_create_profile', 'FINISH');
    const saveLabel = showSBPLabels(sbpLabels, 'lbl_sbp_save_text_edit_profile', 'SAVE');
    const {routeName} = navigation.state;
    const nextLabel = showSBPLabels(sbpLabels, 'lbl_sbp_name_cta', 'NEXT');
    if (!data.isEdit && routeName === routeNames.colors) {
      return finishLabel;
    }
    if (data.isEdit && routeName === routeNames.gender) {
      return nextLabel;
    }
    if (data.isEdit) {
      return saveLabel;
    }
    return nextLabel;
  };

  updateProfileOnEdit = (params) => {
    const {userId, navigation, updateProfile} = this.props;
    const {sbpLabels} = this.props;
    const textToastNotificationCMSLabel =
      sbpLabels && sbpLabels.lbl_sbo_save_toast_text
        ? sbpLabels.lbl_sbo_save_toast_text.replace('[Name]', `${params.name}'s`)
        : `Updating ${params.name}'s profile!`;
    this.setState({isLoading: true});
    this.toast.show(textToastNotificationCMSLabel, toastNotificationDuration, () => {
      const payload = {
        data: this.data,
        userId,
      };
      updateProfile(payload);
      const {name, theme, year, month, relation} = this.oldData;
      const newData = this.data;
      newData.name = name;
      newData.theme = theme;
      newData.month = month;
      newData.year = year;
      newData.relation = relation;
      if (isEqual(this.oldData, newData) === false) {
        setValueInAsyncStorage('plpRefresh', 'true');
        let profileId = payload.data.id;
        setUnbxdProdsNextRefreshTime(0, profileId);
        if (payload.data.gender) {
          profileId = `GEN_${payload.data.gender.replace(/ /g, '_')}`;
          setUnbxdProdsNextRefreshTime(0, profileId);
        }
      }
      navigateToNestedRoute(
        navigation,
        'SbpPlpStack',
        'EditChildProfile',
        Object.assign({}, payload.data, {
          backTo: {
            routeName: 'PLP',
            params: {
              refresh: true,
            },
          },
        }),
      );
    });
  };

  createProfileOnFinish = async (disabled) => {
    const {userId, addChildProfile: addChildProfileNew, navigation} = this.props;
    if (!this.data.styles) this.data.styles = '0';
    if (disabled) return;
    this.setState(
      {
        disabled: true,
        completedProfile: true,
      },
      async () => {
        const payload = {
          data: this.data,
          userId,
        };
        addChildProfileNew(payload);
        await setValueInAsyncStorage(SBP_PROFILE_CREATED, '1');
        await setValueInAsyncStorage(SBP_CURRENT_PROFILE_ID, this.data.id.toString());
        setTimeout(() => {
          navigateToNestedRoute(
            navigation,
            'SbpPlpStack',
            'PLP',
            Object.assign({}, payload.data, {refresh: true, isSBP: true}),
          );
        }, SBP_TIME_DELAY_CREATE_TO_PLP);
      },
    );
  };

  onEditTakeUserToSizeScreen = (navigation, departmentChange) => {
    navigation.navigate({
      routeName: routeNames.size,
      params: Object.assign({}, this.data, {departmentChange}),
      key: new Date().getTime(),
    });
  };

  /* eslint-disable complexity , sonarjs/cognitive-complexity */
  goToNextScreen = () => {
    const {navigation, sbpLabels} = this.props;
    let {disabled} = this.state;
    const {params, routeName} = navigation.state;
    disabled = routeName === routeNames.colors || routeName === 'StylesInput' ? false : disabled;
    const nameHasRegularCharacter = /^[a-z0-9 _]+$/i.test(this.data.name);
    const onProfileEdit = navigation.state && params && params.isEdit;
    let departmentChange = false;

    if (navigation.state && params && params.gender !== this.data.gender) {
      departmentChange = true;
    }
    if (navigation.state && params && params.isEdit && params.gender !== this.data.gender) {
      departmentChange = true;
    }

    if (navigation.state && params && params.isEdit && routeName === routeNames.gender) {
      this.onEditTakeUserToSizeScreen(navigation, departmentChange);
      return;
    }

    if (onProfileEdit && this.data.name.length === 0) {
      const nameToastLabel = showSBPLabels(
        sbpLabels,
        'lbl_sbp_toast_message_name',
        'Please enter a name.',
      );
      this.toast.show(nameToastLabel, toastNotificationDuration);
      return;
    }

    if (navigation.state && !nameHasRegularCharacter) {
      const noSpecialCharsLabel = showSBPLabels(
        sbpLabels,
        'lbl_sbp_toast_message_nospecial_char',
        'Special characters not allowed in name.',
      );
      this.toast.show(noSpecialCharsLabel, toastNotificationDuration);
      return;
    }

    if (onProfileEdit && this.data.interests.length === 0) {
      const selectInteresLabel = showSBPLabels(
        sbpLabels,
        'lbl_sbp_toast_message_interest',
        'Please select at least 1 interest.',
      );
      this.toast.show(selectInteresLabel, toastNotificationDuration);
      return;
    }

    if (onProfileEdit && disabled && routeName === 'SizeInput') {
      const selectSizeLabel = showSBPLabels(
        sbpLabels,
        'lbl_sbp_toast_message_size',
        'Please select at least 1 size per category.',
      );
      this.toast.show(selectSizeLabel, toastNotificationDuration);
      return;
    }

    if (onProfileEdit && this.data.name.length >= 1 && this.data.interests.length > 1) {
      this.updateProfileOnEdit(params);
      return;
    }
    const nextIndex = Number(sequence.indexOf(navigation.state.routeName)) + 1;
    if (sequence.length === nextIndex) {
      this.createProfileOnFinish(disabled);
      return;
    }
    this.setState(
      (prevState) => Object.assign({}, prevState, {nextIndex}),
      () =>
        navigation.navigate({
          routeName: sequence[nextIndex],
          params: Object.assign({}, this.data, {departmentChange}),
          key: new Date().getTime(),
        }),
    );
  };
  /* eslint-enable complexity */

  headerIndicatorStyling = (curr, navigation) => {
    return {
      ...createProfileContainerStyle.viewTwo,
      borderWidth: curr === navigation.state.routeName ? 3 : 0,
      height: curr === navigation.state.routeName ? 10 : 6,
      marginTop: curr === navigation.state.routeName ? 4 : 6,
    };
  };

  headerIndicator = (curr, navigation) => {
    return <View style={this.headerIndicatorStyling(curr, navigation)} />;
  };

  goToPrevScreen() {
    const {navigation} = this.props;
    const {params, routeName} = navigation.state;
    let fromSizeScreen = false;
    if (routeName === routeNames.size) {
      fromSizeScreen = true;
    }

    if (params && params.isEdit && routeName === routeNames.size) {
      navigation.popToTop();
    }

    if (navigation.state && params && params.isEdit) {
      navigation.pop();
      return;
    }
    const prevIndex = Number(sequence.indexOf(navigation.state.routeName)) - 1;

    if (params?.fromNavMenu && prevIndex < 0) {
      const navigateAction = NavigationActions.navigate({
        routeName: 'PlpStack',
        params: {},
        action: NavigationActions.navigate({routeName: ROUTE_NAMES.NAV_MENU_LEVEL_1}),
      });
      navigation.dispatch(navigateAction);
      return;
    }

    if (prevIndex < 0) {
      navigation.navigate('Home', {});
      return;
    }

    this.setState(
      (prevState) => Object.assign({}, prevState, {prevIndex}),
      () =>
        navigation.navigate({
          routeName: sequence[prevIndex],
          params: Object.assign({}, this.data, {fromSizeScreen}),
          key: `${new Date().getTime()}-inverted`,
        }),
    );
  }

  // eslint-disable-next-line complexity
  async handleFinishLater() {
    const {navigation} = this.props;
    const {routeName} = navigation.state;
    const fromPLP = navigation.state && navigation.state.params && navigation.state.params.fromPLP;
    switch (routeName) {
      case routeNames.age:
        delete this.data.relation;
        delete this.data.month;
        delete this.data.year;
        break;
      case routeNames.gender:
        delete this.data.gender;
        break;
      case routeNames.size:
        delete this.data.size;
        break;
      case routeNames.interest:
        delete this.data.interests;
        break;
      default:
        break;
    }

    this.data.isIncomplete = true;

    const {userId, updateProfile} = this.props;

    if (navigation?.state?.params?.fromNavMenu) {
      const navigateAction = NavigationActions.navigate({
        routeName: 'PlpStack',
        params: {},
        action: NavigationActions.navigate({routeName: ROUTE_NAMES.NAV_MENU_LEVEL_1}),
      });
      navigation.dispatch(navigateAction);
      return;
    }

    if (routeName === routeNames.name && !fromPLP) {
      navigation.navigate('Home', {});
      return;
    }
    const payload = {
      data: this.data,
      userId,
    };

    if (fromPLP) {
      if (routeName === routeNames.name) {
        navigateToNestedRoute(navigation, 'SbpPlpStack', 'PLP');
      } else {
        this.toast.show('New profile saved for later', toastNotificationDuration, () => {
          updateProfile(payload);
          navigateToNestedRoute(navigation, 'SbpPlpStack', 'PLP');
        });
      }
    } else {
      updateProfile(payload);
      navigation.navigate('Home', {});
    }
  }

  hasError(routeName) {
    const {
      name,
      theme,
      interests,
      relation,
      gender,
      size,
      favorite_colors: favouriteColor,
    } = this.data;
    switch (routeName) {
      case routeNames.name:
        return doesNotExist(name, theme);
      case routeNames.age:
        return doesNotExist(relation);
      case routeNames.gender:
        return doesNotExist(gender);
      case routeNames.size:
        return doesSizeNotExist(size);
      case routeNames.interest:
        return doesNotExist(interests);
      case routeNames.colors:
        return doesNotExist(favouriteColor);
      default:
        return false;
    }
  }

  selectCorrectAnalyticsEvent = () => {
    const {navigation} = this.props;
    if (navigation?.state?.params?.fromNavMenu) {
      return {
        customEvents: ['event24', 'event171'],
      };
    }
    return {
      customEvents: ['event24'],
    };
  };

  renderTopIndicator = () => {
    const {navigation} = this.props;
    return this.data.isEdit ? (
      <View style={createProfileContainerStyle.renderContainer} />
    ) : (
      <View
        style={{
          ...createProfileContainerStyle.viewOne,
        }}>
        {sequence.map((curr) => this.headerIndicator(curr, navigation))}
      </View>
    );
  };

  renderTopNavigation = () => {
    const {isLoading} = this.state;
    return isLoading ? (
      <View style={createProfileContainerStyle.viewThreeButton} />
    ) : (
      <TouchableOpacity
        accessibilityRole="button"
        onPress={this.goToPrevScreen}
        style={createProfileContainerStyle.viewThreeButton}>
        {this.data.isEdit ? (
          <Image source={closeImgSrc} style={createProfileContainerStyle.viewThreeButtonImg1} />
        ) : (
          <Image source={caretsImgSrc} style={createProfileContainerStyle.viewThreeButtonImg2} />
        )}
      </TouchableOpacity>
    );
  };

  renderFinishLater = () => {
    const {navigation, sbpLabels} = this.props;
    const {routeName} = navigation.state;
    const fromPLP = navigation.state.params && navigation.state.params.fromPLP;
    const finishLaterCMSLabel = showSBPLabels(
      sbpLabels,
      'lbl_sbp_finish_later',
      'FINISH LATER',
    ).toUpperCase();
    const onProfileEdit =
      this.data.isEdit ||
      routeName === routeNames.colors ||
      (routeName === routeNames.name && !fromPLP);
    return onProfileEdit ? null : (
      <TouchableOpacity
        accessibilityRole="button"
        onPress={this.handleFinishLater}
        style={createProfileContainerStyle.viewThreeButton2}>
        <Text style={{...createProfileContainerStyle.textOne}}>{finishLaterCMSLabel}</Text>
      </TouchableOpacity>
    );
  };

  renderSaveButtonOnEdit = () => {
    const {saveButtonDisabled} = this.state;
    return (
      <View style={{...createProfileContainerStyle.saveButtonContainer}}>
        <Text style={{...createProfileContainerStyle.saveButtonText}}>
          {this.handleShowText(this.data)}
        </Text>
        <SaveButton saveOnPress={this.goToNextScreen} disabled={saveButtonDisabled} />
      </View>
    );
  };

  renderNextButton = (completedProfile, disabled, nextButtonStyle) => {
    return (
      this.handleShowText(this.data) !== 'FINISH' && (
        <TouchableOpacity
          accessibilityRole="button"
          disabled={completedProfile || disabled}
          onPress={this.goToNextScreen}
          style={[
            {
              ...createProfileContainerStyle.nextButton,
            },
            nextButtonStyle,
          ]}>
          <Image style={createProfileContainerStyle.groupImg} source={groupImgSrc} />
        </TouchableOpacity>
      )
    );
  };

  renderAnalyticsClickTracker = (completedProfile, disabled, nextButtonStyle) => {
    const {customEvents} = this.selectCorrectAnalyticsEvent();
    return (
      this.handleShowText(this.data) === 'FINISH' && (
        <ClickTracker
          as={TouchableOpacity}
          name={names.screenNames.sbp_finish_profile}
          module="browse"
          clickData={{customEvents}}
          accessibilityRole="button"
          disabled={completedProfile || disabled}
          onPress={this.goToNextScreen}
          style={[
            {
              ...createProfileContainerStyle.nextButton,
            },
            nextButtonStyle,
          ]}>
          <Image style={createProfileContainerStyle.groupImg} source={groupImgSrc} />
        </ClickTracker>
      )
    );
  };

  // eslint-disable-next-line complexity
  renderBottomGradientContent = (routeName) => {
    let {disabled} = this.state;
    const {navigation} = this.props;
    const {completedProfile} = this.state;
    const fromSizeScreen = navigation.state.params && navigation.state.params.fromSizeScreen;
    const buttonMargin =
      routeName === routeNames.size || routeName === routeNames.interest ? -1 : -5;

    disabled = routeName === routeNames.colors || routeName === 'StylesInput' ? false : disabled;
    const nextButtonStyle = {
      opacity: disabled || completedProfile ? 0.4 : 1,
      marginBottom: buttonMargin,
    };
    const textFiveStyle = {
      opacity: disabled ? 0.4 : 1,
      marginBottom: buttonMargin,
    };
    if (this.data.isEdit && routeName !== routeNames.gender) {
      return this.renderSaveButtonOnEdit();
    }
    if (routeName === routeNames.gender && !this.data.isEdit && !fromSizeScreen) {
      return null;
    }
    return (
      <Fragment>
        <Text
          style={[
            {
              ...createProfileContainerStyle.textFive,
            },
            textFiveStyle,
          ]}>
          {this.handleShowText(this.data)}
        </Text>
        {this.renderAnalyticsClickTracker(completedProfile, disabled, nextButtonStyle)}
        {this.renderNextButton(completedProfile, disabled, nextButtonStyle)}
      </Fragment>
    );
  };

  // eslint-disable-next-line complexity
  render() {
    const {navigation, children, title, subtitle} = this.props;
    const {scrollEnabled} = this.state;
    const {routeName} = navigation.state;
    const {theme} = this.data;
    const profileIconNoBg = theme ? Themes[theme].iconNoBackground : Themes[0].iconNoBackground;
    const colorFromTheme =
      theme === undefined || (typeof theme === 'string' && !theme.length)
        ? colorWhite
        : Themes[theme || 0].color;
    const newcolor = [
      `rgba(${hexToRgbNew(colorFromTheme)},.82)`,
      `rgba(${hexToRgbNew(colorFromTheme)},.85)`,
      `rgba(${hexToRgbNew(colorFromTheme)},.93)`,
      `rgba(${hexToRgbNew(colorFromTheme)},.98)`,
    ];
    const childrenWithExtraProp = React.Children.map(children, (child) => {
      return React.cloneElement(child, {
        setValue: this.setValue.bind(this),
        data: Object.assign({}, this.data),
        goToNextScreen: this.goToNextScreen.bind(this),
      });
    });

    const bgColor = {
      backgroundColor: colorFromTheme,
    };

    const shouldScreenScroll =
      routeName === routeNames.age || routeName === routeNames.name ? false : scrollEnabled;

    return (
      <View style={{height}}>
        {this.renderTopIndicator()}
        <View style={{...createProfileContainerStyle.viewThree}}>
          {this.renderTopNavigation()}
          {this.renderFinishLater()}
        </View>
        <View style={createProfileContainerStyle.viewFour}>
          <LinearGradient
            start={{x: 0, y: 0}}
            end={{x: 1, y: 0}}
            colors={[
              'rgba(255,255,255, 0.98)',
              'rgba(255,255,255, 0.92)',
              'rgba(255,255,255, 0.82)',
            ]}
            style={createProfileContainerStyle.viewFourGrad}
          />
        </View>
        <ScrollView
          style={[{padding}, createProfileContainerStyle.scrollViewCont]}
          scrollEnabled={shouldScreenScroll}
          showsVerticalScrollIndicator={false}
          onContentSizeChange={(w, h) => {
            if (routeName !== 'StylesInput')
              this.setState((prevState) =>
                Object.assign({}, prevState, {scrollEnabled: h > height}),
              );
          }}>
          <Text style={{...createProfileContainerStyle.textTwo}}>{title(this.data.name)}</Text>
          <Text style={{...createProfileContainerStyle.textThree}}>{subtitle}</Text>
          {childrenWithExtraProp}
          <View style={createProfileContainerStyle.viewEmpty} />
        </ScrollView>
        <LinearGradient
          colors={
            scrollEnabled && (routeName === routeNames.size || routeName === routeNames.interest)
              ? newcolor
              : ['rgba(255,255,255, 0)', 'rgba(255,255,255, 0)']
          }
          style={{...createProfileContainerStyle.linearGradient}}
          start={{x: 0.5, y: 0.2}}
          end={{x: 1, y: 0}}>
          {this.renderBottomGradientContent(routeName)}
        </LinearGradient>
        {routeName === routeNames.gender && (
          <View style={createProfileContainerStyle.profileIconView}>
            <Image
              source={profileIconNoBg}
              resizeMode="contain"
              style={createProfileContainerStyle.profileIcon}
            />
          </View>
        )}
        {routeName === routeNames.size || routeName === routeNames.interest ? null : (
          <View style={[bgColor, createProfileContainerStyle.profileIconViewEmpty]} />
        )}
        <Toast
          ref={(toast) => {
            this.toast = toast;
          }}
          style={createProfileContainerStyle.toastMessageBg}
          position="top"
          positionValue={80}
          fadeInDuration={550}
          fadeOutDuration={900}
          opacity={0.9}
          textStyle={createProfileContainerStyle.toastMessage}
        />
      </View>
    );
  }
}

CreateProfileContainer.propTypes = {
  addChildProfile: func.isRequired,
  children: shape([]).isRequired,
  navigation: shape({
    state: shape({
      params: shape({
        departmentChange: bool,
        fromPLP: bool,
        fromSizeScreen: bool,
        gender: string,
        isEdit: bool,
      }),
      routeName: string,
    }),
  }).isRequired,
  sbpLabels: shape({}).isRequired,
  subtitle: string.isRequired,
  title: func.isRequired,
  updateProfile: func.isRequired,
  userId: number.isRequired,
};

export default CreateProfileContainer;
