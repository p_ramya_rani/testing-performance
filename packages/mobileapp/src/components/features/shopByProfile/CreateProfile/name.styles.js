// 9fbef606107a605d69c0edbcd8029e5d
import {StyleSheet} from 'react-native';
import {isAndroid} from '@tcp/core/src/utils/utils.app';
import {
  colorWhite,
  colorNero,
  colorNobel,
  colorNero2,
  fontFamilyNunitoRegular,
  fontFamilyNunitoSemiBold,
  fontFamilyNunitoExtraBold,
  weightFont,
  fontSizeFourteen,
  fontSizeTwelve,
} from '../ShopByProfile.styles';

const nameInputStyle = StyleSheet.create({
  animatedView: {
    backgroundColor: colorWhite,
    borderColor: colorWhite,
    borderRadius: 70,
    borderWidth: 4,
    margin: 10,
    marginRight: isAndroid() ? 5 : 9,
  },
  chooseIconText: {
    color: colorNero,
    fontFamily: fontFamilyNunitoSemiBold,
    fontSize: fontSizeFourteen,
    fontWeight: weightFont,
    marginBottom: 20,
    marginTop: 40,
  },
  iconImage: {
    borderColor: colorWhite,
    borderRadius: 100,
    borderWidth: 2,
    height: isAndroid() ? 68 : 73,
    resizeMode: isAndroid() ? 'cover' : 'contain',
    width: isAndroid() ? 68 : 73,
    zIndex: -1,
  },
  imageView: {
    height: '100%',
    width: '100%',
  },
  nameText: {
    color: colorNero,
    fontFamily: fontFamilyNunitoExtraBold,
    fontSize: fontSizeTwelve,
    fontWeight: weightFont,
  },
  nameTextInput: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  textInput: {
    borderBottomColor: colorNobel,
    borderBottomWidth: 1,
    color: colorNero2,
    fontFamily: fontFamilyNunitoRegular,
    fontSize: 16,
    fontWeight: weightFont,
    padding: 4,
    width: '85%',
  },
  textInputIcon: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginHorizontal: -20,
    paddingHorizontal: 2,
  },
});

export default nameInputStyle;
