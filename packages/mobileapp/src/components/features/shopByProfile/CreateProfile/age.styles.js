// 9fbef606107a605d69c0edbcd8029e5d 
import {StyleSheet, Dimensions} from 'react-native';
import {
  colorNero,
  colorNobel,
  fontFamilyNunitoSemiBold,
  fontFamilyNunitoExtraBold,
  weightFont,
  fontSizeFourteen,
} from '../ShopByProfile.styles';

const {width} = Dimensions.get('window');

const ageInputStyle = StyleSheet.create({
  childRelationshipText: {
    color: colorNero,
    fontFamily: fontFamilyNunitoSemiBold,
    fontSize: fontSizeFourteen,
    marginBottom: 24,
    marginTop: 40,
  },
  childRelationshipView: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    width: '100%',
  },
  renderButtonContainer: {
    borderColor: colorNobel,
    borderRadius: 6,
    borderWidth: 1,
    justifyContent: 'center',
    marginBottom: 20,
    marginRight: 20,
    padding: 12,
    width: width / 2 - 52,
  },
  renderButtonText: {
    fontFamily: fontFamilyNunitoExtraBold,
    fontSize: width <= 375 ? 12 : 13,
    fontWeight: weightFont,
    letterSpacing: 1,
    textAlign: 'center',
  },
  renderChildrenContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  renderChildrenPicker: {
    width: '45%',
  },
  renderChildrenView: {
    bottom: 50,
  },
});

export default ageInputStyle;
