// 9fbef606107a605d69c0edbcd8029e5d 
import {connect} from 'react-redux';
import {getSBPLabels} from '../ShopByProfile.selector';
import ColorInput from './colors.component';

const mapStateToProps = state => ({
  sbpLabels: getSBPLabels(state),
});

export default connect(mapStateToProps)(ColorInput);
