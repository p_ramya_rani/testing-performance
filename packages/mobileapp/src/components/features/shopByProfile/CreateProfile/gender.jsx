// 9fbef606107a605d69c0edbcd8029e5d 
import {connect} from 'react-redux';
import {getSBPLabels} from '../ShopByProfile.selector';
import GenderInput from './gender.component';

const mapStateToProps = state => {
  return {
    sbpLabels: getSBPLabels(state),
  };
};

export default connect(mapStateToProps)(GenderInput);
