// 9fbef606107a605d69c0edbcd8029e5d
import React, {PureComponent} from 'react';
import {View, Text, TouchableOpacity, Image, ActivityIndicator} from 'react-native';
import {func, shape, string} from 'prop-types';
import logger from '@tcp/core/src/utils/loggerInstance';
import CreateProfileContainer from './container';
import {
  CTInterestIcons,
  DEPARTMENTS,
  BRAND_ID,
  MAX_NUMBER_INTERESTS,
  SBP_GET_INTERESTS_DELAY,
} from '../ShopByProfile.constants';
import interestsInputStyle from './interests.styles';
import LABELS from '../__labels__';
import {capitalizeWords, showSBPLabels} from '../PLP/helper';
import Themes from '../ShopByProfile.theme';
import {colorWhite, colorNero, colorPrussian, colorNobel} from '../ShopByProfile.styles';

class InterestsInput extends PureComponent {
  constructor(props) {
    super(props);
    this.loadingIcon = this.loadingIcon.bind(this);
  }

  state = {
    selected: null,
    numberOfChildInterestSelected: 0,
    numberOfParentInterestSelected: 0,
  };

  componentDidMount() {
    const {navigation, getInterests} = this.props;
    const selected =
      navigation && navigation.state && navigation.state.params && navigation.state.params.interests
        ? navigation.state.params.interests.split(',')
        : null;
    let numOfChildInterest = 0;
    let numOfParentInterest = 0;

    getInterests();

    setTimeout(() => {
      if (selected) {
        const listChildInterests = this.extractChildInterest();
        selected.forEach((interest) => {
          if (listChildInterests.indexOf(interest) >= 0) {
            numOfChildInterest += 1;
          } else {
            numOfParentInterest += 1;
          }
        });
      }

      this.setState({
        selected,
        numberOfChildInterestSelected: numOfChildInterest,
        numberOfParentInterestSelected: numOfParentInterest,
      });
    }, SBP_GET_INTERESTS_DELAY);
  }

  getL2Categories(setValue) {
    const {navigation, screenProps, L2Categories} = this.props;
    let {gender} = navigation.state.params;
    const {brandId} = screenProps.apiConfig;
    let babyDepartment;
    let categories;
    let subCategories;
    let subCategoriesObj;
    try {
      if (
        gender.toLowerCase() === DEPARTMENTS.BABY_GIRL.toLowerCase() ||
        gender === DEPARTMENTS.BABY_BOY.toLowerCase() ||
        brandId.toLowerCase() === BRAND_ID.GYM.toLowerCase()
      ) {
        gender = capitalizeWords(gender);
        babyDepartment = capitalizeWords(gender);
      }
      if (
        gender.toLowerCase() === DEPARTMENTS.BABY_GIRL.toLowerCase() ||
        gender.toLowerCase() === DEPARTMENTS.BABY_BOY.toLowerCase()
      ) {
        gender = 'Baby';
        const babyCategories = L2Categories.filter(
          (category) => category.categoryContent.name.toLowerCase() === gender.toLowerCase(),
        );
        [subCategoriesObj] = babyCategories;
        ({subCategories} = subCategoriesObj);
      } else {
        categories = L2Categories.filter(
          (category) => category.categoryContent.name.toLowerCase() === gender.toLowerCase(),
        );
        if (categories.length < 1) return;
        [subCategoriesObj] = categories;
        ({subCategories} = subCategoriesObj);
      }
      const filteringGender =
        babyDepartment === capitalizeWords(DEPARTMENTS.BABY_GIRL)
          ? capitalizeWords(DEPARTMENTS.BABY_BOY)
          : capitalizeWords(DEPARTMENTS.BABY_GIRL);
      /* eslint-disable consistent-return */
      return Object.entries(subCategories).map(([key, value]) => {
        if (
          value.hasSubCategory === true &&
          key !== 'UNIDENTIFIED_GROUP' &&
          key !== filteringGender
        ) {
          return (
            <View>
              <View>
                <Text style={interestsInputStyle.subCategoriesKey}>{key}</Text>
              </View>
              <View style={interestsInputStyle.childCategories}>
                {value.items.map((item) => {
                  const category = item.categoryContent.name;
                  const {categoryContent} = item;
                  return this.renderParentInterests(category, setValue, categoryContent);
                })}
              </View>
            </View>
          );
        }
        return null;
      });
    } catch (e) {
      logger.error('Error has occurred: ', {
        error: e,
        extraData: {},
      });
    }
  }

  filterAndSortInterests = (response, type) => {
    return response
      .filter((interest) => interest.interest_type.S === type)
      .map((interest) => interest.name.S)
      .sort();
  };

  loadingIcon = (props) => {
    const {data} = props;
    return (
      <View style={interestsInputStyle.loadingIcon}>
        <ActivityIndicator size="large" color={Themes[data.theme || 0].accent} />
      </View>
    );
  };

  extractChildInterest = () => {
    const {childInterests} = this.props;
    const interests = childInterests && childInterests.Items && childInterests.Items;
    return interests.map((int) => int.name.S);
  };

  toggle = (option, setValue) => {
    const {selected, numberOfChildInterestSelected, numberOfParentInterestSelected} = this.state;
    const selectedNew = selected ? new Array(...selected) : [];
    const i = selectedNew.indexOf(option);
    const isChildInterest = this.extractChildInterest().indexOf(option);
    let numberChildInterest = numberOfChildInterestSelected;
    let numberParentInterest = numberOfParentInterestSelected;
    if (i > -1) {
      if (isChildInterest < 0) {
        numberParentInterest -= 1;
      } else {
        numberChildInterest -= 1;
      }
      selectedNew.splice(i, 1);
    } else if (isChildInterest < 0) {
      if (numberParentInterest === MAX_NUMBER_INTERESTS) return;
      numberParentInterest += 1;
      selectedNew.push(option);
    } else {
      if (numberChildInterest === MAX_NUMBER_INTERESTS) return;
      numberChildInterest += 1;
      selectedNew.unshift(option);
    }
    this.setState((prevState) =>
      Object.assign({}, prevState, {
        selected: selectedNew,
        numberOfChildInterestSelected: numberChildInterest,
        numberOfParentInterestSelected: numberParentInterest,
      }),
    );
    setValue('interests', selectedNew.join(','));
  };

  renderChildInterests(interest, setValue) {
    const {selected} = this.state;
    const isSelected = selected ? selected.indexOf(interest) > -1 : null;
    const bgColor = {
      backgroundColor: isSelected ? colorPrussian : colorWhite,
      borderWidth: isSelected ? 0 : 1,
    };
    const tColor = {
      tintColor: isSelected ? colorWhite : colorNobel,
    };

    return (
      <TouchableOpacity onPress={() => this.toggle(interest, setValue)} accessibilityRole="button">
        <View style={interestsInputStyle.childInterestsButton}>
          <View style={[interestsInputStyle.renderChildInterestsIconContainer, bgColor]}>
            <Image
              source={CTInterestIcons[interest]}
              style={[tColor, interestsInputStyle.childInterestImg]}
            />
          </View>
          <View style={interestsInputStyle.renderChildInterestsIconTextContainer}>
            {interest.split(' ').map((word) => (
              <Text style={interestsInputStyle.renderChildInterestsIconText}>{word}</Text>
            ))}
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  renderChildren = (props) => {
    const {childInterests} = this.props;
    const interestChild = childInterests && childInterests.Items ? childInterests.Items : [];
    const sortedChildInterest = this.filterAndSortInterests(interestChild, 'child');
    return (
      <View>
        <View style={interestsInputStyle.renderChildrenView}>
          {sortedChildInterest.map((interest) =>
            this.renderChildInterests(interest, props.setValue),
          )}
        </View>
        <Text style={interestsInputStyle.subHeaderText}>{LABELS.interests().SUBHEADER}</Text>
        <View style={interestsInputStyle.childCategories}>
          {this.getL2Categories(props.setValue)}
        </View>
      </View>
    );
  };

  renderParentInterests = (interest, setValue, categoryContent) => {
    const {selected} = this.state;
    const isSelected = selected ? selected.indexOf(interest) > -1 : null;
    const bgColor = {
      backgroundColor: isSelected ? colorPrussian : colorWhite,
    };
    const iColor = {
      color: isSelected ? colorWhite : colorNero,
    };

    if (!categoryContent.displayToCustomer) return null;
    return (
      <TouchableOpacity onPress={() => this.toggle(interest, setValue)} accessibilityRole="button">
        <View style={[interestsInputStyle.renderInterestContainer, bgColor]}>
          <Text style={[interestsInputStyle.renderInterestContainerText, iColor]}>{interest}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    const RenderChildren = this.renderChildren.bind(this);
    const {sbpLabels, navigation} = this.props;
    const {name} = navigation.state.params;

    const headerCMSLabel =
      sbpLabels && sbpLabels.lbl_sbp_header_interest
        ? sbpLabels.lbl_sbp_header_interest.replace("[Name's]", `${name}'s`)
        : `What are some of their interests?`;

    const subtitleCMSLabel = showSBPLabels(
      sbpLabels,
      'lbl_sbp_text_interest',
      'Select at least 1 interest.',
    );

    return (
      <CreateProfileContainer
        title={() => headerCMSLabel}
        subtitle={subtitleCMSLabel}
        navigation={navigation}>
        <RenderChildren />
      </CreateProfileContainer>
    );
  }
}

InterestsInput.propTypes = {
  sbpLabels: shape({
    lbl_sbp_header_interest: string,
  }).isRequired,
  navigation: shape({
    state: shape({
      params: shape({
        gender: string,
        interests: string,
        name: string,
      }),
    }),
  }).isRequired,
  screenProps: shape({
    apiConfig: shape({
      brandId: string,
    }),
  }).isRequired,
  L2Categories: shape([]).isRequired,
  getInterests: func.isRequired,
  childInterests: shape([]).isRequired,
};

export default InterestsInput;
