// 9fbef606107a605d69c0edbcd8029e5d 
import {StyleSheet, Dimensions} from 'react-native';
import {padding} from '../ShopByProfile.constants';
import {
  colorWhite,
  colorNero,
  fontFamilyNunitoSemiBold,
  fontSizeFourteen,
} from '../ShopByProfile.styles';

const {width} = Dimensions.get('window');
const bubbleSize = (width - padding * 2) / 4 - 24;

const colorInputStyle = StyleSheet.create({
  closeImg: {
    height: 23,
    width: 23,
  },
  colorContainer: {
    borderRadius: bubbleSize,
    height: bubbleSize,
    margin: 12,
    width: bubbleSize,
  },
  colorName: {
    color: colorNero,
    fontFamily: fontFamilyNunitoSemiBold,
    fontSize: fontSizeFourteen,
    textAlign: 'center',
  },
  colorText: {
    textAlign: 'center',
  },
  renderChildrenView: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  renderColorView: {
    alignItems: 'center',
    borderColor: colorWhite,
    borderRadius: bubbleSize,
    borderWidth: 2,
    height: '100%',
    justifyContent: 'center',
    width: '100%',
  },
});

export default colorInputStyle;
