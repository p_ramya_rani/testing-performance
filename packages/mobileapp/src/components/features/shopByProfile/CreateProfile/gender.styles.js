// 9fbef606107a605d69c0edbcd8029e5d 
import {StyleSheet} from 'react-native';
import {
  colorNobel,
  fontFamilyNunitoRegular,
  fontFamilyNunitoExtraBold,
  weightFont,
  fontSizeFourteen,
  fontSizeTwelve,
} from '../ShopByProfile.styles';

const genderInputStyle = StyleSheet.create({
  renderButtonContainer: {
    borderColor: colorNobel,
    borderRadius: 6,
    borderWidth: 1,
    flex: 1,
    justifyContent: 'center',
    marginBottom: 20,
    marginRight: 20,
    padding: 7,
  },

  renderButtonText: {
    fontFamily: fontFamilyNunitoExtraBold,
    fontSize: fontSizeFourteen,
    letterSpacing: 1,
    textAlign: 'center',
  },
  renderButtonTextRange: {
    fontFamily: fontFamilyNunitoExtraBold,
    fontSize: fontSizeFourteen,
    fontWeight: weightFont,
    letterSpacing: 1,
    textAlign: 'center',
  },
  renderGenderContainer: {
    flexDirection: 'row',
  },
  sizesText: {
    fontFamily: fontFamilyNunitoRegular,
    fontSize: fontSizeTwelve,
  },
});

export default genderInputStyle;
