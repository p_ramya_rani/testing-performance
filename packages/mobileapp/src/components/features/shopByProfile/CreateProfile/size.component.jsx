// 9fbef606107a605d69c0edbcd8029e5d
import React, {PureComponent} from 'react';
import {View, Text, TouchableOpacity, ActivityIndicator} from 'react-native';
import {bool, func, shape, string} from 'prop-types';
import logger from '@tcp/core/src/utils/loggerInstance';
import CreateProfileContainer from './container';
import sizeInputStyle from './size.styles';
import Themes from '../ShopByProfile.theme';
import {showSBPLabels} from '../PLP/helper';
import {
  colorBlack,
  colorMortar,
  colorWhite,
  fontFamilyNunitoBlack,
  fontFamilyNunitoSemiBold,
} from '../ShopByProfile.styles';

class SizeInput extends PureComponent {
  state = {
    loading: true,
    error: false,
    selectedValues: {},
  };

  componentDidMount() {
    const {navigation} = this.props;
    const {gender} = navigation.state.params;
    this.getSizes(gender);
  }

  setSizesFromResponse = () => {
    let {selectedValues} = this.state;
    const {departmentSizes} = this.props;
    selectedValues = JSON.parse(JSON.stringify(selectedValues));
    const sizeMapping = departmentSizes.sort((a, b) => a.ranking - b.ranking);
    const size = {};
    for (let i = 0; i < sizeMapping.length; i += 1) {
      const item = sizeMapping[i];
      const {category} = item;
      const fit = item.fit ? item.fit : 'Regular';
      const sizeS = item.size;

      if (!size[category]) size[category] = {};
      if (!size[category][fit]) size[category][fit] = {sizes: []};
      size[category][fit].sizes.push(sizeS);
      if (!selectedValues[category] && item.fit) selectedValues[category] = {fit: 'Regular'};
    }
    this.processSizeMapping(size, selectedValues);
  };

  getSizes = (gender) => {
    const {getDepartmentSizes} = this.props;
    getDepartmentSizes(gender);
    setTimeout(() => {
      this.setSizesFromResponse();
    }, 2000);
  };

  setSelectedValue = (selectedValues, setValue) => {
    const {size} = this.state;
    const sizeKeys = Object.keys(size);
    const selectedSizeValues = Object.values(selectedValues).filter((o) => o.size && o.size.length);
    if (sizeKeys.length === selectedSizeValues.length)
      setValue('size', JSON.stringify(selectedValues));
    else setValue('size', JSON.stringify({}));
    this.setState({selectedValues});
  };

  processSizeMapping = (size, selectedValues) => {
    let selectedValuesNew = selectedValues;
    const {navigation} = this.props;
    const {size: savedSize} = navigation.state.params;
    const departmentChange = navigation.state.params && navigation.state.params.departmentChange;
    const profileGender = navigation.state.params && navigation.state.params.gender;
    try {
      if (typeof savedSize !== 'undefined' && !departmentChange) {
        const savedSizeParse = JSON.parse(savedSize);
        selectedValuesNew = savedSizeParse;
      } else if (profileGender === 'Boy' || profileGender === 'Girl') {
        selectedValuesNew = {
          Tops: {
            fit: 'Regular',
          },
          Bottoms: {
            fit: 'Regular',
          },
          'Dresses and Rompers': {
            fit: 'Regular',
          },
        };
      }
    } catch (e) {
      logger.error('Error has occurred: ', {
        error: e,
        extraData: {},
      });
    }
    this.setState({size, loading: false, selectedValues: selectedValuesNew});
  };

  select = (value, type, category, setValue) => {
    let {selectedValues} = this.state;
    selectedValues = JSON.parse(JSON.stringify(selectedValues));
    if (!selectedValues[category]) selectedValues[category] = {};
    if (type === 'fit') {
      if (selectedValues[category].fit === value) return;
      selectedValues[category].fit = value;
      selectedValues[category].size = [];
    } else {
      if (!selectedValues[category].size) selectedValues[category].size = [];
      if (selectedValues[category].size.indexOf(value) === -1) {
        if (selectedValues[category].size.length > 2) return;
        selectedValues[category].size.push(value);
      } else selectedValues[category].size.splice(selectedValues[category].size.indexOf(value), 1);
    }
    this.setSelectedValue(selectedValues, setValue);
  };

  renderButton(option, isSelected, type, category, setValue) {
    const bgColor = {
      backgroundColor: isSelected ? colorBlack : colorWhite,
    };
    const textStyle = {
      color: isSelected ? colorWhite : colorMortar,
      fontFamily: isSelected ? fontFamilyNunitoBlack : fontFamilyNunitoSemiBold,
      letterSpacing: 0.42,
    };

    return (
      <TouchableOpacity
        style={{...sizeInputStyle.renderButton}}
        onPress={() => this.select(option, type, category, setValue)}
        accessibilityRole="button">
        <View
          style={[
            {
              ...sizeInputStyle.renderButtonView,
            },
            bgColor,
          ]}>
          <Text
            style={[
              {
                ...sizeInputStyle.renderButtonText,
              },
              textStyle,
            ]}>
            {option.toUpperCase()}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }

  renderButtonSize(sizes, category, setValue) {
    const {selectedValues} = this.state;
    return (
      <View>
        <Text style={sizeInputStyle.renderCardInternalText}>SIZE</Text>
        <View style={{...sizeInputStyle.renderCardView}}>
          {sizes.map((size) => {
            return this.renderButton(
              size,
              !!(
                selectedValues[category] &&
                selectedValues[category].size &&
                selectedValues[category].size.indexOf(size) > -1
              ),
              'size',
              category,
              setValue,
            );
          })}
        </View>
      </View>
    );
  }

  renderButtonFit(options, category, setValue) {
    const fits = Object.keys(options);
    const {selectedValues} = this.state;
    return (
      <View>
        <View>
          <Text style={sizeInputStyle.renderCardInternalText}>FIT</Text>
          <View style={{...sizeInputStyle.renderCardView}}>
            {fits.map((option) => {
              return this.renderButton(
                option,
                selectedValues[category]
                  ? selectedValues[category].fit === option
                  : option === 'Regular',
                'fit',
                category,
                setValue,
              );
            })}
          </View>
        </View>
        {this.renderButtonSize(
          options[
            selectedValues[category] && selectedValues[category].fit
              ? selectedValues[category].fit
              : fits[fits.indexOf('Regular')]
          ].sizes,
          category,
          setValue,
        )}
      </View>
    );
  }

  renderCard(type, options, setValue) {
    const fits = Object.keys(options);
    return (
      <View style={sizeInputStyle.renderCardContainer}>
        <Text style={sizeInputStyle.renderCardText}>{type.toUpperCase()}</Text>
        {fits.length === 1
          ? this.renderButtonSize(options[fits[0]].sizes, type, setValue)
          : this.renderButtonFit(options, type, setValue)}
      </View>
    );
  }

  renderChildren = (props) => {
    const {loading, error, size} = this.state;
    const {data} = props;
    if (loading) return <ActivityIndicator size="large" color={Themes[data.theme || 0].accent} />;
    if (error) return <Text>Error</Text>;
    return (
      <View style={sizeInputStyle.renderChildren}>
        {Object.keys(size).map((s) => {
          return this.renderCard(s, size[s], props.setValue);
        })}
      </View>
    );
  };

  render() {
    const RenderChildren = this.renderChildren.bind(this);
    const {navigation, sbpLabels} = this.props;
    const {name} = navigation.state.params;

    const headerCMSLabel =
      sbpLabels && sbpLabels.lbl_sbp_header_size
        ? sbpLabels.lbl_sbp_header_size.replace('[Name]', name)
        : `What size(s) does ${name} wear?`;

    const subtitleCMSLabel = showSBPLabels(
      sbpLabels,
      'lbl_sbp_text_size',
      'Select 1-3 sizes per style to help filter available sizes while shopping.',
    );

    return (
      <CreateProfileContainer
        title={() => headerCMSLabel}
        subtitle={subtitleCMSLabel}
        navigation={navigation}>
        <RenderChildren />
      </CreateProfileContainer>
    );
  }
}

SizeInput.propTypes = {
  departmentSizes: shape([]).isRequired,
  getDepartmentSizes: func.isRequired,
  navigation: shape({
    state: shape({
      params: shape({
        departmentChange: bool,
        name: string,
        size: string,
      }),
    }),
  }).isRequired,
  sbpLabels: shape({
    lbl_sbp_header_size: string,
  }).isRequired,
};

export default SizeInput;
