// 9fbef606107a605d69c0edbcd8029e5d
export const padding = 32;
export const MAX_CARD_HEIGHT = 245;
export const MAX_NUMBER_PROFILES = 4;
export const SWIPE_AREA = 20;
export const NUMBER_INTERESTS_FOR_TOPMATCHING = 2;
export const INITIAL_NUM_PRODS_INTEREST_GRID = 6;
export const IMAGES_PARAMS = '/t_plp_img_m,f_auto,q_auto,dpr_2.0,w_165/';
export const SBP_SIZE_MAPPING = 'sbpSizeMapping';
export const SBP_CATEGORY_ID_MAPPING = 'sbpCategoryIDMapping';
export const SBP_USER_EMAIL = 'SBP_USER_EMAIL';
export const SBP_UNBXD_SIZES = 'SBP_UNBXD_SIZES';
export const SBP_CURRENT_PROFILE_ID = 'SBP_CURRENT_PROFILE_ID';
export const SBP_PROFILE_CREATED = 'SBP_PROFILE_CREATED';
export const SBP_PROFILE_DELETED = 'SBP_PROFILE_DELETED';
export const SBP_ONE_SIZE = 'one size';
export const SBP_TIME_DELAY_CREATE_TO_PLP = 2000;
export const SBP_GET_INTERESTS_DELAY = 1000;
export const SBP_GET_PROFILES_DELAY = 1400;
export const MAX_NUMBER_INTERESTS = 8;
export const L2_CATEGORY_END = 2;
export const NUMBER_ADDITIONAL_GRID_LAZY_LOAD = 2;
export const INITIAL_DATA = {
  month: 'August',
  year: '2015',
  shoe: 'TODDLER 1',
  fit: 'REGULAR',
  relation: 'Parent',
};
export const sequence = [
  'NameInput',
  'AgeInput',
  'GenderInput',
  'SizeInput',
  'InterestsInput',
  'ColorsInput',
];

export const routeNames = {
  name: 'NameInput',
  age: 'AgeInput',
  gender: 'GenderInput',
  size: 'SizeInput',
  interest: 'InterestsInput',
  colors: 'ColorsInput',
};

export const DEPARTMENTS = {
  BOY: 'BOY',
  GIRL: 'GIRL',
  TODDLER_BOY: 'TODDLER BOY',
  TODDLER_GIRL: 'TODDLER GIRL',
  BABY_BOY: 'BABY BOY',
  BABY_GIRL: 'BABY GIRL',
};

export const BRAND_ID = {
  TCP: 'tcp',
  GYM: 'gym',
};

export const ACCESSORIES_SIZES = {
  BOY: ['S/M(4-7YR)', 'L/XL(8+YR)', 'S (11-13', 'M (1-2)', 'L (3-6)', 'ONE SIZE'],
  GIRL: [
    'M (1-2)',
    'L (3-6)',
    'S (11-13',
    'S/M(4-7YR)',
    'L/XL(8+YR)',
    'ONE SIZE',
    '4-7',
    '8-16',
    '8-10',
    '12-14',
    '4-5',
    '6-7',
  ],
  'TODDLER BOY': [
    'ONE SIZE',
    'XS(6-12 M)',
    'S (12-24M)',
    'M (2T/3T)',
    'L (4T/5T)',
    '6-12 M',
    '12-24 M',
    '3T-4T',
    '2T-3T',
  ],
  'TODDLER GIRL': [
    'ONE SIZE',
    'XS(6-12 M)',
    'S (12-24M)',
    'M (2T/3T)',
    'L (4T/5T)',
    '6-12 M',
    '12-24 M',
    '3T-4T',
    '2T-3T',
  ],
  'BABY GIRL': ['ONE SIZE'],
  'BABY BOY': ['ONE SIZE'],
};

export const DEFAULT_L2_CATEGORY_ORDER_CAT_ID = {
  BOY: ['419530', '47544', '47537', '420029', '47541', '79010', '47540', '458009', '47536'],
  GIRL: [
    '419525',
    '49012',
    '49004',
    '420015',
    '435501',
    '49005',
    '49009',
    '78501',
    '49008',
    '458001',
    '49003',
  ],
  'TODDLER BOY': [
    '419533',
    '47526',
    '47520',
    '420034',
    '47524',
    '77501',
    '47523',
    '458012',
    '47519',
  ],
  'TODDLER GIRL': [
    '419528',
    '47535',
    '47528',
    '435505',
    '47529',
    '47533',
    '79007',
    '47532',
    '458005',
    '47527',
  ],
  'BABY BOY': [
    '453004',
    '351019',
    '714003',
    '54086',
    '478015',
    '478013',
    '79015',
    '54084',
    '351020',
  ],
  'BABY GIRL': [
    '453001',
    '351019',
    '453003',
    '54095',
    '54096',
    '54099',
    '478013',
    '79014',
    '54093',
    '351020',
  ],
};

export const COLORS = [
  {colorName: 'pink', hexValue: '#f240af'},
  {colorName: 'yellow', hexValue: '#fff200'},
  {colorName: 'white', hexValue: '#ffffff'},
  {colorName: 'blue', hexValue: '#0000a8'},
  {colorName: 'metallic', hexValue: '#C0C0C0'},
  {colorName: 'purple', hexValue: '#92278f'},
  {colorName: 'gray', hexValue: '#959595'},
  {colorName: 'green', hexValue: '#00a651'},
  {colorName: 'orange', hexValue: '#ff672d'},
  {colorName: 'multi', hexValue: '#00a5ff'},
  {colorName: 'tan', hexValue: '#c69c6d'},
  {colorName: 'red', hexValue: '#ee1c24'},
  {colorName: 'brown', hexValue: '#754c24'},
  {colorName: 'black', hexValue: '#000000'},
];
export const Months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

export const Years = Array.from(Array(16).keys()).map((y) =>
  String(new Date().getYear() + 1900 - y),
);
export const RELATIONS = ['Parent', 'Grandparent', 'Aunt / Uncle', 'Other'];

/* eslint-disable global-require */
export const CTInterestIcons = {
  Hearts: require('../../../assets/images/shopByProfile/CTInterestsIcons/hearts_2020-09-30/hearts.png'),
  Gaming: require('../../../assets/images/shopByProfile/CTInterestsIcons/video-games_2020-09-30/video-games.png'),
  Vehicles: require('../../../assets/images/shopByProfile/CTInterestsIcons/transportation_2020-09-30/transportation.png'),
  Butterflies: require('../../../assets/images/shopByProfile/CTInterestsIcons/butterfly_2020-09-30/butterfly.png'),
  Positivity: require('../../../assets/images/shopByProfile/CTInterestsIcons/positivity_2020-09-30/positivity.png'),
  Nature: require('../../../assets/images/shopByProfile/CTInterestsIcons/nature_2020-09-30/nature.png'),
  Sports: require('../../../assets/images/shopByProfile/CTInterestsIcons/sports_2020-09-30/sports.png'),
  'Humor & Emojis': require('../../../assets/images/shopByProfile/CTInterestsIcons/comedy_2020-09-30/comedy.png'),
  Dinos: require('../../../assets/images/shopByProfile/CTInterestsIcons/dinos_2020-09-30/dinos.png'),
  'Music & Dance': require('../../../assets/images/shopByProfile/CTInterestsIcons/music_2020-09-30/music.png'),
  'Books & Education': require('../../../assets/images/shopByProfile/CTInterestsIcons/education_2020-09-30/education.png'),
  Unicorns: require('../../../assets/images/shopByProfile/CTInterestsIcons/unicorns_2020-09-30/unicorns.png'),
  Characters: require('../../../assets/images/shopByProfile/CTInterestsIcons/characters_2020-09-30/characters.png'),
  Ninjas: require('../../../assets/images/shopByProfile/CTInterestsIcons/ninja_2020-09-30/ninja.png'),
  Food: require('../../../assets/images/shopByProfile/CTInterestsIcons/foodie_2020-09-30/foodie.png'),
  Mermaids: require('../../../assets/images/shopByProfile/CTInterestsIcons/mermaids_2020-09-30/mermaids.png'),
  Animals: require('../../../assets/images/shopByProfile/CTInterestsIcons/animals_2020-09-30/animals.png'),
  'Sharks & Creatures': require('../../../assets/images/shopByProfile/CTInterestsIcons/sealife_2020-09-30/sealife.png'),
  'Sea Creatures': require('../../../assets/images/shopByProfile/CTInterestsIcons/sealife_2020-09-30/sealife.png'),
};
