// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import {shallow} from 'enzyme';

import NavigationCarousel from '../views/NavigationCarousel.view';

describe('NavigationCarousel', () => {
  it('should be defined', () => {
    expect(NavigationCarousel).toBeDefined();
  });

  it('should render correctly', () => {
    const props = {
      navigation: {
        getParam: param => {
          const obj = {
            navigationObj: [
              {
                title: 'Lorem',
                categoryContent: {
                  name: 'Lorem',
                },
              },
              {
                title: 'Categoriess',
                categoryContent: {
                  name: 'Categoriess',
                },
              },
            ],
            l2Title: 'test',
          };
          return obj[param];
        },
      },
      item: {
        subCategories: [
          {
            title: 'Lorem',
            categoryContent: {
              name: 'Lorem',
            },
          },
          {
            title: 'Categoriess',
            categoryContent: {
              name: 'Categoriess',
            },
          },
        ],
      },
    };
    const component = shallow(<NavigationCarousel {...props} />);
    expect(component).toMatchSnapshot();
  });
});
