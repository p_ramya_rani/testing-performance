// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import {arrayOf, bool, func, shape, string} from 'prop-types';
import {BodyCopy} from '@tcp/core/src/components/common/atoms';
import {isTCP} from '@tcp/core/src/utils';
import {
  PromoAndArrowContainer,
  PromoContainer,
  ArrowIcon,
  ItemView,
  MenuContainer,
} from '../MenuItems.style';

const Icon = require('../../../../../../../assets/images/icons-carets-medium-right.png');

/**
 * @function MenuItems populates the menu item
 * @param {object} links shop by size links
 */
const MenuItems = ({navigate, maxWidthItem, item, hasBadge, promoBannerMargin, hasL3, route}) => {
  const isTcpApp = isTCP();
  return (
    <MenuContainer>
      <ItemView maxWidth={maxWidthItem}>
        <BodyCopy
          fontFamily={isTcpApp ? 'secondary' : 'primary'}
          fontSize="fs16"
          fontWeight={isTcpApp ? 'bold' : 'medium'}
          text={item.categoryContent.name}
          color="text.primary"
          numberOfLines={2}
          textAlign="left"
        />
      </ItemView>
      <PromoAndArrowContainer onPress={() => route(navigate, item.subCategories, item.name, hasL3)}>
        {hasBadge && (
          <PromoContainer marginRight={promoBannerMargin}>
            <BodyCopy
              fontFamily="primary"
              fontSize="fs10"
              fontWeight="black"
              text={item.categoryContent.mainCategory.promoBadge[0].text}
              color="white"
            />
          </PromoContainer>
        )}
        {hasL3 && <ArrowIcon alt="alt" source={Icon} />}
      </PromoAndArrowContainer>
    </MenuContainer>
  );
};

MenuItems.propTypes = {
  hasBadge: bool,
  hasL3: bool,
  item: arrayOf(
    shape({
      categoryContent: shape({
        name: string,
        mainCategory: shape({
          promoBadge: arrayOf(
            shape({
              text: string,
            }),
          ),
        }),
      }),
    }),
  ).isRequired,
  maxWidthItem: string,
  navigate: func.isRequired,
  promoBannerMargin: string,
  route: func.isRequired,
};

MenuItems.defaultProps = {
  hasBadge: false,
  hasL3: false,
  maxWidthItem: '',
  promoBannerMargin: '',
};

export default MenuItems;
