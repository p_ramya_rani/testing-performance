// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';

export const TitleContainer = styled.View`
  background-color: #f3f3f3;
  padding: 14px 0 18px 0;
  font-weight: 900;
`;

export const ItemView = styled.TouchableOpacity`
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: space-between;
  padding: ${props => props.theme.spacing.ELEM_SPACING.LRG} 0
    ${props => props.theme.spacing.ELEM_SPACING.LRG}
    ${props => props.theme.gridDimensions.gridOffsetObj.small}px;
`;

export const ItemViewWithHeading = styled.TouchableOpacity`
  width: 99%;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: flex-start;
  border-radius: 15px;
  margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.SM};
  height: 48px;
  background-color: ${props => (props.accentColor ? props.accentColor : props.theme.colors.WHITE)};
  box-shadow: 2px 2px 2px ${props => props.theme.colors.BOX_SHADOW_LIGHT};
`;

export const ShopBySizeLink = styled.TouchableOpacity`
  margin: ${props => props.theme.spacing.ELEM_SPACING.XS};
  width: 54;
  height: 48px;
  border-radius: 27;
  background-color: ${props => props.theme.colorPalette.white};
  border: 1px solid ${props => props.theme.colorPalette.text.hint};
  align-items: center;
  justify-content: center;
`;

export const ShopBySizeContainer = styled.View`
  margin: ${props => props.theme.spacing.ELEM_SPACING.MED}
    ${props => props.theme.spacing.ELEM_SPACING.XXS};
  flex-direction: row;
  flex-wrap: wrap;
`;

export const PromoContainer = styled.View`
  background: ${props => props.theme.colorPalette.primary.main};
  border-radius: 5px;
  width: 150px;
  height: 30px;
  align-items: center;
  justify-content: center;
`;

export const ArrowIcon = styled.Image`
  width: 10px;
  height: 10px;
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.XXXS};
  margin-left: ${props => props.theme.spacing.ELEM_SPACING.XXS};
`;

const getAdditionalStyles = props => {
  const {isNewReDesignProductTile, newPdpDesignFlag} = props;
  if (isNewReDesignProductTile && !newPdpDesignFlag) {
    return `
    width: 14;
    height: 23;
    margin-bottom: 5;
    `;
  }

  if (newPdpDesignFlag) {
    return `
      width:20px;
      height:20px;
      margin-top:4px;
    `;
  }
  return `
    width:10px;
    height:18px;
  `;
};

export const ArrowBackIconPLP = styled.Image`
  ${getAdditionalStyles}
`;

export const PromoAndArrowContainer = styled.TouchableOpacity`
  align-items: center;
  flex-direction: row;
  padding: 0 ${props => props.theme.gridDimensions.gridOffsetObj.small}px 0 0;
`;

export const RecommendationWrapper = styled.View`
  margin-top: ${props => props.theme.spacing.ELEM_SPACING.XS};
  background: ${props => props.theme.colorPalette.gray[300]};
  margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.XS};
`;

export const NavigationL2Container = styled.View`
  flex: 1;
  padding: 0 ${props => props.theme.spacing.LAYOUT_SPACING.XXS} 0
    ${props => props.theme.spacing.LAYOUT_SPACING.XXS};
  background: ${props => props.theme.colorPalette.gray[300]};
`;

export const StyledScrollView = styled.ScrollView`
  padding-top: 10px;
`;

export const StyledSectionList = styled.SectionList`
  padding-top: 10px;
`;
