// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

export const Container = styled.View`
  padding: ${props => props.theme.spacing.ELEM_SPACING.XS}
    ${props => props.theme.spacing.ELEM_SPACING.XS};
`;

export const ItemView = styled.TouchableOpacity`
  padding: 5px 14px 5px 14px;
  border-radius: 16px;
  border-width: 1px;
  justify-content: center;
  border-color: ${props => props.theme.colorPalette.gray[2300]};
  background-color: ${props =>
    props.isSelected ? props.theme.colorPalette.black : props.theme.colorPalette.white};
  margin-left: 8px;
`;

export const getContentContainerStyle = (listLeftMargin, listRightMargin) => {
  return {
    paddingRight: listRightMargin,
    paddingLeft: listLeftMargin,
  };
};
