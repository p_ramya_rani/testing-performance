// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import {shallow} from 'enzyme';
import * as utils from '@tcp/core/src/utils/utils.app';
import {NavMenuLevel1View} from '../views/NavMenuLevel1.view';
import {navItems, formattedNavItems} from './NavMenuLevel1-mock';

jest.mock('@tcp/core/src/utils/utils');
utils.cropImageUrl = jest.fn();

describe('NavMenuLevel1', () => {
  let component;
  const props = {
    disableGymCrossBrandL1: true,
    navigation: {
      navigate: jest.fn(),
    },
    navigationMenuObj: navItems,
    loadNavigationData: () => {},
  };
  beforeEach(() => {
    component = shallow(<NavMenuLevel1View {...props} />);
  });

  it('should render correctly', () => {
    expect(component).toMatchSnapshot();
  });

  it('formatNavigationArray should format the navigation data correctly', () => {
    utils.cropImageUrl.mockImplementation(imageUrl => {
      return `https://test1.theplace.com/image/upload/dpr_3.0,f_auto/${imageUrl}`;
    });
    const data = component.instance().formatNavigationArray(navItems);
    expect(data).toEqual(formattedNavItems);
  });

  it('getImageUrl should return fallback image if image is not passed', () => {
    const fallBackImgUrl =
      'https://test1.theplace.com/image/upload/dpr_3.0,f_auto/ecom/assets/content/tcp/us/nav-fallback.png';
    utils.cropImageUrl.mockImplementation(imageUrl => {
      return `https://test1.theplace.com/image/upload/dpr_3.0,f_auto/${imageUrl}`;
    });
    const categoryImage = [];
    const imageUrl = component.instance().getImageUrl(categoryImage);
    expect(imageUrl).toEqual(fallBackImgUrl);
  });
});
