// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable sonarjs/cognitive-complexity */
/* istanbul ignore file */
import React, {useState} from 'react';
import {arrayOf, func, shape, string} from 'prop-types';
import {SectionList} from 'react-native';
import {getScreenWidth, getLabelValue, isTCP} from '@tcp/core/src/utils';
import {BodyCopy} from '@tcp/core/src/components/common/atoms';
import {
  visibleCategoryCount,
  findCategoryIdandName,
} from '@tcp/core/src/components/features/browse/ProductListing/container/ProductListing.util';
import Constants from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.constants';
import ShopBySize from '../../ShopBySize';
import shopByConstant from '../../ShopBySize/ShopBySize.constant';
import MenuItem from '../../MenuItems';
import Recommendations from '../../../../../../common/molecules/Recommendations';
import {
  TitleContainer,
  ItemViewWithHeading,
  RecommendationWrapper,
  NavigationL2Container,
  StyledScrollView,
  StyledSectionList,
} from '../NavMenuLevel2.style';
import {navigateFromL2} from '../../../utils';
import SpringAnimation from '../../../../../../common/molecules/Animations/SpringAnimation';

const keyExtractor = (_, index) => index.toString();
const {SHOP_BY_SIZE} = shopByConstant;

const showRegularL2 = (isNestedL2, nestedL2Group, sectionArr) =>
  !isNestedL2 || (nestedL2Group !== '' && sectionArr.length);

const showGroupsL2 = (isNestedL2, nestedL2Group) => isNestedL2 && nestedL2Group === '';

const addShopBySize = (item) => {
  const {
    item: {categoryContent},
  } = item;
  if (!categoryContent) {
    return false;
  }
  let {mainCategory} = categoryContent;
  if (!mainCategory) {
    mainCategory = {
      categoryLayout: [],
    };
  }
  const {categoryLayout} = mainCategory;
  if (categoryLayout && categoryLayout.length) {
    const columnArr = categoryLayout[0].columns;
    const shopBySizeColumn = columnArr.find((colItem) => colItem.shopBySize);
    if (shopBySizeColumn) {
      const [shopBySizeElem] = shopBySizeColumn.shopBySize;
      const {
        text: {text: shopBySizeTitle},
      } = shopBySizeElem;
      return {
        label: shopBySizeTitle,
        order: 0,
        items: shopBySizeColumn.shopBySize,
        isShopBySize: true,
      };
    }
    return false;
  }
  return false;
};

const getL3AndMargin = (subCategories) => {
  let hasL3 = false;
  let promoBannerMargin = 55;

  if (subCategories && subCategories.length && visibleCategoryCount(subCategories)) {
    hasL3 = true;
    promoBannerMargin = 40;
  }
  return {hasL3, promoBannerMargin};
};

const checkIfShopAllAdded = (subCategories, shopAllLabel) => {
  return (
    subCategories &&
    subCategories[0] &&
    subCategories[0].categoryContent &&
    subCategories[0].categoryContent.name &&
    subCategories[0].categoryContent.name !== shopAllLabel
  );
};

const getIsNestedL2 = (l1Id, disableBabyL2Nesting) => {
  return !disableBabyL2Nesting && l1Id === '47504';
};

const getIsNestingNavigation = (isNestedL2, nestedL2Group, title) =>
  isNestedL2 && nestedL2Group === '' && title !== '';

const getCategoryVal = (categoryObj) => categoryObj && categoryObj[0];

/**
 * The Navigation menu level2 is created by this component
 * @param {object} props Props passed from Stack navigator screen and the parent L1
 */
const NavMenuLevel2 = (props) => {
  const {
    navigation: {navigate, getParam},
  } = props;

  const [nestedL2Group, setNestedL2Group] = useState('');

  const accessibilityLabels = getParam('accessibilityLabels');
  const onClickTracking = getParam('trackNavClickAction');
  const l1Title = getParam('l1Title');
  const completeNavObj = getParam('completeNavObj');
  const disableBabyL2Nesting = getParam('disableBabyL2Nesting');
  const labels = getParam('labels');
  const accentColor = getParam('accentColor');
  const newDesignColor = getParam('newDesignColor');

  /**
   * @function renderItem populates the menu item conditionally
   * @param {object} item menu item object
   * @param {object} section contains the section title of the menu item
   */
  const renderItem = ({item, section: {isShopBySize, title}}, isNestedL2) => {
    let maxWidthItem = getScreenWidth() - 60;
    const {promoBannerMargin, hasL3} = getL3AndMargin(item.subCategories);

    let hasBadge = false;

    if (isShopBySize) {
      return <ShopBySize navigate={navigate} links={item.linkList} />;
    }
    const {
      categoryContent: {name, displayToCustomer},
      url,
    } = item;

    const isRedirectUrl = url && url.includes('#redirect');
    if (name.toLowerCase() === 'gymboree') return null;
    if (!displayToCustomer) return null;

    if (
      item.categoryContent.mainCategory &&
      item.categoryContent.mainCategory.promoBadge &&
      item.categoryContent.mainCategory.promoBadge[0].text
    ) {
      hasBadge = true;
      maxWidthItem -= 180;
    }

    const routeHandler = () => {
      if (getIsNestingNavigation(isNestedL2, nestedL2Group, title)) {
        setNestedL2Group(name);
        return null;
      }
      if (isRedirectUrl) {
        const categoryIds = url.split('c?cid=')[1];
        if (categoryIds) {
          const categoryObj = findCategoryIdandName(
            completeNavObj,
            categoryIds.replace('#redirect', ''),
            null,
            true,
          );
          if (getCategoryVal(categoryObj)) {
            const {url: newUrl, categoryContent: newCategoryContent = {}} = categoryObj[0];

            return navigateFromL2(
              navigate,
              [],
              newCategoryContent.name,
              false,
              accessibilityLabels,
              newUrl,
              completeNavObj,
              accentColor,
              newDesignColor,
            );
          }
        }
      } else {
        if (typeof onClickTracking === 'function' && hasL3) {
          const categoryName = l1Title && name && `${l1Title.toLowerCase()}-${name.toLowerCase()}`;
          onClickTracking('navigation_l2', categoryName);
        }
        const {subCategories} = item;
        const {shop_all_label: shopAllLabel = 'Shop All'} = accessibilityLabels;

        if (checkIfShopAllAdded(subCategories, shopAllLabel)) {
          const shopAll = {
            url: item.url,
          };
          shopAll.categoryContent = {
            name: shopAllLabel,
            displayToCustomer: true,
          };
          subCategories.splice(0, 0, shopAll);
        }
        return navigateFromL2(
          navigate,
          subCategories,
          name,
          hasL3,
          accessibilityLabels,
          item.url,
          completeNavObj,
          accentColor,
          newDesignColor,
        );
      }
      return null;
    };

    return (
      <ItemViewWithHeading
        accessibilityRole="button"
        accessibilityLabel={name}
        onPress={routeHandler}
        accentColor={accentColor}>
        <MenuItem
          navigate={navigate}
          route={routeHandler}
          maxWidthItem={maxWidthItem}
          item={item}
          hasBadge={hasBadge}
          promoBannerMargin={promoBannerMargin}
          hasL3={isRedirectUrl ? false : hasL3}
        />
      </ItemViewWithHeading>
    );
  };

  const item = getParam('navigationObj');
  const {
    item: {
      subCategories,
      categoryId,
      categoryContent: {name},
    },
  } = item;
  const shopBySizeElem = addShopBySize(item);
  if (shopBySizeElem) {
    subCategories[SHOP_BY_SIZE] = shopBySizeElem;
  }
  const subCatArr = Object.keys(subCategories).sort((prevGroup, curGroup) => {
    return (
      parseInt(subCategories[prevGroup].order, 10) - parseInt(subCategories[curGroup].order, 10)
    );
  });

  const isNestedL2 = getIsNestedL2(categoryId, disableBabyL2Nesting);
  const sectionArr = [];

  subCatArr.forEach((subcatName) => {
    const subCategoryObj = subCategories[subcatName];
    if (!isNestedL2 || (isNestedL2 && subCategoryObj.label === nestedL2Group)) {
      sectionArr.push({
        data: subCategoryObj.items || [],
        title: subCategoryObj.label,
        order: parseInt(subCategoryObj.order, 10),
        isShopBySize: subCategoryObj.isShopBySize,
        hasSubCategory: subCategoryObj.hasSubCategory,
      });
    }
  });

  const groupArr = subCatArr.map((subcatName) => {
    const subCategoryObj = subCategories[subcatName];
    // If empty group, skip it as it should not appear in the group list
    if (subCategoryObj.label === '' || !subCategoryObj.hasSubCategory) {
      return null;
    }

    const groupItem = [
      {
        categoryContent: {
          name: subCategoryObj.label,
          displayToCustomer: true,
        },
        subCategories: subCategoryObj.items,
        name: subCategoryObj.label,
      },
    ];

    return {
      data: groupItem,
      title: subCategoryObj.label,
      order: 0,
      isShopBySize: false,
      hasSubCategory: subCategoryObj.hasSubCategory,
    };
  });

  const excludedEmptyGroup = groupArr.filter((arrayElem) => arrayElem !== null);

  const combinedArrGroupCat = excludedEmptyGroup.concat(sectionArr);

  const {navigation} = props;

  const recommendationAttributes = {
    variation: 'moduleO',
    navigation,
    page: Constants.RECOMMENDATIONS_PAGES_MAPPING.NAVL2,
    partNumber: name,
    isHeaderAccordion: true,
    categoryName: categoryId,
  };

  const portalDic = {
    Girl: Constants.RECOMMENDATIONS_MBOXNAMES.NAVLEVEL2_Girls,
    'Toddler Girl': Constants.RECOMMENDATIONS_MBOXNAMES.NAVLEVEL2_Toddler_Girls,
    Boy: Constants.RECOMMENDATIONS_MBOXNAMES.NAVLEVEL2_Boys,
    'Toddler Boy': Constants.RECOMMENDATIONS_MBOXNAMES.NAVLEVEL2_Toddler_Boys,
    Baby: Constants.RECOMMENDATIONS_MBOXNAMES.NAVLEVEL2_Baby,
  };

  const portalValue = portalDic[name];

  return (
    <NavigationL2Container>
      <SpringAnimation>
        <StyledScrollView showsVerticalScrollIndicator={false}>
          {showGroupsL2(isNestedL2, nestedL2Group) && (
            <StyledSectionList
              renderItem={(items) => renderItem(items, isNestedL2)}
              keyExtractor={keyExtractor}
              stickySectionHeadersEnabled={false}
              sections={combinedArrGroupCat}
              isNestedL2={isNestedL2}
            />
          )}
          {showRegularL2(isNestedL2, nestedL2Group, sectionArr) && (
            <SectionList
              renderItem={(items) => renderItem(items, isNestedL2)}
              keyExtractor={keyExtractor}
              stickySectionHeadersEnabled={false}
              renderSectionHeader={({section}) => {
                const isSectionHeader =
                  !isNestedL2 && section.title && (section.isShopBySize || section.hasSubCategory);
                return isSectionHeader ? (
                  <TitleContainer>
                    <BodyCopy
                      fontFamily="primary"
                      fontSize="fs20"
                      fontWeight={isTCP() ? 'black' : 'medium'}
                      text={section.title && section.title.toUpperCase()}
                      color="text.primary"
                    />
                  </TitleContainer>
                ) : null;
              }}
              sections={sectionArr}
            />
          )}
          <RecommendationWrapper>
            <Recommendations
              headerLabel={getLabelValue(labels, 'lbl_common_navl2RcommendationTitle')}
              {...recommendationAttributes}
              portalValue={portalValue}
              isNavL2
              margins="0px 12px"
              paddings="0px"
              productImageWidth={130}
              productImageHeight={161}
            />
          </RecommendationWrapper>
        </StyledScrollView>
      </SpringAnimation>
    </NavigationL2Container>
  );
};

NavMenuLevel2.propTypes = {
  labels: shape({}),
  item: shape({
    subCategories: arrayOf({}),
    linkList: arrayOf({}),
    categoryContent: shape({
      name: string,
      displayToCustomer: string,
      mainCategory: shape({}),
    }),
    url: string,
  }),
  navigation: shape({
    getParam: func.isRequired,
    navigate: func,
  }).isRequired,
  section: shape({
    title: string,
  }),
};

NavMenuLevel2.defaultProps = {
  labels: {},
  item: {},
  section: {
    title: '',
  },
};

export default NavMenuLevel2;
