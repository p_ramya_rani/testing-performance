// 9fbef606107a605d69c0edbcd8029e5d 
const COLOR = 'text.primary';
const TEXTALIGN = 'left';
const FONTWEIGHT = 'black';
const FONT_FAMILY = 'primary';
const GYM_FONT_COLOR = 'text.primary';

export default {
  DEFAULT_STYLE: {
    fontFamily: FONT_FAMILY,
    fontSize: 'fs20',
    fontWeight: FONTWEIGHT,
    textAlign: TEXTALIGN,
    color: COLOR,
    letterSpacing: 'ls038',
  },
  GYM_DEFAULT: {
    fontFamily: FONT_FAMILY,
    fontSize: 'fs22',
    fontWeight: 'semibold',
    textAlign: TEXTALIGN,
    color: GYM_FONT_COLOR,
    letterSpacing: 'ls064',
  },
  GYM_DEFAULT_SIZE: {
    fontFamily: FONT_FAMILY,
    fontSize: 'fs16',
    fontWeight: 'regular',
    textAlign: TEXTALIGN,
    color: GYM_FONT_COLOR,
  },
  DIMENSIONS: {
    CARD_HEIGHT: 90,
    CARD_IMAGE_WIDTH: 138,
    BORDER_RADIUS: 16,
    TOP_BANNER_HEIGHT: 160,
    ANDROID_BORDER_RADIUS: 10,
  },
  DIMENSIONS_GYM: {
    CARD_HEIGHT: 120,
    CARD_IMAGE_WIDTH: 172,
    BORDER_RADIUS: 16,
    TOP_BANNER_HEIGHT: 160,
    ANDROID_BORDER_RADIUS: 10,
  },
};
