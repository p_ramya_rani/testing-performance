// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';

export const L1TouchableOpacity = styled.TouchableOpacity`
  align-items: center;
  flex-direction: row;
  height: ${props => props.dimensions.CARD_HEIGHT};
  background-color: ${props =>
    props.accentColor ? `${props.accentColor}` : `${props.theme.colorPalette.white}`};
  margin: ${props => props.theme.spacing.ELEM_SPACING.MED}
    ${props => props.theme.spacing.ELEM_SPACING.MED} 0
    ${props => props.theme.spacing.ELEM_SPACING.MED};
  border-radius: ${props => props.dimensions.BORDER_RADIUS};
  box-shadow: 2px 2px 2px ${props => props.theme.colors.BOX_SHADOW_LIGHT};
  elevation: 3;
  ${props => (props.isLastItemInList ? `margin-bottom: 40px` : '')};
`;

export const L1TextView = styled.View`
  flex-direction: column;
  background-color: ${props => props.theme.colorPalette.white};
  align-items: ${props => (props.isTopBanner ? 'center' : 'flex-start')};
  width: ${props => (props.isGymboree ? '50%' : '60%')};
  justify-content: center;
  padding-left: ${props => props.theme.spacing.ELEM_SPACING.SM};
  padding-right: ${props => props.theme.spacing.ELEM_SPACING.XS};
  ${props =>
    props.isTopBanner
      ? `position: absolute;
    text-align: center;
    width: auto;
    padding: 9px 23px;
    top:${props.isGymboree ? '40%' : '50%'}`
      : `height: ${props.dimensions.CARD_HEIGHT}`};
  margin-left: 14px;
`;

export const FeaturedL1TextView = styled.View`
  border-top-right-radius: 11px;
  border-bottom-right-radius: 11px;
  flex-grow: 1;
  flex-direction: ${props => (props.isSize ? 'column' : 'row')};
  background-color: ${props => props.theme.colors.WHITE_ALPHA};
  width: 60%;
  justify-content: flex-start;
  padding-left: ${props => props.theme.spacing.ELEM_SPACING.SM};
  padding-right: ${props => props.theme.spacing.ELEM_SPACING.XS};
  ${props =>
    props.isTopBanner
      ? `position: absolute;
    text-align: center;
    left: 0;
    padding: 9px 23px;`
      : ''}
`;

export const ContainerList = styled.FlatList`
  padding: ${props => props.theme.spacing.ELEM_SPACING.XS} 0 0 0;
`;

export const NavigationContainer = styled.View`
  background-color: ${props => props.theme.colorPalette.gray[300]};
`;

export const L1TextViewGYM = styled.View`
  align-items: center;
  margin: 10px 0 0 0;
  min-height: 46px;
`;

export const FeaturedCategory = styled.TouchableOpacity`
  align-items: flex-end;
  position: relative;
  margin: ${props => (props.index === 0 ? '0px' : '14px')}
    ${props => props.theme.spacing.ELEM_SPACING.MED} 0
    ${props => props.theme.spacing.ELEM_SPACING.MED};
  border-radius: ${props => props.dimensions.BORDER_RADIUS};
  justify-content: center;
  box-shadow: 2px 2px 2px ${props => props.theme.colors.BOX_SHADOW_LIGHT};
  elevation: 2;
  ${props => (props.accentColor ? `background: ${props.accentColor}` : '')};
`;

export const GymBrandToggleContainer = styled.View`
  min-height: 80px !important;
  width: 100%;
  margin: auto 0 10px 0;
`;

export const SMSTapToJoinContainer = styled.View`
  min-height: 80px !important;
  overflow: hidden;
  justify-content: center;
  background-color: ${props => props.theme.colorPalette.white};
  border-radius: ${props => props.theme.spacing.ELEM_SPACING.MED};
  text-align: center;
  align-items: center;
  ${props =>
    props.isGym
      ? `margin: ${props.theme.spacing.ELEM_SPACING.MED} ${props.theme.spacing.ELEM_SPACING.MED} ${
          props.theme.spacing.APP_LAYOUT_SPACING.SM
        }`
      : `margin: -${props.theme.spacing.APP_LAYOUT_SPACING.XXS} ${
          props.theme.spacing.APP_LAYOUT_SPACING.XS
        } ${props.theme.spacing.FORM_FIELD_HEIGHT}`};
`;
