// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import {PropTypes} from 'prop-types';
import {BodyCopy} from '@tcp/core/src/components/common/atoms';
import {getLabelValue} from '@tcp/core/src/utils';
import {getIcon} from '../../../../../../../utils/utils';
import {
  LogoImage,
  ArrowImage,
  Container,
  LeftSection,
  RightSection,
} from '../GymboreeCrossBrand.style';

const rightArrowImg = require('../../../../../../../assets/images/caret-black-right.png');

const GymboreeCrossBrand = props => {
  const {switchToGymboree, labels} = props;
  return (
    <Container onPress={switchToGymboree}>
      <RightSection>
        <ArrowImage source={rightArrowImg} />
      </RightSection>
      <LeftSection>
        <BodyCopy
          fontFamily="primary"
          fontSize="fs16"
          textAlign="left"
          text={getLabelValue(labels, 'lbl_common_shopAt')}
          color="text.primary"
        />
        <LogoImage source={getIcon('gymboree-logo-l1')} alt="" />
      </LeftSection>
    </Container>
  );
};

GymboreeCrossBrand.propTypes = {
  switchToGymboree: PropTypes.func,
  labels: PropTypes.shape({}),
};

GymboreeCrossBrand.defaultProps = {
  switchToGymboree: () => {},
  labels: {},
};

export default GymboreeCrossBrand;
