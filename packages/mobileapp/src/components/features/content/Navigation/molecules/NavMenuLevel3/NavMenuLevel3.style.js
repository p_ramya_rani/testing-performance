// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

export const StyledView = styled.View`
  background: ${props => props.theme.colorPalette.gray[300]};
  flex: 1;
  padding: 0 ${props => props.theme.spacing.ELEM_SPACING.MED};
`;

export const ItemViewWithHeading = styled.TouchableOpacity`
  width: 99%;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: flex-start;
  border-radius: 15px;
  margin-bottom: ${props => props.theme.spacing.ELEM_SPACING.SM};
  height: 48px;
  background-color: ${props => (props.accentColor ? props.accentColor : props.theme.colors.WHITE)};
  box-shadow: 2px 2px 2px ${props => props.theme.colors.BOX_SHADOW_LIGHT};
  ${props => (props.index === 0 ? `margin-top:  ${props.theme.spacing.ELEM_SPACING.LRG}` : '')};
`;
