// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import {PropTypes} from 'prop-types';
import {FlatList} from 'react-native';
import {BodyCopy} from '@tcp/core/src/components/common/atoms';
import {findCategoryIdandName} from '@tcp/core/src/components/features/browse/ProductListing/container/ProductListing.util';
import {Container, ItemView, getContentContainerStyle} from '../NavigationCarousel.style';

/**
 * @function renderItem populates the menu item conditionally
 * @param {object} item menu item object
 * @param {object} section contains the section title of the menu item
 */
const renderItem = (navigate, getParam, completeNavObj, title, navigation) => (listProps) => {
  const {item, index} = listProps;
  let {url, categoryContent = {}} = item;
  const isRedirectUrl = url && url.includes('#redirect');
  const {name, displayToCustomer} = categoryContent;

  if (!displayToCustomer) return null;

  const navigateToNextScreen = () => {
    if (isRedirectUrl) {
      const categoryIds = url && url.split('c?cid=')[1];
      const categoryObj = findCategoryIdandName(
        completeNavObj,
        categoryIds.replace('#redirect', ''),
        null,
        true,
      );
      const {url: newUrl, categoryContent: newCategoryContent = {}} = categoryObj[0];

      url = newUrl;
      categoryContent = newCategoryContent;
    }
    if (url.includes('-outfit')) {
      // Navigate to outfit listing for outfits
      const categoryIds = url && url.split('c?cid=');
      return navigate('OutfitListing', {
        title: name,
        url,
        outfitPath: (categoryIds && categoryIds.length > 1 && categoryIds[1]) || '',
      });
    }

    if (navigation) {
      navigation.setParams({
        totalProductCount: '',
      });
    }

    const subCategories = getParam('navigationObj');
    return navigate('ProductListing', {
      navigationObj: subCategories,
      url,
      title: name,
      reset: true,
    });
  };

  return (
    <ItemView
      accessibilityRole="link"
      accessibilityLabel={item.categoryContent.name}
      testID={`L3_text_links_${index}`}
      isSelected={title === item.categoryContent.name}
      onPress={() => navigateToNextScreen()}>
      <BodyCopy
        fontFamily="secondary"
        fontSize="fs12"
        fontWeight="semibold"
        text={item.categoryContent.name}
        color={title === item.categoryContent.name ? 'white' : 'gray.2300'}
        textAlign="left"
      />
    </ItemView>
  );
};

const getSelectedIndex = (title, subCategories) => {
  return subCategories.findIndex((obj) => obj.categoryContent.name === title);
};

/**
 * @param {object} props Props passed from Stack navigator screen and the parent L1
 */
const NavigationCarousel = (props) => {
  const {
    navigation: {navigate, getParam},
    title,
    listLeftMargin,
    listRightMargin,
  } = props;

  let flatList = null;

  const subCategories = getParam('navigationObj');
  const completeNavObj = getParam('completeNavObj');
  return (
    <Container>
      <FlatList
        ref={(list) => {
          flatList = list;
        }}
        viewabilityConfig={{
          itemVisiblePercentThreshold: 50,
        }}
        contentContainerStyle={getContentContainerStyle(listLeftMargin, listRightMargin)}
        scrollToOverflowEnabled
        initialNumToRender={subCategories.length}
        initialScrollIndex={getSelectedIndex(title, subCategories)}
        onScrollToIndexFailed={(info) => {
          if (flatList !== null) {
            setTimeout(() => flatList.scrollToIndex({index: info.index, animated: true}), 100);
          }
        }}
        refreshing={false}
        data={subCategories}
        horizontal
        showsHorizontalScrollIndicator={false}
        listKey={(_, index) => index.toString()}
        renderItem={renderItem(navigate, getParam, completeNavObj, title, props.navigation)}
      />
    </Container>
  );
};

NavigationCarousel.propTypes = {
  navigation: PropTypes.shape({
    getParam: PropTypes.func.isRequired,
    navigate: PropTypes.func,
    goBack: PropTypes.func,
  }).isRequired,
  title: PropTypes.string.isRequired,
  listLeftMargin: PropTypes.number,
  listRightMargin: PropTypes.number,
};
NavigationCarousel.defaultProps = {
  listLeftMargin: 0,
  listRightMargin: 0,
};

export default NavigationCarousel;
