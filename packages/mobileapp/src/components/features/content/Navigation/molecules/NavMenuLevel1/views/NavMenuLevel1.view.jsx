/* eslint-disable max-lines */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import {PropTypes} from 'prop-types';
import {cropImageUrl, getDefaultAccentColor, isAndroid} from '@tcp/core/src/utils';
import {isGymboree} from '@tcp/core/src/utils/index.native';
import {Image, BodyCopy} from '@tcp/core/src/components/common/atoms';
import InitialPropsHOC from '@tcp/core/src/components/common/hoc/InitialPropsHOC/InitialPropsHOC.native';
import SMSTapToJoin from '@tcp/core/src/components/common/molecules/SMSTapToJoin';
import GymboreeCrossBrand from '../../GymboreeCrossBrand';
import {APP_TYPE} from '../../../../../../common/hoc/ThemeWrapper.constants';

import {
  L1TouchableOpacity,
  L1TextView,
  ContainerList,
  FeaturedCategory,
  NavigationContainer,
  GymBrandToggleContainer,
  FeaturedL1TextView,
  SMSTapToJoinContainer,
} from '../NavMenuLevel1.style';
import config from '../config';
import NavMenuLevel1Constants from '../NavMenuLevel1.constants';
import ProfileList from '../../../../../shopByProfile/ListProfile/organisms/ProfileList/views/ProfileList';

const keyExtractor = (_, index) => index.toString();
const gymBrand = 'GymBrand';
const TAP_TO_JOIN = 'tapToJoin';

const {DEFAULT_STYLE, GYM_DEFAULT, GYM_DEFAULT_SIZE, DIMENSIONS, DIMENSIONS_GYM} = config;

/**
 * @function NavMenuLevel1 The Navigation menu level1 is created by this component
 * @param {object} props Props passed from Stack navigator screen
 */
class NavMenuLevel1 extends React.PureComponent {
  constructor(props) {
    super(props);
    this.isGymApp = isGymboree();
    this.fallBackImage = `ecom/assets/content/${this.isGymApp ? 'gym' : 'tcp'}/us/nav-fallback.png`;
    this.cssDimensions = this.isGymApp ? DIMENSIONS_GYM : DIMENSIONS;
    this.defaultAccentColor = getDefaultAccentColor(this.isGymApp);
    this.isAndroidDevice = isAndroid();
  }

  componentDidMount() {
    const {userId, getProfilesSBP} = this.props;
    if (this.shouldShowSBPContent()) {
      getProfilesSBP(userId);
    }
  }

  /**
   * @function isGiftCardLink
   * This function will be used to find whether its gift card link or not.
   */
  isGiftCardLink = ({item}, name = '') => {
    const nameToCheck = name.toUpperCase().replace(/\s/g, '');
    return (
      NavMenuLevel1Constants.GIFT_CARD_NAME.includes(nameToCheck) ||
      NavMenuLevel1Constants.GIFT_CARD_CATEGORY_ID === item.categoryId
    );
  };

  trackNavClick = (key, itemName) => {
    const {trackNavigationClick} = this.props;
    const analyticsData = {pageNavigationText: itemName};
    const clickData = {name: key, module: 'global'};
    trackNavigationClick({analyticsData, clickData});
  };

  /**
   * @function ShowL2Navigation populates the L2 menu for the L1 link that has been clicked
   * @param {object} item Details of the L1 menu item that has been clicked
   */
  showL2Navigation = (item, name, accentColor) => {
    const {
      navigation: {navigate},
      navigationMenuObj,
      accessibilityLabels,
      disableBabyL2Nesting,
      labels,
      isNewReDesignProductTile,
    } = this.props;

    const l2AccentColor = accentColor || this.defaultAccentColor;

    this.trackNavClick('navigation_l1', name);
    if (this.isGiftCardLink(item, name)) {
      return navigate('GiftCardPage', {
        title: name,
        accentColor: l2AccentColor,
      });
    }
    return navigate('NavMenuLevel2', {
      navigationObj: item,
      l1Title: name,
      accentColor: l2AccentColor,
      accessibilityLabels,
      trackNavClickAction: this.trackNavClick,
      completeNavObj: navigationMenuObj,
      disableBabyL2Nesting,
      labels,
      newDesignColor: isNewReDesignProductTile,
    });
  };

  getSizeInfo = (mainCategory) => {
    let size = '';
    const sizesRange = mainCategory && mainCategory.sizesRange && mainCategory.sizesRange[0];
    if (sizesRange) {
      size = sizesRange.text;
    }

    return size;
  };

  /**
   * @function renderTextBlock populates the L1 menu content
   * @param {object} item Details of the L1 menu item that has been clicked
   */
  renderTextBlock = (catName, catSize, mainCategory, isTopBanner) => {
    const capsCatName = catName && catName.toUpperCase();

    return (
      <L1TextView isSize={catSize} isTopBanner={isTopBanner} dimensions={this.cssDimensions}>
        <BodyCopy numberOfLines={catSize ? 2 : 3} {...DEFAULT_STYLE} text={capsCatName} />
        {catSize ? <BodyCopy fontFamily="primary" fontSize="fs16" text={catSize} /> : null}
      </L1TextView>
    );
  };

  renderTextBlockFeaturedCategory = (catName, catSize, mainCategory, isTopBanner) => {
    const capsCatName = catName && catName.toUpperCase();
    return (
      <FeaturedL1TextView isSize={catSize} isTopBanner={isTopBanner}>
        <BodyCopy
          numberOfLines={catSize ? 2 : 3}
          {...DEFAULT_STYLE}
          text={capsCatName}
          fontSize="fs20"
        />
        {catSize ? <BodyCopy fontFamily="primary" fontSize="fs16" text={catSize} /> : null}
      </FeaturedL1TextView>
    );
  };

  renderText = (name, size, mainCategory, isTopBanner) => {
    return this.isGymApp
      ? this.renderTextBlockGym(name, size, mainCategory, isTopBanner)
      : this.renderTextBlock(name, size, mainCategory, isTopBanner);
  };

  renderTextFeaturedCategory = (name, size, mainCategory, isTopBanner) => {
    return this.isGymApp
      ? this.renderTextFeaturedCategoryBlockGym(name, size, mainCategory, isTopBanner)
      : this.renderTextBlockFeaturedCategory(name, size, mainCategory, isTopBanner);
  };

  L1TouchableWithImage = (item, categoryImage, mainCategory, isLastItemInList) => {
    const {
      item: {
        categoryContent: {name},
      },
    } = item;
    const {attributes, imageUrl} = mainCategory;
    const size = this.getSizeInfo(mainCategory);
    const imageStyle = {
      borderTopRightRadius: this.cssDimensions.BORDER_RADIUS,
      borderBottomRightRadius: this.cssDimensions.BORDER_RADIUS,
      flex: 1,
    };

    return (
      <L1TouchableOpacity
        accessibilityRole="button"
        accessibilityLabel={name}
        onPress={() => this.showL2Navigation(item, name, attributes.accentColor)}
        dimensions={this.cssDimensions}
        accentColor={attributes.accentColor}
        isLastItemInList={isLastItemInList}>
        {this.renderText(name, size, mainCategory, false)}

        <Image
          alt={categoryImage && categoryImage[0].alt}
          resizeMode="cover"
          source={{
            uri: imageUrl,
          }}
          height={this.cssDimensions.CARD_HEIGHT}
          style={imageStyle}
        />
      </L1TouchableOpacity>
    );
  };

  getImageUrl = (categoryImage) => {
    let imageUrl = {};
    if (categoryImage && categoryImage[0]) {
      imageUrl = cropImageUrl(categoryImage[0].url, categoryImage[0].crop_m);
    } else {
      imageUrl = cropImageUrl(this.fallBackImage);
    }
    return imageUrl;
  };

  getBorderRadiusFeatureCategory = () => {
    let borderRadiusObj = {};
    if (this.isAndroidDevice) {
      if (this.isGymApp) {
        borderRadiusObj = {borderRadius: this.cssDimensions.ANDROID_BORDER_RADIUS};
      } else {
        borderRadiusObj = {
          borderTopRightRadius: this.cssDimensions.ANDROID_BORDER_RADIUS,
          borderBottomRightRadius: this.cssDimensions.ANDROID_BORDER_RADIUS,
        };
      }
    } else if (this.isGymApp) {
      borderRadiusObj = {borderRadius: this.cssDimensions.BORDER_RADIUS};
    } else {
      borderRadiusObj = {
        borderTopRightRadius: this.cssDimensions.BORDER_RADIUS,
        borderBottomRightRadius: this.cssDimensions.BORDER_RADIUS,
      };
    }
    return borderRadiusObj;
  };

  renderFeaturedCategory = (item, mainCategory, index) => {
    const {
      item: {
        categoryContent: {name},
      },
    } = item;

    const {attributes, imageUrl} = mainCategory;
    const size = this.getSizeInfo(mainCategory);

    const imageStyle = {
      width: this.isGymApp ? '100%' : '70%',
      height: this.cssDimensions.TOP_BANNER_HEIGHT,
      ...this.getBorderRadiusFeatureCategory(),
    };

    return (
      <FeaturedCategory
        accessibilityRole="button"
        accessibilityLabel={name}
        accentColor={attributes.accentColor || this.defaultAccentColor}
        onPress={() => this.showL2Navigation(item, name, attributes.accentColor)}
        dimensions={this.cssDimensions}
        index={index}>
        <Image
          resizeMode="stretch"
          source={{
            uri: imageUrl,
          }}
          style={imageStyle}
        />
        {this.renderTextFeaturedCategory(name, size, mainCategory, true)}
      </FeaturedCategory>
    );
  };

  /**
   * @function renderItem populates the L1 menu item from the data passed to it
   * @param {object} item Details of the L1 menu item passed from the loop
   */
  renderItem = (item, navLength) => {
    let {
      item: {
        categoryContent: {mainCategory},
      },
    } = item;

    const {
      item: {categoryId},
      index,
    } = item;
    const isLastItemInList = index === navLength - 1;

    const {labels, navigation} = this.props;

    if (categoryId === gymBrand) {
      return (
        <GymBrandToggleContainer>
          <GymboreeCrossBrand labels={labels} switchToGymboree={this.switchToGymboree} />
        </GymBrandToggleContainer>
      );
    }

    if (categoryId === TAP_TO_JOIN) {
      return (
        <SMSTapToJoinContainer isGym={isGymboree()}>
          <SMSTapToJoin navigation={navigation} smsFromPage="footer" />
        </SMSTapToJoinContainer>
      );
    }

    if (!mainCategory) {
      mainCategory = {
        categoryImage: [],
      };
    }
    const {categoryImage, attributes} = mainCategory;

    if (attributes.pinToTop) return this.renderFeaturedCategory(item, mainCategory, index);

    return this.L1TouchableWithImage(item, categoryImage, mainCategory, isLastItemInList);
  };

  /**
   * @function renderTextBlock populates the L1 menu content
   * @param {object} item Details of the L1 menu item that has been clicked
   */
  renderTextBlockGym = (catName, catSize, mainCategory, isTopBanner) => {
    return (
      <L1TextView
        isSize={catSize}
        isTopBanner={isTopBanner}
        dimensions={this.cssDimensions}
        isGymboree>
        <BodyCopy numberOfLines={catSize ? 2 : 3} {...GYM_DEFAULT} text={catName} />
        {catSize ? (
          <BodyCopy
            {...GYM_DEFAULT_SIZE}
            text={catSize}
            numberOfLines={2}
            lineHeight={1.25}
            margin="5px 0 0 0"
          />
        ) : null}
      </L1TextView>
    );
  };

  renderTextFeaturedCategoryBlockGym = (catName, catSize, mainCategory, isTopBanner) => {
    return (
      <FeaturedL1TextView isSize={catSize} isTopBanner={isTopBanner} isGymboree>
        <BodyCopy numberOfLines={catSize ? 2 : 3} {...GYM_DEFAULT} text={catName} />
        {catSize ? (
          <BodyCopy
            {...GYM_DEFAULT_SIZE}
            text={catSize}
            numberOfLines={2}
            lineHeight={1.25}
            margin="5px 0 0 0"
          />
        ) : null}
      </FeaturedL1TextView>
    );
  };

  getAttributeList = (cmsAttributeSet) => {
    const attributeList = {};
    cmsAttributeSet.forEach((attribute) => {
      attributeList[attribute.key] = attribute.val;
    });
    return attributeList;
  };

  isFeaturedCategory = (attributeList) =>
    attributeList.pinToTop && attributeList.pinToTop === 'true';

  formatNavigationArray = (navigationMenuObj) => {
    let navigationArray = [];
    const featuredCategoryArray = [];
    const unFeaturedCategoryArray = [];
    let gymBrandArray = [];

    navigationMenuObj.forEach((item) => {
      const {
        categoryContent,
        categoryContent: {displayToCustomer},
      } = item;

      let {mainCategory} = categoryContent;

      if (!mainCategory) {
        mainCategory = {
          categoryImage: [],
        };
      }

      if (displayToCustomer) {
        const {set} = mainCategory;
        let attributeList = {};
        let isFeaturedCategory = false;

        mainCategory.attributes = {};

        if (set) {
          attributeList = this.getAttributeList(set);
          isFeaturedCategory = this.isFeaturedCategory(attributeList);
        }

        mainCategory.attributes = attributeList;
        mainCategory.imageUrl = this.getImageUrl(mainCategory.categoryImage);

        if (isFeaturedCategory) {
          featuredCategoryArray.push(item);
        } else {
          unFeaturedCategoryArray.push(item);
        }
      }
    });

    const {disableGymCrossBrandL1} = this.props;

    if (!this.isGymApp && !disableGymCrossBrandL1) {
      gymBrandArray = [
        {
          categoryContent: {
            mainCategory: {
              categoryImage: [
                {
                  url: '',
                },
              ],
              attributes: {},
            },
          },
          categoryId: gymBrand,
        },
      ];
      navigationArray = [...featuredCategoryArray, ...unFeaturedCategoryArray, ...gymBrandArray];
    } else {
      navigationArray = [...featuredCategoryArray, ...unFeaturedCategoryArray];
    }

    const tapToJoinArray = {
      categoryContent: {
        mainCategory: {
          categoryImage: [
            {
              url: '',
            },
          ],
          attributes: {},
        },
      },
      categoryId: TAP_TO_JOIN,
    };
    navigationArray.push(tapToJoinArray);

    return navigationArray;
  };

  switchToGymboree = () => {
    const {updateAppTypeHandler} = this.props;
    if (updateAppTypeHandler) {
      updateAppTypeHandler(APP_TYPE.GYMBOREE);
    }
  };

  shouldShowSBPContent = () => {
    const {isUserLoggedIn, isSBPEnabledShopMenu, isSbpGymEnabled} = this.props;
    return isUserLoggedIn && isSBPEnabledShopMenu && !isSbpGymEnabled;
  };

  renderSBPTab = () => {
    const {navigation} = this.props;
    if (this.shouldShowSBPContent()) return <ProfileList navigation={navigation} />;
    return null;
  };

  render() {
    const {navigationMenuObj = []} = this.props;
    const navigationMenu = this.formatNavigationArray(navigationMenuObj);
    const navLength = navigationMenu.length;
    return (
      <NavigationContainer>
        <ContainerList
          ListHeaderComponent={this.renderSBPTab}
          data={navigationMenu}
          keyExtractor={keyExtractor}
          renderItem={(item) => this.renderItem(item, navLength)}
          showsVerticalScrollIndicator={false}
        />
      </NavigationContainer>
    );
  }
}

NavMenuLevel1.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
  accessibilityLabels: PropTypes.shape({}),
  labels: PropTypes.shape({}),
  navigationMenuObj: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  trackNavigationClick: PropTypes.func,
  disableGymCrossBrandL1: PropTypes.bool,
  updateAppTypeHandler: PropTypes.func,
  disableBabyL2Nesting: PropTypes.bool,
  isNewReDesignProductTile: PropTypes.bool,
  isUserLoggedIn: PropTypes.bool,
  isSBPEnabledShopMenu: PropTypes.bool,
  isSbpGymEnabled: PropTypes.bool,
  getProfilesSBP: PropTypes.func,
  userId: PropTypes.string,
};

NavMenuLevel1.defaultProps = {
  accessibilityLabels: {},
  labels: {},
  trackNavigationClick: () => {},
  disableGymCrossBrandL1: false,
  updateAppTypeHandler: () => {},
  disableBabyL2Nesting: false,
  isNewReDesignProductTile: false,
  isUserLoggedIn: false,
  isSBPEnabledShopMenu: false,
  isSbpGymEnabled: false,
  getProfilesSBP: () => {},
  userId: '',
};

export {NavMenuLevel1 as NavMenuLevel1View};
export default InitialPropsHOC(NavMenuLevel1);
