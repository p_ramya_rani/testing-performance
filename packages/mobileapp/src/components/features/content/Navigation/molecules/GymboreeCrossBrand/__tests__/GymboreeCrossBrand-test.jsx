// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import {shallow} from 'enzyme';

import GymboreeCrossBrand from '../views/GymboreeCrossBrand.view';

describe('NavMenuLevel1', () => {
  let component;

  beforeEach(() => {
    const props = {
      labels: {},
      switchToGymboree: () => {},
    };
    component = shallow(<GymboreeCrossBrand {...props} />);
  });

  it('should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
});
