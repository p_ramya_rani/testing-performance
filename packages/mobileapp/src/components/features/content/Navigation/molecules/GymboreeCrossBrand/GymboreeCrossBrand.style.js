// 9fbef606107a605d69c0edbcd8029e5d 
import styled from 'styled-components/native';

export const Container = styled.TouchableOpacity`
  display: flex;
  flex-direction: row-reverse;
  padding: ${props => props.theme.spacing.ELEM_SPACING.XS} 0px;
  border-radius: 16px;
  margin: ${props => props.theme.spacing.APP_LAYOUT_SPACING.XS};
  background-color: ${props => props.theme.colorPalette.white};
`;

export const LeftSection = styled.View`
  margin: 0px ${props => props.theme.spacing.LAYOUT_SPACING.LRG} 0px 0px;
  display: flex;
  flex-direction: row;
  align-items: center;
`;

export const RightSection = styled.View`
  margin: 0px ${props => props.theme.spacing.LAYOUT_SPACING.XS} 0px 0px;
  justify-content: center;
`;

export const ArrowImage = styled.Image`
  width: 6px;
  height: 14px;
`;
export const LogoImage = styled.Image`
  width: 116px;
  height: 28px;
`;
