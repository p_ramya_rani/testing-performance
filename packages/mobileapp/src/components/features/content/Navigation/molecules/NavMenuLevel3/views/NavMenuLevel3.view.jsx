// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import {PropTypes} from 'prop-types';
import {FlatList} from 'react-native';
import {getScreenWidth} from '@tcp/core/src/utils';
import {findCategoryIdandName} from '@tcp/core/src/components/features/browse/ProductListing/container/ProductListing.util';
import MenuItem from '../../MenuItems';
import {StyledView, ItemViewWithHeading} from '../NavMenuLevel3.style';
import SpringAnimation from '../../../../../../common/molecules/Animations/SpringAnimation';

const keyExtractor = (_, index) => index.toString();

/**
 * @function renderItem populates the menu item conditionally
 * @param {object} item menu item object
 * @param {object} section contains the section title of the menu item
 */
const renderItem =
  (navigate, getParam, completeNavObj, accentColor, newDesignColor) => (listProps) => {
    const maxWidthItem = getScreenWidth() - 60;

    const {item, index} = listProps;
    let {url, categoryContent = {}} = item;

    const isRedirectUrl = url && url.includes('#redirect');

    const {name, displayToCustomer} = categoryContent;

    if (!displayToCustomer) return null;

    const navigateToNextScreen = () => {
      if (url && isRedirectUrl) {
        const categoryIds = url && url.split('c?cid=')[1];
        const categoryObj = findCategoryIdandName(
          completeNavObj,
          categoryIds.replace('#redirect', ''),
          null,
          true,
        );
        const {url: newUrl, categoryContent: newCategoryContent = {}} =
          categoryObj && categoryObj.length > 0 && categoryObj[0];

        url = newUrl;
        categoryContent = newCategoryContent;
      }
      if (url && url.includes('-outfit')) {
        // Navigate to outfit listing for outfits
        const categoryIds = url && url.split('c?cid=');
        return navigate('OutfitListing', {
          title: name,
          url,
          outfitPath: (categoryIds && categoryIds.length > 1 && categoryIds[1]) || '',
        });
      }

      const subCategories = getParam('navigationObj');
      return navigate('ProductListing', {
        navigationObj: subCategories,
        completeNavObj,
        url,
        title: name,
        reset: true,
        newDesignColor,
      });
    };

    return (
      <ItemViewWithHeading
        accessibilityRole="link"
        accessibilityLabel={item.categoryContent.name}
        testID={`L3_text_links_${index}`}
        onPress={() => navigateToNextScreen()}
        accentColor={accentColor}
        index={index}>
        <MenuItem
          navigate={navigate}
          route={() =>
            navigate('ProductListing', {
              url,
            })
          }
          maxWidthItem={maxWidthItem}
          item={item}
          hasBadge={false}
          promoBannerMargin={0}
          hasL3={false}
        />
      </ItemViewWithHeading>
    );
  };

/**
 * The Navigation menu level3 is created by this component
 * @param {object} props Props passed from Stack navigator screen and the parent L1
 */
const NavMenuLevel3 = (props) => {
  const {
    navigation: {navigate, getParam},
  } = props;

  const subCategories = getParam('navigationObj');
  const completeNavObj = getParam('completeNavObj');
  const accentColor = getParam('accentColor');
  const newDesignColor = getParam('newDesignColor');

  return (
    <StyledView>
      <SpringAnimation>
        <FlatList
          renderItem={renderItem(navigate, getParam, completeNavObj, accentColor, newDesignColor)}
          keyExtractor={keyExtractor}
          data={subCategories}
          showsVerticalScrollIndicator={false}
        />
      </SpringAnimation>
    </StyledView>
  );
};

NavMenuLevel3.propTypes = {
  navigation: PropTypes.shape({
    getParam: PropTypes.func.isRequired,
    navigate: PropTypes.func,
    accentColor: PropTypes.string,
  }).isRequired,
};

export default NavMenuLevel3;
