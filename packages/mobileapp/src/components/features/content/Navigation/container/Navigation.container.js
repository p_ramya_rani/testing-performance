// 9fbef606107a605d69c0edbcd8029e5d
import {connect} from 'react-redux';
import {trackClick, setClickAnalyticsData} from '@tcp/core/src/analytics/actions';
import {
  getDisableGymCrossBrandL1,
  getIsBabyL2NestingDisabled,
  getIsNewReDesignEnabled,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import {
  getUserId,
  getUserLoggedInState,
} from '@tcp/core/src/components/features/account/User/container/User.selectors';
import NavMenuLevel1View from '../molecules/NavMenuLevel1';
import {THEME_WRAPPER_REDUCER_KEY} from '../../../../common/hoc/ThemeWrapper.constants';
import updateAppType from '../../../../common/hoc/ThemeWrapper.actions';
import {getReorderedNavData} from './Navigation.selectors';
import {
  isSbpGymEnabled,
  getSBPEnabledShopMenu,
} from '../../../shopByProfile/ShopByProfile.selector';
import {getSBPProfilesAction} from '../../../shopByProfile/ShopByProfileAPI/ShopByProfile.actions';

const mapStateToProps = (state) => {
  return {
    navigationMenuObj: getReorderedNavData(state),
    accessibilityLabels:
      (state.Labels && state.Labels.global && state.Labels.global.accessibility) || {},
    labels: (state.Labels && state.Labels.global && state.Labels.global.CommonBrowseLabels) || {},
    appType: state[THEME_WRAPPER_REDUCER_KEY].get('APP_TYPE'),
    disableGymCrossBrandL1: getDisableGymCrossBrandL1(state),
    disableBabyL2Nesting: getIsBabyL2NestingDisabled(state),
    isNewReDesignProductTile: getIsNewReDesignEnabled(state),
    isUserLoggedIn: getUserLoggedInState(state),
    isSBPEnabledShopMenu: getSBPEnabledShopMenu(state),
    isSbpGymEnabled: isSbpGymEnabled(state),
    userId: getUserId(state),
  };
};

function mapDispatchToProps(dispatch) {
  return {
    trackNavigationClick: (payload) => {
      const {analyticsData, clickData} = payload;

      dispatch(setClickAnalyticsData(analyticsData));
      dispatch(trackClick(clickData));
    },
    updateAppTypeHandler: (appType) => {
      dispatch(updateAppType(appType));
    },
    getProfilesSBP: (payload) => {
      dispatch(getSBPProfilesAction(payload));
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(NavMenuLevel1View);
