// 9fbef606107a605d69c0edbcd8029e5d
import {getABtestFromState} from '@tcp/core/src/utils';
import {createSelector} from 'reselect';
import {reOrderNavArray} from './navigation.util';

const getNavigationData = state => {
  return state.Navigation;
};

export const getl1OrderAbTest = state => {
  return getABtestFromState(state.AbTest).l1Order;
};
export const getIsL1OrderingEnabled = state => {
  return getABtestFromState(state.AbTest).isL1OrderingEnabled;
};

export const getReorderedNavData = createSelector(
  getl1OrderAbTest,
  getNavigationData,
  getIsL1OrderingEnabled,
  (l1OrderABTest, navigationState, isL1OrderingEnabled) => {
    const {navigationData = []} = navigationState;
    let navData = [];
    navigationData.forEach(navItem => {
      navData.push(navItem);
    });
    let variationPresent = false;
    let noOfMatches = 0;
    for (let i = 0; i < navData.length; i += 1) {
      const indexOfCurrent = l1OrderABTest?.indexOf(navData?.[i]?.categoryContent?.name);
      if (indexOfCurrent !== -1) {
        noOfMatches += 1;
      }
    }
    if (noOfMatches === l1OrderABTest?.length) {
      variationPresent = true;
    }
    if (isL1OrderingEnabled && l1OrderABTest && variationPresent) {
      navData = reOrderNavArray(navData, l1OrderABTest);
    }
    return navData;
  },
);

export default {
  getNavigationData,
};
