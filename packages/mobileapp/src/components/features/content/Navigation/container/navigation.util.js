/**
 * @function shiftArrayElement
 * @param {*} arr
 * @param {*} sourceIndex
 * @param {*} destinationIndex
 * @returns array with element shifted from sourceIndex to destinationIndex
 */
export const shiftArrayElement = (arr, sourceIndex, destinationIndex) => {
  const tmp = arr[sourceIndex];
  arr.splice(sourceIndex, 1);
  arr.splice(destinationIndex, 0, tmp);
  return arr;
};

/**
 * @function reOrderNavArray
 * @param {*} arr
 * @param {*} variation
 * @returns reordered navigationData(arr) based on variation required
 */
export const reOrderNavArray = (arr, variation = []) => {
  let navigationData = arr;
  let startingPoint = 0;
  for (let i = 0; i < navigationData.length; i += 1) {
    const indexOfCurrent = variation.indexOf(navigationData?.[i]?.categoryContent?.name);
    if (indexOfCurrent !== -1) {
      startingPoint = i;
      navigationData = shiftArrayElement(navigationData, i, startingPoint + indexOfCurrent);
      break;
    }
  }
  let isReordered = false;
  while (!isReordered) {
    for (let i = startingPoint; i < navigationData.length; i += 1) {
      const indexOfCurrent = variation.indexOf(navigationData?.[i]?.categoryContent?.name);
      if (indexOfCurrent !== -1 && startingPoint + indexOfCurrent !== i) {
        navigationData = shiftArrayElement(navigationData, i, startingPoint + indexOfCurrent);
        break;
      }
    }
    const updatedRange = navigationData.slice(startingPoint, startingPoint + variation.length);
    isReordered = true;
    for (let i = 0; i < variation.length; i += 1) {
      if (updatedRange?.[i]?.categoryContent?.name !== variation[i]) {
        isReordered = false;
      }
    }
  }
  return navigationData;
};
