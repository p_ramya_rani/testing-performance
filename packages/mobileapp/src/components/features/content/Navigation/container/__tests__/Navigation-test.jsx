// 9fbef606107a605d69c0edbcd8029e5d
import mock from '@tcp/core/src/services/abstractors/bootstrap/navigation/mock';
import {reOrderNavArray} from '../navigation.util';

describe('Navigation component', () => {
  it('reorder Navigation selector test 1', () => {
    const variation = ['Toddler Boy', 'Baby', 'Girl', 'Boy', 'Toddler Girl'];
    const data = mock.data.navigation;
    expect(reOrderNavArray(data, variation)).toMatchSnapshot();
  });

  it('reorder Navigation selector test 2', () => {
    const variation = ['Girl', 'Boy', 'Toddler Girl', 'Toddler Boy'];
    const data = mock.data.navigation;
    expect(reOrderNavArray(data, variation)).toMatchSnapshot();
  });
});
