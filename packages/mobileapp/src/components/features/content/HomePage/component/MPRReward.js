/* eslint-disable no-shadow */
/* eslint-disable react/require-default-props */
/* eslint-disable react-native-a11y/has-accessibility-props */
/* eslint-disable react/default-props-match-prop-types */
/* eslint-disable react/jsx-filename-extension */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import {TouchableOpacity} from 'react-native';
import {bool, func, string} from 'prop-types';
import {
  ContextContainer,
  EarnedTitle,
  HomeCarouselItemWrapper,
  MPRHalfCircleContainer,
  MPRRewardCard,
  CouponSaving,
  PriceReward,
  RedeemCheckoutTitle,
  SubContainer,
  MiddleSectionContainer,
  DateContainer,
  DetailsLink,
  PlaceBucksCard,
} from '../HomePage.style';

const rewardsWhite = require('../../../../../assets/images/reward-white.png');
const placeBucks = require('../../../../../assets/images/placebucks.png');
const savingCoupons = require('../../../../../assets/images/savings.png');

class MPRReward extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      titleText,
      priceText,
      dateText,
      detailsTitle,
      description,
      isPlcc,
      isPlaceBucks,
      isCouponSaving,
      isMPR,
      onChangeHandler,
    } = this.props;
    return (
      <HomeCarouselItemWrapper onPress={onChangeHandler}>
        <ContextContainer>
          <MPRHalfCircleContainer
            isPlcc={isPlcc}
            isPlaceBucks={isPlaceBucks}
            isCouponSaving={isCouponSaving}>
            {isPlcc && !isCouponSaving && !isPlaceBucks && <MPRRewardCard source={rewardsWhite} />}
            {isMPR && !isCouponSaving && !isPlaceBucks && <MPRRewardCard source={rewardsWhite} />}
            {isPlaceBucks && <PlaceBucksCard source={placeBucks} />}
            {isCouponSaving && <CouponSaving source={savingCoupons} />}
          </MPRHalfCircleContainer>
          <SubContainer>
            <EarnedTitle>{titleText}</EarnedTitle>
            <PriceReward>{priceText}</PriceReward>
            <MiddleSectionContainer>
              <DateContainer>{dateText}</DateContainer>
              <TouchableOpacity onPress={onChangeHandler}>
                <DetailsLink>{detailsTitle}</DetailsLink>
              </TouchableOpacity>
            </MiddleSectionContainer>
            <RedeemCheckoutTitle>{description}</RedeemCheckoutTitle>
          </SubContainer>
        </ContextContainer>
      </HomeCarouselItemWrapper>
    );
  }
}

MPRReward.propTypes = {
  titleText: string,
  priceText: string,
  dateText: string,
  detailsTitle: string,
  description: string,
  isPlcc: bool,
  isMPR: bool,
  isPlaceBucks: bool,
  isCouponSaving: bool,
  onChangeHandler: func.isRequired,
};

export default MPRReward;
