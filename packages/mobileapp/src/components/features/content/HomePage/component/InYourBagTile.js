/* eslint-disable no-shadow */
import React, {PureComponent} from 'react';
import {FlatList} from 'react-native-gesture-handler';
import {connect} from 'react-redux';
import _ from 'lodash';
import {string, func, number} from 'prop-types';
import {BodyCopyWithSpacing} from '@tcp/core/src/components/common/atoms/styledWrapper';
import {DamImage} from '@tcp/core/src/components/common/atoms';
import BagPageUtils from '@tcp/core/src/components/features/CnC/BagPage/views/Bagpage.utils';
import BagPageSelectors from '../../../../../../../core/src/components/features/CnC/BagPage/container/BagPage.selectors';
import {
  BadgeView,
  DetailsText,
  RightContainer,
  RightIcon,
  YourStoreText,
  HomeCarouselItemWrapper,
  RoundCircle,
  RoundView,
  ImageContainer,
} from '../HomePage.style';

const nextIcon = require('../../../../../../../mobileapp/src/assets/images/carrot-small-rights.png');

export class InYourBagTile extends PureComponent {
  renderImages = () => {
    const {orderItems} = this.props;
    const productsData = BagPageUtils.formatBagCarousalData(orderItems);
    const priceSortedOrderItems = _.sortBy(productsData, 'price').reverse();
    return (
      // eslint-disable-next-line react/jsx-filename-extension
      <>
        <FlatList
          data={priceSortedOrderItems}
          renderItem={this.renderItem}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
        />
      </>
    );
  };

  renderItem = ({item, index}) => {
    const imageUrl = item.imagePath;
    let showItems = true;
    if (index > 4) {
      showItems = false;
    }
    if (showItems) {
      return (
        <ImageContainer paddingTop="10px" paddingLeft="10px">
          <DamImage
            isProductImage
            url={imageUrl}
            width={40}
            height={50}
            itemBrand={item.itemBrand && item.itemBrand.toLowerCase()}
          />
        </ImageContainer>
      );
    }
    return null;
  };

  render() {
    const {headerText, detailsText, onPressHandler, cartVal} = this.props;
    const {orderItems} = this.props;
    const productsData = BagPageUtils.formatBagCarousalData(orderItems);
    let leftOverCartVal;
    let shouldRenderExtraItems = false;
    if (cartVal > 5) {
      leftOverCartVal = cartVal - 5;
    }
    if (leftOverCartVal > 0 && productsData.length > 5) {
      shouldRenderExtraItems = true;
    }
    return (
      <HomeCarouselItemWrapper onPress={onPressHandler}>
        <BadgeView>
          <YourStoreText>{`${headerText} (${cartVal})`}</YourStoreText>
          <RightContainer onPress={onPressHandler}>
            <DetailsText>{detailsText}</DetailsText>
            <RightIcon source={nextIcon} resizeMode="contain" />
          </RightContainer>
        </BadgeView>
        <BadgeView paddingLeft="10px">{this.renderImages()}</BadgeView>
        {shouldRenderExtraItems && (
          <RoundCircle cartVal={leftOverCartVal}>
            <RoundView cartVal={leftOverCartVal}>
              <BodyCopyWithSpacing
                text={`+${leftOverCartVal}`}
                color="white"
                fontSize="fs11"
                fontWeight="extrabold"
                textAlign="center"
                spacingStyles="padding-top-XXS"
              />
            </RoundView>
          </RoundCircle>
        )}
      </HomeCarouselItemWrapper>
    );
  }
}

InYourBagTile.propTypes = {
  detailsText: string.isRequired,
  headerText: string.isRequired,
  onPressHandler: func.isRequired,
  cartVal: number.isRequired,
};

export const mapStateToProps = (state) => {
  return {
    labels: state.Labels.global && state.Labels.global.header,
    cartVal: state.Header && state.Header.cartItemCount,
    orderItems: BagPageSelectors.getOrderItems(state),
  };
};

export default connect(mapStateToProps, null)(InYourBagTile);
