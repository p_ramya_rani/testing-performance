/* eslint-disable no-unused-expressions */
// 9fbef606107a605d69c0edbcd8029e5d
import React, {PureComponent} from 'react';
import {getStoreHours, capitalize} from '@tcp/core/src/utils';
import {connect} from 'react-redux';
import {objectOf, shape, string, func} from 'prop-types';
import {setUserNearStoreData} from '@tcp/core/src/components/features/account/User/container/User.actions';
import {
  BadgeView,
  DetailsText,
  ImageContainerWrapper,
  MapIcon,
  RightContainer,
  RightIcon,
  StyledCurrentLocation,
  StyledStoreLocator,
  TimeText,
  YourStoreText,
  TitleText,
  HomeCarouselItemWrapper,
} from '../HomePage.style';

const MarkerIcon = require('../../../../../../../mobileapp/src/assets/images/store_locator.png');
const nextIcon = require('../../../../../../../mobileapp/src/assets/images/carrot-small-rights.png');

export class YourStore extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      storeName: '',
      storeTime: '',
    };
  }

  componentDidMount() {
    this.getYourStoreData();
  }

  componentDidUpdate(prevProps) {
    const {favStore, nearByStore} = this.props;
    if (prevProps.favStore !== favStore || prevProps.nearByStore !== nearByStore) {
      this.getYourStoreData();
    }
  }

  getYourStoreData = () => {
    const {favStore} = this.props;
    const basicInfo = favStore && favStore.basicInfo;
    const isFavStoreAvailable = basicInfo && basicInfo.storeName;
    isFavStoreAvailable ? this.getFavStore() : this.getUserNearStoreDataList();
  };

  getFavStore = () => {
    const {favStore, labels, storeLocatorLabels} = this.props;
    const basicInfo = favStore && favStore.basicInfo;
    const {hours = {}} = favStore || {};
    const currentDate = new Date();
    const storeTimeData =
      hours && Object.keys(hours).length > 0
        ? getStoreHours(hours, storeLocatorLabels, currentDate)
        : null;
    const isInfoPresent = basicInfo && basicInfo.storeName;
    let favStoreText;
    if (labels && labels.lbl_header_openUntil) {
      favStoreText = isInfoPresent
        ? `${capitalize(basicInfo.storeName)}`
        : labels.lbl_header_storeDefaultTitle;
    }
    this.setState({
      storeName: favStoreText,
      storeTime: storeTimeData,
    });
  };

  errorHandler = (err) => {
    return err || null;
  };

  getUserNearStoreDataList = async () => {
    const {storeLocatorLabels, nearByStore} = this.props;
    if (!nearByStore.length) {
      this.errorHandler({errorCode: 'NO_STORES_FOUND'});
    }
    if (nearByStore[0]?.basicInfo?.storeName) {
      this.setState({
        storeName: nearByStore[0]?.basicInfo?.storeName,
      });
      const hours = nearByStore[0]?.hours;
      const storeTimeData =
        hours && Object.keys(hours).length > 0
          ? getStoreHours(hours, storeLocatorLabels, new Date())
          : null;
      this.setState({
        storeTime: storeTimeData,
      });
    }
  };

  render() {
    const {headerText, detailsText, onPressHandler} = this.props;
    const {storeTime, storeName} = this.state;
    return (
      <HomeCarouselItemWrapper onPress={onPressHandler}>
        <BadgeView>
          <YourStoreText>{headerText}</YourStoreText>
          <RightContainer onPress={onPressHandler}>
            <DetailsText>{detailsText}</DetailsText>
            <RightIcon source={nextIcon} resizeMode="contain" />
          </RightContainer>
        </BadgeView>
        <StyledStoreLocator>
          <ImageContainerWrapper>
            <MapIcon source={MarkerIcon} resizeMode="contain" />
          </ImageContainerWrapper>
          <StyledCurrentLocation>
            <TitleText>{capitalize(storeName || '')}</TitleText>
            <TimeText>{storeTime}</TimeText>
          </StyledCurrentLocation>
        </StyledStoreLocator>
      </HomeCarouselItemWrapper>
    );
  }
}

YourStore.propTypes = {
  favStore: shape({
    basicInfo: shape({}),
  }),
  labels: objectOf(shape({})).isRequired,
  storeLocatorLabels: shape({}),
  detailsText: string.isRequired,
  headerText: string.isRequired,
  onPressHandler: func.isRequired,
  nearByStore: func.isRequired,
};

YourStore.defaultProps = {
  favStore: {},
  storeLocatorLabels: {},
};

export const mapStateToProps = (state) => {
  return {
    labels: state.Labels.global && state.Labels.global.header,
    favStore: state.User && state.User.get('defaultStore'),
    storeLocatorLabels: state.Labels.StoreLocator && state.Labels.StoreLocator.StoreLanding,
    nearByStore: state.User && state.User.get('userNearStoreData'),
  };
};

export const mapDispatchToProps = (dispatch) => ({
  setUserNearStore: (payload) => {
    dispatch(setUserNearStoreData(payload));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(YourStore);
