/* eslint-disable max-lines */
/* eslint-disable no-shadow */
/* eslint-disable react/prop-types */
/* eslint-disable operator-assignment */
/* eslint-disable import/order */
/* eslint-disable import/named */
/* eslint-disable react/no-did-update-set-state */
/* eslint-disable react/sort-comp */
/* eslint-disable consistent-return */
/* eslint-disable default-case */
/* eslint-disable import/no-named-as-default */
/* eslint-disable array-callback-return */
/* eslint-disable sonarjs/cognitive-complexity */
/* eslint-disable no-unused-expressions */
/* eslint-disable react/jsx-filename-extension */
/* eslint-disable sonarjs/no-identical-functions */
/* eslint-disable react/require-default-props */
/* eslint-disable react/default-props-match-prop-types */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react-native/no-color-literals */
/* eslint-disable react/no-redundant-should-component-update */
import React, {PureComponent} from 'react';
import {Dimensions, ScrollView} from 'react-native';
import {FlatList} from 'react-native-gesture-handler';
import {shape, func, PropTypes, bool} from 'prop-types';
import {getLabelValue} from '@tcp/core/src/utils';
import {connect} from 'react-redux';
import {setUserNearStoreData} from '@tcp/core/src/components/features/account/User/container/User.actions';
import ModalBox from 'react-native-modalbox';
import {isAndroid} from '@tcp/core/src/utils/utils.app';
import {
  getUserLoggedInState,
  getUserNearStoreState,
  isPlccUser,
} from '@tcp/core/src/components/features/account/User/container/User.selectors';
import {Anchor, BodyCopy, RichText} from '@tcp/core/src/components/common/atoms';
import {setClickAnalyticsData, trackClick} from '@tcp/core/src/analytics/actions';
import Barcode from '@tcp/core/src/components/common/molecules/Barcode';
import ScreenViewShot from '@tcp/core/src/components/common/atoms/ScreenViewShot';
import RNPrint from 'react-native-print';
import getMarkupForPrint from '@tcp/core/src/components/features/CnC/common/organism/CouponAndPromos/views/CouponDetailPrintHTMLModal.native';
import {getAllCoupons} from '@tcp/core/src/components/features/CnC/common/organism/CouponAndPromos/container/Coupon.selectors';
import YourStore from './YourStore';
import {
  CarouselWrapper,
  PrintContainer,
  DefaultText,
  TopContainer,
  ImageMPRContainer,
  StyledImage,
  PrivacyContent,
} from '../HomePage.style';
import MPRReward from './MPRReward';
import {MAX_CARD_HEIGHT, SWIPE_AREA} from '../../../shopByProfile/ShopByProfile.constants';
import InYourBagTile from './InYourBagTile';
import HOMEPAGE_CONSTANTS from '../HomePage.constants';
import {StyledModalWrapper} from '../../../../../../../core/src/components/features/CnC/common/organism/CouponAndPromos/styles/CouponDetailModal.style.native';
import BagPageSelectors from '../../../../../../../core/src/components/features/CnC/BagPage/container/BagPage.selectors';
import BagPageActions from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.actions';

const {width: viewportWidth} = Dimensions.get('window');
export const sliderWidth = viewportWidth;
const closeIconSrc = require('../../../../../assets/images/shopByProfile/elements/close3x.png');

let rewardList = [];
let counter = 1;
export class HomeCarousel extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      cardOpen: false,
      rewardDataDetails: {},
    };
    this.setScreenViewShotRef = (element) => {
      this.screenViewShotRef = element;
    };
  }

  componentDidMount() {
    this.loadCarouselData();
  }

  loadCarouselData() {
    const {
      ENABLE_HOME_CAROUSEL,
      ENABLE_HOME_CAROUSEL_IN_YOUR_BAG,
      ENABLE_HOME_CAROUSEL_IN_YOUR_STORE,
      ENABLE_HOME_CAROUSEL_REWARDS,
    } = this.props;
    if (ENABLE_HOME_CAROUSEL && this.checkAllLableAvailable()) {
      ENABLE_HOME_CAROUSEL_REWARDS && this.checkMPRRewardsAvailable(); // checking  MPR Reward data priority 1
      ENABLE_HOME_CAROUSEL_IN_YOUR_BAG && this.checkCartValueAvailable();
      ENABLE_HOME_CAROUSEL_IN_YOUR_STORE && this.checkYourStoreAvailable(); // checking your store data priority 6
    }
  }

  checkAllLableAvailable = () => {
    const {labels} = this.props;
    if (
      labels &&
      labels.lbl_viewTxt &&
      labels.lbl_detailsTxt &&
      labels.lbl_header_inYourBagTitle &&
      labels.lbl_header_storeTitle &&
      labels.lbl_validtxt &&
      labels.lbl_useby_txt &&
      labels.lbl_earned_it &&
      labels.lbl_redeem_checkout &&
      labels.lbl_Yuy_saving &&
      labels.lbl_header_storeDefaultTitle
    ) {
      return true;
    }
    return false;
  };

  componentDidUpdate(prevProps) {
    const {
      favStore,
      nearByStore,
      allCouponsList,
      isUserLoggedIn,
      ENABLE_HOME_CAROUSEL,
      ENABLE_HOME_CAROUSEL_IN_YOUR_STORE,
      ENABLE_HOME_CAROUSEL_IN_YOUR_BAG,
      ENABLE_HOME_CAROUSEL_REWARDS,
      cartVal,
    } = this.props;
    const {data} = this.state;
    if (ENABLE_HOME_CAROUSEL && this.checkAllLableAvailable()) {
      const rewardItemCount = data.filter(
        (x) => x.priority === HOMEPAGE_CONSTANTS.HomeCarousel_Priority.MPRReward,
      ).length;
      if (
        prevProps.allCouponsList.size !== allCouponsList.size ||
        allCouponsList.size !== rewardItemCount
      ) {
        ENABLE_HOME_CAROUSEL_REWARDS && this.checkMPRRewardsAvailable();
      }
      const bagItemCount = data.filter(
        (x) => x.priority === HOMEPAGE_CONSTANTS.HomeCarousel_Priority.InYourBag,
      ).length;
      if (prevProps.cartVal !== cartVal || cartVal !== bagItemCount) {
        ENABLE_HOME_CAROUSEL_IN_YOUR_BAG && this.checkCartValueAvailable();
      }
      if (favStore !== prevProps.favStore || nearByStore !== prevProps.nearByStore) {
        ENABLE_HOME_CAROUSEL_IN_YOUR_STORE && this.checkYourStoreAvailable();
      }
      if (isUserLoggedIn !== prevProps.isUserLoggedIn && !isUserLoggedIn) {
        this.removeLoginUserData();
      }
    }
  }

  checkMPRRewardsAvailable = () => {
    const {isUserLoggedIn, isPlcc, labels, allCouponsList} = this.props;
    rewardList = [];
    if (isUserLoggedIn && allCouponsList.size > 0) {
      const mprCouponList = allCouponsList.filter(
        (x) => x.promotionType === HOMEPAGE_CONSTANTS.LOYALTY,
      );
      const savingCouponList = allCouponsList.filter(
        (x) => x.offerType === HOMEPAGE_CONSTANTS.SAVING,
      );
      const placeBucksCouponList = allCouponsList.filter(
        (x) =>
          x.offerType === HOMEPAGE_CONSTANTS.PLACECASH || x.offerType === HOMEPAGE_CONSTANTS.PC,
      );
      if (mprCouponList.size > 0) {
        mprCouponList.map((item) => {
          rewardList.push({
            priority: HOMEPAGE_CONSTANTS.HomeCarousel_Priority.MPRReward,
            rewardData: {
              id: item?.id,
              sequenceId: counter,
              titleText: getLabelValue(labels, 'lbl_earned_it'),
              title: item?.title,
              expirationDate: `${item?.expirationDate}`,
              isPlcc,
              description: getLabelValue(labels, 'lbl_redeem_checkout'),
              legalText: item?.legalText,
              effectiveDate: item?.effectiveDate,
            },
          });
          counter = counter + 1;
        });
      }
      if (placeBucksCouponList.size > 0) {
        placeBucksCouponList.map((item) => {
          rewardList.push({
            priority: HOMEPAGE_CONSTANTS.HomeCarousel_Priority.MPRReward,
            rewardData: {
              id: item?.id,
              sequenceId: counter,
              titleText: getLabelValue(labels, 'lbl_earned_it'),
              title: item?.title,
              expirationDate: `${item?.effectiveDate}-${item?.expirationDate}`,
              isPlaceBucks: true,
              description: getLabelValue(labels, 'lbl_redeem_checkout'),
              legalText: item?.legalText,
              effectiveDate: item?.effectiveDate,
            },
          });
          counter = counter + 1;
        });
      }
      if (savingCouponList.size > 0) {
        savingCouponList.map((item) => {
          rewardList.push({
            priority: HOMEPAGE_CONSTANTS.HomeCarousel_Priority.MPRReward,
            rewardData: {
              id: item?.id,
              sequenceId: counter,
              titleText: getLabelValue(labels, 'lbl_Yuy_saving'),
              title: item?.title,
              expirationDate: `${item?.effectiveDate}- ${item?.expirationDate}`,
              isSaving: true,
              description: getLabelValue(labels, 'lbl_redeem_checkout'),
              legalText: item?.legalText,
              effectiveDate: item?.effectiveDate,
            },
          });
          counter = counter + 1;
        });
      }
    }
  };

  removeLoginUserData = () => {
    const {isUserLoggedIn, favStore, setCouponsDataHomeCarousal} = this.props;
    const {data} = this.state;
    if (!isUserLoggedIn) {
      const guestUserItemList = data.filter(
        (x) =>
          x.priority !== HOMEPAGE_CONSTANTS.HomeCarousel_Priority.MPRReward ||
          (favStore && x.priority !== HOMEPAGE_CONSTANTS.HomeCarousel_Priority.YourStore),
      );
      setCouponsDataHomeCarousal([]);
      this.setState({data: guestUserItemList});
    }
  };

  checkCartValueAvailable = () => {
    const {data} = this.state;
    const {cartVal, subTotal} = this.props;
    if (cartVal > 0 && subTotal >= 35) {
      !data.find((x) => x.priority === HOMEPAGE_CONSTANTS.HomeCarousel_Priority.InYourBag) &&
        data.push({priority: HOMEPAGE_CONSTANTS.HomeCarousel_Priority.InYourBag});
      this.setState({data});
    } else {
      !!data.find((x) => x.priority === HOMEPAGE_CONSTANTS.HomeCarousel_Priority.InYourBag) &&
        this.setState({
          data: data.filter(
            (x) => x.priority !== HOMEPAGE_CONSTANTS.HomeCarousel_Priority.InYourBag,
          ),
        });
    }
  };

  checkYourStoreAvailable = () => {
    const {favStore, nearByStore} = this.props;
    const basicInfo = favStore && favStore.basicInfo;
    const isFavStoreAvailable = basicInfo && basicInfo.storeName;
    const {data} = this.state;
    if (isFavStoreAvailable || nearByStore) {
      !data.find((x) => x.priority === HOMEPAGE_CONSTANTS.HomeCarousel_Priority.YourStore) &&
        data.push({priority: HOMEPAGE_CONSTANTS.HomeCarousel_Priority.YourStore});
    }
  };

  errorHandler = (err) => {
    return err || null;
  };

  renderItem = ({item}) => {
    const {favStore, nearByStore} = this.props;
    switch (item.priority) {
      case HOMEPAGE_CONSTANTS.HomeCarousel_Priority.MPRReward:
        return this.getMPRReward(item?.rewardData);
      case HOMEPAGE_CONSTANTS.HomeCarousel_Priority.InYourBag:
        return this.inYourBagTile();
      case HOMEPAGE_CONSTANTS.HomeCarousel_Priority.YourStore:
        if (
          favStore?.basicInfo?.storeName ||
          (nearByStore && nearByStore[0]?.basicInfo?.storeName)
        ) {
          return this.getStore();
        }
    }
  };

  analyticsOnTileClick = (expectedValue) => {
    const {setClickAnalyticsDataDispatch, trackClickDispatch} = this.props;
    const {
      HOME_CAROUSEL_ANALYTICS_DATA: {customEvents, hpEvents},
    } = HOMEPAGE_CONSTANTS;
    setClickAnalyticsDataDispatch({
      customEvents: [customEvents],
      clickEvent: true,
      internalCampaignId: expectedValue,
    });
    trackClickDispatch({
      name: hpEvents,
    });
  };

  getMPRReward = (item) => {
    const {labels} = this.props;
    const {
      HOME_CAROUSEL_ANALYTICS_DATA: {hpCardsMyPlaceReward},
    } = HOMEPAGE_CONSTANTS;
    return (
      <MPRReward
        titleText={item?.titleText}
        priceText={item?.title}
        dateText={
          item?.isSaving || item?.isPlaceBucks
            ? `${getLabelValue(labels, 'lbl_validtxt')} ${item?.expirationDate}`
            : `${getLabelValue(labels, 'lbl_useby_txt')} ${item?.expirationDate}`
        }
        detailsTitle={getLabelValue(labels, 'lbl_detailsTxt')}
        description={item?.description}
        isMPR={!item?.isPlcc}
        isPlcc={item?.isPlcc}
        isPlaceBucks={item?.isPlaceBucks}
        isCouponSaving={item?.isSaving}
        onChangeHandler={() => {
          this.analyticsOnTileClick(hpCardsMyPlaceReward);
          this.toggleCard(item);
        }}
      />
    );
  };

  onPressHandler = () => {
    const {navigation} = this.props;
    navigation.navigate({
      routeName: 'BagPage',
    });
  };

  onYourStoreClick = () => {
    const {navigation, labels} = this.props;
    const {
      HOME_CAROUSEL_ANALYTICS_DATA: {hpCardsInYourStore},
    } = HOMEPAGE_CONSTANTS;
    this.analyticsOnTileClick(hpCardsInYourStore);
    setTimeout(() => {
      navigation.navigate({
        routeName: 'StoreLanding',
        params: {
          title: getLabelValue(labels, 'lbl_header_storeDefaultTitle').toUpperCase(),
        },
      });
    }, 50);
  };

  getStore = () => {
    const {labels} = this.props;
    return (
      <YourStore
        headerText={getLabelValue(labels, 'lbl_header_storeTitle')}
        detailsText={getLabelValue(labels, 'lbl_detailsTxt')}
        onPressHandler={this.onYourStoreClick}
      />
    );
  };

  toggleCard = (item) => {
    this.setState((prevState) =>
      Object.assign({}, prevState, {cardOpen: !prevState.cardOpen, rewardDataDetails: item}),
    );
  };

  showValidity = () => {
    const {rewardDataDetails} = this.state;
    const {labels} = this.props;
    const validityLbl = rewardDataDetails.isPlaceBucks
      ? getLabelValue(labels, 'lbl_coupon_validity')
      : getLabelValue(labels, 'lbl_useby_txt');
    const validityStr = rewardDataDetails.isPlaceBucks
      ? `${rewardDataDetails.effectiveDate} - ${rewardDataDetails.expirationDate}`
      : `${rewardDataDetails.expirationDate}`;
    return `${validityLbl} ${validityStr}`;
  };

  printHTML = async (rewardDataDetails, labels) => {
    const uri = await this.screenViewShotRef.capture();
    await RNPrint.print({
      html: getMarkupForPrint(rewardDataDetails, labels, this.showValidity(), uri),
    });
  };

  inYourBagTile = () => {
    const {labels} = this.props;
    return (
      <InYourBagTile
        headerText={getLabelValue(labels, 'lbl_header_inYourBagTitle')}
        detailsText={getLabelValue(labels, 'lbl_viewTxt')}
        onPressHandler={this.onPressInYourBagTile}
      />
    );
  };

  onPressInYourBagTile = () => {
    const {navigation} = this.props;
    const {
      HOME_CAROUSEL_ANALYTICS_DATA: {hpCardsInYourBag},
    } = HOMEPAGE_CONSTANTS;
    this.analyticsOnTileClick(hpCardsInYourBag);
    setTimeout(() => {
      navigation.navigate({
        routeName: 'BagPage',
      });
    }, 50);
  };

  renderCard = () => {
    const {rewardDataDetails} = this.state;
    const {labels} = this.props;

    return (
      <ScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="handled">
        <TopContainer>
          <BodyCopy
            fontSize="fs16"
            fontFamily="secondary"
            fontWeight="extrabold"
            text={rewardDataDetails?.title}
            textAlign="center"
          />
          <ImageMPRContainer onPress={this.toggleCard}>
            <StyledImage source={closeIconSrc} />
          </ImageMPRContainer>
        </TopContainer>
        <DefaultText>
          <BodyCopy
            fontSize="fs16"
            fontFamily="secondary"
            fontWeight="regular"
            text={
              rewardDataDetails?.isSaving
                ? `${getLabelValue(labels, 'lbl_validtxt')} ${rewardDataDetails?.expirationDate}`
                : `${getLabelValue(labels, 'lbl_useby_txt')} ${rewardDataDetails?.expirationDate}`
            }
          />
        </DefaultText>
        <StyledModalWrapper>
          {rewardDataDetails?.id ? (
            <ScreenViewShot
              setScreenViewShotRef={this.setScreenViewShotRef}
              options={{format: 'png', quality: 0.9, result: 'base64'}}>
              <Barcode value={rewardDataDetails?.id} fontSize="fs12" />
            </ScreenViewShot>
          ) : (
            <ScreenViewShot
              setScreenViewShotRef={this.setScreenViewShotRef}
              options={{format: 'png', quality: 0.9, result: 'base64'}}>
              <BodyCopy fontSize="fs42" fontFamily="primary" fontWeight="black" text=" " />
            </ScreenViewShot>
          )}
          <PrintContainer>
            <Anchor
              fontSizeVariation="large"
              underline
              anchorVariation="primary"
              text="Print"
              class="clickhere"
              onPress={() => this.printHTML(rewardDataDetails, labels, this.showValidity())}
            />
          </PrintContainer>
          {rewardDataDetails?.legalText && (
            <PrivacyContent>
              <RichText source={{html: rewardDataDetails?.legalText}} />
            </PrivacyContent>
          )}
        </StyledModalWrapper>
      </ScrollView>
    );
  };

  render() {
    const {data, cardOpen} = this.state;
    // const rewardList = data.filter(
    //   (x) => x.priority === HOMEPAGE_CONSTANTS.HomeCarousel_Priority.MPRReward,
    // );
    const otherDataList = data
      .filter((x) => x.priority !== HOMEPAGE_CONSTANTS.HomeCarousel_Priority.MPRReward)
      .sort((a, b) => (a.priority > b.priority ? 1 : -1));
    const itemDataList = [];
    rewardList.map((item) => {
      itemDataList.push(item);
    });
    otherDataList.map((item) => {
      itemDataList.push(item);
    });

    return (
      <>
        {cardOpen ? (
          <ModalBox
            ref={(ref) => {
              this.modal = ref;
            }}
            animationDuration={300}
            swipeThreshold={10}
            style={[
              {backgroundColor: '#ffffff', borderRadius: 40},
              {height: MAX_CARD_HEIGHT + (isAndroid() ? 400 : 200)},
            ]}
            position="bottom"
            swipeArea={SWIPE_AREA}
            isOpen={cardOpen}
            onClosed={() => this.toggleCard()}
            entry="bottom"
            coverScreen>
            {this.renderCard()}
          </ModalBox>
        ) : null}
        <CarouselWrapper>
          <FlatList
            data={itemDataList}
            renderItem={this.renderItem}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
          />
        </CarouselWrapper>
      </>
    );
  }
}

HomeCarousel.propTypes = {
  navigation: shape({navigate: func}),
  labels: PropTypes.shape({}).isRequired,
  favStore: shape({
    basicInfo: shape({}),
  }),
  cartVal: Number.isRequired,
  isPlcc: bool.isRequired,
  allCouponsList: shape([]),
  isUserLoggedIn: bool,
  subTotal: Number.isRequired,
  trackClickDispatch: PropTypes.func,
  setClickAnalyticsDataDispatch: PropTypes.func,
};

HomeCarousel.defaultProps = {
  navigation: {},
  labels: {},
  favStore: {},
  subTotal: 2,
};
export const mapStateToProps = (state) => {
  return {
    labels: state.Labels.global && state.Labels.global.header,
    favStore: state.User && state.User.get('defaultStore'),
    nearByStore: getUserNearStoreState(state),
    isUserLoggedIn: getUserLoggedInState(state),
    isPlcc: isPlccUser(state),
    cartVal: state.Header && state.Header.cartItemCount,
    subTotal: BagPageSelectors.getOrderSubTotal(state),
    allCouponsList: getAllCoupons(state),
  };
};

export const mapDispatchToProps = (dispatch) => ({
  setUserNearStore: (payload) => {
    dispatch(setUserNearStoreData(payload));
  },
  setCouponsDataHomeCarousal: (payload) =>
    dispatch(BagPageActions.setCouponsDataForHomeCarousel(payload)),
  setClickAnalyticsDataDispatch: (payload) => dispatch(setClickAnalyticsData(payload)),
  trackClickDispatch: (payload) => dispatch(trackClick(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(HomeCarousel);
