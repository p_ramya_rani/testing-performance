// 9fbef606107a605d69c0edbcd8029e5d
import styled from 'styled-components/native';
import {Button, Image} from '@tcp/core/src/components/common/atoms';
import HP_NEW_LAYOUT from '@tcp/core/src/constants/hpNewLayout.constants';
import {StyleSheet} from 'react-native';

const setBackground = (props) => {
  if (props.theme.isGymboree) {
    return `
    background-color: ${props.theme.colorPalette.orange[900]};
    `;
  }
  return `
  background-color: ${props.theme.colorPalette.blue[800]};
  `;
};

export const TextComponent = styled.Text`
  color: ${(props) => props.theme.colorPalette.gray['900']};
  font-size: ${(props) => props.theme.typography.fontSizes.fs12};
  font-weight: bold;
  margin: 0 auto 5px;
`;

export const TextInputComponent = styled.TextInput`
  height: 40px;
  border: 1px solid ${(props) => props.theme.colorPalette.gray['500']};
  border-radius: 3px;
  color: ${(props) => props.theme.colorPalette.gray['900']};
  width: 300px;
  margin: 0 auto;
  padding: 5px;
`;

export const ButtonComponent = styled(Button)`
  margin: 10px auto;
`;

export const SMSTapToJoinContainer = styled.View`
  min-height: 80px !important;
  overflow: hidden;
  margin: ${(props) => props.theme.spacing.ELEM_SPACING.SM}
    ${(props) => props.theme.spacing.APP_LAYOUT_SPACING.XS} 60px;
  justify-content: center;
  background-color: ${(props) => props.theme.colorPalette.white};
  border-radius: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  text-align: center;
  align-items: center;
`;

export const SMSTapToJoinSeparator = styled.View`
  height: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
  background-color: ${(props) => props.theme.colorPalette.gray[300]};
  margin: 0 -${(props) => props.theme.spacing.APP_LAYOUT_SPACING.XS};
`;

export const StyledPageSlotsWrapper = styled.View`
  padding: ${(props) => (props.isHpNewLayoutEnabled ? `0 ${HP_NEW_LAYOUT.BODY_PADDING}px` : 0)};
`;
export const CarouselWrapper = styled.View`
  margin-bottom: 10px;
  width: 100%;
`;

export const HomeCarouselItemWrapper = styled.TouchableOpacity`
  width: 310px;
  height: 108px;
  border-radius: 16px;
  background-color: white;
  shadow-opacity: 0.6;
  shadow-radius: 10px;
  shadow-color: rgba(0, 0, 0, 0.12);
  shadow-offset: 0px 4px;
  elevation: 12;
  margin-left: 15px;
  margin-right: 10px;
`;
export const CardContainer = styled.View`
  border-radius: 16px;
  width: 287px;
  height: 108px;
  background: ${(props) => props.theme.colorPalette.white};
  align-self: center;
  border: 1px solid ${(props) => props.theme.colorPalette.gray['500']};
  margin-bottom: 10px;
`;

export const YourStoreText = styled.Text`
  font-family: ${(props) => props.theme.typography.fonts.secondary};
  font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy3}px;
  font-weight: ${(props) => props.theme.typography.fontWeights.extrabold};
  font-style: normal;
  letter-spacing: 0.2px;
  text-align: left;
  color: ${(props) => props.theme.colors.PRIMARY.DARK};
  margin-left: 15px;
  margin-top: 10px;
`;

export const BadgeView = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  ${(props) => (props.paddingLeft ? `padding-left: ${props.paddingLeft}` : '')};
`;

export const StyledStoreLocator = styled.View`
  width: 100%;
  flex-direction: row;
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.MED};
`;

export const StyledCurrentLocation = styled.View`
  margin-left: 5px;
`;
export const ImageContainerWrapper = styled.View`
  margin-left: 10px;
`;
export const createProfileContainerStyle = StyleSheet.create({
  viewFour: {
    height: 350,
    position: 'absolute',
    top: '0%',
    width: '100%',
  },
  viewFourGrad: {
    height: '100%',
    width: '100%',
  },
});

export const MapIcon = styled.Image`
  width: ${(props) => props.theme.spacing.ELEM_SPACING.XXML};
  height: ${(props) => props.theme.spacing.ELEM_SPACING.XXML};
`;

export const DetailsText = styled.Text`
  font-family: ${(props) => props.theme.typography.fonts.secondary};
  font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy3}px;
  font-weight: bold;
  font-style: normal;
  letter-spacing: 0.03px;
  text-align: right;
  color: #254f6e;
  right: 10px;
`;

export const RightContainer = styled.TouchableOpacity`
  padding-left: 10px;
  right: 10px;
  flex-direction: row;
  margin-top: 10px;
`;

export const RightIcon = styled.Image`
  width: 7px;
  height: 8px;
  right: 5px;
  margin-top: 6px;
`;

export const TimeText = styled.Text`
  font-family: ${(props) => props.theme.typography.fonts.secondary};
  font-size: 12px;
  font-weight: 600;
  font-style: normal;
  letter-spacing: 0.2px;
  text-align: left;
  color: #26762c;
  bottom: 2px;
  text-transform: capitalize;
`;

export const TitleText = styled.Text`
  font-family: ${(props) => props.theme.typography.fonts.secondary};
  font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy3}px;
  font-weight: bold;
  font-style: normal;
  letter-spacing: 0.2px;
  text-align: left;
  color: ${(props) => props.theme.colors.PRIMARY.DARK};
`;

export const MPRHalfCircleContainer = styled.View`
  ${(props) => {
    if (props.isPlcc) {
      return `background-color: ${props.theme.colors.PLCC_BACKGROUND}`;
    }
    if (props.isPlaceBucks) {
      return `background-color: ${props.theme.colors.PRIMARY.GREEN}`;
    }
    if (props.isCouponSaving) {
      return `background-color: ${props.theme.colors.PROMO.PINK}`;
    }
    return `background-color: ${props.theme.colors.DEFAULT_ACCENT_GYM}`;
  }}
  width:56px;
  height: 108px;
  border-top-left-radius: 16px;
  border-bottom-left-radius: 16px;
  border-top-right-radius: 75px;
  border-bottom-right-radius: 75px;
  align-items: center;
  justify-content: center;
  overflow: hidden;
`;

export const MPRRewardCard = styled.Image`
  width: ${(props) => props.theme.spacing.ELEM_SPACING.XXML};
  height: ${(props) => props.theme.spacing.ELEM_SPACING.XXML};
  right: 5px;
`;

export const PlaceBucksCard = styled.Image`
  width: ${(props) => props.theme.spacing.ELEM_SPACING.XXML};
  height: ${(props) => props.theme.spacing.ELEM_SPACING.XXML};
`;

export const CouponSaving = styled.Image`
  width: 35.2px;
  height: 35.1px;
  right: 2px;
`;

export const ContextContainer = styled.View`
  flex-direction: row;
`;

export const SubContainer = styled.View`
  flex-direction: column;
  margin-left: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy3}px;
`;

export const PriceReward = styled.Text`
  font-family: ${(props) => props.theme.typography.fonts.secondary};
  font-size: 17px;
  font-weight: 800;
  font-style: normal;
  letter-spacing: 0.2px;
  color: ${(props) => props.theme.colors.PRIMARY.DARK};
  padding-right: 10px;
`;

export const EarnedTitle = styled.Text`
  font-family: ${(props) => props.theme.typography.fonts.secondary};
  font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy3}px;
  font-weight: 800;
  font-style: normal;
  color: ${(props) => props.theme.colors.PRIMARY.DARK};
  margin-top: 14px;
  letter-spacing: 0.2px;
`;

export const RedeemCheckoutTitle = styled.Text`
  font-family: ${(props) => props.theme.typography.fonts.secondary};
  font-size: 12px;
  letter-spacing: 0.2px;
  color: ${(props) => props.theme.colors.PRIMARY.DARK};
`;
export const MiddleSectionContainer = styled.View`
  flex-direction: row;
`;

export const DateContainer = styled.Text`
  font-family: ${(props) => props.theme.typography.fonts.secondary};
  font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy3}px;
  font-weight: normal;
  font-style: normal;
  letter-spacing: 0.2px;
  color: #595959;
`;

export const DetailsLink = styled.Text`
  font-family: ${(props) => props.theme.typography.fonts.secondary};
  font-size: ${(props) => props.theme.fonts.fontSize.body.bodytext.copy3}px;
  font-weight: normal;
  font-style: normal;
  letter-spacing: 0.5px;
  color: ${(props) => props.theme.colors.PRIMARY.DARK};
  text-decoration: underline;
  text-decoration-color: ${(props) => props.theme.colors.PRIMARY.DARK};
  margin-left: 5px;
`;

export const HorizontalView = styled.View`
  flex-direction: row;
  text-align: center;
  align-items: center;
  margin-top: 17px;
  justify-content: center;
  width: 256px;
  height: 106px;
`;

export const CodeIdView = styled.View`
  margin-top: 10px;
`;

export const DescriptionText = styled.Text`
  margin-top: 21px;
`;

export const TopContainer = styled.View`
  flex-direction: row;
  margin-top: 20px;
  justify-content: center;
  align-items: center;
`;

export const DefaultText = styled.View`
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XXML};
  justify-content: center;
  align-items: center;
`;

export const PrintContainer = styled.View`
  margin-top: 21px;
`;

export const ImageMPRContainer = styled.TouchableOpacity`
  position: absolute;
  right: 35;
`;

export const StyledImage = styled(Image)`
  /* stylelint-disable-next-line */
  tint-color: #000;
  width: 20px;
  height: 20px;
`;

export const RoundView = styled.View`
  ${setBackground}
  width: ${(props) => props.theme.spacing.ELEM_SPACING.LRCX};
  height: ${(props) => props.theme.spacing.ELEM_SPACING.LRCX};
  border-radius: 12;
  margin: 2px;
`;

export const RoundCircle = styled.View`
  background-color: ${(props) => props.theme.colorPalette.white};
  width: ${(props) => props.theme.spacing.ELEM_SPACING.MED_2};
  height: ${(props) => props.theme.spacing.ELEM_SPACING.MED_2};
  border-radius: 10px;
  justify-content: center;
  align-items: center;
  border: 2px solid white;
  position: absolute;
  right: 20;
  bottom: 30;
  padding-left: 5px;
`;

export const ImageContainer = styled.View`
  padding-top: ${(props) => (props.paddingTop ? props.paddingTop : '0px')};
  padding-left: ${(props) => (props.paddingLeft ? props.paddingLeft : '0px')};
`;

export const PrivacyContent = styled.View`
  text-align: left;
  justify-content: flex-start;
  flex-direction: row;
  flex-wrap: wrap;
  margin-top: ${(props) => props.theme.spacing.ELEM_SPACING.XL};
  margin-bottom: ${(props) => props.theme.spacing.ELEM_SPACING.SM};
`;

export const BannerSeparator = styled.View`
  width: 100%;
  height: ${(props) => props.theme.spacing.ELEM_SPACING.XXM};
`;

export default {
  TextComponent,
  TextInputComponent,
  ButtonComponent,
  SMSTapToJoinContainer,
  SMSTapToJoinSeparator,
  StyledPageSlotsWrapper,
  CarouselWrapper,
  HomeCarouselItemWrapper,
  CardContainer,
  BadgeView,
  MapIcon,
  ImageContainerWrapper,
  YourStoreText,
  StyledStoreLocator,
  StyledCurrentLocation,
  DetailsText,
  RightContainer,
  RightIcon,
  TimeText,
  TitleText,
  createProfileContainerStyle,
  MPRHalfCircleContainer,
  MPRRewardCard,
  ContextContainer,
  EarnedTitle,
  SubContainer,
  PriceReward,
  RedeemCheckoutTitle,
  MiddleSectionContainer,
  DateContainer,
  DetailsLink,
  PlaceBucksCard,
  CouponSaving,
  HorizontalView,
  CodeIdView,
  DescriptionText,
  TopContainer,
  DefaultText,
  PrintContainer,
  RoundView,
  RoundCircle,
  ImageContainer,
  ImageMPRContainer,
  PrivacyContent,
  BannerSeparator,
};
