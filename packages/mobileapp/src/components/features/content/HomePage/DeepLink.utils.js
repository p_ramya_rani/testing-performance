// 9fbef606107a605d69c0edbcd8029e5d 
/* eslint-disable sonarjs/cognitive-complexity */
import {navigateToNestedRoute, configureInternalNavigationFromCMSUrl} from '@tcp/core/src/utils';
import {
  navigateToPage,
  redirectToInAppView,
  isURISchemeUsed,
  isAndroid,
} from '@tcp/core/src/utils/index.native';
import {decode} from '@tcp/core/src/utils/base64.utils';
import DEEPLINK_CONSTANTS from './Deeplink.constants';

const PATH_SEPERATOR = '/us/';

const getAbsoluteUrl = (uriSchemeUsed, completeUrl) => {
  let absoluteUrl = completeUrl;
  if (uriSchemeUsed) {
    const deepLinkUrl = completeUrl.split('/?deepLinkURL=');
    absoluteUrl = deepLinkUrl && deepLinkUrl[1] ? deepLinkUrl[1] : completeUrl;
  }
  return absoluteUrl;
};

/**
 * @function : navigate
 * This function will be used to open the specific page when the app is opened through deep link.
 * @param {*} completeUrl
 * @param {*} queryString
 * @param {*} navigation
 * @param {*} setDeepLinkClickAnalyticsData
 */
// eslint-disable-next-line complexity
const navigate = (completeUrl, queryString, navigation) => {
  if (completeUrl) {
    const {
      URL_PATTERN: {
        CATEGORY_LANDING,
        PRODUCT_LIST,
        OUTFIT_DETAILS,
        SEARCH_DETAIL,
        BUNDLE_DETAIL,
        CONTENT,
        HELP_CENTER,
        LOGIN,
        REGISTER,
        STORE_LOCATOR,
        HOME,
        CHANGE_PASSWORD,
      },
      CATEGORY: {LOGIN_CREATE_ACCOUNT_CATEGORY},
      LOGIN_CREATE_ACCOUNT_CONST: {LOGIN: ACCOUNT_CONST_LOGIN, CREATE_ACCOUNT_TYPE},
    } = DEEPLINK_CONSTANTS;

    /**
     * Taking care of existing universal link functionality.
     * Logic: When deep link will enabled absolute URL will come in query param: deepLinkURL
     *
     */
    const uriSchemeUsed = isURISchemeUsed(completeUrl);
    const absoluteURL = getAbsoluteUrl(uriSchemeUsed, completeUrl);
    const parsedURL = queryString.parseUrl(absoluteURL);
    if (!parsedURL) {
      return null;
    }
    const {url, query = {}} = parsedURL;
    const {cid: externalCampaignId, om_mid: omMID, om_rid: omRID} = query;
    const analyticsParams = {
      ...(externalCampaignId && {externalCampaignId}),
      ...(omMID && {omMID}),
      ...(omRID && {omRID}),
    };

    if (
      url.includes(CATEGORY_LANDING) ||
      url.includes(PRODUCT_LIST) ||
      url.includes(OUTFIT_DETAILS) ||
      url.includes(SEARCH_DETAIL) ||
      url.includes(BUNDLE_DETAIL)
    ) {
      // ** This If will match PLP, PDP, Bundle, Search & Outfit Details Pages.
      const tempUrl = url.split(PATH_SEPERATOR)[1];
      let urlWithQueryParam = `/${tempUrl}`;
      if (query.cid) {
        urlWithQueryParam += `?icid=${query.cid}`;
      }
      const cmsValidatedUrl = configureInternalNavigationFromCMSUrl(urlWithQueryParam);
      navigateToPage(cmsValidatedUrl, navigation, analyticsParams);
    } else if (url.includes(CONTENT) || url.includes(HELP_CENTER)) {
      // ** This Else If will match Content & help Center Pages.

      /**
       * IOS throws error when pass query params in URL
       */
      const contentURL = isAndroid() ? absoluteURL : url;
      redirectToInAppView(contentURL, navigation, null, false, analyticsParams);
    } else if (url.includes(STORE_LOCATOR)) {
      // ** This Else If will match Store Locator Pages.
      navigation.navigate('StoreLanding', analyticsParams);
    } else if (url.includes(LOGIN) || url.includes(REGISTER)) {
      // ** This Else If will match Login & Create Account
      return {
        category: LOGIN_CREATE_ACCOUNT_CATEGORY,
        isTriggerHomeAnalytics: true,
        analyticsParams,
        type: url.includes(LOGIN) ? ACCOUNT_CONST_LOGIN : CREATE_ACCOUNT_TYPE,
      };
    } else if (url.includes(HOME)) {
      // ** This else if will match Home
      return {
        isTriggerHomeAnalytics: true,
        analyticsParams,
      };
    } else if (url.includes(CHANGE_PASSWORD)) {
      // ** This else if will match change password
      const {
        query: {logonPasswordOld = '', em = '', email = '', page = ''},
      } = parsedURL;
      const oldPassword = logonPasswordOld ? logonPasswordOld.replace(/\s/g, '+') : '';
      navigateToNestedRoute(navigation, 'AccountStack', 'Account', {
        component: 'change-password',
        logonPasswordOld: oldPassword,
        em,
        email: email ? decode(email) : '',
        page,
        fromDeepLink: true,
        navigatedFromDeepLink: 'change-password',
      });
    }
  }
  return null;
};

export default navigate;
