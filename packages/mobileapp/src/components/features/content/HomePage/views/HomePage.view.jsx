/* eslint-disable max-lines */
/* eslint-disable complexity */
/* eslint-disable consistent-return */
/* eslint-disable spaced-comment */
/* eslint-disable sonarjs/no-collapsible-if */
/* eslint-disable no-unused-expressions */
/* eslint-disable import/no-named-as-default */
/* eslint-disable react/no-unused-state */
/* eslint-disable sonarjs/cognitive-complexity */
// 9fbef606107a605d69c0edbcd8029e5d
import AsyncStorage from '@react-native-community/async-storage';
import InitialPropsHOC from '@tcp/core/src/components/common/hoc/InitialPropsHOC/InitialPropsHOC.native';
import {
  ModuleA,
  ModuleB,
  ModuleD,
  ModuleE,
  ModuleG,
  ModuleJ,
  ModuleK,
  ModuleL,
  ModuleM,
  ModuleN,
  ModuleTwoCol,
  OutfitCarouseModule,
} from '@tcp/core/src/components/common/molecules';
import CategoryCarousel from '@tcp/core/src/components/common/molecules/CategoryCarousel';
import LoyaltyPromoBanner from '@tcp/core/src/components/common/molecules/LoyaltyPromoBanner';
import BTSModal from '@tcp/core/src/components/common/molecules/Modal/index';
import ModuleContainer from '@tcp/core/src/components/common/molecules/ModuleContainer';
import ModuleFont from '@tcp/core/src/components/common/molecules/ModuleFont';
import ModuleT from '@tcp/core/src/components/common/molecules/ModuleT';
import ModuleX from '@tcp/core/src/components/common/molecules/ModuleX';
import PageSlots from '@tcp/core/src/components/common/molecules/PageSlots';
import RecommendationsConstants from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.constants';
import SMSTapToJoin from '@tcp/core/src/components/common/molecules/SMSTapToJoin';
import OpenLoginModal from '@tcp/core/src/components/features/account/LoginPage/container/LoginModal.container';
import {ENV_PREVIEW} from '@tcp/core/src/constants/env.config';
import HP_NEW_LAYOUT from '@tcp/core/src/constants/hpNewLayout.constants';
import {TIER1} from '@tcp/core/src/constants/tiering.constants';
import {getCartData} from '@tcp/core/src/services/abstractors/CnC/CartItemTile';
import {storeResponseParserMapbox} from '@tcp/core/src/services/abstractors/common/storeLocator/storeLocator';
import {API_CONFIG} from '@tcp/core/src/services/config';
import {executeExternalAPICall} from '@tcp/core/src/services/handler';
import {
  getAPIConfig,
  getLabelValue,
  LAZYLOAD_HOST_NAME,
  resetNavigationStack,
} from '@tcp/core/src/utils';
import {readCookie} from '@tcp/core/src/utils/cookie.util';
import logger from '@tcp/core/src/utils/loggerInstance';
import {isGymboree} from '@tcp/core/src/utils/utils';
//import GetCandid from '@tcp/core/src/components/common/molecules/GetCandid/index.native';
import {isIOS} from '@tcp/core/src/utils/utils.app';
import {arrayOf, bool, func, shape, string} from 'prop-types';
import queryString from 'query-string';
import React from 'react';
import {Animated, Dimensions, Linking, Modal, ScrollView, View} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {NavigationActions, StackActions} from 'react-navigation';
import withErrorBoundary from '../../../../common/hoc/ErrorBoundary';
import ApplePayPromo from '../../../../common/molecules/ApplePayPromo';
import BackToSchoolPromo from '../../../../common/molecules/BackToSchoolPromo';
import BackToSchoolErrorMsg from '../../../../common/molecules/BackToSchoolPromo/BackToSchoolErrorMsg';
import HomeNewHeader from '../../../../common/molecules/Header/HomeNewHeader';
import HeaderPromo from '../../../../common/molecules/HeaderPromo';
import PreviewDate from '../../../../common/molecules/PreviewDate';
import Recommendations from '../../../../common/molecules/Recommendations';
import GlobalModals from '../../../../common/organisms/GlobalModals';
import ShopByProfile from '../../../shopByProfile';
import HomeOverlay from '../../../shopByProfile/HomeOverlay';
import InAppWebView from '../../InAppWebView/views/InAppWebView.view';
import HomeCarousel from '../component/HomeCarousel';
import DEEPLINK_CONSTANTS from '../Deeplink.constants';
import navigate from '../DeepLink.utils';
import {HOMEPAGE_CONSTANTS} from '../HomePage.constants';
import {
  BannerSeparator,
  createProfileContainerStyle,
  SMSTapToJoinContainer,
  SMSTapToJoinSeparator,
  StyledPageSlotsWrapper,
} from '../HomePage.style';

const modulesMap = {
  moduleD: ModuleD,
  moduleK: ModuleK,
  moduleL: ModuleL,
  moduleM: ModuleM,
  moduleN: ModuleN,
  moduleA: ModuleA,
  moduleB: ModuleB,
  moduleJ: ModuleJ,
  moduleT: ModuleT,
  moduleE: ModuleE,
  moduleG: ModuleG,
  moduleX: ModuleX,
  outfitCarousel: OutfitCarouseModule,
  module2columns: ModuleTwoCol,
  module_container: ModuleContainer,
  moduleFont: ModuleFont,
};

const modulesMapWithErrorBoundary = Object.keys(modulesMap).reduce((modulesMapObj, key) => {
  const modulesMapWithErrorsBoundary = modulesMapObj;
  const Module = modulesMap[key];
  modulesMapWithErrorsBoundary[key] = (props) => withErrorBoundary(Module)(props);
  return modulesMapWithErrorsBoundary;
}, {});

let currentScrollValue = 0;

const DEFAULT_RADIUS = 75;
export class HomePageView extends React.PureComponent {
  isResetStack = false;

  constructor(props) {
    super(props);
    this.submitDate = this.submitDate.bind(this);
    this.analyticsLoaded = false;
    this.isInitialLinkCaptured = false;
    this.state = {
      isBootStrapDone: true,
      handeOpenURLRegister: false,
      value: '',
      customNavigationID: null,
      showLoginModal: false,
      loginType: null,
      loadSlots: false,
      showHeader: false,
      active: false,
      loadMoreSlots: false,
      getCartCall: false,
    };
  }

  /**
   * @function componentDidMount
   *
   * @loadNavigationData : load navigation data .
   *
   * @isStatus : To manage the bootstrap call in componentDidMount .
   */
  async componentDidMount() {
    const {
      loadNavigationData,
      trackPageLoad,
      navigation,
      previewDate,
      favStore,
      isEnabledHomeCarousel,
      isEnableHomeCarouselInYourStore,
      isEnableHomeCarouselRewards,
      isEnabledHomeCarouselInYourBag,
    } = this.props;
    this.didFocus = navigation.addListener('didFocus', () => {
      this.setState({active: true, loadMoreSlots: false});
    });
    this.didBlur = navigation.addListener('didBlur', async () => {
      this.setState({active: false, showHeader: false, loadMoreSlots: false});
      this.setAppOpenFromNotificationData();
    });
    const {isBootStrapDone, active} = this.state;
    const {
      screenProps: {checkFirst, closeOverlay},
      isUserLoggedIn,
    } = this.props;
    if (closeOverlay && isUserLoggedIn) {
      await checkFirst('HomeStack');
    }
    if (isBootStrapDone) {
      this.isResetStack = navigation && navigation.getParam('isResetStack');

      if (!this.isResetStack) {
        this.loadBootstrapData();
        loadNavigationData();
        const {handeOpenURLRegister} = this.state;
        if (!handeOpenURLRegister) {
          this.setState({handeOpenURLRegister: true});
          Linking.addEventListener('url', this.handleOpenURL);
        }
        const {HOME_PAGE_ANALYTICS_DATA} = HOMEPAGE_CONSTANTS;
        if (!this.analyticsLoaded) {
          trackPageLoad(HOME_PAGE_ANALYTICS_DATA);
          this.analyticsLoaded = true;
        }
      }
      this.setState({isBootStrapDone: false, value: previewDate});
    }
    const isAppOpenWithNotification = await AsyncStorage.getItem('isAppOpenWithNotification');

    if (
      !favStore &&
      isEnabledHomeCarousel &&
      isEnableHomeCarouselInYourStore &&
      !active &&
      isAppOpenWithNotification !== 'true'
    ) {
      await this.setInYourStoreData();
    }
    if (isEnabledHomeCarousel && isAppOpenWithNotification !== 'true') {
      (isEnableHomeCarouselRewards || isEnabledHomeCarouselInYourBag) &&
        (await getCartData({}).then((res) => {
          const {setCouponsDataHomeCarousal, setCartData} = this.props;
          const {coupons, orderDetails} = res;
          if (isUserLoggedIn) {
            isEnableHomeCarouselRewards && setCouponsDataHomeCarousal(coupons);
          }
          isEnabledHomeCarouselInYourBag && setCartData({...orderDetails});
        }));
    }
  }

  async componentDidUpdate(prevProps) {
    const {
      isCustomNavigate,
      navigation,
      customNavigateToPage,
      customNavigationData,
      notificationId,
      isBootstrapDone,
      isNavigationDataDone,
      cartVal,
      setCouponsDataHomeCarousal,
      isEnabledHomeCarousel,
      subTotal,
      modalState,
      isEnableHomeCarouselInYourStore,
      nearByStore,
      getIsPDPBossBopisEnabled,
      getEnableBossBopisPDPSizeDrawer,
    } = this.props;

    const {getCartCall} = this.state;
    const isAppOpenWithNotification = await AsyncStorage.getItem('isAppOpenWithNotification');
    if (
      (cartVal !== prevProps.cartVal &&
        isEnabledHomeCarousel &&
        isAppOpenWithNotification !== 'true') ||
      (cartVal > 0 &&
        subTotal === 0 &&
        isEnabledHomeCarousel &&
        getCartCall === false &&
        isAppOpenWithNotification !== 'true') ||
      (isEnabledHomeCarousel !== prevProps.isEnabledHomeCarousel &&
        isAppOpenWithNotification !== 'true')
    ) {
      this.setState({getCartCall: true});
      await getCartData({}).then((res) => {
        const {setCartData} = this.props;
        const {orderDetails, coupons} = res;
        setCartData({...orderDetails});
        setCouponsDataHomeCarousal(coupons);
      });
    }
    if (modalState !== prevProps.modalState) {
      this.setAppOpenFromNotificationData();
    }

    if (
      isAppOpenWithNotification !== 'true' &&
      ((isEnabledHomeCarousel !== prevProps.isEnabledHomeCarousel &&
        isEnableHomeCarouselInYourStore !== prevProps.isEnableHomeCarouselInYourStore) ||
        getIsPDPBossBopisEnabled !== prevProps.getIsPDPBossBopisEnabled ||
        getEnableBossBopisPDPSizeDrawer !== prevProps.getEnableBossBopisPDPSizeDrawer) &&
      !nearByStore
    ) {
      await this.setInYourStoreData();
    }

    const {customNavigationID} = this.state;
    const {requiredNavigationData} = customNavigationData;
    const callCustomNavigation = requiredNavigationData ? isNavigationDataDone : true;
    if (isNavigationDataDone && isBootstrapDone && !this.isInitialLinkCaptured) {
      this.isInitialLinkCaptured = true;
      Linking.getInitialURL().then((url) => {
        this.handleOpenURL({url});
      });
    }
    if (
      navigation &&
      isCustomNavigate &&
      isBootstrapDone &&
      notificationId !== customNavigationID &&
      callCustomNavigation
    ) {
      const payload = {
        navigation,
        NavigationActions,
        StackActions,
      };
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState(
        {
          customNavigationID: notificationId,
        },
        () => {
          customNavigateToPage(payload);
        },
      );
    }
  }

  componentWillUnmount() {
    this.setState({isBootStrapDone: true});
    if (!this.isResetStack) {
      Linking.removeEventListener('url', this.handleOpenURL);
      this.setState({handeOpenURLRegister: false});
    }
  }

  setAppOpenFromNotificationData = async () => {
    await AsyncStorage.setItem('isAppOpenWithNotification', 'false');
  };

  /**
   * @function shouldSetApiConfig
   * set Api config if brand is toggled
   * or if config data do not have required values
   * @memberof HomePageView
   */
  shouldSetApiConfig = (configData) => {
    const configIsEmpty = !configData || (configData && !Object.keys(configData).length);
    return configIsEmpty || (configData && configData.isBrandSwitched);
  };

  setInYourStoreData = () => {
    const {setUserNearStore} = this.props;
    const latLong = readCookie('tcpGeoLoc');
    if (latLong) {
      latLong.then((response) => {
        if (response) {
          const {mapBoxApiKey, mapboxStoreLocatorApiURL} = getAPIConfig();
          const [lat, long] = response.split('|');
          const apiUrl = `${mapboxStoreLocatorApiURL}/childrensplace.store-locator/tilequery/${long},${lat}.json?radius=${
            DEFAULT_RADIUS * 1000
          }&limit=${1}&access_token=${mapBoxApiKey}`;
          const payload = {
            webService: {
              URI: apiUrl,
              method: 'GET',
            },
          };

          return executeExternalAPICall(payload)
            .then((res) => {
              let fetchedStores = res.body.features || [];
              if (!fetchedStores.length) {
                this.errorHandler({errorCode: 'NO_STORES_FOUND'});
              }
              fetchedStores = [
                ...new Map(
                  fetchedStores.map((store) => [store?.properties?.storeLocId, store]),
                ).values(),
              ];
              setUserNearStore(fetchedStores.map(storeResponseParserMapbox));
            })
            .catch((err) => {
              this.errorHandler(err);
              return [];
            });
        }
      });
    }
  };

  errorHandler = (err) => {
    return err || null;
  };

  /**
   * @function loadBootstrapData
   * Loads bootstrap data
   *
   * @memberof HomePageView
   */
  loadBootstrapData = () => {
    const {
      getBootstrapData,
      setApiConfigData,
      getPageLayout,
      screenProps: {apiConfig},
    } = this.props;
    const configData = getAPIConfig();
    if (this.shouldSetApiConfig(configData)) {
      setApiConfigData(apiConfig);
    }
    if (configData?.brandId) {
      getPageLayout('home', configData);
      getBootstrapData(
        {
          name: 'home',
          modules: ['header'], // labels will be queried separately
        },
        apiConfig,
      );
    }
  };

  /**
   * @function : handleOpenURL
   * This function will be used to redirect user to specific page through deep link
   */

  handleOpenURL = (event) => {
    const {navigation} = this.props;
    const deepLinkReturnedValue = navigate(event.url, queryString, navigation);
    if (deepLinkReturnedValue) {
      this.handleReturendDeeplink(deepLinkReturnedValue);
    }
  };

  /**
   * @function : handleReturendDeeplink
   * This function will handle the Modals thats need to be opened when deep link is captured.
   */
  handleReturendDeeplink = (deepLinkReturnedValue) => {
    const {category, isTriggerHomeAnalytics, analyticsParams} = deepLinkReturnedValue;
    if (category === DEEPLINK_CONSTANTS.CATEGORY.LOGIN_CREATE_ACCOUNT_CATEGORY) {
      const {isUserLoggedIn} = this.props;
      if (!isUserLoggedIn) {
        const {type} = deepLinkReturnedValue;
        this.openLoginCreateAccountModal(type);
      }
    }
    if (isTriggerHomeAnalytics) {
      const {HOME_PAGE_ANALYTICS_DATA} = HOMEPAGE_CONSTANTS;
      const {trackPageLoad} = this.props;
      setTimeout(() => {
        trackPageLoad({
          ...HOME_PAGE_ANALYTICS_DATA,
          customEvents: [...HOME_PAGE_ANALYTICS_DATA.customEvents, 'event18'],
          otherParams: analyticsParams,
        });
      }, 300);
    }
  };

  /**
   * @function : openLoginCreateAccountModal
   * This function will be used to open the Login/Create Account Modal
   */

  openLoginCreateAccountModal = (loginType) => {
    this.setState({
      showLoginModal: true,
      loginType,
    });
  };

  /**
   * @function : setLoginModal
   * This function will be called on close of login modal.
   */

  setLoginModal = (params) => {
    this.setState({
      showLoginModal: params && params.state,
    });
  };

  /**
   * @function submitDate
   * Submit date for scheduled preview and it
   * will be submitted to graphql along with query
   *
   * @memberof HomePageView
   */
  submitDate = () => {
    const {loadNavigationData, navigation, updatePreviewDate} = this.props;
    const {value} = this.state;
    updatePreviewDate(value);
    this.loadBootstrapData();
    loadNavigationData();
    resetNavigationStack(navigation);
  };

  /**
   * @function submitDate
   * will be submitted to graphql along with query
   *
   * @memberof HomePageView
   */
  refreshPreviewData = () => {
    const {loadNavigationData, navigation, updatePreviewDate} = this.props;
    updatePreviewDate('');
    this.loadBootstrapData();
    loadNavigationData();
    resetNavigationStack(navigation);
  };

  getMerkleUrl = async () => {
    try {
      const {getMerkleUrl} = this.props;
      getMerkleUrl();
    } catch (error) {
      logger.error('HOMEPAGE_getMerkleUrl', error);
    }
  };

  showBackToSchoolPromo = (slotsData) => {
    try {
      const slots = JSON.parse(slotsData);
      return slots && slots.some(({name}) => name === 'slot_2');
    } catch (e) {
      logger.error('Error in HomePage.view.jsx :: showBackToSchoolPromo :: ', e);
      return false;
    }
  };

  renderBackToSchoolPromo = () => {
    const {
      slots,
      navigation,
      labels,
      screenProps: {apiConfig},
    } = this.props;

    const imageUrl = '/ecom/assets/content/tcp/us/25DaysofGiving/daysofgiving.png';
    const labelUrl = labels?.global?.appleHomePagePromo?.lbl_giveaway_img_url || imageUrl;
    const imageAlt = 'Win $1000 During Our 25 Days Of Giving';
    const {width} = Dimensions.get('window');
    const imgHeight = 250;
    const showBTSBanner = this.showBackToSchoolPromo(slots);
    const {brandId} = apiConfig;
    return showBTSBanner && brandId === 'tcp' ? (
      <BackToSchoolPromo
        onPress={this.getMerkleUrl}
        imgUrl={labelUrl}
        imageWidth={width}
        imageHeight={imgHeight}
        alt={imageAlt}
        navigation={navigation}
        labels={labels}
        holidayGiveAway
      />
    ) : null;
  };

  modalActionComplete = () => {
    const {
      screenProps: {onInitialModalsActionComplete},
    } = this.props;
    this.setState({loadSlots: true});
    onInitialModalsActionComplete();
  };

  showHomeHeader = () => {
    return (
      <Animated.View>
        <HomeNewHeader stickyHeader={true} {...this.props} />
      </Animated.View>
    );
  };

  loadMoreSlots = ({layoutMeasurement, contentOffset, contentSize}) => {
    return layoutMeasurement?.height + contentOffset?.y >= contentSize?.height / 2;
  };

  handleScroll = (event) => {
    const {showHeader} = this.state;
    const loadMore = this.loadMoreSlots(event.nativeEvent);
    if (loadMore) {
      this.setState({loadMoreSlots: true});
    }
    currentScrollValue = event.nativeEvent.contentOffset.y;
    if (currentScrollValue > 320) {
      if (!showHeader) {
        this.setState({showHeader: true});
      }
    } else {
      this.setState({showHeader: false});
    }
  };

  renderModal = () => {
    const {
      navigation,
      labels,
      showBTSModal,
      setBTSModal,
      screenProps: {apiConfig},
    } = this.props;
    const {merkleWebViewUrl} = apiConfig;
    const webViewUrl = labels?.global?.appleHomePagePromo?.lbl_giveaway_url || merkleWebViewUrl;
    return (
      <BTSModal
        isOpen={showBTSModal}
        overlayClassName="TCPModal__Overlay"
        className="TCPModal__Content"
        fixedWidth
        fullWidth
        onRequestClose={setBTSModal}
        widthConfig={{small: '375px', medium: '600px', large: '704px'}}
        heightConfig={{
          minHeight: '534px',
          height: '620px',
          maxHeight: '650px',
        }}
        headingAlign="center"
        heading
        stickyHeader
        stickyCloseIcon
        horizontalBar={false}
        isBTSWebView>
        {webViewUrl !== '' ? (
          <InAppWebView pageUrl={webViewUrl} navigation={navigation} isBTSWebView />
        ) : (
          <BackToSchoolErrorMsg labels={labels} />
        )}
      </BTSModal>
    );
  };

  renderRecommendations = () => {
    const {
      navigation,
      screenProps: {deviceTier},
      recentlyViewedLabel,
      quickViewLoader,
      quickViewProductId,
    } = this.props;
    return (
      <>
        <Recommendations
          navigation={navigation}
          showButton
          variation="moduleO"
          page={RecommendationsConstants.RECOMMENDATIONS_PAGES_MAPPING.HOMEPAGE}
          sequence="1"
          isHomePage
          isCardTypeTiles
          productImageWidth={130}
          productImageHeight={161}
          paddings="0px"
          margins="5px 6px 18px"
          quickViewLoader={quickViewLoader}
          quickViewProductId={quickViewProductId}
        />
        {deviceTier === TIER1 && (
          <Recommendations
            navigation={navigation}
            isRecentlyViewed
            page={RecommendationsConstants.RECOMMENDATIONS_PAGES_MAPPING.HOMEPAGE}
            variation="moduleO"
            portalValue={RecommendationsConstants.RECOMMENDATIONS_MBOXNAMES.RECENTLY_VIEWED}
            headerLabel={recentlyViewedLabel}
            sequence="2"
            isHomePage
            isCardTypeTiles
            productImageWidth={130}
            productImageHeight={161}
            paddings="0px"
            margins="5px 6px 18px"
            quickViewLoader={quickViewLoader}
            quickViewProductId={quickViewProductId}
          />
        )}
      </>
    );
  };

  renderSlots = () => {
    const {slots, navigation, isHpNewLayoutEnabled} = this.props;
    const {loadMoreSlots} = this.state;
    return (
      <StyledPageSlotsWrapper isHpNewLayoutEnabled={isHpNewLayoutEnabled}>
        <PageSlots
          slots={slots}
          modules={modulesMapWithErrorBoundary}
          navigation={navigation}
          isHomePage
          isHpNewLayoutEnabled={isHpNewLayoutEnabled}
          loadMoreSlots={loadMoreSlots}
        />
      </StyledPageSlotsWrapper>
    );
  };

  renderLoyalty = () => {
    const {loyaltyPromoBanner, isUserGuest, isUserPlcc} = this.props;
    return (
      <LoyaltyPromoBanner
        richTextList={loyaltyPromoBanner}
        isUserGuest={isUserGuest}
        isUserPlcc={isUserPlcc}
      />
    );
  };

  /*eslint-disable */
  renderApplePayPromo = () => {
    const {navigation, applePayHpBannerAbTestEnabled, labels} = this.props;
    if (isIOS() && applePayHpBannerAbTestEnabled) {
      const applePayPromoLabels = labels?.global?.appleHomePagePromo;
      return <ApplePayPromo navigation={navigation} labels={applePayPromoLabels} />;
    }
    return null;
  };

  renderBannerSeparator = () => {
    const {applePayHpBannerAbTestEnabled} = this.props;
    if (isIOS() && applePayHpBannerAbTestEnabled) return <BannerSeparator />;
    return null;
  };

  // eslint-disable-next-line complexity
  // eslint-disable-next-line sonarjs/cognitive-complexity
  render() {
    const {
      isSBPEnabled: sbpEnabled,
      navigation,
      screenProps: {
        apiConfig,
        apiConfig: {brandId},
        deviceTier,
        isSplashVisible,
        closeOverlay,
        gotoOutSideClick,
        checkFirst,
      },
      loyaltyPromoBanner,
      labels,
      headerPromo,
      promoHtmlBannerCarousel,
      isUserLoggedIn,
      isSbpGymEnabled,
      isBootstrapDone,
      isNavigationDataDone,
      isHpNewLayoutEnabled,
      isEnabledSBPRedesign,
      isEnabledHomeCarousel,
      isEnabledHomeCarouselInYourBag,
      isEnableHomeCarouselInYourStore,
      isEnableHomeCarouselRewards,
    } = this.props;
    const {value, showLoginModal, loginType, loadSlots, active, showHeader} = this.state;

    const headerBgImage = getLabelValue(
      labels,
      'lbl_header_bgImage_mobile_url',
      'header',
      'global',
    );
    const isSBPEnabled =
      sbpEnabled && isUserLoggedIn && (isSbpGymEnabled || brandId !== API_CONFIG.brandIds.gym);

    const hpBackgroundStyle = {
      backgroundColor: HP_NEW_LAYOUT.HP_BG_COLOR,
    };
    const contentContainerStylesObj = {
      paddingBottom: 50,
    };

    const isGymApp = !!isGymboree();

    return (
      <>
        <View>
          {showHeader ? this.showHomeHeader() : null}
          <ScrollView
            scrollEventThrottle={160}
            onScroll={this.handleScroll}
            onMomentumScrollEnd={this.handleScroll}
            onScrollEndDrag={this.handleScroll}
            name={LAZYLOAD_HOST_NAME.HOME}
            style={isHpNewLayoutEnabled && hpBackgroundStyle}
            contentContainerStyle={contentContainerStylesObj}
            bounces={false}>
            <View style={createProfileContainerStyle.viewFour}>
              {isGymApp ? (
                <LinearGradient
                  start={{x: 0, y: 0}}
                  end={{x: 0, y: 1}}
                  colors={[
                    'rgba(251, 213, 165, 1)',
                    'rgba(251, 213, 165, 0.51)',
                    'rgba(251, 213, 165, 0)',
                  ]}
                  style={createProfileContainerStyle.viewFourGrad}
                />
              ) : (
                <LinearGradient
                  start={{x: 0, y: 0}}
                  end={{x: 0, y: 1}}
                  colors={[
                    'rgba(75, 159, 221, 1)',
                    'rgba(75, 159, 221, 0.51)',
                    'rgba(75, 159, 221, 0)',
                  ]}
                  style={createProfileContainerStyle.viewFourGrad}
                />
              )}
            </View>
            <HomeNewHeader {...this.props} />
            <HomeCarousel
              {...this.props}
              ENABLE_HOME_CAROUSEL={isEnabledHomeCarousel}
              ENABLE_HOME_CAROUSEL_IN_YOUR_BAG={isEnabledHomeCarouselInYourBag}
              ENABLE_HOME_CAROUSEL_IN_YOUR_STORE={isEnableHomeCarouselInYourStore}
              ENABLE_HOME_CAROUSEL_REWARDS={isEnableHomeCarouselRewards}
            />
            <HeaderPromo
              headerPromo={headerPromo}
              promoHtmlBannerCarousel={promoHtmlBannerCarousel}
              headerBgImage={headerBgImage}
              navigation={navigation}
              addBackNavigation
            />
            {loyaltyPromoBanner.length > 0 ? this.renderLoyalty() : null}
            <CategoryCarousel navigation={navigation} isEnabled />
            {apiConfig.previewEnvId === ENV_PREVIEW ? (
              <PreviewDate
                onSubmit={this.submitDate}
                onRefresh={this.refreshPreviewData}
                onPreviewChange={(text) => this.setState({value: text})}
                labels={labels}
                value={value}
              />
            ) : null}
            {this.renderBackToSchoolPromo()}
            {this.renderBannerSeparator()}
            {this.renderApplePayPromo()}
            {active && loadSlots ? this.renderSlots() : null}
            {/* {deviceTier === TIER1 && (
              <GetCandid
                apiConfig={apiConfig}
                navigation={navigation}
                openedFromStack="HomeStack"
              />
            )} */}
            {active && labels && Object.keys(labels).length ? this.renderRecommendations() : null}
            {showLoginModal && !isUserLoggedIn && (
              <OpenLoginModal
                component={loginType}
                openState={showLoginModal}
                setLoginModalMountState={this.setLoginModal}
              />
            )}
            <GlobalModals
              navigation={navigation}
              onInitialModalsActionComplete={this.modalActionComplete}
              checkFirst={checkFirst}
              isFromHomePage={true}
            />
            {isBootstrapDone && isNavigationDataDone && (
              <>
                <SMSTapToJoinSeparator />
                <SMSTapToJoinContainer>
                  <SMSTapToJoin navigation={navigation} smsFromPage="footer" isHomePage />
                </SMSTapToJoinContainer>
              </>
            )}
          </ScrollView>
          {!isSplashVisible && closeOverlay && isSBPEnabled && isEnabledSBPRedesign ? (
            <Modal transparent visible>
              <HomeOverlay
                navigation={navigation}
                closeOverlay={closeOverlay}
                gotoOutSideClick={gotoOutSideClick}
              />
            </Modal>
          ) : null}
          {isUserLoggedIn && (!closeOverlay || !isEnabledSBPRedesign) && isSBPEnabled ? (
            <ShopByProfile navigation={navigation} onCardOpen={() => {}} />
          ) : null}
        </View>
        {this.renderModal()}
      </>
    );
  }
}

HomePageView.propTypes = {
  customNavigateToPage: func.isRequired,
  customNavigationData: shape([]),
  getBootstrapData: func.isRequired,
  getMerkleUrl: func.isRequired,
  getPageLayout: func.isRequired,
  headerPromo: shape({}),
  isBootstrapDone: bool.isRequired,
  isCustomNavigate: bool.isRequired,
  isHpNewLayoutEnabled: bool.isRequired,
  isNavigationDataDone: bool.isRequired,
  isSBPEnabled: bool.isRequired,
  isEnabledSBPRedesign: bool.isRequired,
  isSbpGymEnabled: bool.isRequired,
  isUserGuest: bool.isRequired,
  isUserLoggedIn: bool.isRequired,
  isUserPlcc: bool.isRequired,
  labels: shape({}).isRequired,
  loadNavigationData: func,
  loyaltyPromoBanner: shape([]),
  merkleUrl: string.isRequired,
  navigation: shape({}).isRequired,
  notificationId: string.isRequired,
  previewDate: string,
  promoHtmlBannerCarousel: shape([]),
  recentlyViewedLabel: string,
  screenProps: shape({
    apiConfig: shape({
      brandId: string,
      previewEnvId: string,
    }),
  }),
  setApiConfigData: func.isRequired,
  setBTSModal: func.isRequired,
  showBTSModal: bool.isRequired,
  slots: arrayOf(
    shape({
      contentId: string,
      data: shape({}),
      moduleName: string,
      name: string,
    }),
  ).isRequired,
  trackPageLoad: func.isRequired,
  updatePreviewDate: func,
  quickViewLoader: bool,
  quickViewProductId: string,
  applePayHpBannerAbTestEnabled: bool,
  cartVal: Number.isRequired,
  setCartData: func.isRequired,
  setCouponsDataHomeCarousal: func.isRequired,
  isEnableHomeCarouselInYourStore: bool.isRequired,
  isEnableHomeCarouselRewards: bool.isRequired,
  isEnabledHomeCarousel: bool.isRequired,
  isEnabledHomeCarouselInYourBag: bool.isRequired,
};

HomePageView.defaultProps = {
  screenProps: {},
  loadNavigationData: () => {},
  updatePreviewDate: () => {},
  loyaltyPromoBanner: [],
  headerPromo: {},
  promoHtmlBannerCarousel: [],
  customNavigationData: {
    deepLink: '',
    requiredNavigationData: false,
  },
  previewDate: '',
  recentlyViewedLabel: '',
  quickViewLoader: false,
  quickViewProductId: '',
  applePayHpBannerAbTestEnabled: false,
};

export default InitialPropsHOC(HomePageView);
