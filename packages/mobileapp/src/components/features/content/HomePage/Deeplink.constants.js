// 9fbef606107a605d69c0edbcd8029e5d 
import URL_PATTERN from '@tcp/core/src/constants/urlPatterns.constants';

/**
 * @description - This will be used to call different function if needs to be called based on deep link route
 */
const CATEGORY = {
  LOGIN_CREATE_ACCOUNT_CATEGORY: 'login_create_account_modal',
};

/**
 * @description - These are constants which will be used to open the login & Create account Modal.
 */
const LOGIN_CREATE_ACCOUNT_CONST = {
  LOGIN_TYPE: 'login',
  CREATE_ACCOUNT_TYPE: 'createAccount',
};

export default {URL_PATTERN, CATEGORY, LOGIN_CREATE_ACCOUNT_CONST};
