// 9fbef606107a605d69c0edbcd8029e5d
export const HOMEPAGE_CONSTANTS = {
  FETCH_HEADER_LINKS: 'FETCH_HEADER_LINKS',
  SET_HEADER_LINKS: 'ADD_HEADER_LINKS',
  FETCH_ESPOT: 'FETCH_ESPOT',
  SET_ESPOST: 'ADD_INDEX_ESPOTS',
  HOME_PAGE_ANALYTICS_DATA: {
    customEvents: ['event80'],
    pageName: 'home page',
    pageSection: 'homepage',
    pageSubSection: 'home page',
    pageType: 'home page',
  },
  SET_HOMEPAGE_MODAL: 'SET_HOMEPAGE_MODAL',
  SET_MERKLE_URL: 'SET_MERKLE_URL',
  GET_MERKLE_URL: 'GET_MERKLE_URL',
  CLEAR_MERKLE_URL: 'CLEAR_MERKLE_URL',
  LOYALTY: 'LOYALTY',
  SAVING: 'saving',
  PLACECASH: 'PLACECASH',
  PC: 'PC',
  HomeCarousel_Priority: {
    MPRReward: 3,
    InYourBag: 5,
    YourStore: 6,
  },
  HOME_CAROUSEL_ANALYTICS_DATA: {
    customEvents: ['event81'],
    internalCampaignId: 'Internal_campaigns_v19',
    hpCardsMyPlaceReward: 'hpcards_myplacereward',
    hpCardsInYourBag: 'hpcards_inyourbag',
    hpCardsInYourStore: 'hpcards_inyourstore',
    hpEvents: 'homePageCardClicks',
  },
};

export default HOMEPAGE_CONSTANTS;
