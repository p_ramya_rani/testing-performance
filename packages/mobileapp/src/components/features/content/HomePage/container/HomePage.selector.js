// 9fbef606107a605d69c0edbcd8029e5d
import {createSelector} from 'reselect';
import {updateHeaderPromoStyleOverride, parseBoolean} from '@tcp/core/src/utils';
import logger from '@tcp/core/src/utils/loggerInstance';
import {SESSIONCONFIG_REDUCER_KEY} from '@tcp/core/src/constants/reducer.constants';

const stateSelector = (state) => state;
export const layoutsSelector = (state) => state.Layouts;
export const labelsSelector = (state) => state.Labels;

export const globalLabelSelector = createSelector(labelsSelector, (Labels) => Labels.global || {});

export const globalAccessibilityLabelsSelector = createSelector(
  globalLabelSelector,
  (global) => global.accessibility,
);

export const homePageLayoutSelector = createSelector(
  layoutsSelector,
  (Layouts) => Layouts.home || {},
);

export const homePageSlotsSelector = createSelector(homePageLayoutSelector, (home) => home.slots);

export const slotModulesDataSelector = createSelector(
  stateSelector,
  homePageSlotsSelector,
  globalAccessibilityLabelsSelector,
  (state = {}, homepageSlots = [], accessibility = {}) => {
    const {Modules} = state;
    const slotsData = homepageSlots
      .map((slot) => {
        // Logic for accommodating two modules in one slot (Half width modules view)
        const {contentId: slotContent = ''} = slot;
        const contentIds = slotContent && slotContent.split(',');
        if (contentIds && contentIds.length > 1) {
          const response = {
            ...slot,
            accessibility,
            data: {
              slot: [],
            },
          };

          contentIds.forEach((contentId) => {
            response.data.slot.push(updateHeaderPromoStyleOverride(Modules[contentId]));
          });

          return response;
        }
        // Logic ends
        return {
          ...slot,
          accessibility,
          data: updateHeaderPromoStyleOverride(Modules[slot.contentId]),
        };
      })
      .filter((item) => item.data);
    try {
      return JSON.stringify(slotsData);
    } catch (e) {
      logger.error('Error in HomePage.selector.js :: slotModulesDataSelector :: ', e);
      return '';
    }
  },
);

export const isHomePageModalSelector = (state) => {
  return state && state.HomePageReducer && state.HomePageReducer.showHomePageModal;
};

export const isMerkleUrlSelector = (state) => {
  return state && state.HomePageReducer && state.HomePageReducer.merkleUrl;
};

export const getEnableCarouselInYourBag = (state) => {
  // Kill switch Handling for ENABLE_HOME_CAROUSEL_IN_YOUR_BAG
  return (
    state &&
    state[SESSIONCONFIG_REDUCER_KEY] &&
    parseBoolean(state[SESSIONCONFIG_REDUCER_KEY].siteDetails.ENABLE_HOME_CAROUSEL_IN_YOUR_BAG)
  );
};

export const getEnableCarouselInYourStore = (state) => {
  // Kill switch Handling for ENABLE_HOME_CAROUSEL_IN_YOUR_STORE
  return (
    state &&
    state[SESSIONCONFIG_REDUCER_KEY] &&
    parseBoolean(state[SESSIONCONFIG_REDUCER_KEY].siteDetails.ENABLE_HOME_CAROUSEL_IN_YOUR_STORE)
  );
};

export const getEnableCarouselRewards = (state) => {
  // Kill switch Handling for ENABLE_HOME_CAROUSEL_REWARDS
  return (
    state &&
    state[SESSIONCONFIG_REDUCER_KEY] &&
    parseBoolean(state[SESSIONCONFIG_REDUCER_KEY].siteDetails.ENABLE_HOME_CAROUSEL_REWARDS)
  );
};

export const getEnabledHomeCarousel = (state) => {
  // Kill Switch Handling for ENABLE_HOME_CAROUSEL
  return (
    state &&
    state[SESSIONCONFIG_REDUCER_KEY] &&
    parseBoolean(state[SESSIONCONFIG_REDUCER_KEY].siteDetails.ENABLE_HOME_CAROUSEL)
  );
};
export const getDisableCodepushOptimisation = (state) => {
  // Kill Switch Handling for DISABLE_CODEPUSH_OPTIMISATION
  return (
    state &&
    state[SESSIONCONFIG_REDUCER_KEY] &&
    parseBoolean(state[SESSIONCONFIG_REDUCER_KEY].siteDetails.DISABLE_CODEPUSH_OPTIMISATION)
  );
};

export default {
  globalLabelSelector,
  globalAccessibilityLabelsSelector,
  homePageLayoutSelector,
  homePageSlotsSelector,
  slotModulesDataSelector,
  isHomePageModalSelector,
  isMerkleUrlSelector,
  getEnableCarouselInYourBag,
  getEnableCarouselInYourStore,
  getEnableCarouselRewards,
  getEnabledHomeCarousel,
};
