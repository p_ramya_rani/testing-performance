// 9fbef606107a605d69c0edbcd8029e5d
import {HOMEPAGE_CONSTANTS} from '../HomePage.constants';

export const getHeaderlinks = payload => {
  return {
    payload,
    type: HOMEPAGE_CONSTANTS.FETCH_HEADER_LINKS,
  };
};

export const setHeaderlinks = payload => {
  return {
    payload,
    type: HOMEPAGE_CONSTANTS.SET_HEADER_LINKS,
  };
};

export const getEspots = payload => {
  return {
    payload,
    type: HOMEPAGE_CONSTANTS.FETCH_ESPOT,
  };
};

export const setEspots = payload => {
  return {
    payload,
    type: HOMEPAGE_CONSTANTS.SET_ESPOST,
  };
};

export const setBTSModal = () => {
  return {
    type: HOMEPAGE_CONSTANTS.SET_HOMEPAGE_MODAL,
  };
};

export const getMerkleUrl = () => {
  return {
    type: HOMEPAGE_CONSTANTS.GET_MERKLE_URL,
  };
};

export const setMerkleUrl = payload => {
  return {
    payload,
    type: HOMEPAGE_CONSTANTS.SET_MERKLE_URL,
  };
};

export const clearMerkleUrl = () => {
  return {
    type: HOMEPAGE_CONSTANTS.CLEAR_MERKLE_URL,
  };
};
