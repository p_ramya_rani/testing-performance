// 9fbef606107a605d69c0edbcd8029e5d
import {HOMEPAGE_CONSTANTS} from '../HomePage.constants';

const INITIAL_STATE = {
  links: [],
  eSpots: [],
  showHomePageModal: false,
  merkleUrl: '',
};

const HomePageReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case HOMEPAGE_CONSTANTS.SET_HEADER_LINKS:
      return {...state, links: action.payload};
    case HOMEPAGE_CONSTANTS.SET_ESPOST:
      return {...state, eSpots: action.payload};
    case HOMEPAGE_CONSTANTS.SET_HOMEPAGE_MODAL:
      return {...state, showHomePageModal: !state.showHomePageModal};
    case HOMEPAGE_CONSTANTS.SET_MERKLE_URL:
      return {...state, merkleUrl: action.payload.result.url};
    case HOMEPAGE_CONSTANTS.CLEAR_MERKLE_URL:
      return {...state, merkleUrl: ''};
    default:
      return state;
  }
};

export default HomePageReducer;
