// 9fbef606107a605d69c0edbcd8029e5d
import {connect} from 'react-redux';
import {
  bootstrapData,
  setPreviewDate,
  setAPIConfig,
  fetchPageLayout,
} from '@tcp/core/src/reduxStore/actions';
import {fetchNavigationData} from '@tcp/core/src/components/features/content/Navigation/container/Navigation.actions';
import {navigateTo} from '@tcp/core/src/components/features/content/CustomNavigation/container/CustomNavigation.actions';
import {trackPageView, setClickAnalyticsData} from '@tcp/core/src/analytics/actions';

import {
  getNavigationState,
  getNavigationData,
  getNotificationId,
  getIsBootstrapDone,
  getIsNavigationDataDone,
} from '@tcp/core/src/components/features/content/CustomNavigation/container/CustomNavigation.selectors';
import {
  isPlccUser,
  getIsGuest,
  getUserLoggedInState,
  getUserBirthday,
  getUserFullName,
  getProfileInfoTileData,
  getUserNearStoreState,
} from '@tcp/core/src/components/features/account/User/container/User.selectors';
import {getFavoriteStoreActn} from '@tcp/core/src/components/features/storeLocator/StoreLanding/container/StoreLanding.actions';
import {
  getPreviewDate,
  getIsCategoryCarouselEnabled,
  getIsHpNewLayoutEnabled,
  getApplePayHpBannerAbTestEnabled,
} from '@tcp/core/src/reduxStore/selectors/session.selectors';
import {getLabelValue} from '@tcp/core/src/utils';
import {
  getModalState,
  getUniqueId,
} from '@tcp/core/src/components/common/organisms/QuickViewModal/container/QuickViewModal.selectors';
import {getQuickViewLoader} from '@tcp/core/src/components/common/molecules/Loader/container/Loader.selector';
import BagPageSelectors from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.selectors';
import bagPageActions from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.actions';
import {setUserNearStoreData} from '@tcp/core/src/components/features/account/User/container/User.actions';
import {
  getEnableBossBopisPDPSizeDrawer,
  getIsPDPBossBopisEnabled,
} from '@tcp/core/src/components/common/organisms/ProductPickup/container/ProductPickup.selectors';
import {setBTSModal, getMerkleUrl} from './HomePage.actions';
import HomePageView from '../views';
import {THEME_WRAPPER_REDUCER_KEY} from '../../../../common/hoc/ThemeWrapper.constants';
import {
  slotModulesDataSelector,
  isHomePageModalSelector,
  isMerkleUrlSelector,
  getEnableCarouselInYourBag,
  getEnableCarouselInYourStore,
  getEnableCarouselRewards,
  getEnabledHomeCarousel,
} from './HomePage.selector';
import {isSBPEnabled, isSbpGymEnabled} from '../../../shopByProfile/ShopByProfile.selector';
import checkoutSelectors from '../../../../../../../core/src/components/features/CnC/Checkout/container/Checkout.selector';

const mapStateToProps = (state) => {
  const {Header = {}} = state;
  const {promoTextBannerCarousel: headerPromo, loyaltyPromoBanner} = Header;
  const {promoHtmlBannerCarousel} = Header;
  const labels = state.Labels;
  return {
    slots: slotModulesDataSelector(state),
    headerPromo,
    loyaltyPromoBanner,
    appType: state[THEME_WRAPPER_REDUCER_KEY].get('APP_TYPE'),
    labels,
    promoHtmlBannerCarousel,
    isCustomNavigate: getNavigationState(state),
    customNavigationData: getNavigationData(state),
    notificationId: getNotificationId(state),
    isBootstrapDone: getIsBootstrapDone(state),
    isNavigationDataDone: getIsNavigationDataDone(state),
    isUserGuest: getIsGuest(state),
    isUserPlcc: isPlccUser(state),
    previewDate: getPreviewDate(state),
    isUserLoggedIn: getUserLoggedInState(state),
    userBirthday: getUserBirthday(state),
    userFullName: getUserFullName(state),
    userInfo: getProfileInfoTileData(state),
    isSBPEnabled: isSBPEnabled(state),
    isSbpGymEnabled: isSbpGymEnabled(state),
    recentlyViewedLabel: getLabelValue(
      state.Labels,
      'lbl_recently_viewed',
      'PDP_Sub',
      'ProductDetailPage',
    ),
    isEnabled: getIsCategoryCarouselEnabled(state),
    showBTSModal: isHomePageModalSelector(state),
    merkleUrl: isMerkleUrlSelector(state),
    isHpNewLayoutEnabled: getIsHpNewLayoutEnabled(state),
    isEnabledSBPRedesign: checkoutSelectors.getIsAppleSBPReDesignEnabled(state),
    quickViewLoader: getQuickViewLoader(state),
    quickViewProductId: getUniqueId(state),
    applePayHpBannerAbTestEnabled: getApplePayHpBannerAbTestEnabled(state),
    isEnabledHomeCarousel: getEnabledHomeCarousel(state),
    isEnabledHomeCarouselInYourBag: getEnableCarouselInYourBag(state),
    isEnableHomeCarouselInYourStore: getEnableCarouselInYourStore(state),
    isEnableHomeCarouselRewards: getEnableCarouselRewards(state),
    cartVal: state.Header && state.Header.cartItemCount,
    favStore: state.User && state.User.get('defaultStore'),
    nearByStore: getUserNearStoreState(state),
    subTotal: BagPageSelectors.getOrderSubTotal(state),
    modalState: getModalState(state),
    getIsPDPBossBopisEnabled: getIsPDPBossBopisEnabled(state),
    getEnableBossBopisPDPSizeDrawer: getEnableBossBopisPDPSizeDrawer(state),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setApiConfigData: (apiConfig) => {
      dispatch(setAPIConfig(apiConfig));
    },
    getPageLayout: (pageName, apiConfig) => {
      dispatch(fetchPageLayout({pageName, apiConfig}));
    },
    getBootstrapData: (pages, apiConfig) => {
      const payload = {
        ...pages,
        apiConfig,
        siteConfig: true,
      };
      dispatch(bootstrapData(payload));
    },
    loadNavigationData: (payload) => dispatch(fetchNavigationData(payload)),
    updatePreviewDate: (payload) => dispatch(setPreviewDate(payload)),
    trackPageLoad: (payload) => {
      const {products, customEvents, otherParams} = payload;
      dispatch(
        setClickAnalyticsData({
          products,
          customEvents,
          ...(otherParams && {...otherParams}),
        }),
      );
      dispatch(
        trackPageView({
          currentScreen: 'home_page',
          pageData: {
            ...payload,
          },
          props: {
            initialProps: {
              pageProps: {
                pageData: {
                  ...payload,
                },
              },
            },
          },
        }),
      );
    },
    customNavigateToPage: (payload) => {
      dispatch(navigateTo(payload));
    },
    loadFavoriteStore: (payload) => dispatch(getFavoriteStoreActn(payload)),
    setBTSModal: () => dispatch(setBTSModal()),
    getMerkleUrl: () => dispatch(getMerkleUrl()),
    setCartData: (payload) => dispatch(bagPageActions.getOrderDetailsComplete(payload)),
    setCouponsDataHomeCarousal: (payload) =>
      dispatch(bagPageActions.setCouponsDataForHomeCarousel(payload)),
    setUserNearStore: (payload) => {
      dispatch(setUserNearStoreData(payload));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HomePageView);
