// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable */
import React from 'react';
import {shallow} from 'enzyme';
import {YourStore, mapDispatchToProps, mapStateToProps} from '../component/YourStore';
import {HomeCarouselItemWrapper, StyledStoreLocator, TitleText} from '../HomePage.style';

describe('YourStore', () => {
  let component;

  const props = {
    setUserNearStore: jest.fn(),
    getFavStore: jest.fn(),
    getUserNearStoreDataList: jest.fn(),
    headerText: 'ABC',
    detailsText: 'Details',
    onPressHandler: () => {},
    favStore: {
      name: 'saint andreas',
      basicInfo: {
        storeName: 'Find a Store',
        hours: [],
      },
    },
    nearByStore: [
      {
        basicInfo: {
          storeName: 'Find a Store',
          hours: [],
        },
      },
    ],
  };

  beforeEach(() => {
    component = shallow(<YourStore {...props} />);
  });

  it('YourStore should be defined', () => {
    const wrapper = shallow(<YourStore {...props} />);
    expect(wrapper).toBeDefined();
  });

  it('YourStore should render correctly', () => {
    const wrapper = shallow(<YourStore {...props} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should HomeCarouselItemWrapper component render', () => {
    expect(component.find(HomeCarouselItemWrapper)).toHaveLength(1);
  });

  it('should StyledStoreLocator component render', () => {
    expect(component.find(StyledStoreLocator)).toHaveLength(1);
  });

  it('mapStateToProps', () => {
    const state = {
      Labels: {
        global: {
          header: {
            lbl_header_storeDefaultTitle: 'Find a Store',
            lbl_header_welcomeMessage: 'Welcome!',
          },
        },
        StoreLocator: {
          StoreLanding: 'Test',
        },
      },
    };
    const mappedProps = mapStateToProps(state);
    const expected = {
      labels: {
        lbl_header_storeDefaultTitle: 'Find a Store',
        lbl_header_welcomeMessage: 'Welcome!',
      },
      storeLocatorLabels: 'Test',
    };
    expect(mappedProps).toEqual(expected);
  });
});
