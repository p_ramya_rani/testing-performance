// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable */
import React from 'react';
import {shallow} from 'enzyme';
import MPRReward from '../component/MPRReward';

describe('MPR Reward', () => {
  let component;

  const props = {
    navigation: {
      navigate: jest.fn(),
    },
    titleText: 'MPR REWARD',
    priceText: '$5 Reward',
    dateText: 'Use by 10/4/2021',
    detailsTitle: 'Details',
    description: 'description',
    isPlcc: false,
    isPlaceBucks: false,
    isCouponSaving: false,
    isMPR: true,
    onChangeHandler: jest.fn(),
  };

  beforeEach(() => {
    component = shallow(<MPRReward {...props} />);
  });

  it('MPRReward should be defined', () => {
    expect(component).toBeDefined();
  });

  it('MPRReward should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
});
