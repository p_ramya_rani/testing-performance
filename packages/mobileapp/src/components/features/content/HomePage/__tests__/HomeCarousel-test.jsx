// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable */
import React from 'react';
import {shallow} from 'enzyme';
import {HomeCarousel, mapStateToProps, mapDispatchToProps} from '../component/HomeCarousel';

describe('HomeCarousel', () => {
  const props = {
    favStore: {},
    nearByStore: {},
    labels: {},
    data: [6],
    checkYourStoreAvailable: jest.fn(),
    getStore: jest.fn(),
    onPressHandler: jest.fn(),
    setUserNearStore: jest.fn(),
  };
  it('should render HomeCarousel', () => {
    const component = shallow(<HomeCarousel {...props} />);
    expect(component).toMatchSnapshot();
  });
  it('should return an action setUserNearStore which will call dispatch function on execution', () => {
    const dispatch = jest.fn();
    const dispatchProps = mapDispatchToProps(dispatch);
    dispatchProps.setUserNearStore();
    expect(dispatch.mock.calls).toHaveLength(1);
  });

  it('should return an action trackClickDispatch which will call dispatch function on execution', () => {
    const dispatch = jest.fn();
    const dispatchProps = mapDispatchToProps(dispatch);
    dispatchProps.trackClickDispatch();
    expect(dispatch.mock.calls).toHaveLength(1);
  });

  it('should return an action setClickAnalyticsDataDispatch which will call dispatch function on execution', () => {
    const dispatch = jest.fn();
    const dispatchProps = mapDispatchToProps(dispatch);
    dispatchProps.setClickAnalyticsDataDispatch();
    expect(dispatch.mock.calls).toHaveLength(1);
  });
});
