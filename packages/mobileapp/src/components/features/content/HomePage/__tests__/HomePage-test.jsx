// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import {shallow} from 'enzyme';
import {TIER1} from '@tcp/core/src/constants/tiering.constants';
import {HomePageView} from '../views/HomePage.view';

describe('HomePageView', () => {
  let component;
  let props;
  const setApiConfigData = jest.fn();
  const getBootstrapData = jest.fn();
  const getPageLayout = jest.fn();
  const getCartData = jest.fn();

  beforeEach(() => {
    props = {
      slot_1: {className: 'moduleD'},
      slot_2: {className: 'moduleH'},
      screenProps: {
        apiConfig: {
          previewEnvId: 'STAGING',
        },
        isSplashVisible: false,
        closeOverlay: true,
        checkFirst: jest.fn(),
        gotoOutSideClick: jest.fn(),
        onInitialModalsActionComplete: jest.fn(),
        deviceTier: TIER1,
      },
      setApiConfigData,
      getPageLayout,
      getBootstrapData,
      getCartData,
      appType: 'tcp',
      pageUrl: '',
      loadNavigationData: () => {},
      trackPageLoad: jest.fn(),
      isUserGuest: false,
      isUserPlcc: false,
      updatePreviewDate: () => {},
      customNavigationData: {
        deepLink: '',
        requiredNavigationData: false,
      },
      previewDate: '',
      recentlyViewedLabel: '',
      loyaltyPromoBanner: [{text: 'Foo', userType: 'Fooo-test'}],
      headerPromo: {},
      promoHtmlBannerCarousel: [],
      labels: {
        foo: 'test',
      },
    };
    component = shallow(<HomePageView {...props} />);
    component.setState({active: true});
  });

  it('should be defined', () => {
    expect(component).toBeDefined();
  });

  it('should render blank fragment', () => {
    expect(component).toMatchSnapshot();
  });

  it('should render correctly', () => {
    component.setState({active: true, loadSlots: true, showHeader: true});
    expect(component).toMatchSnapshot();
  });

  it('should call ScrollView method and set showHeader true', () => {
    const event = {
      nativeEvent: {
        contentOffset: {
          y: 400,
        },
      },
    };
    component.instance().handleScroll(event);
    expect(component.state().showHeader).toBeTruthy();
  });

  it('should call ScrollView method and set showHeader false', () => {
    const event = {
      nativeEvent: {
        contentOffset: {
          y: 200,
        },
      },
    };
    component.instance().handleScroll(event);
    expect(component.state().showHeader).toBeFalsy();
  });

  it('should call modalActionComplete', () => {
    component.instance().modalActionComplete();
    expect(component.state().loadSlots).toBeTruthy();
    expect(props.screenProps.onInitialModalsActionComplete).toBeCalled();
  });

  it('should call showBackToSchoolPromo', () => {
    component.instance().showBackToSchoolPromo();
  });
});
