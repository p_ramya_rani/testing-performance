import React from 'react';
import {fromJS} from 'immutable';
import {shallow} from 'enzyme';
import {InYourBagTile} from '../component/InYourBagTile';

describe('InYourBagTile', () => {
  let component;
  beforeEach(() => {
    const props = fromJS({
      orderItems: [
        {
          itemInfo: {
            offerPrice: 100,
          },
          productInfo: {
            name: '',
          },
        },
      ],
    });
    component = shallow(
      <InYourBagTile
        {...props}
        headerText="Bag"
        detailsText="Details"
        cartVal={8}
        onPressHandler={jest.fn()}
      />,
    );
  });

  it('InYourBagTile should be defined', () => {
    expect(component).toBeDefined();
  });

  it('InYourBagTile should render correctly', () => {
    expect(component).toMatchSnapshot();
  });
});
