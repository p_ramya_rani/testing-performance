// 9fbef606107a605d69c0edbcd8029e5d
import {
  layoutsSelector,
  labelsSelector,
  globalLabelSelector,
  globalAccessibilityLabelsSelector,
  homePageLayoutSelector,
  homePageSlotsSelector,
  slotModulesDataSelector,
} from '../container/HomePage.selector';

const stateMock = {
  Layouts: {
    home: {
      slots: [
        {
          name: 'slot_1',
          moduleName: 'moduleA',
          contentId: 'a123',
        },
        {
          name: 'slot_2',
          moduleName: 'moduleB',
          contentId: 'b123',
        },
      ],
    },
  },
  Modules: {
    a123: {a: 1},
    b123: {b: 1},
  },
  Labels: {global: {accessibility: {label: 'Click Me'}}},
};

describe('Home Page selectors', () => {
  it('should verify valid Layouts Modules and Labels selection', () => {
    expect(layoutsSelector(stateMock)).toBe(stateMock.Layouts);
    expect(labelsSelector(stateMock)).toBe(stateMock.Labels);
    expect(globalLabelSelector(stateMock)).toBe(stateMock.Labels.global);
  });

  it('should verify valid Accessibility and global selection', () => {
    expect(globalLabelSelector(stateMock)).toBe(stateMock.Labels.global);
    expect(globalAccessibilityLabelsSelector(stateMock)).toBe(
      stateMock.Labels.global.accessibility,
    );
  });

  it('should verify valid homepage layout and slots selection', () => {
    expect(homePageLayoutSelector(stateMock)).toBe(stateMock.Layouts.home);
    expect(homePageSlotsSelector(stateMock)).toBe(stateMock.Layouts.home.slots);
  });

  it('should verify slots modules data selection', () => {
    expect(slotModulesDataSelector(stateMock)).toEqual(
      '[{"name":"slot_1","moduleName":"moduleA","contentId":"a123","accessibility":{"label":"Click Me"},"data":{"a":1}},{"name":"slot_2","moduleName":"moduleB","contentId":"b123","accessibility":{"label":"Click Me"},"data":{"b":1}}]',
    );
  });
});
