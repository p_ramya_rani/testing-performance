// 9fbef606107a605d69c0edbcd8029e5d
/* istanbul ignore file */
import React from 'react';
import PropTypes from 'prop-types';
import names from '@tcp/core/src/constants/eventsName.constants';
import {Dimensions, View, KeyboardAvoidingView} from 'react-native';
import {
  getScreenHeight,
  configureInternalNavigationFromCMSUrl,
  getAPIConfig,
  isWebViewPage,
  isIOS,
} from '@tcp/core/src/utils';
import {CUSTOMER_HELP_GA} from '@tcp/core/src/components/features/account/CustomerHelp/CustomerHelp.constants';
import {navigateToPage, redirectToInAppView, UrlHandler} from '@tcp/core/src/utils/index.native';
import {WebView} from 'react-native-webview';
import config from '@tcp/core/src/components/common/atoms/Anchor/config.native';
import {StackActions} from 'react-navigation';
import {WebViewHeaderChannel} from '@tcp/core/src/services/api.constants';
import internalEndpoints from '@tcp/core/src/components/features/account/common/internalEndpoints';
import {checkForLoginRegisterLink} from '@tcp/core/src/utils/utils.app';
import OpenLoginModal from '@tcp/core/src/components/features/account/LoginPage/container/LoginModal.container';
import {appendToken} from '@tcp/core/src/utils/utils';
import logger from '@tcp/core/src/utils/loggerInstance';

const staticPageName = 'static page';
let clickedText = '';

class InAppWebView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showLoginModal: false,
      loginType: '',
    };
  }

  componentDidMount() {
    const {trackPageLoad, navigation, isExternalCampaignFired, setExternalCampaignFired} =
      this.props;
    const externalCampaignId = navigation.getParam('externalCampaignId');
    const omMID = navigation.getParam('omMID');
    const omRID = navigation.getParam('omRID');

    const customEvents = [];
    if (externalCampaignId) {
      customEvents.push('event18');
      setExternalCampaignFired(true);
    }
    if (isExternalCampaignFired && !externalCampaignId) {
      customEvents.push('event19');
      setExternalCampaignFired(false);
    }
    trackPageLoad(
      {
        currentScreen: 'static_page',
        pageData: {
          pageName: staticPageName,
          pageSection: 'staticpage',
          pageSubSection: staticPageName,
          pageType: staticPageName,
        },
      },
      {
        customEvents,
        ...(externalCampaignId && {
          externalCampaignId,
        }),
        ...(omMID && {omMID}),
        ...(omRID && {omRID}),
      },
    );
  }

  /**
   * This function will be used to trigger all the anchors clicked inside module X.
   */
  captureAnchors = () => {
    return `
    document.querySelectorAll('.moduleX a').forEach(a => {
      a.onclick = event => {
        const { href, target, text } = a;
        window.ReactNativeWebView.postMessage(JSON.stringify({href, text}));
      }
      }); `;
  };

  /**
   * This function will be used to update the token value in the url;
   */
  updateToken = (webURL) => {
    const {webAppToken} = getAPIConfig();

    if ((webURL && webURL.includes('token')) || !webAppToken) {
      return webURL;
    }
    const token = `token=${webAppToken}`;
    const splittedUrl = webURL.split('?');
    const queryParam = splittedUrl.length > 1 ? `&${token}` : `?${token}`;
    return `${webURL}${appendToken() ? queryParam : ''}`;
  };
  /**
   * This will be used to update the url when only the base path is given
   * @param {*} url
   */

  updateUrl = (webURL) => {
    const {webAppDomain, siteId} = getAPIConfig();
    const url = this.updateToken(webURL);
    let URL = url;
    // url path transformation in case of absolute image URL
    if (/^http/.test(url)) {
      URL = url ? url.replace(/^\//, '') : '';
    } else if (webURL.includes(`/${siteId}/`)) {
      // url path transformation in case of relative image URL
      URL = `${webAppDomain}${url}`;
    } else {
      URL = `${webAppDomain}/${siteId}${url}`;
    }
    return URL;
  };

  setAnalyticsData = (customEvents, name) => {
    const {trackAnalyticsClick} = this.props;
    const loyaltyLocation = 'account';
    trackAnalyticsClick({loyaltyLocation, customEvents}, {name});
  };

  /**
   * To remove the last stack from the navigation when user is redirected to home or plp,pdp
   * this will open the account page again not the web view.
   */
  removeLastNavStack = () => {
    const {navigation} = this.props;
    const popAction = StackActions.pop({
      n: 1,
    });
    navigation.dispatch(popAction);
  };

  redirectToHome = () => {
    const {navigation} = this.props;
    this.removeLastNavStack();
    navigation.navigate('Home');
  };

  redirectToAccount = () => {
    this.removeLastNavStack();
  };

  redirectToPDPPLP = (url) => {
    const {navigation} = this.props;
    this.removeLastNavStack();
    const cmsValidatedUrl = configureInternalNavigationFromCMSUrl(url);
    navigateToPage(cmsValidatedUrl, navigation);
  };

  redirectToStoreLocator = () => {
    const {navigation} = this.props;
    this.removeLastNavStack();
    navigation.navigate('StoreLanding');
  };

  showCreateAccountModal = () => {
    let loginType = 'createAccount';

    if (clickedText && clickedText.includes('create')) {
      this.setAnalyticsData(['event125'], names.screenNames.loyalty_join_click);
    } else if (clickedText && clickedText.includes('club')) {
      loginType = 'joinBirthdayClub';
      this.setAnalyticsData(['event136'], names.screenNames.loyalty_join_club_birthday);
    }

    this.setState({
      showLoginModal: true,
      loginType,
    });
  };

  showPLCCModal = (fromEspot) => {
    const {navigation} = this.props;
    this.setAnalyticsData(['event131'], names.screenNames.loyalty_apply_or_accept_offer_click);
    navigation.goBack();
    const navData = {};
    if (fromEspot) {
      navData.isPlccModalOpen = true;
    }
    navigation.navigate({
      routeName: 'ApplyNow',
      params: navData,
    });
  };

  redirectToGiftCard = (url) => {
    if (url.includes('gift-card-balance')) {
      const {navigation} = this.props;
      navigation.replace('EGiftCardPage');
    }
  };

  showLoginModal = () => {
    this.setAnalyticsData(['event126'], names.screenNames.loyalty_login_click);
    this.setState({
      showLoginModal: true,
      loginType: 'login',
    });
  };

  setLoginModal = (params) => {
    this.setState({
      showLoginModal: params && params.state,
    });
    const {navigation} = this.props;
    navigation.goBack();
  };

  redirectToCustomerSelfHelp = () => {
    const {navigation, selectOrderAction, trackAnalyticsClick} = this.props;
    trackAnalyticsClick(
      {orderHelpLocation: CUSTOMER_HELP_GA.BANNER_CTA_TRACK.CD126},
      {name: 'orderHelpLocation_click', module: 'account'},
    );
    selectOrderAction({navigation});
  };

  shouldRedirectToPLPPDP = (url) => {
    const {
      URL_PATTERN: {PRODUCT_LIST, CATEGORY_LANDING, OUTFIT_DETAILS},
    } = config;
    return (
      url.includes(PRODUCT_LIST) || url.includes(CATEGORY_LANDING) || url.includes(OUTFIT_DETAILS)
    );
  };

  getUrlAndSetClickedText = (event) => {
    let url = '';
    try {
      if (event && event.nativeEvent && event.nativeEvent.data) {
        const {href: redirectUrl, text} = JSON.parse(event.nativeEvent.data);
        url = redirectUrl;
        clickedText = text && text.toLowerCase().trim();
      }
    } catch (err) {
      logger.error('Webview: unable to parse event data', err);
    }
    return url;
  };

  closeWebView = (url) => {
    const {callback} = this.props;
    if (
      (url.includes('applepromo_na_button') ||
        url.includes('applepromo_na_image') ||
        url.includes('doNotOpenInWebView')) &&
      callback
    ) {
      callback();
    }
  };

  /**
   * To capture the postmessages for web view.
   */

  // eslint-disable-next-line complexity
  handleWebViewEvents = (event) => {
    const {navigation} = this.props;
    const fromEspot = navigation && navigation.getParam('fromEspot');
    const url = this.getUrlAndSetClickedText(event);
    const {URL_PATTERN} = config;
    const loginRegisterLink = checkForLoginRegisterLink(url);
    this.redirectToGiftCard(url);
    this.closeWebView(url);
    if (
      url &&
      /^http/.test(url) &&
      !url.includes('gymboree.com') &&
      !url.includes('childrensplace.com')
    ) {
      UrlHandler(url);
      navigation.goBack();
    } else if (url && !isWebViewPage(url)) {
      if (loginRegisterLink === 'create-account') {
        this.showCreateAccountModal();
      } else if (loginRegisterLink === 'login') {
        this.showLoginModal();
      } else if (loginRegisterLink === 'plcc') {
        this.showPLCCModal(fromEspot);
      } else if (url.includes(internalEndpoints.shopNowPage.path)) {
        this.redirectToHome();
      } else if (url.includes('/account')) {
        this.redirectToAccount();
      } else if (url.includes('manageacct')) {
        redirectToInAppView(url, navigation);
      } else if (url.includes(URL_PATTERN.CUSTOMER_SELF_HELP)) {
        this.redirectToCustomerSelfHelp();
      } else if (this.shouldRedirectToPLPPDP(url)) {
        this.redirectToPDPPLP(url);
      } else if (url.includes(internalEndpoints.storeLocator.path)) {
        this.redirectToStoreLocator();
      }
    }
  };

  render() {
    const {pageUrl, thirdPartyCookiesEnabled, isApplyDeviceHeight, isBTSWebView} = this.props;
    const screenHeight = Math.round(Dimensions.get('window').height);
    const style = {backgroundColor: 'transparent'};
    const styleWithHeight = {backgroundColor: 'transparent', height: screenHeight};
    const {showLoginModal, loginType} = this.state;
    const isIOSDevice = isIOS();
    const keyboardBehavior = {style: {flex: 1}};
    if (!isIOSDevice && !isBTSWebView) {
      keyboardBehavior.behavior = 'padding';
    }
    let subtractingHeight = 200;
    if (isBTSWebView && isIOSDevice) {
      subtractingHeight = 0;
    }
    return (
      <View height={getScreenHeight() - subtractingHeight}>
        <KeyboardAvoidingView {...keyboardBehavior}>
          <WebView
            androidHardwareAccelerationDisabled={true}
            style={isApplyDeviceHeight ? styleWithHeight : style}
            originWhitelist={['*']}
            javaScriptEnabled
            injectedJavaScript={this.captureAnchors()}
            startInLoadingState
            thirdPartyCookiesEnabled={thirdPartyCookiesEnabled}
            source={{
              uri: this.updateUrl(pageUrl),
              headers: {'x-tcp-channel': WebViewHeaderChannel},
            }}
            onMessage={this.handleWebViewEvents}
            useWebKit={isIOSDevice}
            allowsInlineMediaPlayback
          />
        </KeyboardAvoidingView>

        {showLoginModal && (
          <OpenLoginModal
            component={loginType}
            openState={showLoginModal}
            setLoginModalMountState={this.setLoginModal}
          />
        )}
      </View>
    );
  }
}

InAppWebView.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.shape({})),
  pageUrl: PropTypes.string,
  thirdPartyCookiesEnabled: PropTypes.bool,
  isApplyDeviceHeight: PropTypes.bool,
  trackPageLoad: PropTypes.func,
  isExternalCampaignFired: PropTypes.bool.isRequired,
  setExternalCampaignFired: PropTypes.func,
  selectOrderAction: PropTypes.func,
  trackAnalyticsClick: PropTypes.func,
  callback: PropTypes.func,
  isBTSWebView: PropTypes.bool,
};

InAppWebView.defaultProps = {
  navigation: null,
  pageUrl: '',
  thirdPartyCookiesEnabled: false,
  isApplyDeviceHeight: false,
  trackPageLoad: () => {},
  setExternalCampaignFired: () => {},
  selectOrderAction: () => {},
  trackAnalyticsClick: () => {},
  isBTSWebView: false,
  callback: null,
};

export default InAppWebView;
