// 9fbef606107a605d69c0edbcd8029e5d 
import {connect} from 'react-redux';
import {trackPageView, setClickAnalyticsData, trackClick} from '@tcp/core/src/analytics/actions';
import {setExternalCampaignState} from '@tcp/core/src/components/common/molecules/Loader/container/Loader.actions';
import {getExternalCampaignFired} from '@tcp/core/src/components/features/browse/ProductListing/container/ProductListing.selectors';
import {selectOrder} from '@tcp/core/src/components/features/account/CustomerHelp/container/CustomerHelp.actions';
import InAppWebView from '../views';

const mapStateToProps = state => ({
  isExternalCampaignFired: getExternalCampaignFired(state),
});
const mapDispatchToProps = dispatch => {
  return {
    setExternalCampaignFired: payload => {
      dispatch(setExternalCampaignState(payload));
    },
    trackPageLoad: (payload, customData) => {
      dispatch(setClickAnalyticsData(customData));
      dispatch(trackPageView(payload));
      setTimeout(() => {
        dispatch(setClickAnalyticsData({}));
      }, 300);
    },
    selectOrderAction: payload => {
      dispatch(selectOrder(payload));
    },
    trackAnalyticsClick: (eventData, payload) => {
      dispatch(setClickAnalyticsData(eventData));
      const timer = setTimeout(() => {
        dispatch(trackClick(payload));
        clearTimeout(timer);
      }, 800);
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(InAppWebView);
