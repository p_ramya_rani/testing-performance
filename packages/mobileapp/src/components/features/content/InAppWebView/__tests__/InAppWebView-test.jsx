// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import {shallow} from 'enzyme';
import {WebView} from 'react-native-webview';

import InAppWebView from '../views/InAppWebView.view';

describe('SearchBar native should render correctly', () => {
  const getParam = () => 'Webview getparam';

  it('should render correctly', () => {
    const navigation = {
      getParam,
      navigate: jest.fn(),
      dispatch: jest.fn(),
    };
    const props = {trackPageLoad: jest.fn()};
    const wrapper = shallow(<InAppWebView navigation={navigation} {...props} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should render correctly with url', () => {
    const navigation = {
      getParam,
      navigate: jest.fn(),
      dispatch: jest.fn(),
    };
    const props = {trackPageLoad: jest.fn()};
    const wrapper = shallow(
      <InAppWebView
        isApplyDeviceHeight
        navigation={navigation}
        {...props}
        pageUrl="https://www.gymboree.com"
      />,
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('should verify onMessage home redirect logic', () => {
    const navigation = {
      getParam,
      navigate: jest.fn(),
      dispatch: jest.fn(),
    };
    const props = {trackPageLoad: jest.fn()};
    const wrapper = shallow(
      <InAppWebView navigation={navigation} {...props} pageUrl="https://www.gymboree.com/home" />,
    );
    wrapper
      .find(WebView)
      .props()
      .onMessage({
        nativeEvent: {data: JSON.stringify({href: 'https://www.gymboree.com/home', text: ''})},
      });
    expect(navigation.navigate).toHaveBeenCalledWith('Home');

    wrapper
      .find(WebView)
      .props()
      .onMessage({
        nativeEvent: {data: JSON.stringify({href: 'https://www.gymboree.com/account', text: ''})},
      });
    expect(navigation.navigate).toHaveBeenCalledTimes(1);
  });

  it('should verify onMessage product redirect logic', () => {
    const navigation = {
      getParam,
      navigate: jest.fn(),
      dispatch: jest.fn(),
    };
    const props = {trackPageLoad: jest.fn()};
    const wrapper = shallow(
      <InAppWebView navigation={navigation} {...props} pageUrl="https://www.gymboree.com/p" />,
    );

    wrapper
      .find(WebView)
      .props()
      .onMessage({
        nativeEvent: {data: JSON.stringify({href: 'https://www.gymboree.com/p', text: ''})},
      });
    expect(navigation.dispatch).toHaveBeenCalledTimes(1);
  });

  it('should verify onMessage category redirect logic', () => {
    const navigation = {
      getParam,
      navigate: jest.fn(),
      dispatch: jest.fn(),
    };
    const props = {trackPageLoad: jest.fn()};
    const wrapper = shallow(
      <InAppWebView navigation={navigation} {...props} pageUrl="https://www.gymboree.com/c" />,
    );

    wrapper
      .find(WebView)
      .props()
      .onMessage({
        nativeEvent: {data: JSON.stringify({href: 'https://www.gymboree.com/outfit/', text: ''})},
      });
    expect(navigation.dispatch).toHaveBeenCalledTimes(1);
  });

  it('should verify onMessage outfit redirect logic', () => {
    const navigation = {
      getParam,
      navigate: jest.fn(),
      dispatch: jest.fn(),
    };
    const props = {trackPageLoad: jest.fn()};
    const wrapper = shallow(
      <InAppWebView
        navigation={navigation}
        {...props}
        pageUrl="https://www.gymboree.com/outfit/"
      />,
    );

    wrapper
      .find(WebView)
      .props()
      .onMessage({
        nativeEvent: {data: JSON.stringify({href: 'https://www.gymboree.com/outfit/', text: ''})},
      });
    expect(navigation.dispatch).toHaveBeenCalledTimes(1);
  });
});
