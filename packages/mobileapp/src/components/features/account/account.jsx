// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { SafeAreaView } from 'react-native';
import AccountNative from '@tcp/core/src/components/features/account/Account/container/Account';

export default class HomeScreen extends React.PureComponent {
  render() {
    return (
      <SafeAreaView>
        <AccountNative component="accountOverviewMobile" {...this.props} />
      </SafeAreaView>
    );
  }
}
