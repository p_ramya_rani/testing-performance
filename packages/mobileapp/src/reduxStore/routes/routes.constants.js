// 9fbef606107a605d69c0edbcd8029e5d 
const ROUTE_NAMES = {
  NAV_MENU_LEVEL_1: 'Navigation',
  NAV_MENU_LEVEL_2: 'NavMenuLevel2',
  NAV_MENU_LEVEL_3: 'NavMenuLevel3',
  PRODUCT_LISTING: 'ProductListing',
  PRODUCT_DETAIL_PAGE: 'ProductDetail',
  SEARCH_RESULTS_PAGE: 'SearchDetail',
  CONFIRMATION: 'Confirmation',
  OUTFIT_LISTING: 'OutfitListing',
  BUNDLE_DETAIL: 'BundleDetail',
  ORDER_RECEIPT: 'OrderReceiptPage',
};

export default ROUTE_NAMES;
