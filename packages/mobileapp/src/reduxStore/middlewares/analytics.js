// 9fbef606107a605d69c0edbcd8029e5d 
/* eslint-disable  */
import { createMiddleware } from 'redux-beacon';
import eventMapping from '../../context/analytics/eventMapping';
import trackingTarget from '../../context/analytics/trackingTarget';

/**
 * Create the Redux-Beacon middleware instance.
 */

export default function create() {
  return createMiddleware(eventMapping, trackingTarget);
}
