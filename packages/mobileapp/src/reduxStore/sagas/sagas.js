// 9fbef606107a605d69c0edbcd8029e5d
import {all} from 'redux-saga/effects';
import BootstrapSaga from '@tcp/core/src/reduxStore/sagas/bootstrap';
import LayoutSaga from '@tcp/core/src/reduxStore/sagas/layout';
import SubNavigationSaga from '@tcp/core/src/reduxStore/sagas/subNavigation';
// import LabelsSaga from '@tcp/core/src/reduxStore/sagas/labels';
import LoginPageSaga from '@tcp/core/src/components/features/account/LoginPage/container/LoginPage.saga';
import UserSaga from '@tcp/core/src/components/features/account/User/container/User.saga';
import LogOutPageSaga from '@tcp/core/src/components/features/account/Logout/container/LogOut.saga';
import ForgotPasswordSaga from '@tcp/core/src/components/features/account/ForgotPassword/container/ForgotPassword.saga';
import ProductListingSaga from '@tcp/core/src/components/features/browse/ProductListing/container/ProductListing.saga';
import ProductDetailSaga from '@tcp/core/src/components/features/browse/ProductDetail/container/ProductDetail.saga';
import QuickViewSaga from '@tcp/core/src/components/common/organisms/QuickViewModal/container/QuickViewModal.saga';
import PaymentSaga from '@tcp/core/src/components/features/account/Payment/container/Payment.saga';
import UpdateProfileSaga from '@tcp/core/src/components/features/account/AddEditPersonalInformation/container/AddEditPersonalInformation.saga';
import AddEditAddressSaga from '@tcp/core/src/components/common/organisms/AddEditAddress/container/AddEditAddress.saga';
import AddressVerificationSaga from '@tcp/core/src/components/common/organisms/AddressVerification/container/AddressVerification.saga';
import DefaultPaymentSaga from '@tcp/core/src/components/features/account/Payment/container/DefaultPayment.saga';
import AddressBookSaga from '@tcp/core/src/components/features/account/AddressBook/container/AddressBook.saga';
import DeleteAddressSaga from '@tcp/core/src/components/features/account/AddressBook/container/DeleteAddress.saga';
import {SetDefaultShippingAddressSaga} from '@tcp/core/src/components/features/account/AddressBook/container/DefaultShippingAddress.saga';
import AddedToBagSaga from '@tcp/core/src/components/features/CnC/AddedToBag/container/AddedToBag.saga';
import AddedToBagNewSaga from '@tcp/core/src/components/features/CnC/AddedToBag/container/AddedToBagNew.saga.native';
import CreateAccountSaga from '@tcp/core/src/components/features/account/CreateAccount/container/CreateAccount.saga';
import AccountSaga from '@tcp/core/src/components/features/account/Account/container/Account.saga';
import CartPageSaga from '@tcp/core/src/components/features/CnC/CartItemTile/container/CartItemTile.saga';
import BonusPointsSaga from '@tcp/core/src/components/common/organisms/BonusPointsDays/container/BonusPointsDays.saga';
import GiftCardBalanceSaga from '@tcp/core/src/components/features/account/Payment/container/GetCardBalance.saga';
import BagPageSaga from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.saga';
import LoyaltyPageSaga from '@tcp/core/src/components/features/CnC/LoyaltyBanner/container/Loyalty.saga';
import DeleteCardSaga from '@tcp/core/src/components/features/account/Payment/container/DeleteCard.saga';
import PointsHistorySaga from '@tcp/core/src/components/features/account/common/organism/PointsHistory/container/PointsHistory.saga';
import EarnExtraPointsSaga from '@tcp/core/src/components/features/account/common/organism/EarnExtraPointsTile/container/EarnExtraPointsTile.saga';
import EarnedPointsNotificationSaga from '@tcp/core/src/components/features/account/common/organism/EarnExtraPointsTile/container/EarnedPointsNotification.saga';

import CouponSaga from '@tcp/core/src/components/features/CnC/common/organism/CouponAndPromos/container/Coupon.saga';
import CheckoutSaga from '@tcp/core/src/components/features/CnC/Checkout/container/Checkout.saga';
import AddEditCreditCardSaga from '@tcp/core/src/components/features/account/AddEditCreditCard/container/AddEditCreditCard.saga';
import {AddGiftCardSaga} from '@tcp/core/src/components/features/account/Payment/AddGiftCard/container/AddGiftCard.saga';
import TrackOrderSaga from '@tcp/core/src/components/features/account/TrackOrder/container/TrackOrder.saga';
import NavigationSaga from '@tcp/core/src/components/features/content/Navigation/container/Navigation.saga';
import ChangePasswordSaga from '@tcp/core/src/components/features/account/ChangePassword/container/ChangePassword.saga';
import BillingPaymentSaga from '@tcp/core/src/components/features/CnC/Checkout/organisms/BillingPaymentForm/container/CreditCard.saga';
import ProductTabListSaga from '@tcp/core/src/components/common/organisms/ProductTabList/container/ProductTabList.saga';
import StyliticsProductTabListSaga from '@tcp/core/src/components/common/organisms/StyliticsProductTabList/container/StyliticsProductTabList.saga';
import GetCandidSaga from '@tcp/core/src/components/common/molecules/GetCandid/container/GetCandid.saga';
import GiftCardsSaga from '@tcp/core/src/components/features/CnC/Checkout/organisms/GiftCardsSection/container/GiftCards.saga';
import MailingAddressSaga from '@tcp/core/src/components/features/account/MyProfile/organism/MailingInformation/container/MailingAddress.saga';
import SearchPageSaga from '@tcp/core/src/components/features/browse/SearchDetail/container/SearchDetail.saga';
import BirthdaySavingsSaga from '@tcp/core/src/components/features/account/common/organism/BirthdaySavingsList/container/BirthdaySavingsList.saga';
import StoreDetailSaga from '@tcp/core/src/components/features/storeLocator/StoreDetail/container/StoreDetail.saga';
import StoreLandingSaga from '@tcp/core/src/components/features/storeLocator/StoreLanding/container/StoreLanding.saga';
import ExtraPointsSaga from '@tcp/core/src/components/features/account/ExtraPoints/container/ExtraPoints.saga';

import PickupStoreSaga from '@tcp/core/src/components/common/organisms/PickupStoreModal/container/PickUpStoreModal.saga';
import OutfitDetailsSaga from '@tcp/core/src/components/features/browse/OutfitDetails/container/OutfitDetails.saga';
import ConfirmationPageSaga from '@tcp/core/src/components/features/CnC/Confirmation/container/Confirmation.saga';
import NavigateXHRSaga from '@tcp/core/src/components/features/account/NavigateXHR/container/NavigateXHR.saga';
import ApplyCreditCardSaga, {
  SubmitInstantCardApplication,
} from '@tcp/core/src/components/features/browse/ApplyCardPage/container/ApplyCard.saga';
import ResetPasswordSaga from '@tcp/core/src/components/features/account/ResetPassword/container/ResetPassword.saga';
import SocialAccountSaga from '@tcp/core/src/components/common/organisms/SocialAccount/container/Social.saga';
import PointsClaimSaga from '@tcp/core/src/components/features/account/PointsClaim/container/PointsClaim.saga';
import OrdersSaga from '@tcp/core/src/components/features/account/Orders/container/Orders.saga';
import OrderDetailsSaga from '@tcp/core/src/components/features/account/OrderDetails/container/OrderDetails.saga';
import SearchBarSaga from '@tcp/core/src/components/common/molecules/SearchBar/SearchBar.saga';
import FavoriteSaga from '@tcp/core/src/components/features/browse/Favorites/container/Favorites.saga';
import RecommendationsSaga from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.saga';
import RecentSearchSaga from '@tcp/core/src/components/common/organisms/SearchProduct/RecentSearch.saga';
import SubscribeStoreSaga from '@tcp/core/src/components/features/account/MyPreferenceSubscription/container/MyPreferenceSubscription.saga';
import BundleProductSaga from '@tcp/core/src/components/features/browse/BundleProduct/container/BundleProduct.saga';
import CustomNavigation from '@tcp/core/src/components/features/content/CustomNavigation/container/CustomNavigation.saga';
import ProductPickup from '@tcp/core/src/components/common/organisms/ProductPickup/container/ProductPickup.saga';
import AbtestSaga from '@tcp/core/src/components/common/molecules/abTest/abTest.saga';
import AppVersionSaga from '@tcp/core/src/components/common/molecules/AppUpdate/container/AppVersion.saga';
import AnalyticsSaga from '@tcp/core/src/analytics/Analytics.saga';
import EddSaga from '@tcp/core/src/components/features/CnC/common/molecules/EddComponent/container/EddComponent.saga';
import BagCarouselModuleSaga from '@tcp/core/src/components/common/molecules/BagCarouselModule/container/BagCarouselModule.saga';
import ResendOrderConfirmationSaga from '@tcp/core/src/components/common/organisms/ResendOrderConfirmation/container/ResendOrderConfirmation.saga';
import RedirectSaga from '@tcp/core/src/components/features/content/RedirectContentPage/container/RedirectContentPage.saga';
import CustomerHelpSaga from '@tcp/core/src/components/features/account/CustomerHelp/container/CustomerHelp.saga';
import GiftCardSaga from '@tcp/core/src/components/features/GiftCard/container/GiftCard.saga';
import HomePageSaga from '../../components/features/content/HomePage/container/HomePage.saga';
import ShopByProfileSaga from '../../components/features/shopByProfile/ShopByProfileAPI/ShopByProfile.saga';

export default function* rootSaga() {
  yield all([
    // LabelsSaga(),
    GiftCardSaga(),
    BootstrapSaga(),
    HomePageSaga(),
    CustomNavigation(),
    RecommendationsSaga(),
    NavigationSaga(),
    AddEditAddressSaga(),
    AddressVerificationSaga(),
    LoginPageSaga(),
    LogOutPageSaga(),
    ForgotPasswordSaga(),
    PaymentSaga(),
    DefaultPaymentSaga(),
    SetDefaultShippingAddressSaga(),
    AddressBookSaga(),
    DeleteAddressSaga(),
    AddedToBagSaga(),
    AddedToBagNewSaga(),
    CreateAccountSaga(),
    BagPageSaga(),
    LoyaltyPageSaga(),
    CartPageSaga(),
    BonusPointsSaga(),
    GiftCardBalanceSaga(),
    CouponSaga(),
    DeleteCardSaga(),
    PointsHistorySaga(),
    EarnExtraPointsSaga(),
    EarnedPointsNotificationSaga(),
    ProductListingSaga(),
    AddEditCreditCardSaga(),
    UserSaga(),
    AddGiftCardSaga(),
    CheckoutSaga(),
    TrackOrderSaga(),
    ChangePasswordSaga(),
    BillingPaymentSaga(),
    ProductTabListSaga(),
    StyliticsProductTabListSaga(),
    GetCandidSaga(),
    UpdateProfileSaga(),
    GiftCardsSaga(),
    MailingAddressSaga(),
    SearchPageSaga(),
    ProductDetailSaga(),
    BirthdaySavingsSaga(),
    StoreDetailSaga(),
    StoreLandingSaga(),
    ApplyCreditCardSaga(),
    SubmitInstantCardApplication(),
    QuickViewSaga(),
    PointsClaimSaga(),
    SocialAccountSaga(),
    ConfirmationPageSaga(),
    PickupStoreSaga(),
    ExtraPointsSaga(),
    OrdersSaga(),
    OrderDetailsSaga(),
    SearchBarSaga(),
    FavoriteSaga(),
    OutfitDetailsSaga(),
    RecentSearchSaga(),
    AccountSaga(),
    SubscribeStoreSaga(),
    LayoutSaga(),
    NavigateXHRSaga(),
    ResetPasswordSaga(),
    BundleProductSaga(),
    ProductPickup(),
    SubNavigationSaga(),
    AbtestSaga(),
    AppVersionSaga(),
    AnalyticsSaga(),
    EddSaga(),
    BagCarouselModuleSaga(),
    ResendOrderConfirmationSaga(),
    RedirectSaga(),
    CustomerHelpSaga(),
    ShopByProfileSaga(),
  ]);
}
