// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import {createStackNavigator} from 'react-navigation-stack';
import {Text} from 'react-native';
import {getLocator, isTCP} from '@tcp/core/src/utils';
import NavBarIcon from '../components/common/atoms/NavBarIcon';

const BrandSwitch = () => <Text>Brand Switching happens here</Text>;

const BrandSwitchStack = createStackNavigator({
  BrandSwitch,
});

BrandSwitchStack.navigationOptions = {
  title: 'brand_logo',
  showLabel: false,
  tabBarIcon: (props = {}) => {
    // eslint-disable-next-line react/prop-types
    const {focused} = props;
    return (
      <NavBarIcon
        iconActive="brand-close-icon"
        iconInactive="brand-logo"
        {...props}
        accessibilityLabel={
          focused
            ? 'close'
            : (isTCP() && getLocator('fav_brand_tcp_lbl')) || getLocator('fav_brand_gym_lbl')
        }
        style={{
          icon: {
            width: 100,
            height: 75,
          },
        }}
      />
    );
  },
};

export default BrandSwitchStack;
