// 9fbef606107a605d69c0edbcd8029e5d
import {createStackNavigator} from 'react-navigation-stack';
import React from 'react';
import {SafeAreaView} from 'react-navigation';
import CustomerHelp from '@tcp/core/src/components/features/account/CustomerHelp';
import {CUSTOMER_HELP_PAGE_TEMPLATE} from '@tcp/core/src/components/features/account/CustomerHelp/CustomerHelp.constants';
import HeaderNew from '../components/common/molecules/Header/HeaderNew';
import {headerStyle} from '../components/common/molecules/Header/Header.style';

const getNewHeader = (navigation, showSearch = false, showSearchIcon = false, hideBack = false) => {
  const noHeader = navigation && navigation.getParam('noHeader');
  return {
    header: props =>
      !noHeader ? (
        <SafeAreaView style={headerStyle} forceInset={{top: 'always', bottom: 'never'}}>
          <HeaderNew
            {...props}
            navigation={navigation}
            showSearch={showSearch}
            showSearchIcon={showSearchIcon}
            hideBack={hideBack}
          />
        </SafeAreaView>
      ) : null,
    headerBackground: 'transparent',
  };
};

const CustomerHelpStack = createStackNavigator({
  [CUSTOMER_HELP_PAGE_TEMPLATE.SELECT_ORDER]: {
    // eslint-disable-next-line react/prop-types
    screen: ({navigation}) => {
      return <CustomerHelp navigation={navigation} />;
    },
    navigationOptions: ({navigation}) => {
      return getNewHeader(navigation, false, false);
    },
  },
  [CUSTOMER_HELP_PAGE_TEMPLATE.TELL_US_MORE]: {
    // eslint-disable-next-line react/prop-types
    screen: ({navigation}) => {
      return <CustomerHelp navigation={navigation} />;
    },
    navigationOptions: ({navigation}) => {
      return getNewHeader(navigation, false, false);
    },
  },

  [CUSTOMER_HELP_PAGE_TEMPLATE.REVIEW]: {
    // eslint-disable-next-line react/prop-types
    screen: ({navigation}) => {
      return <CustomerHelp navigation={navigation} />;
    },
    navigationOptions: ({navigation}) => {
      return getNewHeader(navigation, false, false);
    },
  },
  [CUSTOMER_HELP_PAGE_TEMPLATE.CONFIRMATION]: {
    // eslint-disable-next-line react/prop-types
    screen: ({navigation}) => {
      return <CustomerHelp navigation={navigation} />;
    },
    navigationOptions: ({navigation}) => {
      return getNewHeader(navigation, false, false, true);
    },
  },
});

export default CustomerHelpStack;
