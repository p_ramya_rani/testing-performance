// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import {StatusBar} from 'react-native';
import {SafeAreaView} from 'react-navigation';
import {isAndroid} from '@tcp/core/src/utils/utils.app';
import {createStackNavigator} from 'react-navigation-stack';
import LoginPageContainer from '@tcp/core/src/components/features/account/LoginPage';
import GetCandidGallery from '@tcp/core/src/components/common/molecules/GetCandidGallery/views/GetCandidGallery.native';
import StoreLanding from '@tcp/core/src/components/features/storeLocator/StoreLanding/container/StoreLanding.container';
import StoreDetails from '@tcp/core/src/components/features/storeLocator/StoreDetail';
import ProductListing from '@tcp/core/src/components/features/browse/ProductListing';
import ProductDetail from '@tcp/core/src/components/features/browse/ProductDetail';
import ProductBundleContainer from '@tcp/core/src/components/features/browse/BundleProduct';
import OutfitDetail from '@tcp/core/src/components/features/browse/OutfitDetails';
import SearchDetail from '@tcp/core/src/components/features/browse/SearchDetail';
import RedirectContentView from '@tcp/core/src/components/features/content/RedirectContentPage';
import StyleGuide from '@tcp/core/styles/themes/TCP';

import Home from '../components/features/content/HomePage';
import account from '../components/features/account/account';
import NavBarIcon from '../components/common/atoms/NavBarIcon';
import Header from '../components/common/molecules/Header';
import Navigation from '../components/features/content/Navigation';
import ProductLanding from '../components/features/browse/ProductLanding/ProductLanding';
import HeaderNew from '../components/common/molecules/Header/HeaderNew';
import {headerStyle} from '../components/common/molecules/Header/Header.style';
import {getProductDetailHeader, getPdpRouteParams} from '../navigation/getProductDetailHeader';

const setStatusBar = (backgroundColor) => {
  StatusBar.setBackgroundColor(backgroundColor);
  StatusBar.setTranslucent(false);
};

const getNewHeader = (navigation, navTitle, showSearchIcon = false) => {
  const title = navTitle || (navigation && navigation.getParam('title'));
  const newDesignColor = navigation && navigation.getParam('newDesignColor');
  const totalProductCount = navigation && navigation.getParam('totalProductCount');

  let backgroundColor = StyleGuide.colors.WHITE;
  if (newDesignColor) {
    backgroundColor = StyleGuide.colors.PRIMARY.PALEGRAY;
  }
  if (isAndroid()) {
    setStatusBar(backgroundColor);
  }
  return {
    header: (props) => (
      <SafeAreaView
        style={[headerStyle, {backgroundColor}]}
        forceInset={{top: 'always', bottom: 'never'}}>
        <HeaderNew
          totalProductCount={totalProductCount}
          {...props}
          title={title}
          showSearchIcon={showSearchIcon}
        />
      </SafeAreaView>
    ),
    headerBackground: 'transparent',
    gesturesEnabled: false,
  };
};

const getDefaultHeaderWithSearch = (navigation) => {
  return {
    header: (props) => (
      <SafeAreaView style={headerStyle} forceInset={{top: 'always', bottom: 'never'}}>
        <Header {...props} showSearch navigation={navigation} />
      </SafeAreaView>
    ),
    headerBackground: 'transparent',
  };
};

const getDefaultHeaderForStore = (navigation, navTitle) => {
  const title = navTitle || (navigation && navigation.getParam('title'));
  return {
    header: (props) => (
      <SafeAreaView style={headerStyle} forceInset={{top: 'always', bottom: 'never'}}>
        <Header {...props} title={title} navigation={navigation} headertype="store" />
      </SafeAreaView>
    ),
    headerBackground: 'transparent',
  };
};

export const ProductDetailPage = {
  screen: ProductDetail,
  params: getPdpRouteParams(),
  navigationOptions: ({navigation}) => {
    return getProductDetailHeader({
      navigation,
      showSearch: false,
      navTitle: null,
    });
  },
};

const HomeStack = createStackNavigator(
  {
    Home: {
      screen: Home,
    },
    account,
    Navigation,
    ProductLanding,
    LoginPageContainer,
    GetCandidGallery: {
      screen: GetCandidGallery,
      navigationOptions: ({navigation}) => {
        return getNewHeader(navigation);
      },
    },
    ProductDetail: ProductDetailPage,
    OutfitDetail: {
      screen: OutfitDetail,
      navigationOptions: ({navigation}) => {
        return getNewHeader(navigation);
      },
    },
    ProductListing: {
      screen: ProductListing,
      navigationOptions: ({navigation}) => {
        return getNewHeader(navigation, false, true);
      },
    },
    BundleDetail: {
      screen: ProductBundleContainer,
      params: getPdpRouteParams(),
      navigationOptions: ({navigation}) => {
        return getProductDetailHeader({
          navigation,
          showSearch: false,
          navTitle: null,
          showCross: true,
        });
      },
    },
    StoreDetails: {
      screen: StoreDetails,
      path: 'store-details/:storeId',
      navigationOptions: ({navigation}) => {
        const title = navigation && navigation.getParam('title');
        const navTitle = (title && `${title.toUpperCase()}`) || '';
        return getDefaultHeaderForStore(navigation, navTitle);
      },
    },
    StoreLanding: {
      screen: StoreLanding,
      path: 'store-landing',
      // eslint-disable-next-line sonarjs/no-identical-functions
      navigationOptions: ({navigation}) => {
        const title = navigation && navigation.getParam('title');
        const navTitle = (title && `${title.toUpperCase()}`) || '';
        return getDefaultHeaderForStore(navigation, navTitle);
      },
    },
    SearchDetail: {
      screen: SearchDetail,
      navigationOptions: ({navigation}) => {
        const title = navigation && navigation.getParam('title');
        const navTitle = (title && `"${title.toUpperCase()}"`) || '';
        return getNewHeader(navigation, navTitle, true);
      },
    },
    redirectContentPage: {
      screen: RedirectContentView,
      navigationOptions: ({navigation}) => {
        return getDefaultHeaderWithSearch(navigation);
      },
    },
  },
  {
    defaultNavigationOptions: {
      header: (props) => <Header {...props} />,
      headerBackground: 'transparent',
    },
  },
);

HomeStack.navigationOptions = {
  initialRouteName: 'Home',
  headerMode: 'float',
  tabBarLabel: 'home',
  tabBarIcon: (props) => (
    <NavBarIcon iconActive="home-active" iconInactive="home-inactive" {...props} />
  ),
};

export default HomeStack;
