// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import {Animated, Easing, View, StyleSheet} from 'react-native';
import {SafeAreaView} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {isAndroid} from '@tcp/core/src/utils/utils.app';
import ProductDetail from '@tcp/core/src/components/features/browse/ProductDetail';
import OutfitListing from '@tcp/core/src/components/features/browse/OutfitListing';
import OutfitDetail from '@tcp/core/src/components/features/browse/OutfitDetails';
import {getProductDetailHeader, getPdpRouteParams} from '../navigation/getProductDetailHeader';
import SbpPlp from '../components/features/shopByProfile/PLP';
import CreateProfile from '../components/features/shopByProfile/CreateProfile';
import EditProfile from '../components/features/shopByProfile/EditProfile';
import NameInputSBP from '../components/features/shopByProfile/CreateProfile/name';
import AgeInput from '../components/features/shopByProfile/CreateProfile/age';
import GenderInput from '../components/features/shopByProfile/CreateProfile/gender';
import SizeInput from '../components/features/shopByProfile/CreateProfile/size';
import ColorsInput from '../components/features/shopByProfile/CreateProfile/colors';
import InterestsInput from '../components/features/shopByProfile/CreateProfile/interests';
import HeaderNew from '../components/common/molecules/Header/HeaderNew';
import Themes from '../components/features/shopByProfile/ShopByProfile.theme';
import {isDeviceIPhone12} from '../components/features/shopByProfile/PLP/helper';
import LABELS from '../components/features/shopByProfile/__labels__';
import ProfileIcon from '../components/common/atoms/ProfileIcon';
import {
  headerStyle,
  transcluscentHeaderStyle,
  androidPdpHeaderStyle,
  androidHeaderStyle,
  outfitDetailStyle,
} from '../components/common/molecules/Header/Header.style';
import ROUTE_NAMES from '../reduxStore/routes';

const CreateProfileStack = createStackNavigator(
  {
    CreateProfile: {
      screen: CreateProfile,
    },
    NameInput: {
      screen: NameInputSBP,
    },
    AgeInput: {
      screen: AgeInput,
    },
    GenderInput: {
      screen: GenderInput,
    },
    SizeInput: {
      screen: SizeInput,
    },
    InterestsInput: {
      screen: InterestsInput,
    },
    ColorsInput: {
      screen: ColorsInput,
    },
  },
  {
    headerMode: 'none',
    transitionConfig: () => ({
      transitionSpec: {
        duration: 300,
        easing: Easing.out(Easing.poly(4)),
        timing: Animated.timing,
      },
      screenInterpolator: (sceneProps) => {
        const {layout, position, scene} = sceneProps;
        const {index, route} = scene;
        const isInverted = route && route.key && route.key.toString().indexOf('inverted') > -1;
        const width = layout.initWidth;
        const translateX = position.interpolate({
          inputRange: [index - 1, index, index + 1],
          outputRange: [isInverted ? -width : width, 0, 0],
        });

        return {transform: [{translateX}]};
      },
    }),
  },
);

const styles = StyleSheet.create({
  profileIconContainer: {
    alignSelf: 'center',
    position: 'absolute',
    top: 60,
  },
});

// eslint-disable-next-line
const getNewHeaderPDP = (
  navigation,
  showSearch,
  navTitle,
  showSearchIcon = false,
  isTransparent = false,
  // eslint-disable-next-line
) => {
  const backgroundColor = (navigation && navigation.getParam('backgroundColor')) || 'white';
  const title = navTitle || (navigation && navigation.getParam('title'));
  const isScrolled = navigation && navigation.getParam('isScrolled');
  const capitalizedTitle = title && title.toUpperCase();
  const navOptions = isTransparent ? {headerTransparent: true} : {headerBackground: 'transparent'};
  const isSBP = navigation && navigation.getParam('isSBP');
  const profile = navigation && navigation.state.params && navigation.state.params.profile;
  const screenRouteName = navigation && navigation.state && navigation.state.routeName;
  let safeAreaStyles = {};
  if (isTransparent) {
    if (isScrolled) {
      safeAreaStyles = transcluscentHeaderStyle;
    }
  } else {
    safeAreaStyles = headerStyle;
  }
  if (isAndroid() && isSBP)
    return {
      header: (props) => (
        <View
          style={[
            androidPdpHeaderStyle,
            screenRouteName === 'OutfitDetail' ? outfitDetailStyle : null,
          ]}
          forceInset={{top: 'always'}}>
          <HeaderNew
            {...props}
            title={!isTransparent ? capitalizedTitle : ''}
            showSearch={showSearch}
            showSearchIcon={showSearchIcon}
            isSBP={isSBP}
            profileName={profile && profile.name}
            profile={profile}
          />
        </View>
      ),
      ...navOptions,
    };
  return {
    header: (props) => (
      <SafeAreaView style={[safeAreaStyles, {backgroundColor}]} forceInset={{top: 'always'}}>
        <HeaderNew
          {...props}
          title={!isTransparent ? capitalizedTitle : ''}
          showSearch={showSearch}
          showSearchIcon={showSearchIcon}
          isSBP={isSBP}
          profileName={profile.name}
        />
      </SafeAreaView>
    ),
    ...navOptions,
  };
};

const getNewHeader = (navigation, navTitle, showIcon) => {
  const name = navigation && navigation.getParam('name');
  const theme = navigation && navigation.getParam('theme');
  const showTitleHeaderPLP = navigation && navigation.getParam('showTitleHeaderPLP');
  const profileIconStyling = isDeviceIPhone12() ? {top: 70} : {};
  const showTitleOnHeader =
    navigation.state.routeName === 'PLP' && !showTitleHeaderPLP
      ? null
      : navTitle || LABELS.plpHeaderTitle(name).TITLE;
  const profile = navigation.state.params;
  const androidHeaderStyleCheck = isAndroid() ? androidHeaderStyle : [];
  return {
    header: (props) => {
      return (
        <SafeAreaView
          style={{
            backgroundColor: (Themes && Themes[theme || 0] && Themes[theme || 0].color) || '#000',
            ...androidHeaderStyleCheck,
          }}
          forceInset={{top: 'always', bottom: 'never'}}>
          {showIcon && !showTitleHeaderPLP && !isAndroid() && (
            <View style={[styles.profileIconContainer, profileIconStyling]}>
              <ProfileIcon theme={theme} />
            </View>
          )}
          <HeaderNew
            {...props}
            showTitleHeaderPLP={showTitleHeaderPLP}
            navigation={navigation}
            hideBack={false}
            title={showTitleOnHeader}
            isSBP
            isTransparent
            isScrolled
            profileName={name}
            profile={profile}
          />
        </SafeAreaView>
      );
    },
    headerStyle: {
      elevation: 0,
      shadowOpacity: 0,
    },
  };
};

const PLPStack = createStackNavigator({
  PLP: {
    screen: SbpPlp,
    navigationOptions: ({navigation}) => {
      return getNewHeader(navigation, null, true);
    },
  },
  EditChildProfile: {
    screen: EditProfile,
    navigationOptions: ({navigation}) => {
      return getNewHeader(navigation, 'EDIT PROFILE');
    },
  },
  ProductDetailSBP: {
    screen: ProductDetail,
    params: getPdpRouteParams(),
    navigationOptions: ({navigation}) => {
      return getProductDetailHeader({
        navigation,
        showSearch: false,
        navTitle: null,
        isTransparentHeader: true,
      });
    },
  },
  OutfitDetail: {
    screen: OutfitDetail,
    navigationOptions: ({navigation}) => {
      return getProductDetailHeader({
        navigation,
        showSearch: false,
        navTitle: undefined,
        showSearchIcon: false,
        showCross: false,
      });
    },
  },
  OutfitDetailSBP: {
    screen: OutfitDetail,
    navigationOptions: ({navigation}) => {
      return getNewHeaderPDP(navigation);
    },
  },
  [ROUTE_NAMES.OUTFIT_LISTING]: {
    screen: OutfitListing,
    navigationOptions: ({navigation}) => {
      return getNewHeaderPDP(navigation);
    },
  },
});

export {PLPStack, CreateProfileStack};
