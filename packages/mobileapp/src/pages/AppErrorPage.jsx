// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import { PropTypes } from 'prop-types';
import { getBootstrapError } from '@tcp/core/src/reduxStore/selectors/session.selectors';
import { connect } from 'react-redux';

class AppErrorPage extends React.Component {
  componentDidUpdate() {
    const { bootstrapError, appPageErrorHandler } = this.props;
    if (bootstrapError) {
      appPageErrorHandler(true);
    }
  }

  render() {
    return <React.Fragment />;
  }
}

AppErrorPage.propTypes = {
  appPageErrorHandler: PropTypes.func.isRequired,
  bootstrapError: PropTypes.bool,
};

AppErrorPage.defaultProps = {
  bootstrapError: false,
};

const mapStateToProps = state => {
  const bootstrapError = getBootstrapError(state);
  let isValidError = false;
  if (bootstrapError !== null && typeof bootstrapError === 'object') {
    isValidError = bootstrapError.isErrorInBootstrap;
  }
  return {
    bootstrapError: isValidError,
  };
};

export default connect(mapStateToProps)(AppErrorPage);
