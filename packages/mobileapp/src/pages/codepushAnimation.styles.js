import styled from 'styled-components/native';
import createThemeColorPalette from '@tcp/core/styles/themes/createThemeColorPalette';

const colorPallete = createThemeColorPalette();

export const Container = styled.View`
  align-items: center;
  background-color: ${colorPallete.white};
  flex: 0.8;
  justify-content: center;
  width: 100%;
`;

export const Description = styled.Text`
  margin-bottom: 40;
  text-align: center;
`;

export const Header = styled.Text`
  font-size: 27;
  font-weight: bold;
  margin-bottom: 10;
  width: 100%;
  text-align: center;
`;

export const UserHeader = styled.Text`
  font-weight: bold;
  margin-bottom: 6;
`;

export const VersionSection = styled.View`
  flex: 1;
  position: absolute;
  bottom: 0;
  align-self: center;
  margin-top: 15;
`;

export const TextSection = styled.View`
  align-items: center;
  width: 60%;
`;
