import React, {useEffect} from 'react';
import {PropTypes} from 'prop-types';
import {Animated, Modal} from 'react-native';
import LottieView from 'lottie-react-native';
import {BodyCopy} from '@tcp/core/src/components/common/atoms';
import {withTheme} from 'styled-components';
import {
  Container,
  Description,
  Header,
  TextSection,
  VersionSection,
  UserHeader,
} from './codepushAnimation.styles';

const CodePushAnimationLoader = require('../assets/json/codepushAnimation.json');

const CodePushAnimation = ({progressPercent, username, version, removeSplash}) => {
  const rotateValue = new Animated.Value(progressPercent || 0);

  useEffect(() => {
    Animated.timing(rotateValue, {
      toValue: progressPercent,
      duration: 1000,
    }).start(() => {
      if (removeSplash) removeSplash();
    });
  }, [progressPercent]);

  const lottieStyle = {
    height: 300,
    width: 300,
  };
  const headerText = username ? `Hello ${username}` : 'Better Than Ever!';
  const descriptionText = `We are updating ${
    username ? 'the' : 'your'
  } app for a better experience!`;
  const versionText = `Version ${version}`;
  return (
    <Modal visible>
      <Container>
        <LottieView source={CodePushAnimationLoader} style={lottieStyle} progress={rotateValue} />
        <TextSection>
          <Header>{headerText}</Header>
          <Description>{descriptionText}</Description>
        </TextSection>
        <BodyCopy
          text={`${Math.ceil(progressPercent * 100)}%`}
          fontSize="fs24"
          fontWeight="black"
        />
      </Container>
      <VersionSection>
        <UserHeader>WHAT&apos;S NEW</UserHeader>
        <Description>{versionText}</Description>
      </VersionSection>
    </Modal>
  );
};

CodePushAnimation.propTypes = {
  progressPercent: PropTypes.number.isRequired,
  username: PropTypes.string.isRequired,
  version: PropTypes.string.isRequired,
  removeSplash: PropTypes.func.isRequired,
};

export default withTheme(CodePushAnimation);
