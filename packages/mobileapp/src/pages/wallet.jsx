// 9fbef606107a605d69c0edbcd8029e5d 
import React from 'react';
import {createStackNavigator} from 'react-navigation-stack';
import {Text} from 'react-native';
import NavBarIcon from '../components/common/atoms/NavBarIcon';

const Wallet = () => <Text>Wallet Page</Text>;

const WalletStack = createStackNavigator({
  Wallet,
});

WalletStack.navigationOptions = {
  tabBarLabel: 'wallet',
  tabBarIcon: props => (
    <NavBarIcon iconActive="wallet-active" iconInactive="wallet-inactive" {...props} />
  ),
};

export default WalletStack;
