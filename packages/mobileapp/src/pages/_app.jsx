/* eslint-disable max-lines */
/* dummy commit */
// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import NetInfo from '@react-native-community/netinfo';
import {StatusBar, StyleSheet, UIManager, Platform, AppState, Text} from 'react-native';
import QuantumMetricLibrary from 'react-native-quantum-metric-library';
import NetworkProvider, {
  useNetworkState,
} from '@tcp/core/src/components/common/hoc/NetworkProvider.app';
import {Box} from '@fabulas/astly';
import codePush from 'react-native-code-push';
import CookieManager from 'react-native-cookies';
import {decrypt, encrypt} from 'react-native-simple-encryption';
import AsyncStorage from '@react-native-community/async-storage';
import {bool, shape, string} from 'prop-types';
import {
  SetTcpSegmentMethodCall,
  setBrandSwitched,
  setAPIConfig,
  loadLabelsData,
  loadXappConfigData,
  loadXappConfigDataOtherBrand,
  fetchLastUpdated,
  setCartsSessionStatus,
  loadLayoutData,
  loadModulesData,
} from '@tcp/core/src/reduxStore/actions';

import {
  getUserLoggedInState,
  getMyPlaceNumber,
} from '@tcp/core/src/components/features/account/User/container/User.selectors';
import {setMaxItemErrorDisplayed} from '@tcp/core/src/components/features/CnC/BagPage/container/BagPage.actions.util';
import {initAppErrorReporter} from '@tcp/core/src/utils/errorReporter.util.native';
import {setCouponsFetchTimestampState} from '@tcp/core/src/components/features/CnC/common/organism/CouponAndPromos/container/Coupon.actions';
import COUPON_CONSTANTS from '@tcp/core/src/components/features/CnC/common/organism/CouponAndPromos/Coupon.constants';
import {
  createAPIConfig,
  switchAPIConfig,
  resetApiConfig,
  isAndroid,
  getAppVersion,
  disableTcpApp,
  checkIfUserInfoCallRequired,
  getAPIConfig,
  checkForDuplicateAuthCookies,
  clearLoginCookiesFromAsyncStore,
  isTCP,
} from '@tcp/core/src/utils';
import xappAbstractor from '@tcp/core/src/services/abstractors/bootstrap/xappConfig';
import labelsAbstractor from '@tcp/core/src/services/abstractors/bootstrap/labels';
import Loader from '@tcp/core/src/components/common/molecules/Loader';
import {getUserInfo} from '@tcp/core/src/components/features/account/User/container/User.actions';
import {fetchAbTestData} from '@tcp/core/src/components/common/molecules/abTest/abTest.actions';
import initializeForter from '@tcp/core/src/utils/forter.util.native';
import DefaultPreference from 'react-native-default-preference';
import {
  setQMSessionIdFromDefaultPreference,
  getCurrentAppVersion,
  readCookie,
  clearStateCookiesFromAsyncStore,
  setValueInAsyncStorage,
  getValueFromAsyncStorage,
  isIOS,
} from '@tcp/core/src/utils/utils.app';
import logger from '@tcp/core/src/utils/loggerInstance';
import ErrorPage from '@tcp/core/src/components/common/molecules/ErrorPage/container';
import {getMonetateEnabledFlag} from '@tcp/core/src/components/common/molecules/Recommendations/container/Recommendations.selector';
import {checkNotifications, RESULTS} from 'react-native-permissions';
import {trackForterAction, ForterActionType} from '@tcp/core/src/utils/forter.util';
import {setAppLaunch} from '@tcp/core/src/components/features/content/CustomNavigation/container/CustomNavigation.actions';
import env from 'react-native-config';
import {getAppVersionInfo} from '@tcp/core/src/components/common/molecules/AppUpdate/container/AppVersion.selectors';
import {appVersionRequest} from '@tcp/core/src/components/common/molecules/AppUpdate/container/AppVersion.actions';
import {setClickAnalyticsData, trackClick} from '@tcp/core/src/analytics/actions';
import {updateSBPEvent} from '@tcp/core/src/components/common/organisms/Header/container/Header.actions';
import {getDisableCodepushOptimisation} from '../components/features/content/HomePage/container/HomePage.selector';
/* Adobe analytics removed 28/01/2021 */
/*
import {
  ACPCore,
  ACPLifecycle,
  ACPIdentity,
  ACPSignal,
  ACPMobileLogLevel,
} from '@adobe/react-native-acpcore';
import {ACPAnalytics} from '@adobe/react-native-acpanalytics';
*/

// eslint-disable-next-line
import ReactotronConfig from './Reactotron';
import AppErrorPage from './AppErrorPage';
import ThemeWrapperHOC from '../components/common/hoc/ThemeWrapper.container';
import AppNavigator, {NoInternetNavigator} from '../navigation/AppNavigator';
import NavigationService from '../navigation/NavigationService';
import AppSplash from '../navigation/AppSplash.conainter';
import {APP_TYPE} from '../components/common/hoc/ThemeWrapper.constants';
import AnimatedBrandChangeIcon from '../components/common/atoms/AnimatedBrandChangeIcon/AnimatedBrandChangeIcon.container';
import {updateBrandName, createUUID} from '../utils/utils';
import {
  AppProvider,
  store,
  useInfoState,
  usePermissionState,
  useLocationState,
  useErrorReporter,
} from '../context';
import constants from '../constants/config.constants';
import withErrorBoundary from '../components/common/hoc/ErrorBoundary';
import Analytics from '../components/features/analytics';
import {getActiveRoute} from '../navigation/helpers/getActiveRoute';
import tcpLabels from '../assets/json/tcp-labels.json';
import gymLabels from '../assets/json/gym-labels.json';
import tcpXapp from '../assets/json/tcp-xapp.json';
import gymXapp from '../assets/json/gym-xapp.json';
import tcpLayout from '../assets/json/tcp-layout.json';
import gymLayout from '../assets/json/gym-layout.json';
import {isTierDevice} from '../utils/device-tiering';
import CodePushAnimation from './codepushAnimation';
// To Disable the Allow font scaling

if (Text.defaultProps == null) Text.defaultProps = {};
Text.defaultProps.allowFontScaling = false;

const styles = StyleSheet.create({
  // eslint-disable-next-line react-native/no-color-literals
  container: {
    backgroundColor: '#fff',
    flex: 1,
  },
});

const EXCLUDED_ROUTES_FOR_USER_INFO = ['Checkout'];
const INITIAL_STATUS_BAR_COLOR = '#FFFFFF';
const TRANSPARENTL_STATUS_BAR_COLOR = '#FFFFFF99';

initializeForter(store);
export class App extends React.PureComponent {
  state = {
    isSplashVisible: true,
    showBrands: false,
    apiConfig: null,
    onLoadCheckInternet: false,
    sessionId: createUUID(),
    showPeekABooAnimation: false,
    bootstrapError: false,
    appState: AppState.currentState,
    codePushInProgress: true,
    codePushUpdating: false,
    closeOverlay: true,
    progress: 0,
    appReadyToLaunch: false,
    isUserVisited: false,
    CPnontimeout: false,
  };

  /* eslint-disable-next-line */
  UNSAFE_componentWillMount() {
    this.store = store;
    const {appType} = this.props;
    // create and save api configs for tcp and gymboree in app
    updateBrandName(appType);
    const apiConfig = createAPIConfig(env, appType);
    this.setState({apiConfig});
    // Enable Layout animations for android
    if (isAndroid() && UIManager.setLayoutAnimationEnabledExperimental) {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
    getValueFromAsyncStorage('isUserOnBoardingScreenVisited').then((userVisited) => {
      if (userVisited === 'true') {
        this.setState({isUserVisited: true});
      }
    });
  }
  /* eslint-disable-next-line */
  handleAppStateChange = (nextAppState) => {
    const {appState} = this.state;
    if (appState.match(/inactive|background/) && nextAppState === 'active') {
      this.checkNotificationStatus();
    }
    this.setState({appState: nextAppState});
  };

  handleProgressUpdate = (progress, isRedesignCodePushAnimationDisabled) => {
    if (!isRedesignCodePushAnimationDisabled) {
      const codePushPercent = parseFloat(
        Number(progress.receivedBytes) / Number(progress.totalBytes),
      ).toFixed(2);
      if (Number(codePushPercent) >= 1) {
        this.setState({codePushInProgress: false, codePushUpdating: false, progress: 1});
      } else {
        this.setState({progress: Number(codePushPercent)});
      }
    }
  };

  handleCodePushUpdate = (update) => {
    const {isRedesignCodePushAnimationDisabled} = getAPIConfig();
    if (update.isMandatory) {
      codePush.disallowRestart();
      this.setState({codePushUpdating: true});
      codePush.sync(
        {},
        (status) => {
          if (
            status === codePush.SyncStatus.UP_TO_DATE ||
            status === codePush.SyncStatus.UPDATE_INSTALLED
          ) {
            this.setState({codePushInProgress: false, codePushUpdating: false}, () => {
              codePush.allowRestart();
              if (!update.failedInstall) {
                codePush.restartApp();
              }
            });
          }
        },
        (progress) => this.handleProgressUpdate(progress, isRedesignCodePushAnimationDisabled),
      );
    } else {
      setTimeout(() => {
        codePush.sync({
          installMode: codePush.InstallMode.ON_NEXT_RESTART,
        });
      }, 10000);
      this.setState({codePushInProgress: false, codePushUpdating: false, appReadyToLaunch: true});
    }
  };

  triggerCodePush = (appVersion) => {
    codePush.checkForUpdate().then(
      (update) => {
        try {
          if (update) {
            this.handleCodePushUpdate(update);
          } else {
            this.setState({
              codePushInProgress: false,
              appReadyToLaunch: true,
              codePushUpdating: false,
            });
          }
        } catch (e) {
          this.setState(
            {codePushInProgress: false, appReadyToLaunch: true, codePushUpdating: false},
            () => {
              logger.error({
                error: `Exception in code push update: ${JSON.stringify(e)}`,
                errorTags: ['app-code-push-error', appVersion],
              });
            },
          );
        }
      },
      () => {
        this.setState({
          codePushInProgress: false,
          codePushUpdating: false,
          appReadyToLaunch: true,
        });
      },
    );
  };

  getXappData = async (brandId) => {
    // for the main brand
    const xappKey = `${brandId}-config`;
    const {configurationKey: bundledXapp, lastUpdated} = isTCP() ? tcpXapp : gymXapp;
    const xappFromStorage = await getValueFromAsyncStorage(xappKey);

    let xapp = xappFromStorage ? JSON.parse(xappFromStorage) : {};
    if (Object.keys(xapp).length < 1) {
      // if async store has nothing it is our first time launching the app
      xapp = xappAbstractor.processData({configurationKey: bundledXapp});
      await setValueInAsyncStorage(xappKey, JSON.stringify(xapp));
    }

    // for the toggle brand
    const otherBrandId = brandId === 'tcp' ? 'gym' : 'tcp';
    const xappOtherBrandKey = `${otherBrandId}-config`;
    const {configurationKey: bundledOtherBrandXapp} = isTCP() ? gymXapp : tcpXapp;
    const otherBrandXappFromStorage = await getValueFromAsyncStorage(xappOtherBrandKey);
    let otherBrandXapp = otherBrandXappFromStorage ? JSON.parse(otherBrandXappFromStorage) : {};
    if (Object.keys(otherBrandXapp).length < 1) {
      otherBrandXapp = xappAbstractor.processData({
        configurationKey: bundledOtherBrandXapp,
      });
      await setValueInAsyncStorage(xappOtherBrandKey, JSON.stringify(otherBrandXapp));
    }

    await store.dispatch(loadXappConfigData(xapp));
    await store.dispatch(loadXappConfigDataOtherBrand(otherBrandXapp));
    return lastUpdated;
  };

  getLabelsData = async (brandId) => {
    const labelsKey = `${brandId}-labels`;
    const {labels: bundledLabels, lastUpdated} = isTCP() ? tcpLabels : gymLabels;
    const labelsFromStorage = await getValueFromAsyncStorage(labelsKey);
    let labels = labelsFromStorage ? JSON.parse(labelsFromStorage) : {};
    if (Object.keys(labels).length < 1) {
      labels = labelsAbstractor.processData(bundledLabels);
      await setValueInAsyncStorage(labelsKey, JSON.stringify(labels));
    }
    await store.dispatch(loadLabelsData(labels));
    return lastUpdated;
  };

  writeBundledLayoutToStorage = async (brandId) => {
    try {
      const layoutKey = `${brandId}-layout`;
      const modulesKey = `${brandId}-modules`;
      const {
        layout: bundledLayout,
        modules: bundledModules,
        lastUpdated,
      } = isTCP() ? tcpLayout : gymLayout;
      const layoutFromStorage = await getValueFromAsyncStorage(layoutKey);
      const modulesFromStorage = await getValueFromAsyncStorage(modulesKey);
      let layout = layoutFromStorage ? JSON.parse(layoutFromStorage) : {};
      let modules = modulesFromStorage ? JSON.parse(modulesFromStorage) : {};
      if (Object.keys(layout).length < 1) {
        // here we assume that layout and modules are both written simultaneously to async storage
        layout = bundledLayout;
        // eslint-disable-next-line extra-rules/no-commented-out-code
        // modules = LayoutAbstractor.processModuleData(bundledModules)
        modules = bundledModules;
        await setValueInAsyncStorage(layoutKey, JSON.stringify(layout));
        await setValueInAsyncStorage(modulesKey, JSON.stringify(modules));
      }
      await store.dispatch(loadLayoutData(layout.home.items[0].layout, 'home'));
      await store.dispatch(loadModulesData(modules));
      return lastUpdated;
    } catch (e) {
      logger.error({
        error: `Error in _app.jsx writeBundledLayoutToStorage: ${JSON.stringify(e)}`,
      });
      return 0;
    }
  };

  codePushTimeout(timeout = 5000) {
    const {apiConfig} = this.state;
    const {RWD_APP_VERSION} = apiConfig;
    const appVersion = getCurrentAppVersion() || RWD_APP_VERSION;

    return Promise.race([
      codePush
        .checkForUpdate()
        .then(
          (update) => {
            if (update) {
              this.handleCodePushUpdate(update);
            } else {
              this.setState({
                codePushInProgress: false,
                appReadyToLaunch: true,
                codePushUpdating: false,
              });
            }
          },
          () => {
            this.setState({
              codePushInProgress: false,
              appReadyToLaunch: true,
              codePushUpdating: false,
            });

            logger.error({
              error: `Exception in code push update`,
              errorTags: ['app-code-push-error-reject', appVersion],
            });
          },
        )
        .catch(() => {
          this.setState({
            codePushInProgress: false,
            appReadyToLaunch: true,
            codePushUpdating: false,
          });
          logger.error({
            error: `Exception in code push update`,
            errorTags: ['app-code-push-error-catch', appVersion],
          });
        }),
      new Promise((_, reject) => setTimeout(() => reject(new Error('timeout')), timeout)),
    ]);
  }

  compareVersion = (var1, var2) => {
    let v1 = var1;
    let v2 = var2;
    v1 = v1?.split('.');
    v2 = v2?.split('.');
    const k = Math.min(v1?.length, v2?.length);
    for (let i = 0; i < k; i += 1) {
      v1[i] = parseInt(v1[i], 10);
      v2[i] = parseInt(v2[i], 10);
      if (v1[i] > v2[i]) return true;
      if (v1[i] < v2[i]) return false;
    }
    return false;
  };

  isCPNeeded = () => {
    const getAppVersionInfoObj = getAppVersionInfo(store.getState('AppVersion'));
    if (
      (getAppVersionInfoObj?.iosCPVersion && getAppVersionInfoObj?.iosAppVersion) ||
      (getAppVersionInfoObj?.androidCPVersion && getAppVersionInfoObj?.androidAppVersion)
    ) {
      if (isIOS()) {
        return this.compareVersion(
          getAppVersionInfoObj?.iosCPVersion,
          getAppVersionInfoObj?.iosAppVersion,
        );
      }
      return this.compareVersion(
        getAppVersionInfoObj?.androidCPVersion,
        getAppVersionInfoObj?.androidAppVersion,
      );
    }
    return false;
  };

  codePushCheck = () => {
    const {CPnontimeout} = this.state;
    if (CPnontimeout !== true) {
      if (this.isCPNeeded()) {
        this.setState({CPnontimeout: true});
        this.codePushTimeout(5000)
          .then(
            () => {},
            (err) => {
              this.setState({
                codePushInProgress: false,
                appReadyToLaunch: true,
                codePushUpdating: false,
              });
              logger.error({
                error: `Exception in code push update: ${JSON.stringify(err)}`,
              });
            },
          )
          .catch(() => {
            this.setState({
              codePushInProgress: false,
              appReadyToLaunch: true,
              codePushUpdating: false,
            });
            logger.error({
              error: `Exception in code push update`,
            });
          });
      } else {
        this.setState({
          codePushInProgress: false,
          appReadyToLaunch: true,
          codePushUpdating: false,
        });
      }
    }
  };

  async componentDidMount() {
    const {apiConfig} = this.state;
    try {
      await store.dispatch(fetchLastUpdated(apiConfig));
      await store.dispatch(setAPIConfig(apiConfig));
      store.dispatch(setCartsSessionStatus(1));
      const CPKillSwitchStatus = getDisableCodepushOptimisation(store.getState());
      const {RAYGUN_API_KEY, brandId, RWD_APP_VERSION, isErrorReportingActive} = apiConfig;
      const appVersion = getCurrentAppVersion() || RWD_APP_VERSION;

      if (CPKillSwitchStatus) {
        this.triggerCodePush(appVersion);
      } else {
        await store.dispatch(appVersionRequest());
        setTimeout(() => {
          this.codePushCheck();
        }, 600);
      }

      store.dispatch(setAppLaunch(true));
      this.setCouponsUpdatedTime();
      const xappLastUpdated = await this.getXappData(brandId);
      const labelsLastUpdated = await this.getLabelsData(brandId);
      const layoutLastUpdated = await this.writeBundledLayoutToStorage(brandId);
      const username = await getValueFromAsyncStorage('username');
      setValueInAsyncStorage(
        `${brandId}-last-updated`,
        JSON.stringify({
          labels: labelsLastUpdated,
          configurationKeys: xappLastUpdated,
          layout: layoutLastUpdated,
        }),
      );

      if (isErrorReportingActive) {
        initAppErrorReporter({
          isDevelopment: __DEV__,
          raygunApiKey: RAYGUN_API_KEY,
          appType: brandId,
          envId: appVersion,
        });
      }
      // Getting QM Session Id from default preferences to send in API's header and Raygun Error
      if (Platform.OS === 'android') DefaultPreference.setName('NativeStorage');
      AppState.addEventListener('change', this.handleAppStateChange);

      DefaultPreference.get('QuantumMetricSessionCookie').then((value) => {
        setQMSessionIdFromDefaultPreference(value);
        /* this method needs to be exposed and will be called by Adobe target if required. and in fututr a payload neess to be passed in it , for reference please check _app.jsx of web.
         */
        /* Adobe analytics removed 28/01/2021
      this.initializeAnalyticsSdk();
      */

        this.store.dispatch(SetTcpSegmentMethodCall());
        this.getMobileAbtest();
        this.getConnectionInfo();

        // set the version first time.
        AsyncStorage.getItem('appVersion').then((result) => {
          if (result === null) {
            AsyncStorage.setItem('appVersion', getAppVersion());
          }
        });

        AsyncStorage.getItem('appLoadingBrand').then((result) => {
          if (result === null) {
            AsyncStorage.setItem('appLoadingBrand', brandId);
          }
        });

        this.initQuantum();
      });
      this.setCartMergeErrorMessageState();
      this.setState({username, version: getAppVersion()});
    } catch (e) {
      logger.error({
        error: `Exception in mobile app App.jsx did mount: ${JSON.stringify(e)}`,
      });
    }
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this.handleAppStateChange);
  }

  checkNotificationStatus = () => {
    checkNotifications().then(({status}) => {
      if (status === RESULTS.GRANTED) {
        trackForterAction(ForterActionType.ACCOUNT_LOGIN, 'Notification_Active');
      }
    });
  };

  /**
   * @method setCouponsUpdatedTime
   * @description - gets current date time from the async storage and if not available then
   * sets to async storage and redux state for the coupons api to set source as login when timeout is happended
   * Timeout value is driven from BE xapp config
   */
  setCouponsUpdatedTime = async () => {
    const {COUPONS_UPDATE_TIMEOUT} = COUPON_CONSTANTS;
    AsyncStorage.getItem(COUPONS_UPDATE_TIMEOUT).then((result) => {
      const currentDateTime = new Date().toString();
      if (!result) {
        AsyncStorage.setItem(COUPONS_UPDATE_TIMEOUT, currentDateTime);
        this.store.dispatch(setCouponsFetchTimestampState(currentDateTime));
      } else {
        this.store.dispatch(setCouponsFetchTimestampState(result));
      }
    });
  };

  /**
   * @method setCartMergeErrorMessageState
   * @description need to check if merge cart messaging is already done and no need to show cookie message again.
   * Added this as on android clearing specific cookie is not supporting.
   */
  setCartMergeErrorMessageState = () => {
    getValueFromAsyncStorage('mergeCartMessageShown').then((value) => {
      if (value) {
        this.store.dispatch(setMaxItemErrorDisplayed(true));
      }
    });
  };

  /**
   * @function initializeAnalyticsSdk
   * Initialize the Adobe Analytics SDK
   */
  /*
  initializeAnalyticsSdk = () => {
    try {
      const {apiConfig} = this.state;
      const appId = apiConfig.APP_ANALYTICS_ID;
      ACPCore.setLogLevel(ACPMobileLogLevel.VERBOSE);
      ACPCore.configureWithAppId(appId);
      ACPLifecycle.registerExtension();
      ACPIdentity.registerExtension();
      ACPSignal.registerExtension();
      ACPAnalytics.registerExtension();
      ACPCore.start();
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log(`Error initializing Analytics SDK: ${e}`);
    }
  };
*/
  /**
   * @function getMobileAbtest
   * This method dispatch empty action to trigger mbox call to get the abtest based on the current route/stack.
   *
   * @memberof App
   */
  getMobileAbtest = (currentScreen) => {
    const {context: {device: {screenHeight, screenWidth, hasNotch, systemName} = {}} = {}} =
      this.props;
    const {
      apiConfig: {brandId, webAppDomain, siteId, adobeDeliveryApi},
      sessionId,
    } = this.state;
    const MONETATE_ENABLED = getMonetateEnabledFlag();
    const isUserLoggedIn = getUserLoggedInState(store.getState());
    const myPlaceNumber = getMyPlaceNumber(store.getState());
    const action = {
      payload: {
        abTest: {},
        parameters: {
          currentScreen,
          screenHeight,
          screenWidth,
          hasNotch,
          systemName,
          brandId,
          webAppDomain,
          siteId,
          sessionId,
          adobeDeliveryApi,
          MONETATE_ENABLED,
          isUserLoggedIn,
          myPlaceNumber,
        },
      },
    };
    this.store.dispatch(fetchAbTestData(action));
  };

  setStatusBar = (currentScreen, prevScreen) => {
    if (currentScreen.params && currentScreen.params.isTransparentHeader) {
      StatusBar.setBackgroundColor(TRANSPARENTL_STATUS_BAR_COLOR);
      const isNewReDesignEnabled =
        store.getState()?.AbTest?.parsedAbtestResponse?.isNewReDesignEnabled;
      if (
        (isNewReDesignEnabled &&
          (prevScreen.routeName === 'BagPage' ||
            prevScreen.routeName === 'Account' ||
            prevScreen.routeName === 'PLP') &&
          currentScreen.routeName === 'ProductDetail') ||
        currentScreen.routeName === 'ProductDetailSBP'
      ) {
        StatusBar.setTranslucent(false);
      } else {
        StatusBar.setTranslucent(true);
      }
    } else {
      StatusBar.setBackgroundColor(INITIAL_STATUS_BAR_COLOR);
      StatusBar.setTranslucent(false);
    }
  };

  /**
   * @function handleNavigationStateChange
   * This method is triggered on each route change and result in triggering the getMobileAbtest method if the route changes
   *
   * @memberof App
   */
  handleNavigationStateChange = (prevState, currentState) => {
    NavigationService.onNavigationStateChange(prevState, currentState);

    const currentScreen = getActiveRoute(currentState);
    const prevScreen = getActiveRoute(prevState);
    global.CURRENT_ROUTE_NAME = currentScreen.routeName;
    if (currentScreen.routeName !== prevScreen.routeName) {
      this.getMobileAbtest(currentScreen);
      if (checkIfUserInfoCallRequired(EXCLUDED_ROUTES_FOR_USER_INFO, currentScreen.routeName)) {
        this.store.dispatch(getUserInfo());
      }
      if (isAndroid()) {
        this.setStatusBar(currentScreen, prevScreen);
      }
    }
  };

  initQuantum = () => {
    if (isAndroid() && QuantumMetricLibrary) {
      try {
        QuantumMetricLibrary.init();
      } catch (e) {
        // eslint-disable-next-line no-console
        console.log('QuantumMetricLibrary not initialized');
      }
    }
  };

  getStateCookieStringValue = async (stateCookieValue, stateCookieKey) => {
    let stateCookieString = '';
    if (stateCookieValue) {
      await clearStateCookiesFromAsyncStore();
      setValueInAsyncStorage(stateCookieKey, encrypt(stateCookieKey, stateCookieValue));
    } else {
      stateCookieString = stateCookieKey;
    }
    return stateCookieString;
  };

  setCooKies = async () => {
    const date = new Date();
    const daysAlive = constants.DAYS_ALIVE;
    const expiration = date.setTime(date.getTime() + daysAlive * 24 * 60 * 60 * 1000);
    const stateCookieKey = 'tcpState';
    const stateCookieValue = await readCookie(stateCookieKey);
    let stateCookieString = '';

    const {host} = getAPIConfig();
    if (host) {
      const duplicateKeyExists = await checkForDuplicateAuthCookies();
      if (duplicateKeyExists) {
        await clearLoginCookiesFromAsyncStore();
      }

      stateCookieString = await this.getStateCookieStringValue(stateCookieValue, stateCookieKey);

      AsyncStorage.getAllKeys().then((keys) => {
        AsyncStorage.multiGet(keys).then((items) => {
          const promises = [];
          // eslint-disable-next-line no-plusplus
          for (let index = 0; index < items.length; index++) {
            const item = items[index];
            if (
              item[0].startsWith('WC_') ||
              item[0] === 'tcp_isregistered' ||
              item[0] === stateCookieString
            ) {
              const cookie = {
                path: '/',
                value: decrypt(item[0], item[1]),
                domain: host,
                name: item[0],
                origin: host,
                version: '1',
                expiration,
              };
              promises.push(CookieManager.set(cookie));
            }
          }
          Promise.all(promises).then(() => {
            this.store.dispatch(
              getUserInfo({
                loadFavStore: true,
                removePersistentCookie: () => CookieManager.clearByName('WC_PERSISTENT'),
              }),
            );
          });
        });
      });
    }
  };

  removeSplash = () => {
    this.setState({isSplashVisible: false});
    this.setState({isUserVisited: true});
  };

  setCookiesCall = () => {
    if (Platform.OS === 'ios') {
      this.setCooKies();
    } else {
      this.store.dispatch(getUserInfo({loadFavStore: true}));
    }
  };

  /**
   * This will function will be triggered by <GlobalModals /> when all initial sequential modals
   * complete. For instance, first time user onboarding screen, location-permission request modal,
   * push notification permission request modal, etc.
   */
  onInitialModalsActionComplete = () => {
    const showPeekABooAnimation = disableTcpApp() !== true;
    /* Show pick-a-boo animation when user finish interacting with all initial modals */
    this.setState({showPeekABooAnimation});
  };

  /**
   * @function toggleBrandAction
   * This method toggles showBrands value in state, on the basis of which brand switch option is displayed on screen
   *
   * @memberof App
   */
  toggleBrandAction = (showAnimatedLogo) => {
    const showAnimatedBrands = disableTcpApp() ? false : showAnimatedLogo;
    this.setState({showBrands: showAnimatedBrands}, () => {
      this.setCookiesCall();
    });
  };

  /**
   * @function togglePeekAction
   * Turn off brand peek animation once it is complete on App launch
   *
   * @memberof App
   */
  togglePeekAction = ({finished}) => {
    if (finished) {
      this.setState({showPeekABooAnimation: false});
    }
  };

  /**
   * @function switchBrand
   * This methods current app type in utils and switches apiConfig in app
   *
   * @memberof App
   */

  switchBrand = async (appType) => {
    resetApiConfig();
    updateBrandName(appType);
    const apiConfig = switchAPIConfig(env);
    this.setState({apiConfig});
    this.store.dispatch(setBrandSwitched(true));
  };

  getConnectionInfo = () => {
    NetInfo.getConnectionInfo().then((connectionInfo) => {
      const infoType = Platform.OS === 'ios' ? 'none' : 'unknown';
      if (connectionInfo.type !== infoType) {
        this.setState(
          {
            onLoadCheckInternet: true,
          },
          () => {
            this.setCookiesCall();
          },
        );
      }
    });
  };

  retryNetwork = () => {
    const {
      context: {
        network: {isConnected},
      },
      appType,
    } = this.props;
    if (isConnected) {
      createAPIConfig(env, appType);
      this.setState(
        {
          onLoadCheckInternet: true,
        },
        () => {
          this.setCookiesCall();
        },
      );
    }
  };

  appPageErrorHandler = (status) => {
    this.setState({
      bootstrapError: status,
    });
  };

  analyticsOnFirstForSBP = async (customEvents, name) => {
    const data = {
      customEvents: [customEvents],
      name,
      clickEvent: true,
    };
    await store.dispatch(setClickAnalyticsData(data));
    await store.dispatch(trackClick({name: 'SBP_Making_More_Discoverable'}));
  };

  setFirst = async () => {
    const value = await getValueFromAsyncStorage('appFirstForSBP');
    if (value === null) {
      await setValueInAsyncStorage('appFirstForSBP', 'true');
      await this.setState({closeOverlay: true});
    } else {
      await setValueInAsyncStorage('appFirstForSBP', 'false');
      await this.setState({closeOverlay: false});
    }
  };

  checkFirst = async (screenName = '') => {
    const {closeOverlay} = this.state;
    const isUserLoggedIn = getUserLoggedInState(store.getState());
    await this.setState({closeOverlay: false});
    const value = await getValueFromAsyncStorage('appFirstForSBP');

    if (screenName === 'HomeStack') {
      if (value === null || value === 'true') {
        await this.setState({closeOverlay: true});
        await store.dispatch(updateSBPEvent(true));
      } else {
        await this.setState({closeOverlay: false});
      }
    } else {
      await this.setState({closeOverlay: false});
    }
    if (closeOverlay && isUserLoggedIn) {
      setTimeout(() => {
        this.gotoOutSideClick(false);
      }, 4000);
    }
  };

  gotoOutSideClick = async (triggerEvent = true) => {
    this.setState({
      closeOverlay: false,
    });
    await setValueInAsyncStorage('appFirstForSBP', 'false');
    if (triggerEvent) {
      this.analyticsOnFirstForSBP('event162', 'SBP_overlay_close_e162');
      setTimeout(() => {
        this.analyticsOnFirstForSBP('event163', 'SBP_create_custom_shop_click_e163');
      }, 2000);
    }
  };

  renderCodePushAnimation = () => {
    const {isRedesignCodePushAnimationDisabled} = getAPIConfig();

    if (isRedesignCodePushAnimationDisabled) {
      return this.renderOldCodePushAnimation();
    }
    return this.renderNewCodePushAnimation();
  };

  renderOldCodePushAnimation = () => {
    const {appType} = this.props;
    const {codePushInProgress, codePushUpdating} = this.state;

    return (
      <AppSplash
        appType={appType}
        removeSplash={this.removeSplash}
        codePushInProgress={codePushInProgress}
        codePushUpdating={codePushUpdating}
      />
    );
  };

  renderNewCodePushAnimation = () => {
    const {appType} = this.props;
    const {codePushInProgress, codePushUpdating, progress, username, version} = this.state;
    return (
      <>
        <CodePushAnimation
          progressPercent={progress}
          username={username}
          version={version}
          removeSplash={this.removeSplash}
        />
        <AppSplash
          appType={appType}
          removeSplash={this.removeSplash}
          codePushInProgress={codePushInProgress}
          codePushUpdating={codePushUpdating}
        />
      </>
    );
  };

  // prettier-ignore
  render() {
    const { appType, context, deviceTier} = this.props;
    const { network } = context;
    const {isRedesignCodePushAnimationDisabled} = getAPIConfig();
    const {
      isSplashVisible,
      showBrands,
      apiConfig,
      onLoadCheckInternet,
      showPeekABooAnimation,
      bootstrapError,
      codePushInProgress,
      codePushUpdating,
      closeOverlay,
      isUserLoggedIn,
      appReadyToLaunch,
      isUserVisited
    } = this.state;

    getUserLoggedInState(store.getState());
    if(codePushInProgress && codePushUpdating ) {
      return this.renderCodePushAnimation();
    }

    return (
      <ThemeWrapperHOC
        appType={appType}
        switchBrand={this.switchBrand}
        setCookiesCall={this.setCookiesCall}
      >
        {bootstrapError ? (
          <ErrorPage />
        ) : (
          <>
            {!appReadyToLaunch && (
              <AppSplash
                appType={appType}
                removeSplash={this.removeSplash}
                codePushInProgress={codePushInProgress}
                codePushUpdating={codePushUpdating}
              />
            )
            }
            {appReadyToLaunch && (
            <React.Fragment>
              {!codePushInProgress && isRedesignCodePushAnimationDisabled && <Loader isSplashVisible={isSplashVisible} />}
              {/* Patch fix for missing loaders in the App after taft release */}
              <Loader />
              <AppErrorPage appPageErrorHandler={this.appPageErrorHandler} />
              <Analytics />
              <Box style={styles.container}>
                {Platform.OS === 'ios' ? (
                  <StatusBar barStyle="default" />
                ) : (
                  <StatusBar backgroundColor={INITIAL_STATUS_BAR_COLOR} barStyle="dark-content" />
                )}

                {onLoadCheckInternet  ? (
                  <AppNavigator
                    ref={navigatorRef => {
                      NavigationService.setTopLevelNavigator(navigatorRef);
                    }}
                    onNavigationStateChange={this.handleNavigationStateChange}
                    screenProps={{
                      toggleBrandAction: this.toggleBrandAction,
                      showBrands,
                      apiConfig,
                      network,
                      showPeekABooAnimation,
                      onInitialModalsActionComplete: this.onInitialModalsActionComplete,
                      deviceTier,
                      togglePeekAction: this.togglePeekAction,
                      closeOverlay,
                      gotoOutSideClick: this.gotoOutSideClick,
                      checkFirst: this.checkFirst,
                      isSplashVisible,
                      isUserLoggedIn,
                      isTCP:isTCP(),
                    }}
                  />
                ) : (
                  <NoInternetNavigator
                    screenProps={{
                      retryNetwork: this.retryNetwork,
                      network,
                    }}

                  />
                )}
                {!isUserVisited && (
                  <AppSplash
                    appType={appType}
                    removeSplash={this.removeSplash}
                    codePushInProgress={codePushInProgress}
                    codePushUpdating={codePushUpdating}
                  />
                )}
                {showBrands && <AnimatedBrandChangeIcon toggleBrandAction={this.toggleBrandAction} />}
              </Box>
            </React.Fragment>
          )}
          </>
        )}
      </ThemeWrapperHOC>
    );
  }
}

App.propTypes = {
  appType: string,
  context: shape({
    network: shape({
      isConnected: bool,
    }),
  }).isRequired,
  deviceTier: string.isRequired,
};

App.defaultProps = {
  appType: APP_TYPE.TCP,
};

// @anup_mankar to look into this assignment
// eslint-disable-next-line no-class-assign
App = codePush({
  checkFrequency: codePush.CheckFrequency.MANUAL,
})(withErrorBoundary(App));

function RenderApp(props) {
  const info = useInfoState();
  const {permissions, request, ...rest} = usePermissionState();
  const location = useLocationState();
  const error = useErrorReporter();
  const network = useNetworkState();
  const context = {
    ...info,
    permissions: {...rest},
    location,
    error,
    network,
  };
  const {device} = context;
  const {appName, memoryTotal} = device;
  const appType = appName.toLowerCase().includes('gymboree') ? 'gymboree' : 'tcp';

  const deviceTier = isTierDevice(memoryTotal, Platform, network);
  /**
   * Setting first router name for apollo client.
   * It is used in cache tags
   * */
  global.CURRENT_ROUTE_NAME = 'Home';

  return <App context={context} deviceTier={deviceTier} appType={appType} {...props} />;
}

export default (props) => {
  return (
    <AppProvider>
      <NetworkProvider>
        <RenderApp {...props} />
      </NetworkProvider>
    </AppProvider>
  );
};
