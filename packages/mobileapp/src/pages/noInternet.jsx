// 9fbef606107a605d69c0edbcd8029e5d 
import {createStackNavigator} from 'react-navigation-stack';
import NoInternetPage from '../components/common/molecules/NoInternetPage';

const NoInternetStack = createStackNavigator(
  {
    NoInternetPage: {
      screen: NoInternetPage,
    },
  },
  {
    headerMode: 'none',
  },
);

export default NoInternetStack;
