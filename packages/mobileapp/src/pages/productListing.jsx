// 9fbef606107a605d69c0edbcd8029e5d
import React from 'react';
import {View, StatusBar} from 'react-native';
import {SafeAreaView, NavigationActions} from 'react-navigation';
import {isAndroid} from '@tcp/core/src/utils/utils.app';
import {createStackNavigator} from 'react-navigation-stack';
import ProductListing from '@tcp/core/src/components/features/browse/ProductListing';
import OutfitListing from '@tcp/core/src/components/features/browse/OutfitListing';
import OutfitDetail from '@tcp/core/src/components/features/browse/OutfitDetails';

import ProductDetail from '@tcp/core/src/components/features/browse/ProductDetail';
import SearchDetail from '@tcp/core/src/components/features/browse/SearchDetail';
import Confirmation from '@tcp/core/src/components/features/CnC/Confirmation';
import ProductBundleContainer from '@tcp/core/src/components/features/browse/BundleProduct';
import GetCandidGallery from '@tcp/core/src/components/common/molecules/GetCandidGallery/views/GetCandidGallery.native';
import StyleGuide from '@tcp/core/styles/themes/TCP';
import NavBarIcon from '../components/common/atoms/NavBarIcon';
import Header from '../components/common/molecules/Header';
import HeaderNew from '../components/common/molecules/Header/HeaderNew';
import Navigation from '../components/features/content/Navigation';
// import CategoryListing from '@tcp/core/src/components/features/browse/CategoryListing'; // @TODO when clp can link to PLP
import NavMenuLevel2 from '../components/features/content/Navigation/molecules/NavMenuLevel2';
import NavMenuLevel3 from '../components/features/content/Navigation/molecules/NavMenuLevel3';
import ROUTE_NAMES from '../reduxStore/routes';
import Colors from '../constants/Colors';

import {
  headerStyle,
  transcluscentHeaderStyle,
} from '../components/common/molecules/Header/Header.style';
import {getProductDetailHeader, getPdpRouteParams} from '../navigation/getProductDetailHeader';

const navigationInformation = (navigation, navTitle) => {
  const backgroundColor = (navigation && navigation.getParam('backgroundColor')) || 'white';
  const isSBP = navigation && navigation.getParam('isSBP');
  const title = navTitle || (navigation && navigation.getParam('title'));
  const isScrolled = navigation && navigation.getParam('isScrolled');
  const capitalizedTitle = title && title.toUpperCase();
  const newDesignBackgroundColor = navigation && navigation.getParam('newDesignColor');
  const disableHeaderBottomWidth = navigation && navigation.getParam('disableHeaderBottomWidth');
  const totalProductCount = navigation && navigation.getParam('totalProductCount');

  return {
    backgroundColor,
    isSBP,
    isScrolled,
    capitalizedTitle,
    newDesignBackgroundColor,
    disableHeaderBottomWidth,
    totalProductCount,
  };
};

const getProfileName = (navigation) => {
  return (
    navigation &&
    navigation.state.params &&
    navigation.state.params.profile &&
    navigation.state.params.profile.name
  );
};

const sbpHeaderAndroid = (
  showSearch,
  showSearchIcon,
  profileName,
  isTransparent,
  navigation,
  navOptions,
) => {
  const {isSBP, capitalizedTitle, disableHeaderBottomWidth, totalProductCount} =
    navigationInformation(navigation);
  return {
    header: (props) => (
      <View
        style={
          {
            /* intentionally empty */
          }
        }
        forceInset={{top: 'always'}}>
        <HeaderNew
          {...props}
          title={!isTransparent ? capitalizedTitle : ''}
          showSearch={showSearch}
          showSearchIcon={showSearchIcon}
          isSBP={isSBP}
          profileName={profileName}
          isTransparent
          isScrolled
          disableHeaderBottomWidth={disableHeaderBottomWidth}
          totalProductCount={totalProductCount}
        />
      </View>
    ),
    ...navOptions,
  };
};

const setStatusBar = (statusBarColor) => {
  StatusBar.setTranslucent(false);
  StatusBar.setBackgroundColor(statusBarColor);
};

const getHeaderParameters = (
  isTransparent,
  isScrolled,
  navigation,
  showSearchBarInline,
  backgroundColor,
) => {
  const navOptions = isTransparent ? {headerTransparent: true} : {headerBackground: 'transparent'};
  const profileName = getProfileName(navigation);
  const statusBarColor = showSearchBarInline ? Colors.shopMenuBackground : backgroundColor;
  const opacity = isScrolled ? 0.9 : 1;

  return {
    navOptions,
    profileName,
    statusBarColor,
    opacity,
  };
};

const getNewHeader = (
  navigation,
  showSearch,
  navTitle,
  showSearchIcon = false,
  isTransparent = false,
  overrideStyle = {},
  showSearchBarInline = false,
  isOutfitDetail = false,
  // eslint-disable-next-line
) => {
  const {
    backgroundColor,
    isSBP,
    isScrolled,
    capitalizedTitle,
    newDesignBackgroundColor,
    disableHeaderBottomWidth,
    totalProductCount,
  } = navigationInformation(navigation, navTitle);
  const headerParameters = getHeaderParameters(
    isTransparent,
    isScrolled,
    navigation,
    showSearchBarInline,
    backgroundColor,
  );
  const {navOptions, profileName, opacity} = headerParameters;
  let {statusBarColor} = headerParameters;
  let safeAreaStyles = {};

  if (isOutfitDetail && !isScrolled) {
    statusBarColor = Colors.shopMenuBackground;
  }
  if (isTransparent) {
    if (isScrolled) {
      safeAreaStyles = transcluscentHeaderStyle;
    }
  } else {
    safeAreaStyles = headerStyle;
  }

  if (newDesignBackgroundColor) {
    statusBarColor = StyleGuide.colors.PRIMARY.PALEGRAY;
  }
  if (isAndroid()) {
    setStatusBar(statusBarColor);
    if (isSBP) {
      sbpHeaderAndroid(
        showSearch,
        showSearchIcon,
        profileName,
        isTransparent,
        navigation,
        navOptions,
      );
    }
  }

  return {
    header: (props) => (
      <SafeAreaView
        style={[safeAreaStyles, {backgroundColor: statusBarColor, opacity}]}
        forceInset={{top: 'always'}}>
        <HeaderNew
          {...props}
          title={!isTransparent || isOutfitDetail ? capitalizedTitle : ''}
          showSearch={showSearch}
          showSearchIcon={showSearchIcon}
          isSBP={isSBP}
          profileName={profileName}
          isTransparent={isSBP ? true : isTransparent}
          isScrolled={isSBP ? true : isScrolled}
          overrideStyle={overrideStyle}
          showSearchBarInline={showSearchBarInline}
          disableHeaderBottomWidth={disableHeaderBottomWidth}
          totalProductCount={totalProductCount}
        />
      </SafeAreaView>
    ),
    ...navOptions,
  };
};
const navOverrideStyle = {'text-transform': 'uppercase', 'font-size': '14px'};

const PlpStack = createStackNavigator(
  {
    [ROUTE_NAMES.NAV_MENU_LEVEL_1]: {
      screen: Navigation, // CategoryListing
      navigationOptions: ({navigation}) => {
        return getNewHeader(navigation, false, '', false, false, {}, true);
      },
    },
    OutfitDetail: {
      screen: OutfitDetail,
      navigationOptions: ({navigation}) => {
        const isOutfitDetail = true;
        return getNewHeader(navigation, false, '', false, true, {}, false, isOutfitDetail);
      },
    },
    [ROUTE_NAMES.NAV_MENU_LEVEL_2]: {
      screen: NavMenuLevel2,
      navigationOptions: ({navigation}) => {
        const title = navigation && navigation.getParam('l1Title');
        return getNewHeader(navigation, false, title, true, false, navOverrideStyle);
      },
    },
    [ROUTE_NAMES.NAV_MENU_LEVEL_3]: {
      screen: NavMenuLevel3,
      navigationOptions: ({navigation}) => {
        const title = navigation && navigation.getParam('l2Title');
        return getNewHeader(navigation, false, title, true, false, navOverrideStyle);
      },
    },
    [ROUTE_NAMES.PRODUCT_LISTING]: {
      screen: ProductListing,
      navigationOptions: ({navigation}) => {
        return getNewHeader(navigation, false, '', true);
      },
    },
    [ROUTE_NAMES.PRODUCT_DETAIL_PAGE]: {
      screen: ProductDetail,
      params: getPdpRouteParams(),
      navigationOptions: ({navigation}) => {
        const isSBP = navigation && navigation.getParam('isSBP');
        if (isSBP) return getNewHeader(navigation);
        return getProductDetailHeader({
          navigation,
          showSearch: false,
          navTitle: null,
          showCross: true,
        });
      },
    },
    [ROUTE_NAMES.PRODUCT_DETAIL_PAGE_SHOP_BY_PROFILE]: {
      screen: ProductDetail,
      navigationOptions: ({navigation}) => {
        return getNewHeader(navigation, false, null, true, true);
      },
    },
    [ROUTE_NAMES.OUTFIT_LISTING]: {
      screen: OutfitListing,
      navigationOptions: ({navigation}) => {
        return getNewHeader(navigation);
      },
    },
    [ROUTE_NAMES.SEARCH_RESULTS_PAGE]: {
      screen: SearchDetail,
      navigationOptions: ({navigation}) => {
        const title = navigation && navigation.getParam('title');
        const navTitle = (title && `"${title.toUpperCase()}"`) || '';
        return getNewHeader(navigation, false, navTitle, true);
      },
    },
    [ROUTE_NAMES.CONFIRMATION]: {
      screen: Confirmation,
    },
    [ROUTE_NAMES.BUNDLE_DETAIL]: {
      screen: ProductBundleContainer,
      params: getPdpRouteParams(),
      navigationOptions: ({navigation}) => {
        return getProductDetailHeader({
          navigation,
          showSearch: false,
          navTitle: null,
          showCross: true,
        });
      },
    },
    GetCandidGallery: {
      screen: GetCandidGallery,
      navigationOptions: ({navigation}) => {
        return getNewHeader(navigation, false);
      },
    },
  },
  {
    defaultNavigationOptions: {
      header: (props) => (
        <SafeAreaView style={headerStyle} forceInset={{top: 'always', bottom: 'never'}}>
          <Header {...props} showSearch />
        </SafeAreaView>
      ),
      headerBackground: 'transparent',
    },
  },
);

PlpStack.navigationOptions = ({navigation}) => {
  const {
    state: {index, routes},
  } = navigation;

  const navigateAction = NavigationActions.navigate({
    routeName: 'PlpStack',
    params: {},
    action: NavigationActions.navigate({routeName: ROUTE_NAMES.NAV_MENU_LEVEL_1}),
  });

  const tabBarOnPress = ({defaultHandler}) => {
    if (navigation && navigation.state) {
      const {routeName} = navigation.state.routes[navigation.state.index];

      if (routeName === ROUTE_NAMES.CONFIRMATION) {
        navigation.dispatch(navigateAction);
        return;
      }
    }

    defaultHandler();
  };

  return {
    tabBarOnPress,
    tabBarLabel: 'shop',
    tabBarIcon: (props) => (
      <NavBarIcon iconActive="shop-active" iconInactive="shop-inactive" {...props} />
    ),
    tabBarVisible: routes[index].routeName !== 'OutfitDetail',
  };
};

export default PlpStack;
