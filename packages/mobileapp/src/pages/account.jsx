// 9fbef606107a605d69c0edbcd8029e5d
/* eslint-disable react/prop-types */
import React from 'react';
import {SafeAreaView} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import PointsPage from '@tcp/core/src/components/features/account/PointsClaim';
import {navigateToNestedRoute} from '@tcp/core/src/utils/utils.app';
import PointsHistory from '@tcp/core/src/components/features/account/PointHistory';
import ProductDetail from '@tcp/core/src/components/features/browse/ProductDetail';
import TrackOrderContainer from '@tcp/core/src/components/features/account/TrackOrder';
import OrderDetail from '@tcp/core/src/components/features/account/OrderDetails';
import Settings from '@tcp/core/src/components/features/account/Settings';
import PurchaseGiftsCard from '@tcp/core/src/components/features/account/PurchaseGiftsCard';
import StoreLanding from '@tcp/core/src/components/features/storeLocator/StoreLanding/container/StoreLanding.container';
import StoreDetails from '@tcp/core/src/components/features/storeLocator/StoreDetail';
import OrderReceipt from '@tcp/core/src/components/features/account/OrderDetails/organism/OrderReceipt';
import EGiftsCard from '@tcp/core/src/components/features/GiftCard';
import InAppWebView from '../components/features/content/InAppWebView';
import LoginSync from '../screens/LoginSync';
import NavBarIcon from '../components/common/atoms/NavBarIcon';
import Account from '../components/features/account/account';
import Header from '../components/common/molecules/Header';
import ROUTE_NAMES from '../reduxStore/routes';
import HeaderNew from '../components/common/molecules/Header/HeaderNew';
import {headerStyle} from '../components/common/molecules/Header/Header.style';
import {getProductDetailHeader, getPdpRouteParams} from '../navigation/getProductDetailHeader';

const getNewHeader = (
  navigation,
  showSearch = false,
  showSearchIcon = false,
  overrideStyle = {},
) => {
  const title = navigation && navigation.getParam('title');
  const showHeader = navigation && navigation.getParam('noHeader');
  return {
    header: (props) =>
      !showHeader ? (
        <SafeAreaView style={headerStyle} forceInset={{top: 'always', bottom: 'never'}}>
          <HeaderNew
            {...props}
            title={title}
            navigation={navigation}
            showSearch={showSearch}
            showSearchIcon={showSearchIcon}
            overrideStyle={overrideStyle}
          />
        </SafeAreaView>
      ) : null,
    headerBackground: 'transparent',
  };
};

const getDefaultHeaderForStore = (navigation, navTitle) => {
  const title = navTitle || (navigation && navigation.getParam('title'));
  return {
    header: (props) => (
      <SafeAreaView style={headerStyle} forceInset={{top: 'always', bottom: 'never'}}>
        <Header {...props} title={title} navigation={navigation} headertype="store" />
      </SafeAreaView>
    ),
    headerBackground: 'transparent',
  };
};

const storeLocatorNavigationOptions = ({navigation}) => {
  const title = navigation && navigation.getParam('title');
  const navTitle = (title && `${title.toUpperCase()}`) || '';
  return getDefaultHeaderForStore(navigation, navTitle);
};

const AccountStack = createStackNavigator(
  {
    Account,
    LoginSync,
    EGiftCardPage: {
      screen: EGiftsCard,
      navigationOptions: ({navigation}) => {
        return getNewHeader(navigation);
      },
    },
    PointsClaimPage: {
      screen: PointsPage,
      navigationOptions: ({navigation}) => {
        return getNewHeader(navigation, false, true);
      },
    },
    GiftCardPage: {
      screen: PurchaseGiftsCard,
      navigationOptions: ({navigation}) => {
        return getNewHeader(navigation, false, true, {
          'text-transform': 'uppercase',
          'font-size': '14px',
        });
      },
    },
    PointsHistoryPage: {
      screen: PointsHistory,
      navigationOptions: ({navigation}) => {
        return getNewHeader(navigation, false, true);
      },
    },
    TrackOrder: {
      // eslint-disable-next-line react/prop-types
      screen: ({navigation}) => {
        const handleToggle = navigation.getParam('handleToggle');
        const trackOrderInitiator = navigation.getParam('trackOrderInitiator');
        return (
          <TrackOrderContainer
            trackOrderInitiatorInApp={trackOrderInitiator}
            handleToggle={handleToggle}
            navigation={navigation}
          />
        );
      },
      navigationOptions: ({navigation}) => {
        return getNewHeader(navigation);
      },
    },
    AppSettings: {
      // eslint-disable-next-line react/prop-types
      screen: ({navigation}) => {
        const handleToggle = navigation.getParam('handleToggle');
        const isUserLoggedIn = navigation.getParam('isUserLoggedIn');
        return (
          <Settings
            handleToggle={handleToggle}
            navigation={navigation}
            isUserLoggedIn={isUserLoggedIn}
          />
        );
      },
      navigationOptions: ({navigation}) => {
        return getNewHeader(navigation);
      },
    },
    InAppView: {
      // eslint-disable-next-line react/prop-types
      screen: ({navigation}) => {
        const url = navigation.getParam('url');
        return <InAppWebView pageUrl={url} navigation={navigation} />;
      },
      navigationOptions: ({navigation}) => {
        return getNewHeader(navigation);
      },
    },
    OrderDetailPage: {
      // eslint-disable-next-line react/prop-types
      screen: ({navigation}) => {
        const router = navigation.getParam('router');
        return <OrderDetail navigation={navigation} router={router} />;
      },
      navigationOptions: ({navigation}) => {
        return getNewHeader(navigation, false, true);
      },
    },
    [ROUTE_NAMES.ORDER_RECEIPT]: {
      // eslint-disable-next-line react/prop-types
      screen: ({navigation}) => {
        const router = navigation.getParam('router');
        return <OrderReceipt navigation={navigation} {...router} />;
      },
      navigationOptions: ({navigation}) => {
        return getNewHeader(navigation, false, false);
      },
    },
    [ROUTE_NAMES.PRODUCT_DETAIL_PAGE]: {
      screen: ProductDetail,
      params: getPdpRouteParams(),
      navigationOptions: ({navigation}) => {
        return getProductDetailHeader({
          navigation,
          showSearch: false,
          navTitle: null,
        });
      },
    },
    StoreDetails: {
      screen: StoreDetails,
      path: 'store-details/:storeId',
      navigationOptions: storeLocatorNavigationOptions,
    },
    StoreLanding: {
      screen: StoreLanding,
      path: 'store-landing',
      navigationOptions: storeLocatorNavigationOptions,
    },
  },
  {
    defaultNavigationOptions: {
      header: (props) => (
        <SafeAreaView style={headerStyle} forceInset={{top: 'always', bottom: 'never'}}>
          <Header {...props} showSearch />
        </SafeAreaView>
      ),
      headerBackground: 'transparent',
    },
  },
);

AccountStack.navigationOptions = {
  tabBarLabel: 'account',
  tabBarIcon: (props) => (
    <NavBarIcon iconActive="account-active" iconInactive="account-inactive" {...props} />
  ),
  tabBarOnPress: ({navigation}) => {
    navigateToNestedRoute(navigation, 'AccountStack', 'Account', {
      isResetStack: true,
    });
  },
};

export default AccountStack;
