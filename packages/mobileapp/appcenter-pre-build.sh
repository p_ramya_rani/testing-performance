#!/usr/bin/env bash
# 9fbef606107a605d69c0edbcd8029e5d
# Apply a patch for the asset resolver
/bin/rm -f node_modules/react-native/Libraries/Image/AssetSourceResolver.js
/bin/cp -f ../../patches/react-native/AssetSourceResolver.txt node_modules/react-native/Libraries/Image/AssetSourceResolver.js

# Bundle Labels and XAPP into the app.
cd ../../ && yarn run bundle-labels --tcpUrl="$RWD_APP_GRAPHQL_API_ENDPOINT_TCP" --gymUrl="$RWD_APP_GRAPHQL_API_ENDPOINT_GYM"
cd ../../ && yarn run bundle-xapp --tcpUrl="$RWD_APP_GRAPHQL_API_ENDPOINT_TCP" --gymUrl="$RWD_APP_GRAPHQL_API_ENDPOINT_GYM"
cd ../../ && yarn run bundle-layout --tcpUrl="$RWD_APP_GRAPHQL_API_ENDPOINT_TCP" --gymUrl="$RWD_APP_GRAPHQL_API_ENDPOINT_GYM"
cd packages/mobileapp

# Creates an .env from ENV variables for use with react-native-config
if [ ! -z "$ENV" ]; then
   ENV_WHITELIST=${ENV_WHITELIST:-"^RWD_APP"}
   printf "Creating an .env file with the following whitelist:\n"
   if [[ "$ENV" = "int" ]]; then
     set | egrep -e $ENV_WHITELIST > src/env/.env.int
     printf "\n.env.int created with contents:\n"
     cat src/env/.env.int
   elif [[ "$ENV" = "preview" ]]; then
     set | egrep -e $ENV_WHITELIST > src/env/.env.preview
     printf "\n.env.preview created with contents:\n"
     cat src/env/.env.preview
   elif [[ "$ENV" = "qa" ]]; then
     set | egrep -e $ENV_WHITELIST > src/env/.env.qa
     printf "\n.env.qa created with contents:\n"
     cat src/env/.env.qa
   elif [[ "$ENV" = "qa-preview" ]]; then
     set | egrep -e $ENV_WHITELIST > src/env/.env.qa-preview
     printf "\n.env.qa-preview created with contents:\n"
     cat src/env/.env.qa-preview
   elif [[ "$ENV" = "uat" ]]; then
     set | egrep -e $ENV_WHITELIST > src/env/.env.uat
     printf "\n.env.uat created with contents:\n"
     cat src/env/.env.uat
   elif [[ "$ENV" = "uat-preview" ]]; then
     set | egrep -e $ENV_WHITELIST > src/env/.env.uat-preview
     printf "\n.env.uat-preview created with contents:\n"
     cat src/env/.env.uat-preview
   elif [[ "$ENV" = "prod" ]]; then
     set | egrep -e $ENV_WHITELIST > src/env/.env.prod
     printf "\n.env.prod created with contents:\n"
     cat src/env/.env.prod
   elif [[ "$ENV" = "prod-preview" ]]; then
     set | egrep -e $ENV_WHITELIST > src/env/.env.prod-preview
     printf "\n.env.prod-preview created with contents:\n"
     cat src/env/.env.prod-preview
   else
     set | egrep -e $ENV_WHITELIST > src/env/.env.int
     printf "\n.not environment found, defaulting to int env:\n"
     cat src/env/.env.int
   fi
fi
printenv
