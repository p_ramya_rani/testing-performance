package com.mobileapp.notifications;

import android.content.Context;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.mobileapp.R;
import com.vibes.vibes.NotificationFactory;
import com.vibes.vibes.PushPayloadParser;

public class VibesNotificationFactory extends NotificationFactory {

  public VibesNotificationFactory(Context context) {
    super(context);
  }

  @Override
  public NotificationCompat.Builder getBuilder(PushPayloadParser pushModel, Context context) {
    return super.getBuilder(pushModel, context)
      .setColor(ContextCompat.getColor(context, R.color.colorBase))
      .setSmallIcon(R.drawable.ic_notification);

  }

}
