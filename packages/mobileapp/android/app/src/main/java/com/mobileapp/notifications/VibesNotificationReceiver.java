package com.mobileapp.notifications;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.facebook.react.ReactApplication;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.bridge.ReactContext;
import com.google.gson.Gson;
import com.mobileapp.R;
import com.mobileapp.vibes.PushEvtEmitter;
import com.mobileapp.vibes.VibesAppHelper;
import com.vibes.vibes.PushPayloadParser;
import com.vibes.vibes.VibesReceiver;

import static android.content.Context.MODE_PRIVATE;

public class VibesNotificationReceiver extends VibesReceiver {
  private static final String TAG = "Vibes Receiver";
  private static Context context;

  @Override
  protected void onPushOpened(Context context, PushPayloadParser pushModel) {
    super.onPushOpened(context, pushModel);
    VibesNotificationReceiver.context = context;

    //save current notification to preferences
    String currentNotification = new Gson().toJson(pushModel);
    SharedPreferences preferences = context.getSharedPreferences(context.getString(R.string.vibesPreference), MODE_PRIVATE);
    SharedPreferences.Editor editor = preferences.edit();
    editor.putString(context.getString(R.string.currentNotification), currentNotification);
    editor.apply();

    VibesAppHelper appHelper = new VibesAppHelper((Application) context.getApplicationContext());
    appHelper.invokeApp();
    Log.d(TAG, "Push message tapped. Emitting event");
    emitPayload(context, pushModel, false);
  }

  @Override
  protected void onPushDismissed(Context context, PushPayloadParser pushModel) {
    Log.d(TAG, "Push message Received in Dismissed");
  }

  public static void emitCurrentPush(){
    try {
      if (context == null) return;
      SharedPreferences preferences = context.getSharedPreferences(context.getString(R.string.vibesPreference), MODE_PRIVATE);
      String currentNotification = preferences.getString(context.getString(R.string.currentNotification), null);

      if (currentNotification != null) {

        PushPayloadParser pushModel = new Gson().fromJson(currentNotification, PushPayloadParser.class);
        Log.d(TAG, "pushModal " + pushModel);
        Log.d(TAG, "Emitting Current Notification");
        emitPayload(context, pushModel, true);

        //Clear Current Notification
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(context.getString(R.string.currentNotification), null);
        editor.apply();
      }
    } catch (Exception e) {
      Log.d(TAG, "Unable to get current notification");
      e.printStackTrace();
    }
  }

  private static void emitPayload(Context context, PushPayloadParser pushModel, Boolean isAppOpenWithNotification) {
//        final VibesNotificationReceiver serviceRef = this;
    // We need to run this on the main thread, as the React code assumes that is true.
    // Namely, DevServerHelper constructs a Handler() without a Looper, which triggers:
    // "Can't create handler inside thread that has not called Looper.prepare()"
    Handler handler = new Handler(Looper.getMainLooper());
    handler.post(new Runnable() {
      public void run() {
        // Construct and load our normal React JS code bundle
        final ReactInstanceManager mReactInstanceManager = ((ReactApplication) context.getApplicationContext()).getReactNativeHost().getReactInstanceManager();
        ReactContext context = mReactInstanceManager.getCurrentReactContext();
        // If it's constructed, send a notification
        if (context != null) {
          PushEvtEmitter pushEmitter = new PushEvtEmitter(context);
          pushEmitter.notifyPushOpened(pushModel,isAppOpenWithNotification);
        } else {
          // Otherwise wait for construction, then send the notification
          mReactInstanceManager.addReactInstanceEventListener(new ReactInstanceManager.ReactInstanceEventListener() {
            public void onReactContextInitialized(ReactContext context) {
              PushEvtEmitter pushEmitter = new PushEvtEmitter(context);
              pushEmitter.notifyPushOpened(pushModel,isAppOpenWithNotification);
              mReactInstanceManager.removeReactInstanceEventListener(this);
            }
          });
          if (!mReactInstanceManager.hasStartedCreatingInitialContext()) {
            // Construct it in the background
            mReactInstanceManager.createReactContextInBackground();
          }
        }
      }
    });
  }
}
