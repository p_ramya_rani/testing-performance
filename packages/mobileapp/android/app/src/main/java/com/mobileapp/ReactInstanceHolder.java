// 9fbef606107a605d69c0edbcd8029e5d 
package com.mobileapp;


import com.facebook.react.ReactInstanceManager;


public interface ReactInstanceHolder {

    /**
     * Get the current {@link ReactInstanceManager} instance. May return null.
     */
    ReactInstanceManager getReactInstanceManager();
}