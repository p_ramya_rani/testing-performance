package com.mobileapp.vibes;

import android.app.Application;
import android.util.Log;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.mobileapp.notifications.VibesNotificationReceiver;
import com.vibes.vibes.Vibes;
import com.vibes.vibes.VibesListener;

public class VibesModule extends ReactContextBaseJavaModule {

  private final String moduleName = "VibesModule";
  public static final String VIBES_TAG = "Vibes";
  public static final String VIBES_PREFERENCE = "Vibes";
  private VibesAppHelper appHelper;

  public VibesModule(ReactApplicationContext reactApplicationContext) {
    super(reactApplicationContext);
    appHelper = new VibesAppHelper((Application) reactApplicationContext.getApplicationContext());
  }

  @Override
  public String getName() {
    return moduleName;
  }

  @ReactMethod
  public void invokeApp(final Promise promise) {
    appHelper.invokeApp();
    promise.resolve(null);;
  }

  /**
   * @param externalPersonId The id to associate with the logged-in person.
   */
  @ReactMethod
  public void associatePerson(final String externalPersonId, final Promise promise) {
    Log.d(VIBES_TAG, "Associating Person --> " + externalPersonId);
    VibesListener<Void> listener = new VibesListener<Void>() {
      public void onSuccess(Void value) {
        promise.resolve("ASSOCIATE_PERSON_SUCCESS");
      }

      public void onFailure(String errorText) {
        promise.reject("ASSOCIATE_PERSON_FAILED", errorText);
      }
    };
    this.associatePerson(externalPersonId, listener);
  }

  private void associatePerson(String externalPersonId, VibesListener<Void> listener) {
    Vibes.getInstance().associatePerson(externalPersonId, listener);
  }

  @ReactMethod
  public void unregisterPush(final Promise promise) {
        VibesListener<Void> listener = new VibesListener<Void>() {
            public void onSuccess(Void credential) {
                Log.d(VIBES_TAG, "Unregister push successful");
                promise.resolve("Success");
            }

            public void onFailure(String errorText) {
                Log.d(VIBES_TAG, "Unregister push failed");
                promise.reject("REGISTER_PUSH_ERROR", errorText);
            }
        };
        Vibes.getInstance().unregisterPush(listener);
  }

  @ReactMethod
  public void registerPush(final Promise promise) {
        String pushToken = appHelper.getPushToken();
        if(pushToken == null ){
            String msg = "Failure registering token with vibes: Token not yet generated";
            promise.reject("UNREGISTER_PUSH_ERROR", msg);
            return;
        }
        Log.d(VIBES_TAG, "Vibes Module Registering push vibes");
        Vibes.getInstance().registerPush(pushToken, new VibesListener<Void>() {
            public void onSuccess(Void credential) {
                Log.d(VIBES_TAG, "Push token registration success");
                promise.resolve("Success");
                Log.d(VIBES_TAG, "Finish");
            }

            public void onFailure(String errorText) {
                Log.d(VIBES_TAG, "Failure registering token with vibes: " + errorText);
                promise.reject("UNREGISTER_PUSH_ERROR", errorText);
            }
        });
  }

  @ReactMethod
  public void isAppOpenWithNotification() {
    VibesNotificationReceiver.emitCurrentPush();
  }
}
