package com.mobileapp.vibes;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.mobileapp.MainApplication;

import static com.mobileapp.vibes.VibesModule.VIBES_PREFERENCE;
import static com.mobileapp.vibes.VibesModule.VIBES_TAG;


public class VibesAppHelper {
  private Context context;

  public VibesAppHelper(Application context) {
    this.context = context;
  }

  private Class getMainActivityClass() {
    String packageName = context.getPackageName();
    Intent launchIntent = context.getPackageManager().getLaunchIntentForPackage(packageName);
    String className = launchIntent.getComponent().getClassName();
    try {
      return Class.forName(className);
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
      return null;
    }
  }

  public void invokeApp() {
    try {
      Class<?> activityClass = getMainActivityClass();
      Intent activityIntent = new Intent(context, activityClass);
      activityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

      context.startActivity(activityIntent);
    } catch (Exception e) {
      Log.e(VIBES_TAG, "Class not found", e);
      return;
    }
  }

  public String getPushToken(){
    SharedPreferences preferences =context.getSharedPreferences(VIBES_PREFERENCE, Context.MODE_PRIVATE);
    return preferences.getString(MainApplication.TOKEN_KEY,null);
  }

}
