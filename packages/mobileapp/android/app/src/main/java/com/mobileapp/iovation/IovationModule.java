package com.mobileapp.iovation;

import android.content.Context;
import android.os.Bundle;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.iovation.mobile.android.FraudForceManager;

public class IovationModule extends ReactContextBaseJavaModule {
  //constructor
  public IovationModule(ReactApplicationContext reactContext) {
    super(reactContext);
  }

  //Mandatory Function getName that specifies the module name
  @Override
  public String getName() {
    return "IovationModule";
  }

  //Custom Function That we are going to export to JS
  @ReactMethod
  public void getBlackBoxData(Callback cb, Callback err) {
    try {
      FraudForceManager.getInstance().refresh(getReactApplicationContext());
      String blackBox = FraudForceManager.getInstance().getBlackbox(getReactApplicationContext());
      cb.invoke(blackBox, null);
    } catch (Exception e) {
      err.invoke(e.toString(), null);
    }
  }
}
