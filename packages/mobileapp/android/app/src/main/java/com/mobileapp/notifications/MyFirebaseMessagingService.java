// 9fbef606107a605d69c0edbcd8029e5d
package com.mobileapp.notifications;

import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.facebook.react.ReactApplication;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.bridge.ReactContext;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.mobileapp.MainApplication;
import com.mobileapp.vibes.PushEvtEmitter;
import com.vibes.vibes.PushPayloadParser;
import com.vibes.vibes.Vibes;

import java.util.Map;

import static com.mobileapp.vibes.VibesModule.VIBES_PREFERENCE;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

  private static final String TAG = "Firebase Service";

  /**
   * @see #onMessageReceived(RemoteMessage)
   */
  @Override
  public void onMessageReceived(RemoteMessage message) {
    Log.d(TAG, "Push message received. Processing");
    PushPayloadParser pushModel = this.createPushPayloadParser(message.getData());

    // Print for debug
    PayloadWrapper wrapper = new PayloadWrapper(pushModel);
    Log.d(TAG, wrapper.toString());

    // pass the received payload to the handleNotification SDK method. It takes care
    // of displaying the message
    Vibes.getInstance().setNotificationFactory(new VibesNotificationFactory(getApplicationContext()));
    Vibes.getInstance().handleNotification(getApplicationContext(), message.getData());
    if (!pushModel.isSilentPush()) {
      emitPayload(pushModel);
    }
  }

  /**
   * This is invoked everytime the application generates a new Firebase push
   * token, which is then sent the the Vibes server to be able to target this
   * device with push messages.
   *
   * @param pushToken
   */
  @Override
  public void onNewToken(String pushToken) {
    super.onNewToken(pushToken);
    Log.d(TAG, "Firebase token obtained as " + pushToken);
    SharedPreferences preferences = getSharedPreferences(VIBES_PREFERENCE, MODE_PRIVATE);
    SharedPreferences.Editor editor = preferences.edit();
    editor.putString(MainApplication.TOKEN_KEY, pushToken);
    editor.apply();

    boolean registered = preferences.getBoolean(MainApplication.VIBES_REGISTERED, false);
    if (registered) {
      MainApplication.registerPush(pushToken);
    }
  }

  public PushPayloadParser createPushPayloadParser(Map<String, String> map) {
    return new PushPayloadParser(map);
  }

  private void emitPayload(PushPayloadParser pushModel) {
    final FirebaseMessagingService serviceRef = this;
    // We need to run this on the main thread, as the React code assumes that is true.
    // Namely, DevServerHelper constructs a Handler() without a Looper, which triggers:
    // "Can't create handler inside thread that has not called Looper.prepare()"
    Handler handler = new Handler(Looper.getMainLooper());
    handler.post(new Runnable() {
      public void run() {
        // Construct and load our normal React JS code bundle
        final ReactInstanceManager mReactInstanceManager = ((ReactApplication) serviceRef.getApplication()).getReactNativeHost().getReactInstanceManager();
        ReactContext context = mReactInstanceManager.getCurrentReactContext();
        // If it's constructed, send a notification
        if (context != null) {
          PushEvtEmitter pushEmitter = new PushEvtEmitter(context);
          pushEmitter.notifyPushReceived(pushModel);
        } else {
          // Otherwise wait for construction, then send the notification
          mReactInstanceManager.addReactInstanceEventListener(new ReactInstanceManager.ReactInstanceEventListener() {
            public void onReactContextInitialized(ReactContext context) {
              PushEvtEmitter pushEmitter = new PushEvtEmitter(context);
              pushEmitter.notifyPushReceived(pushModel);
              mReactInstanceManager.removeReactInstanceEventListener(this);
            }
          });
          if (!mReactInstanceManager.hasStartedCreatingInitialContext()) {
            // Construct it in the background
            mReactInstanceManager.createReactContextInBackground();
          }
        }
      }
    });
  }

}
