// 9fbef606107a605d69c0edbcd8029e5d
package com.mobileapp;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.akamai.react.RNAkamaibmpPackage;
import com.facebook.react.PackageList;
import com.facebook.react.ReactApplication;
import com.iovation.mobile.android.FraudForceConfiguration;
import com.iovation.mobile.android.FraudForceManager;
import com.mkuczera.RNReactNativeHapticFeedbackPackage;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.soloader.SoLoader;

import com.microsoft.codepush.react.CodePush;
import com.microsoft.codepush.react.ReactInstanceHolder;
import com.mobileapp.iovation.IovationPackage;
import com.mobileapp.vibes.VibesPackage;
import com.quantummetric.instrument.QuantumMetric;
import com.quantummetric.instrument.SessionCookieOnChangeListener;
import com.vibes.vibes.Credential;
import com.vibes.vibes.Vibes;
import com.vibes.vibes.VibesConfig;
import com.vibes.vibes.VibesListener;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import io.invertase.firebase.app.ReactNativeFirebaseApp;
import io.invertase.firebase.analytics.ReactNativeFirebaseAnalyticsPackage;

import android.database.CursorWindow;
import java.lang.reflect.Field;

import static com.mobileapp.vibes.VibesModule.VIBES_PREFERENCE;
import static com.mobileapp.vibes.VibesModule.VIBES_TAG;

public class MainApplication extends Application implements ReactApplication {
  public static final String VIBES_REGISTERED = "vibes_registered";
  public static final String TOKEN_KEY = "push_token";
  private ReactInstanceHolder instanceHolder = new ReactInstanceHolder() {
    @Override
    public ReactInstanceManager getReactInstanceManager() {
      return mReactNativeHost.getReactInstanceManager();
    }
  };


  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {

      @Override
      protected String getJSBundleFile(){
        return CodePush.getJSBundleFile();
      }

      @Override
      public boolean getUseDeveloperSupport() {
        return BuildConfig.DEBUG;
      }

        @Override
        protected List<ReactPackage> getPackages() {
          @SuppressWarnings("UnnecessaryLocalVariable")
          List<ReactPackage> packages = new PackageList(this).getPackages();
          // Packages that cannot be autolinked yet can be added manually here, for example
          // packages.add(new FBSDKPackage());
          packages.add(new CodePush(BuildConfig.CODEPUSH_KEY, MainApplication.this, BuildConfig.DEBUG));
          packages.add(new ReactNativeRestartPackage());
          packages.add(new RNAkamaibmpPackage(MainApplication.this));
          packages.add(new VibesPackage());
          packages.add(new IovationPackage());
          return packages;
        }

        @Override
        protected String getJSMainModuleName() {
          return "index";
        }
      };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
//    CodePush.setReactInstanceHolder(instanceHolder);
    super.onCreate();
    /**
     * This initializes the Vibes SDK
     */
    initializeVibes();
    QuantumMetric.initialize(
            AppConstants.QM_SUBSCRIPTION,
            AppConstants.QM_UID,
            this
    ).start();
    SoLoader.init(this, /* native exopackage */ false);
    // initializeFlipper(this); // Remove this line if you don't want Flipper enabled
    //FirebaseApp.initializeApp(this);
    ReactNativeFirebaseApp.setApplicationContext(this);

    QuantumMetric.addSessionCookieOnChangeListener(
            new SessionCookieOnChangeListener() {
              @Override
              protected void onChange(
                      String sessionCookie,
                      String userCookie) {
                SharedPreferences pref = getApplicationContext().getSharedPreferences(AppConstants.QM_STORAGE_NAME,MODE_PRIVATE); // 0 - for private mode
                SharedPreferences.Editor editor = pref.edit();
                editor.putString(AppConstants.QM_SESSION_COOKIE_NAME, sessionCookie);
                editor.commit();
              }
            });
    /**
     * When the product data is bigger than 2 Mb in a field then it is throwing an error
     * while retrieving field data (Async Storage)
     * To avoid this scenario added below code
     * It supports from API level 28
     */
    try {
      Field field = CursorWindow.class.getDeclaredField("sCursorWindowSize");
      field.setAccessible(true);
      field.set(null, 10 * 1024 * 1024); //the 10MB is the new size
    } catch (Exception e) {
      if (e != null) {
        e.printStackTrace();
      }
    }

    //Iovation SDK Initialization
    FraudForceConfiguration fraudForceConfiguration = new FraudForceConfiguration.Builder()
      .subscriberKey("PxwOAtH1sWE0OXSlZXvyxxKrykoTjA5Pq6plyfQQSQE")
      .build();
    FraudForceManager fraudForceManager = FraudForceManager.getInstance();
    fraudForceManager.initialize(fraudForceConfiguration,getApplicationContext());
  }
  private void initializeVibes() {
    if (BuildConfig.DEBUG) Log.d(VIBES_TAG, "Initializing Vibes SDK");

    VibesConfig config = new VibesConfig.Builder()
      .setApiUrl(getString(R.string.vibes_server_url))
      .setAppId(getString(R.string.vibes_app_id))
      .build();
    Vibes.initialize(super.getApplicationContext(), config);
    Vibes.getInstance().registerDevice(new VibesListener<Credential>() {
      public void onSuccess(Credential credential) {
        SharedPreferences preferences = getSharedPreferences(VIBES_PREFERENCE, MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(MainApplication.VIBES_REGISTERED, true);
        editor.apply();
        String deviceId = credential.getDeviceID();
        Log.d(VIBES_TAG, "Device id obtained is --> " + deviceId);
        String pushToken = preferences.getString(MainApplication.TOKEN_KEY, null);
        if (pushToken == null) {
          Log.d(VIBES_TAG, "Token not yet available. Skipping registerPush");
          return;
        }
        Log.d(VIBES_TAG, "Token found after registering device. Attempting to register push token");
        registerPush(pushToken);
      }

      public void onFailure(String errorText) {
        Log.e(VIBES_TAG, "Failure registering device with vibes: " + errorText);
      }
    });
  }


  /**
   * To be able to target each device, we need to send the push token generated by the Firebase environment to the
   * server-side via the SDK. However, ensure that {@link Vibes#registerDevice()} has been called before this is called.
   *
   * @param pushToken
   */
  public static void registerPush(String pushToken) {
    Log.d(VIBES_TAG, "Main Application::Registering push token with vibes");
    Vibes.getInstance().registerPush(pushToken, new VibesListener<Void>() {
      public void onSuccess(Void credential) {
        Log.d(VIBES_TAG, "Push token registration success");
      }

      public void onFailure(String errorText) {
        Log.d(VIBES_TAG, "Failure registering token with vibes: " + errorText);
      }
    });
  }
  /**
   * Loads Flipper in React Native templates.
   *
   * @param context
   */
  private static void initializeFlipper(Context context) {
    if (BuildConfig.DEBUG) {
      try {
        /*
         We use reflection here to pick up the class that initializes Flipper,
        since Flipper library is not available in release mode
        */
        Class<?> aClass = Class.forName("com.facebook.flipper.ReactNativeFlipper");
        aClass.getMethod("initializeFlipper", Context.class).invoke(null, context);
      } catch (ClassNotFoundException e) {
        e.printStackTrace();
      } catch (NoSuchMethodException e) {
        e.printStackTrace();
      } catch (IllegalAccessException e) {
        e.printStackTrace();
      } catch (InvocationTargetException e) {
        e.printStackTrace();
      }
    }
  }
}
