// 9fbef606107a605d69c0edbcd8029e5d
/**
 * @format temp test for QG
 */

import {AppRegistry} from 'react-native';
import MobileApp from './src/pages/_app';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => MobileApp);

// eslint-disable-next-line
console.disableYellowBox = true;
