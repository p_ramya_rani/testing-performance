#!/usr/bin/bash
#9fbef606107a605d69c0edbcd8029e5d
echo "Current Directory"
pwd
ls -ltrh
npm install -g react-native-cli

TOKEN="R0koMqB4vinyJTdV7iR7U3STqWD0jjUI"

case "$ENVIRONMENT" in
    #case 1
    "INT")
		if [ "$APPLICATION" == "tcp" ]
		then
			RAYGUN_APP_ID="1rfc0ce"
		else
			RAYGUN_APP_ID="1rfc94a"
		fi
		;;

    #case 2
    "UAT")
		if [ "$APPLICATION" == "tcp" ]
		then
			RAYGUN_APP_ID="1uvhkzr"
		else
			RAYGUN_APP_ID="1uvhndr"
		fi
		;;

    #case 3
    "PROD")
		if [ "$APPLICATION" == "tcp" ]
		then
			RAYGUN_APP_ID="1rfc5ij"
		else
			RAYGUN_APP_ID="1rfcdvi"
		fi
		;;

	#case 4
    "UAT_2")
		if [ "$APPLICATION" == "tcp" ]
		then
			RAYGUN_APP_ID="1uvhkzr"
		else
			RAYGUN_APP_ID="1uvhndr"
		fi
		;;
	#case 1
    "INT_2")
		if [ "$APPLICATION" == "tcp" ]
		then
			RAYGUN_APP_ID="1rfc0ce"
		else
			RAYGUN_APP_ID="1rfc94a"
		fi
		;;
    "UAT_3")
		if [ "$APPLICATION" == "tcp" ]
		then
			RAYGUN_APP_ID="1uvhkzr"
		else
			RAYGUN_APP_ID="1uvhndr"
		fi
		;;
    "INT_3")
		if [ "$APPLICATION" == "tcp" ]
		then
			RAYGUN_APP_ID="1rfc0ce"
		else
			RAYGUN_APP_ID="1rfc94a"
		fi
		;;

esac


if [ "$APP_CENTER_CURRENT_PLATFORM" == "android" ]
then
	# Android
	react-native bundle --platform android --dev false --entry-file index.js --bundle-output ./android-release.bundle --sourcemap-output ./android-release.bundle.map
	URL="index.android.bundle"
	PATH="\\\android-release.bundle.map"
	echo "URL : ${URL}"
	echo "PATH : ${PATH}"
	echo "RAYGUN_APP_ID : ${RAYGUN_APP_ID}"
	echo "TOKEN : ${TOKEN}"
	/usr/bin/curl -X POST -F url="${URL}" -F file=@./android-release.bundle https://app.raygun.com/upload/jssymbols/${RAYGUN_APP_ID}?authToken=$TOKEN
	/usr/bin/curl -X POST -F url="${PATH}" -F file=@./android-release.bundle.map https://app.raygun.com/upload/jssymbols/${RAYGUN_APP_ID}?authToken=$TOKEN
else
    react-native bundle --platform ios --dev false --entry-file index.js --bundle-output ./ios-release.bundle --sourcemap-output ./ios-release.bundle.map
	URL="main.jsbundle"
	PATH="file://reactnative.local/main.jsbundle"
	echo "URL : ${URL}"
	echo "PATH : ${PATH}"
	echo "RAYGUN_APP_ID : ${RAYGUN_APP_ID}"
	echo "TOKEN : ${TOKEN}"
	/usr/bin/curl -X POST -F url="${URL}" -F file=@./ios-release.bundle https://app.raygun.com/upload/jssymbols/${RAYGUN_APP_ID}?authToken=$TOKEN
	/usr/bin/curl -X POST -F url="${PATH}" -F file=@./ios-release.bundle.map https://app.raygun.com/upload/jssymbols/${RAYGUN_APP_ID}?authToken=$TOKEN
fi
