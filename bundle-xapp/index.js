// 9fbef606107a605d69c0edbcd8029e5d
const axios = require('axios');
const path = require('path');
const fs = require('fs');
const { argv } = require('optimist');

const updatedTimeStamp = new Date().getTime();
const assetsDir = path.resolve(__dirname, '../packages/mobileapp/src/assets/json');

const getXappDataJSON = (brandId, endpoint) => {
  const brand = { tcp: 'TCP', gym: 'Gymboree' }[brandId];
  axios
    .post(
      `${endpoint}/graphql`,
      {
        operationName: 'fetchCMSData',
        variables: {},
        query: `query fetchCMSData {configurationKey(brand: "${brand}", country: "USA", channel: "Mobile") {  key  value  errorMessage __typename } __typename }`,
      },
      {
        timeout: 20000,
        headers: { 'Content-Type': 'application/json' },
      }
    )
    .then((response) => {
      if (response && response.data && response.data.data) {
        const { configurationKey } = response.data.data;
        fs.writeFileSync(
          `${assetsDir}/${brandId.toLowerCase()}-xapp.json`,
          JSON.stringify({
            lastUpdated: updatedTimeStamp,
            configurationKey,
          })
        );
      }
    })
    .catch((error) => {
      console.error(error);
    });
};
console.log('tcpUrl---->', argv.tcpUrl);
console.log('gymUrl---->', argv.gymUrl);

getXappDataJSON(
  'tcp',
  argv.tcpUrl ? argv.tcpUrl.replace('www', 'rwd-stage') : 'https://rwd-stage.childrensplace.com'
);
getXappDataJSON(
  'gym',
  argv.gymUrl ? argv.gymUrl.replace('www', 'rwd-stage') : 'https://rwd-stage.gymboree.com'
);
