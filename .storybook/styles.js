// 9fbef606107a605d69c0edbcd8029e5d 
import { css } from 'styled-components';

export default css`
  .gm-err-autocomplete {
    background-image: none !important;
    padding: 0;

    &::placeholder {
      color: transparent;
    }
  }
`;
