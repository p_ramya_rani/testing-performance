// 9fbef606107a605d69c0edbcd8029e5d
module.exports = (api) => {
  api.cache(true);

  return {
    presets: [
      [
        '@babel/env',
        {
          targets: {
            node: '8.9',
          },
        },
      ],
      [
        '@babel/preset-react',
        {
          development: process.env.BABEL_ENV !== 'build',
        },
      ],
    ],
    plugins: [
      '@babel/plugin-proposal-class-properties',
      '@babel/transform-flow-strip-types',
      '@babel/plugin-syntax-dynamic-import',
      '@babel/plugin-proposal-optional-chaining',
    ],
    env: {
      build: {
        ignore: ['**/*.story.tsx', '__snapshots__', '__tests__', '__stories__'],
      },
      production: {
        plugins: ['transform-remove-console'],
      },
    },
    ignore: ['node_modules'],
  };
};
