// 9fbef606107a605d69c0edbcd8029e5d
const fs = require('fs');
const path = require('path');
const endOfLine = require('os').EOL;

const envToJSONMap = [
  {
    pattern: 'INT_0.json',
    envFile: '.env.int0',
  },
  {
    pattern: 'INT_0_Preview.json',
    envFile: '.env.int0-preview',
  },
  {
    pattern: 'INT.json',
    envFile: '.env.int',
  },
  {
    pattern: 'INT_Preview.json',
    envFile: '.env.preview',
  },
  {
    pattern: 'PROD.json',
    envFile: '.env.prod',
  },
  {
    pattern: 'PROD_Preview.json',
    envFile: '.env.prod-preview',
  },
  {
    pattern: 'QA.json',
    envFile: '.env.qa',
  },
  {
    pattern: 'QA_Preview.json',
    envFile: '.env.qa-preview',
  },
  {
    pattern: 'UAT.json',
    envFile: '.env.uat',
  },
  {
    pattern: 'UAT_Preview.json',
    envFile: '.env.uat-preview',
  },
  {
    pattern: 'INT_2.json',
    envFile: '.env.int2',
  },
  {
    pattern: 'INT_3.json',
    envFile: '.env.int3',
  },
  {
    pattern: 'INT_2_Preview.json',
    envFile: '.env.int2-preview',
  },
  {
    pattern: 'INT_3_Preview.json',
    envFile: '.env.int3-preview',
  },
  {
    pattern: 'UAT_2.json',
    envFile: '.env.uat2',
  },
  {
    pattern: 'UAT_3.json',
    envFile: '.env.uat3',
  },
  {
    pattern: 'UAT_2_Preview.json',
    envFile: '.env.uat2-preview',
  },
  {
    pattern: 'UAT_3_Preview.json',
    envFile: '.env.uat3-preview',
  },
  {
    pattern: 'INT_4.json',
    envFile: '.env.int4',
  },
  {
    pattern: 'INT_4_Preview.json',
    envFile: '.env.int4-preview',
  },
  {
    pattern: 'UAT_4.json',
    envFile: '.env.uat4',
  },
  {
    pattern: 'UAT_4_Preview.json',
    envFile: '.env.uat4-preview',
  },
  {
    pattern: 'INT_0.json',
    envFile: '.env.int0',
  },
  {
    pattern: 'INT_0_Preview.json',
    envFile: '.env.int0-preview',
  },
  {
    pattern: 'INT_5.json',
    envFile: '.env.int5',
  },
  {
    pattern: 'INT_5_Preview.json',
    envFile: '.env.int5-preview',
  },
  {
    pattern: 'UAT_5.json',
    envFile: '.env.uat5',
  },
  {
    pattern: 'UAT_5_Preview.json',
    envFile: '.env.uat5-preview',
  },
  {
    pattern: 'DR_STAGE.json',
    envFile: '.env.drstage',
  },
];

const directoryPath = path.resolve(__dirname, '../packages/mobileapp/src/env/');
const devOpsFileDirectory = path.resolve(__dirname, '../devops/mobile/');

const convertEnvToJSON = fileName => {
  const result = [];
  const envFile = fs.readFileSync(path.resolve(`${directoryPath}/${fileName}`), 'utf-8');
  result.push({
    name: 'ENV',
    value: fileName.replace('.env.', ''),
  });
  envFile.split(endOfLine).forEach(newLine => {
    if (newLine && newLine.trim().length) {
      const indexOfSeperator = newLine.indexOf('=');
      if (indexOfSeperator > 0) {
        result.push({
          name: newLine.slice(0, indexOfSeperator),
          value: newLine.slice(indexOfSeperator + 1),
        });
      }
    }
  });
  return result;
};

fs.readdir(devOpsFileDirectory, (err, files) => {
  files.forEach(file => {
    const devOpsFile = fs.readFileSync(`${devOpsFileDirectory}/${file}`);
    const devOpsObject = JSON.parse(devOpsFile);
    const { envFile } = envToJSONMap.find(jsonMap => file.includes(jsonMap.pattern));
    if (envFile) {
      const existingEnv = devOpsObject.environmentVariables;
      devOpsObject.environmentVariables = convertEnvToJSON(envFile);
      devOpsObject.environmentVariables = Object.assign(
        devOpsObject.environmentVariables,
        existingEnv
      );
      fs.writeFileSync(`${devOpsFileDirectory}/${file}`, JSON.stringify(devOpsObject, null, 2));
    }
  });
});
