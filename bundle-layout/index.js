// 9fbef606107a605d69c0edbcd8029e5d
const axios = require('axios');
const path = require('path');
const fs = require('fs');
const { argv } = require('optimist');

const updatedTimeStamp = new Date().getTime();
const assetsDir = path.resolve(__dirname, '../packages/mobileapp/src/assets/json');
const {
  buildQuery: buildLayoutQuery,
} = require('../packages/core/src/services/handler/graphQL/queries/layout/layout.query.string');

function getLayoutQuery(brandId) {
  return buildLayoutQuery({
    pageName: 'home',
    path: 'home',
    brand: brandId === 'GYM' ? 'Gymboree' : brandId,
    country: 'USA',
    channel: 'Mobile',
  });
}

function processModules(modules) {
  return Object.values(modules).reduce((newslots, slot) => {
    const { set = [], name: moduleName = '', columns = [], contentId, composites } = slot;
    const sets = set.reduce((newset, { key, val }) => {
      const s = {};
      s[key] = val;
      return { ...newset, ...s };
    }, {});
    const obj = {};
    obj[contentId] = { moduleName, columns, ...sets, ...composites };
    return { ...newslots, ...obj };
  }, {});
}

function getModuleQuery({ slot, moduleName, contentId }) {
  if (moduleName === null) return '';
  const {
    buildQuery: moduleQuery,
    // eslint-disable-next-line import/no-dynamic-require, global-require
  } = require(`../packages/core/src/services/handler/graphQL/queries/${moduleName}/${moduleName}.query.string.js`);
  return moduleQuery({
    slot,
    contentId,
    lang: '',
  });
}
const getLayoutDataJSON = (brandId, endpoint) => {
  axios
    .post(
      `${endpoint}/graphql`,
      {
        operationName: 'fetchCMSData',
        variables: {},
        query: `query fetchCMSData { ${getLayoutQuery(brandId)} }`,
      },
      {
        timeout: 20000,
        headers: {
          'Content-Type': 'application/json',
        },
      }
    )
    .then(layoutResponse => {
      if (layoutResponse && layoutResponse.data && layoutResponse.data.data) {
        const {
          home: { items },
        } = layoutResponse.data.data;
        const {
          layout: { slots },
        } = (items && items[0]) || {
          layout: {
            slots: [],
          },
        };
        const modulesQuery = slots.reduce((queryString, module) => {
          const { name, contentId, moduleName, value } = module;
          if (moduleName === 'module2columns') {
            const contentIds = contentId.split(',');
            const moduleNames = value.split(',');
            const query1 = getModuleQuery({
              slot: `${name}_0`,
              moduleName: moduleNames[0],
              contentId: contentIds[0],
            });
            const query2 = getModuleQuery({
              slot: `${name}_1`,
              moduleName: moduleNames[1],
              contentId: contentIds[1],
            });
            return queryString.concat([query1, query2]);
          }
          return queryString.concat(
            getModuleQuery({
              slot: name,
              moduleName,
              contentId,
            })
          );
        }, []);

        // eslint-disable-next-line no-unused-expressions
        modulesQuery.length > 0 &&
          axios
            .post(
              `${endpoint}/graphql`,
              {
                operationName: 'fetchCMSData',
                variables: {},
                query: `query fetchCMSData { ${modulesQuery} }`,
              },
              {
                timeout: 20000,
                headers: {
                  'Content-Type': 'application/json',
                },
              }
            )
            .then(modulesResponse => {
              if (modulesResponse && modulesResponse.data && modulesResponse.data.data) {
                const modules = processModules(modulesResponse.data.data);
                fs.writeFileSync(
                  `${assetsDir}/${brandId.toLowerCase()}-layout.json`,
                  JSON.stringify({
                    lastUpdated: updatedTimeStamp,
                    layout: layoutResponse.data.data,
                    modules,
                  })
                );
              }
            });
      }
    })
    .catch(error => {
      console.error(error);
    });
};
console.log('tcpUrl---->', argv.tcpUrl);
console.log('gymUrl---->', argv.gymUrl);

getLayoutDataJSON(
  'TCP',
  argv.tcpUrl ? argv.tcpUrl.replace('www', 'rwd-stage') : 'https://rwd-stage.childrensplace.com'
);
getLayoutDataJSON(
  'GYM',
  argv.gymUrl ? argv.gymUrl.replace('www', 'rwd-stage') : 'https://rwd-stage.gymboree.com'
);
