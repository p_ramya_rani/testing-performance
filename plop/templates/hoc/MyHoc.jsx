// 9fbef606107a605d69c0edbcd8029e5d 
import React, { Fragment } from 'react';

const MyHoc = WrappedComponent => {
  return props => {
    return (
      <Fragment {...props}>
        <WrappedComponent {...props} />
      </Fragment>
    );
  };
};

export default MyHoc;
