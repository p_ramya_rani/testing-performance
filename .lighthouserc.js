module.exports = {

  ci: {

    collect: {
      url: ['https://localhost:3000/us/home'],
      startServerCommand: 'yarn start:web:lighthouse',
      numberOfRuns: 5,
      settings: {
        chromeFlags: "--no-sandbox",
        onlyCategories: ['performance', 'pwa'],
        formFactor: 'mobile'
      },
    },
    assert: {
      preset: "lighthouse:recommended",
      assertions: {
       // config: "./lighthouserc.js",
        total-blocking-time: ["error", {"maxNumericValue": 1500}],
      },
    },
  },
};
